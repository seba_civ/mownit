import houghtransform.HoughTransform;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ImgUtils.ImgUtils.deepCopy;
import static ImgUtils.ImgUtils.extendImage;
import static ImgUtils.ImgUtils.saveImage;

public class FontGenerator {
    private final BufferedImage inputImage;
    private final String name;
    private final Map<Integer, String> letterMap;

    private FontGenerator(BufferedImage inputImage, String fontName) {
        this.inputImage = inputImage;
        this.name = fontName;
        this.letterMap = getMap();
    }

    private Map<Integer, String> getMap() {
        int alphabetSize = 'z'-'a'+1;
        Map<Integer, String> map = IntStream.range(0, alphabetSize)
                .mapToObj(j -> j)
                .collect(Collectors.toMap(i -> i, i -> String.valueOf((char)(i + 'a'))));
        map.putAll(IntStream.range(0, 10)
                .mapToObj(j -> j)
                .collect(Collectors.toMap(i -> i+alphabetSize, String::valueOf)));
        List<String> chars = new ArrayList<>(Arrays.asList("dot", "comma", "ex", "qu"));
        map.putAll(IntStream.range(0, 4)
                .mapToObj(j -> j)
                .collect(Collectors.toMap(i -> i+alphabetSize+10, chars::get)));
        return map;
    }

    private void generate() {
        BufferedImage gray = ImagePreprocessor.toGray(null, inputImage);
        BufferedImage binarized = ImagePreprocessor.binarize(null, gray);
        HoughTransform transform = new HoughTransform(binarized);
        List<BufferedImage> words = transform.lineDetection(-1, -1).stream()
                .map(FontGenerator.getWords).flatMap(l -> l.stream().map(cutLetter))
                .collect(Collectors.toList());
        boolean successful = Paths.get(System.getProperty("user.dir"), name).toFile().mkdir();
        letterMap.entrySet()
                .forEach(e -> saveImage(Paths.get(name, e.getValue()+".png").toString(), words.get(e.getKey())));
    }

    private static Function<BufferedImage, List<BufferedImage>> getWords = img -> {
        BufferedImage lineRotated = ImagePreprocessor.rotate90(img);
        HoughTransform transform = new HoughTransform(lineRotated);
        return transform.wordDetection(img, 6, 2, null).stream()
                .map(i -> extendImage(i, 0, 0.3))
                .collect(Collectors.toList());
    };

    private static Function<BufferedImage, BufferedImage> cutLetter = img -> {
        HoughTransform transform = new HoughTransform(img);
        List<BufferedImage> letters = transform.lineDetection(13, 1, null);
        if (letters.size() != 1) {
            System.out.println("problem " + letters.size());
            saveImage("problem.png", img);
            return new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        }
        BufferedImage cut = letters.get(0);
        return deepCopy(cut.getSubimage(1, 1, cut.getWidth()-2, cut.getHeight()-2));
    };

    public static void generateFont(String filename, String fontName) throws IOException {
        File font = Paths.get(System.getProperty("user.dir"), filename).toFile();
        FontGenerator fGen = new FontGenerator(ImageIO.read(font), fontName);
        fGen.generate();
    }
}
