import ImgUtils.ImgUtils;
import houghtransform.HoughTransform;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.function.Function;
import java.util.stream.IntStream;

import static ImgUtils.ImgUtils.extendImage;
import static ImgUtils.ImgUtils.saveImage;
import static ImgUtils.ImgUtils.shrinkImage;
import static java.lang.Math.max;

public class ImagePreprocessor {

    public static BufferedImage toGray(String name, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage imageGray = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                Color c = new Color(image.getRGB(j, i));
                int red = (int)(c.getRed() * 0.299);
                int green = (int)(c.getGreen() * 0.587);
                int blue = (int)(c.getBlue() * 0.114);
                int grey = red+green+blue;
                imageGray.setRGB(j, i, new Color(grey, grey, grey).getRGB());
            }
        }
        if (name != null) saveImage(name, imageGray);
        return imageGray;
    }

    private static int[] imageHistogram(BufferedImage input) {
        int[] histogram = new int[256];
        for (int i=0; i<histogram.length; i++) histogram[i] = 0;
        for (int i=0; i<input.getWidth(); i++) {
            for (int j=0; j<input.getHeight(); j++) {
                int red = new Color(input.getRGB (i, j)).getRed();
                histogram[red]++;
            }
        }
        return histogram;
    }

    private static int otsuTreshold(BufferedImage original) {
        int[] histogram = imageHistogram(original);
        int total = original.getHeight() * original.getWidth();
        float sum = 0;
        for (int i=0; i<256; i++) sum += i * histogram[i];
        float sumB = 0;
        int wB = 0;
        int wF = 0;
        float varMax = 0;
        int threshold = 0;
        for (int i=0 ; i<256 ; i++) {
            wB += histogram[i];
            if(wB == 0) continue;
            wF = total - wB;
            if(wF == 0) break;
            sumB += (float) (i * histogram[i]);
            float mB = sumB / wB;
            float mF = (sum - sumB) / wF;
            float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);
            if (varBetween > varMax) {
                varMax = varBetween;
                threshold = i;
            }
        }
        return threshold;
    }

    public static BufferedImage binarize(String name, BufferedImage original) {
        int newPixel;
        int threshold = otsuTreshold(original);
        BufferedImage binarized = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
        for (int i=0; i<original.getWidth(); i++) {
            for (int j=0; j<original.getHeight(); j++) {
                int red = new Color(original.getRGB(i, j)).getRed();
                if (red > threshold) {
                    newPixel = Color.WHITE.getRGB();
                } else {
                    newPixel = Color.BLACK.getRGB();
                }
                binarized.setRGB(i, j, newPixel);
            }
        }
        if (name != null) saveImage(name, binarized);
        return binarized;
    }

    public static BufferedImage rotate(BufferedImage image, double angle) {
        AffineTransform tx = new AffineTransform();
        tx.rotate(angle * Math.PI / 180.0, image.getHeight() / 2, image.getWidth() / 2);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        BufferedImage rotated =  op.filter(image, null);
        IntStream.range(0, rotated.getWidth())
                .forEach(i -> IntStream.range(0, rotated.getHeight())
                        .forEach(j -> {
                            if (rotated.getRGB(i, j) == 0) rotated.setRGB(i, j, Color.WHITE.getRGB());
                        }));
        return rotated;
    }

    public static BufferedImage rotate90(BufferedImage image) {
        int diff = image.getHeight() - image.getWidth();
        if (diff >= 0) {
            BufferedImage img = extendImage(image, diff + 1, 0);
            return shrinkImage(rotatedAdjusted(img, rotate(img)), 0, diff + 1);
        }
        return rotatedAdjusted(image, rotate(image));
    }

    private static BufferedImage rotate(BufferedImage image) {
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.quadrantRotate(1, image.getHeight() / 2, image.getWidth() / 2);
        AffineTransformOp operation = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);
        return operation.filter(image, null);
    }

    private static BufferedImage rotatedAdjusted(BufferedImage image, BufferedImage rotated) {
        BufferedImage rotated_adjusted = new BufferedImage(image.getHeight(), image.getWidth(), image.getType());
        Function<Integer, Integer> cordMapX = x -> max(rotated.getWidth()-image.getHeight()+x, 0);
        Function<Integer, Integer> cordMapY = y -> max(rotated.getHeight()-image.getWidth()+y, 0);
        try {
            IntStream.range(0, image.getHeight())
                    .forEach(x -> IntStream.range(0, image.getWidth())
                            .forEach(y -> rotated_adjusted.setRGB(x, y, rotated.getRGB(cordMapX.apply(x), cordMapY.apply(y)))));
            return rotated_adjusted;
        } catch (ArrayIndexOutOfBoundsException e) {
            saveImage("pro1.png", image);
            saveImage("pro2.png", rotated);
            e.printStackTrace();
        }
        return rotated_adjusted;
    }

    public static BufferedImage scale(BufferedImage image, double scale) {
        AffineTransform tx = new AffineTransform();
        tx.scale(scale, scale);
        AffineTransformOp operation = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        BufferedImage scaled = operation.filter(image, null);
        IntStream.range(0, scaled.getWidth())
                .forEach(i -> IntStream.range(0, scaled.getHeight())
                        .forEach(j -> {
                            if (scaled.getRGB(i, j) == 0) scaled.setRGB(i, j, Color.WHITE.getRGB());
                        }));
        return scaled;
    }

    public static BufferedImage full(BufferedImage image) throws IOException {
        BufferedImage gray = ImagePreprocessor.toGray("gray.png", image);
        BufferedImage binarized = ImagePreprocessor.binarize("bin.png", gray);
        HoughTransform transform = new HoughTransform(binarized);
        double angle = transform.rotationDetection(15, 5);
        System.out.println("Image rotation " + (90.0 - angle) + " degrees");
        transform.writeOutputImage("rotation_hough.png");
        BufferedImage rotated = ImagePreprocessor.rotate(binarized, 90.0 - angle);
        ImgUtils.saveImage("rotated.png", rotated);
        return ImagePreprocessor.binarize("bin2.png", rotated);
    }
}
