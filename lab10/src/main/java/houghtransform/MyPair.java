package houghtransform;

/**
 * Created by Sebastian on 2016-06-04.
 */

public class MyPair<T> {
    private T first;
    private T second;

    public MyPair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T first() {
        return first;
    }

    public T second() {
        return second;
    }

    @Override
    public String toString() {
        return first + ", " + second;
    }
}
