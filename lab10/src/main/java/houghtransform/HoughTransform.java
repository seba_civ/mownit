package houghtransform;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ImgUtils.ImgUtils.*;

public class HoughTransform {
    private final BufferedImage inputImage;
    private DataArray inputData;
    private DataArray outputData = null;
    private int width;
    private int height;

    public HoughTransform(BufferedImage image) {
        this.inputImage = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.inputData = new DataArray(image.getRGB(0, 0, width, height, null, 0, width), width, height);
    }

    public double rotationDetection(int thetaDelta, int thetaDensity) {
        calculateTransform(thetaDelta, thetaDensity, contrast);
        return angle(outputData.getMaxColId(), thetaDelta, thetaDensity);
    }

    private void calculateTransform(int thetaDelta, int thetaDensity, BiPredicate<Integer, Integer> acceptor) {
        int maxRadius = (int) Math.ceil(2*Math.hypot(width, height));
        int halfRAxisSize = maxRadius >>> 1;
        this.outputData = new DataArray(2*thetaDelta*thetaDensity, maxRadius);
        double leftAngle = (90-thetaDelta)*Math.PI/180;
        double stepAngle = Math.PI/(180*thetaDensity);
        List<Double> sinTable = IntStream.range(0, 2*thetaDelta*thetaDensity)
                .mapToObj(i -> Math.sin(leftAngle + i*stepAngle))
                .collect(Collectors.toList());
        List<Double> cosTable = IntStream.range(0, 2*thetaDelta*thetaDensity)
                .mapToObj(i -> Math.cos(leftAngle + i*stepAngle))
                .collect(Collectors.toList());
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (acceptor.test(x, y)) {
                    for (int theta = 0; theta < outputData.getWidth(); theta++) {
                        double r = cosTable.get(theta) * x + sinTable.get(theta) * y;
                        int rScaled = (int) Math.round(r) + halfRAxisSize;
                        outputData.accumulate(theta, rScaled, 1);
                    }
                }
            }
        }
    }

    private double angle(int colId, int thetaDelta, int thetaDensity) {
        return (90.0 - thetaDelta) + colId * (1.0 / thetaDensity);
    }

    private BiPredicate<Integer, Integer> isBlack = (x, y) -> (inputData.get(x, y) == -16777216);

    private BiPredicate <Integer, Integer> contrast = (x, y) -> {
        List<MyPair<Integer>> neighbours = IntStream.range(-1, 2)
                .mapToObj(i -> IntStream.range(-1, 2)
                        .mapToObj(j -> new MyPair<>(x+i, y+j)))
                .flatMap(l -> l)
                .filter(p -> p.first() >=0 && p.first() < width)
                .filter(p -> p.second() >=0 && p.second() < height)
                .collect(Collectors.toList());
        int centerValue = inputData.get(x, y);
        return !neighbours.stream()
                .filter(p -> Math.abs(inputData.get(p.first(), p.second()) - centerValue) >= 100)
                .collect(Collectors.toList()).isEmpty();
    };

    private List<MyPair<Integer>> calculateIntervals(int widthThreshold, int valueThreshold, List<Integer> data) {
        List<Integer> begins = new ArrayList<>();
        List<Integer> ends = new ArrayList<>();
        for (int i = 1; i < data.size(); i++) {
            if (data.get(i-1) < valueThreshold && data.get(i) >= valueThreshold) {
                if (begins.isEmpty() || i - ends.get(ends.size()-1) > widthThreshold) {
                    begins.add(i);
                } else {
                    ends.remove(ends.size()-1);
                }
            }
            if (data.get(i-1) >= valueThreshold && data.get(i) < valueThreshold) {
                ends.add(i);
            }
        }
        assert begins.size() == ends.size();
        return IntStream.range(0, begins.size())
                .mapToObj(i -> new MyPair<>(begins.get(i), ends.get(i)))
                .collect(Collectors.toList());
    }

    private List<Integer> extractRadiusData() {
        int col = outputData.getWidth()/2;
        List<Integer> d = outputData.getCol(col).collect(Collectors.toList());
        return IntStream.range(0, inputImage.getHeight()).mapToObj(i -> d.get(i + d.size()/2)).collect(Collectors.toList());
    }

    public List<BufferedImage> lineDetection(int widthThreshold, int valueThreshold) {
        return lineDetection(widthThreshold, valueThreshold, null);
    }

    public List<BufferedImage> lineDetection(int widthThreshold, int valueThreshold, String resultName) {
        calculateTransform(2, 2, contrast);
        int w = widthThreshold-1;
        List<MyPair<Integer>> intervals = calculateIntervals(widthThreshold, valueThreshold, extractRadiusData());
        return extractImages(intervals, w, resultName);
    }

    private List<BufferedImage> extractImages(List<MyPair<Integer>> intervals, int w, String resultName) {
        List<BufferedImage> lines = intervals.stream()
                .filter(p -> p.second()-p.first() > w)
                .map(p -> deepCopy(inputImage.getSubimage(0, p.first(), inputImage.getWidth(), p.second()-p.first())))
                .collect(Collectors.toList());
        if (resultName != null) {
            BufferedImage out = deepCopy(inputImage);
            intervals.forEach(p -> IntStream.range(0, out.getWidth()).forEach(i -> out.setRGB(i, p.first(), Color.GREEN.getRGB())));
            intervals.forEach(p -> IntStream.range(0, out.getWidth()).forEach(i -> out.setRGB(i, p.second(), Color.BLUE.getRGB())));
            saveImage(resultName, out);
        }
        return lines;
    }

    public BufferedImage shrinkLetter() {
        calculateTransform(2, 2, isBlack);
        List<MyPair<Integer>> intervals = calculateIntervals(inputImage.getHeight()/2, 1, extractRadiusData());
        List<BufferedImage> letters = extractImages(intervals, 1, null);
        if (letters.size() != 1) {
            System.out.println("problem " + letters.size());
            saveImage("problem.png", inputImage);
            return new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        }
        return shrinkImage(letters.get(0), 1, 0);
    }

    public List<BufferedImage> wordDetection(BufferedImage original, int widthThreshold, int valueThreshold, String resultName) {
        calculateTransform(2, 2, contrast);
        List<MyPair<Integer>> intervals = calculateIntervals(widthThreshold, valueThreshold, extractRadiusData());
        return extractImages(original, intervals, 4, resultName);
    }

    // naive way - works poorly...
    public List<BufferedImage> letterDetection(BufferedImage original, int widthThreshold, int valueThreshold) {
        return letterDetection(original, widthThreshold, valueThreshold, null);
    }

    public List<BufferedImage> letterDetection(BufferedImage original, int widthThreshold, int valueThreshold, String filename) {
        calculateTransform(2, 2, isBlack);
        List<MyPair<Integer>> intervals = calculateIntervals(widthThreshold, valueThreshold, extractRadiusData());
        return extractImages(original, intervals, 3, filename);
    }

    private List<BufferedImage> extractImages(BufferedImage original, List<MyPair<Integer>> intervals, int w, String filename) {
        intervals.stream().filter(p -> p.second()-p.first() > w).collect(Collectors.toList());
        List<BufferedImage> letters = intervals.stream().map(p -> extendImage(original
                .getSubimage(p.first()-1, 0, p.second()-p.first()+2, original.getHeight()), 0, 0.1))
                .collect(Collectors.toList());
        if (filename != null) {
            saveClassificationResult(original, filename, intervals);
        }
        return letters;
    }

    private void saveClassificationResult(BufferedImage original, String filename, List<MyPair<Integer>> intervals) {
        BufferedImage out = deepCopy(original);
        intervals.forEach(p -> IntStream.range(0, out.getHeight()).forEach(i -> out.setRGB(p.first(), i, Color.GREEN.getRGB())));
        intervals.forEach(p -> IntStream.range(0, out.getHeight()).forEach(i -> out.setRGB(p.second(), i, Color.BLUE.getRGB())));
        saveImage(filename, out);
    }

    public BufferedImage writeOutputImage(String filename) throws IOException {
        if (outputData == null) {
            throw new IllegalStateException();
        }
        int max = outputData.getMax();
        BufferedImage outputImage = new BufferedImage(outputData.getWidth(), outputData.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < outputData.getHeight(); y++) {
            for (int x = 0; x < outputData.getWidth(); x++) {
                int n = Math.min((int) Math.round(outputData.get(x, y) * 255.0 / max), 255);
                outputImage.setRGB(x, y, (n << 16) | (n << 8) | 0x90 | -0x01000000);
            }
        }
        saveImage(filename, outputImage);
        return outputImage;
    }
}
