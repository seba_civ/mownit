package houghtransform;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class DataArray {
    private final List<List<Integer>> dataArray;
    private final int width;
    private final int height;

    DataArray(int angles, int radius) {
        this.width = angles;
        this.height = radius;
        this.dataArray = IntStream.range(0, angles)
                .mapToObj(i -> IntStream.range(0, radius)
                        .mapToObj(j -> 0)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    DataArray(int[] dataArray, int width, int height) {
        this.dataArray = IntStream.range(0, width)
                .mapToObj(i -> IntStream.range(0, height)
                        .mapToObj(j -> dataArray[i +  j* width])
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        this.width = width;
        this.height = height;
    }

    int get(int x, int y) {
        return dataArray.get(x).get(y);
    }

    private void set(int x, int y, int value) {
        dataArray.get(x).set(y, value);
    }

    void accumulate(int x, int y, int delta) {
        set(x, y, get(x, y) + delta);
    }

    int getMax() {
        return dataArray.stream().flatMapToInt(l -> l.stream().mapToInt(Integer::intValue)).max().orElse(0);
    }

    int getMaxColId() {
        int max = dataArray.get(0).get(0);
        int maxCol = 0;
        for (int i = 0; i < this.width; i++) {
            int columnMax = dataArray.get(i).stream().mapToInt(j -> j).max().orElse(0);
            if (columnMax > max) {
                max = columnMax;
                maxCol = i;
            }
        }
        return maxCol;
    }

    Stream<Integer> getCol(int colId) {
        return dataArray.get(colId).stream();
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }
}
