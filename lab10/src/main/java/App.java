import ImgUtils.ImgUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        FontParser fontParser = new FontParser();
        Map<String, Map<String, BufferedImage>> fonts = fontParser.parseFonts();

//        try {
//            FontGenerator.generateFont("times_font.png", "times2");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        boolean b = true;
//        while (b) {
//
//        }
        File input = Paths.get(System.getProperty("user.dir"), "texts", args[0]).toFile();
        try {
            BufferedImage binarized = ImagePreprocessor.full(ImageIO.read(input));

            ImageClassifier imageClassifier = new ImageClassifier(binarized);
            List<List<List<BufferedImage>>> letters = imageClassifier.classify();

            LetterRecognizer letterRecognizer = new LetterRecognizer(fonts, letters, 0.75);
            String text = letterRecognizer.recognize();
            System.out.println(text);

            //-----------------------------------------

            //debugTests(fonts, letters);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void debugTests(Map<String, Map<String, BufferedImage>> fonts, List<List<List<BufferedImage>>> letters) {
        Map<String, BufferedImage> f = fonts.get("times");

        BufferedImage let = letters.get(0).get(1).get(0);
        ImgUtils.saveImage("random.png", let);

        double ratio = (let.getHeight()) / ((double) f.get("i").getHeight());
        BufferedImage patternScaled = ImagePreprocessor.scale(f.get("i"), ratio);
        ImgUtils.saveImage("j_scaled.png", ImagePreprocessor.binarize(null, patternScaled));
        double ratio2 = (let.getHeight()) / ((double) f.get("i").getHeight());
        BufferedImage patternScaled2 = ImagePreprocessor.scale(f.get("i"), ratio2);
        ImgUtils.saveImage("i_scaled.png", ImagePreprocessor.binarize(null, patternScaled2));

        Map<String, Double> cor = f.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> LetterRecognizer.compareLetters(let, e.getValue())));
        System.out.println("o " + cor.get("o"));
        Map.Entry<String, Double> cand = cor.entrySet().stream().max((e1, e2) -> e1.getValue().compareTo(e2.getValue())).orElseGet(null);
        String letter = (cand.getValue() < 0.5) ? "#" : cand.getKey();
        System.out.println(letter + " " + cand.getValue());
    }
}
