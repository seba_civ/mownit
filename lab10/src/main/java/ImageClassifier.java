import houghtransform.HoughTransform;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import static ImgUtils.ImgUtils.extendImage;


public class ImageClassifier {
    private final BufferedImage inputImage;
    private static final Random random = new Random();
    private static int ID = 0;

    public ImageClassifier(BufferedImage inputImage) {
        this.inputImage = inputImage;
    }

    public List<List<List<BufferedImage>>> classify() {
        return getLines.apply(inputImage).stream()
                .map(getWords)
                .map(line -> line.stream()
                        .map(getLetters)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    private static Function<BufferedImage, List<BufferedImage>> getLines = img -> {
        HoughTransform transform = new HoughTransform(img);
        return transform.lineDetection(3, 4, "lines_split.png");
    };

    private static Function<BufferedImage, List<BufferedImage>> getWords = line -> {
        BufferedImage lineRotated = ImagePreprocessor.rotate90(line);
        HoughTransform transform = new HoughTransform(lineRotated);
        return transform.wordDetection(line, line.getHeight()/5, 2, getRandomName(0.3))
                .stream().map(i -> extendImage(i, 0.1, 0.1))
                .collect(Collectors.toList());
    };

    private static Function<BufferedImage, List<BufferedImage>> getLetters = word -> {
        BufferedImage wordRotated = ImagePreprocessor.rotate90(word);
        HoughTransform transform = new HoughTransform(wordRotated);
        return transform.letterDetection(word, 0, 2, getRandomName(0.03)).stream()
                .map(let -> new HoughTransform(let).shrinkLetter())
                .collect(Collectors.toList());
    };

    private static String getRandomName(double val) {
        if (random.nextDouble() > val) {
            return null;
        }
        ID++;
        return "split" + ID + ".png";
    }
}
