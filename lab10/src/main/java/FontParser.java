import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FontParser {

    private final Map<String, String> letterMap;

    public FontParser() {
        letterMap = new HashMap<>();
        letterMap.put("comma", ",");
        letterMap.put("ex", "!");
        letterMap.put("qu", "?");
        letterMap.put("dot", ".");
    }

    public Map<String, Map<String, BufferedImage>> parseFonts() {
        Map<String, Map<String, BufferedImage>> fonts = new HashMap<>();
        try {
            fonts.putAll(Files.list(new File(getRes()).toPath())
                    .filter(p -> Files.isDirectory(p))
                    .collect(Collectors.toMap(p -> p.getFileName().toString(), this::parseFont)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //fonts.entrySet().forEach(e -> e.getValue().keySet().forEach(System.out::println));
        return fonts;
    }

    private String getRes() {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource("fonts").getPath();
    }

    private Map<String, BufferedImage> parseFont(Path directory) {
        Map<String, BufferedImage> font = new HashMap<>();
        try {
            font = Files.list(directory)
                    .filter(p -> getExtension(p.getFileName().toString()).equals("png"))
                    .collect(Collectors.toMap(this::getLetter, this::readImage));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return font;
    }

    private BufferedImage readImage(Path path) {
        try {
            return ImageIO.read(path.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }

    private String getLetter(Path path) {
        String name = path.getFileName().toString();
        String letter = name.substring(0, name.lastIndexOf('.'));
        return letterMap.getOrDefault(letter, letter);
    }

    private static String getExtension(String filename) {
        return filename.substring(filename.lastIndexOf('.') + 1);
    }
}
