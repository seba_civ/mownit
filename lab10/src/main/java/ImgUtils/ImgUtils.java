package ImgUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.IntStream;

public class ImgUtils {
    public static BufferedImage deepCopy(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = image.copyData(image.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static BufferedImage extendImage(BufferedImage image, double extraWidth, double extraHeight) {
        int extraW = (int) (image.getWidth() * extraWidth);
        int extraH = (int) (image.getHeight() * extraHeight);
        BufferedImage out = new BufferedImage(image.getWidth() + 2*extraW, image.getHeight() + 2*extraH, image.getType());
        IntStream.range(0, out.getWidth())
                .forEach(x -> IntStream.range(0, out.getHeight())
                        .forEach(y -> out.setRGB(x, y, Color.WHITE.getRGB())));
        IntStream.range(0, image.getWidth())
                .forEach(x -> IntStream.range(0, image.getHeight())
                        .forEach(y -> out.setRGB(x+extraW, y+extraH, image.getRGB(x, y))));
        return out;
    }

    public static BufferedImage extendImage(BufferedImage image, int extraWidth, int extraHeight) {
        BufferedImage out = new BufferedImage(image.getWidth() + 2*extraWidth, image.getHeight() + 2*extraHeight, image.getType());
        IntStream.range(0, out.getWidth())
                .forEach(x -> IntStream.range(0, out.getHeight())
                        .forEach(y -> out.setRGB(x, y, Color.WHITE.getRGB())));
        IntStream.range(0, image.getWidth())
                .forEach(x -> IntStream.range(0, image.getHeight())
                        .forEach(y -> out.setRGB(x+extraWidth, y+extraHeight, image.getRGB(x, y))));
        return out;
    }

    public static BufferedImage shrinkImage(BufferedImage image, int w, int h) {
        BufferedImage out = new BufferedImage(image.getWidth() - 2*w, image.getHeight() - 2*h, image.getType());
        IntStream.range(0, out.getWidth())
                .forEach(x -> IntStream.range(0, out.getHeight())
                        .forEach(y -> out.setRGB(x, y, image.getRGB(x+w, y+h))));
        return out;
    }

    public static void saveImage(String name, BufferedImage image) {
        File out = Paths.get("img_data", name).toFile();
        try {
            ImageIO.write(image, "png", out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
