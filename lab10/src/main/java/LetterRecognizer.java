import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class LetterRecognizer {
    private final  Map<String, Map<String, BufferedImage>> fonts;
    private final List<List<List<BufferedImage>>> letters;
    private final double threshold;

    public LetterRecognizer(Map<String, Map<String, BufferedImage>> fonts, List<List<List<BufferedImage>>> letters, double threshold) {
        this.fonts = fonts;
        this.letters = letters;
        this.threshold = threshold;
    }

    public String recognize() {
        String text = fonts.entrySet().stream().map(f -> letters.parallelStream()
                .map(line -> line.stream()
                        .map(word -> word.stream()
                                .map(let -> bestMatch(let, f.getValue()))
                                .collect(Collectors.joining()))
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n")))
                .max((t1, t2) -> successRatio(t1).compareTo(successRatio(t2)))
                .orElseGet(() -> "");
        long recognized = text.chars().filter(c -> c != '#').count();
        System.out.println(recognized / (double)text.length());
        return text;
    }

    private Double successRatio(String text)  {
        long recognized = text.chars().filter(c -> c != '#').count();
        return recognized / (double)text.length();
    }

    private String bestMatch(BufferedImage let, Map<String, BufferedImage> font) {
        Map<String, Double> correlations = font.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> compareLetters(let, e.getValue())));
        Map.Entry<String, Double> candidate = correlations.entrySet().stream()
                .max((e1, e2) -> e1.getValue().compareTo(e2.getValue())).orElseGet(null);
        return (candidate.getValue() < threshold) ? "#" : candidate.getKey();
    }

    public static double compareLetters(BufferedImage source, BufferedImage pattern) {
        double heightRatio = (source.getHeight()) / ((double) pattern.getHeight());
        if (heightRatio < 0.01) {
            System.out.println(source.getHeight() + " " + pattern.getHeight());
            throw new IllegalArgumentException("sth wrong");
        }
        BufferedImage patternScaled = ImagePreprocessor.binarize(null, ImagePreprocessor.scale(pattern, heightRatio));
        int widthDiff = source.getWidth() - patternScaled.getWidth();
        double widthRatio = source.getWidth() / ((double) pattern.getWidth());
        double ratio = heightRatio / widthRatio;
        if (ratio > 1.5 || ratio < 0.5) {
            return 0.0;
        }
        if (widthDiff > 5 || widthDiff < -5) {
            return 0.0;
        }
        if (widthDiff < 0) {
            BufferedImage tmp = patternScaled;
            patternScaled = source;
            source = tmp;
        }
        int matching = 0;
        for (int i = 0; i < Math.abs(widthDiff)+1; i++) {
            int m = 0;
            for (int x = 0; x < patternScaled.getWidth(); x++) {
                for (int y = 0; y < patternScaled.getHeight(); y++) {
                    if (source.getRGB(x+i, y) == patternScaled.getRGB(x, y)) {
                        m++;
                    }
                }
            }
            matching = Math.max(matching, m);
        }
        return matching/(double)(patternScaled.getWidth()*patternScaled.getHeight());
    }
}
