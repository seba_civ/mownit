from numpy import array, copy, zeros, dot

__author__ = 'Sebastian'


def lu_factorization(matrix):
    # P A Q = L U
    # P, Q - permutation matrices
    n, m = matrix.shape
    if n != m:
        raise ValueError("A is not square matrix")

    A = matrix.copy()  # don't destroy original matrix
    col_id = [i for i in range(n)]
    row_id = [i for i in range(n)]
    for k in range(n):
        p_row, p_col = find_pivot(A, k)

        swap_rows(A, row_id, k, p_row)
        swap_cols(A, col_id, k, p_col)

        scale(A, k)

        back_substitution(A, k)

    P, Q = permutation(col_id, row_id)
    return A, P, Q


def permutation(col_id, row_id):
    n = len(col_id)
    P = zeros((n, n))
    Q = zeros((n, n))
    for i in range(n):
        P[i][row_id[i]] = 1
        Q[i][col_id[i]] = 1
    return P, Q


def swap_cols(A, col_id, k, p_col):
    if p_col != k:
        A[:, k], A[:, p_col] = copy(A[:, p_col]), copy(A[:, k])
        col_id[k], col_id[p_col] = col_id[p_col], col_id[k]


def swap_rows(A, row_id, k, p_row):
    if p_row != k:
        A[k, :], A[p_row, :] = copy(A[p_row, :]), copy(A[k, :])
        row_id[k], row_id[p_row] = row_id[p_row], row_id[k]


def back_substitution(A, k):
    n, m = A.shape
    for row in range(k+1, n):
        A[row, k+1:] -= A[row, k] * A[k, k+1:]


def scale(A, k):  # scaling
    pivot = A[k, k]
    A[k+1:, k] = A[k+1:, k] / pivot


def find_pivot(A, k):  # full pivoting
    n, m = A.shape
    pivot_row = k
    pivot_col = k
    for row in range(k+1, n):
        for col in range(k, n):
            if abs(A[row, col]) > abs(A[pivot_row, pivot_col]):
                pivot_row = row
                pivot_col = col
    return pivot_row, pivot_col


def get_LU(A):  # retrieves L U matrices from A
    n, m = A.shape
    L = zeros((n, n))
    U = zeros((n, n))
    for row in range(n):
        U[row, row:] = A[row, row:]
        L[row, :row] = A[row, :row]
        L[row][row] = 1
    return L, U


def main():
    m = array([[4.0, 3.0],
               [6.0, 3.0]])

    A, P, Q = lu_factorization(m)
    print(A)
    L, U = get_LU(A)
    print(L)
    print(U)
    print(dot(dot(P, m), Q))


if __name__ == "__main__":
    main()
