from copy import copy

import networkx as nx
from numpy import zeros, array, dot
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
from zad1 import gauss_jordan

__author__ = 'Sebastian'


def read_graph():
    graph = nx.Graph()
    with open('graph.txt', 'r') as f:
        s, t, E = [s for s in f.readline().split()]
        counter = 0
        for lines in f:
            u, v, r = [s for s in lines.split()]
            graph.add_edge(int(u), int(v), id=counter, resistance=float(r), voltage=0.0, first=-1)
            counter += 1
        if graph.has_edge(int(s), int(t)):
            i = max(graph.nodes()) + 1
            graph.add_edge(int(s), int(i), id=counter, resistance=0.0, voltage=0.0, first=-1)
            counter += 1
            graph.add_edge(int(i), int(t), id=counter, resistance=0.0, voltage=float(E), first=-1)
        else:
            graph.add_edge(int(s), int(t), id=counter, resistance=0.0, voltage=float(E), first=-1)
    return graph


def construct_system(G):
    cycles = nx.cycle_basis(G)
    rows = len(cycles) + G.number_of_nodes()
    shape = (rows, G.number_of_edges())
    matrix = zeros(shape)
    column_vector = zeros(rows)

    for index, cycle in enumerate(cycles):
        for u, v in zip(cycle, shift_list(cycle)):
            matrix[index][G[u][v]['id']] = get_sign(G, u, v) * G[u][v]['resistance']
            column_vector[index] += G[u][v]['voltage']

    for ind, u in enumerate(G.nodes()):
        index = ind + len(cycles)
        for v in nx.all_neighbors(G, u):
            matrix[index][G[u][v]['id']] = -1.0 * get_sign(G, u, v)

    return matrix, column_vector


def get_sign(G, u, v):
    if G[u][v]['first'] == -1:
        G[u][v]['first'] = u
    return 1.0 if G[u][v]['first'] == u else -1.0


def normalise_equation(A, b, dim):
    transposed = array([[row[i] for row in A] for i in range(0, dim)])
    normalised_A = dot(transposed, A)
    normalised_b = dot(transposed, b)
    return normalised_A, normalised_b


def shift_list(cycle):
    cycle_next = copy(cycle)
    cycle_next.append(cycle_next.pop(0))
    return cycle_next


def draw_graph(graph, label=True):
    pos = nx.spring_layout(graph, scale=100, iterations=100000)
    jet = cm = plt.get_cmap('jet')
    scalar_map = cmx.ScalarMappable(
        norm=colors.Normalize(vmin=0, vmax=max([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])),
        cmap=jet)
    color_list = [scalar_map.to_rgba(abs(e[2]['current'])) for e in graph.edges_iter(data=True)]

    x_max = max([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])
    x_min = min([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])

    def calc_width(x, delta, w_min):
        return (delta * (x - x_min) / (x_max - x_min)) + w_min

    edge_widths = [calc_width(abs(e[2]['current']), 2.0, 0.5) for e in graph.edges_iter(data=True)]

    nx.draw_networkx_nodes(graph, pos, node_color='r', node_size=15)
    nx.draw_networkx_edges(graph, pos, edge_color=color_list, width=edge_widths)
    if label:
        node_labels = dict([(u, str(u)) for u in graph.nodes()])
        edge_labels = dict([((u, v,), str(abs(d['current']))[:4]) for u, v, d in graph.edges(data=True)])
        nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels, font_size=3)
        nx.draw_networkx_labels(graph, pos, labels=node_labels)
    plt.axis('off')
    plt.savefig('graph.png', dpi=300)


def solve():
    G = read_graph()
    A, b = construct_system(G)
    C, d = normalise_equation(A, b, G.number_of_edges())
    solution = gauss_jordan(C, d)
    solved_graph = nx.Graph()
    for edge in G.edges(data=True):
        u, v, d = edge
        if d['voltage'] == 0.0:
            solved_graph.add_edge(u, v, current=solution[G[u][v]['id']])
    return solved_graph


def main():
    draw_graph(solve(), False)


if __name__ == "__main__":
    main()
