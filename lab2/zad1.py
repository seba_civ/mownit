from numpy import array, copy

__author__ = 'Sebastian'

eps = 1e-20


def gauss_jordan(A, b):
    n, m = A.shape
    if n != m:
        raise ValueError("A is not square matrix")

    col_id = [i for i in range(n)]
    for k in range(n):
        p_row, p_col = find_pivot(A, k, n)

        swap_rows(A, b, k, p_row)
        swap_cols(A, col_id, k, p_col)

        scale(A, b, k)

        back_substitution(A, b, k)

    return array(rearrange_cols(b, col_id))


def rearrange_cols(b, col_id):
    n = len(col_id)
    x = [0] * n
    for i in range(n):
        x[col_id[i]] = b[i]
    return x


def swap_cols(A, col_id, k, p_col):
    if p_col != k:
        A[:, k], A[:, p_col] = copy(A[:, p_col]), copy(A[:, k])
        col_id[k], col_id[p_col] = col_id[p_col], col_id[k]


def swap_rows(A, b, k, p_row):
    if p_row != k:
        A[k, :], A[p_row, :] = copy(A[p_row, :]), copy(A[k, :])
        b[k], b[p_row] = b[p_row], b[k]


def back_substitution(A, b, k):
    n = b.size
    for row in range(n):
        if row != k:
            b[row] = b[row] - A[row, k] * b[k]
            A[row, k:] = A[row, k:] - A[row, k] * A[k, k:]


def scale(A, b, k):  # scaling
    pivot = A[k, k]
    if abs(pivot) < eps:
        raise ValueError("matrix is likely singular")
    A[k, k:] = A[k, k:] / pivot
    b[k] = b[k] / pivot


def find_pivot(A, k, n):  # full pivoting
    pivot_row = k
    pivot_col = k
    for row in range(k+1, n):
        for col in range(k, n):
            if abs(A[row, col]) > abs(A[pivot_row, pivot_col]):
                pivot_row = row
                pivot_col = col
    return pivot_row, pivot_col


def main():
    A = array([[0.0, 2.0, 0.0, 1.0],
               [2.0, 2.0, 3.0, 2.0],
               [4.0,-3.0, 0.0, 1.0],
               [6.0, 1.0,-6.0,-5.0]])
    A1 = array([
        [1.2, 1.3, 1.4],
        [2.1, 4.5, 3.6],
        [1.6, 2.7, 3.3]
    ])
    b1 = array([1.2, 3.1, 1.5])
    b = array([0.0, -2.0, -7.0, 6.0])
    print(gauss_jordan(A1, b1))


if __name__ == "__main__":
    main()
