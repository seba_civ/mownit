from itertools import starmap
from random import choice, randrange
import matplotlib.pyplot as plt
from PIL import Image
from numpy import array, exp, zeros
from numpy.random import random


class Point:
    def __init__(self, x, y, filled):
        self.x = x
        self.y = y
        self.filled = filled

    def flip(self):
        self.filled = not self.filled

    def __repr__(self):
        return '1' if self.filled else '0'


class FourNeighbourhoodStrategy:
    def __init__(self, n, positive, negative):
        self.n = n
        self.positive = positive
        self.negative = negative
        self.dist = max(positive[1], negative[1])

    def check(self, x, y, i, j):
        return 0 <= x < self.n and 0 <= y < self.n and not (i == 0 and j == 0)

    def neighbours(self, x, y):
        return [(x + i % 3 - 1, y + i//2 - 1) for i in range(1, 5) if self.check(x + i % 3 - 1, y + i//2 - 1, 1, 1)]

    def scale_neighbours(self, x, y, n):
        l = [(x+(n-i), y+j) for i in range(0, n+1) for j in range(-i, i+1) if self.check(x+(n-i), y+j, (n-i), j)]
        l.extend([(x-(n-i), y+j) for i in range(0, n) for j in range(-i, i+1) if self.check(x-(n-i), y+j, (n-i), j)])
        return l

    def distance(self, p1, p2):
        return abs(p1.x - p2.x) + abs(p1.y - p2.y)


class EightNeighbourhoodStrategy(FourNeighbourhoodStrategy):
    def borders(self, x, n):
        return range(max(-1*n + x, 0), min(n+1+x, self.n))

    def neighbours(self, x, y):
        return self.scale_neighbours(x, y, 1)

    def scale_neighbours(self, x, y, n):
        return [(i, j) for i in self.borders(x, n) for j in self.borders(y, n) if not (i == x and j == y)]

    def distance(self, p1, p2):
        return max(abs(p1.x - p2.x), abs(p1.y - p2.y))


class EightSixteenNeighbourhoodStrategy(EightNeighbourhoodStrategy):
    def neighbours(self, x, y):
        return self.scale_neighbours(x, y, 1)

    def scale_neighbours(self, x, y, n):
        return super().scale_neighbours(x, y, 2*n)

    def distance(self, p1, p2):
        return super().distance(p1, p2) / 2


def acceptance(old_energy, new_energy, temperature):
    return 1.0 if old_energy >= new_energy else exp((old_energy - new_energy) / temperature)


def global_energy(tab, strategy):
    points = list(filter(lambda e: e.filled, [i for row in tab for i in row]))
    return sum([point_energy(tab, p, strategy) for p in points])


def single_energy(distance, factor=10.0):
    return factor / distance


def sign(distance, strategy):
    if strategy.positive[0] <= distance < strategy.positive[1]:
        return -1.0
    elif strategy.negative[0] <= distance < strategy.negative[1]:
        return 1.0
    else:
        return 0.0


def f(n, s):
    return 1.0 if n in s else 2.0


def point_energy(tab, p, strategy, s=set()):
    neighbours = [(n, strategy.distance(p, tab[n])) for n in strategy.scale_neighbours(p.x, p.y, strategy.dist)
                  if tab[n].filled]
    return sum([single_energy(d) * sign(d, strategy) * f(n, s) for n, d in neighbours])


def t_factory(amp, center, width):
    def temperature(x):
        return amp * (1 / (1 + exp((x - center) / width)))

    return temperature


def init_tab(density, n):
    return array([[Point(i, j, density > random()) for j in range(n)] for i in range(n)])


def new_state(tab, strategy, energy):
    s = set()

    def test(arg):
        if arg[3] in s:
            return False
        s.add(arg[3])
        return True

    def check_sth(q):
        return choice(q) if len(q) > 0 else None

    n, m = tab.shape
    # choose some random filled fields to move
    to_move = set(filter(lambda point: point.filled, [tab[randrange(n), randrange(n)] for i in range(n)]))
    # get neighbourhoods
    to_move_filled = [list(filter(lambda p: not tab[p].filled, strategy.neighbours(e.x, e.y))) for e in to_move]
    if len(to_move_filled) == 0:
        return energy, None, None
    # choose some random destinations in neighbourhoods
    dest = list(filter(lambda r: r[1] is not None, zip(to_move, list(map(check_sth, to_move_filled)))))
    if len(dest) == 0:
        return energy, None, None
    a, b, c, d = list(map(list, zip(*filter(test, starmap(lambda g, t: (g, (g.x, g.y), tab[t], t), dest)))))

    s2 = set(b)
    old_energies = [point_energy(tab, p, strategy, s2) for p in a]
    move_points(tab, b, d)
    new_energies = [point_energy(tab, p, strategy, s) for p in c]
    diff = sum(new_energies) - sum(old_energies)
    return energy + diff, b, d


def move_points(tab, origin, destination):
    for i, j in zip(destination, origin):
        tab[i].flip(), tab[j].flip()


def anneal(tab, iterations, strategy, T):
    current_energy = global_energy(tab, strategy)
    print('Start energy', current_energy)

    energies = zeros(iterations)
    temperatures = zeros(iterations)

    for i in range(iterations):
        current_temperature = T(i)
        next_energy, a, b = new_state(tab, strategy, current_energy)
        if acceptance(current_energy, next_energy, current_temperature) > random():
            current_energy = next_energy
        else:
            move_points(tab, b, a)  # reverse change
        energies[i] = current_energy
        temperatures[i] = current_temperature
    print('Final energy', current_energy)
    return tab, energies, temperatures


def main():
    density = 0.1
    n = 60
    it = 25000
    tab = init_tab(density, n)
    generate_image(tab, *tab.shape, 'image_start')
    s = EightNeighbourhoodStrategy(n, (3, 5), (0, 3))
    T = t_factory(100, 0, 2000)
    tab, energies, temperatures = anneal(tab, it, s, T)
    display_results(energies, temperatures)
    generate_image(tab, *tab.shape, 'image_end')


def display_results(energies, temperatures):
    data_figure, axes_e = plt.subplots(2)
    plot_data(axes_e[0], energies, 'energy', 'b')
    plot_data(axes_e[1], temperatures, 'temperature', 'r')
    save_figure(data_figure, 'data')


def save_figure(fig, title):
    fig.savefig(title + '.png', dpi=200)
    plt.close(fig)


def plot_data(axis, data, title, style='b'):
    axis.set_title(title)
    axis.plot(data, style)


def generate_image(data, width, height, name):
    img = Image.frombytes('L', (width, height), bytes([0 if p.filled else 255 for row in data for p in row]))
    img.save(name + '.png')


if __name__ == "__main__":
    main()
