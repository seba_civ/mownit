from math import fsum
from random import random, seed, randint, normalvariate, shuffle
import matplotlib.pyplot as plt
from numpy import array, exp, copy, zeros, insert, delete, copyto, arange
from numpy.linalg import linalg
from matplotlib import animation


class RandomSwapStrategy:
    def __init__(self, n):
        self.n = n
        self.name = 'RandomSwapStrategy'

    def choose_index(self):
        return randint(0, self.n - 1), randint(0, self.n - 1)

    def new_energy(self, points, energy, a, b):
        result = energy
        if a == b:
            return result
        if a > b:
            a, b = b, a

        if a != 0:
            result += (single_energy(points[b], points[a-1]) - single_energy(points[a], points[a-1]))

        if not abs(a-b) == 1:
            result += (single_energy(points[b], points[a+1]) - single_energy(points[a], points[a+1]))
            result += (single_energy(points[a], points[b-1]) - single_energy(points[b], points[b-1]))

        if not b == (self.n - 1):
            result += (single_energy(points[a], points[b+1]) - single_energy(points[b], points[b+1]))

        return result

    def swap(self, points, a, b):
        points[a], points[b] = copy(points[b]), copy(points[a])
        return points


class ConsecutiveSwapStrategy(RandomSwapStrategy):
    def __init__(self, n):
        super().__init__(n)
        self.name = 'ConsecutiveSwapStrategy'

    def choose_index(self):
        a = randint(0, self.n - 2)
        return a, a + 1


class InsertStrategy:
    def __init__(self, n):
        self.n = n
        self.name = 'InsertStrategy'

    def choose_index(self):
        return randint(0, self.n - 1), randint(0, self.n - 2)

    def new_energy(self, points, energy, a, b):
        new_state = insert(delete(points, a, 0), b, points[a], 0)
        return state_energy(new_state)

    def swap(self, points, a, b):
        elem = points[a]
        new_state = insert(delete(points, a, 0), b, elem, 0)
        copyto(points, new_state)


def uniform_generator(n, size):
    return array([[random() * size, random() * size] for i in range(n)])


def normal_generator(n, size, group=4):
    signs = [1.0, 0.0, -1.0]
    if group == 4:
        r = 1
    elif group == 9:
        r = 2
    else:
        raise ValueError()
    return array([[normalvariate(size*signs[randint(0, r)], 2),
                   normalvariate(size*signs[randint(0, r)], 2)] for i in range(n)])


def t_factory(amp, center, width):
    def temperature(x):
        return amp * (1 / (1 + exp((x - center) / width)))

    return temperature


def state_energy(points):
    n, m = points.shape
    return fsum([single_energy(points[i], points[i + 1]) for i in range(n - 1)])


def single_energy(p1, p2):
    return linalg.norm(p1 - p2)


def acceptance(old_energy, new_energy, temperature):
    return 1.0 if old_energy >= new_energy else exp((old_energy - new_energy) / temperature)


def anneal(points, iterations, strategy, T):
    n, m = points.shape
    current_energy = state_energy(points)
    print('Start energy', current_energy)

    energies = zeros(iterations)
    temperatures = zeros(iterations)
    frames = array([[[0, 0] for j in range(n)] for i in range(iterations // (iterations // 20))])

    for i in range(iterations):
        current_temperature = T(i)
        a, b = strategy.choose_index()
        next_energy = strategy.new_energy(points, current_energy, a, b)
        if acceptance(current_energy, next_energy, current_temperature) > random():
            current_energy = next_energy
            strategy.swap(points, a, b)
        energies[i] = current_energy
        temperatures[i] = current_temperature
        if i % (iterations // 20) == 0:
            frames[i // (iterations // 20)] = points
    print('Final energy', current_energy)
    return points, energies, temperatures, frames


def compare_strategies(points, iterations, T):
    n, m = points.shape
    strategies = [RandomSwapStrategy(n), ConsecutiveSwapStrategy(n), InsertStrategy(n)]
    styles = ['bv-', 'go-', 'r^-']
    points_figure, axes_p = plt.subplots(3, sharex=True, sharey=True, figsize=(3, 9))
    energies_figure, axes_e = plt.subplots(3, sharex=True, sharey=True, figsize=(4, 8))
    for i, s in enumerate(strategies):
        pt, energies, temperatures, frames = anneal(copy(points), iterations, s, T)
        plot_configuration(axes_p[i], pt, s.name, styles[i])
        plot_data(axes_e[i], energies, s.name, styles[i][0])

    save_figure(points_figure, 'Final_configurations')
    save_figure(energies_figure, 'Final energy')


def main():
    seed()
    n = 40
    points = normal_generator(n, 50)
    save_configuration(points, 'Init_configuration')
    T = t_factory(500, 0, 2000)
    #compare_strategies(points, 50000, T)
    p, energies, temperatures, frames = anneal(points, 20000, InsertStrategy(n), T)
    display_results(energies, frames, p, temperatures)


def save_configuration(points, title):
    f = plt.figure()
    plot_configuration(f.gca(), points, title, 'ro-')
    save_figure(f, title)


def display_results(energies, frames, p, temperatures):
    save_configuration(p, 'Final_configuration')

    data_figure, axes_e = plt.subplots(2)
    plot_data(axes_e[0], energies, 'energy', 'b')
    plot_data(axes_e[1], temperatures, 'temperature', 'r')
    save_figure(data_figure, 'data')

    visualization(frames)


def save_figure(fig, title):
    fig.savefig(title + '.png', dpi=200)
    plt.close(fig)


def plot_configuration(axis, points, title, style='ro-'):
    axis.set_title(title)
    axis.plot(points[:, 0], points[:, 1], style)


def plot_data(axis, data, title, style='b'):
    axis.set_title(title)
    axis.plot(data, style)


def visualization(frames):
    def init():
        line.set_data([], [])
        return line,

    def animate(i):
        x = frames[i, :, 0]
        y = frames[i, :, 1]
        line.set_data(x, y)
        return line,

    k, n, m = frames.shape
    fig = plt.figure()
    ax = plt.axes(xlim=(min(frames[0, :, 0]) - 5, max(frames[0, :, 0]) + 5),
                  ylim=(min(frames[0, :, 1]) - 5, max(frames[0, :, 1]) + 5))
    line, = ax.plot([], [], lw=2)
    animation.FuncAnimation(fig, animate, init_func=init, frames=k, interval=500, blit=True, repeat_delay=1000)
    plt.show()


if __name__ == "__main__":
    main()
