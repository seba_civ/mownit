from random import seed, randrange
import matplotlib.pyplot as plt
from numpy import array, where, zeros, exp, random


class Val:
    def __init__(self, n):
        self.val = n

    def __repr__(self):
        return str(self.val)

    def __eq__(self, other):
        return self.val == other.val


def t_factory(amp, center, width):
    def temperature(x):
        return amp * (1 / (1 + exp((x - center) / width)))

    return temperature


def square(x, y):
    return y // 3 + 3 * (x // 3)


def global_energy(rows, cols):
    return sum(single_energy(cols, rows, x, y) for x in range(9) for y in range(9)) // 2


def single_energy(cols, rows, x, y):
    return len(where(rows[x] == rows[x][y])[0]) - 1 + len(where(cols[y] == rows[x][y])[0]) - 1


def energy_diff(cols, rows, x1, y1, x2, y2):
    old_energy = single_energy(cols, rows, x1, y1) + single_energy(cols, rows, x2, y2)
    rows[x1][y1].val, rows[x2][y2].val = rows[x2][y2].val, rows[x1][y1].val  # swap
    return single_energy(cols, rows, x1, y1) + single_energy(cols, rows, x2, y2) - old_energy


def acceptance(old_energy, new_energy, temperature):
    return 1.0 if old_energy >= new_energy else exp((old_energy - new_energy) / temperature)


def initial_fill(squares):
    for i in range(9):
        possible = [j for j in range(1, 10) if Val(j) not in squares[i]]
        vals = [j for j in squares[i] if j.val == 0]
        for v, w in zip(possible, vals):
            w.val = v


def get_x(s_num, s_ind):
    return s_ind // 3 + 3 * (s_num // 3)


def get_y(s_num, s_ind):
    return s_ind % 3 + 3 * (s_num % 3)


def init_board(m):
    rows = array([[Val(i) for i in row] for row in m])
    columns = array([[row[i] for row in rows] for i in range(9)])
    squares = array([[rows[get_x(i, j)][get_y(i, j)] for j in range(9)] for i in range(9)])
    nonfixed_squares = array([[j for j in range(9) if rows[get_x(i, j)][get_y(i, j)].val == 0] for i in range(9)])
    initial_fill(squares)
    return columns, rows, nonfixed_squares


def choose_fields(squares):
    square_id = randrange(9)  # square
    a = randrange(len(squares[square_id]))  # 1st pos
    c = randrange(len(squares[square_id]))  # 2nd pos
    # translate to normal coordinates
    x1, y1 = get_x(square_id, squares[square_id][a]), get_y(square_id, squares[square_id][a])
    x2, y2 = get_x(square_id, squares[square_id][c]), get_y(square_id, squares[square_id][c])
    return x1, x2, y1, y2


def anneal(columns, rows, squares, iterations, T):
    energy = global_energy(rows, columns)
    energies = zeros(iterations)
    temperatures = zeros(iterations)
    for i in range(iterations):
        t = T(i)
        x1, x2, y1, y2 = choose_fields(squares)
        diff = energy_diff(columns, rows, x1, y1, x2, y2)
        if acceptance(energy, energy + diff, t) > random.uniform():
            energy += diff
        else:
            rows[x1][y1].val, rows[x2][y2].val = rows[x2][y2].val, rows[x1][y1].val  # reverse change
        energies[i] = energy
        temperatures[i] = t
        if energy == 0:
            break

    print('End energy', energy)
    return energy, energies, temperatures


def main():
    seed()
    m = read_file('sudoku2.txt')
    print('Init board\n', m)
    columns, rows, squares = init_board(m)

    print("init energy: ", global_energy(rows, columns))
    T = t_factory(40, 0, 1600)
    iterations = 16000
    restarts = 30
    for i in range(restarts):
        print('restart nr', i)
        e, ener, temp = anneal(columns, rows, squares, iterations, T)
        if e == 0:
            print('Solution found!\n', rows)
            save_data(ener, temp)
            break
        if i == restarts-1:
            print('Solution not found!\n', rows)
            save_data(ener, temp)


def read_file(name):
    with open(name, 'r') as file:
        m = [[int(c) if c != 'x' else 0 for c in line.split()] for line in file]
    return array(m)


def save_data(energies, temperatures):
    data_figure, axes_e = plt.subplots(2)
    plot_data(axes_e[0], energies, 'energy', 'b')
    plot_data(axes_e[1], temperatures, 'temperature', 'r')
    data_figure.savefig('data.png', dpi=200)
    plt.close(data_figure)


def plot_data(axis, data, title, style='b'):
    axis.set_title(title)
    axis.plot(data, style)


if __name__ == "__main__":
    main()
