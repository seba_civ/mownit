import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Sebastian on 2016-05-12.
 *
 * Correlations calculation without SVD
 */

class Correlations implements Function<Map<Integer, Double>, List<Double>> {
    private final BagOfWords bagOfWords;

    Correlations(BagOfWords bagOfWords) {
        this.bagOfWords = bagOfWords;
    }

    @Override
    public List<Double> apply(Map<Integer, Double> normalizedQueryVector) {
        return bagOfWords.getFileResult()
                .parallelStream()
                .map(m -> normalizedQueryVector.entrySet()
                        .stream()
                        .map(e -> e.getValue() * m.getOrDefault(e.getKey(), 0.0))
                        .reduce(0.0, (sum, e) -> sum + e))
                .collect(Collectors.toList());
    }
}
