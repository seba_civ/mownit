import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.lang.Math;

/**
 * Created by Sebastian on 2016-05-08.
 *
 * Analyzes user query
 */

class QueryAnalyzer {
    private final BagOfWords bagOfWords;
    private final Function<Map<Integer, Double>, List<Double>> correlations;

    QueryAnalyzer(BagOfWords bagOfWords, Function<Map<Integer, Double>, List<Double>> correlations) {
        this.bagOfWords = bagOfWords;
        this.correlations = correlations;
    }

    public List<String> analyze(String query, int top) {
        Map<Integer, Double> normalizedQueryVector = createQueryVector(query);
        List<Double> correlationList = correlations.apply(normalizedQueryVector);
        List<Pair> pairList = IntStream
                .range(0, correlationList.size())
                .mapToObj(i -> new Pair(bagOfWords.getFileNames().get(i), correlationList.get(i)))
                .collect(Collectors.toList());
        pairList.sort((p1, p2) -> p2.getValue().compareTo(p1.getValue()));

        List<String> searchResults = pairList.stream().
                filter(p -> p.getValue() > 0.05)
                .map(Pair::getFileName)
                .collect(Collectors.toList());
        return searchResults.subList(0, Math.min(searchResults.size(), top));
    }

    private Map<Integer, Double> createQueryVector(String query) {
        Map<String, Integer> queryVector = new FileAnalyzer().apply(new StringReader(query));
        return BagOfWordsFinder.normalize(queryVector.entrySet()
                .stream()
                .filter(e -> bagOfWords.getIndexMap().containsKey(e.getKey()))
                .collect(Collectors.toMap(e -> bagOfWords.getIndexMap().get(e.getKey()),
                        e -> e.getValue() * bagOfWords.getImportanceMap().get(bagOfWords.getIndexMap().get(e.getKey())))));
    }

    private static class Pair {
        String fileName;
        Double value;

        public Pair(String fileName, double value) {
            this.fileName = fileName;
            this.value = value;
        }

        public String getFileName() {
            return fileName;
        }

        public Double getValue() {
            return value;
        }

        @Override
        public String toString() {
            return fileName + " " + value;
        }
    }
}
