import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Sebastian on 2016-05-12.
 *
 * Correlations calculation with SVD
 */

class SVDCorrelations implements Function<Map<Integer, Double>, List<Double>> {
    private final SVDBagsOFWords svd;

    SVDCorrelations(SVDBagsOFWords svd) {
        this.svd = svd;
    }

    @Override
    public List<Double> apply(Map<Integer, Double> normalizedQueryVector) {
        return IntStream.range(0, svd.getNorms().size()).parallel()
                .mapToObj(i -> normalizedQueryVector.entrySet()
                        .stream()
                        .map(e -> e.getValue() * getElement(e.getKey(), i) / svd.getNorms().get(i))
                        .reduce(0.0, (sum, e) -> sum + e))
                .collect(Collectors.toList());
    }

    private double getElement(int row, int col) {
        return IntStream.range(0, svd.getK())
                .mapToDouble(i -> svd.getU().get(i).get(row) * svd.getV().get(i).get(col))
                .sum();
    }
}
