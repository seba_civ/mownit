import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2016-05-12.
 */

class SVDBagsOFWords {
    private final List<Double> norms;
    private final List<Map<Integer, Double>> u;
    private final List<Map<Integer, Double>> v;
    private final int k; // SVD parameter

    SVDBagsOFWords(List<Double> norms, List<Map<Integer, Double>> u, List<Map<Integer, Double>> v, int k) {
        this.norms = norms;
        this.u = u;
        this.v = v;
        this.k = k;
    }

    public List<Double> getNorms() {
        return norms;
    }

    public List<Map<Integer, Double>> getU() {
        return u;
    }

    public List<Map<Integer, Double>> getV() {
        return v;
    }

    public int getK() {
        return k;
    }
}
