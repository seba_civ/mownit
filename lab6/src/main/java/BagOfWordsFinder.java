import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Sebastian on 5/1/16.
 */

class BagOfWordsFinder {
    private final Map<String, Integer> indexMap = new HashMap<>(); // word -> word_id
    private final Map<String, Integer> bagOfWords = new HashMap<>(); // word -> number of occurrences
    private final String rootDirectory;

    BagOfWordsFinder(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    BagOfWords findBagOfWords(FileAnalyzer analyzer, boolean idf) {
        List<File> files = FileUtils.getFileList(rootDirectory);
        List<String> fileNames = mapFileNames(files);
        List<Map<String, Integer>> fileBags = constructFileVectors(analyzer, files);
        System.out.println("Number of files: " + fileNames.size());

        fileBags.forEach(this::bag);

        Map<Integer, Double> importanceMap = constructImportanceMap(idf, fileBags.size());

        System.out.println("Number of words: " + importanceMap.size());

        // creates final normalized document bag of words
        List<Map<Integer, Double>> fileResult = fileBags.parallelStream()
                .map(f -> f.entrySet().stream()
                        .collect(Collectors.toMap(e -> indexMap.get(e.getKey()),
                                e -> e.getValue() * importanceMap.get(indexMap.get(e.getKey())))))
                .map(BagOfWordsFinder::normalize)
                .collect(Collectors.toList());

        try {
            saveData(fileResult, importanceMap, fileNames);
        } catch (IOException e) {
            System.out.println("Error saving data to JSON");
            e.printStackTrace();
        }
        return new BagOfWords(indexMap, importanceMap, fileResult, fileNames);
    }

    /**
     * creates list of file names
     */
    private List<String> mapFileNames(List<File> files) {
        return files.stream()
                .filter(file -> FileUtils.getExtension(file).equals("txt"))
                .map(File::getName)
                .collect(Collectors.toList());
    }

    /**
     * @param vector file bag of words vector
     * @return normalized document bag of words
     */
    static Map<Integer, Double> normalize(Map<Integer, Double> vector) {
        Double norm = Math.sqrt(vector.keySet().stream()
                .map(vector::get)
                .reduce(0.0, (sum, m) -> sum + m*m));
        return vector.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue() / norm));
    }

    /**
     * calculate for each word in bagOfWords IDF or
     * sets it's importance to 1.0 if no IDF calculation is desired
     *
     * @param idf whether perform IDF calculation
     * @param fileBagsSize size of list of document bag of words
     * @return importance map
     */
    private Map<Integer, Double> constructImportanceMap(boolean idf, int fileBagsSize) {
        Function<String, Double> mapper;
        if (idf) {
            mapper = e -> Math.log(fileBagsSize / (double) bagOfWords.get(e));
        } else {
            mapper = e -> 1.0;
        }
        return bagOfWords.keySet()
                .parallelStream()
                .collect(Collectors.toMap(indexMap::get, mapper));
    }

    /**
     * for each .txt file creates mapping: word to it's number of occurrences in the file
     *
     * @param analyzer file analyzer
     * @param files list of files to check
     * @return list of mappings
     */
    private List<Map<String, Integer>> constructFileVectors(FileAnalyzer analyzer, List<File> files) {
        return files.parallelStream()
                    .filter(file -> FileUtils.getExtension(file).equals("txt"))
                    .map(FileUtils::getReader)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(analyzer)
                    .collect(Collectors.toList());
    }

    /**
     * analyzes file vector adding new words to bag of words and counting occurrences
     *
     * @param vector document bag of words
     */
    private void bag(Map<String, Integer> vector) {
        for (String word : vector.keySet()) {
            Integer count = bagOfWords.getOrDefault(word, 0);
            bagOfWords.put(word, count+1);
            indexMap.putIfAbsent(word, indexMap.size());
        }
    }

    /**
     * Saves calculated data to JSON
     */
    private void saveData(List<Map<Integer, Double>> fileResult,
                          Map<Integer, Double> importanceMap, List<String> fileNames) throws IOException {
        JSONObject map = new JSONObject(indexMap);
        try (FileWriter file = new FileWriter(Paths.get("bag.json").toString())) {
            map.write(file);
        }
        JSONObject importance = new JSONObject(importanceMap);
        try (FileWriter file = new FileWriter(Paths.get("importance.json").toString())) {
            importance.write(file);
        }
        JSONArray array = new JSONArray(fileResult);
        try (FileWriter file = new FileWriter(Paths.get("files.json").toString())) {
            array.write(file);
        }
        JSONArray names = new JSONArray(fileNames);
        try (FileWriter file = new FileWriter(Paths.get("file_names.json").toString())) {
            names.write(file);
        }
    }
}
