import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.AttributeFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by Sebastian on 5/1/16.
 *
 * Stems file, creates mapping: word to it's number of occurrences
 * Uses default Lucene library parameters
 */

final class FileAnalyzer implements Function<Reader, Map<String, Integer>> {

    @Override
    public Map<String, Integer> apply(Reader reader) {
        Map<String, Integer> solution = new HashMap<>();
        TokenFilter tokenFilter = getTokenFilter(reader);
        CharTermAttribute value = tokenFilter.addAttribute(CharTermAttribute.class);
        try {
            tokenFilter.reset();
            while (tokenFilter.incrementToken()) {
                if (isPureAscii(value.toString())) {
                    Integer count = solution.getOrDefault(value.toString(), 0);
                    solution.put(value.toString(), count+1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return solution;
    }

    private TokenFilter getTokenFilter(Reader reader) {
        Tokenizer lowerToken = new LowerCaseTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY);
        lowerToken.setReader(reader);
        TokenFilter stopFilter = new StopFilter(lowerToken, EnglishAnalyzer.getDefaultStopSet());
        TokenFilter stemFilter = new KStemFilter(stopFilter);
        return new LengthFilter(stemFilter, 3, 30);
    }


    /**
     * Dummy way to filter out chinese words etc.
     *
     * @param token word
     * @return if word contains only ASCII characters
     */
    private boolean isPureAscii(String token) {
        byte bytearray []  = token.getBytes();
        CharsetDecoder d = Charset.forName("US-ASCII").newDecoder();
        try {
            CharBuffer r = d.decode(ByteBuffer.wrap(bytearray));
            r.toString();
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }
}
