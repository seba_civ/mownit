import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2016-05-08.
 */


class BagOfWords {
    /**
     * indexMap maps String word to integer id
     * importanceMap maps word_id to IDF value for this word
     * fileResult is list of file bag of words vectors
     * fileNames is list of file names
     */
    private final Map<String, Integer> indexMap; // word -> word_id
    private final Map<Integer, Double> importanceMap; // word_id -> idf
    private final List<Map<Integer, Double>> fileResult; // list of word_id -> value
    private final List<String> fileNames; // list of file names

    BagOfWords(Map<String, Integer> indexMap, Map<Integer, Double> importanceMap, List<Map<Integer, Double>> fileResult, List<String> fileNames) {
        this.indexMap = indexMap;
        this.importanceMap = importanceMap;
        this.fileResult = fileResult;
        this.fileNames = fileNames;
    }

    Map<String, Integer> getIndexMap() {
        return indexMap;
    }

    Map<Integer, Double> getImportanceMap() {
        return importanceMap;
    }

    List<Map<Integer, Double>> getFileResult() {
        return fileResult;
    }

    List<String> getFileNames() {
        return fileNames;
    }
}
