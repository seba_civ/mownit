import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Sebastian on 2016-05-07.
 * Application
 */

public class Application {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> fileNames = FileUtils.searchForJSONFiles(System.getProperty("user.dir"));
        QueryAnalyzer qa = getQueryAnalyzer(fileNames);
        do {
            System.out.println("Type query");
            String query = reader.readLine();
            qa.analyze(query, 10).forEach(System.out::println);
            System.out.println("next query? (y/n)");
        } while (reader.readLine().equals("y"));
        System.out.println("Bye!");
    }

    private static QueryAnalyzer getQueryAnalyzer(List<String> fileNames) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        if (fileNames.contains("bag.json") && fileNames.contains("importance.json")
                && fileNames.contains("svd_result_u.json") && fileNames.contains("svd_result_v.json")
                && fileNames.contains("norms.json") && fileNames.contains("files.json")
                && fileNames.contains("file_names.json")) {
            System.out.println("JSON data files detected. Recalculate? (y/n)");
            String tmp = reader.readLine();
            if (tmp.equals("y")) {
                return recalculateData();
            } else {
                return parseJSONData();
            }
        } else {
            return recalculateData();
        }
    }

    private static QueryAnalyzer recalculateData() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Configure: [is_idf] [is_svd] [k]\n[true/false] [true/false] [number]");
        String [] arg = reader.readLine().split(" ");
        boolean is_idf = Boolean.parseBoolean(arg[0]);
        boolean is_svd = Boolean.parseBoolean(arg[1]);
        String root = Paths.get("python_part", "data").toString();
        BagOfWords bag = new BagOfWordsFinder(root).findBagOfWords(new FileAnalyzer(), is_idf);
        if (is_svd) {
            int k = Integer.parseInt(arg[2]);
            try {
                if (callPython(k)) {
                    return new QueryAnalyzer(bag, new SVDCorrelations(
                            getSVDResult(k, bag.getFileNames().size(), bag.getImportanceMap().size())));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return new QueryAnalyzer(bag, new Correlations(bag));
    }

    private static boolean callPython(int k) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("python", Paths.get("python_part", "svd.py").toString(),""+k);
        Process p = pb.start();
        BufferedReader errorOutput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        int exitCode = p.waitFor();
        if (exitCode != 0) {
            System.out.println("SVD calculation failed! Skipping...");
            String line;
            while ((line = errorOutput.readLine()) != null) {
                System.out.println("Python Output: " + line);
            }
            return false;
        }
        return true;
    }

    private static SVDBagsOFWords getSVDResult(int k, int fileNum, int wordNum) throws IOException {
        List<Map<Integer, Double>> result_u = parseJSON("svd_result_u.json",
                new TypeToken<List<Map<Integer, Double>>>(){}.getType());

        List<Map<Integer, Double>> result_v = parseJSON("svd_result_v.json",
                new TypeToken<List<Map<Integer, Double>>>(){}.getType());

        List<Double> norms = IntStream.range(0, fileNum)
                .parallel()
                .mapToObj(col -> Math.sqrt(IntStream.range(0, wordNum)
                        .mapToDouble(row -> elem(row, col, k, result_u, result_v))
                        .sum()))
                .collect(Collectors.toList());

        JSONArray array = new JSONArray(norms);
        try (FileWriter file = new FileWriter(Paths.get("norms.json").toString())) {
            array.write(file);
        }
        return new SVDBagsOFWords(norms, result_u, result_v, k);
    }

    private static double elem(int row, int col, int k, List<Map<Integer, Double>> result_u, List<Map<Integer, Double>> result_v) {
        double val = IntStream.range(0, k)
                .mapToDouble(i -> result_u.get(i).get(row) * result_v.get(i).get(col))
                .sum();
        return val * val;
    }

    private static QueryAnalyzer parseJSONData() throws IOException {
        try {
            List<String> fileNames = parseJSON("file_names.json", new TypeToken<List<String>>(){}.getType());
            Map<String, Integer> indexMap = parseJSON("bag.json", new TypeToken<Map<String, Integer>>(){}.getType());
            Map<Integer, Double> importanceMap = parseJSON("importance.json", new TypeToken<Map<Integer, Double>>(){}.getType());
            List<Map<Integer, Double>> fileResult = parseJSON("files.json", new TypeToken<List<Map<Integer, Double>>>(){}.getType());
            BagOfWords bag = new BagOfWords(indexMap, importanceMap, fileResult, fileNames);

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Use SVD? (y/n)");
            String ans = reader.readLine();
            if (ans.equals("y")) {
                List<Double> norms =  parseJSON("norms.json", new TypeToken<List<Double>>(){}.getType());
                List<Map<Integer, Double>> result_u = parseJSON("svd_result_u.json", new TypeToken<List<Map<Integer, Double>>>(){}.getType());
                List<Map<Integer, Double>> result_v = parseJSON("svd_result_v.json", new TypeToken<List<Map<Integer, Double>>>(){}.getType());
                SVDBagsOFWords svdBag = new SVDBagsOFWords(norms, result_u, result_v, result_u.size());
                return new QueryAnalyzer(bag, new SVDCorrelations(svdBag));
            } else {
                return new QueryAnalyzer(bag, new Correlations(bag));
            }
        } catch (IOException e) {
            System.out.println("Error during reading json data. Recalculating...");
            e.printStackTrace();
            return recalculateData();
        }
    }

    private static <T> T parseJSON(String file, Type type) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File(Paths.get(file).toString())));
        return new Gson().fromJson(br, type);
    }
}
