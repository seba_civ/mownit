import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Sebastian on 2016-05-07.
 *
 * Contains methods helping in files operations
 */

class FileUtils {
    static List<File> getFileList(String directory) {
        File [] fileList = new File(directory).listFiles();
        if (fileList == null) {
            System.out.println("Empty directory");
            return new ArrayList<>();
        }
        return Arrays.asList(fileList);
    }

    static String getExtension(File file) {
        return file.getName().substring(file.getName().lastIndexOf('.') + 1);
    }

    static List<String> searchForJSONFiles(String dir) {
        return FileUtils.getFileList(dir)
                .stream()
                .filter(f -> FileUtils.getExtension(f).equals("json"))
                .map(File::getName)
                .collect(Collectors.toList());
    }

    static Optional<Reader> getReader(File file) {
        Optional<Reader> result = Optional.empty();
        try {
            Reader reader = new InputStreamReader(new FileInputStream(file));
            result = Optional.of(reader);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }
}
