import wikipedia
import re
from datetime import timedelta


def main():
    wikipedia.set_lang('en')
    wikipedia.set_rate_limiting(True, timedelta(milliseconds=5))
    i = 0
    article_list = wikipedia.random(200000)
    for a in article_list:
        try:
            page = wikipedia.page(a)
            content = page.content
            title = 'data/' + re.sub(r'\W+', '', page.title) + '.txt'
            l = len(content)
            if l > 1000:
                print(l, title)
                with open(title, 'w', encoding='utf-8') as f:
                    f.write(content)
                    i += 1
        except Exception as e:
            print(e)


if __name__ == "__main__":
    main()
