Camilo Bonilla Paz (born 30 September 1971) is a retired Honduran football player who made his name with the national team in the 1990s.


== Club career ==
Bonilla started his career at Real España for whom he netted 17 league goals in 176 matches. He also had spell with Peruvian club Sipesa, where he played alongside compatriot Mario Peri.
He played the final seasons of his career in Guatemala and El Salvador, joining Atlético Balboa for the 2002 Clausura. In summer 2005 he went in pre-season training with Salvadoran side Isidro Metapán from Dragón.


== International career ==
Bonilla made his debut for Honduras in a May 1991 UNCAF Nations Cup match against Panama and has earned a total of 18 caps, scoring no goals. He has represented his country at the 1991, and 1997 UNCAF Nations Cups, as well as at the 1991, and 1998 CONCACAF Gold Cups. He also was a non-playing squad member at the 1996 CONCACAF Gold Cup.
His final international was a February 1998 CONCACAF Gold Cup match against Mexico.


== References ==


== External links ==
Camilo Bonilla at National-Football-Teams.com