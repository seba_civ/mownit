The Cuban Institute of Cinematographic Art and Industry (ICAIC) was established by the Cuban government in March 1959, after the Cuban Revolution. Its major founders are Julio García Espinosa, Alfredo Guevara and Santiago Álvarez
The ICAIC is an organization of a film industry to produce, distribute and exhibit films and related work following the Cuban Revolution. The aim of the ICAIC is to use film as a powerful mass communication medium to mobilize and educate people, improve the quality level of Cuba films with appreciation among the masses and reach a wide public. Through educating the new generation of young directors, one of the responsibilities of the ICAIC is to transform Cuba from a country of cinematic consumption to a production.


== History of the ICAIC ==


=== The creation of the ICAIC ===
Before the Revolution, Cubans did not receive free education. Only wealthy people could get educated. Therefore, prior to 1959, the illiteracy rate was very high. The government wanted to find a way to reach public. It is hard for the government to produce books to reach public because people are required to have basic knowledge on understanding the wordings. So, cinema and other mass media were considered the best way to reach the public. Therefore, the Cuban government put more focus on the development of mass media.
Hollywood is the dominant film market all over the world, including Cuba. Also, Cuban film is described as “Imperfect Cinema”. This is a theme that shows the authentic livelihoods and the Third World struggles to the public. The style is to educate people with the correct human beings, train audiences to become more active on interpreting the meanings and enhance their critical thinking skills.
Due to these backgrounds, the Cuban government established a national film institute called Cuban Institute of Cinematographic Art and Industry (ICAIC). The ICAIC is the first cultural act of the revolutionary government. The aims are to educate people and improve the quality level of Cuban films. Also, the ICAIC hopes to transform Cuba from a country of consumption to production. ICAIC made two clarifications: first, “cinema is an art”, and second, cinema constitutes “an instrument of opinion and formation of individual and collective consciousness”. Due to these clarifications, it led numerous underground filmmakers joining together.


=== In the 1960s ===
Due to some institutions establishing before ICAIC, they organized workshops to train technicians. These institutions include Film Department at Havana University, Sociedad Cultural “Nuestro Tiempo” (Cultural Society “Our Times”) and Catholic Center of Cinematographic Orientation (CCOC). Therefore, Cuba's film industry did not lack of skilled technicians, but it lacked experienced directors. Tomás Gutiérrez Alea and Julio García Espinosa were the only two main directors in Cuba in the 1960s, so the international program for educating young directors became indispensable. ICAIC sent trainee directors to work as assistants in France, Italy, East Germany, and elsewhere. Working as an assistant can help understand the whole process of filmmaking. And the basic filmmaking skill is very useful when they become directors in the future.
In 1960, Cine Cubano, the ICAIC’s film journal, is published. It includes films, interviews, filmographies, and movie plots. They put education into powerful words. Cine Cubano invites all people to submit their writings and share their opinions to the films, as well as teach their critical thinking. Beyond subsidies by the government, ICAIC receives a lot of sponsorships from Cine Cubano. Thanks to these sponsorships, the ICAIC have money on financing films, training filmmakers and educating audiences.
Due to the U.S. monopolization, ICAIC only produced about eight or nine features in a year  This was not healthy to the Cuban film industry. After that, the U.S. trade embargo made the Hollywood films become less accessible to Cuba. To Cuban cinema, it was about time to explore something new from other countries and not rely on the U.S. production. In order to keep Cuba’s theaters going, the ICAIC imported a large number of movies from France, Italy, Japan and other countries, which creates a great diversity. For audiences, it is a great chance to know more different cultures from movies, and then they can enhance the culture exchange and stimulate a new way of thinking.


=== In the 1970s ===
Although the Revolution has improved the Cubans’ livelihoods, Cuba was still in a circle of underdevelopment. The government focused on the agricultural development, especially in sugar production, in 1969, which called the Year of the Decisive Effort. The aim was to produce a ten-million-ton sugar harvest in 1970. Due to the sugar production project, the government tightened the control on subsidizing the ICAIC. Therefore, Cuba cinema entered a “gray period” in the 1970s. ICAIC suffered many problems such as limited financial and technical resources. The number of audiences who are willing to go to the theaters decreased due to the growth of the television in the 1970s. Comparing to the very productive period of the 1960s, an overall decline in quality exists in the 1970s. Luckily, those young directors went back to Cuba and contributed to the Cuban film industry after training. Although the films were not extraordinary, it helped maintain the Cuban film industry. In 1979, ICAIC establishes International Festival of the New Latin American in Havana. This festival is to celebrate the Latin American cinema by gather outstanding directors, producers and people who contributes to Latin American’s film industry. Moreover, it is to bring world attention to cinema from Cuba and the rest of Latin America. It shows to the other countries such as Europe, North America that Latin America film industry is able to stand on their own.


=== In the 1980s ===
Cuba in the 1980s had not fully escaped the economic legacy of its past. Despite of the economic influence, the passion of the ICAIC on education through cinema did not decline. ICAIC started working on international coproduction with Spain and the Soviet Union. During the 1980s, ICAIC has changed the organization structure and formed a separate office only for exhibition and associated activities for educating people. Also, the Escuela International de Cine y Televisión (International School of Film and Television) got founded by the ICAIC in 1986. Active directors, editors, producers, sound technicians, and other experts from around the world have been invited to this school to share their experience. EICTV helps the way for new modes of producing and distributing films, which prepares elites for new generation. Those changes point out that the ICAIC is devoted to educate people and draw people’s attention by organizing activities.


=== The Special Period in Times of Peace (1990-1995) ===
The collapse of the Soviet Union put Cuba into a crisis because Cuba relied on the Soviet Union to remain a viable economy. People in Cuba suffered from daily shortages such as electricity blackouts, severe gasoline rationing, huge cuts in public transportation, and bicycles from China. Filmmakers, artists and intellectuals suffered the same consequence as everyone else. ICAIC only completed three features in 1990. At 1991, due to the economic crisis, it also led people to escape the island.
The consequence of the “Special Period” of the 1990s for Cuban cinema is drastic reduction in the resources from film production to distribution. ICAIC understands that during the economic crisis, pursuing international coproduction with commercial partners can ensure the survival. So, the ICAIC did a lot of International coproduction with other countries such as Spain, Germany and Mexico. Fresa y chocolate (Strawberry and Chocolate) is one of the significant movies coproduced with Spain during the Special Period. More than that, it helps maintain Cuban films internationally and expand the Spanish-language film market.


=== Recent years in Cuban cinema ===
Cuban cinema was resolved slowly after the coproduction and local Cuban cinema. As ICAIC has contributed to the Cuban cinema for many years, ICAIC’s current goals principally are to survive and maintain the film industry in Cuba. The ICAIC celebrated its 50th anniversary in 2009. Despite ongoing difficulties, ICAIC continues to exist and develop the film industry. It also shows everyone that ICAIC’s contribution is highly significant and important to Cuban culture.


== See also ==
Cinema of Cuba
List of Cuban films


== References ==


== External links ==
Cubca Cine, ICAIC portal
Instituto Cubano del Arte e Industria Cinematográficos in IMDB
Instituto Cubano del Arte e Industrias Cinematográficos at the Big Cartoon DataBase
Instituto Cubano del Arte e Industria Cinematográficos in Ecured