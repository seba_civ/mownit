Shlomo ha-Levi Alkabetz, also spelt Alqabitz, Alqabes; (Hebrew: שלמה אלקבץ) (c.1500, Salonica– 1580, Safed ) was a rabbi, kabbalist and poet perhaps best known for his composition of the song Lecha Dodi; sources differ as to when he wrote it (1529, 1540 and 1571 have all been suggested).


== Biography ==
Alkabetz studied Torah under Rabbi Yosef Taitatzak. In 1529, he married the daughter of Yitzhak Cohen, a wealthy householder living in Salonica. Alkabetz gave his father-in-law a copy of his newly completed work Manot ha-Levi. He settled in Adrianople, Turkey where he wrote Beit Hashem, Avotot Ahava, Ayelet Ahavim and Brit ha-Levi. This latter work he dedicated to his admirers in Adrianople. His students included Rabbi Shmuel Ozida, author of Midrash Shmuel on Avot, and Rabbi Avraham Galante, author of Yareach Yakar on Zohar. His circle included Moshe Alsheich and Yosef Karo, as well as his famous brother-in-law Moshe Cordovero.


== Move to Safed ==
Following the practice described in the Zohar of reciting biblical passages known as the Tikūn on the night of Shavuot, Rabbi Shlomo and Rabbi Joseph Karo stayed awake all that night reading. During the recitation of the required texts, Rabbi Karo had a mystical experience: The Shekhinah appeared as a maggid, praising the circle and telling them to move to the Land of Israel. When they stayed up again the second night of Shavuot, the Shekhinah was adamant about their moving to the land of Israel. The account was recorded by Alkabetz. He settled in Safed in 1535.


== Thought ==
His works written in Adrianople center on the holiness of the people Israel, the Land of Israel, and the specialness of the mitzvot. Alkabetz accepts the tradition that Esther was married to Mordechai before being taken to the king's palace and becoming queen, and even continued her relationship with Mordechai after taking up her royal post. The view of midrash articulated by Alkabetz and other members of the school of Joseph Taitatsak represents an extension of the view of the authority of the oral law and halachic midrash to aggadic midrash and thus leads to the sanctification and near canonization of aggadic expansions of biblical narrative 


== Works ==
Among his printed works:
Lecha Dodi (1579), a mystical hymn to inaugurate the Shabbat
Manot HaLevi (completed 1529, published 1585) on the Book of Esther
Ayalet Ahavim (completed 1532, published 1552) on Song of Songs
Shoresh Yishai (completed 1552, published 1561) on the Book of Ruth
Brit HaLevi (1563), a kabbalistic commentary on the Passover Haggada
Or Tzadikim, a book of sermons
Among those existing in manuscript are:
Divrei Shlomo, on the section of Scripture known as Writings
Naim Zemirot, on Psalms
Sukkat Shalom, Avotot Ahavah, on the Torah
Pitzei Ohev, on the Book of Job
Apiryon Shlomo, Beit Hashem, Beit Tefilla, interpretations of the prayers
Lechem Shlomo, on the guidelines for the sanctification of meals, according to kabbalah
Mittato shel Shlomo, on the mystical significance of sexual union
Shomer Emunim, on the fundamental principles of faith


== References ==


== Bibliography ==
Joseph Yahalom, "Hebrew mystical poetry and its Turkish background," in Andreas Tietze and Joseph Yahalom, Ottoman Melodies Hebrew Hymns: a 16th century cross-cultural adventure (Budapest: Akadémiai Kiadó, 1995), pp. 9–43.
Bracha Sack, The Secret Teaching of R. Shlomo Halevi Alkabetz (Ph. D., Brandeis University, 1977)


== External Links ==
Rabbi Shlomo Ha-Levi Alkabetz (1500-1580) Find A Grave Memoria
Lecha Dodi with Sephardic last verse YouTube video
Video Lecture on Rabbi Shlomo Alkabets by Dr. Henry Abramson