Sara Ullrika Watkins (born June 8, 1981) is an American singer-songwriter and fiddler. Watkins debuted in 1989 as fiddler and founding member of the progressive bluegrass group Nickel Creek along with her brother Sean and mandolinist Chris Thile. In addition to singing and fiddling, Watkins also plays the ukulele and the guitar, and also played percussion while touring with The Decemberists.
With Nickel Creek, Watkins released five studio albums, one compilation album and seven singles. During the band's seven-year hiatus, Watkins released two solo albums, Sara Watkins and Sun Midnight Sun on Nonesuch Records.


== Biography ==


=== 1989-2007: Nickel Creek ===


=== 2007-Present: Solo career ===
In late 2005, Watkins stated in a PopMatters interview when discussing her first solo-written recorded song, "Anthony", that she "definitely [makes] the effort [to write more], but it's something that doesn't come too easy for me. Nor does it come really easy for the guys, I think they've just been doing it for a very long time."

Watkins signed as a solo artist with Nonesuch Records in fall 2008 and released her self-titled debut solo album on April 7, 2009. Watkins started recording the album in February 2008, and it was jointly recorded in Nashville and Los Angeles. Sara Watkins was produced by bassist John Paul Jones of Led Zeppelin fame, who first performed with Watkins on a Mutual Admiration Society tour in 2004.
Watkins toured the United States as an opening act in 2008. She performed short tours as an opening act for singer-songwriter Tift Merritt along the West Coast in March and April and with Robert Earl Keen around New England in September, as well as doing a 17-date tour with Donavon Frankenreiter in October. On April 14, 2009, she appeared on Late Night with Jimmy Fallon, with Jones on bass, Questlove of The Roots on drums, and her brother Sean Watkins on guitar. She has also performed on A Prairie Home Companion at the Minnesota State Fair and the Fitzgerald Theater and at the Alaska State Fair on August 28, 2011. In 2012, she toured with Jackson Browne as the opening act for his acoustic winter tour.
In May 2012, she released her second solo album, Sun Midnight Sun, again on Nonesuch Records. Produced by former Simon Dawes guitarist Blake Mills, it features guest appearances from Fiona Apple and Jackson Browne.


=== Other projects ===

In 2002, Watkins starred in an advertisement for cell phone provider Cingular Wireless alongside her former fiddle teacher Dennis Caplinger, as well as other prominent bluegrass artists. Los Angeles session musician Gabe Witcher was originally offered the gig, but was asked to back out because of a casting desire for a "female fiddler."
As mentioned above, in between 2000 and 2004, Sara Watkins and the other members of Nickel Creek (Chris Thile and Sean Watkins), John Paul Jones, and Pete Thomas for an E.P. "Mutual Admiration Society Solo Sampler", an album "Mutual Admiration Society" (recorded in 2000) and a brief tour.
The 2007 documentary film Arctic Tale featured a song by Watkins and musician Grant-Lee Phillips, titled "Song of the North (Beneath the Sun)." Watkins lent her vocal talents to be used in the film alongside other performers such as Aimee Mann and Brian Wilson.
In January 2008, Billboard reported a new supergroup octet tentatively named The Scrolls, later named Works Progress Administration (W.P.A.). The octet is composed of Watkins, her brother Sean Watkins (guitar), Glen Phillips (guitar, vocals), Benmont Tench (piano), Luke Bulla (fiddle), Greg Leisz (various), Pete Thomas (drums), and Davey Faragher (bass). The group released their album WPA in September 2009.
She is featured on Needtobreathe's CD The Outsiders on the track "Stones Under Rushing Water."
In late January and early February 2010 Watkins undertook a short tour with Jerry Douglas and Aly Bain in Scotland and England under the "Transatlantic Sessions" banner culminating in a performance in the Royal Festival Hall in London on 6 February 2010. In June 2010, Sara appeared on the Nerdist podcast as a musical guest, performing her best-known solo song, "Long Hot Summer Days" (a John Hartford cover). Later that summer, she participated in the Summer Love Tour with Garrison Keillor in venues across the United States. She guest hosted Keillor's show A Prairie Home Companion on January 15, 2011.
Watkins contributed fiddle, guitar, percussion, and vocals on The Decemberists' 2011 tour.
In September 2011, Watkins indicated via Twitter that she would begin work on a second album in the Fall of 2011. Also in 2011, she (and her brother Sean) began to perform as a musician and singer in the humor/retro-radio podcast "Thrilling Adventure Hour".
Sara played violin and sang on "A Face To Call Home" on John Mayer's album Born and Raised.
During 2012 and 2013, Watkins toured with Jackson Browne, both as an opening act and accompanying him during the performance.
In 2015, Sara toured extensively with Sarah Jarosz and Aoife O'Donovan (of Crooked Still fame). The trio hosted the radio show A Prairie Home Companion on October 10, 2015. Watkins also performed vocals for "June & Johnny" on Jon Foreman's 2015 EP The Wonderlands: Darkness.


=== Personal life ===
On August 16, 2008, Watkins married her boyfriend Todd Cooper, in her parents' backyard in Vista, California.


== Discography ==


=== Solo career ===


==== Studio albums ====


==== Music videos ====


=== With Nickel Creek ===


== References ==


== External links ==
Official website


=== Interviews ===
Interview on Metromix.com