The copyright law of South Africa governs copyright, the right to control the use and distribution of artistic and creative works, in the Republic of South Africa. It is embodied in the Copyright Act, 1978 and its various amendment acts, and administered by the Companies and Intellectual Property Commission in the Department of Trade and Industry.
South Africa is a party to the Berne Convention and the TRIPS Agreement. It has signed, but not ratified, the WIPO Copyright Treaty.


== History ==
Initially, after the creation of the Union of South Africa in 1910, the copyright laws of the four formerly-independent provinces continued unchanged. In 1916, Parliament enacted the Patents, Designs, Trade Marks and Copyright Act, 1916, which repealed the various provincial laws and incorporated the British Imperial Copyright Act 1911 into South African law. In 1928, along with the other British dominions, South Africa became a party to the Berne Convention in its own right.
South Africa having become a republic in 1961, Parliament enacted its own copyright law, separate from that of the United Kingdom, in the Copyright Act, 1965. Nonetheless, this act was largely based on the British Copyright Act 1956. In 1978 it was replaced by the Copyright Act, 1978, which (as amended) remains in force. The 1978 Act draws both from British law and from the text of the Berne Convention. It has been amended several times, most notably in 1992 to make computer programs a distinct class of protected work, and in 1997 to bring it into line with the TRIPS agreement.


== Eligibility for copyright ==
The Copyright Act defines nine classes of work that are eligible for copyright:
literary works - including novels, poems, plays, film scripts, textbooks, articles, encyclopaedias, reports, speeches, etc.
musical works - excluding words sung with the music
artistic works - including paintings, sculptures, drawings, photographs, architectural works, works of craftsmanship, etc.
cinematograph films - in any medium, including film, tape or digital data
sound recordings - in any medium, but excluding film soundtracks
broadcasts - signals transmitted by radio waves and intended for public reception
programme-carrying signals - signals representing audio and/or video and transmitted via satellite
published editions - particular typographical arrangements of literary or musical works
computer programs - instructions, in any medium, that direct the operation of a computer
For a work to be eligible for copyright, it must be original, and it must have been written down or recorded in some way (except for broadcasts and programme-carrying signals, which must have been broadcast or transmitted, respectively). "Originality" requires the work to have been produced by the exercise of skill and effort by the author(s). As in all Berne Convention countries, copyright is automatic and does not require registration.
The Copyright Act automatically protects works created by South Africans or in South Africa. It also permits the Minister of Trade and Industry to extend the same protection to works created in, or by residents of, other countries; such protection has been extended to all Berne Convention countries.


== Copyright term ==
For literary, musical and artistic works, except for photographs, the copyright term in South Africa is fifty years from the end of the year of the author's death, or fifty years from publication if it is first published after the author's death. For photographs, films and computer programs, the term is fifty years from first publication, or fifty years from creation if not published within fifty years. For sound recordings, broadcasts, programme-carrying signals and published editions, it is fifty years from first publication or transmission.
Anonymous works are protected for the shorter of fifty years from first publication or fifty years from the year when it is reasonable to presume the author is dead. For works with multiple authors, the fifty years from death are calculated from the death of the last author to die. Government works are protected for fifty years from first publication.


== References ==


== External links ==
Companies and Intellectual Property Commission