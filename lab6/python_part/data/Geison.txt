Geison (Ancient Greek: γεῖσον – often interchangeable with somewhat broader term cornice) is an architectural term of relevance particularly to ancient Greek and Roman buildings, as well as archaeological publications of the same. The geison is the part of the entablature that projects outward from the top of the frieze in the Doric order and from the top of the frieze course (or sometimes architrave) of the Ionic and Corinthian orders; it forms the outer edge of the roof on the sides of a structure with a sloped roof. The upper edge of the exterior often had a drip edge formed as a hawksbeak molding to shed water; there were also typically elaborate moldings or other decorative elements, sometimes painted. Above the geison ran the sima. The underside of the geison may be referred to as a soffit. The form of a geison (particularly the Hawksbeak molding of the outer edge) is often used as one element of the argument for the chronology of its building.


== Horizontal geison ==

The horizontal geison runs around the full perimeter of a Greek temple, projecting from the top of the entablature to protect it from the elements and as a decorative feature. Horizontal geisa may be found in other ancient structures that are built according to one of the architectural orders. The horizontal sima (with its antefixes and water-spouts) ran above the horizontal geison along the sides of a building, acting as a rain gutter and final decoration.


=== Doric order ===

In the Doric order, the sloped underside of the horizontal geison is decorated with a series of protruding, rectangular mutules aligned with the triglyphs and metopes of the Doric frieze below. Each mutule typically had three rows of six guttae (decorative conical projections) protruding from its underside. The gaps between the mutules are termed viae (roads). The effect of this decoration was to thematically link the entire Doric entablature (architrave, frieze, and geisa) with a repeating pattern of vertically and horizontally aligned architectural elements. Use of the hawksbill molding at the top of the projecting segment is common, as is the undercutting of the lower edge to aid in dispersing rainwater. In order to separate the geison from the frieze visually, there is typically a bed molding aligned with the face of the triglyphs.


=== Ionic and Corinthian orders ===
Horizontal geisa of these orders relied on moldings rather than the mutules of the Doric order for their decoration.


== Raking geison ==

A raking geison ran along the top edge of a pediment, on a temple or other structure such as the aedicula of a scaenae frons (theater stage building). This element was typically less decorative than the horizontal geison, and often of a differing profile from the horizontal geison of the same structure. The difference is particularly marked in the Doric order, where the raking geison lacks the distinctive mutules. The raking sima ran over the raking geison as a decorative finish and, essentially, a rain gutter.


== Notes ==


== References ==
Robertson, D. S. 1943. Handbook of Greek and Roman Architecture 2nd Edition. Cambridge: Cambridge University Press