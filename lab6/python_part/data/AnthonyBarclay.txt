Anthony Barclay is a British actor who has appeared in productions including Judge John Deed, Common as Muck and New Tricks, all for the BBC. He played Anthony Berg in Shaun Severi's film Citizen versus Kane which won the Canal+ Award at Clermont Ferrand International Film Festival and which earned Barclay a Best Actor nomination.
Barclay is the son of Danny Williams, a 1960s balladeer best known for the song Moon River. He has three children with former wife Doon Mackichan. and one child with current wife, Antonia Fiber-Barclay.


== Career highlights ==
He played Young Buddy in Stephen Sondheim's Follies in the West End, and worked again with Sondheim as The Balladeer in Sam Mendes' production of Assassins at London's Donmar Warehouse. Notable roles in theatre include: Aziz in Sasha Wares production of Timberlake Wertenbaker's Credible Witness at The Royal Court, alongside Oscar-winning actress Olympia Dukakis; Feste in Lucy Bailey's production of Twelfth Night at The Royal Exchange, and Japheth in John Caird and Stephen Schwartz's production of Children of Eden in London's West End.
Notable roles on television include: Florian in Carla Lane's Screaming, Sunil in Common As Muck, Karl Harper in Coronation St, Sir Tim Listfield in Judge John Deed and Carlos Alvarez in New Tricks.
Barclay spent 2010-13 working with Steven Berkoff in Berkoff's Biblical Tales at the Hampstead Theatre; in Oedipus: After Sophocles at the Nottingham Playhouse, the Liverpool Playhouse, the Pleasance Grand in Edinburgh and the Memminger Theatre in South Carolina for the Spoleto Festival, USA; and Line-Up/Gas in the Steven Berkoff Season at the Jermyn Street Theatre in London.


== References ==


== External links ==
Anthony Barclay at the Internet Movie Database