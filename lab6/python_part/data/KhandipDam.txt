Khandip Dam (also known as Khandip Aniket, in Hindi खंडीप बांध) is a weir which is built on the Ganbhir River at Khandip Village of Rajasthan, India. It lies in a valley to the south of the Khandip and north of the Katkad, east of Nawajipura and West of Medi and Phulwara.
The town of Khandip is situated close to the dam wall and the villages Katkad, Medi, Nawajipura, Phulwara can be found alongside its banks.


== History ==
Until 2008, the Command Area of Panchana Dam, which is dominated by Meenas, used to get water from the Panchana Dam. Gurjars who live upstream of the Gambhir river and around the Panchana Dam site stopped the water supply to Meena villages. This was a big crisis for command area villages, including Khandip, Pilauda, Rendayel Gurjar and more than 25 other villages.
The government did not help the command area people and the economy started failing in command area villages. This helpless situation forced the command area people to think find water sources for themselves. When country was planning to joining rivers and bigger water projects, the command area was facing drought and water scarcity.
In 2013, Khandip organized a meeting (called a "Panchayat") in the area to discuss the issue. All the people supported the idea of making a water reservoir for the area. This idea led to the proposal to make a dam over Ganbhir River to solve the water issues in the village and surrounding areas.


== Funding ==
Without good economical resources, it was almost impossible to build a big dam where a lot of money would be needed, but the command area people did not lose hope. They organized a Panchayat again and asked every family of the village Khandip to contribute in the project. Every family of the village contributed a minimum of 1000INR to the project. Those who could contribute more added an extra 11,0000-21,000 INR. The work started with this funding only.
Later, Katkad village added 5-6 lakhs INR and other command area village people added more money for the project. The political leaders Ramesh Chand Meena guided this projects and helped with technical human resources and engineering. A Meena leader, Makkhan Lal, contributed 11 lakhs for the project, and Kirodi Lal contributed 2 lakhs INR for this project.


== Use ==
The Khandip Dam supplies irrigation water through a 20 kilometres (12 mi) of farmland on which mustard, wheat, lucerne, fruit and flowers are produced.
The Khandip Dam has become a very popular holiday and weekend resort for the inhabitants of Khandip.


== References ==