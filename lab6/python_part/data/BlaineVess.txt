Blaine Vess is an American internet entrepreneur and television producer who lives in Los Angeles. He is the co-founder of StudyMode and Four Henrys Productions.


== Career ==
A couple of weeks into his freshman year of college, Vess co-founded StudyMode, which owns and operates educational websites including StudyMode.com and Cram.com. In 2009, Vess and producing partner, Aisha Wynn, founded Four Henrys Productions, a company that creates and produces original television shows such as I'm Married to a... on VH1.
After graduating from college, Vess worked at New Line Cinema as an interactive marketing consultant where he helped market films including Snakes on a Plane and Wedding Crashers.


== Philanthropy ==
Vess serves on the board of Liberty in North Korea (LiNK), which provides protection and aid to North Korean refugees hiding in China, and has personally assisted in the rescuing of North Korean refugees in Southeast Asia.
Vess' interest in human rights in North Korea was first sparked by an episode of National Geographic Explorer. He has since traveled to North Korea. In July 2013, Vess screened LiNK's documentary, Danny From North Korea, as part of a fundraiser at his home. Vess' monetary donations to LiNK account for three to five percent of its annual budget.


== References ==