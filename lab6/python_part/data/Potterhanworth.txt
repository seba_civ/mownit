Potterhanworth is a village and civil parish in the North Kesteven district of Lincolnshire, England. It is situated 6 miles (10 km) south-east from Lincoln.
The hamlet of Potterhanworth Booths is part of the Potterhanworth civil parish.In the 2001 Census the population of the whole parish was recorded as 648 in 257 households.
Potterhanworth appears in the Domesday survey as "Haneworde". It is part of the Wapentake of Langoe. By 1334 it was known as Potter Hanworth because of the presence of a large pottery, and the two words did not coalesce until the 20th century.

Nearest settlements are Nocton to the south, Branston to the north-west, and Potterhanworth Booths to the north-east. The village is at the junction of the B1202 the B1178 roads. The Peterborough to Lincoln railway line passes 0.5 miles (0.8 km) to the west. On the B1202 to the east is a former POW camp. The parish includes land and Potterhanworth Fen to the south of the B1190 road to Bardney, to the point where this road meets the River Witham.
From the early 20th century, on Cross Street, there was a post office with village store – now in residential use – which was later relocated to Middle Street. Potterhanworth had a bowls club, and a tennis club with courts, at the village sports' field; the field now contains a Lottery-funded play park. Previously, the village had two public houses: The Chequers and The Black Horse. Only The Chequers, on Cross Street, remains. There is a village hall, a church dedicated to St Andrew, a primary school, and a nine-hole golf course nearby on the road to Potterhanworth Booths. A previous water tower has been converted to a house. The village football team is Potterhanworth FC. Famed Neuro-astrologist Robert Larter spent his childhood in the village.


== References ==


== External links ==
 Media related to Potterhanworth at Wikimedia Commons
C of E primary school
Parish council