The 1967 Stanley Cup Final was a best-of-seven series played between the Montreal Canadiens and the Toronto Maple Leafs. The Maple Leafs would win the series four games to two to win their thirteenth Stanley Cup. As of 2015, this is Toronto's most recent Stanley Cup championship, most recent appearance in the championship final, and with the Chicago Blackhawks ending a 49-year Cup drought with their victory in the 2010 Stanley Cup Finals, is tied for the longest-active championship drought in the NHL with the St. Louis Blues (who have never won since joining the NHL in 1967). The 1967 Stanley Cup Final was also the last Stanley Cup Final in the Original Six Era.


== Paths to the Final ==

This was the last Stanley Cup before the 1967 expansion which meant only three series in total were played in the playoffs. Montreal defeated New York to advance to the finals and Toronto defeated Chicago.


== The series ==
The average age of the Leafs' players was 31, the oldest lineup to win the Cup. Johnny Bower was 42 and Allan Stanley was 41. Dave Keon won the Conn Smythe Trophy.
Montreal won the opener 6–2, soundly trouncing Toronto. For the second game, Terry Sawchuk was replaced with Bower and provided the Leafs with a shutout win 3–0. Bower was in net for game three won 3–2 on Bob Pulford's overtime goal. This game has been described as "one of the most exciting games ever played".
Bower was injured before game four and Sawchuk had to take over. Al Smith was called up from the minors to served as back-up for the fourth and fifth games. The Canadiens defeated the Leafs 6–2 again, this time in Toronto to even the series. Sawchuk would play very well in the next two games, backstopping the Leafs to the Cup. In the sixth game Bower returned to the line-up as back up. Jim Pappin scored his seventh goal of the playoffs and Sawchuk stopped 41 shots helping Toronto win the cup. Pappin had four goals and four assists in the Final series.


=== Game 1 ===


== Toronto Maple Leafs 1967 Stanley Cup champions ==


=== Roster ===


=== Stanley Cup engraving ===
The "K" in Peter Stemkowski name was engraved backwards.
Stafford Smythe name was misspelled C. ST FFORD SMYTHE PRESIDENT missing an "A". Both mistakes were corrected on the Replica Cup created in 1992–93.
Leonard "Red" Kelly won 8 Stanley Cups. He was engraved as Leonard Kelly in 1954, 1955, 1962, 1963, 1964. Kelly was engraved as Red Kelly in 1950, 1952, 1967.
††Johnny Bower was 42 years old when he won his fourth and final Stanley Cup. Bower missed 2 games in the finals with an injury. Al Smith was dressed in his place. Smith's name was left off the Cup, because he only played 1 regular season games, and did not play in the playoffs. Al Smith qualified to be on the Cup, because he was dressed in the finals.
† #19 Kent Douglas (defense), and #24 John Brenneman (winger) were sent to the minors before the trading deadline. They played half regular season games, qualified to win the Cup, but name was left off for playing in minors during the playoffs.
Dr. Hugh Symthe (Team Physician) - Also left (His name is on cup 1942 with Toronto as mascot).


=== Won all 4 Stanley Cups in 6 Years with Toronto 1962, 1963, 1964, 1967 ===
George Armstrong, Bob Baun, Johnny Bower, Larry Hillman, Tim Horton, Red Kelly, Dave Keon, Frank Mahovlich, Bob Pulford, Eddie Shack, Allan Stanley (11 players), Stafford Smythe, Harold Ballard, John Bassette, Punch Imlach, King Clancy, Bob Haggart, Tom Nayler (7 non-players), Bob Davidson, Karl Elieff (were part of all 4 cups, but were not included on the cup each season.)


== See also ==
1966–67 NHL season


== Notes ==


== References ==
Diamond, Dan, ed. (2000). Total Stanley Cup. NHL. 
McFarlane, Brian (1973). The History of the National Hockey League. New York: Charles Scribner's Sons. ISBN 0-684-13424-1. 
Podnieks, Andrew; Hockey Hall of Fame (2004). Lord Stanley's Cup. Triumph Books. ISBN 978-1-55168-261-7.