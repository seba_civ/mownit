Christoph Knasmüllner (born 30 April 1992) is an Austrian football midfielder who plays for Admira Wacker.


== Career ==
He began his career with FK Austria Wien and played for the reserves, making his debut 1–3 loss against AKA Austria Kärnten and scored his first goal. He scored his second goal in FSA Austria Wien's 5–0 win against Fußballakademie Linz. In the 2007–08 season, he played 12 games for the reserve team and scored five goals.
In July 2008, he moved to Bayern Munich Under-17. He made his debut in a 3–1 win against Eintracht Frankfurt U17 and scored his first goal. He scored his second goal in the second match. He made his debut for Bayern's Under 19 team in their 8–0 win against SSV Jahn Regensburg U19.
In November 2009, a number of suspensions caused Knasmüllner to be called up to Bayern's reserve team, and he made his debut in a 3. Liga match against Eintracht Braunschweig. He immediately became a regular at that level and was named in Bayern's first-team squad for the 2010–11 UEFA Champions League, where he was given the number 29, and was named on the substitutes' bench for a Bundesliga match against Hannover 96 in October 2010. He moved to Internazionale in January 2011 to play for their Primavera team. After half a season without appearance in the first squad, he moved on 31 August 2011 to FC Ingolstadt 04 in the 2. Bundesliga. Three years later he returned to Austria, signing for Admira Wacker.


== References ==


== External links ==
Christoph Knasmüllner profile at Fussballdaten