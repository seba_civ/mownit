The eleventh season of the American comedy television series It's Always Sunny in Philadelphia premiered on FXX on January 6, 2016. The season consists of 10 episodes and concluded on March 9, 2016.


== Cast ==


=== Main cast ===
Charlie Day as Charlie Kelly
Glenn Howerton as Dennis Reynolds
Rob McElhenney as Ronald "Mac" McDonald
Kaitlin Olson as Deandra "Dee" Reynolds
Danny DeVito as Frank Reynolds


=== Recurring cast ===
Mary Elizabeth Ellis as The Waitress (2 episodes)
Lance Barber as Bill Ponderosa (2 episodes)
David Hornsby as Matthew "Rickety Cricket" Mara ("Dee Made a Smut Film")
Artemis Pebdani as Artemis ("Being Frank")
Brian Unger as the Lawyer ("McPoyle vs. Ponderosa: The Trial of the Century")
Catherine Reitman as Maureen Ponderosa ("McPoyle vs. Ponderosa: The Trial of the Century")
Andrew Friedman as Uncle Jack Kelly ("McPoyle vs. Ponderosa: The Trial of the Century")
Thesy Surface as Margaret McPoyle ("McPoyle vs. Ponderosa: The Trial of the Century")


=== Guest stars ===
Andy Buckley as Andy ("Chardee MacDennis 2: Electric Boogaloo")
Dean Cameron as Driscoe ("The Gang Hits the Slopes")
Courtney Gains as Roach ("The Gang Hits the Slopes")
Kevin Farley as Turkey ("The Gang Hits the Slopes")
Richard Grieco as Richard Grieco ("Dee Made a Smut Film")
Reginald VelJohnson as Judge Melvoy ("McPoyle vs. Ponderosa: The Trial of the Century")
Guillermo del Toro as Pappy McPoyle ("McPoyle vs. Ponderosa: The Trial of the Century")
Brian Doyle-Murray as Captan Garcia ("The Gang Goes to Hell")
Tuc Watkins as Scott ("The Gang Goes to Hell")
Bryan Cogman as Insurance Adjuster ("The Gang Goes to Hell: Part Two")


== Production ==
The series was renewed for an eleventh and twelfth season on April 4, 2014, each to consist of 10 episodes.


== Episodes ==


== References ==


== External links ==
List of It's Always Sunny in Philadelphia episodes at the Internet Movie Database
List of It's Always Sunny in Philadelphia season 11 episodes at TV.com
It's Always Sunny in Philadelphia at epguides.com