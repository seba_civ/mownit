Online Chess Kingdoms is a chess simulation game for the PlayStation Portable released by Konami in 2006 in North America and 2007 in Europe. Online Chess Kingdoms was the first chess game released for the PlayStation Portable. Chessmaster: The Art of Learning was released by Ubisoft the following year.


== Features ==
Online Chess Kingdoms uses the PlayStation Portable's wireless connectivity, featuring several multiplayer modes playable locally between PSP owners or over the internet. A single-player mode is also featured, allowing players to compete against an AI opponent at four different difficulty levels. Several different types of chess can be played, including speed chess and a battle mode in which games are played in real-time. Sets can be displayed in either 2D or 3D, the latter sets available in different themes (including magicians and an aquatic civilization) and featuring animated pieces that battle when captures take place, similar to Battle Chess. Games can be played individually or through a story mode.


== Story mode ==
The plot of the story mode concerns five warring realms: Order, Chaos, Magic, Reason, and Spirit. Phrenos, the creator of the universe, separated the realms to prevent them from fighting. After some time, however, the realms find a way to one another and begin fighting anew. Players choose a realm to fight with — each with its own unique animated chess set — and move their armies around a tactical map in an attempt to capture squares, which is done by winning games of chess.


== Critical reaction ==

Rolando Cordova of IGN gave Online Chess Kingdoms a score of 6.0 out of 10. He praised the "attention grabbing ... visual style", "fun to watch" animated battles, and computer AI, which "does a fine job of challenging a range of players, from novice to master strategists", but felt the game "offers nothing that previous chess videogames haven't covered a billion times before. ... Regardless of what the pieces look like, or how well they're animated, they still behave the same way." Cordova said the online mode "works fine, but finding a partner can take a good long while." He concluded that while Online Chess Kingdoms "offer[s] a new visual style to [chess]", players should not buy it "expecting to find a revolutionized and less complicated version of [the game]."
Greg Kasavin of GameSpot gave the game a score of 6.4 out of 10. He praised the "fairly good looking" visuals, the "challenging" computer AI, and the online mode, saying that it "works fairly well", though he criticized it for its lack of ability to communicate with other players. Kasavin felt the story mode "represents an awkward attempt to layer a storyline and greater sense of purpose to chess", remarking that "chess doesn't need a plot", and called the real-time battle mode "at best ... a decent distraction from the real game." He concluded that Online Chess Kingdoms "inherently has something going for it just by letting you play a competent version of chess on the PSP", but he compared the game unfavorably to chess programs available on other platforms, saying that "other chess programs out there provide far more sophisticated scales of artificial intelligence to play against, as well as complete tutorials and training modes".


== References ==


== External links ==
Online Chess Kingdoms at PlayStation.com