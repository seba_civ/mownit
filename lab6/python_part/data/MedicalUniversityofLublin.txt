Medical University of Lublin has its origins in the year 1944 in Lublin, Poland. The university gained its autonomy in 1950. As the years passed, new departments were added such as the Department of Dentistry in 1973. An agreement with the Hope Medical Institute in the United States was signed in the early 1990s, which initiated a program of teaching medicine to English-speaking students at the Medical University of Lublin. Thereafter a four-year MD program was formed.
The teaching program covers all branches of modern medicine and is comparable to medical programs in the US. Through its partner Hope Medical Institute, the Medical University provides the last two years of medical school (clinical rotations) to eligible students in the US and Canada.
The university maintains lively international scientific contacts in cooperation with Hvidovre Hospital in Copenhagen, Denmark; Ziekenhuis-Tilburg Hospital (Netherlands), and Lvov Medical University in Ukraine, among others.


== Faculties ==
Faculty of Medicine
Faculty of Dentistry
Faculty of Pharmacy


== Notable people ==
Professor Tadeusz Krwawicz (Poland) developed the first cryoprobe for intracapsular cataract extraction in 1961.


== English Language Division ==
In 2001, the English Language Division of the medical faculty was formed. This branch is responsible for designing and implementing a curriculum for students in the English language. There are four-year and six-year programs. After completion of the coursework in Poland, students are given the option of pursuing clinical clerkships in Poland or the United States to fulfill the requirements for an M.D. degree.


== External links ==
Medical University of Lublin (in Polish)
Medical University of Lublin (in English)
Medical University of Lublin and Hope Medical Institute (English)
Sister school in Taiwan: Taipei Medical University


== References ==