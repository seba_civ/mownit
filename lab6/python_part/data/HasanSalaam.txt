Hasan Salaam (born January 12, 1981) is a rapper born in New York City and raised in New Jersey., his lyrics touch on such subjects as "post-colonial exploitation of African (hip-hop) culture", "the African diaspora". Salaam organizes food and clothing drives in Jersey City on the third Sunday of each month.


== Biography ==
Salaam started rapping at the age of 10. After seeing a preview for the movie Malcolm X in 1992 he read the Autobiography of Malcolm X, which inspired him.
In 2005 he won an award for Best Live Performance and Best Underground Song of the Year for the song "Blaxploitation" from his debut album Paradise Lost at the third Annual Underground Music Awards.
Together with Hicoup, Rugged N Raw, Impaq and Badsportt he is a member of the group 5th Column.
Salaam featured on the UK Hip hop group Mecca2Medina's Truthseekers album in 2006.
Salaam is a deeply spiritual MC who has been described as having "the powerful delivery of Chuck D, the racial love of Mos Def, the instructional attitude of Krs-One, and the anger of Immortal Technique." Salaam's third album proper, Children of God, was released in 2008, featuring guest appearances from Lord Jamar, Masta Ace and Salaam’s 5th Column crew. Exclaim! described the album as "well rounded" and "handled superbly", saying that Salaam has a voice that is "trained like an instrument, commands attention". Children of God has been described as " themed deeply in all religions", and was described by HipHopDX as possibly "the new archetype in what religious-minded rap can be".


== Charitable work ==
Salaam organizes food and clothing drives in Jersey City on the third Sunday of each month. Hasan works with charities, non-profits, and youth all over the world. In November 2010, Hasan became the first U.S hip hop artist to ever perform in Guinea-Bissau. On this same trip he worked with the young artists, taught creative writing to the youth, and headlined a concert to promote freedom of speech. The “Music Is My Weapon” project is the next step, in an effort to provide change to a country that remains one of the poorest and least politically stable in the world. December 2011, Hasan Salaam released an EP "Music Is My Weapon" employing all profits to build a well, school, and clinic in Guinea-Bissau.


== Discography ==
5th Column Mixtape Vol.II (2004)
Paradise Lost (2005) Day by Day Entertainment
The Reavers Terra Firma (2005)
Tales of the Lost Tribe: Hidden Jewels (2006)
Children of God (2008) 5th Column Media
Mohammad Dangerfield - "Mohammad Dangerfield" (self-titled) (2011)
"Music Is My Weapon" (2011)
Life in Black and White (2014)


== References ==


== External links ==
Jasarevic, Mina (2008) "Underground Report: Elzhi and Hasan Salaam", HipHopDX, 16 August 2008