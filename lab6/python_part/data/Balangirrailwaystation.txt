Balangir railway station is a railway station which serves Balangir district in Indian state of Odisha.


== History ==
The 79 km (49 mi) Vizianagaram-Parvatipuram line was opened in 1908–09. The Parvatipuram-Raipur line was completed in 1931. In 1960, Indian Railway took up three projects: the Kottavalasa-Koraput-Jeypore-Kirandaul line ( Dandakaranya Project ), the Titlagarh-Bolangir-Jharsuguda Project and the Rourkela-Kiriburu Project. All the three projects taken together were popularly known as the DBK Project or the Dandakaranya Bolangir Kiriburu Project. The Sambalpur-Titlagarh line was opened to traffic in 1963.


== Railway reorganisation ==
The Bengal Nagpur Railway was nationalised in 1944.Eastern Railway was formed on 14 April 1952 with the portion of East Indian Railway Company east of Mughalsarai and the Bengal Nagpur Railway. In 1955, South Eastern Railway was carved out of Eastern Railway. It comprised lines mostly operated by BNR earlier. Amongst the new zones started in April 2003 were East Coast Railway and South East Central Railway. Both these railways were carved out of South Eastern Railway.


== Khurda Balangir rail link ==
The Khurda Balangir rail link was surveyed in 1945 but the project was sanctioned in 1994–95. The stretch of only 41.5 kilometres (25.8 mi) from Khurda Road to Rajsunakhala has been constructed out of the 289 kilometres (180 mi) project. The work on infrastructure started in 2007. The state government has signed Memorandum of Understanding with Indian railways to undertake some parts of the project on cost sharing basis. The project which was initially estimated to be ₹1000 crore (US$150 million) is now estimated to be nearly ₹2000 crore (US$300 million) after almost 20 years of delay due to low budgetary provisions and issues of land acquisition. This rail line will link cities of Nayagarh, Boudh and Sonepur with Bhubaneswar and Balangir. Smaller towns like Dasapalla, Banigocha and Manipur will also be linked by this rail line. Work at Balangir side of this project has also started.


== Electrification ==
Electrification of the Titlagarh–Sambalpur-Jharsuguda was sanctioned in the Railway Budget for 2012–13.


== References ==