David Dark is an American writer, the author of The Sacredness of Questioning Everything, Everyday Apocalypse: The Sacred Revealed in Radiohead, The Simpsons, and Other Pop Culture Icons and The Gospel According To America: A Meditation on a God-blessed, Christ-haunted Idea, which was included in Publishers’ Weekly’s top religious books of 2005. He also contributed a chapter to the book Radiohead and Philosophy: Fitter Happier More Deductive (Chicago: Open Court, 2009). Following years of teaching high school English, he received his doctorate in 2011 and now teaches at the Tennessee Prison for Women, Charles Bass Correctional Facility, and Belmont University where he is assistant professor in the School of Religion. A resident of Nashville, Tennessee, he is married to singer/songwriter Sarah Masen.


== Appearances ==
David Dark has four times presented at the Festival of Faith and Music at Calvin College in Grand Rapids, Michigan. He was a workshop speaker in 2003 and 2009, and the keynote speaker at the 2005 and 2007 events. He is in the 2013 documentary "American Jesus" in which he tells the story of 'Uncle Ben' to explain the relationship Christians have with their faith.


== Published works ==
Everyday Apocalypse: The Sacred Revealed in Radiohead, The Simpsons, and Other Pop Culture Icons (2002, Brazos Press)
The Gospel According To America: A Meditation on a God-blessed, Christ-haunted Idea (2005)
The Sacredness of Questioning Everything (2009, Zondervan)
Life's Too Short to Pretend You're Not Religious (2016, InterVarsity Press)


== External links ==
David Dark's Blogspot
Dark Interviewed by Ray Waddle
Coverage in Publishers' Weekly
Review of The Sacredness of Questioning Everything