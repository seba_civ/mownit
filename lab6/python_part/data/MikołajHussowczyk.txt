Mikołaj Hussowczyk (Belarusian: Мікола Гусоўскі/Mikola Husoŭski, Lithuanian: Mikalojus Husovianas, Latin: Nicolaus Hussovianus). Other name spelling variants include Hussoviensis, Hussovianus, Ussovius, Hussowski, Gusowski); (c. 1480 – c. 1533) was a Belarusian and Polish early Renaissance poet and humanist, and cultural and social activist. His most notable work is Carmen de statura...bisontis (Song about...the bison).


== Biography ==
Little is known of his life, except during the 1520s. His place of birth is alternatively stated as Husów/Hussowo near Łańcut or in an unspecified Belarusian village with similarly sounding name  (there are many Belarusian toponyms of the Usava, Husau, Usa kind. Some folkloristic features in the works of Husowski may point to regions in vicinity of Hrodna).
He was a catholic priest (from 1518) and apostolic notary for Grand Duchy of Lithuania. Hussowczyk was a citizen of the multinational Grand Duchy of Lithuania. As a side-effect of this, along with his uncertain birthplace, modern Polish, Lithuanian and Belarusian researchers tend to ascribe works of Husowski to their respective cultural heritages.
Hussowczyk became a courtier of Erazm Ciołek, the Bishop of Płock. In 1521 he accompanied the bishop on his year-long journey to Rome and he returned to Poland after Ciołek's death, settling in Kraków, where he probably had his ordination.


== Works ==
Per the scope of his literary works, Hussowczyk is considered to be the founder of the Belarusian Renaissance literary tradition, and author of the oldest examples of Polish-Latin poetry of the early Renaissance. Hussowczyk wrote in publicistical, epic, heroic, lyrical, historical and satirical genres.
His Latin poem Carmen de statura, feritate ac venatione bisontis ("A Song about the Appearance, Savagery and Hunting of the Bison", Kraków 1523) is considered to be the first large scale fictional work about Medieval Lithuania (or Belarus), describing the bison's life and habits, Lithuania's landscape and the relationship of its inhabitants with the environment. Written in Latin for Pope Leo X, an avid hunter, the poem stems from Hussowczyk's experience in hunting and observing bison, and contains no literary comparisons with ancient legendary creatures. Due to the deaths of both the Pope and Hussowczyk's patron, bishop Ciołek, the poem was eventually presented to Polish Queen Bona in Kraków. According to Polish researcher Edmund Kotarski, Hussowczyk belonged to the circle of Polish-Latin poets of the early Renaissance.
His best known works are:
«Carmen de statura feritate ac venatione bisontis» (English: «The Song about Bison, Its Stature, Ferocity and Hunt», written in Classical Latin, 1522).
«Nova et miranda victoria de Turcis mense Iulio» («New and famous victory over Turks in month of July») (1524, written in one day, under the impression from the victory of the Polish Great Hetman of the Crown Mikołaj Firlej over Turks by Trembowla on July 2, 1524).
«De vita et gestis Divi Hyacinthi» («Life and feats of St. Hyacynth») (about Polish Saint Hyacinth (Jacek Odrowąż)) (1525).


== See also ==
Renaissance in Poland


== References ==


== Further reading ==
Jerzy Ochmański Narodowość Mikołaja Hussowskiego w świetle jego autografu w: Słowiańszczyzna i dzieje powszechne Warszawa 1985 s. 317
J. Pelczar, Mikołaj Hussowski, jego życie i dzieła. Ustęp z dziejów humanizmu w Polsce, Kraków 1900,
J. Krókowski, Mikołaja Hussowskiego Carmen de bisonte, Wrocław 1959,
C. Backvis, Mikołaj z Hussowa, w: Tegoż, Szkice o kulturze staropolskiej, Warsawa 1975.
J. Ziomek, Renesans, Warszawa 1998.
Entry in Polski Słownik Biograficzny


== External links ==
Biography and text of "Carmen de bisontis", translation into Polish by Jan Kasprowicz
(Polish) Dariusz Rott, Mikołaj Hussowski
Text of "Carmen de bisontis" in Lithuanian
Text of "Carmen de bisontis" in Belarusian
"Hunting the Lithuanina Bison" Amphora 6.1, by Frederick J. Booth