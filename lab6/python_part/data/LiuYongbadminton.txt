Liu Yong (simplified Chinese: 刘永; traditional Chinese: 劉永; pinyin: Liú Yǒng;, born August 12, 1975 in Nanjing, Jiangsu) is a male former international level badminton player for China who specialized in mixed doubles.
He is married to former singles player on the Chinese national badminton team Dai Yun.


== Career ==
Liu Yong started badminton training when he was 8, and was taken into the Chinese National Team in 1993. He won numerous international titles in mixed doubles, the majority of them with Ge Fei. He won men's doubles at the 2002 Malaysia Open with Chen Qiqiu.


=== World Championships ===
Liu won the 1997 IBF World Championships in mixed doubles, with Ge Fei, beating Jens Eriksen and Marlene Thomsen in the final. They also won a bronze medal at the 1999 IBF World Championships. In the next edition in 2001, Liu reached the mixed doubles quarterfinals with Cheng Jiao but lost to Michael Søgaard and Rikke Olsen.


=== Summer Olympics ===
Liu Yong competed in badminton at the 2000 Summer Olympics in mixed doubles with Ge Fei. In the first round they had a bye, and in the second round they were defeated by Chris Bruil and Erica van den Heuvel, from the Netherlands.


== Major achievements ==


== References ==


== External links ==
BWF Player Profile