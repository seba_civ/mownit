Edmund Michael Dunne (February 2, 1864 – October 17, 1929) was an American prelate of the Roman Catholic Church. He served as Bishop of Peoria from 1909 until his death in 1929.


== Biography ==
Edmund Dunne was born in Chicago, Illinois, to an Irish family, and attended the parochial school of Holy Name Cathedral, Chicago. He studied at St. Ignatius College in Chicago before entering Niagara University in New York. He completed his theological studies at the American College in Leuven, Belgium. He was ordained to the priesthood by Archbishop Patrick Feehan on June 24, 1887.
Dunne furthered his studies at the Pontifical Gregorian University in Rome, from where he obtained a Doctor of Divinity degree in 1890. Following his return to the United States, he received his first pastorate at St. Columbkille Church in Chicago, where he remained for eight years. He was later named pastor of Guardian Angels Parish and chancellor of the Archdiocese of Chicago.
On June 30, 1909, Dunne was appointed the second Bishop of Peoria by Pope Pius X. He received his episcopal consecration on the following September 1 from Archbishop Diomede Falconio, O.F.M., with Bishops John Janssen and Peter Muldoon serving as co-consecrators. He served as the spiritual leader of Catholics in Central Illinois for twenty years, until his death at age 65.


== References ==

 