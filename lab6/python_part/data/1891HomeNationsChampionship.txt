The 1891 Home Nations Championship was the ninth series of the rugby union Home Nations Championship. Six matches were played between 3 January and 7 March. It was contested by England, Ireland, Scotland and Wales.
The 1891 Championship was won by Scotland, the fourth time the country had topped the table, but the first time Scotland had taken the Triple Crown title.
Rule changes this year, included the introduction of penalty goals, as although penalty kicks were introduced in 1882 no goal attempts could be made from one until this season. The two umpires were renamed as touch-judges and their powers were reduced to mark the spot where the ball left the field of play; a status that remained until additional powers were reinstated in 1982. Players could now pick up a dead ball, and the dead ball line was set at a maximum of 25 yards.


== Table ==


== Results ==


=== Scoring system ===
The matches for this season were decided on points scored. A try was worth one point, while converting a kicked goal from the try gave an additional two points. A dropped goal was worth three points. Penalty goals were now worth three points, as although introduced in 1882, no attempt could be made from a penalty kick at goal until this season.


== The matches ==


=== Wales vs. England ===

Wales: Billy Bancroft (Swansea), Tom Pearson (Cardiff), Charlie Arthur (Cardiff), David Gwynn (Swansea), Percy Lloyd (Llanelli), Charlie Thomas (Newport), Hugh Ingledew (Cardiff), Percy Bennett (Cardiff Harlequins), David William Evans (Cardiff), Harry Packer (Newport), William Bowen (Swansea) capt., Walter Rice Evans (Swansea), Jim Hannan (Newport), Rowley Thomas (London Welsh), Edward Pegge (Neath)
England: William Grant Mitchell (Richmond), Frederic Alderson (Hartlepool Rovers) capt., RE Lockwood (Heckmondwike), Percy Christopherson (Blackheath), William Leake (Harlequins), J Berry (Tyldesley), Eustace North (Oxford Uni.), Tom Kent (Salford), Sammy Woods (Cambridge U.), JT Toothill (Bradford), D Jowett (Heckmondwike), Richard Budworth (Blackheath), RP Wilson (Liverpool OB), William Bromet (Tadcaster), J Richards (Bradford)


=== Ireland vs. England ===

Ireland Dolway Walkington (NIFC) capt., RW Dunlop (Dublin U.), S Lee (NIFC), R Montgomery (NIFC), Benjamin Tuke (Bective Rangers), AC McDonnell (Dublin U.), JN Lytle (NIFC), EG Forrest (Wanderers), J Waites (Bective Rangers), JH O'Conner (Bective Rangers), CV Rooke (Dublin U.), J Roche (Wanderers), WJN Davis (Bessbrook), Victor Le Fanu (Landsdowne), LC Nash (Queen's Co. Cork)
England: William Grant Mitchell (Richmond), Frederic Alderson (Hartlepool Rovers) capt., RE Lockwood (Heckmondwike), Piercy Morrison (Cambridge U.), William Leake (Harlequins), J Berry (Tyldesley), Eustace North (Oxford Uni.), Tom Kent (Salford), Sammy Woods (Cambridge U.), JT Toothill (Bradford), D Jowett (Heckmondwike), Launcelot Percival (Oxford Uni.), RP Wilson (Liverpool OB), William Bromet (Tadcaster), J Richards (Bradford)


=== Scotland vs. Wales ===

Scotland: Henry Stevenson (Edinburgh Acads), Gregor MacGregor (Cambridge Uni), Paul Clauss (Oxford Uni.), W Neilson (Merchiston), CE Orr (West of Scotland), Darsie Anderson (London Scottish), Frederick Goodhue (London Scottish), A Dalglish (Gala), HTO Leggatt (Watsonians), GT Neilson (West of Scotland), MC McEwan (Edinburgh Acads) capt., I MacIntyre (Edinburgh Wanderers), Robert MacMillan (West of Scotland), JD Boswell (West of Scotland), JE Orr (West of Scotland)
Wales: Billy Bancroft (Swansea), William McCutcheon (Swansea), Dickie Garrett (Penarth), David Gwynn (Swansea), George Thomas (Newport), Ralph Sweet-Escott (Cardiff), Hugh Ingledew (Cardiff), Percy Bennett (Cardiff Harlequins), Sydney Nicholls (Cardiff), Tom Graham (Newport), William Bowen (Swansea), Walter Rice Evans (Swansea), David Daniel (Llanelli), Rowley Thomas (London Welsh), Willie Thomas (Llanelli) capt.


=== Ireland vs. Scotland ===

Ireland Dolway Walkington (NIFC) capt., RW Dunlop (Dublin U.), S Lee (NIFC), HG Wells (Bective Rangers), Benjamin Tuke (Bective Rangers), ED Cameron (Bective Rangers), JN Lytle (NIFC), RD Stokes (Queens College), J Moffatt (Belfast Albion), JH O'Conner (Bective Rangers), JH O'Conor (Bective Rangers), J Roche (Wanderers), WJN Davis (Bessbrook), EF Frazer (Bective Rangers), LC Nash (Queen's Co. Cork)
Scotland: Henry Stevenson (Edinburgh Acads), Gregor MacGregor (Cambridge Uni), Paul Clauss (Oxford Uni.), GR Wilson (Royal HSFP), CE Orr (West of Scotland), William Wotherspoon (Cambridge Uni), Frederick Goodhue (London Scottish), A Dalglish (Gala), HTO Leggatt (Watsonians), GT Neilson (West of Scotland), MC McEwan (Edinburgh Acads) capt., I MacIntyre (Edinburgh Wands), WR Gibson (Royal HSFP), JD Boswell West of Scotland, JE Orr (West of Scotland)


=== England vs. Scotland ===

England: William Grant Mitchell (Richmond), Frederic Alderson (Hartlepool Rovers) capt., RE Lockwood (Heckmondwike), Percy Christopherson (Blackheath), William Leake (Harlequins), J Berry (Tyldesley), Eustace North (Oxford Uni.), Tom Kent (Salford), Sammy Woods (Cambridge U.), Edgar Bonham-Carter (Oxford Uni.), D Jowett (Heckmondwike), Richard Budworth (Blackheath), RP Wilson (Liverpool OB), John Rogers (Moseley), J Richards (Bradford)
Scotland: Henry Stevenson (Edinburgh Acads), Gregor MacGregor (Cambridge Uni), Paul Robert Clauss (Oxford Uni.), Willie Neilson (Merchiston), CE Orr (West of Scotland), Darsie Anderson (London Scottish, Frederick Goodhue (London Scottish), Robert MacMillan (London Scottish), HTO Leggatt (Watsonians), GT Neilson (West of Scotland), MC McEwan (Edinburgh Acads) capt., I MacIntyre (Edinburgh Wands), WR Gibson (Royal HSFP), JD Boswell (West of Scotland), JE Orr (West of Scotland)


=== Wales vs. Ireland ===

Wales: Billy Bancroft (Swansea), Tom Pearson (Cardiff), Dickie Garrett (Penarth), Charlie Thomas (Newport), Percy Lloyd (Llanelli), Evan James (Swansea), David James (Swansea), John Samuel (Swansea), Charles Nicholl (Llanelli), Tom Graham (Newport), Stephen Thomas (Llanelli), Tom Deacon (Swansea), David Samuel (Swansea), Rowley Thomas (London Welsh), Willie Thomas (Llanelli) capt.
Ireland Dolway Walkington (NIFC), RW Dunlop (Dublin U.), S Lee (NIFC), HG Wells (Bective Rangers), R Pedlow (Bessbrook), ED Cameron (Bective Rangers), T Fogarty (Garryowen), RD Stokes (Queens College), FO Stoker (Wanderers), JS Jameson (Lansdowne), R Stevenson (Dungannon) capt., J Roche (Wanderers), WJN Davis (Bessbrook), CV Rooke (Dublin U.), LC Nash (Queen's Co. Cork)


== External links ==
"6 Nations History". rugbyfootballhistory.com. Archived from the original on 14 October 2007. Retrieved 2007-10-28. 


== References ==