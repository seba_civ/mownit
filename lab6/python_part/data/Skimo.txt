Skimo is the first Nickelodeon comedy sitcom made in Mexico. It is produced by Nickelodeon Latin America and Macias Group International. The show first aired on May 15, 2006, and ran at 8:00 pm for the first two seasons. In the third season of the show, it was moved to 6:00 pm on Mondays. Now, for the 2007 summer-fall transition, the show airs reruns weekdays at 7:00 pm; also, during the 2006 summer-fall transition, the show aired reruns on Mondays at 6:00pm; during the 2007 break between seasons 2 and 3, the show aired reruns of 3 episodes on Mondays at 6:00 pm as well. Currently, it is airing reruns on Nick at Nite block on Nickelodeon Latin America. The opening song for the show, called "Skimo Es el Lugar" ("Skimo Is the Place"), is performed by the pop rock band Motel.


== Overview of the show ==
Skimo is the story of two boys, Fito and Tavo, who made their dreams reality by opening their own shop. Skimo is described as "the coolest place in the neighbourhood".
As an eatery, Skimo sells many innovative flavoured foods, like strawberry milkshakes, mango on a stick with chamoy sauce, or the vanilla ice-cream stuffed pepper.


== Episodes ==


== Characters ==


=== Fito (Daniel Tovar) ===
Fito is the typical "cool guy" co-protagonist in the series. He does not earn good grades in school, but he is athletic and portrays himself as "the coolest dude in the neighborhood" almost everywhere and always has odd ideas to improve Skimo (like selling one Skimo at twice its price, instead of selling 2X1). He likes rock, hip-hop, and beatbox music and he has an "internal beat" when he is speaking or doing something. He's a dreamer and a schemer, and he talks about his fantasies as if they were real. Fito is 16 years old. His most notorious feature is his surfer-afro hair, and his skateboard-style clothes.


=== Tavo Nachón (Miguel Santa Rita) ===

Tavo is Fito's best friend, although they call themselves business partners. He is the smart co-protagonist in the series, and his intelligence is fundamental for Skimo's success. He has a secret crush on Úrsula. He is very timid, and also considered a "geek" due to his obsession with order, control and extreme cleanliness. He uses better or different words than Fito because he is smarter. In the first episode is revealed that Tavo's last name is "Nachon".
Just like all of the friends, Tavo is the opposite of Fito, and maybe that's the reason that they are friends. He wears casual clothes, and he hates all kinds of sickness. Tavo is 17 years old. By the end of season 2 his parents get divorced and he has to go live in another state and this ends season 2. In season 3 he spends a lot of time with his new girlfriend, Cris, who makes Ursula very jealous; however, it is hinted that maybe he is still in love with Ursula.
Tavo's name was probably inspired by the Spanish word "bonachón", which translated means "goody".


=== Don Filemón (Pedro Romo) ===
Don Filemón was the owner of the pizzeria that once occupied the site that Skimo now occupies. Fito and Tavo decided to rent the whole place from him to make Skimo.
Don Filemón is the classical lifelong storyteller, and always speaks about his world travels, or his travelling family; no one believes these stories.


=== Fátima (Claudia Bollat) ===
Fatima is the portly, expert cook in Skimo. She makes all dishes and meals and helps in a lot of things. She is a very dreamy person who likes to know how much she is needed. She sometimes experiments by combining two different recipes, making the dishes that Skimo is noted for. When the order is done she shouts very loud "Sale un (order) para la mesa (number of the table)" which is Spanish for "Ready some... (order)... for the table number...(number of the table)" until the grade of a deafening.
She also has an alter-ego known as Super Fat, a renowned Lucha Libre wrestler. The reason she became a wrestler was to get revenge against her ex-boyfriend Nitro because he refused to drink one of her strawberry milkshakes. However, it is later proved that Nitro is actually allergic to strawberry, so they solve their misunderstanding and go back together.


=== Nora and Nori (Alejandra and Avelina Vazquez) ===

Nora and Nori are 16-years old and are the typical antagonists in the show. They are very showoffy, irritating, selfish, and annoying. They always want to lead Skimo to bankruptcy, and also try to complicate everything in Fito and Tavo's life.
They always refer to one another as twinie, and they shout out "twin power" anytime they have an idea or plot. Also, anytime Nora and Nori hear something outrageous they shout "Oh Please". Everyone in the show mistakes one for the other.


=== Úrsula (Tatiana Martinez) ===

She is the "I-always-do-the-right-thing" girl, and she is a very good friend of Fito and Tavo, but she ignores the fact that Tavo has a crush on her. Ursula is very intelligent and can fix almost any gadget. She is totally fool, and is sometimes considered to be gothic or punk. She always wants Fito and Tavo to use their brains to solve their problems.
She is courageous and fearless, and she is the opposite of Nora and Nori. She usually always bangs Nora and Nori's heads together. She adds red highlights to her hair in Season 2. Ursula is 16 years old.
Also in season 2 Ursula falls in love with Fito who seems to have fallen in love with her too ignoring the fact that they are always arguing. However, their crush fade almost instantly and from season 3 onward, she starts developing a crush on Tavo and tries several times to steal him from Cris.


=== Shi (Paul Cortinas) ===
He is a very good example of a videogame addict. He was included in a very expensive arcade machine that Fito bought "because it has 3-D Graphics and it was very cheap". He is always playing at the arcade machine, and the little times he leaves the machine, he plays handheld videogames. He never allows anyone to touch the arcade when he is playing, excepting for Shila. In one episode, he receives a trophy for being the country's best gamer. Nobody really knows who this 12-year-old boy is. At the beginning it was thought that Shi was speechless, but in the last episode is revealed that he speaks.


=== Mucho Mucho (Oswaldo Ibarra) ===
He is the classic jack-of-all-trades, and helps supplying Skimo. He likes to say his name a lot and talks very quickly. He is also seen doing another jobs, like those of policeman, plumber, doctor, surgeon, etc. He always has every needed thing in his almost infinite Hammerspace bag. His name might translate as "A lot A lot" or "Very Very".


== Other characters ==


=== Salvador ===
He's a poor little boy whom Tavo gave the Christmas Star (that goes on top of the Christmas tree) in the (Christmas'Two-part episode). In the episode "No Lotto No", Tavo gave Salvador a ticket for winning a contest that you win money in. Tavo in the Christmas Special made a show just for Salvador but Nora and Nori, the twins, ruined it. Salvador still liked the show as his Christmas present.


=== Cris Long ===
She's like Tavo's girlfriend and crush from season 3-. She collects Wrestling masks like Tavo. She is Nora and Nori's cousin. She almost married Tavo in the Kermes Fair, but Nora and Nori arrested them just before Tavo was about to say yes. She is portrayed by Karen Sandoval


=== Aquiles Boy ===
He is the twins' bodyguard. He is also the nephew of Nitro, the wrestler that is defeated by Super-Fat (Fatima) in the episode "Arena Skimo". His name is the homophone of the Spanish sentence "Aquí les voy", which means "Here I come".


=== Tavini ===
He is Tavo's magician whose magic tricks almost never work correctly.


=== Nicole ===
Nicole is a French teenager who goes to Fito and Tavo's school. Fito immediately had a crush on her when she arrived so he decided to use his low average in French class to get tutoring from her. Nicole only appeared in the episode "French 101".


=== Fulvio ===
Fulvio "Caradura" is an antagonist to Fito. He has a skater band and is considered a "tough guy" in the neighbourhood. He appeared in the very first episode in a brief cameo, but his debut appearance was later in the episode "Hang Em' High". After menacing Fito, who dated his sister Andrea, Tavo decided to give him a mega wedgie. At the end of the episode "Boys Will be Girls", Tavo was dressed like a girl and Fulvio fell in love with "her". When he re-tells the story of the mega-wedgie given to him by Tavo, Fito, who was also disguised as a girl at the time, avenges himself by giving Fulvio a mega wedgie.


=== Evil Albatross Skaters Gang ===
Fulvio's skater gang, that come sometimes to Skimo and wreak havoc around the place, as well as give wedgies to Fito and Tavo. Both the twins and Fito tried to become part of the gang in the episode "Of Surprises And Skates". One of the members is also well known for being a kitten lover and a poet.


=== Andrea ===
Andrea is Fulvio's sister, who Ursula accuses of being a frivolous flirt. Fito dated her for a little while.


=== Deyanira ===
She's Cris's "not too well looking" friend. She fell in love with Fito but Fito didn't fall in love with her. Indeed, she married Fito at the Kermes Fair. Fito can't free himself from the marriage unless he gets the "wedding ring" off of his finger, but it is stuck. When Fito finally braks her heart she gets a makeover and enters the "Miss Skimo" contest by posing as an international super-model who Fito instantly develops a crush with.


=== The Chilindrina ===
She's an actress that Tavo and Don Filemon hire to play Don Filemon's niece, so Fito got busy taking care of her while they planned his birthday party. She faked to have a crush on Fito although she doesn't receive the same feelings from Fito.


=== Heicker ===
Heicker is Tavo's brother. He is a little bit of a distracted guy but with many girlfriends. At the end of the episode "Oh Brother!", he saves Tavo from an explosion in one of his magic tricks. Heicker thinks Tavo likes him but Tavo is always jealous of him.


=== Tato ===
He appears briefly during the episode "Goodbye Tavo". He became Fito's new best friend and partner, but got fired for having a crush on Nora and Nori.


=== Super Fat ===
She's Fatima's wrestling name and other personality. She has appeared until now in the "Arena Skimo" episode and in the "Mucho Mucho Is A Go Go" episode.


=== Shila ===
She's another "videogame-addict". She has a crush on Shi and talks more than Shi (he NEVER talks). Shi has a crush on her too.


=== Churritos ===
Churritos is a fluffy doll dog. He appears in few episodes of Skimo. He seems to be a toy of Fito, but it is not clarified in the series. His "catchphrase" is Hola, soy Churritos, ¿quieres ser mi amigo? (Hi, I'm Churritos, wanna be my friend?). His name is a variation of a typical Mexican food, the churro.


== Episodes ==


== Running Gags ==
Whenever Uuuu's names is called he jumps into the air out of his fishbowl.
Ursula bangs Nora and Nori's heads together whenever they bother her.
Many of the characters call Don Filemon, "Don File" which usually aggravates him.


== Notable Guest Stars ==
Motel
Kudai
RBD
Kalimba Marichal
Jesse & Joy
María Antonieta de las Nieves
Gerardo Reyero


== Trivia ==
Pedro Romo is very known by his role in the film series La risa en vacaciones.
In an episode of the second season, if you pay attention at the screen of the arcade game that Shi is playing, he is playing The Simpsons (note Homer Simpson's name on the screen).
Shi is a character like Lenny in Lizzie McGuire, he never speaks.
Tatiana Martinez bares close resemblance to Selena Gomez.
In the episode "Hang 'Em High", Nora and Nori were reading Nick Magazine.
In the episode "Something Scary This Way Comes" Fito is seen wearing shirt with the logo of Ghostbusters.
The character Mucho Mucho is like Gordy from another Nickelodeon series Ned's Declassified School Survival Guide.


== See also ==
Isa TKM
Isa TK+
Sueña conmigo


== External links ==
Official Skimo website
Official Skimo mobile game
Official Skimo IMDB page


== References ==