Erik Johan Löfgren (15 May 1825, Turku - 10 December, Turku) was a Finnish-Swedish portrait painter.


== Biography ==
He was born to a family of merchants. His first art lessons came from a Norwegian-born drawing teacher named T.J. Legler who, together with his friends, convinced Löfgren's father that he should be sent to Stockholm for further studies. He was enrolled at the Royal Swedish Academy of Arts in Stockholm from 1842 to 1852 and studied with Fredric Westin.

In 1846, Robert Wilhelm Ekman was able to gain a grant for him from the Finnish Art Association so he could continue his studies. Two years later, again in need of money, the influential literary critic Fredrik Cygnæus suggested that he provide illustrations for " Döbeln at Jutas", a section from The Tales of Ensign Stål, a new epic poem by Johan Ludvig Runeberg, but only sketches were completed.
In 1851, Cygnæus helped to organize an exhibition to raise money for his post-graduate studies. He presented portraits of Psyche and Tycho Brahe. Once again, on Cygnæus' recommendation, he chose to go to the Kunstakademie Düsseldorf in 1853. His primary instructors there were Theodor Hildebrandt and Otto Mengelberg. He completed five paintings on themes they recommended; including Jesus in Gethsemane and Hagar in the desert with Ishmael.

He returned to Finland in 1858 and remained there until 1862, living in Turku at first, then moving to Helsinki, where he became a successful portrait painter and teacher. But, after sixteen years abroad, he became restless at home and took an opportunity to move to Paris. The following two years, he exhibited at the Salon. In 1864, he completed what is, perhaps, his best known work; Karin Månsdotter with mad King Erik XIV sleeping on her knee. After being exhibited in Sweden, King Charles XV awarded him the medal "Litteris et Artibus".
After returning to Finland in 1865 and settling in Helsinki, he devoted himself entirely to portrait painting. In addition to Cygnæus and Runeberg, his sitters included Carl Gustaf Estlander, Zachris Topelius and Magnus von Wright.
In 1878, he received a commission from the church in Somero for an altarpiece featuring the Transfiguration of Jesus. He began working there then, in 1879, went to Munich to complete some of the panels. While there, he became seriously ill and returned to the family home in Turku in 1881. He soon had a stroke, followed by a loss of vision that left him unable to work,and died in 1884.


== References ==


== Further reading ==
Erik Johan Löfgren: kuvataiteen lyyrinen romantikko (Romanticism in art, exhibition catalog: 6/9/1989-28/1/1990) Museovirasto, 1989 ISBN 951-90753-1-3


== External links ==
 Media related to Erik Johan Löfgren at Wikimedia Commons