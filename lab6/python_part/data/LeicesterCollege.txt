Leicester College is a further education college in Leicester, England. It is one of the largest colleges in the UK, with more than 26,000 students, 1,600 staff, plus an annual budget of £51million. It has three main campuses in the city centre, and more than 200 community venues across Leicester.


== History ==
Leicester College was formed from the merger of Charles Keene College and Southfields College on 31 July 1999. Since then, more than 250,000 people have studied with the College. In July 2002, Maggie Galliers was appointed as Principal of Leicester College. In June 2009 she was appointed a CBE in the Queen’s Birthday Honours List for services to local and national Further Education. The College entered the top 25% of colleges nationally, based on success rates, in 2004.


== Campuses and facilities ==
HRH the Princess Royal officially opened Leicester College’s flagship Abbey Park Campus on 12 May 2009. The royal visit marked the culmination of an ambitious £48 million accommodation project which also saw the College complete extensive building works at its Freemen’s Park Campus, as well as refurbishment of St Margaret’s Campus.


=== Abbey Park Campus ===
Abbey Park Campus features four floors of modern teaching and learning facilities including a 140-seat drama studio, science labs, dance studio, music studios, language lab, dental and childcare training facilities. It is also home to the college’s Technology and Engineering Centre which is based opposite the main campus buildings.


=== Freemen’s Park Campus ===
The vocational home of the college, Freemen’s Park has a professional hair and beauty salon, working kitchens which supply a bistro and silver service restaurant and construction workshops where learners can train in plumbing, brickwork, woodwork, painting and decorating. A food manufacturing innovation suite was officially opened at Freemen’s Park on 3 November 2009.


=== St Margaret’s Campus ===
The creative heart of Leicester College, St Margaret’s Campus is where courses in Art and Design, Fashion and Footwear, Computing, Media, Photography and Print take place. In 2009 several projects were completed, including a new online testing room, refurbishment of the refectory and the Media studio and Fashion rooms.


=== Outreach Centres ===
Leicester College provides training in 200 outreach venues.


=== Upper Brown Street ===
In November 2009, the college, together with four local music promoters, were successful in their bid for a five-year lease of the former Phoenix Arts Centre, in the centre of the city. Renamed Upper Brown Street, the venue is a showcase for live music and performances.


=== Retail Skills Centre ===
Leicester College is home to the Retail Skills Centre Leicester. The centre is a dedicated retail training facility and a founder member of the National Skills Academy for Retail – a government-backed network of training centres designed to increase productivity in the sector.


== HE provision ==
Leicester College is an Associate College of De Montfort University and part of Leicester University’s College and University Liaison Network. The College works with these and other higher education institutions to provide routes to Degree level in many subject areas, including Art & Design, Fashion, Business and Performing Arts, Construction, Care and Engineering.


== Notable alumni ==
Aaron Patterson – Michelin-starred chef at Hambleton Hall
Jane Stidever – Paralympic swimming gold medallist
Gok Wan – celebrity stylist


== External links ==
Official website