The Peach Blossom Fan (Chinese: 桃花扇; pinyin: Táohuā shàn; Wade–Giles: T'ao-hua shan) is a musical play and historical drama in 44 scenes that was completed in 1699 by the early Qing dynasty playwright Kong Shangren after more than 10 years of effort.
The play depicts the drama that resulted in the 1644 collapse of the Ming dynasty. The play recounts the death of the Ming dynasty through the love story of its two main characters, young scholar Hou Fangyu (侯方域) and a courtesan named Li Xiangjun (李香君). The Indiana Companion to Traditional Chinese Literature has called it "China's greatest historical drama."
An English translation published by the University of California Press was translated by Chen Shih-hsiang and Harold Acton, K.B.E. with Cyril Birch collaborating.


== Background ==
In the early Qing dynasty, the rise and fall of the dynasty touched many poets and playwrights, especially intellectuals, which pushed them into thinking of the historical lessons taught by the downfall of the Ming. These writers, including Kong Shangren, expressed hatred and regret at its collapse through their works and a sense of historical responsibility. Kong said he wanted to make clear what had made the decay happen. Kong heard stories about the period of Hong Guang (C: 弘 光) from his cousin Kong Fangxun (孔方訓), whose tale of Li Xiangjun inspired him into creating a script. But at that time, it was only a draft because Kong wanted to collect historical details. So during his three-year stay in the south, where the story took place, Kong got acquainted with Ming loyalists like Mao Xiang (冒襄), Deng Hanyi (鄧漢儀), Xu Shuxue (許漱雪), Zong Yuanding (宗元鼎), She Chacun (社茶村) and masters of art like Shitao, Gong Xian, and Cha Shibiao (查士標). He also visited historical sites such as Plum Blossom Mountain (梅花岭), Qin Huai River (秦淮河), Swallow Rock (燕子磯), Imperial Palace, and the Mausoleum of the Ming Emperor (明孝陵).


== Composition ==
The play was conceived as a two-part play, as stated in the notes of Liang Qichao. The play has over 40 total scenes. Birch wrote that this length is "not unduly long" for a southern-style (Yangtze Valley) Chinese play, citing the 55-scene length of Peony Pavilion.
The main portion of the play includes exactly 40 scenes. The "Enquiry" (prelude) section is located in the play's beginning. The first portion of the main play forms part one, the upper (上) part. The "Inter-calary" scene is in between the two parts of the main play. The second portion of the main portion of the play forms part two, the lower (下) part. The "Additional Scene" and then the "Sequel", the epilogue, are the final portions of the play.


=== Plot ===
In the late Ming dynasty, the reformist Donglin movement reinstituted the "Restoration Society" (C: 復社, P: fùshè, W: fu-she) in Nanjing to fight corrupt officials. Hou Fangyu, one of the Society's members, falls in love with courtesan Li Xiangjun beside the Qinhuai River. He sends Li Xiangjun a fan as a gift and becomes engaged to her. An official called Ruan Dacheng, delivers trousseau through celebrity Yang Longyou (T: 楊龍友, S: 杨龙友, P: Yáng Lóngyǒu, W: Yang Lung-yu) for Hou in order not to be isolated from the royal court. Hou is persuaded into accepting it, but Li Xiangjun rejects the gift firmly, which wins Hou Fangyu's respect.
Because he lacks military provisions, the commander of Wuchang Zuo Liangyu intends to move his army south to Nanjing, which terrifies the court. Considering Hou Fangyu's father had once been Zuo Liangyu's superior, Nanjing officials send Yang to ask Hou for help as a substitute. Hou Fangyu wries a letter to discourage Zuo from moving, but is slandered by Ruan for betraying the country, forcing him to find shelter with Shi Kefa in Yangzhou. Li Xiangjun and Hou Fangyu are separated.
At that time, the political situation runs out of control. News comes that Li Zicheng, the leader of peasant rebellion, had captured the capital Beijing, and that the Chongzhen Emperor had hanged himself. Ruan and Ma Shiying, the local governor of Fengyang (鳳陽督撫), crowns the Prince of Fu (福王) Zhu Yousong as new Emperor and changes the title of the reign into Hongguang 弘光. They persecute Reformists and indulge the Emperor with lust. Governor of Cao (曹撫) Tian Yang (田仰) covets Li's beauty and wants to take her as concubine. At the marriage ceremony, Li resists with a suicide attempt. She knocks her head on a pillar, leaving blood spots on the fan which was given by Hou Fangyu. After that, Yang draws a branch of peach blossoms with Li Xiangjun's blood on the fan, and it is sent to Hou Fangyu to show Li Xiangjun's determination. Jin Fu, author of Chinese Theatre, wrote that the fan and poem symbolize the integrity and determination of Li Xiangjun.
The Qing's army continues to go south, threatening the Ming government. However, the internal conflicts among four generals, who are in charge of strategic posts in north of the Yangtze River, are fierce, and Shi Kefa himself could not retrieve the defeat. Meanwhile, the new Emperor never cares about politics, only losing himself in song and dance. Ma Shiying and Ruan Dacheng send Li into the court as a gift, catering to the Emperor. Li Xiangjun scolds the evil officials to their faces and is beaten cruelly. Hou Fangyu flees to Nanjing during the chaotic war but was caught and sent into prison by Ruan Dacheng.
Yangzhou falls and Shi Kefa drowns himself into the river. The new Emperor is captured by the Qing army. The end of the play features a Taoist ceremony mourning the loss of the Ming dynasty. The remaining protagonists decide to seclude themselves instead of serving in the Qing dynasty. Hou Fangyu and Li Xiangjun meet each other occasionally at Qixia Mountain. When they are telling their affection, Zhang Yaoxing, a Taoist master, criticized them for the affair, asking "How laughable to cling to your amorous desires when the world has been turned upside down?" (or: "When there are such tremendous changes, you still indulge in love?"). This gives them both a realization. Li Xiangjun thus becomes a nun, while Hou Fangyu follows her step to become a Taoist priest. Cyril Birch, who collaborated on a University of California Press translation of The Peach Blossom Fan, wrote that "There can be no happy ending, given the historical authenticity of the action".
Like other Southern-style plays the play incorporates martial scenes and a love affair central to the plot. Birch wrote that the Hou Fangyu-Fragrant Princess love affair "is brilliantly integrated with the more weighty matter of the plot" and that the martial scenes "perfectly reflect the unhappy progress of the Ming cause and depict in vivid terms the gallant but ultimately futile loyalty or generals like Huang Te-kung and Shih-K'o-fa."


== Characters ==

The play involves 30 dramatis personae. Kong Shangren assigned them to five departments. The principal four are the left department (C: 左部, P: zuǒ bu), the right department (C: 右部, P: yòu bu), the odd department (C: 奇部, P: jī bu), the even department (C: 偶部, P: ǒu bu), and the fifth department. These five departments may be re-organized as the"Se Bu" (Color department 色部), "Qi Bu" (Energy department 氣部) and "Zong Bu" (Overall department 總部).
The characters in the left and right departments represent colors or positive modes (C: 色, P: sè). Hou Fangyu represents the left department and Li Xiangjun]represents the right department. The left department includes male characters and the right department includes female characters. These characters express the sentiment of separation and reunion, so therefore they are positive characters. The left and right departments are divided into "Zheng Se" (Main Colour正色), "Jian Se" (Neutral Colour 間色), "He Se" (Combinative Colour 合色), and "Run Se" (Supporting Colour 潤色) four sections.
The characters in the odd department, headed by Shi Kefa, and the even department, headed by Zuo Liangyu, are represented in negative moods (T: 氣, S: 气, P: qì) because the characters are participants in political conflicts that create and destroy dynasties, so therefore the odd department characters are negative. "Qi Bu", with a total of 12 characters, perform mostly on the rise and fall of history. It has two parts, the "Ji Bu" (Odd department 奇部) and the "Ou Bu" (Even department 偶部), where the later one consists of "Zhong Qi" (Neutural energy 中氣), "Li Qi" (Criminal energy 戾氣), "Yu Qi" (Residual energy 餘氣) and "Sha Qi" (Evil energy 煞氣).
The other 2 characters in "Zong Bu", Taoist warp star (經星) Zhang Yaoxing (張瑶星) and weft star (緯星) who is the master of ceremonies in Nanjing Tai Chang Temple, runs through the whole play intending to introduce the backgrounds and other complements.
Kong Shangren used the yin and yang principle in organizing his characters.
The protagonists are historical figures. Like many southern Chinese (Yangtze Valley) plays, there are contrasting character groupings. Hou Fangyu and his friends are in one grouping, while the Ma Shiying and Ruan Dacheng group forms an opposing grouping. Each role-type may control a set of characters. The "painted-face" (P: jing, W: ching) role controls Ma Shiying, Liu Liangzuo, Su Kunsheng, and Zhang Yanzhu. The "comic" (P: chou, W: ch'ou) role type controls Liu Jingting, Cai Yisuo, Zhen Tuoniang, and several attendants and servants.
Birch wrote that the audience is "led to a deep respect for Hou Fang-yü, Liu Ching-t'ing, and Shih K'o-fa, as in their different ways they follow their doomed ideals."


=== Character list ===
(in order of appearance)
The Master of Ceremonies of the Imperial Temple in Nanjing. He states that The Peach Blossom Fan "employs the emotions entailed by separation and union, to depict feelings about rise and fall." (T: 借離合之情，寫興亡之感,[...], S: 借离合之情，写兴亡之感,[...])
C. H. Wang wrote that the Master "seems to impersonate" Kong Shangren.
Hou Fangyu (C: 侯方域, P: Hóu Fāngyù, W: Hou Fang-yü), a young scholar of distinction
Hou Fangyu opposes corrupt officials who sell out to Manchus and is a loyalist to the Ming cause.
Chen Zhenhui (Ch'en Chen-hui), fellow member of the Revival ClubWu Yingji (Wu Ying-chi), fellow member of the Revival ClubLiu Jingting (C: 柳敬亭, P: Liǔ Jìngtíng, W: Liu Ching-t'ing), a veteran minstrel of renown
Lianche Tu Fang, an author on an encyclopedia article about Liu Jingting, wrote that the person was one of two people used in the story to "bring together the various incidents of the plot."
Li Zhenli (Li Chen-li), proprietress of an elegant house of pleasure and foster mother of the heroineYang Wencong (Yang Wen-ts'ung), painter, poet, and officialLi Xiangjun (C: 李香君, P: Lǐ Xiāngjūn, W: Li Hsiang-chün), a courtesan and the heroine. Li Xiangjun, the Fragrant Princess, follows her desires on who to love and opposes bullies on the royal court.
Jin Fu, author of Chinese Theatre, wrote that "Although Li Xiangjun is a singer, her emotions and actions are shown to be more noble than those of the scholars."
Su Kunsheng (T: 蘇崑生, S: 苏昆生, P: Sū Kūnshēng, W: Su K'un-sheng), Li Xiangjun's singing teacher - Su Kunsheng asks Li Xiangjun to perform The Peony Pavilion.
Lianche Tu Fang, an author on an encyclopedia article about Liu Jingting, wrote that the person was one of two people used in the story to "bring together the various incidents of the plot."
Ruan Dacheng (T: 阮大鋮, S: 阮大铖, P: Ruǎn Dàchéng, W: Juan Ta-ch'eng), corrupt politician, dramatist and poetDing Jizhi (Ting Chi-chih), poet-musicianShen Gongxian (Shen Kung-hsien), poet-musicianZhang Yanzhu (Chang Yen-chu), poet-musicianBian Yujing (Pien Yü-ching), professional singing-girlKou Baimen (K'ou Pai-men), professional singing-girlZheng Tuoniang (Cheng T'o-niang), professional singing-girl
Cyril Birch wrote that Zheng Tuoniang is "an important female part" and that the role to "offset the demure elegance of the ingenue (tan) role, Fragrant Princess", is one of the "major functions" of Zheng Tuoniang. Birch wrote that "We can imagine her as conspicuously ugly with her tart's makeup, lewd gestures, and regular caterwaul of a singing voice".
General Zuo Liangyu (C: 左良玉, P: Zuǒ Liángyù, W: Tso Liang-yü), commander of the Wu Chang garrison
General Shi Kefa (C: 史可法, P: Shǐ Kéfǎ, W: Shih K'o-fa), President of the Board of War at Nanjing
Birch states that Shi Kefa is a general who has a "gallant but ultimately futile loyalty".
Ma Shiying (T: 馬士英, S: 马士英, P: Mǎ Shìyīng, W: Ma Shih-ying), Governor of Feng Yang and Grand Secretary
General Yuan Jixian (Yüan Chih-hsien)
General Huang Degong (Huang Te-kung)
Birch states that Huang Degong is a general who has a "gallant but ultimately futile loyalty".
Emperor Hong Guang (Emperor Hung-kuang)
General Liu Zeqing (Liu Tse-ch'ing)
General Gao Jie (Kao Chieh)
General Liu Liangzuo (Liu Liang-tso)Lan Ying (T: 藍 瑛, S: 蓝 瑛, P: Lán Yīng, W: Lan Ying), a famous painterCai Yisuo (C: 蔡益所, P: Cài Yìsuǒ, W: Ts'ai Yi-so), a Nanjing booksellerZhang Wei (T: 张 薇, S: 张 薇, P: Zhāng Wēi, W: Chang Wei) or Zhang the Taoist (T: 張瑤星, S: 张瑶星, P: Zhāng yáoxīng), former commander of the Imperial Guard in BeijingHuang Shu (Huang Shu), Inspector GeneralTian Xiong (T'ien Hsiung), adjutant to General Huang DegongHan Zanzhou (Hsu Ch'ing-chün), a magistrate's runner


== Analysis ==
Cyril Birch wrote that "The world of The Peach Blossom Fan is that late-Ming world of gross corruption, of callousness and cowardice and the breakdown of a long-cherished order. Yet the quality of life revealed in the play is of extraordinary cultivation and sensibility. There is a great poignancy in this contrast". C. H. Wang wrote that the play has an intertwining of the motifs of separation and union of people in love, and the motifs of the decline and ascent of political powers, and that "The parallel structure is not contained within a single plot only" but rather to the entire work.


== Creation and conception ==
C. H. Wang, author of "The Double Plot of T'ao-hua shan," wrote that the author "attempted in this work not only to retell for common theatre-goers a romantic love-story but also to arouse scholars-especially Confucian intellectuals-to consider why and how China so easily lost her strength in the national crises of 1644-45." The play was written fewer than 50 years after the fall of the Ming dynasty, during the reign of the Kangxi Emperor of the Qing dynasty.


== Stage performance and adaptations ==
As soon as Kong finished the script of The Peach Blossom Fan, it was lent out and spread quickly among scholars and aristocrats. In the autumn of the year Jimao, even the emperor sent servant to Kong's house, asking in haste for the complete script. In the next year, General Li Muan set up a therical troupe called Jin Dou to perform the play, which gained huge fame immediately. Each time the troupe performed, actors and actresses were given considerable tips.
The play was a particular favorite of the Kangxi Emperor.
Merchants in Yang Zhou once raised 160 thousand gold for the costume in the play.
During the last century, the play has been performed in forms of Peking Opera, Drama, Chu Opera, Gui Opera, Yue Opera, Xiang Opera, Min Opera, Bei Kun, Nan Kun and Huangmei Opera, and it has been adapted into 3 kinds of endings, including one that ends in a happy reunion.
In 1937, when the World War Two broke out, the famous Chinese playwright Ouyang Yuqian altered the ending of the play into "Having cut his hair, Hou surrendered to the Qing dynasty and served its royal court", satirizing the traitor Wang Jingwei of that time.
In 1964, playwright Mei Qian (梅阡) and Sun Jing (孫敬), using Ou's endding, put the drama into a movie script, starring famous actress Wang Fengdan and actor Feng Jie.


== Publication ==
The Peach Blossom Fan was printed during Kong Shangren's lifetime. Several variations in the text appear in subsequent editions of the play.
There was 1982 edition edited by Wang Chi-ssu and others, published in Beijing. The play is presented in four juan (chüan) rather than the standard two parts.


=== Translations ===
One edition published by the University of California Press was translated into English by Chen Shih-hsiang and Harold Acton, K.B.E. with Cyril Birch collaborating. Birch wrote that the University of California Press translation is "complete except for a very few places". Portions translated included what Birch described as "the contrasting low punning and bawdy badinage," the scholars' formal compliments and greetings, "high poetry" within the songs, and self-introduction speeches and soliloquies described by Birch as "sometimes rather stiff".
Acton wrote that he and Chen Shih-hsiang hoped that their translation would be published at some point but that they translated the play "for its own sake rather than for publication." Chen Shih-hsiang had been researching early Chinese poetry and Acton had suggested translating The Peach Blossom Fan. Chen Shih-hsiang died in May 1971. At that time there was a manuscript draft with all scenes except for the final seven translated.
Cyril Birch, who had worked with Chen Shih-hsiang at the University of California Berkeley, translated the final seven scenes and revised the drafts. As a guide Birch used the People's Literature Press edition published in 1959 in Beijing. He used the annotations written by Wang Chi-ssu (C: 王季思, P: Wáng Jìsī, W: Wang Chi-ssu) and Su Huan-chung.
Due to the usage of allusions common in plays in the Ming and Qing Dynasties the University of California translation uses footnotes for what Birch described as "many" of these allusions. Birch wrote "many more have been sacrificed to the interests of readability". Birch wrote that in the final scenes, if the closest translation "would have impossibly retarded the movement of the verse" Birch used paraphrasing to follow on the actions of Chen Shih-hsiang and Acton. Birch cited Scene 32 as an example of a place where the translation was abridged. There the Master of Ceremonies' speech's strings of instructions indicating commands such as "Kneel! Rise! Kneel!" were omitted. Birch wrote that "These commands, in performance, would punctuate an elaborate posturing dance, but they make for boring reading."


== Reception ==
Liang Qichao (1873–1929) wrote that this play was "a book of utmost desolation, poignant splendor, and utmost turmoil." He further wrote: "With the refined strictness of its structure, the magnificence of its style, and the depth of its sentiments, I would venture that Kong Shangren's Peach Blossom Fan surpasses the works of all epochs!"
Scholar Wang Guowei (1877–1927), who held the play in great esteem, compared it to the novel Dream of the Red Chamber.
Harold Acton, who co-wrote an English translation, stated that The Peach Blossom Fan is a "highly poetic chronicle play" that is "a vivid evocation of the downfall of the Ming dynasty" that "deserves to be better known to students of Chinese literature and history."
Dylan Suher of the literary magazine Asymptote described The Peach Blossom Fan as "The greatest masterpiece of the literature of political disappointment", and the play contains "some of the most elegant Chinese ever written—a density of poetic expression that rivals Shakespeare's."
Several modern adpations of the play has also received acclaim. Kevin J. Wetmore reviewing the Edward Mast adpated and Chen Shi-Zheng directed version for Theatre Journal, describes it as "a powerfully moving, brilliantly theatrical, and playfully entertaining production."


== See also ==

The Peony Pavilion


== Notes ==


== References ==
Acton, Harold. "Preface". In: K'ung, Shang-jen. Translators: Chen, Shih-hsiang and Harold Acton. Collaborator: Birch, Cyril. The Peach Blossom Fan (T'ao-hua-shan). University of California Press, 1976. ISBN 0-520-02928-3.
Aoki, Masaru (J: 青木 正児 Aoki Masaru, Chinese: T: 青木 正兒, S: 青木 正儿, P: Qīngmù Zhèngér) (2010) [1930], Zhongguo jindai xiqushi 中国近代戏曲史 ["History of early modern Chinese musical plays"], Translated by: Wang, Gulu (T: 王古魯, S: 王古鲁, P: Wáng Gǔlǔ, W: Wang Ku-lu), Beijing: Zhonghua shuju 中华书局, ISBN 978-7-101-06444-5  CS1 maint: Multiple names: authors list (link).
Birch, Cyril. "Introduction: The Peach Blossom Fan as Southern Drama." In: K'ung, Shang-jen. Translators: Chen, Shih-hsiang and Harold Acton. Collaborator: Birch, Cyril. The Peach Blossom Fan (T'ao-hua-shan). University of California Press, 1976. ISBN 0-520-02928-3.
Fu, Jin. Chinese Theatre. Cambridge University Press, March 9, 2012. ISBN 0521186668, 9780521186667.
K'ung, Shang-jen. Translators: Chen, Shih-hsiang and Harold Acton. Collaborator: Birch, Cyril. The Peach Blossom Fan (T'ao-hua-shan). University of California Press, 1976. ISBN 0-520-02928-3.
Nienhauser, William H., ed. (1986), The Indiana Companion to Traditional Chinese Literature, Volume 1, Bloomington: Indiana University Press, ISBN 0-253-32983-3 .
Niu, Biao 钮骠, ed. (2004), Zhongguo xiqushi jiaocheng 中国戏曲史教程 ["Curriculum on the history of Chinese musical plays"], Beijing: Wenhua yishu chubanshe 文化艺术出版社, ISBN 7-5039-2572-8 .
Shen, Jing. Playwrights and Literary Games in Seventeenth-Century China: Plays by Tang Xianzu, Mei Dingzuo, Wu Bing, Li Yu, and Kong Shangren. Lexington Books, August 4, 2010. ISBN 073913857X, 9780739138571.
Tu Fang, Lianche. "LIU Ching-t'ing." In: Association for Asian Studies. Ming Biographical History Project Committee. Dictionary of Ming Biography, 1368-1644, Volume 1. Columbia University Press, 1976. p. 946-947. ISBN 0231038011, 9780231038010.
Wang, C. H. "The Double Plot of T'ao-hua shan." Journal of the American Oriental Society. Vol. 110, No. 1, January–March, 1990. p. 9-18. Available on JStor.
Zhang, Geng 张庚; Guo, Han 郭汉, eds. (1992), Zhongguo xiqu tongshi 中国戏曲通史 ["General history of Chinese musical plays"], Zhongguo xiqu chubanshe 中国戏曲出版社, ISBN 7-104-01967-7 .
Zhou, Yibai 周贻白, ed. (2004), Zhongguo xijushi changbian 中国戏剧史长编 ["Collected documents on the history of Chinese theater"], Shanghai: Shanghai shudian chubanshe 上海书店出版社, ISBN 7-80622-907-8 .


== Further reading ==
Duan, Li (S: 段 丽, P: Duàn Lí) (Nanjing University Chinese Department (南京大学中文系)). Cai Yisuo: a Minor Character in The Peach Blossom Fan (《桃花扇》中书客影). Root Exploration, 2008, Issue 5. December 16, 2008. doi： 10.3969/j.issn.1005-5258.2008.05.019. 机标分类号: I20 J80. Info page (Archive)
Owen, Stephen, "Kong Shang-ren, Peach Blossom Fan: Selected Acts," in Stephen Owen, ed. An Anthology of Chinese Literature: Beginnings to 1911. New York: W. W. Norton, 1997. p. 942-972 (Archive).
Japanese:
Aoki, Masaru (青木 正児 Aoki Masaru). 支那近世戯曲史 Shina kinse gikyoku shi. Koubundou Bookshop (弘文堂書房) (Tokyo), 1930. Book profile (Archive)


== External links ==
The peach blossom fan, by Kung Shang-jen (1648–1718), translated by Chen Shih-hsiang and Harold Action with the collaboration of Cyril Birch, University of California Press,
Kun Opera House of Jiangsu Performing Arts Group's Official Website, "The best" version of the peach blossom fan tonight
The Official Website of the 16th Asian Games, Opera Concert: Kunqu The Peach Blossom Fan (1699)