Óscar Alejandro Ulloa Alonzo (born August 19, 1986 in Metapán, El Salvador) is a Salvadoran football forward who currently plays for FAS in the Salvadoran Premier Division.


== Club career ==
Nicknamed Lagartito (Little Lizard), Ulloa came through the youth ranks at hometown club Isidro Metapán and made his senior debut in the 2005/2006 season. He joined San Salvador F.C. for the 2007 Clausura and moved to Once Municipal for the 2007 Apertura season. He returned to Metapán in 2008 and joined FAS in 2010.


== International career ==
Ulloa began his international career with El Salvador's U-21 national team in 2006. He took part in the Central American and Caribbean Games. Unfortunately, El Salvador failed to regain the gold medal they had won in Central American and Caribbean Games losing to Venezuela 2-1.
That same year Ulloa was also part of the El Salvador's U-23 national team .


== Personal life ==
Ulloa is the son of former Salvadoran national team player Óscar Lagarto Ulloa. His brother Ricardo Ulloa also plays for FAS.


== References ==


== External links ==
Profile - BDFA