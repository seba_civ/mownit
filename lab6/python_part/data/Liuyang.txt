Liuyang (simplified Chinese: 浏阳; traditional Chinese: 瀏陽; pinyin: Liúyáng) is a county-level city under the administration of Changsha, the capital of Hunan province, China. Liuyang is located in the northeast of Hunan province, in the east of Changsha City, and adjoins the province of Jiangxi. In 2010, Liuyang’s GDP is 55.677 billion RMB (8.225 billion dollars). Liuyang has an area of 5,007.75 square kilometers with a population of 1,407,104 (in 2010). There are 27 towns and 6 townships (in 2009). The government seat is in Guankou Subdistrict.


== History ==
According to historical relics discovered in Yong’an Middle School’s construction site in 1991, in the Old Stone Age, there were people living in Liuyang. In the eastern Han Dynasty (in 209 AD), Liuyang was established as a county, and its government was located in Juling (now the northern Guandu). In the Tang Dynasty, the government was moved to Huaichuan (now the Huaichuan Street), and in the Yuan Dynasty (in 1295 AD), the government was moved back to Juling, and Liuyang became a “state”. In the Ming Dynasty (in 1369 AD), Liuyang was degraded as a county, and its government was moved to Huaichuan again. In the eastern Han Dynasty, Sun Quan set Liuyang (刘阳). In the Three Kingdoms-Wu, Liuyang belonged to Changsha Prefecture. In the Liu Song period of Southern Dynasties, the name was changed from Liuyang (刘阳) to Liuyang (浏阳), and the new name is used till now. In the Sui Dynasty, Liuyang was merged into Changsha, and in the Tang Dynasty (in 708 AD) Liuyang was established as a county again. In the Sui Dynasty and the Tang Dynasty, Liuyang belonged to Tanzhou. In the Ming Dynasty and the Qing Dynasty, Liuyang belonged to Changsha. In the 26th reign year of the Republic of China (in 1937 AD), Liuyang belonged to the First District, Hunan Province Chief Inspector's Office. In 1949, the First District, Hunan Province Chief Inspector’s Office, was renamed as Changsha prefecture. In 1952, Changsha prefecture was replaced by Xiangtan prefecture, and Liuyang belonged to Xiangtan prefecture (later, it was called Xiangtan area). In February, 1983, Xiangtan area was cancelled, and Liuyang was attributed to Changsha city. On 16 January 1993, Liuyang was not a county any more, and it becomes a county-level city, called Liuyang city. In September 1927 Mao Zedong assembled Left-Kuomintang troops here, in the town of Wenjiashi(文家市) on the Jiangxi border, for the Com-Intern-planned seizure of Changsha, the provincial capital, 100 km to the west. Mao led the troops 170 km south into the Jinggang Mountains. Half a year later, he was joined there by the troops of Zhu De.


== Administrative divisions ==
The government seat is in Guankou Subdistrict. The county-level city is divided into the following township-level administrative divisions:


=== Towns ===

Shegang (社港)
Longfu (龙伏)
Chunkou (淳口)
Shashi (沙市)
Yong'an (永安)
Beisheng (北盛)
Zhangfang (张坊)
Chengtanjiang (澄潭江)
Zhonghe (中和) 
Wenjiashi (文家市)
Yonghe (永和) 
Guandu (官渡)
Gugang (古港)
Yanxi (沿溪)
Zhentou (镇头)
Taipingqiao (太平桥)
Puji (普迹)
Jingang (金刚)
Guanqiao (官桥)
Dayao (大瑶)
Daweishan (大围山)
Dahu (达浒)
Chengchong (枨冲)
Baijia (柏加)


=== Townships ===
Xijiang (溪江乡)
Qibaoshan (七宝山乡)
Guanqiao (官桥乡)
Gaoping (高坪乡)
Gejia(葛家乡)
Sankou (三口乡)
Dongyang (洞阳乡)
Yanghua (杨花乡)
Xiaohe (小河乡)
Jiaoxi (蕉溪乡)


== Geography ==
Liuyang is located in the northeast of Hunan province, and it is in the upstream regions of the Liuyang River. The geographical coordinates of Liuyang is 27°51′—28°34′ N， and 113°10′—114°15′ E. Liuyang covers an area of 5,007.75 square kilometers. There are 105.8 kilometers from east to west and 80.9 kilometers from north to south. Liuyang is surrounded by mountains and hills. There are 53% mountain lands, 25% hills, 21% plains around hills and 1% water in Liuyang. The terrain of Liuyang is that north-east is higher than south-west, and landscape types change because of a lot of rivers. Liuyang is in a subtropical monsoon climate zone. Annual average temperature is 17.3 ℃, and annual precipitation is 1562 mm. There are two national roads, G106 and G319. Besides, there are three main rivers, Liuyang River, Laodao River and Nanchuan River, which can be used as shipping lanes throughout the whole year. Liuyang lies in the south of Pingjiang county, Yueyang city, and in the east of Tonggu, Wanzai, Yichun and Shangli, Jiangxi province, and in the north of Liling and Zhuzhou, and in the east of Changsha county. Liuyang is about 50 kilometers far from Changsha city.


== Population ==
According to the sixth population census in 2010, the number of permanent resident population of Liuyang is 1,278,928, and the number of registered population is 1,407,104. In registered population, the agricultural population is 1, 252, 238, and the nonagricultural population is 154, 866. Natural population growth rate is 2.5 ‰. There are more than 300,000 Hakka, and their ancestors moved from Meizhou to Liuyang.


== Language ==
Traditionally, there are three dialect areas in Liuyang, Gan Dialect, new Xiang Dialect and Hakka. Gan Dialect is Jiangxi Dialect, and it is used in downtown, northern Liuyang, eastern Liuyang and southern Liuyang. This is the most popular dialect in Liuyang, and people may call it Liuyang Dialect. Besides, new Xiang Dialect is used in western Liuyang, like Puji, Zhentou, Baijia, Guanqiao, Gejia and Chengchong, and it is also used in some areas in northern Liuyang, like Beisheng and Yong’an. This kind of dialect is not totally the same as Changsha Dialect, but it is similar to it. As for Hakka, most people living in the east of Liuyang use this, and it is almost the same as Hakka in Meizhou area.


== Economy ==

Liuyang cultivates food crops, like rice. Main cash crops are flue-cured tobacco, medicinal materials, vegetables, oil-seed camellias, flowers and plants. Liuyang has been mining some natural resources, like sepiolite, chrysanthemum stone, coal and phosphorite. Industries include fireworks production, bio-pharmaceuticals and food processing. Liuyang fireworks have been in the global market for more than 100 years, and they were sold to Hong Kong, Macao and the Southeast Asia in the Guangxu period of the Qing dynasty, and fireworks become an important brand and pillar industry of Liuyang. Liuyang fireworks are protected as an original product and hold the honor of China's famous brand. In 2010, Liuyang’s GDP was up to 55.677 billion RMB (8.225 billion dollars). The added value of primary industry was 5.169 billion RMB (0.764 billion dollars). The added value of secondary industry was 37.905 billion RMB (5.599 billion dollars). The added value of tertiary industry was 12.603 billion RMB (1.862 billion dollars). The total value of out-put of Agriculture Animal Husbandry and Fishery was 8.006 billion RMB (1.183 billion dollars). In agriculture, except for grain production, the annual output of flue-cured tobacco was 131,670,000 tons. Liuyang’s total industrial output value was 90.289 billion RMB (13.338 billion dollars). There are 89 cooperates with a more than 100,000,000 RMB turnover. Total sales of fireworks industry is 12.43 billion RMB (1.84 billion dollars), and the revenue of fireworks industry is 0.97 billion RMB. The total industrial output value of Liuyang Biomedical Park is 17.082 billion RMB (2.523 billion dollars). The volume of export goods is 0.39 billion dollars. Liuyang’s general financial revenue is 3.324 billion RMB, including 29% fireworks' revenue. The per-capita disposable income of rural residents is 10,747 RMB (1,588 dollars), and the per-capita disposable income of city dwellers is 21,258 RMB (3,140 dollars).


=== Agriculture ===
Liuyang’s main grain crops are rice, and Liuyang is one of the top counties for food production in Hunan province. Main cash crops are flue-cured tobacco, vegetables, flowers and plants, and oil-seed camellias. Production of fruits, corns, bamboo shoots, phyllostachys pubescens, and medicinal materials also has a certain scale. In fish breeding and poultry feeding, except for traditional pigs raising, black goats and bees' raising are also competitive to some extent. Liuyang enjoys the honor as a city of flowers and plants' cultivation in China, and it is famous for Hongzhimu (a kind of plants). In regional distribution of agricultural industry, eastern Liuyang focus on fruits, and western Liuyang focus on flowers and plants, and northern Liuyang focus on tobacco, and these contribute to a grain, vegetables, flue-cured tobacco, flowers and plants-oriented agricultural economy. Liuyang is one of the top 100 counties that have advantages in grain production, and it is a big city for raising pigs and black goats. Vegetable production in Liuyang is up to 304,239 tons, and it is in the second place in Hunan province. The meat output is 80,450 tons, and it is in the 21st place in China. Liuyang runs scale operation, and the quantity of black goats is more than 700 thousand.


=== Fireworks industry ===
Liuyang fireworks have a history of more than 1,400 years. During the Northern Song Dynasty, Liuyang fireworks prospered greatly. During the Qing Dynasty (in 1723), Liuyang fireworks served as tribute. Since 1875, Liuyang fireworks have been exported to Japan, Korea, India, Iran, the UK, United States, Russia, etc. In 1990s, Liuyang's fireworks were the area's biggest export. However, in 1998, Liuyang fireworks went through a bottleneck period because of technological deficiencies. Since 1998, Liuyang has been reforming its industry. It is currently undergoing industrial consolidation and the upgrading of technology. Liuyang sets a ban on more than 10 thousand illegal factories, and Liuyang sets a standard to ensure the safety of fireworks production. Liuyang cooperates with some institutions of higher education and research institutes to invent fireworks with better technology and safety. In 8, August, 2001, Liuyang fireworks went public in Shanghai. In 2003, Liuyang succeeded in getting a fireworks display project in Brazil. Liuyang has invested more than 1 million dollars to buy lands in Brazil to build fireworks factories. There are more than 30 fireworks companies in Liuyang.


=== Biopharmaceutical industry ===
Liuyang Biomedical Park is the core of Changsha national biological industry base. Liuyang Biomedical Park was established in January 1998, and it is located in Dongyang town, Liuyang. It is the only medical professional development zone in Hunan province. It has an area of 28 square kilometres, and 8 square kilometres of them have been used. In 2005, the output was up to 3.5 billion RMB, and in 2010, the output was 17.082 billion RMB. In 2010, the finance and tax revenue was 0.52 billion RMB.


== Natural resources and special local products ==
The Daweishan Nature Reserve is home to the red-billed leiothrix and many other species. The Liuyang area is abundant in mineral deposits such as coal, tungsten, sulphur, phosphorus, and barite. There are four main special local products of Liuyang, glycinemax, grass cloth, chrysanthemum stone and fireworks.


== Culture and tourism ==
In 2010, Liuyang attracted 5.7 million tourists which brought an income of 3.5 billion RMB. Since Liuyang was established as a county in the Han dynasty, it is abundant in historical tourist resources. There are 2 national relic protection units, the Tan Sitong's Former Residence and site of the Autumn Harvest Uprising in Wenjiashi. There are 8 provincial culture and relics sites, like Xin’an Wind-Rain Bridge that was built in the Ming dynasty and the Liuyang Confucius Temple built in the Song dynasty. There are 13 Changsha city cultural relics’ protection units, like the Stone Frost Temple built in the Tang dynasty. There are 17 county cultural relics’ protection units, like Yaotou Mountain site and Luobei Mountain site.
The Ouyang Yuqian Grand Theater, named after Chinese dramatist Ouyang Yuqian, was founded in 2002 and is used for drama, musical and children's theater performances.
Major Buddhist Temples in Liuyang include Wenjin Temple (built in Tang dynasty) and Shishuang Temple. Major Taoist Temple include Yaowang Shengchong Temple. Major tourist destinations include Tan Sitong's Former Residence, Song Renqiong's Former Residence, Wang Zhen's Former Residence, Yang Yong's Former Residence, and Hu Yaobang's Former Residence.


== Notable people ==
Ouyang Xuan (欧阳玄)
Tan Sitong (谭嗣同)
Tang Caichang (唐才常)
Hu Yaobang, general secretary (1980–87) and chairman (1981–82) of the Chinese Communist Party (CCP)
Liu Zhaoxuan (刘兆玄)
Wang Zhen politician and general, who was an uncompromising hard-liner who used his position as vice president (1988–93) of China to promote Maoism
Xun Huaizhou (寻淮洲)
Song Renqiong, one of the Eight Immortals of the Communist Party of China
Yang Yong (杨勇)
Tang Liang (唐亮)
Li Zhimin (李志民)
Li Bai (李白)
Luo Zhanglong (罗章龙)
Jiao Dafeng (焦达峰)
Long Yun (龙云)
Peng Shiliang (彭士亮)
Peng Peiyun (彭佩云)
Zhou Qifeng (周其凤) President of Peking University
Zhang Chu (张楚)


== See also ==
West Lake Restaurant


== References ==


== External links ==
Liuyang official website