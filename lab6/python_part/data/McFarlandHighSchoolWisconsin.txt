McFarland High School is a high school located in McFarland, in Dane County, Wisconsin. Administered by the McFarland School District, it had an enrollment of 700 students in 2007. The school is accredited by the North Central Association of Colleges and Schools. The school's colors are Columbia blue, navy blue, and white and the mascot is the Spartan.


== Academics ==
McFarland High School offers eight AP (Advanced Placement) classes: calculus, physics, chemistry, psychology, composition, U.S. history, U.S. government, and European history. The school year is divided into four quarters, each about nine weeks long. McFarland High School uses block scheduling, with the school day consisting of four 85 minute blocks.


== Extracurricular activities ==
Clubs at the McFarland High School include:
Academic Decathlon
Ambassadors
Art Studio
Aud Squad
Chess
DECA
Drama
Gay–straight alliance
Model United Nations
Musical
National Honor Society
Newspaper (Spartan Spotlight)
Driftwood (literary publication)
Science Olympiad
Social Studies in Action
Spirit Dance Team
Student Council
T.A.D.A.
Tutoring Program
Yearbook
The 2007 marching band was composed of more than 200 musicians. Concert band, jazz band, choir, and vocal jazz are also offered at McFarland.


=== Athletics ===
Since the 2008-09 school year, McFarland teams have competed in the WIAA Rock Valley Conference. The swim team is an exception because the Rock Valley Conference has no swimming program.
In 2008-2009, the McFarland football team had a 9-0 record and won the Rock Valley Conference. The girls' tennis team and both cross country teams also won their respective Rock Valley Conference titles that season. The boys' swimming team has won six consecutive Division II state titles, from 2007 to 2012, and was the runner-up in 2003 and 2014. The boys' golf team earned the state title once and runner-up twice in the last five years. The team has been in the state tournament the last nine years, and won the sectional tournament three of the last four years. Girls' soccer took a second place finish in the 2007 state games. The McFarland boys' track team won the WIAA Division 2 regionals. Boys' soccer lost in the state semi-finals in 2013, the McFarland boys' first state appearance in school history.


== References ==


== External links ==
McFarland High School