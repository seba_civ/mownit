Justin Monahan (born September 1, 1989) is a professional American football wide receiver who is currently a free agent. He attended Portland State University and played for the school's NCAA Division I football team. Statistically, 2011 was Monahan's best season at Portland State, catching 52 passes for 707 yards and six touchdowns. He was an All-Big Sky Conference Honorable Mention that season. Over his three year career with the Vikings, Monahan had 112 receptions for 1,579 yards and six touchdowns.


== Early life ==
Monahan was born in West Linn, Oregon on September 1, 1989 to Joe and Theresa Monahan. Justin Monahan attended West Linn High School where he was a three-sport athlete in basketball, baseball and football. He earned all-state honors in baseball and football and was named a National Football Foundation Scholar–Athlete.


== College career ==
After his high school graduation he was admitted to Whitworth University in Spokane, Washington after graduation. He spent one redshirt season with the school's football team, the Pirates who were National Collegiate Athletic Association (NCAA) Division III members. During his sophomore season he transferred to Portland State University in Portland, Oregon where he played wide receiver as a walk-on for the Vikings football team over the next three years (2010–13). Monahan's high school coach knew the Vikings then-offensive coordinator Mouse Davis, which convinced him to go to Portland State over a walk-on offer with the Oregon Ducks. Monahan's best season statistically for the Vikings came in 2011 catching 52 passes for 707 yards and six touchdowns. He was an All-Big Sky Honorable Mention that season. Over his three year career with the Vikings, Monahan had 112 receptions for 1,579 yards and six touchdowns.


== Professional career ==
After failing to be drafted in 2013, Monahan signed with the Hamilton Tiger-Cats of the Canadian Football League (CFL), but never appeared in a game for them. He joined the inaugural AFL Portland Thunder roster in February 2014, which Portland's head coach Matthew Sauk telling The Oregonian, "It’s great to get more of that [Pacific] Northwest football talent [...] Justin has great hands and a great attitude—he’s ready to come in, play hard, and prove he can play professional football." According to the AFL, Monahan stands at 6 feet 1 inch (1.85 m) and weighs 202 pounds (92 kg).


== References ==