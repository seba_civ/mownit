Alternative Risk Transfer (often referred to as ART) is the use of techniques other than traditional insurance and reinsurance to provide risk bearing entities with coverage or protection. The field of alternative risk transfer grew out of a series of insurance capacity crises in the 1970s through 1990s that drove purchasers of traditional coverage to seek more robust ways to buy protection.
Most of these techniques permit investors in the capital markets to take a more direct role in providing insurance and reinsurance protection, and as such the broad field of alternative risk transfer is said to be bringing about a convergence of insurance and financial markets.


== Areas of activity ==
A major sector of alternative risk transfer activity is risk securitization including catastrophe bonds and reinsurance sidecars.
Standardization and trading of risk in non-indemnity form is another area of alternative risk transfer and includes industry loss warranties.
In addition, a number of approaches involve funding risk transfer, often within the structures of the traditional reinsurance market. Captive insurance companies are formed by firms and re/insurers to receive premiums that are generally held and invested as a "funded" layer of insurance for the parent company. Some captives purchase excess of loss reinsurance and offer coverage to third parties, sometimes to leverage their skills and sometimes for tax reasons. Financial reinsurance in various forms (finite, surplus relief, funded, etc.) consists of various approaches to reinsurance involving a very high level of prospective or retrospective premiums relative to the quantity of risk assumed. While such approaches involve "risk finance" as opposed to "risk transfer," they are still generally referred to under the heading of alternative risk transfer
Alternative risk transfer is often used to refer to activities through which reinsurers or insurers transform risks from the capital markets into insurance or reinsurance form. Such transformation can occur through the policy itself, or through the use of a transformer reinsure, a method important in credit risk markets, hard asset value coverage and weather markets. Reinsurers were notable participants in the early development of the synthetic CDO and weather derivative markets through such activities.
A subset of activities in which reinsurers take capital markets risks is dual-trigger or multiple trigger contracts. Such contracts exist between a protection buyer and a protection seller, and require that two or more events take place before a payment from the latter to the former is "triggered." For example, an oil company may desire protection against certain natural hazards, but may only need such protection if oil prices are low, in which case they would purchase a dual trigger derivative or re/insurance contract. There was a great deal of interest in such approaches in the late 1990s, and re/insurers worked to develop combined risk and enterprise risk insurance. Reliance Insurance extended this further and offered earnings insurance until the company suspended its own business operations. This area of alternative risk transfer activity diminished after the general hardening of the commercial insurance and reinsurance markets following the 9-11 terrorist attacks.
Another area of covergence is the emergence of pure insurance risk hedge funds, that function economically like fully collaterallized reinsurers and sometimes operate through reinsurance vehicles, but take the form of hedge funds.
Life insurance companies have developed a very extensive battery of alternative risk transfer approaches including life insurance securitization, full recourse reserve funding, funded letters of credit, surplus relief reinsurance, administrative reinsurance and related trechniques. Because life reinsurance is more "financial" to begin with, there is less separation between the conventional and alternative risk transfer markets than in the property & casualty sector.
Emerging areas of alternative risk transfer include intellectual property insurance, automobile insurance securitization and life settlements. It should be possible to adapt these instruments to other contexts. It has, for example, been suggested adapting cat bonds to the risks that large auditing firms face in cases asserting massive securities law damages.


== Key market participants ==
Banks; there are Wikipedia articles on: JPMorgan Chase, Goldman Sachs, Bank of America, and Citibank.
Insurers; there are Wikipedia articles on: AIG, Zurich, USAA, and XL Capital
Reinsurers; there are Wikipedia articles on: Munich Re, Hannover Re. and Swiss Re.
Brokers; there are Wikipedia articles on: Willis, Marsh, Aon Corporation
Consultants; there are Wikipedia articles on Towers Watson, Mercer (consulting firm), and Cavendish Consulting (consulting firm).
Sovereigns: Mexico is the only national sovereign to have issued cat bonds (in 2006, for hedging earthquake risk; in 2009, a multistructure instrument covering earthquake and hurricane risk).


== See also ==
Captive Insurance
Reinsurance
Finite Risk insurance
Dual trigger insurance
Weather derivatives
Risk financing
International Society of Catastrophe Managers


== Sources ==


== References ==
For extensive coverage of this space see Reactions Magazine, Benfield Quarterly, Insurance Insider. The key reference work for the space is "Alternative Risk Strategies" published by Risk magazine 2002
Captive & ART Review [2]
A monthly publication dedicated to Alternative Risk Transfer for the business community
Alternative Risk Strategies, published by Risk Books and edited by Morton Lane is a comprehensive though quite dated guide to the entire area of Alternative Risk Transfer (Risk Waters, London, 2002)


== External links ==
Artemis - The Alternative Risk Transfer Internet Portal