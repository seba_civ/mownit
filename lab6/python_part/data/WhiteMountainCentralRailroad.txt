The White Mountain Central Railroad is the short heritage railway at Clark's Trading Post in Lincoln, New Hampshire. It is notable as being one of the few places in New England with regular steam locomotive operation, as well as being a very rare example of a purpose-built tourist railroad (like those found in amusement parks and theme parks) that uses 4 ft 8 1⁄2 in (1,435 mm) standard gauge track instead of narrow gauge track.


== Route ==
The entrance building to Clark's Trading Post doubles as the train station. From there, the train leaves north through the park, and then past the small locomotive shop. The railroad crosses the Pemigewasset River on a covered bridge and then heads into a wooded area. In this section of the railroad, an actor playing a wild prospector named "the Wolfman" chases the train in a primitive car. Finally, the railroad goes under a ramp for Interstate 93 and terminates.


== Schedule and fare ==
The railroad operates whenever Clark's Trading Post is open, between late May and early October. There are between one and seven round trips per day, depending on the park's hours. Each trip is 30 minutes long, and the fare is included in the Trading Post admission price. During one weekend in September, Railroad Days are celebrated, with extra trains and special consists.


== History ==
Construction on the railroad began in 1955. The first train ride was on July 30, 1958. The covered bridge was moved from East Montpelier, Vermont, and was reconstructed in its current location between 1963 and 1965.


== Locomotive roster ==


== Station ==
The Clark's Trading Post station came from Freedomland U.S.A., after that park closed in 1964.


== References ==