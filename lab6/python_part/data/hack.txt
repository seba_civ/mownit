.hack (pronounced "dot-hack") is a Japanese multimedia franchise that encompasses two projects; Project .hack and .hack Conglomerate. Both projects were primarily created/developed by CyberConnect2, and published by Bandai. The series is mainly followed through the anime and video game installations, and has been adapted through manga, novels and other related media.


== Project .hack ==
Project .hack was the first project of the .hack series. It launched in 2002 with the anime series .hack//Sign in April 2002 and the PlayStation 2 game .hack//Infection in June 2002. Project developers included Koichi Mashimo (Bee Train), Kazunori Ito (Catfish), and Yoshiyuki Sadamoto, (Gainax). Since then, Project .hack has spanned television, video games, manga, and novels.


=== Games ===
.hack, a series of four PlayStation 2 games that follow the story of the .hackers, Kite and BlackRose, and their attempts to find out what caused the sudden coma of Kite's friend, Orca, and BlackRose's brother, Kazu. The four volumes, in sequence, are .hack//Infection, .hack//Mutation, .hack//Outbreak, and .hack//Quarantine.
.hack//fragment, the first .hack ORPG (online role-playing game). It was released in only Japan, and the servers have since been closed, ending all official online gameplay.
.hack//Enemy, a collectible card game created by Decipher Inc. based on the .hack series. It was discontinued after running five separate expansions between 2003 and 2005.


=== Anime ===
.hack//Sign is an anime television series directed by Kōichi Mashimo and produced by studio Bee Train and Bandai Visual. .hack//Sign consists of twenty six original episodes and three additional bonus episodes released as on DVD as original video animations (OVAs). The series focuses on a Wavemaster (magic user) named Tsukasa, a player character of a virtual-reality massively multiplayer online role-playing game (MMORPG) called The World. Tsukasa wakes up to find himself in a dungeon in The World, but he suffers from short-term memory loss as he wonders where he is and how he got there. The situation gets worse when he discovers he is unable to log out and is trapped in the game. From then on, along with other players Tsukasa embarks on a quest to figure out the truth behind his abnormal situation. The series is influenced by psychological and sociological subjects such as anxiety, escapism, and interpersonal relationships. The series premiered in Japan on TV Tokyo between April 4, 2002 and September 25, 2002. It was later broadcast across East Asia, Southeast Asia, South Asia, and Latin America by the anime television network, Animax; and across the United States, Nigeria, Canada, and United Kingdom by Cartoon Network, YTV, and AnimeCentral (English and Japanese) respectively. It is distributed across North America by Bandai Entertainment.
.hack//Legend of the Twilight is a twelve episode anime adaption of the manga series written by Tatsuya Hamazaki and drawn by Rei Izumi. The series was directed by Koichi Mashimo and Koji Sawai, and produced by Bee Train. Set in a fictional MMORPG, The World, the series focuses on twins Rena and Shugo, who receive chibi avatars in the design of the legendary .hackers known as Kite and BlackRose. After Shugo is given the Twilight Bracelet by a mysterious girl, the two embark on a quest to find Aura and unravel the mystery of the Twilight Bracelet. The anime series features many of the same characters as the manga version, but with an alternative storyline. It was incorrectly called .hack//Dusk, among other names, in early fan-translated versions.
.hack//Liminality is a set of four DVD OVAs included with the .hack video game series for the PlayStation 2. Liminality is focused on the real world as opposed to the games' MMORPG The World. Separated into four volumes; each volume was released with its corresponding game. The initial episode is 45 minutes long and each subsequent episode is 30 minutes long. The video series was directed by Koichi Mashimo, written by Kazunori Itō with music by Yuki Kajiura. Primary Animation production was handled by Mashimo's studio Bee Train which collaborated for the four games as well as handled major production on .hack//Sign. Liminality follows the story of Mai Minase, Yuki Aihara, Kyoko Tohno, and ex-CyberConnect employee Junichiro Tokuoka as they attempt to find out why players are falling into comas when playing in The World.
.hack//Gift, a self-deprecating, tongue-in-cheek, OVA that was created as a "gift" for those who had bought and completed all four .hack video games. It was released under Project .hack. In Japan, it was available when the Data Flag on the memory card file in .hack//Quarantine was present, whereas the American version included Gift on the fourth Liminality DVD. It is predominantly a comedy that makes fun of everything that developed throughout the series, even the franchise's own shortcomings. Character designs are deliberately simplistic.


=== Novels ===
.hack//AI buster, a novel released under Project .hack. It tells the story of Albireo and a prototype of the ultimate AI, Lycoris, and of how Orca and Balmung defeated "The One Sin" and became the Descendants of Fianna.
.hack//AI buster 2, a collection of stories released under Project .hack. It involves the characters of AI Buster and Legend of the Twilight Bracelet: ".hack//2nd Character", ".hack//Wotan's Spear", ".hack//Kamui", ".hack//Rumor" and ".hack//Firefly". "Rumor" was previously released with the Rena Special Pack in Japan.
.hack//Another Birth, series of novelizations released under Project .hack. It retells the story of the .hack video games from BlackRose's point of view.
.hack//Zero, a novel series released under Project .hack. It tells the story of a Long Arm named Carl, of what happened to Sora after he was trapped in The World by Morganna, and of Tsukasa's real life after being able to log out from The World.
.hack//Epitaph of Twilight, a novel series telling the story of Harald Hoerwick's niece, Lara Hoerwick, who finds herself trapped in an early version of The World.


=== Manga ===
.hack//Legend of the Twilight, a manga series released under Project .hack. It tells the story of two player characters Shugo and Rena, as they win a mysterious contest that earns them chibi character models of the legendary .hackers Kite and BlackRose (from the .hack PlayStation 2 games).


== .hack Conglomerate ==
.hack Conglomerate is the second and the current project of .hack by CyberConnect2 and various other companies. The companies include Victor Entertainment, Nippon Cultural Broadcasting, Bandai, TV Tokyo, Bee Train, and Kadokawa Shoten. It encompasses a series of three PlayStation 2 games called .hack//G.U., an anime series called .hack//Roots, prose, and manga.


=== Games ===
.hack//G.U. is a series of three video games (Vol. 1 Rebirth, Vol. 2 Reminisce, and Vol. 3 Redemption) released for the .hack Conglomerate project. It focuses on Haseo's search for a cure after his friend was attacked by a player known as Tri-edge, which led to his eventual involvement with Project G.U, and the mysterious AIDA who plague The World R:2. This latest series has proven greatly successful to the .hack franchise, with the recent release of .hack//G.U. Vol 3 in Japan selling over 100,000 copies in one day and becoming the number one game the week of its release in Japan.
.hack//Link, a PSP game released under the .hack Conglomerate project. It was claimed to be the last game in the series. Contains unplayable characters from .hack and .hack//G.U. video games.
.hack//Versus, a PS3 game released under the .hack Conglomerate project. The game is the first .hack fighter game, which is bundled with the film .hack//The Movie.
.hack//Guilty Dragon, a game for Android and iOS, it is exclusive for Japan. Its services began from October 15, 2012 and ended on March 23, 2016.
.hack//G.U. The Card Battle is a trading card game similar to that of .hack//Enemy released under the .hack Conglomerate project. Unlike .hack//Enemy, the game was made by the original creators of .hack//G.U.. There are two sets of rules, one based on the mini game in the G.U. series, Crimson VS, and the one specifically designed for the trading card game.
New World Vol. 1: Maiden of Silver Tears, a Android & iOS game released under the .hack Conglomerate project. it is a Japanese exclusive, it serves as a reboot to the franchise. It's services began on January 08, 2016.


=== Anime ===
.hack//Roots, an anime series released under the .hack Conglomerate project. It follows Haseo and his joining (and subsequent exploits with) the Twilight Brigade guild. It also shows his rise to power and how he becomes known as "The Terror of Death". Towards the end of the series we see the start of .hack.//G.U. This series is the last in the .hack anime series to be licensed by Bandai Entertainment.
.hack//G.U. Trilogy, a CGI video adaptation of the .hack//G.U. video games released under the .hack Conglomerate project.
.hack//G.U. Returner, a short follow up video and the conclusion to .hack//Roots released under the .hack Conglomerate project. It tells the story about the characters of .hack//G.U. in one last adventure.
.hack//Quantum, a three part OVA series from Kinema Citrus and the first in the anime series of .hack to be licensed by Funimation.
.hack//The Movie, a CGI movie, announced in August 23, 2011. On January 21, 2012, it was launched in theaters throughout Japan.
Thanatos Report, OVA in .hack//Versus unlocked after finishing Story Mode.


=== Novels ===
.hack//Cell, a novel released under the .hack Conglomerate project. .hack//CELL takes place at the same time as .hack//Roots. The main premise of the story covers the happenings that Midori and Adamas witness and experience in The World R:2, an extremely popular MMORPG that is a new version of the original game, The World. Midori meets numerous characters from .hack//Roots (most notably Haseo,) and .hack//G.U. (such as Silabus and Gaspard.) The main plot centers around Midori selling herself out to would-be PKers, and some real-world events that center around the girl who also bears the name Midori (Midori Shimomura) who is in a coma. It is later revealed that Midori is a sentient PC, a result of the "virtual cell" that was taken from Midori Shimomura's blood. After Midori Shimomura awakens from her coma, she enters The World R:2 with a PC identical to Midori. Tokyopop has obtained the rights to .hack//CELL and was released on March 2, 2010.
.hack//G.U., a novel adaptation of the three .hack//G.U. Video games released under the .hack Conglomerate project.
.hack//bullet, a web novel that follows Flugel after the events of .hack//Link.


=== Manga ===
.hack//4koma, a manga series which throws a little humor into both .hack and .hack//G.U. alike.
.hack//Alcor, a manga series released under the .hack Conglomerate project. It focuses on a girl called Nanase, who appears to be quite fond of Silabus, as well as Alkaid during her days as empress of the Demon Palace.
.hack//GnU, a humorous manga series released under the .hack Conglomerate project. It revolves around a male Blade Brandier called Raid and the seventh division of the Moon Tree guild.
.hack//G.U.+, a manga adaptation of the three .hack//G.U. video games released under the .hack Conglomerate project.
.hack//XXXX (read as "X-Fourth"), a manga series released under the .hack Conglomerate project. The manga adapts the four original .hack video games.
.hack//Link, a manga released under the .hack Conglomerate project. It occurs three years after the end of .hack//G.U. in a new version of The World called The World R:X. It focuses on a player named Tokio and a mysterious exchange student named Saika.


== Other appearances ==
A few characters from the franchise appear in the Nintendo 3DS games Project X Zone and Project X Zone 2.


== References ==


== External links ==
.hack// - Official (Worldwide)
.hack// - Official (Japanese)
Project .hack// - Official (Japanese)
.hack// Conglomerate - Official (Japanese)
.hack//Trilogy - Official (Japanese)