Juan Escalera (born March 27, 1961) is a Mexican luchador enmascarado (masked professional wrestler) better known as Octagón. He is best known for working for Asistencia Asesoría y Administración (AAA), having worked for the company since it was founded in 1992. In 2011 he was inducted into the promotion's Hall of Fame. Octagón formed a tag team with El Hijo Del Santo to take on Los Gringos Locos (Eddie Guerrero and Art Barr) in a double mask versus hair match on the first pay-per-view put on by a Mexican wrestling promotion to air in the United States. Over the years the Octagón gimmick has spawned a Mascot called Octagoncito and an "Evil clone" known as Pentagón. In 2014, Escalera quit AAA.


== Professional wrestling career ==
Growing up, Octagón was mainly interested in Martial Arts and earned a black belt in Shotokan Karate. He viewed Lucha Libre (professional wrestling) only as a hobby. This changed when he met Raúl Reyes, a former professional wrestler from the Veracruz area. Reyes convinced Octagón that his martial art skills could help him earn a living as a professional wrestler. After learning the basics, he made his debut in December 1981 as "Dragón Dorado" (Spanish for "Golden Dragon"). Not long after, he changed gimmicks (in-ring persona) and became known as "La Amenaza Elegante" (Spanish for "the Elegant Threat") in 1982. He was not very successful as La Amenaza Elegante, although he did manage to obtain a contract with Mexico's largest, and the world's oldest, wrestling promotion, Consejo Mundial de Lucha Libre (CMLL).


=== Creating Octagón ===
In late 1988 to early 1989, Octagón, along with CMLL booker Antonio Peña (the man in charge of writing the storylines that CMLL used), decided to create a new character that played off Octagón's martial arts background. The name was inspired by the movie The Octagon starring Chuck Norris. Together they created the concept of "Octagón", a Mexican Ninja complete with black clothes and a black and white mask that incorporated a red headband. The Octagón gimmick also played off previous Karate gimmicks in Lucha Libre such as Kung Fu and Kato Kung Lee. The Octagón persona was a hit with the fans, especially the younger fans earning Octagón the nickname "El ídolo de Los niños" (Spanish for the idol of the kids). Octagón became very involved in charities that benefited children, something else that helped make him popular with the younger fans. His popularity was reflected in CMLL's decision to give him a run with the Mexican National Middleweight Championship, defeating Emilio Charles, Jr. for the title on November 20, 1991.
Octagón began teaming with Atlantis, forming a duo so popular that they would go on to star in the movie La Fuerza bruta in 1991 and Octagón Y Atlantis; La Revancha (Octagón and Atlantis: The Revenge) in 1992. He also starred in another movie with Máscara Sagrada called Octagón y Mascara Sagrada, lucha a muerte ("Octagon and Mascara Sagrada in Fight to the Death" in the US). The team of Octagón, Atlantis and Mascara Sagrada was dubbed "Los Movie Stars" and were booked to win the Mexican National Trios Championship from a team called "Los Thundercats" (patterned after the ThunderCats animated series) in 1991. The team was allowed to remain champions only until August 11, 1991, where Los Capos (the team of Cien Caras, Mascara Año 2000 and Universo 2000) took over from them. Octagón's success both in the ring and on the movie screen was met with criticism from several established wrestlers such as Mil Mascaras and El Canek who referred to Octagón as "Muchachito" (the Spanish equivalence of "Little boy") in a derogatory tone.


=== Asistencia Asesoría y Administración ===
When Antonio Peña left CMLL and created Asistencia Asesoría y Administración (AAA) in 1992, Octagón was one of the CMLL wrestlers that left with Peña, staying loyal to the man that helped create his wrestling persona. In AAA, Octagón was quickly paired up with El Hijo del Santo to form what would become the top Face (good guy, referred to as "Técnico" in Lucha Libre) tag team of the promotion. The duo teamed with veteran Villano III at the inaugural Triplemania event to defeat Fuerza Guerrera, Heavy Metal and Rambo. The top face team of the company soon began to work a storyline with the top "bad guy" (called Heels, Rudos in Mexico).


==== Working with Los Gringos Locos ====
The team of Octagón and Hijo del Santo was matched against the group known as Los Gringos Locos, especially Eddy Guerrero and Love Machine who were the two main protagonists of the group. The storyline began in late 1993 and quickly had the two teams face off in a match designed to crown the first AAA / IWC Tag Team Champions. Octagón and El Hijo del Santo were chosen to be the first champions and on November 5, 1993, they defeated Guerrero and Love Machine. The storyline was soon expanded to see Octagón and El Hijo del Santo working with other Gringos Locos members such as Black Cat. On April 26, 1994, Octagón and Hijo del Santo teamed with Perro Aguayo to defeat Guerrero, Love Machine and Black Cat at Triplemania II-A. Four days after Triplemanía Octagón became a double champion as he regained the Mexican National Middleweight Championship from Blue Panther; Panther had brought the title with him when he jumped to AAA. Just over two weeks later, Octagón and El Hjio del Santo were chosen to team with Jushin Thunder Liger and Tiger Mask III, two Japanese wrestlers making a special appearance in Mexico. The four-man team won their match at Triplemania II-B against La Parka, Psicosis, Blue Panther, and Eddy Guerrero.
At the end of May 1994 Octagón lost the Mexican National Middleweight title, but not in the traditional way. Instead of losing the title in the ring, the storyline was that Octagón was too injured to compete in the third Triplemania of 1994 and thus the title was given to Blue Panther by default. It is not clear if AAA chose to handle the title change in this way because Octagón was legitimately injured, or if it was indeed part of the storyline. What is known is that Octagón was healthy enough to step into the ring on July 23, 1994 and lose the Tag Team titles to Guerrero and Love Machine. The match was mapped out so that the title change was not clean, Los Gringos Locos cheated to win the belts adding to the "heat" (the intensity of which the fans watched the storyline). The storyline between Los Gringos and Octagón and Hijo del Santo was one of the driving forces and main selling points of the first ever wrestling pay-per-view produced by a wrestling company in Mexico, When Worlds Collide. The match was not designed to be a title defense; instead the company put the biggest prize that a Mexican Wrestler can win on the line. They made it a "Luchas de Apuestas" match, a match where each participants bet either his hair or his mask. Originally it was planned to be the mask of El Hijo del Santo and the hair of Eddy Guerreo on the line in a singles match, with Octagón and Love Machine acting as seconds outside the ring. Some time before the show however, it became a tag team match in which Octagón and Santo bet their masks, and Guerrero and Love Machine bet their hair. The two teams produced a well received, highly regarded match that ended when El Hijo del Santo pinned Eddy Guerrero for the final fall. After the match, the crowd watched and applauded all four men as Guerrero and Love Machine had their hair shaved off. The match was given a five-star rating by Dave Meltzer of the Wrestling Observer Newsletter. Shortly after the show, Love Machine, (real name Art Barr) died from unknown causes which ended the storyline. The AAA Tag Team Championship was vacated after Barr's death, but AAA never crowned new champions.


==== Octagon clones ====
In the early 1990s, Antonio Peña created a mini version of Octagón called Octagoncito, and in 1995, Peña and AAA decided that it was time to once again cash in on Octagón, this time by creating a "Mortal Enemy" character for Octagón to work with. In May 1995, Pentagón was introduced, looking and acting very much like Octagón with only minor differences on the mask. Since he was masked it was easy for AAA to use a well known wrestler, who had played out his previous gimmick and marketability, to play the role. They chose a wrestler known as Espanto, Jr. to play the part, giving Octagón his next storyline. Initially the two faced off in six-man or eight-man tag team matches as a way to build tension for singles matches further along the storyline. Their first encounter came at Triplemania III-A, the first of AAA's major shows of 1995, when Octagon teamed with Konnan, Perro Aguayo and La Parka to defeat Cien Caras, Mascara Año 2000, Pentagon and Jerry Estrada although without Octagón being the one that won the match over Pentagón. One week later at Triplemania III-B Octagón reunited with El Hijo del Santo, who along with Rey Misterio, Jr. and La Parka defeated Pentagón, Blue Panther, Psicosis, and Fuerza Guerrera. Again Octagón and Pentagón were not involved in the finish of the match but faced off several times in the ring to further the storyline.
In early 1996, the man behind the Pentagón mask was forced to retire from wrestling due to his age and injuries; since he always wore a mask, AAA were able to replace the man with a new Pentagón, generally referred to as Pentagón II. Octagón and Pentagón continued to build their storyline, stretching out the conflict between them over years, even at times seeing the storyline turn so that Octagón and Pentagón would team up. In early 1997, Octagón began his third reign as Mexican National Middleweight champion, given to him so that Pentagón II could be seen a few month later cheating his way to winning the title from him, increasing the storyline tension between the two. While "Mask vs. Mask" matches were hinted at and talked about, nothing ever came of this between Octagón and Pentagón II. Pentagón II left AAA in 2001, halting the storyline without an actual ending. In order to create the payoff that the storyline had been building to over the years, AAA decided to introduce a new Pentagón, Pentagón III to take the place of his predecessor in the storyline. On June 5, 2002, Octagón finally met his "arch nemesis" in a match where both masks were on the line. Octagón got the ultimate revenge on Pentagón (at least according to Lucha Libre traditions) when he pinned Pentagón III forcing him to unmask and reveal his real name. The unmasking more or less put an end to the storyline between the two characters, with one final match between the two in 2004 where Octagón won the right to shave Pentagón III's hair off as well.
On June 20, 2003, Octagón teamed with La Parka to win the Mexican National Tag Championship from the team of Electroshock and Chessman. In 2006 Octagón added another title to his collection as he won the Mexican National Middleweight title for the fourth time. With the Mexican National titles not being defended very frequently Octagón still remains the holder of both belts to this day. In December 2008 AAA announced that they will no longer recognize or promote any title that does not belong to AAA ending the long standing practice. It is unclear if the Mexican National titles will be quietly forgotten or if Octagón will, at some later time, be stripped of the titles and then put to work in a different Mexican promotion.


==== Rudo turn ====
On June 7, 2011, AAA announced that Octagón would be inducted to the AAA Hall of Fame at Triplemanía XIX. After working as a technico continuously for his entire career, Octagón made his first rudo turn on October 9, 2011, at Héroes Inmortales by attacking Dr. Wagner, Jr. and aligning himself with La Sociedad, along with fellow longtime AAA defender, La Parka. On November 5, Octagón was named the new leader of La Sociedad subgroup La Milicia. On March 18, 2012, at Rey de Reyes, Octagón and La Milicia turned on La Parka and kicked him out of La Sociedad. On August 5 at Triplemanía XX, Octagón teamed with El Consejo in an eight-man tag team match, where they were defeated by La Parka and Los Psycho Circus, with Parka pinning Octagón for the win. Afterward, Octagón was attacked by El Consejo, which led to Parka saving his former partner. On October 7, at Héroes Inmortales, Octagón made peace with La Parka, officially ending his rudo turn.


==== Storyline with Octagón Jr. ====
On November 15, 2012, Octagón, once again a técnico, adopted Samuray del Sol as his new protégé, giving him a similar mask to the one he was wearing and renaming him Octagón Jr. On December 2 at Guerra de Titanes, La Sociedad responded to the debut of Octagón Jr. by debuting Pentagón Jr.. In the resulting match, Octagón, Octagón Jr. and La Parka defeated Pentagón Jr., the also debuting La Parka Negra and Silver King. After months of complaining about his status in AAA, Octagón announced he had parted ways with the promotion on March 2, 2014. According to Octagón, he had reached an agreement with AAA that allowed him to keep his ring name on the independent circuit.


=== Post-AAA ===
In December 2014, Octagón announced he was suing AAA for money he claimed the promotion owed him. When AAA introduced a second Octagón Jr. in March 2016, Octagón again threatened the promotion with a lawsuit. On April 10, 2016, Octagón appeared during a Octagón Jr. autograph signing and unmasked him. Three days later, Octagón introduced his own Octagón Jr. or Hijo de Octagón, who he claimed was his son. This claim was later disputed with sources stating that the new Hijo de Octagón was actually a Mexico City independent wrestler and not Octagón's biological son.


== Not to be mistaken for ==
Octagón shares many visible similarities to Japanese wrestler Masanori Murakawa, who wrestles as "the Great Sasuke", the main visible differences being patterns on their masks. Octagón made his debut with the mask design in 1989, before Masanori Murakawa adopted his Great Sasuke persona in the early 1990s.


== Filmography ==
In the early 1990s the Lucha Libre film genre saw a revival with several new films being produced, starring the new generation of Luchadors. Octagón played or starred in three movies in 1991 and 1992:
La Fuerza bruta (1991) (No English title known, probably only released in Mexico; Also starred wrestlers Atlantis and Emilio Charles, Jr.)
Lucha a muerte aka. Octagon y mascara sagrada, lucha a muerte (Octagon and Mascara Sagrada in Fight to the Death, 1992) (Also starring wrestler Mascara Sagrada)
Octagon y Atlantis, la revancha (Octagón and Atlantis: the Revenge, 1992) (Also starring wrestler Atlantis)


== Personal life ==
Octagón has claimed to have fathered several children, though the exact number of them is unknown. In April 2016, he claimed he had fathered three sons with an unnamed luchadora, though only two of them, a set of twins, were believed to actually be his biological children. He was previously known to have only fathered daughters.


== In wrestling ==
Finishing moves
La Escalera (Russian legsweep into an armbar submission)
La Jarocha (Spinning over his opponent's back into an arm drag)
Octagón Especial (Crucifix armbar with neckscissors)
Quebradora con Giro (Tilt-a-whirl backbreaker)

Nicknames
El Amo de los Ocho Ángulos (Spanish for The Lord of the Eight Angles)
El ídolo de Los Niños (Spanish for The Idol of the Kids)

Entrance themes
"War" by Vince DiCola transitioning into "La Bamba" by Ritchie Valens


== Championships and accomplishments ==
Asistencia Asesoría y Administración
AAA World Tag Team Championship (1 time) – with El Hijo del Santo
Mexican National Middleweight Championship (4 times)
Mexican National Tag Team Championship (1 time) – with La Parka
Mexican National Trios Championship (1 time) – with Rey Misterio, Jr. and Super Muñeco (1)
AAA Hall of Fame (Class of 2011)

Colosal
Copa Frontera (2014) - with Máscara Sagrada and L.A. Park

Consejo Mundial de Lucha Libre
Mexican National Trios Championship (1 time) – with Atlantis and Máscara Ságrada

Pro Wrestling Illustrated
PWI ranked him # 26 of the 100 best tag teams with El Hijo del Santo during the PWI Years in 2003.
PWI ranked him # 258 of the 500 best singles wrestlers during the PWI Years in 2003.
PWI ranked him # 50 of the 500 best singles wrestlers of the PWI 500 in 1999.

Wrestling Observer Newsletter awards
5 star match Los Gringos Locos (Eddie Guerrero and Art Barr) vs. El Hijo del Santo and Octagón (hair vs. masks; 2-out-of-3 Falls under elimination rules for each fall) – AAA When Worlds Collide on November 6, 1994


=== Luchas de Apuestas record ===


== Footnotes ==


== References ==


== External links ==

AAA profile
Octagón at the Internet Movie Database
Fuerza bruta, La at the Internet Movie Database
Lucha a muerte at the Internet Movie Database
Octagon y Atlantis, la revancha at the Internet Movie Database