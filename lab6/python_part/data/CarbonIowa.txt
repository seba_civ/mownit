Carbon is a city in Douglas Township, Adams County, Iowa, United States. The population was 34 at the 2010 census.
Note that other places in Iowa have been named Carbon. There was a Carbon Post Office in Davis County, 6 miles south of Ottumwa, that operated for less than a year in 1881. There was also a Carbon Post Office in Webster County, 3 miles south-east of Fort Dodge, that operated from 1870 to 1872. A post office on the latter location was known as Gypsum from 1902 to 1905.


== History ==
Carbon Iowa is located on the Nodaway River. This was home to the Pottawatomie, Oto and Iowa Indian tribes until the treaty of 1851 forced their removal to Kansas or Indian Territory now known as Oklahoma. The first settler to Carbon was Elijah Walters, who established a sawmill there in 1849. He reported seeing huts, old camp fires but stated that the tribes had moved on, only to see an occasional "red man" appear and look over old camp sites and burial sites then move on. Elijah built a mill on the Nodaway river that became a great social center of its time.
The village was originally named Walters, after its first settler, but was renamed Carbon after the discovery of coal. The town was established in 1873, but was not incorporated until 1903. A post office was established in Carbon in 1874, but from 1877 to 1880, the name was changed to Shinn.
By the time of the Civil War, there were small coal mines along the riverbanks near Carbon. In 1894, there were 9 coal mines in Carbon, of which 5 were in current use. Typical mine shafts were 80 feet deep, working a coal seam 12 to 20 inches (30 to 50 cm) thick. The town at that time was described as a mining camp. Unlike the vast majority of Iowa's coal camps, the United Mine Workers of America had no union local in Carbon between 1900 and 1912; during that period, most of Iowa's coal camps were organized, and the largest UMWA locals in the country were based in Buxton and Centerville, Iowa.
By 1908, it was estimated that the total coal production of the county had exceeded 350,000 tons, despite the lack of any rail connections. Most of this was from the Carbon area. Coal from the Carbon mines was sold locally, with some sold as far away as Corning and other towns within a 25-mile radius. The two most notable mines in the area in 1908 were the Jones, Smith and Tindall mine, a new mine employing 25 miners 1/4 mile south of Carbon and the J.F. Ruth mine, a year-old mine employing 30 miners 1/2 mile northeast of town. Both used horse whims to power their hoists, and had minimal safety features. At the time, one other mine had recently been closed by a cave-in, and another was just being opened.
The heyday of the Carbon coal mines lasted approx. 75 years beginning in the late 19th century and ending in the early 20th century. Carbon has always been known as a lawless town for those that like to fight. Many a gun fight did take place in the streets when the coal mines were booming. Until 1998 The only water supply was on what is now called the "pump road". On the edge this road sits an old hand pump. The pump has stood for over 100 years. Horse-drawn wagons would carry ice from the river and water from the pump back to town. Only one home within the Carbon city limits had a well. It was only in the late 1990s that rural water was finely piped in.


== Geography ==
Carbon is located at 41°3′1″N 94°49′31″W (41.050208, -94.825171). The town is located on the south bank of the Middle Nodaway River just east of its confluence with Bull Creek.
According to the United States Census Bureau, the city has a total area of 0.71 square miles (1.84 km2), all of it land.


== Demographics ==


=== 2010 census ===
As of the census of 2010, there were 34 people, 18 households, and 6 families residing in the city. The population density was 47.9 inhabitants per square mile (18.5/km2). There were 21 housing units at an average density of 29.6 per square mile (11.4/km2). The racial makeup of the city was 97.1% White and 2.9% Native American. Hispanic or Latino of any race were 2.9% of the population.
There were 18 households of which 11.1% had children under the age of 18 living with them, 22.2% were married couples living together, 5.6% had a female householder with no husband present, 5.6% had a male householder with no wife present, and 66.7% were non-families. 50.0% of all households were made up of individuals and 5.6% had someone living alone who was 65 years of age or older. The average household size was 1.89 and the average family size was 3.00.
The median age in the city was 50 years. 17.6% of residents were under the age of 18; 8.9% were between the ages of 18 and 24; 11.7% were from 25 to 44; 47.2% were from 45 to 64; and 14.7% were 65 years of age or older. The gender makeup of the city was 55.9% male and 44.1% female.


=== 2000 census ===
As of the census of 2000, there were 28 people, 15 households, and 4 families residing in the city. The population density was 39.5 people per square mile (15.2/km²). There were 28 housing units at an average density of 39.5 per square mile (15.2/km²). The racial makeup of the city was 100.00% White.
There were 15 households out of which 26.7% had children under the age of 18 living with them, 20.0% were married couples living together, 13.3% had a female householder with no husband present, and 66.7% were non-families. 46.7% of all households were made up of individuals and 33.3% had someone living alone who was 65 years of age or older. The average household size was 1.87 and the average family size was 2.80.
Age spread: 21.4% under the age of 18, 10.7% from 18 to 24, 25.0% from 25 to 44, 25.0% from 45 to 64, and 17.9% who were 65 years of age or older. The median age was 42 years.
The median income for a household in the city was $10,500, and the median income for a family was $0. Males had a median income of $8,750 versus $0 for females. The per capita income for the city was $9,891. There were no families and 45.5% of the population living below the poverty line, including no under eighteens and none of those over 64.


== References ==