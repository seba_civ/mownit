Blackenstein, also known as Black Frankenstein, is a low budget 1973 blaxploitation horror film loosely based on Mary Shelley's Frankenstein. It was made in an attempt to cash in on the success of Blacula, released the previous year by American International Pictures. However, Blackenstein fared poorly in comparison to its predecessor, with most reviews agreeing that the movie was "a totally inept mixture of the worst horror and blaxploitation films".


== Plot ==
Big and burly African-American soldier Eddie Turner (Joe De Sue) stepped on a land mine while serving in Vietnam and lost both arms and legs. His physicist fiancee Doctor Winifred Walker (Ivory Stone) thinks she's found help for him in her white former teacher and colleague Doctor Stein (John Hart) who has recently won a Nobel Peace Prize for "solving the DNA genetic code".
In a tour of Doctor Stein's castle-like Los Angeles home, Winifred is introduced to his other patients: the ninety-year-old Eleanor who looks to be only fifty (Andrea King) thanks to Stein's treatments, and the bald Bruno (Nick Bolan) whose lower legs have been successfully re-attached via "laser beam fusion" and Stein's "DNA solution".
Winifred is startled when she sees one of Bruno's legs is tiger-striped, which Doctor Stein attributes to "an unknown RNA problem" which he hopes to correct during the course of treatment. His sinister black assistant Malcomb (Roosevelt Jackson) seems overly interested in her reaction to this sight and in her in general.
Meanwhile, the stoically suffering Eddie is being verbally abused by an obnoxious white orderly (Bob Brophy) at the local Veteran's Hospital. When Doctors Stein and Walker arrive to ask if he'd be interested in submitting to experimental limb transplant surgery that could correct his condition, he consents.
Doctor Stein gives Eddie new replacement arms using his DNA solution, and Eddie seems to be recovering well until Malcomb confesses his attraction to Winifred. Winifed tries to let him down gently, explaining that she intends to marry Eddie as soon as the surgeries are complete, and Malcomb seems to accept her statement, but he later vindictively sabotages the DNA solution used during Eddie's leg surgeries with the contaminated RNA, causing the former soldier to start to devolve into a primitive brutish state with hairy hands and a Neanderthal brow ridge.
As his condition worsens and he loses the mental capacity for speech and rational thought, the stony-faced Eddie becomes a slowly shambling monster resembling an African-American version of the iconic Boris Karloff monster with a squarish afro instead of the usual scars and neck bolts.
Although he lies in a near catatonic state by day, compelled by horrible cannibalistic urges the black suit and turtleneck-clad Eddie secretly leaves the house late each night in search of victims who he dismembers, disembowels and devours zombie-style, always returning in time each morning for his ongoing schedule of DNA injections with his doctors none the wiser.
Two police detectives visit Doctor Stein as the body count starts to rise (their suspicions aroused due to the fact that all the killings took place in the surrounding vicinity and that the abusive hospital orderly was the vengeful Eddie's first victim), but Stein is ignorant of the fact that there is now a murderous monster living in his basement laboratory. Winifred however has become suspicious of Malcomb and spends her time in the lab, examining the various solutions used during Eddie's surgery.
One night, returning from his usual senseless rampage, Eddie hears screaming coming from Winifred's room. He enters to find Malcomb at her bedside and interrupts the attempted rape. Malcomb grabs a gun and empties it into the unaffected Eddie as Winifred flees. Eddie strangles Malcomb and then goes on to kill Bruno and Eleanor, the latter aging rapidly as she dies.
Doctor Stein meets Winifred on the stairs, where she tells him Eddie is the monster. Together they down run to the lab.
Winifred busies herself preparing an injection of the DNA solution that she hopes will cure Eddie. When Eddie draws near, he seems moved by her terror and backs away, perhaps dimly remembering that she is his fiancee. Doctor Stein however attacks him from behind, provoking a violent response. After a brief tussle with his creator that ends with Stein being fatally knocked into the high voltage electrical equipment, Eddie leaves the house.
The police arrive too late to stop Eddie but discover Doctor Stein's body and console Winifred. Eddie finds a brunette attempting to start a Jeep and spends several long minutes chasing her around an empty industrial warehouse. The police call in the Los Angeles County Canine Corps, and the Dobermans surround Eddie, knock him to the ground and, with a fittingly macabre irony, viciously tear the monster to pieces in the same way he killed his victims.


== Cast ==
John Hart as Dr. Stein
Ivory Stone as Dr. Winifred Walker
Joe De Sue as Eddie Turner
Roosevelt Jackson as Malcomb
Andrea King as Eleanor
Nick Bolin as Bruno Stragor
Karin Lind as Hospital Supervisor
Yvonne Robinson as Hospital Receptionist
Liz Renay as Blond Murder Victim


== Frank R. Saletri ==
According to director William A. Levey, non-actor Joe De Sue was cast in the title role because he was a client of criminal lawyer turned writer/producer Frank R. Saletri, as was celebrity cult icon Liz Renay.
Saletri also wrote, produced and directed the never-released Black the Ripper and wrote the screenplays for two unmade Sherlock Holmes movies, Sherlock Holmes in the Adventures of the Werewolf of the Baskervilles and Sherlock Holmes in the Adventures of the Golden Vampire which was to star Alice Cooper as Dracula.
In 1982 Saletri was found murdered "gangland style" in his home, a mansion formerly owned by Bela Lugosi.


== Trivia ==
Despite all the talk of DNA and laser surgery, the movie's laboratory set uses Kenneth Strickfaden's original sparking and zapping electrical equipment from the 1931 Frankenstein film.
Except for the soulful songs written and sung by Cardella DiMilo, the musical score consists of stock music taken from classical composers and old horror movies.
Surprisingly for a blaxploitation movie of this time period and despite its title, Blackenstein features little, if any, overt displays of racism, with even the angry tirade the white orderly directs toward the bedridden Eddie motivated more by bitter jealousy about not being able to join the army than any form of bigotry.
Several sequels were announced by various producers, including The Fall of the House of Blackenstein and Black Frankenstein Meets the White Werewolf, but were never made. The Return Of Blackenstein is not a sequel but merely a retitled re-release of the original film.
The Mexican lobby card for Blackenstein is actually "swiped" from the American poster for the 1965 Japanese kaiju flick Frankenstein Conquers the World with the title loincloth-clad monster repainted brown.


== Other Appearances ==
Blackenstein has appeared in skits on Saturday Night Live and on MADtv where he was re-dubbed "Funkenstein".
The movie was referenced in the 2003 South Korean film Save the Green Planet!.
Blackenstein is given a surprisingly serious scholarly examination as an aspect of a larger cultural perspective in Elizabeth Young's Black Frankenstein: The Making of an American Metaphor published in 2009 as part of the America and the Long 19th Century series from New York University Press.


== Home Video Release ==
Blackenstein was first released on video in the late '70s by Media Home Entertainment and again on DVD and VHS by Xenon Pictures in 2003.


== See also ==
List of American films of 1973
List of films featuring Frankenstein's monster


== References ==


== External links ==
Blackenstein at the Internet Movie Database
Blackenstein at AllMovie