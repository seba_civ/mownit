Edward M. Kopko is an American businessman and entrepreneur. He is CEO of Mercury Z, an innovation and engineering company.


== Early life and education ==
Kopko was born in Connecticut and resides in Ft. Lauderdale, Florida. He holds a B.A. degree in economics from the University of Connecticut and an M.A. in economics from Columbia University.


== Career ==
Kopko was the Chairman and Chief Executive Officer and Publisher of CE Group and Chief Executive Magazine.
Kopko was President and Chief Executive Officer of Butler international Inc. a worldwide provider of technical and technology services, from 1987 - March 2009.
Kopko is Editor of the 'Best and Worst States' web site which provides information and facts about the best and worst aspects of individual American states.
Kopko is a member of the Board of Trustees of the Foundation for Economic Education, a non-political, non-profit educational organization promoting individual liberty, private property, the free market and constitutionally limited government.
Kopko received the Ellis Island Medal of Honor in 1999 from the National Ethnic Coalition. Kopko received the Award of Merit for Corporate Responsibility and Volunteerism in 2001 from the Bergen Community College Foundation. Kopko served as a Trustee of the Committee for Economic Development, a nonprofit, nonpartisan, business-led public policy organization that delivers well-researched analysis and reasoned solutions to our nation’s most critical issues. Kopko served on the Board of Governors of Ramapo College from 1998-2002 and was a recipient of its Distinguished Citizens Award.
From 2001 until 2005, Kopko served as President of the Board of Trustees for the Helen Hayes Theatre Company, a not-for profit performing arts organization, based in Nyack, NY. From 2000 to 2005 Kopko owned "Pretty Penny" an historic Hudson River home which for 63 years had been the home of Helen Hayes, the late actress and "First Lady of the American Theater." Kopko purchased the home from Rosie O'Donnell, the American comedian, actress, author and television personality.


== Publications ==
Edward M Kopko and Jeffrey Sonnenfeld, "What's in A Leader", Forbes.com (August 7, 2007)
Edward M Kopko, "The CEO Serves: Moral Purpose and Business Leadership ", Acton Institute: Religion and Liberty (Fall 2007)
Edward M Kopko, "Debate Over: Job Creating CEO's Prefer McCain", Chief Executive (September/October 2008)
Edward M Kopko and JP Donlon, "Jobs Will Be Hard To Come By For a While", Final Word, Chief Executive Magazine (July/August 2008)
Edward M Kopko,"What Michigan Needs to Get its Job Engine Going", Detroit Free Press (September 2008) and Michigan Forward Magazine (Nov/Dec 2008,Pages 12–13)
Edward M Kopko and William J Holstein, "Spitzer's Climate of Fear", Wall Street Journal (November 23, 2004,Page B2)
Edward M Kopko, "Selecting the Best State", Chief Executive Magazine (May/June 2010)


== References ==