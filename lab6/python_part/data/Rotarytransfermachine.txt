A rotary transfer machine is a machine tool, typically for metal working by machining, comprising a large indexing table with machining stations surrounding the table. Such rotary transfer machines are used for producing a large lot of parts in fairly short cycle times.


== Operation ==
In rotary transfer machines, the workpieces are located and clamped in pallet type fixtures that are indexed in a circular path. During one cycle, sequential machining operations are performed simultaneously on the workpieces. The indexed table turns vertically or horizontally, and its movement could be continuous or intermittent. As the indexing table turns, the subsequent machining operation is repeated on the workpiece which was just machined by the previous station. This design combines automated part feed with simultaneous operations, enabling rapid completion of parts.


== Applications ==
Rotary transfer machines are commonly used for mass-production of metal parts in the automotive industry and for pneumatic and hydraulic fittings. The parts can range from simple to complex, depending on the layout of the machining tool, which is often custom designed for manufacturing of a single part or family of parts. Rotary arrangement presents a compact arrangement which saves floor space. Annual production capacity of one rotary transfer machine can range from 100'000 units to tens of millions of units.