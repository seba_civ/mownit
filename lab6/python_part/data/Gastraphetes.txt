The gastraphetes (from Ancient Greek γαστραφέτης, English translation: "belly-releaser") was a hand-held crossbow used by the Ancient Greeks. It was described in the 1st century AD by the Greek author Heron of Alexandria in his work Belopoeica, which draws on an earlier account of the famous Greek engineer Ctesibius (fl. 285–222 BC). Heron identifies the gastraphetes as the forerunner of the later catapult, which places its invention some unknown time prior to c. 420 BC.
Unlike later Roman and medieval crossbows, spanning the weapon was not done by pulling up the string, but by pushing down an elaborate slider mechanism.


== Description ==

A fairly detailed description and drawing of the gastraphetes appears in Heron's Belopoeica (Ancient Greek Βελοποιικά, English translation: On arrow-making), drawn from the account by the 3rd-century BC engineer Ctesibius. The weapon was powered by a composite bow. It was cocked by resting the stomach in a concavity at the rear of the stock and pressing down with all strength. In this way considerably more energy can be summoned up than by using only one arm of the archer as in the hand-bow.
There are no attestations through pictures or archaeological finds, but the description by Heron is detailed enough to have allowed modern reconstructions to be made. According to some authors, the dimensions of the gastraphetes may have involved some kind of prop.
A larger version of the gastraphetes were the oxybeles, which were used in siege warfare. These were later supplanted by the early ballistae that later also developed into smaller versions supplanting also the gastraphetes.


== Date ==

According to a long dominant view expressed by E. W. Marsden, the gastraphetes was invented in 399 BC by a team of Greek craftsmen assembled by the tyrant Dionysius I of Syracuse. However, recent scholarship has pointed out that the historian Diodorus Siculus (fl. 1st century BC) actually did not mention the gastraphetes, but was referring to the invention of the "katapeltikon", a mechanical arrow firing catapult. Since Heron states in his Belopoeica that stand-mounted mechanical artillery such as the katapeltikon was inspired by the earlier hand-held gastraphetes, the invention of handheld crossbows into Greek warfare must have thus occurred some unknown time before 399 BC.
The terminus ante quem may be more precisely defined as being before 421 BC, since another Greek author, Biton (fl. 2nd century BC), whose reliability has been positively reevaluated by recent scholarship, credits two advanced forms of the gastraphetes to a certain Zopyros. This Zopyros was probably a Pythagorean engineer from southern Italy. He may have designed his stand-mounted bow-machines on the occasion of the sieges of Cumae and Milet between 421 BC and 401 BC, thus marking the date by which the archetypical gastraphetes must have already been known.
It is believed that the gastraphetes was one of the many effective siege weapons used by Alexander the Great as he besieged Tyre. Aside from that event, he may have utilized the great composite crossbow in his other battles.


== Other ancient crossbows ==

Besides the gastraphetes, the ancient world knew a variety of mechanical hand-held weapons similar to the later medieval crossbow. The exact terminology is a subject of continuing scholarly debate.
Greek and Roman authors like Vegetius (fl. 4th century AD) note repeatedly the use of arrow firing weapons such as arcuballista and manuballista respectively cheiroballistra. While most scholars agree that one or more of these terms refer to handheld mechanical weapons, there is disagreement about whether these were flexion bows or torsion powered like the recent Xanten find.
The Roman commander Arrian (c. 86 – after 146 AD) records in his Tactica Roman cavalry training for firing some mechanical handheld weapon from horseback.
Sculptural reliefs from Roman Gaul depict the use of crossbows in hunting scenes. Dating to the 1st–2nd century AD, the specimens are remarkably similar to the later medieval crossbow, including the typical nut lock. From their reflexible shape they were composite bows.


== References ==
Notes

Bibliography
Baatz, Dietwulf (1994), "Die römische Jagdarmbrust", Bauten und Katapulte des römischen Heeres, Stuttgart: Franz Steiner Verlag, pp. 284–293, ISBN 3-515-06566-0 
Baatz, Dietwulf (1999), "Katapulte und mechanische Handwaffen des spätrömischen Heeres", Journal of Roman Military Equipment Studies 10: 5–19 
de Camp, L. Sprague (1961), "Master Gunner Apollonios", Technology and Culture 2 (3): 240–244, doi:10.2307/3101024 
Campbell, Duncan (1986), "Auxiliary Artillery Revisited", Bonner Jahrbücher 186: 117–132 
Campbell, Duncan (2003), Greek and Roman Artillery 399 BC-AD 363, Oxford: Osprey Publishing, ISBN 1-84176-634-8 
Hacker, Barton C. (1968), "Greek Catapults and Catapult Technology: Science, Technology, and War in the Ancient World", Technology and Culture 9 (1): 34–50, doi:10.2307/3102042 
Lewis, M. J. T. (1999), "When was Biton?", Mnemosyne 52 (2): 159–168, doi:10.1163/1568525991528860 
Marsden, E. W. (1969), Greek and Roman artillery. Historical development, Oxford: Clarendon 
Ober, Josiah (1987), "Early Artillery Towers: Messenia, Boiotia, Attica, Megarid", American Journal of Archaeology 91 (4): 569–604 
Schellenberg, Hans Michael (2006), "Diodor von Sizilien 14,42,1 und die Erfindung der Artillerie im Mittelmeerraum" (PDF), Frankfurter Elektronische Rundschau zur Altertumskunde 3: 14–23 
Further reading
Diels, H.; Schramm, E. (eds.): "Herons 'Belopoiika'", (Abhandlungen der preussischen Akademie der Wissenschaften, Philosoph.-hist. Kl. 2.) Berlin: Reimer, 1918, Chapter 7
Schellenberg, H. M.: "Anmerkungen zu Heron von Alexandria und seinem Werk über den Geschützbau", in: Schellenberg, H.M. / Hirschmann, V. E./ Krieckhaus, A. (eds.): "A Roman Miscellany. Essays in Honour of Anthony R. Birley on his Seventieth Birthday", Gdansk 2008, pp. 92–130


== External links ==
Ancient Greek Artillery Technology
Reconstructions and Plans of Greek and Roman Artillery