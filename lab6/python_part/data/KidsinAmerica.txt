"Kids in America" is a song recorded by British singer Kim Wilde. It was released in the United Kingdom as her debut single in January 1981, and in the United States in spring 1982. It has been covered by many artists from different genres.


== Background ==
The song was recorded in 1980 after RAK Records boss Mickie Most heard Wilde singing on a backing track for her brother Ricky Wilde. He liked her voice and image and expressed an interest to work with her. Ricky Wilde, together with his father Marty Wilde, a pre-Beatles British rock and roll star, wrote the song "Kids in America" for Wilde to record. Once the demo was sent, Most remixed the track himself and released it on RAK as Wilde's first single in January 1981. The synthesizer featured at the start of the recording is the WASP synthesizer, which was owned by Ricky Wilde at the time and inspired the writing of the song.


=== 1994 version ===
"Kids in America 1994" was released in May 1994 in order to help promote Wilde's compilation album The Remix Collection. Although it was intended to be released in the UK, for reasons unknown these plans were cancelled at the last minute. However, the track was released in other countries in several remixed forms using Wilde's original vocals from 1981. The "radio version" of the track was remixed by Cappella, with James Stevenson on guitar.


=== 2006 version ===
Among some of her other classic hits, Wilde recorded a new version of the song for her 2006 comeback album Never Say Never, featuring English singer Charlotte Hatherley. This version, like the rest of the album, was produced by German producer Uwe Fahrenkrog-Petersen, whom she had previously worked with in 2002 for German singer Nena's 20th anniversary album Nena feat. Nena on the track "Anyplace, Anywhere, Anytime", a new version of her 1984 hit single. Wilde contributed English verses to the song, which was slightly remixed and released as a single in 2003.
In 1995, The Muffs' cover of "Kids In America" was featured on the soundtrack for the 1995 film Clueless, which eventually won an RIAA award for Platinum level sales. It was later reissued on the Muffs' 2000 album Hamburger. The song is also used in Rock Band 2.
A version of the song by Alexa Brevig was used in a 2010 commercial for Totino's pizza and pizza rolls.


== Reaction ==
The song peaked at number 2 in the United Kingdom. The following year it reached the top 30 on the Billboard Hot 100 hit in the United States and ranked as the 91st most successful song of 1982 on the Hot 100 year-end chart. In the summer of 1981, the track appeared on Wilde's self-titled debut album.
"Kids in America" was the song that signalled the start of Wilde's career. Her father and brother continued to write songs for her (with the latter also given production credits), although in later years Wilde herself and her brother would usually be credited with the songwriting.


== Track listing (1981/1982) ==


=== 7" (UK) RAK 327 (track listing same as most international 7" formats) ===
A. Kids in America (3:26)
B. Tuning In Tuning On (4:30)


=== 7" (US/Canada) EMI America PB-8110 ===
A. Kids In America (3:26)
B. You'll Never Be So Wrong (4:11)


=== 12" (Germany) RAK 052-64249 ===
A. Kids in America (3:26)
B. Tuning In Tuning On (4:30)


== Track listing (1994) ==


=== 12" (Netherlands) MCA MCT 31556 ===
Kids In America 1994 (Extension Mix)
Kids In America 1994 (House Mix)
Kids In America 1994 (Plus Staples)


=== 12" (UK) MCA SAM 29T (Promo) ===
Kids In America 1994 (Extension Mix)
Kids In America 1994 (X Club Dub)
Kids In America 1994 (Instrumental)
Kids In America 1994 (House Dub)
Kids In America 1994 (Plus Staples)
Kids In America 1994 (X Cut Cut)


=== CD Single (Germany) MCA MCD 31555 ===
Kids In America 1994 (Cappella Mix)
Kids In America 1994 (Extension Mix)
Kids In America


=== CD Single (Germany) MCA MCD 31926 ===
Kids In America 1994 (Cappella Mix)
Kids In America 1994 (Extension Mix)


=== CD Single (Japan) MCA MVCM 13010 ===
Kids In America 1994 (Cappella Mix)
Kids In America 1994 (Extension Mix)
Kids In America 1994 (House Mix)
Kids In America 1994 (Plus Staples)


== Charts ==


== Covers ==
The song has been covered by a wide range of artists, mostly from 1991 onwards. Notable bands include The Bloodhound Gang, Atomic Kitten, Cascada and Foo Fighters.
No Secrets covered the song for the 2001 film Jimmy Neutron: Boy Genius and was released as a single, and later appeared on their 2002 debut album No Secrets.


== References ==


== External links ==
Lyrics of this song at MetroLyrics