In economics and consumer theory, a linear utility function is a function of the form:

or, in vector form:

where:
 is the number of different goods in the economy.
 is a vector of size  that represents a bundle. The element  represents the amount of good  in the bundle.
 is a vector of size  that represents the subjective preferences of the consumer. The element  represents the relative value that the consumer assigns to good . If , this means that the consumer thinks that product  is totally worthless. The higher  is, the more valuable a unit of this product is for the consumer.
A consumer with a linear utility function has the following properties:
The preferences are strictly monotone: having a larger quantity of even a single good strictly increases the utility.
The preferences are weakly convex, but not strictly convex: a mix of two equivalent bundles is equivalent to the original bundles, but not better than it.
The marginal rate of substitution of all goods is constant. For every two goods :

.

The indifference curves are straight lines (when there are two goods) or hyperplanes (when there are more goods).
Each demand curve (demand as a function of price) is a step function: the consumer wants to buy zero units of a good whose utility/price ratio is below the maximum, and wants to buy as many units as possible of a good whose utility/price ratio is maximum.
The consumer regards the goods as perfect substitute goods.


== Economy with linear utilities ==
Define a linear economy as an exchange economy in which all agents have linear utility functions. A linear economy has several properties.
Assume that each agent  has an initial endowment . This is a vector of size  in which the element  represents the amount of good  that is initially owned by agent . Then, the initial utility of this agent is .
Suppose that the market prices are represented by a vector  - a vector of size  in which the element is the price of good . Then, the budget of agent  is . While this price vector is in effect, the agent can afford all and only the bundles  that satisfy the budget constraint: .


== Competitive equilibrium ==
A competitive equilibrium is a price vector and an allocation in which the demands of all agents are satisfied (the demand of each good equals its supply). In a linear economy, it consists of a price vector  and an allocation , giving each agent a bundle  such that:
 (the total amount of all goods is the same as in the initial allocation; no goods are produced or destroyed).
For every agent , its allocation  maximizes the utility of the agent, , subject to the budget constraint .
In equilibrium, each agent holds only goods for which his utility/price ratio is weakly maximal. I.e, if agent  holds good  in equilibrium, then for every other good :

(otherwise, the agent would want to exchange some quantity of good  with good , thus breaking the equilibrium).
Without loss of generality, it is possible to assume that every good is desired by at least one agent (otherwise, this good can be ignored for all practical purposes). Under this assumption, an equilibrium price of a good must be strictly positive (otherwise the demand would be infinite).


== Existence of competitive equilibrium ==
David Gale proved necessary and sufficient conditions for the existence of a competitive equilibrium in a linear economy. He also proved several other properties of linear economies.
A set  of agents is called self-sufficient if all members of  assign a positive value only for goods that are owned exclusively by members of  (in other words, they assign value  to any product  which is owned by members outside ). The set  is called super-self-sufficient if someone in  owns a good which is not valued by any member of  (including himself). Gale's existence theorem says that:

A linear economy has a competitive equilibrium if and only if no set of agents is super-self-sufficient.

Proof of "only if" direction: Suppose the economy is in equilibrium with price  and allocation . Suppose  is a self-sufficient set of agents. Then, all members of  trade only with each other, because the goods owned by other agents are worthless for them. Hence, the equilibrium allocation satisfies:

.

Every equilibrium allocation is Pareto efficient. This means that, in the equilibrium allocation , every good is held only by an agent which assigns positive value to that good. By the equality just mentioned, for each good , the total amount of  held by members of  in the equilibrium allocation  equals the total amount of  held by members of  in the initial allocation . Hence, in the initial allocation , every good is held by a member of , only if it is valuable to one or more members of . Hence,  is not super-self-sufficient.


== Competitive equilibrium with equal incomes ==
Competitive equilibrium with equal incomes (CEEI) is a special kind of competitive equilibrium, in which the budget of all agents is the same. I.e, for every two agents  and :

The CEEI allocation is important because it is guaranteed to be envy-free: the bundle  gives agent  a maximum utility among of all the bundles with the same price, so in particular it gives him at least as much utility as the bundle .
One way to achieve a CEEI is to give all agents the same initial endowment, i.e, for every  and :

(if there are  agents then every agent receives exactly  of the quantity of every good). In such an allocation, no subsets of agents are self-sufficient. Hence, as a corollary of Gale's theorem:

In a linear economy, a CEEI always exists.


=== Examples ===
In all examples below, there are two agents - Alice and George, and two goods - apples (x) and guavas (y).
A. Unique equilibrium: the utility functions are:
,
.
The total endowment is . Without loss of generality, we can normalize the price vector such that . What values can  have in CE? If , then both agents want to give all their y for x; if , then both agents want to give all their x for y; hence, in CE . If , then Alice is indifferent between x and y, while George wants only y. Similarly, if , then George is indifferent while Alice wants only x. If , then Alice wants only x while George wants only y. Hence, the CE allocation must be [(6,0);(0,6)]. The price vector depends on the initial allocation. E.g, if the initial allocation is equal, [(3,3);(3,3)], then both agents have the same budget in CE, so . This CE is essentially unique: the price vector may be multiplied by a constant factor, but the CE equilibrium will not change.
B. No equilibrium: Suppose Alice holds apples and guavas but wants only apples. George holds only guavas but wants both apples and guavas. The set {Alice} is self-sufficient, because Alice thinks that all goods held by George are worthless. Moreover, the set {Alice} is super-self-sufficient, because Alice holds guavas which are worthless to her. Indeed, a competitive equilibrium does not exist: regardless of the price, Alice would like to give all her guavas for apples, but George has no apples so her demand will remain unfulfilled.
C. Many equilibria: Suppose there are two goods and two agents, both agents assign the same value to both goods (e.g. for both of them, ). Then, in equilibrium, the agents may exchange some apples for an equal number of guavas, and the result will still be an equilibrium. For example, if there is an equilibrium in which Alice holds 4 apples and 2 guavas and George holds 5 apples and 3 guavas, then the situation in which Alice holds 5 apples and 1 guava and George 4 apples and 4 guavas is also an equilibrium.
But, in both these equilibria, the total utilities of both agents are the same: Alice has utility 6 in both equilibria, and George has utility 8 in both equilibria. This is not a coincidence, as shown in the following section.


== Uniqueness of utilities in competitive equilibrium ==
Gale proved that:

In a linear economy, all agents are indifferent between all the equilibria.

Proof. The proof is by induction on the number of traders. When there is only a single trader, the claim is obvious. Suppose there are two or more traders and consider two equilibria: equilibrium X with price vector  and allocation , and equilibrium Y with price vector  and allocation . There are two cases to consider:
a. The price vectors are the same up to multiplicative constant:  for some constant . This means that in both equilibria, all agents have exactly the same budget set (they can afford exactly the same bundles). In equilibrium, the utility of every agent is the maximum utility of a bundle in the budget set; if the budget set is the same, then so is the maximum utility in that set.
b. The price vectors are not proportional. This means that the price of some goods changed more than others. Define the highest price-rise as:

and define the highest price-rise goods as those good/s that experienced the maximum price change (this must be a proper subset of all goods since the price-vectors are not proportional):

and define the highest price-rise holders as those trader/s that hold one or more of those maximum-price-change-goods in Equilibrium Y:

In equilibrium, agents hold only goods whose utility/price ratio is weakly maximal. So for all agents in , the utility/price ratio of all goods in  is weakly maximal under the price vector . Since the goods in  experienced the highest price-rise, when the price vector is  their utility/price ratio is strongly maximal. Hence, in Equilibrium X, all agents in  hold only goods from . In equilibrium X, someone must hold goods that are not in ; hence,  must be a proper subset of the agents.
So in equilibrium X, the -agents hold only -goods, and in equilibrium Y, -agents hold all the -goods. This allows us to do some budget calculations:
On one hand, in equilibrium X with price , the -agents spend all their budget on -goods, so:

(where  is the total initial endowment from good ).
On the other hand, in equilibrium Y with price , the -agents can afford all the -goods, so:

Combining these equations leads to the conclusion that, in both equilibria, the -agents only trade with each other:

.

Hence, the agents not in  also only trade with each other. This means that equilibrium X is composed of two equilibria: one that involves only -agents and -goods, and the other that involves only non--agents and non--goods. The same is true for agent Y. Since  is a proper subset of the agents, the induction assumption can be invoked and the theorem is proved.


== Calculating competitive equilibrium ==
Eaves presented an algorithm for finding a competitive equilibrium in a finite number of steps, when such an equilibrium exists.


== Related concepts ==
Linear utilities functions are a small subset of Quasilinear utility functions.
Goods with linear utilities are a special case of substitute goods.
Suppose the set of goods is not finite but continuous. E.g, the commodity is a heterogeneous resource, such as land. Then, the utility functions are not functions of a finite number of variables, but rather set functions defined on Borel subsets of the land. The natural generalization of a linear utility function to that model is an additive set function. This is the common case in the theory of fair cake-cutting. An extension of Gale's result to this setting is given by Weller's theorem.
Under certain conditions, an ordinal preference relation can be represented by a linear and continuous utility function.


== References ==