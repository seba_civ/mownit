Danny Barrett (born December 18, 1961) is an American football coach and former player. He is currently the running backs coach of the Miami Dolphins. He served as the UCF Knights interim head coach in 2015, and is a former Canadian Football League (CFL) quarterback and former quarterbacks coach at the University at Buffalo. He was previously the head coach of the CFL's Saskatchewan Roughriders.


== Playing career ==
Before turning pro with the Calgary Stampeders, Barrett was a star at the University of Cincinnati. In 1982, he co-captained the Bearcats and earned an Honorable Mention Associated Press All-American nomination. During his professional playing career, Barrett played in 163 regular season CFL games with Calgary, Toronto, B.C. and Ottawa. His career totals include; 23,419 yards passing, 1,656 completions in 3,078 attempts and 133 touchdowns passed. At one point, he held the record for most passing yards (601) in a CFL game [1].
In 1985, Barrett saw playing time as a slotback with the Calgary Stampeders hauling in 32 passes for 455 yards and two touchdowns. Barrett also quarterbacked the 1987 Argonauts and the 1991 Stampeders to Grey Cup appearances. In the 1991 Grey Cup, Barrett set a CFL record for completions with 34. In 1992, Barrett was selected as the Tom Pate recipient while a member of the BC Lions for his outstanding contributions to the league, his team and his community. This award was voted on by the CFLPA. In 1993, still as a member of the BC Lions, Barrett briefly held the CFL record for passing in a single game, with 601 yards, which was surpassed the following year by Matt Dunigan (713 yards).


== Coaching career ==
Barrett joined the coaching fraternity in 1997 with the Calgary Stampeders. As a first year assistant coach, Barrett was in charge of the Stampeder quarterbacks, which included Jeff Garcia, Dave Dickenson and Henry Burris. In 1998, his first season with the BC Lions, Barrett began the season on the sidelines as the quarterback coach and assistant offensive coordinator but was forced out of retirement and dressed as the Lions’ backup quarterback for 15 games. During the 1999 campaign, Barrett coached the Lions’ receiver corps.
In 1999, Roy Shivers, the former Director of Player Personnel for the Calgary Stampeders, assumed the duties of general manager of the Roughriders. Shivers hired Barrett as the head coach despite the latter's limited coaching experience. The Roughriders made football history by being the first professional team with a black general manager and head coach. The team improved during Barrett's first four seasons, largely because Shivers—an astute appraiser of football talent with many connections to U.S. college teams—recruited and signed better players. In 2003 the team ended with an 11-7 record, and lost a close Western final playoff game to the Edmonton Eskimos. The team seemed on the verge of being championship calibre.
By 2006, after three consecutive .500 seasons—capped by a humiliating 2006 Western final loss to the BC Lions by a score of 45-18—an increasing number of fans began to question the leadership provided by Shivers and Barrett, and Shivers eventually was dismissed by the team's Board of Directors. On August 23, 2006 the Roughriders hired Eric Tillman as general manager. There was rampant speculation that Barrett would be dismissed, but Tillman was publicly circumspect. In the weeks leading to his ultimate departure, Barrett was unusually—and perhaps unwisely—expressive to the media about his views on the firing of Shivers. Reports indicate that Tillman had offered Barrett a one-year contract to continue as head coach, but either Barrett rejected the offer or Tillman withdrew it.
Barrett coached the Riders in more regular season games than any other coach in Roughrider history; however, Barrett posted a winning record only once in his previous seven years of coaching the Roughriders. His overall record as head coach of the Roughriders was 57 wins, 68 losses, and one tie.
On February 7, 2007, Barrett turned down an offensive coordinator position offered by the Winnipeg Blue Bombers, and opted to become the quarterbacks and assistant head coach at the University at Buffalo.[2]
On December 13, 2009, Barrett was named interim head coach of the Buffalo Bulls after spending two seasons (2007 and 2008) as the Bulls quarterbacks coach/assistant head coach. The move followed the departure of Turner Gill, who left Buffalo to take the head coaching position at the University of Kansas.
On February 10, 2010, the Daytona Beach News-Journal reported Barrett was named offensive coordinator—in charge of the "Speedway Offense"—at Bethune-Cookman University.
On February 21, 2011, UCFAthletics.com reported Barrett was named running backs coach at UCF.
On January 14, 2015, UCFKnights.com reported that Barret was moved from running backs coach to quarterbacks coach at UCF.
On October 25, 2015, UCFKnights.com reported that Barret was announced as interim head coach for the remainder of the 2015 season, after George O'Leary announced his retirement from football.
On January 23, 2016, Barrett was hired by the Miami Dolphins to be their running backs coach.


== Head coaching record ==


=== CFL ===


=== College ===


== References ==


== External links ==
UCF profile