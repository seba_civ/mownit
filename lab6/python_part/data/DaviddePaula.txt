David de Paula Gallardo (born 31 May 1984) is a Spanish footballer who plays for FK Austria Wien as a midfielder.


== Football career ==
Born in Durango, Biscay, de Paula finished his formation at Athletic Bilbao after joining the club at the age of 18. His first and only two competitive appearances for the first time occurred during the 2005 UEFA Intertoto Cup, as he started in both legs of the tie against FC Ecomax, lost after a penalty shootout.
De Paula only played lower league football in his country, amassing Segunda División B totals of 239 games and 41 goals for Bilbao Athletic, Alicante CF, SD Lemona, SD Ponferradina, CF Palencia and UD Logroñés, during eight seasons. With the latter, he scored a career-best 11 goals in 2011–12, helping to a final fifth position.
Aged 28, de Paula moved to Austria, signing with Wolfsberger AC. His Bundesliga debut took place on 25 July 2012, when he came on as a 69th-minute substitute for compatriot Jacobo in a 0–1 home defeat to FK Austria Wien.
In the 2014 January transfer window, de Paula moved to Austria Wien.


== References ==


== External links ==
David de Paula profile at BDFutbol
Athletic Bilbao profile
David de Paula profile at Soccerway
David de Paula at ESPN FC