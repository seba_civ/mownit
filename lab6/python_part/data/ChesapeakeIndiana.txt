Chesapeake was the first town in Steuben Township, Warren County, Indiana, which was formed in 1834. It was located about two miles east of present-day town of Marshfield and was the site of the first meetings of the township trustees in the 1830s. County Agent Luther Tillotson lived south of the town and may have had some involvement in its creation. Chesapeake consisted of at least several houses, a country store operated by William Newell and Thomas Washburn, and a blacksmith shop. There was also a school house there named for the town for many years, but this also is gone.
Little else is known of Chesapeake and there is no record of a town platting. An 1883 history of the county notes that "comparatively nothing can be learned regarding this little town... The village began early and then died early, as good-looking babies are said to do."


== Geography ==
Chesapeake was located at 40°14'40.5240" North, 87°26'35.6640" West (40.24459,-87.44324). Chesapeake Creek begins in open farmland north of the site and flows east into Redwood Creek.


== References ==
Goodspeed, Weston A. (1883). "Part II. History of Warren County". Counties of Warren, Benton, Jasper and Newton, Indiana. Chicago: F. A. Battey and Company. p. 114. 
Clifton, Thomas, ed. (1913). Past and Present of Fountain and Warren Counties Indiana. Indianapolis: B. F. Bowen and Company. p. 325. 
Warren County Historical Society (1966), A History of Warren County, Indiana