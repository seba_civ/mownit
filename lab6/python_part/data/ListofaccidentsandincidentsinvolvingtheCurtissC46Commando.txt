The Curtiss C-46 Commando was a transport aircraft originally derived from a commercial high-altitude airliner design. It was instead used as a military transport during World War II by the United States Army Air Forces as well as the U.S. Navy/Marine Corps under the designation R5C. Known to the men who flew them as "The Whale," or the "Curtiss Calamity,"  the C-46 served a similar role as its counterpart, the Douglas C-47 Skytrain, but was not as extensively produced.
After World War II, a few surplus C-46 aircraft were briefly used in their original role as passenger airliners, but the glut of surplus C-47s dominated the marketplace with the C-46 soon relegated to primarily cargo duty. The type continued in U.S. Air Force service in a secondary role until 1968. However, the C-46 continues in operation as a rugged cargo transport for Arctic and remote locations with its service life extended into the 21st century.


== Accidents and incidents ==
Like every other major type in long service and operation, accidents and incidents have been recorded that have substantially reduced the numbers flying. The following list is typical of such a record of operational use.


=== 1940s ===
20 September 1944: A nighttime training mission both leaving and returning to Syracuse Army Air Base went missing. It was eventually discovered to have flown directly into the NE face of Blue Ridge Mountain near Speculator, NY. All three servicemen perished.
12 October 1945: While trying to navigate their approach to Beijing Nanyuan Airport, USAAF C-46F 44-78591 (carrying four US crew and 55 Chinese soldiers) struck a radio antenna and crashed, killing all on board in the worst-ever accident involving the C-46.
8 April 1946: A C-46 crashed after takeoff from Chongqing Airport due to losing its course, killing all on board, including Ye Ting, Wang Ruofei, Qin Bangxian and Deng Fa.
13 July 1946: A Central Air Transport C-46 crashed after takeoff from Jinan Airport due to engine failure, killing 13 of 49 on board.
10 December 1946: US Marine Corps R5C-1 39528 en route from San Diego to Seattle crashed into South Tahoma Glacier on the side of Mount Rainier in Washington, killing all 32 US Marines on board. The pilot was flying entirely by instruments in severe weather and it was determined that wind had moved the aircraft off course. At the time, this accident was the worst in U.S. aviation history.
25 December 1946: A China National Aviation Corporation C-46 (115) crashed while on approach to Longhua Airport, killing 31 of 36 on board (some sources say 29 dead); this aircraft was one of three that crashed on this night.
5 January 1947: A China National Aviation Corporation C-46 (XT-T51 / 121) struck a mountain west of Qingdao, China, killing all 43 on board.
28 January 1947: A China National Aviation Corporation C-46 (XT-T45 / 145) crashed 30 min after takeoff from Hankou, killing 25 of 26 on board.
14 February 1947: A Slick Airways C-46E (NC59486) crashed at Denver due to a loss of control, killing both pilots.
21 August 1947: A Slick Airways C-46E (NC59488) crashed at Hanksville, Utah after entering a thunderstorm, killing all three on board; the aircraft struck a mountain while flying too low.
20 January 1948: A China National Aviation Corporation C-46 crashed on takeoff from Mukden, China, killing 11 of 54 on board.
16 May 1948: A Slick Airways C-46E (NC56489) crashed near Port Columbus International Airport due to a loss of control following structural failure of the tail, killing both pilots.
29 July 1948: A Civil Air Transport C-46D (XT-822) crashed at Qingdao Airport after entering a spin after takeoff, killing all 19 on board.
7 June 1949: A Strato-Freight C-46D (NC92857) crashed on climbout from Isla Grande Airport following a loss of power in the right engine due to maintenance errors, killing 53 of 81 on board; the accident remains the worst in Puerto Rico.
12 July 1949: Standard Air Lines Flight 897R (a C-46E, N79978) crashed at Chatsworth, California, due to pilot error, killing 35 of 48 on board.
2 August 1949: A Varig C-46D (PP-VBI) operating a flight from São Paulo-Congonhas Airport to Porto Alegre made an emergency landing on rough terrain near the location of Jaquirana, approximately 20 minutes before landing in Porto Alegre, following fire on the cargo hold. Of the 36 passengers and crew aboard, five died.
9 October 1949: Slick Airways Flight 11-8 (a C-46E, NC59485) crashed near Cheyenne, Wyoming due to loss of control caused by icing and severe turbulence, killing all three on board.
9 December 1949: A Civil Air Transport C-46D (XT-820) crashed near Lanzhou, killing all 38 on board.
10 December 1949: A Civil Air Transport C-46D crashed at Haikou, killing 17 of 40 on board.


=== 1950s ===
5 June 1950: A Westair Transport C-46F (N1248N) operating a flight from San Juan, PR to Wilmington, NC ditched into the Atlantic 300 miles east of Melbourne, Florida due to failure of both engines for reasons unknown. It sank in one of the deepest areas of the Atlantic and could not be recovered. Of the 65 passengers and crew aboard, 28 died.
29 July 1951: A Lóide Aéreo Nacional C-46A (CB-39), flying from Cochabamba to Rio de Janeiro, probably operating a delivery ferry flight still bearing the Bolivian registration number, crashed on takeoff. All seven occupants died.
16 December 1951: A Miami Airlines C-46F (N1678M) stalled and crashed at Elizabeth, New Jersey due to an engine fire, killing all 56 on board.
29 December 1951: Continental Charters Flight 44-2 (a C-46A, N3944C) struck Bucktooth Ridge near Napoli, New York due to pilot error, killing 26 of 40 on board.
30 December 1951: Transocean Air Lines Flight 501 (a C-46F, N68963) crashed near Fairbanks, Alaska due to spatial disorientation caused by pilot error, killing all four on board.
18 April 1952: Robin Airlines Flight 416W (a C-46E, N8404C) crashed into a hill near Whitter, California after the pilot descended too low, killing all 29 on board.
24 May 1952: A Lóide Aéreo Nacional C-46D (PP-LDE) during take-off from Manaus-Ponta Pelada stalled when trying to return to the airport following an engine failure. It crashed into the Rio Negro. The 6 occupants died.
7 January 1953: An Associated Air Transport C-46F (N1648M) crashed 8 mi west of Fish Haven, Idaho after the pilot involuntary descended into icing and turbulent conditions, killing all 40 on board; the wreckage was found five days later.
20 August 1953: An Itaú C-46A (PP-ITD) crashed and caught fire during an emergency landing at Corumbá. Three crew members died and one survived.
4 June 1954: A Varig C-46A (PP-VBZ) operating a cargo flight between São Paulo-Congonhas Airport and Porto Alegre crashed during take-off from São Paulo. All crew of 3 died.
3 April 1955: An Itaú C-46A (PP-ITG) struck a hill two miles short of the runway while on an instrument approach to Vitória. The crew of three died.
7 April 1957: A Varig C-46A (PP-VCF) operating a flight from Bagé to Porto Alegre crashed during takeoff from Bagé, following a fire developed in the left main gear wheel well and consequent technical difficulties. All 40 passengers and crew died.
31 May 1958: A Paraense C-46D (PP-BTB) crashed on climbout from Rio de Janeiro-Santos Dumont while operating a cargo flight. The crew of four died.
5 September 1958: A Lóide Aéreo Nacional C-46D (PP-LDX) crashed during approach to Campina Grande. Of a total of 18 people aboard, 2 crew members and 11 passengers died.
16 January 1959: Austral Líneas Aéreas Flight 205 crashed on approach to Mar del Plata killing 51 occupants: five crew members and 46 passengers. The cause of the crash was determined as pilot error.
6 May 1959: A Paraense C-46A (PP-BTA) crashed shortly after takeoff from Belém-Val de Cães. Three crew members died.
1 June 1959: An Aerolíneas Nacionales C-46 (TI-1022) was shot down by a Nicaraguan Air Force P-51 over Nicaragua, resulting in the death of both pilots.


=== 1960s ===
22 September 1960: A Paraense C-46A (PP-BTF) crashed shortly after takeoff from Belém-Val de Cans. Seven occupants died.
29 October 1960: A chartered C-46 carrying the Cal Poly football team crashed on takeoff in Toledo, Ohio, resulting in the death of 22 of the 48 people on board.
7 December 1960: A Real C-46A (PP-AKF) belonging to Transportes Aéreos Nacional operating flight 570 from Cuiabá to Manaus-Ponta Pelada crashed on Cachimbo mountains. The no.2 engine failed during the flight. Altitude was lost, the pilot jettisoned some of the cargo but the aircraft continued to lose height. It crashed and caught fire and 15 passengers and crew died.
30 April 1964: An Aerolineas Carreras C-46D (HK-527) made an emergency landing in the Andean region of Puna de Atacama, Argentina on a cargo flight between Lima, Perú and Buenos Aires, Argentina all crew was safe and rescued with the only loss of a handful of thoroughbred mares they were shipping.
20 June 1964: Civil Air Transport Flight 106 (a C-46D, B-908) crashed on climbout from Taichung Airport due to loss of control following engine problems, killing all 57 on board.
12 August 1965: A Paraense C-46A (PP-BTH) en route to Cuiabá caught fire and crashed in Buracão, close to Barra do Bugre, in the State of Mato Grosso. All 13 passengers and crew died.
11 July 1966: An Aeropesca Colombia C-46D (HK-527) disappeared near Cerro el Planchon, Chile on a cargo flight between El Dorado International Airport, Bogota, Colombia and Buenos Aires, Argentina with the loss of a crew of eight.
16 April 1969: Shortly after takeoff, a CIA-chartered (operated by WIGMO) C-46 (9T-PLJ?) crashed into the Congo River, killing all 45 people on board. The pilot had reported a landing gear malfunction, and was trying to return to N'djili Airport.


=== 1970s ===
24 September (or possibly 16 September) 1976: An Aerosucre Colombia C-46D (HK-1282) disappeared on a cargo flight en route to Queen Beatrix International Airport, Oranjestad, Aruba with the loss of a crew of two.
13 November 1979: A Lamb Air C-46 Commando C-GYHT c/n 22375 crashed after take off from Churchill, Manitoba. The aircraft, nicknamed Miss Piggy, was carrying a load of one snowmobile and many cases of pop when it lost oil pressure in the left engine shortly after take-off. The crew attempted to land the aircraft, crashing several hundred metres short of the runway. Two of the three crew members were injured. The plane wreck remains in place today 


=== 1980s ===
15 November 1980: A BWI Leasing C-46A (N355BY) crashed off Norman's Cay, Bahamas. There were no injuries.


=== 2000s ===
21 April 2012, 7 minutes after taking off from Viru – Viru International, Santa Cruz de la Sierra – Bolivia, Curtiss C-46 CP-1319 lost one engine and crashed with the loss of a crew of three and one injured.
25 September 2015, a Buffalo Airways C-46A (C-GTXW) diverted to Deline Airport, Northwest Territories, where it made an emergency gear-up landing. Although the aircraft was substantially damaged, all four on board were not injured.


== References ==
Notes

Bibliography


== External links ==