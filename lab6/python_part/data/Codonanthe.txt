Codonanthe is a New World genus, consists of a dozen or more species of evergreen epiphytic compact creeping vines with rooting along their stems, and scandent herbs or subshrubs with woody, upright stems from Brazil, Guiana, Central America and West Indies. In its own habitat, the plant usually grows in association with tree ant nests. The botanical name comes from the Greek for bellflower. Small nice red stems held flat or hanging, about 30cm long, with pairs of relatively succulent, hard-surfaced and small round dark foliage. Delicate, pale-pink to white, bell-shaped flowers grow from the leaf axil, either single or in group and always produce in a large number. Flower tube has 2 lips, each with 2 and 3 lobes; which may be white, pink, lilac to deep purple with yellow throat. These delightful waxy flowers are strongly scented. Attractive colorful berry-like fruits appear after flowering. Seeds look like ant eggs or larvae, which are carried to the nest by ants and inserted into the fertile nest wall where they germinate. Seeds that germinate outside a nest tend to grow slower.


== Species ==
Codonanthe calcarataCodonanthe caribaeCodonanthe corniculataCodonanthe carnosaCodonanthe crassifoliaCodonanthe decurrensCodonanthe devosianaCodonanthe dignaCodonanthe dissimulataCodonanthe gracilisCodonanthe luteolaCodonanthe macradeniaCodonanthe uleanaCodonanthe ulei


== References ==
Botanica Sistematica