Seymour is a city in and the county seat of Baylor County, Texas, United States. The population was 2,740 as of the 2010 Census.


== Geography ==
Seymour is located at 33°35′30″N 99°15′39″W (33.591552, −99.260968), on the Brazos River. It is 52 miles (84 km) southwest of Wichita Falls and 102 miles (164 km) north-northeast of Abilene. According to the United States Census Bureau, the city has a total area of 2.9 square miles (7.6 km2), of which 0.004 square miles (0.01 km2), or 0.20%, is water.


=== Climate ===
The climate in this area is characterized by hot, humid summers and generally mild to cool winters. According to the Köppen Climate Classification system, Seymour has a humid subtropical climate, abbreviated "Cfa" on climate maps.


== Demographics ==


=== 2010 census ===
As of the census of 2010, there were 2,740 people, a decrease of 5.78% since 2000 (168 people). The racial makeup of the town was 91.28% White (2,501 people), 3.61% Hispanic or Latino of any race (373 people), 2.45% African American (67 people), 0.22% Native American (6 people), 0.11% Asian (3 people), 0.11% Pacific Islander (3 people), 4.11% from other races (104 people), and 3.80% from two or more races (56 people).
There were 1,451 housing units, 249 of which were vacant.


=== 2000 census ===
As of the census of 2000, there were 2,908 people, 1,273 households, and 790 families residing in the city. The population density was 1,067.5 people per square mile (412.8/km²). There were 1,534 housing units at an average density of 563.1 per square mile (217.8/km²). The racial makeup of the city was 89.24% White (2,595 people), 10.45% Hispanic or Latino of any race (304 people), 4.57% African American (133 people), 0.48% Native American (14 people), 0.72% Asian (21 people), 0.10% Pacific Islander (3 people), 3.44% from other races (100 people), and 1.44% from two or more races (42 people).
There were 1,273 households out of which 26.0% had children under the age of 18 living with them, 49.1% were married couples living together, 10.0% had a female householder with no husband present, and 37.9% were non-families. 35.7% of all households were made up of individuals and 21.9% had someone living alone who was 65 years of age or older. The average household size was 2.25 and the average family size was 2.90.
In the city the population was spread out with 24.3% under the age of 18, 6.1% from 18 to 24, 20.9% from 25 to 44, 23.9% from 45 to 64, and 24.8% who were 65 years of age or older. The median age was 44 years. For every 100 females there were 83.2 males. For every 100 females age 18 and over, there were 79.6 males.
The median income for a household in the city was $23,662, and the median income for a family was $32,917. Males had a median income of $21,891 versus $19,292 for females. The per capita income for the city was $16,062. About 15.6% of families and 19.5% of the population were below the poverty line, including 31.8% of those under age 18 and 10.7% of those age 65 or over.


== Businesses and organizations ==
The local Seymour Chamber of Commerce, Seymour Council for the Arts & Enrichment, Rodeo Association, Lions Club, and Garden Club are amongst the many organizations in Seymour, as well as an active Relay for Life Event.


== History ==
Seymour was founded by settlers from Oregon, who called the town Oregon City; it was originally located where the Western Trail crossed the Brazos River, which flows just south of the townsite. A post office was established in 1879, at which time the town's name was changed to honor local cowboy Seymour Munday, after whom nearby Munday was also named.
Commerce, a newspaper, a hotel, and the county courthouse all followed soon after, as did violence between cowboys and settlers. The town experienced two distinct economic booms: the first, short-lived, with the construction of the Wichita Valley rail line in 1880, and the second due to the discovery of oil in 1906. The population grew from 500 in 1884 to almost 3800 in 1950; it remained at about that level for more than thirty years, but has declined since to 2,740 in the 2010 census. Agribusiness, as well as some tourism from nearby Lake Kemp, has overtaken oil as the driving factor of the local economy. The Old Settlers Reunion and Rodeo has been held each July since 1896.
The town calls itself "the crossroads of North Texas" because it is located at the junction of five highways: U.S. highways 82, 277, 183 and 283, as well as State Highway 114.
The Reptiliomorpha order Seymouriamorpha and genus Seymouria are named after this city. The last seymouriamorph became extinct by the end of the Permian period.
On August 12, 1936, the temperature at Seymour reached 120 °F (49 °C), the highest temperature ever recorded in the state of Texas.
The Seymour Division of La Escalera Ranch is located north of Seymour in Baylor County and consists of 34,000 contiguous acres (120 km²) in Baylor County and Archer County. Previously known as Circle Bar Ranch, La Escalera Limited Partnership purchased the ranch from the Claude Cowan Sr. Trust in January 2005. La Escalera partner Jo Lyda Granberg and husband K. G. Granberg manage the cow-and-calf operation. The ranch is known for its reputation herd of Black Angus cattle and its abundant wildlife, including white-tailed deer, white-winged dove, mourning dove, Northern bobwhite quail, Rio Grande wild turkey and feral pig.


== Famous people from Seymour ==
The famous chuckwagon cook Joseph "Cap" Warren was from Seymour. The Saturday Evening Post wrote an article about him in the 1940s, and he was featured in the book Bowl of Red.


== In popular culture ==
Lake Kemp, near Seymour, is the location where the Internet viral video titled "Failed Dock Jump Attempt" was filmed. This video was featured on G4tv's Attack of the Show 


== Education ==
Seymour is served by the Seymour Independent School District.


== References ==


== External links ==
City of Seymour official website
Seymour Independent School District