The House of Beauharnais (or House of Leuchtenberg) is a French noble house. It is now represented by the Duke of Leuchtenberg, descendant in male line of Eugène de Beauharnais.


== History ==

Originating in Brittany, the Beauharnais (or Beauharnois) became established in the fourteenth century in Orléans. When that city was besieged in 1429, Jehan Beauharnais played a role in its defence and by doing so witnessed to the process of Joan of Arc's rehabilitation. The Beauharnais provided the kingdom with soldiers and magistrates, and contracted alliances in several spheres, including that of the university of law in Orléans. In the 16th century, there were Beauharnais in Orléans as magistrates, merchants, canons and other professions.
From the end of the 16th century to the end of the 17th, the offices of president and of lieutenant général to the bailliage and siège présidial of Orléans were handed down hereditarily through the Beauharnais family. The most eminent of these magistrates was Francis IV de Beauharnais, sieur of la Grillère (at Vouzon, Loir-et-Cher), born in Orléans in 1630, and dying there in 1681.
At the end of the 17th century, the office of lieutenant général du bailliage d'Orléans was ceded to an allied branch, the Curaults. The most eminent of the Beauharnais thus turned to careers in the navy and the colonial administration in the Americas. Another François de Beauharnais (1665–1746) became intendant of New France (i.e. Canada), where a seigneurie was granted to him in 1707. His nephew, Francis V de Beauharnais, was made chef d'escadre des armées royales, then governor of Martinique.
The Beauharnais of Orléans were also great landholders thanks to their many seigneuries in the region. In the 15th century they were seigneurs of la Chaussée (or Miramion), fief of the parish of Saint-Jean-de-Braye, in the suburbs of Orléans. They also had interests at Sologne until the early 16th century, and Guillaume de Beauharnais held the estate of Villechauve, at Sennely. Some years later, the Beauharnais acquiring the neighbouring seigneurie of la Grillère, at Vouzon, with they held until the beginning of the 18th century (when it passed, by the marriage of a female Beauharnais, to the Choiseul-Gouffier family).
On 20 April 1752, Francis V, marquis de Beauharnais (1714–1800), governor of Martinique, maternal great-grandfather of the future Napoleon III of France, bought the seigneurie of La Ferté-Avrain, in Sologne. He was raised to a marquis by letters patent dated July 1764 with the title of La Ferté-Beauharnais, a name the commune still bears (département of Loir-et-Cher).


== Genealogy ==
Partial genealogy (male and female lines)
William I Beauharnais
William II Beauharnais
Jean Beauharnais, seigneur de Miramion
William III Beauharnais
William IV Beauharnais
Francis I of Beauharnais, seigneur of Miramion (died 1587)
François II de Beauharnais (died 1651), first president of the Présidial d'Orléans (en 1598) and lieutenant général au bailliage ; Third Estate député at the States General of 1614 ; seigneur of la Grillère, at Vouzon (Loir-et-Cher).
Married to Anne Brachet ; 7 children, including
Jean de Beauharnais (1606–1661), maître d'Hôtel ordinaire du roi;
Married to Marie Mallet ; 3 children including
François de Beauharnais (1630-1681), écuyer, sieur de la Boische.
Married in 1663 to Marguerite Françoise Pivart de Chastullé ; 14 children, including
Charles de la Boische, Marquis de Beauharnois
Claude de Beauharnais (1680–1736), seigneur de Beaumont ; capitaine des vaisseaux du roi, chevalier de Saint-Louis.
Married to Renée Hardouineau ; 2 sons
I. François de Beauharnais (1714–1800), marquis de La Ferté-Beauharnais (from 1764) ; chef d'escadre des armées royales ; governor of Martinique.
Married twice -
his cousin Marie-Anne Pivart de Chastullé
Marie-Euphémie Tascher de la Pagerie.
Two children from his first marriage:

A. François de Beauharnais (1756–1823), marquis de La Ferté-Beauharnais.
B. Alexandre François Marie de Beauharnais (1760–1794), vicomte de Beauharnais ; noble député from the bailliage of Blois to the Estates General of 1789 ; général en chef of the Armée du Rhin in 1793 ; guillotined in 1794.

In 1779 married Marie-Josèphe-Rose (known as "Joséphine") Tascher de la Pagerie. Two children :

1. Eugène Rose de Beauharnais (1781–1824), prince français, viceroy of Italy, prince of Venice, grand-duke of Frankfurt, duke of Leuchtenberg and prince of Eichstätt.
a. Joséphine Maximilienne Eugénie Napoléone de Beauharnais (1807–1876), princess of Bologne, duchess of Galliera, duchess of Leuchtenberg, queen of Sweden and of Norway.
i. Charles Louis Eugène Bernadotte (1826–1872), king of Sweden and Norway (grandson of general Bernadotte, founder of the dynasty).
ii. Gustave Bernadotte (1827–1852), prince of Sweden and duke of Uppland.
iii. Oscar Frédéric Bernadotte (1829–1907), king of Sweden and Norway.
iv. Charlotte Eugénie Auguste Amélie Albertine Bernadotte (1830–1889), princess of Sweden.
v. Charles Nicolas Auguste Bernadotte (1831–1873), prince of Sweden and duke of Dalécarlie.

b. Eugénie Hortense Auguste de Beauharnais (1808–1847), duchess of Leuchtenberg and princess of Hohenzollern-Hechingen.
c. Auguste Charles Eugène Napoléon de Beauharnais (1810–1835), duke of Leuchtenberg, prince of Eichstätt, infante of Portugal and duke of Santa-Cruz.
d. Amélie Auguste Eugénie Napoléone de Beauharnais (1812–1873), duchess of Leuchtenberg and empress of Brazil.
i. Marie Amélie Augusta Eugénie Joséphine Louise Théodelinde Héloïse Françoise de Bragance (1831–1853), princess of Brazil.

e. Théodelinde Louise Eugénie Auguste Napoléone de Beauharnais (1814–1857), duchess of Leuchtenberg, countess of Württemberg and duchess of Urach.
f. Caroline Clotilde de Beauharnais (1816–1816).
g. Maximilien Joseph Eugène Auguste Napoléon de Beauharnais (1817–1852), duke of Leuchtenberg, prince of Eichstätt and duke in Russia with the style "your imperial highness".

2. Hortense Eugénie Cécile de Beauharnais (1783–1837), queen of Holland.
a. Charles Louis Napoléon Bonaparte (1808–1873), French emperor

II. Claude de Beauharnais (1717–1784), comte des Roches-Baritaud.
A. Claude de Beauharnais (1756–1819), comte des Roches-Baritaud, first married Adrienne de Lezay-Marnésia
Stéphanie (1789+1860) imperial princess, in 1806 married Charles II, grand duke of Baden, presumed mother of Gaspard Hauser.


== Members by marriage ==
Fanny de Beauharnais (1738–1813), French poet, wife of Claude de Beauharnais (1717–1784).
Joséphine de Beauharnais (1763–1814), French empress, wife of Alexandre de Beauharnais (1760–1794) then of Napoleon I of France.


== Members of the Beauharnois house ==
François de Beauharnois de la Chaussaye, Baron de Beauville (grandson of Jean de Beauharnais (1606-1661) )
Charles de la Boische, Marquis de Beauharnois (grandson of Jean de Beauharnais (1606-1661) )
Claude de Beauharnois de Beaumont et de Villechauve (grandson of Jean de Beauharnais (1606-1661) )


== References ==


== Sources ==
A. Pommier, "Recherches sur les Beauharnais du XVIIe siècle à Orléans", dans Bulletin de la Société historique et archéologique de l'Orléanais, t. XXIII, n° 235 (1937).
R. Gallon, Les Beauharnais, Orléans, La Galerie des Ventes d'Orléans, 1979.
Christian Poitou, "Napoléon III et la Sologne", dans La Sologne et son passé, 9, Bulletin du Groupe de Recherches Archéologiques et Historiques de Sologne, t. XIII, n° 2, avril-juin 1991.
Philippe de Montjoulvent, Les Beauharnais, 1-Les grands ancêtres, Paris, Editions Christian, 2005 (569 p.)


== External links ==
 "Beauharnais". Encyclopædia Britannica (11th ed.). 1911.