Zakir Husain Delhi College, formerly Zakir Husain College, Anglo Arabic College and Delhi College, founded in 1692, is the oldest existing educational institution in Delhi, and is a constituent college of the University of Delhi, offering undergraduate and post graduate courses in Arts, Commerce and Sciences. It has had a considerable influence on modern education as well as Urdu and Islamic learning in India, and today remains the only Delhi University college offering BA (Hons) courses in Arabic and Persian.


== History ==

It was initially founded by Ghaziuddin Khan, a general of Mughal Emperor Aurangzeb, a leading Deccan commander and the father of Qamar-ud-din Khan, Asaf Jah I, the founder of the Asaf Jahi dynasty of Hyderabad, also known as the first Nizam of Hyderabad, in 1690s, and was originally termed Madrasa Ghaziuddin Khan after him. However with a weakening Mughal Empire, the Madrasa closed between 1790 and 1791, but with the support of local nobility, an oriental college for literature, science and art, was established at the site in 1792.
It stood just outside the walled city of Delhi outside the Ajmeri Gate, near Paharganj close to the New Delhi railway station. It was originally surrounded by a wall and connected to the walled city fortifications and was referred to as the College Bastion.
It was reorganized as the 'Anglo Arabic College' by the British East India Company in 1828 to provide, in addition to its original objectives, an education in English language and literature. The object was “to uplift” what the Company saw as the “uneducated and half-barbarous people of India.” Behind the move was Charles Trevelyan, the brother-in-law of Thomas Babingdon Macaulay, the same infamous Macaulay whose famously declared that “a single shelf of a good European library was worth the whole native literature of India and Arabia”.
Rev. Jennings started secret Bible classes in the officially secular Delhi College. In July 1852, two prominent Delhi Hindus, Dr. Chaman Lal, one of Zafar’s personal physicians, and his friend Master Ramchandra, a mathematics lecturer at the Delhi College, baptised a public ceremony at St. James' Church, Delhi.
Dr. Sprenger, then principal, presided over the founding of the college press, the Matba‘u ’l-‘Ulum and founded the first college periodical, the weekly Qiranu ’s-Sa‘dain, in 1845.
Another cultural intermediatory was Mohan Lal Kashmiri, diplomat, and author, who worked for the East India Company and was educated at the college.
It was renamed Zakir Husain College in 1975 after Dr. Zakir Husain, a distinguished educator and a President of India. The college was later shifted to its present building outside Turkman Gate in 1986, the old structure in the Madrasa Ghaziuddin complex, still houses a hostel for the college. It was declared a heritage monument by the ASI in 2002. Then in 2008, a separate archive on its history was set up within the college library, with centuries-old books and documents on display, chronicling its 300-year-old history.


== Alumni and impact ==
It has had a number of distinguished alumni. Sir Syed Ahmed Khan, the founder of Aligarh Muslim University, Liaqat Ali Khan, Pakistan’s first Prime Minister, Maulana Mohammed Hussain Azad, the father of Urdu prose, Deputy Nazir Ahmed, the Urdu essayist and ICS, poets, Ali Sardar Jafri and Akhtar ul-Iman, Mirza M N Masood, an Indian hockey olympian, eminent linguist and the 5th Vice-Chancellor of Jamia Millia Islamia Professor Emeritus Masud Husain Khan, Khwaja Ahmed Farooqui (literatteur), Prof A N Kaul (pro-vice chancellor, Delhi University), J N Dixit (Defence Analyst), Prof Gopi Chand Narang (world-renowned Urdu/Persian critic), Pankaj Vohra (Associate Editor, Hindustan Times), B N Uniyal, Shahid Siddiqui, Manmohan, Mukul Vyas, Chandra Prabha, Habib Akhtar, M Afzal (journalists) and politicians like Jagdish Tytler and Sikandar Bakht.
Among the greats of Delhi College was Prof. Bhishma Sahni of the English department who was a noted writer and dramatist. Prof. Sahni was the brother of Actor Balraj Sahni.
It's said that Ghalib was once a candidate for the Persian post for Delhi College. However, the administrator conducting the interview failed to come out to greet him, and an offended Ghalib left.
Mamluk Ali Nanutavi, the distinguished scholar, who descendants founded Darul Uloom Deoband, taught Arabic here in 1830s.
Zakir Husain Delhi College has been offering an extremely wide range of courses for students. It offers science, humanities and commerce as well as language courses. One important feature of the college is that it is (at least used to be) the only college in Delhi which offers Graduation courses to male students in Psychology. All other colleges which offer this course are exclusively for female students.
The college enjoys a sterling reputation in the Delhi University and is amongst the top 10 colleges in the University.


== Zakir Husain Memorial Trust ==
Zakir Husain Delhi College is run by the 'Zakir Husain Memorial Trust' under the chairmanship of "Dr. Manmohan Singh",the former prime minister of India and minister of human resources development is the vice-chairman of the trust as well as of the college.


== Mirza Mehmood Begg Library & Book Bank ==
The College has a well stocked library possessing about 1,18,462 books.It runs on open shelf system but some important text books are also kept in reserve section.It not only caters to the academic requirements but also houses leisure books and books to increase general awareness.The Library is named after the college principal Mirza Mehmood Begg.
The College Book Bank renders service to students by providing them with text books for the whole academic session.This is the only college in the Delhi University which offers four textbooks to every students for whole academic session free of cost.


== Salman Gani Hashmi Auditorium ==
The College has fully functional,Centrally Air Conditioned auditorium with a seating capacity of 417 persons.The auditorium has a state of art audio-visual system and a multi functional lighting arrangement.Various cultural programmes,lectures and college annual function are also organised in this auditorium.This auditorium is named after the college principal Salman Gani Hashmi.


== College Archives ==
The Delhi College Archives,situated in a section of the M.M. Begg Library,was inaugurated by Prof. Sabyasachi Bhattacharya,Chairman,Indian Council of Historical Research,on February 18, 2008.
The archives contain a large number of files relating to the college and significant developments in higher education in Delhi and North India from 1823 onwards. These have been located within the National Archives of India and the Delhi Archives,and analyzed over the last couples of years.
Original writings by teachers and alumni of the college in Urdu,Persian and English are also available in the archives.Text-books prepared and/or used during the 19th Century for instruction in mathematics,history,geography,philosophy,literature etc., are on display.The archives also contains secondary sources and books relating to Delhi College and the intellectual ferment in Delhi region during the 18th and 19th centuries.


== Zakir Husain Memorial Lecture ==
A major annual event in the college calendar is the Zakir Husain Memorial Lecture to commemorate Dr. Zakir Husain. The speaker is an eminent personality of his field. It is organized in the 1st week of February. Zakir Husain Memorial Lecture is organized since 2006 annually.The lecture was delivered by the following persons;
Ms. Aruna Roy (2006) Prof. Sukhadeo Thorat (2007) Mr. Intizae Husain (2008) Shri M. Hamid Ansari (2009) Prof. B.B. Bhattacharya (2010) Mr. Soli J. Sorabjee (2011) Prof. C.M. Naim (2012) Prof V.S. Chauhan (2013)


== Delhi College Lecture Series ==
From the academic session 2006-07,the college has started another series of lectures named Delhi College Lecture Series. The endeavour is to recreate and carry forward the legacy of the Delhi Renaissance and the progress made thereof in Science,Literature and the positive interventions in the field of education. The last lecture was delivered by Mrinal Pande,eminent journalist and chairman Prasar Bahrti.


== Convocation Ceremony ==
Another important event in the college is the full dress convocation ceremony. The students who have graduated in the previous academic session are given their degree by a noted personality .The purpose behind holding convocation is to inculcate a sense of institutional pride among our students through a public recognition of their achievement and also inspire them to strive for greater goals in life. The occasion brings together past and present students and teachers to strengthen bonds within the fraternity.This is only college in the Delhi University which holds convocation ceremony.


== Miscellaneous ==


=== Philanthropic Tradition ===
Significantly, the college has a proud philanthropic tradition to present as well. At one point in the recent history of the college, when the college had to shift its campus from the Ajmeri Gate locale, the staff and administration were given a choice to be part of the North Campus of Delhi University. However, the then heads of the institute had taken more humane considerations into account. They had observed that the college had acquired great aspirational value for many members of the surrounding community, who could not travel too far due to cultural limitations. It was clear that this deserving section of society would be deprived of a golden chance if the college shifted base to a place so far away if this decision was taken, and thus it was rejected. Thus, the college instead set itself up nearby at Jawahar Lal Nehru Marg, opposite Ram Leela Maidan.


== See also ==
Mohan Lal (Zutshi)
University of Delhi
Anglo Arabic Senior Secondary School another descedent of the original institution


== References ==

The Delhi College,by Margit Pernau,Oxford University Press.


== External links ==
Official website
College Profile on student media website University Express