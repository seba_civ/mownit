County Kildare in the province of Leinster, Ireland, was first defined as a diocese in 1111, shired in 1297 and assumed its present borders in 1836. Its location in the Liffey basin on the main routes from Dublin to the south and west meant it was a valuable possession and important theatre of events throughout Irish history.


== Ancient history ==
An inland town on Ptolemy's map of Ireland of 100 AD may be Rheban on the Barrow river, the only written records from pre-Christian County Kildare. The estimated date for the abandonment of the sacred pre-Christian site of Knockaulin/ Dún Áilinne is 400 AD, the traditional date for foundation of the monastery at Cill Dara is 490 AD, the date for the death of first Bishop Conlaed ua hEimri, (St Conleth) is 520 AD and the estimated date for the death of foundress St Brigid (Irish: Naomh Bríd), is 524 AD (also dated 521 and 526). Her death has been celebrated traditionally on 1 February, which is also the pre-Christian festival of Imbolc. The rise of Kildare sept the Uí Dúnlainge after 633AD helped promote the cult of St Brigid, as she was related to that dynasty, giving her status as one of three 'national saints' of Ireland and increase the status of the two monasteries where they had influence, Kildare and Glendalough.
The first hagiography of St Brigid, Vita Brigitae, already containing familiar wonder tales such as the story of how her cloak expanded to cover the area now known as the Curragh of Kildare, was compiled in 650AD by Cogitosus for Faolán mac Colmáin the first of the Uí Dúnlainge kings of Leinster. In 799 a reliquary in gold and silver was created for relics of Conlaed (St Conleth). Further south the death of Diarmait (St Diarmuid), anchorite scholar and founder of Castledermot created a second major monastic site in the county. There were also about 50 local saints associated with pattern days and wells in the county. Kildare is home to five surviving round towers at Kildare town, Castledermot, Old Kilcullen, Taghadoe near Maynooth and Oughter Ard near Ardclough.


== Kings of Leinster ==
The Uí Dúnlainge claimed descent from Dúnlaing, son of Enna Nia. Their positions as Kings of Leinster were unopposed following the death of Aed mac Colggan in the Battle of Ballyshannon, on 19 August 738. The dynasty then divided into three kindreds, amongst which the kingship rotated from c.750 until 1050. This is unusual in early Irish history, according to Professor Francis John Byrne of University College Dublin, for it was the equivalent of "keeping three oranges in the air." 14 Uí Meiredaig kings (later to become the O'Tooles) were based at Mullaghmast/Máistín 9 Uí Faelain kings (later the O'Byrnes) were based at Naas/ Nás na Ríogh and 10 Uí Dúnchada kings (later the Hiberno-Norman FitzDermots) were based at Lyons Hill/ Líamhain. The influence of the family helped secure place-myths for prominent Kildare landmarks in the heroic and romantic literature such as the Dindeanchas, Dinnshenchas Érenn as one of the "assemblies and noted places in Ireland"
In 833 Vikings raided Kildare monastery for first of sixteen times, the second and most destructive raid following three years after, and the power of the Uí Dúnlainge waned after the battles of Gleann Mama, beside Lyons Hill in the north of the county in 999 and Clontarf in 1014. After the death of the last Kildare-based King of Laighin, Murchad Mac Dunlainge, in 1042, the Kingship of Leinster reverted to the Uí Cheinnselaig sept based in the south east.
In the Gaelic-era "Triads of Ireland", Kildare was described at line 4 as: "The heart of Ireland".


== End of the Abbacy ==
In 1132 Kildare monastery was destroyed by Diarmait Mac Murchada /Diarmait MacMurrough, King of Laighin, when he forced the abbess to marry one of his followers and installed his niece as abbess. It was the end of the only major Irish church office open to women, in 1152 the Synod of Kells deprived the Abbess of Kildare of traditional precedence over bishops and when the last abbess of Kildare, Sadb ingen Gluniarainn Meic Murchada, (niece of Diarmait Mac Murchada), died in 1171 the Norman invasion of Ireland brought the famous abbacy to an end. Gerald of Wales/ Giraldus Cambrensis visited Kildare in 1186 and described the (later lost) Book of Kildare as the "dictation of an angel." He also recorded the sacred fire of Kildare, the pagan nature of which was subject of iconoclastic suspicion as early as 1220 when it was extinguished by Henry de Londres, archbishop of Dublin. According to folklore, it was rekindled and continued to burn until the Protestant Reformation in 1541.


== Boundaries ==


=== Origins as diocese ===
The first attempt to define the borders of Kildare was in 1111 when a sphere of influence for Kildare diocese was defined by the synod of Raith Bressail. For a short time Kilcullen was also a diocese.


=== Initial Norman structures ===
After the Cambro-Norman invasion removed the Uí Dúnlainge dynasty from power in 1170, Diarmait Mac Murcada's Norman allies led by Strongbow divided Kildare amongst themselves: the Barony of Carbury to Meyler FitzHenry, Naas Offalia to Maurice Fitzgerald, Norragh to Robert FitzHereford and Salt (Saltus salmonus – Salmon Leap, i.e. Leixlip) to Adam FitzHereford. In 1210 Kildare became one of original twelve Norman counties of Ireland, originally known as the "Liberty of Kildare". The Normans introduced the feudal system which was the usual landholding system in western Europe at the time.
In 1247 the estate of Anselm Marshall was subdivided, Kildare was assigned to Sybilla (fourth daughter of William Marshall and Isabella, heiress to Strongbow and Aoife). Sybilla was already dead so the "Liberty of Kildare", including what is now counties Laois and Offaly, passed to her daughter Agnes and husband William de Vesci. In 1278 the "Liberty" (later County) of Kildare was restored to Anges de Vesci. On her death in 1290 her son William succeeded to the Lordship of Kildare.


=== Beginning of the County ===
In 1297 William de Vesci surrendered the "Liberty of Kildare" to the English crown. "County Kildare" came into being and was defined as such by an Act of the new Irish Parliament of Edward I.
Shortly afterwards De Vesci fled to France, leaving the FitzGeralds of Maynooth to become the pre-eminent family in the county. John FitzThomas FitzGerald, 5th Baron of Offaly, was created the first Earl of Kildare on 14 May 1316.
The Norman settlers also had their own literature. In 1200–25 the "Song of Dermot and the Earl" was drafted in Norman-French, and mentioned parts of Kildare. Soon after 1300 the "Kildare Poems" were written in medieval English.,


=== Changes in boundaries ===
The 1297 boundaries of County Kildare included much of the present counties Offaly and Laois. These were shired as King's and Queen's Counties in 1556.


=== Final form ===
County Kildare assumed its current borders in 1836 when it was reassigned three detached sections of County Dublin (including Ballymore Eustace) and one detached district of King's County (the western Harristown and Kilbracken), while a detached district of Kildare, around Castlerickard, was reassigned to County Meath.


== Monastic houses ==
The establishment of a Cistercian Abbey at Monasterevan by the O'Dempsey's in 1189 and an Augustinian priory in Naas in 1200 brought a new monastic tradition to Kildare. In 1202 Great Connell Priory Augustinian priory, set to become one of the finest in medieval Ireland, was founded by Meyler FitzHenry. In 1223 the last Gaelic bishop of Kildare, Cornelius MacFaelain, was succeeded by Ralph of Bristol and control of the church remained in Norman hands. In 1253 a Dominican friary was established at Athy and in 1302 a Franciscan abbey at Castledermot. In the early 14th century, the Kildare Poems, comprising some of the earliest written documents of English in Ireland, are thought to have been composed by Franciscan monks from Kildare.


== The Fitzgeralds ==
In the years leading to the ascendancy of the FitzGerald family (1470–1535) Kildare came virtual capital of Ireland. The Irish Parliament sat in Naas on twenty occasions between 1255 and 1484, and there were also sittings in Kildare in 1266–67 and 1310, 12 in Castledermot between 1264 and 1509, Ballymore Eustace in 1390 and Great Connell Priory in 1478. English King Richard II took the submission of Irish chiefs at Great Connell Priory Augustinian Priory in 1395. in 1481, Gerald FitzGerald, Gearóid Mór, eighth earl of Kildare, was appointed English King's Deputy in Ireland by Edward IV. The principles of the county, Edmond Lane, Bishop of Kildare, the Prior of Great Connell Priory and Gearóid Mór all assisted in the coronation of the Yorkist pretender Lambert Simnel in Dublin but were pardoned by the new king Henry VII after Simnel's defeat.
In 1488 Gearóid Mór became one of the first to use guns in Ireland, importing six handguns from Germany for his personal guard and using cannon to destroy Balrath Castle in County Westmeath. When he was established in 1496 as Lord Deputy of Ireland, English King Henry VIII's man in Ireland, the king allegedly said "if all Ireland cannot rule this man, let him rule all Ireland." In 1504 Gearóid Mór defeated Clanricard and O Bríain at Knockdoe, Co Galway, the most important battle of his career. Gearóid Mór rebuilt Athy castle to secure his southern frontier in 1506 but died in Athy in 1513 from gunshot wounds received in an engagement with O'Mores and was succeeded by Gearóid Óg. Gearóid Óg established Ireland's first University at Maynooth in 1518.
Even at the supposed height of their power, accusations by rivals that the family was plotting against Henry VIII bedeviled the FitzGerald dynasty. Gearóid Mór spent two years and Gearóid Óg 11 years in all as the King's prisoner in the Tower of London. In 1534 Gearóid Óg was recalled to London once more (February), leaving his 20-year-old son Silken Thomas in charge. Thomas declared rebellion (11 June) on false information that his father had been executed. In 1535 Maynooth Castle, stronghold of Silken Thomas, was bombarded by cannon for 18 days and taken by William Brereton. Rathangan castle was also taken before Thomas submitted in October. Despite a guarantee of personal safety, Silken Thomas and five uncles were executed in the Tower of London in 1537. Thomas's younger brother Gearóid was smuggled to Tuscany. The FitzGerald lands were confiscated and the biggest share-out of Kildare land since the Cambro-Norman conquest took place. In 1552 Gearóid the only survivor of FitzGerald family, was restored to his ancestral title and possessions.


== Religious change ==
After King Henry VIII broke with the Roman Catholic Church in 1533 after his decision to remarry, the Pope appointed Franciscan Dónall O Bóacháin bishop of Kildare. When he died almost immediately Thady Reynolds was appointed and initially recognised by Henry VIII. Reynolds refused to break with Rome in common with most Irish bishops and while he continued to minister Henry VIII appointed William Miagh in opposition as the first Protestant bishop of Kildare. Some later documents refer to his 1550 successor Thomas Lancaster as the first Protestant bishop, partly because he was Kildare's first married bishop and partly because Henry VIII also disliked Lutherans until his death in 1547. By 1550 Edward VI was formulating a more Lutheran state religion.
When the English crown turned back to Catholicism under Queen Mary in 1555–58, Thomas Leverous became the first native Kildare bishop in 400 years, being of Norman descent. In 1558 the new queen, Elizabeth I, ascended the throne and as Leverous refused to take the Oath of Allegiance he was deprived of his see. Pope Pius V, in his papal bull Regnans in Excelsis, finally declared Elizabeth to be an illegitimate heretic in 1570, and from this point on it became harder for Kildare's landed families, most of whom were Catholic, to be simultaneously loyal to the queen and also to be observant Catholics. Kildare's numerous Norman families became known as Old English, to distinguish them from newer arrivals who conformed to the state religion.


== Elizabethan Kildare ==
Queen Elizabeth I granted charters to Naas in 1568 and Athy in 1613. In 1576 the earliest record of grazing rights on the Curragh named Robert Bathe as the beneficiary. In 1580, during the Second Desmond Rebellion, 200 Spaniards who had arrived in Smerwick in the Dingle Peninsula as part of the 1579 Papal invasion force and marched to Naas were massacred by the English crown forces at Fód Spáinigh. In 1581 Catholic martyrs Fr James Eustace and Fr Nicholas FitzGerald were executed in Naas.


== Wars of the 1640s ==

Kildare suffered greatly in the civil wars of the 1640s that ravaged both Ireland and Britain -see Wars of the Three Kingdoms. Lord Deputy Thomas Wentworth came to reside at the uncompleted Jigginstown House in Naas, Ireland's first royal palace, in 1637. When he was recalled and executed in 1641 it remains unfinished and today only the basement is still standing.
The wars began in Ireland with the Irish Rebellion of 1641 that broke out in October of that year. The early fighting in Kildare saw small bands of Irish Catholic rebels attacking English troops and Protestant settlers, followed by a punitive English expedition led by the Earl of Ormonde. In early 1642 Ormonde led out his royalist forces to subdue Kildare; burned the town of Lyons Hill, gave up Naas to his soldiers to plunder, reduced Kildare cathedral to ruins through cannon fire and sent parties to burn Kilcullen, Castlemartin, and "all the county for 17 miles in length and 25 in breadth". Butler garrisoned Naas and then defeated the Confederate Irish forces under Lord Mountgarret in the Battle of Kilrush (15 April). When Father Peter Higgins of Naas was hanged, he became the county's third famous Catholic martyr.
In May 1642, the landed Catholic rebels set up their own government at Kilkenny known as Confederate Ireland. Most of the Kildare landowners participated in this assembly. The English position was weakened by the outbreak of the English Civil War, the recall of many of their troops and the split of the remaining forces between Royalists and Parliamentarians.
The Parliamentarians were the more hostile faction to the Confederates and a truce known as the first Ormonde Peace, a ceasefire between Royalists and Irish Confederates, was signed at Jigginstown House in Naas (15 Sept). The ceasefire broke down in May 1646 and Confederate forces marched through Kildare to besiege Dublin. The Royalists then handed the capital over to Parliamentarian troops in 1647 and the Confederate armies tried to eliminate this hostile force. Owen Roe O'Neill took Woodstock Castle in Athy briefly in 1647. Thomas Preston also took Maynooth castle in that year and hanged its garrison. However, Preston's Leinster army was destroyed, losing 3000 killed at the battle of Dungans Hill, on the road between Maynooth and Trim in August 1647, crippling Confederate power in the area. Kildare landowner and Confederate cavalry officer Garret Cron Fitzgerald was killed early in the battle. In 1648 Owen Roe O'Neill refused to ally his army with Ormonde's royalists and the moderate Confederates, and engaged in a brief war with them which fatally weakened the Confederate cause.
In 1649, Oliver Cromwell landed in Dublin with over 10,000 Parliamentarian troops and began a thorough re-conquest of Ireland. In 1650 Naas and Kildare surrendered to Cromwellian forces. Cromwell's Dublin-based commander John Hewson took Ballisonan Castle by force. Athy and Castledermot were captured without opposition.


== Lands redistributed ==
The first major map of Kildare, The Down Survey was completed in 1656. It served as the basis of more redistribution of land confiscated after the Cromwellian conquest, in line with the Adventurers Act (see also Plantations of Ireland). After the Treaty of Limerick in 1691, further estates in Kildare forfeited included those of Talbot, Dongan, Tyrrel, Eustace, Trant and Lawless who continued to support the losing Jacobite cause. The best known buyer of land from the new grantees was the Donegal-born lawyer and estate agent, William Conolly, who built what was then the largest private house in Ireland at Castletown House, Celbridge in 1722–28.


== Diocese of Kildare ==
The Catholic diocese of Kildare first united with Leighlin Diocese to the south in 1676 when Mark Forstall, bishop of Kildare, was also appointed administrator of Leighlin by St Oliver Plunkett. He was arrested in 1678 and again in 1681 for 'having exercised papal jurisdiction.' The union was formalised in 1694 when John Dempsey was appointed bishop of Kildare and administrator of Leighlin, despite penal laws. The last Catholic bishop to reside in Kildare was James Gallagher, much of it in hiding near the Bog of Allen. His Sixteen Irish Sermons (1736) is the major Irish language theological work of the age and has gone through 14 editions by 1820. The Anglican/Episcopalian Diocese of Kildare merged with Dublin in 1846 after the death of the last Church of Ireland bishop of Kildare, Charles Dalrymple Lindsay. In 1976 the Church of Ireland diocese of Kildare separated from Dublin and joined to Meath.


== Georgian Kildare ==
Kildare enjoyed prosperity during the 18th century, as the focus of economic life turned to the large landed estates and market towns. The Earl of Kildare purchased and started reconstruction of Carton House near Maynooth in 1739. Henry Boyle Carter purchased and started reconstruction of Castlemartin near Kilcullen in 1730. The running of horse races on the Curragh, well established for centuries, was formalised in 1717 when the duties of the Ranger of the Curragh were extended to supervising "the proper conduct of the King's Plate". Maps of the county compiled by Noble & Keenan in 1753 and Alexander Taylor in 1783 show the advent of arterial drainage and the boglands of the north west of the county being reclaimed for agriculture.
Turnpike (toll) roads were laid from the 1730s, largely in line with today's main roads. In the late 1700s the grand canal and the Royal Canal passed through the county on the way from Dublin to the Shannon. The county was run by landowners on the grand jury system. While much of Ireland had a problem with absentee landlords living and spending their rents mostly in Dublin or London, most Kildare landlords lived on their land and reinvested more of their income locally.


== Constituencies ==
In the Parliament of Ireland (1297–1800), by 1684 Kildare was represented by two men for Kildare County, and two each for the boroughs of Naas, Kildare, Athy and Harristown. Therefore, the county had 10 seats in the 300-seat Irish House of Commons.
In the Parliament of the United Kingdom of Great Britain and Ireland (1801–1918) Kildare became
the single constituency of Kildare in 1801–1885, returning 2 members;
two constituencies of North Kildare and South Kildare, returning one member each;
In 1918 both elections were won by members who sat in the First Dáil
From the 1921 election and the creation of the Irish Free State the county has been merged with other constituencies, or has been divided:
Kildare–Wicklow 1921–22
Kildare 1923–37
Carlow–Kildare 1937–48
Kildare 1948–97
Kildare North 1997–
Kildare South 1997–


== Industrial Revolution ==
Industrial projects were started by largely Quaker families at Ballitore by Abraham Shackleton in 1726 while Robert Brooke was assisted by a £25,000 grant from the Irish Parliament in building a cotton mill and town of 200 houses at the newly named town of Prosperous in the 1780s. Turnpike roads were built from the 1730s. John Wynn Baker opened Kildare's earliest factory, manufacturing agricultural instruments at Loughlinstown, Celbridge in 1764. John Cassidy established a distillery in Monasterevan in 1784. In 1729 Ireland's first turnpike road was created from Dublin to Kilcullen. In 1756 the year that construction work on the Grand Canal commenced in the north of the county. 31-year-old Celbridge-born brewer Arthur Guinness leased a brewery at Leixlip in 1755 and bought a second brewery at St James's Gate in Dublin. In the 1790s the Royal Canal was dug from Dublin along the north of the county and the first railways were laid in the 1840s.


== Population growth ==
Early estimates of Kildare's population include GP Bushe's 1788 return of the number of households in Kildare at 11,272 (population afterwards estimated at 71,570) and DA Beaufort's household returns of 11,205 in 1790, and estimated population at 56,000. Mason's Statistical Survey of 1813 calculated the number of households at 14,564, and the population at 85,000 with figures for towns: Athy 3,192, Naas 2,018, Maynooth 1,468, Kildare 1,299. The first census in 1821 recorded a population of 99,065 (Athy 3,693, Naas 3,073, Kildare 1,516, Maynooth 1,364).


== University ==
Maynooth, which had been the site of Ireland's first 'college' in 1518, was re-established by the government as a seminary for Catholic lay and ecclesiastical students in 1795, with Kildare-born Fr John Chetwode Eustace among first professors. In 1817 Maynooth's lay college closed and it functioned solely as a Catholic seminary for 150 years. In 1910 it became a constituent college of the National University of Ireland and reopened for lay students in 1967. Nobel Peace prize winner John Hume is among its alumni. In 1812 Clongowes Wood College near Clane was founded by the Jesuit order as a centre for second-level education. James Joyce and three Taoisigh of the Republic are among its alumni.


== Canals ==
Work on the Grand Canal began in 1756 and it reached the Kildare border in 1763. In 1779 the first section of Grand Canal was opened to goods traffic, from Dublin to Ballyheally, near Celbridge and in 1780 to passenger boats. Ten years later the Naas branch of the Grand Canal completed. The canal reached Tullamore in 1784, and a southern branch known as the Barrow navigation reached Athy in 1791.
Work began on the Royal Canal in 1789 and it reached Kilcock in 1796, but this more northerly line was never a commercial success.
Traffic on the Grand Canal peaked at 120,615 passengers in 1846 and 379,045 tons of cargo in 1865. The canal was motorised in 1911–24 and closed for commercial traffic in 1960. The Grand Canal remains open for pleasure boats and restoration of the Royal Canal was completed in 2006. Both were seriously affected by the advent of railways in Kildare from the 1840s.


== 1798 rebellion and Emmet rebellion of 1803 ==
See also Irish Rebellion of 1798
Support in Kildare for the United Irishmen's revolutionary democratic movement at the time of the 1798 rebellion has been estimated at 10,000. It has also been suggested that Valentine Lawless who inherited Lyons near Ardclough was a prominent member of the government in waiting should the rebellion succeed. United Irish leader and later informer Thomas Reynolds lived at Kilkea, Lord Edward Fitzgerald returned to Maynooth in 1796 to organise the United Irishmen and Theobald Wolfe Tone was buried at his godfather's family plot at Bodenstown. In the years leading up to the rebellion there were anti-militia riots in Kilcullen and Ballitore. Lawrence O'Connor was executed in Naas for plotting against the English administration in 1795. In December 1797, 1,500 guns and 3,000 bayonets were captured on a boat on the canal at Athy.
The first shots of the 1798 rebellion were fired in Kildare. On 23 May, the signal for rebellion given when mail coaches were seized at Johnstown and Maynooth. Kildare rebels attacked Kilcullen Prosperous, were repulsed at Naas and Clane, and a force under William Aylmer was eventually defeated at the battle of Ovidstown on 18 June. 350 surrendering prisoners were slaughtered in the Gibbet Rath massacre at the Curragh despite an initially successful effort by General Dundas to defuse the rising with a policy of mass pardons. In turn, the two loyalist garrisons at Rathangan were also slaughtered after surrendering. The fighting in Kildare did not end until the surrender of William Aylmer in mid-July.
In 1803 Kildaremen recruited by Michael Quigly participated in a brief United Irish uprising organised by Robert Emmet. Maynooth was the only town successfully seized by the rebels ( 23–25 July) and Kildare troops under Nicholas Gray marched to Thomas Street in Dublin to participate in the ill-fated rebellion. Emmett's uniform was later found at Rathcoffey. The most prominent victim of the Emmet rebellion, Arthur Wolfe, Lord Kilwarden, was buried at Oughterard in Ardclough.


== Military camp ==
One outcome of the rebellion was the establishment of a temporary military encampment at the Curragh in 1805. In 1816 a new town came into being with the building of a military barracks near a bridge over the Liffey – it was to be called Newbridge. In 1855 a permanent encampment was built for 10,000 infantry on the Curragh.


== Local politicians ==
Kildare had ten parliamentary representatives in old Irish House of Commons – two for the Kildare county and two members each from Athy, Harristown, Kildare Borough and Naas. Two of the most powerful figures in 18th century politics resided in the county, Speakers of the house William Conolly at Castletown House near Celbridge and John Ponsonby at Bishopscourt near Kill. The post-1801 Act of Union Kildare county constituency had two seats in the British House of Commons. The La Touche and Fitzgerald families controlled local politics through the first half of the 19th century until challenged by Balyna-born Richard More O'Ferrall. Naas Corporation, countrolled by the Bourke family, was dissolved in 1840. In 1898 Stephen J Brown was elected first chairman of the first Kildare County Council to be directly elected. With the rise of the Home Rule movement and the establishment of a nationalist newspaper, the Leinster Leader in Naas in 1884, William Cogan and Otho Fitzgerald were succeeded by Home Rule members of parliament Charles Henry Meldon, James Leahy and James Carew, owner of the Leinster Leader and founder of the Irish Independent newspaper.


== Railways ==

The first sod on the new railway line from Dublin to Cork was turned at Adamstown near the Dublin-Kildare border in January 1846. By June the line had been completed to Sallins. The first train ran to Carlow in 1846 and to Cork in 1850. The third worst rail accident in Irish history occurred at Straffan Station in 1853, when a goods train ran into the back of a stationary passenger train killing 18 people, including a nephew of Irish political leader Daniel O'Connell. As rail traffic declined Straffan Station was closed in 1947 and Hazelhatch and Sallins stations in 1963. Kildare was also served by the Tullow Extension, running south from Naas, through Harristown (for that area and Kilcullen) and on to Tullow in County Carlow.

In 1995 a section of the line was opened for a new Dublin area commuter service, the Arrow, and Sallins and Hazlehatch stations reopened as part of the "Southwestern Commuter" line. Another reopened line runs westwards, serving Leixlip, Maynooth and Kilcock, continuing towards Enfield, County Meath.


== Sporting Revolution ==
The Turf Club was founded at the Curragh horse racing circuit in 1790 to regulate the racing of horses, but attempts to establish an Irish 1000 guineas in 1815 and an "O'Darby Stakes" in 1817 were unsuccessful until the most important flat race in the country, the Irish Derby was established on an annual basis from 1866 on. The Turf Club regulated to famous bare knuckle contests involving Dublin prize fighter Dan Donnelly against Tom Hall in 1814 and George Cooper in 1815, drawing estimated crowds of 20,000 to the Curragh. In 1846 the first railway excursion organised for a sporting event worldwide ran on the new Great Southern and Western Railway line to Curragh races. The first annual ball of the Kildare hunt was held in 1860, soon to become the social event of the year in the county. Punchestown Races were reorganised and reconstituted as 'Kildare and National Hunt Steeplechases' in 1861. The first day of the 1868 meeting attracted an estimated 150,000 spectators.


== Athletes and horses ==

Cricket clubs were established from the 1850s on and Ireland's first golf course laid out on the Curragh in 1852 by Musselburgh club member David Ritchie. In 1871 County Kildare Cricket Club was formed "for the promotion of cricket, football, archery, pigeon shooting, lawn tennis and, if possible, polo. Kildaremen winning sporting fame in the USA included Clane-born Jack Kelly, alias Jack (Nonpareil) Dempsey who won the world middleweight boxing title in 1884 in Great Kills, New York, held the title for seven years and inspired a later heavyweight boxer to borrow his name. In 1893 Clane born Tommy Conneff ran a new world mile record of 4 minutes 17.8 seconds, a record that was to stand for 20 years. In 1903 the fourth Gordon Bennett Cup Motor Race staged in Athy, setting new speed records of over 60 MPH. The GAA was established in the county in 1887 and Kildare GAA helped establish Gaelic football as a major sport meeting Kerry three times in 1903 GAA All Ireland "home" final attracting attendances of 12,000, 18,000 and 20,000.
In 1995 the annual staging of the European Open golf tournament was moved to the K Club at Straffan from Birmingham and the course staged the Ryder Cup in September 2006.
In 2000 Kildare was designated the "Thoroughbred County" by its county council in recognition of its equine tradition. In 2000 Kildare-trained racehorses won the leading races in England and Ireland over jumps and on the flat, Ted Walsh from Greenhills, Kill won the Irish (Comanche Court) and English (Papillon) Grand Nationals while Sindaar, trained by John Oxx on the Curragh, won the Irish and English Derbies. Ted is the father of jockey Ruby Walsh. Kildare's reputation as a stud capital was undamaged by the high profile kidnap of the English Derby winner Shergar in 1983.


== A New State ==

Kildare did not participate in the Fenian rebellion of 1867, though John Devoy was born at Kill. Incidents in the Land War such as the Clongorey evictions politicised the largely agricultural county and one of the first politicians elected to the new Irish parliament, Dáil Éireann in 1922, Hugh Colohan, was a veteran of the Clongorey campaign. Several Kildare politicians have held high rank since independence including Dónal Ó Buachalla, last Governor General of the Irish Free State, who had led a column of volunteers from Maynooth to participate in the 1916 Easter Rising, Art O'Connor, appointed Minister for Agriculture by the first Dáil in 1919 and briefly leader of Sinn Féin after Éamon de Valera founded Fianna Fáil in 1926 before he, too, joined Fianna Fáil, William Norton leader of the Labour Party 1932–60 and Tánaiste 1948–51 and 1954–57, Alan Dukes leader of Fine Gael 1987–90 and Minister for Finance 1982–86, Gerry Sweetman Minister for Finance 1954–57, Charlie McCreevy Minister for Finance 1997–2004 and later EU commissioner, and Paddy Power Minister for Forestry and Fisheries 1979–81 and Defence 1982.


== Towns and trends ==
Kildare's population plunged to a low of 57,892 in 1936. Athy, Kildare's most populous town since records began, was briefly overtaken by Naas as Kildare's largest in 1901 (Naas 3,836, Athy 3,599) but regained its position by a small margin in 1926. By 1956 Newbridge was the largest town with a population of 4,157, (Athy 3,948, Naas 3,915). In 1986 Leixlip became the largest town, and Celbridge was recorded as the fastest growing town in Ireland. Naas was the largest town in 1996 only to be overtaken by Newbridge again in 2002 when the census recorded a highest ever population of 163,995 for the county, a 21.5pc increase on 1996. Infrastructural projects helped change the demographics of the county. The Kildare leg of the dual carriageway to Naas opened in 1963 and was followed by Ireland's first section of motorway, the Naas Bypass in 1983, the Newbridge bypass (1993), Kildare bypass (2003) and Monasterevan bypass (2004) on the M7, the Maynooth bypass (1994) and Kilcock- Kinnegad bypass (2005) on the M4, and the Kilcullen by-pass (1994) on the M9.


== Bibliography ==
"Journals of the Kildare Archaeological Society",


== References ==


== External links ==
Kildare.ie