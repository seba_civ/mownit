The Mikulich General Store is a commercial building located at the junction of County Roads 1 and 44 in Limestone Township, Traunik, Michigan. It was listed on the National Register of Historic Places in 1993 and designated a Michigan State Historic Site in 1987.


== History ==
In the early 1900s, Traunik developed as the center of a large Slovenian community, who settled on land that at the time had been recently logged. In 1922 William J. Kehoe, a logging camp operator and former teacher, purchased 2.6 acres in Traunik for $75. Kehoe hired a local Native American carpenter known as "Wild Bill" Copeland (or Coplin) to build a store on the site; construction took place in 1922-23. However, soon after the store opened, a new school was built nearby, and Kehoe resumed his teaching career. Kehoe sold the store to John Knaus Sr., who sold the store again in 1926 to Louis Mikulich Sr. In 1927, Mikulich became postmaster of Traunik and remodelled the store and opened a post office there. The Mikulich family lived on the second floor, and the store soon became the social and economic center for the community.
Lousi Mikulich operated the store unilt 1961, when his son, Louis Mikulich Jr., took it over. Louis Mikulich Jr., operated both store and post office until 1987, when he sold it to Delayne (Dee) K. Morgan. Dee and Bill Morgan spent five years rehabilitating the building, and opened "Morgan's Country Store and Museum" in 1992. The store later changed hands again, opening as "Lily's of Traunik," a cafe and health food store, in 2008. The store was sold again in 2010, and after extensive renovations and updating, reopened in September 2013 as the Mikulich General Store & Deli by David and Laura Coleman


== Description ==
The Mikulich General Store is a two-story rectangular building with a flat roof. The front facade has a recessed central entrance with two large display windows on each side. The store likely had some form of wooden siding originally; it was covered with asphalt siding in the 1920s, and later with clapboards when the asphalt began to deteriorate. The building originally rested on wooden posts, which were replaced with a concrete foundation in the 1980s. Inside, the lower floor housed the general store and post office (the original post office remains intact) and the upper floor housed living quarters for the owner. The site of the store also includes a privy, one-story grain and fuel shed, and a two-car garage and poured concrete tennis court from the 1930s.


== References ==


== External links ==
HISTORICAL MIKULICH GENERAL STORE