Kyburz (formerly, Slippery Ford and Slipperyford) is a small unincorporated community in El Dorado County, California. It is located along the South Fork of the American River and U.S. Route 50, and is surrounded by the El Dorado National Forest. Its elevation is 4058 feet (1237 m) above sea level and it is named for Albert Kyburz, a former postmaster.
The Slippery Ford post office opened in 1861; the name was changed to Slipperyford in 1896, and to Kyburz in 1911, but no one knows why.
Ski racer Spider Sabich grew up in Kyburz, where his father Vladimir, Sr., was stationed with the California Highway Patrol. Spider and his younger brother Steve raced at the Edelweiss ski area, which closed in the 1960s, and is now known as Camp Sacramento.


== Demographics ==
As of the 2000 Census the population was 167, although the road sign on Highway 50 states 139 as of 2008. Including 86 males and 81 females, 65 males age 18 and older, 66 females age 18 and older, 11 males age 65 or older, and 16 females age 65 or older.
There were 5 people under age 5, 8 people age 5 to 9, 12 people age 10 to 14, 14 people age 15 to 19, 4 people age 20 to 24, 16 people age 25 to 34, 21 people age 35 to 44, 30 people age 45 to 54, 14 people age 55 to 59, 16 people age 60 to 64, 12 people age 65 to 74, 11 people age 75 to 84, 4 people age 85 and older.
The median age was 45.5 and people age 18 and older numbered 131, age 21 and over 128, and age 65 and over 27.


== Notes ==


== External links ==
Kyburz News
MSRmaps.com - USGS topo map & aerial photo