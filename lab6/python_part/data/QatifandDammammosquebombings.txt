The Qatif and Dammam mosque bombings occurred on 22 and 29 May 2015. On Friday May 22, a suicide bomber attacked the Shia "Imam Ali ibn Abi Talib Mosque" situated in Qudeih village of Qatif city in Eastern Province, Saudi Arabia. The Islamic State of Iraq and Syria claimed responsibility for the blast, which killed at least 21 people. The event is the second deadly attack against Shia in six months.
Saudi Minister of Health Khaled Al-Falih, accompanied by senior health officials, visited the injured persons who were hospitalized in the Qatif Central Hospital. The Minister instructed the hospital management to provide the best treatment to those injured in the attack at Al-Qudaih’s Ali Bin Abi Talib Mosque during the Friday prayer on 22 May 2015 in which 21 worshippers lost their lives and 88 people were injured. Al-Falih prayed for speedy recovery of the wounded persons and conveyed condolences on behalf of King Salman, the Custodian of the Two Holy Mosques, to the family members of those killed in the blast.


== Background ==

An estimated 14% to 15% of the approximately 16 million natives of Saudi Arabia are Shia Muslims, mostly living in the oil-rich areas of the Eastern Province where Qatif is located. The government of Saudi Arabia follows the strict Sunni Islamic "Wahhabi movement", which dominates religious institutions, courts and education of the kingdom and believes that Shia Muslims are not true Muslims; thus Shia have alleged severe discrimination in Saudi Arabia. According to a 2009 Human Rights Watch report, Shia citizens in Saudi Arabia "face systematic discrimination in religion, education, justice, and employment". The report alleged widespread discrimination against Saudi Shia, including restrictions in the state education system, where Shia students were forbidden from learning about their religion and told they were unbelievers by Sunni teachers. Judges often ban Shia witnesses during trials because of their faith and bar Shia from taking jobs in government ministries or the military. In the weekly sermons in the mosques, Shiites are regularly denounced has heretics and infidels.
At the time of the incident, the Syrian civil war spawned the creation of ISIS, which also gained prominence in Iraq before spreading to other places in the region. Saudi Arabia was also partaking in bombings during the Yemeni civil war. In November 2014, eight Shia worshippers were killed by gunmen during Ashura celebrations at a shrine in the city of al-Ahsa. Jafar Al Shayeb, a member of the Qatif municipal council, says that sectarian tensions have already risen sharply as a result of the war Saudi Arabia’s new King Salman is waging against the Houthis and that divisive rhetoric from Wahhabi preachers was increasing. He believes that the situation for Saudi Shia was about to get worse.
Critics point out that the government has done nothing to address rising sectarian tensions in the country. The country is built on the Wahhabi creed of Islam, whose ideology shares many similarities with that of ISIS. Hours before the bombing, one imam in Riyadh was quoted as telling devout worshippers at the end of Friday prayers: “Allah, attack all the Shia everywhere; Allah, send them earthquakes; Allah, kill them all.”


== Bombing ==
In Saudi Arabia, several attacks against Shia Muslim Minority were reported, however, suicide explosion in Imam Ali ibn Abi Talib Mosque is one of the deadliest attack. Reportedly 150 people were present in the mosque for Friday prayers. The mosque suicide attack killed 13 people. This kind of suicide attack has been seen many times in Pakistan, Iraq and other Muslim countries, but it is first time in the history of Saudi Arabia that such an attack occurred on a mosque during prayers.


== Responsibility ==
In an online statement, Islamic State of Iraq and the Levant has claimed responsibility, saying their soldiers were behind the attack at the Imam Ali Mosque and that one of their suicide bombers, identifying him as Abu Amer Al-Najdi, had detonated an explosive belt. An ISIL Twitter account posted an image of a suicide bomber they say was involved in the attack. Saudi authorities, however, identified Saudi national Salih bin Abdulrahman Salih Al Ghishaami as the suicide bomber.


== Reactions ==
A large number of people protested in Qatif to denounce the terrorist attack and chanted slogans to show their anger at the government, Jafar Al Shayeb, a member of the Qatif municipal council, said.
Doctor Fahd bin Saad al-Majed, General Secretary of the Council of Senior Scholars, Saudi Arabia has condemned the attack and clarified that aims of terrorists to harm the unity of the Saudi people and to destabilize the kingdom, could never be successful. Saudi Arabia’s top cleric, Grand Mufti Abdul-Aziz ibn Abdullah Al ash-Sheikh, has also condemned the attack and described the terrorist act as “a crime, shame and great sin”.
A statement from UN Secretary General Ban Ki-moon condemned the attack and said “such attacks on places of worship are abhorrent and intended to promote sectarian conflict.”
 Lebanon: Hezbollah issued a statement condemning the attack, and also saying the authorities in the kingdom bore responsibility. "Hezbollah holds the Saudi authorities fully responsible for this ugly crime, for its embrace and sponsorship for these criminal murderers ... to carry out similar crimes in other Arab and Muslim countries. Saudi authorities incited against Shia citizens from the minbars (pulpits) of mosques and in the media,” the statement says.
 Iran: Foreign Ministry spokeswoman Marziyeh Afkham condemned the attack and urged the government of Saudi Arabia to arrest those involved in the incident.
 Pakistan: Prime Minister Nawaz Sharif and President Mamnoon Hussain condemned the bombing. Foreign Office spokesman Qazi Khalilullah in a statement condemned the attack and said “We share the grief of the brotherly people of the Kingdom of Saudi Arabia over this cowardly and deliberate attack against innocent civilians." Pakistan Ambassador Manzur Ul-Haq condemned the bombing and stated further that Pakistan strongly condemned the brutal act which caused loss of lives of many people and harmed the several innocent. To share solidarity Pakistan Embassy also postponed their evening community function already scheduled on that day.
 Bangladesh: Bangladesh's Ambassador Golam Moshi condemned the attack stating that the terrible crime is unbearable and we are ready to cooperate with Saudi Arabia to maintain peace and security.
 Sri Lanka: Sri Lankan Ambassador Mohamed Hussein Mohamed condemned the attack, and calling for those responsible to be brought to justice.
 United Arab Emirates: has condemned the terrorist attack that targeted worshippers in a mosque in Qudayh. In a statement Dr. Anwar bin Mohammed Gargash, Minister of State for Foreign Affairs reaffirmed that UAE discards all kinds of terrorism without any distinction of religious and moral values. He also stated that “Such a heinous crime requires the international community to rally efforts to confront such cowardly acts and devious thoughts, which pay no respect to human lives and sacred places".
 United States: White House spokesman Josh Earnest condemned the attack and said, “We mourn the loss of life and condemn this violence.”


== Funeral procession ==
Half a million people participated in funeral processions for victims of explosion in Ali Bin Abi Talib Mosque in Al-Qudaih town of Qatif governorate in the Saudi Eastern Province on Friday, the 22 May 2015. The victims were buried following funeral prayers held in the Qadeeh market square on 25 May. Black flags of mourning flew in the streets of Qatif, where police mounted checkpoints while volunteers in bright yellow and orange vests inspected vehicles. The procession stretched for over three miles from different settlements in the province to the burial site to show respect for the victims bombing which led to the killing of 21 worshipers, including two children. The huge rally comes despite warning by extremists that they may again launch an attack on Shia gatherings. Even women and children declared their willingness to attend in huge numbers, although they were warned to stay away from the main gathering of mourners. The bodies were carried on litters decked with flowers in a final procession towards the cemetery in Qudaih village.


== Dammam Mosque bomb blast ==
According to Saudi Kingdom’s official news agency a suicide bomber on Friday the 29 May 2015 has blown himself up in the parking lot of a Shia mosque in Saudi Arabia's city of Dammam a coastal city about 70 kilometers (45 miles) from the Persian Gulf. Reportedly four people have died after a suicide bomber targeted a Shia mosque Imam Hussain in Saudi Arabia’s eastern province which is the second attack of its kind in a week, fuelling fears of an organised campaign by Islamic State of Iraq and the Levant to foment sectarian tensions inside the conservative Sunni kingdom. ISIS quickly claimed that the latest attack had been carried out by one of their “soldiers of the Caliphate” identifying the suicide bomber as Abu Jandal alJazrawi.


=== Dammam Mosque Victims and attacker ===
Preliminary reporting revealed that the explosion was a result of car bomb blast, however, Saudi Arabian Interior Ministry later confirmed that the explosion happened when a person wearing female clothes blew himself up using an explosives belt at the mosque’s gate as the security men were approaching him to check his identity. Interior Ministry spokesman also stated that those “martyred” in the attack were:
(1) Abdul-Jalil Al-Arbash,
(2) Mohamed Al-Eissa (cousin of Jalil Al-Arbash )
(3) Mohamed Jomaa Al-Arbash (Elder brother of Jalil Al-Arbash)
(4) Abdul Hadi Salman Al-Hashim
The three injured victims were identified as (i) Ahmed Abdullah Al-Abdul Karim (ii) Hassan Ali Al-Sagheerat (3) Hassan Al-Nijaidi.
The Saudi Arabian interior ministry also disclosed the identity of the attacker as 19-year-old Khalid Ayed Mohammed Wahhabi Shammari.

According to Middle East Eye Net and Shia Post, a male attacker dressed in traditional female clothing as a cover-up, after finding the female entrance closed, had attempted to enter the mosque from male entrance. The attacker was challenged by a young Shia volunteer, Abed AlJalil Al-Arbash, at Masjid Imam Hussain who dared to Stop the Suicide Bomber at the Parking Lot Gate and foiled the suicide attack by sacrificing his own life. Per sources of KSN News (KSNW-TV) Abduljaleel’s older brother, Muhammed Jumah Alarbash, has also died on Friday night from injuries he sustained in the terror attack on 29 May 2015.


=== Alarbash an Engineering student at WSU called Saudi hero ===
The Wichita State University (WSU) community shocked when aware of the tragic death of one of their students Abed Al-Jaleel Alarbash who sacrificed his life while preventing a suicide bomber from entering the Imam Hussain mosque in Saudi Arabian city of Dammam on last Friday of May 2015. Alarbash, 22, was an undergraduate student in electrical engineering at WSU, had returned to Saudi Arabia to get married. He is being hailed as a hero who gave his life to keep a suicide bomber away from a crowded mosque and saved the lives of hundres. WSU community offered its condolences to his family, friends and colleagues. One of Alarbash’s professors Preethika Kumar disclosed that she got to know Alarbash well this past semester while he was enrolled in one of her courses. She remembers him as a kind person who was always happy and cooperative.


== See also ==
2015 Arar attack
Najran shelling
Shia Islam in Saudi Arabia
Human rights in Saudi Arabia
Kuwait mosque bombing


== References ==