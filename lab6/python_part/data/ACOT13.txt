Acyl-CoA thioesterase 13 is a protein that in humans is encoded by the ACOT13 gene. This gene encodes a member of the thioesterase superfamily. In humans, the protein co-localizes with microtubules and is essential for sustained cell proliferation.


== Structure ==
The orthologous mouse protein forms a homotetramer and is associated with mitochondria. The mouse protein functions as a medium- and long-chain acyl-CoA thioesterase. Multiple transcript variants encoding different isoforms have been found for this gene.


== Function ==
The protein encoded by the ACOT13 gene is part of a family of Acyl-CoA thioesterases, which catalyze the hydrolysis of various Coenzyme A esters of various molecules to the free acid plus CoA. These enzymes have also been referred to in the literature as acyl-CoA hydrolases, acyl-CoA thioester hydrolases, and palmitoyl-CoA hydrolases. The reaction carried out by these enzymes is as follows:
CoA ester + H2O → free acid + coenzyme A
These enzymes use the same substrates as long-chain acyl-CoA synthetases, but have a unique purpose in that they generate the free acid and CoA, as opposed to long-chain acyl-CoA synthetases, which ligate fatty acids to CoA, to produce the CoA ester. The role of the ACOT- family of enzymes is not well understood; however, it has been suggested that they play a crucial role in regulating the intracellular levels of CoA esters, Coenzyme A, and free fatty acids. Recent studies have shown that Acyl-CoA esters have many more functions than simply an energy source. These functions include allosteric regulation of enzymes such as acetyl-CoA carboxylase, hexokinase IV, and the citrate condensing enzyme. Long-chain acyl-CoAs also regulate opening of ATP-sensitive potassium channels and activation of Calcium ATPases, thereby regulating insulin secretion. A number of other cellular events are also mediated via acyl-CoAs, for example signal transduction through protein kinase C, inhibition of retinoic acid-induced apoptosis, and involvement in budding and fusion of the endomembrane system. Acyl-CoAs also mediate protein targeting to various membranes and regulation of G Protein α subunits, because they are substrates for protein acylation. In the mitochondria, acyl-CoA esters are involved in the acylation of mitochondrial NAD+ dependent dehydrogenases; because these enzymes are responsible for amino acid catabolism, this acylation renders the whole process inactive. This mechanism may provide metabolic crosstalk and act to regulate the NADH/NAD+ ratio in order to maintain optimal mitochondrial beta oxidation of fatty acids. The role of CoA esters in lipid metabolism and numerous other intracellular processes are well defined, and thus it is hypothesized that ACOT- enzymes play a role in modulating the processes these metabolites are involved in.


== References ==


== Further reading ==

This article incorporates text from the United States National Library of Medicine, which is in the public domain.