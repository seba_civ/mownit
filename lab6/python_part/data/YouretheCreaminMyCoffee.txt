"You're the Cream in My Coffee" is a popular song. It was published in 1928.
The music was written by Ray Henderson, the lyrics by Buddy G. DeSylva and Lew Brown and appears in the Broadway musical Hold Everything! and is featured in the Warner Brothers film version of the musical in 1930.
The song was recorded by Annette Hanshaw in 1928.
The song was later recorded by Les Brown, Chris Connor (with the Jerry Wald Orchestra), the Ray Conniff Singers, and many others.
Marlene Dietrich sang the song in her screen test for The Blue Angel, her breakthrough role. She later recorded the song on her album "Dietrich in Rio" in 1959.
The song provided a backdrop for the 1980 television play Cream in My Coffee by English dramatist Dennis Potter.
This song was used as the theme song for The Mrs Bradley Mysteries in 2000 and it was recorded by BBC Records and sung by Graham Dalby and The Grahamophones in a re-creation of Jack Hylton's 1928 version.
The song was also recorded by Seth MacFarlane on his debut album, Music Is Better Than Words.


== References ==