Paul E. Sperry is an American author, political commentator, and investigative journalist. He was a media fellow at the Hoover Institution.


== Journalism and political commentary ==
Sperry has previously been the Washington Bureau Chief at Investor's Business Daily and WorldNetDaily. He wrote articles critical of George W. Bush for his decision to initiate the 2003 invasion of Iraq, accusing Bush of lying.
He has also had pieces in The Wall Street Journal as well as made regular appearances on Fox News and other media outlets. He currently regularly writes Op-Ed pieces for the New York Post. 


== Author ==
In the wake of the Fort Hood Shooting, he gave an interview to Coast to Coast AM on November 7, 2009, about his co-authored book Muslim Mafia. His latest book, "The Great American Bank Robbery," is about how the government's attempt to increase minority home-ownership helped create the sub-prime housing crisis.


== Books ==
Muslim Mafia (WND, 2009) (co-author)
Infiltration: How Muslim Spies and Subversives have Penetrated Washington (Thomas Nelson, 2005)
Crude Politics: How Bush's Oil Cronies Hijacked the War on Terrorism (Thomas Nelson, 2003)


== Select articles ==
Op-Ed, "When the Profile Fits the Crime," The New York Times, July 28, 2005
Op-Ed, "It's the age of terror: What would you do?", The New York Times, July 31, 2005
"The military's blinders", New York Post, November 7, 2009
"Soldiers Of Allah Or Of America: Does Military Know — Or Care? ", Investor's Business Daily, December 15, 2009


== References ==


== External links ==
"U.S. Marines Build Shrine To Islam". June 13, 2006. Assyrian International News Agency
Sperry's website
"Muslim Brotherhood 'Conspiracy' to Subvert America", Sperry speaking about Muslim Mafia, October 16, 2009