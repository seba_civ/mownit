
== Regular season ==


=== Schedule ===


=== Game summaries ===


==== Week 1 (Saturday September 26, 1959): Cleveland Browns ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 33,844
Referee:
TV announcers:
Scoring drives:
Cleveland – Plum 1 run (Groza kick) CLE 7–0
Pittsburgh – FG Layne 26 CLE 7–3
Pittsburgh – Orr 20 pass from Layne (Layne kick)PIT 10–7
Pittsburgh – Brewster 19 pass from Layne (Bobby PIT 17-7ne kick)


==== Week 2 (Sunday October 4, 1959): Washington Redskins ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 26,750
Referee:
TV announcers:
Scoring drives:
Washington – FG Baker 25 WSH 3–0
Washington – FG Baker 47 WSH 3–0
Washington – FG Baker 48 WSH 9–0
Washington – Anderson 70 pass from Gugliemi (Baker kick) WSH 16–0
Pittsburgh – FG Layne 32 WSH 16–3
Washington – Walton 26 pass from Gugliemi (Baker kick)WSH 23–3
Pittsburgh – Brewster 30 pass from Layne (Bobby WSH 23-10 NEKICK)
Pittsburgh – Krutko 2 run (Layne kick)WSH 23–17


==== Week 3 (Sunday October 11, 1959): Philadelphia Eagles ====
at Franklin Field, Philadelphia, Pennsylvania
Game time:
Game weather:
Game attendance: 27,343
Referee: Bud Brubaker
TV announcers:
Scoring drives:
Pittsburgh – Layne 10 run (Layne kick)
Philadelphia – Barnes 3 run (Walston kick)
Philadelphia – Powell 58 punt return (Walston kick)
Pittsburgh – FG Layne 12
Philadelphia – Van Brocklin 1 run (Walston kick)
Pittsburgh – Tracy 11 pass from Layne (Layne kick)
Philadelphia – McDonald 18 pass from Van Brocklin (Walston kick)
Pittsburgh – Orr 17 pass from Layne (Layne kick)


==== Week 4 (Sunday October 18, 1959): Washington Redskins ====
at Griffith Stadium, Washington, DC
Game time:
Game weather:
Game attendance: 28,218
Referee:
TV announcers:
Scoring drives:
Pittsburgh – Krutko 1 run (Layne kick)
Pittsburgh – Tarasovic 38 fumble run (Layne kick)
Washington – FG Baker 30
Washington – FG Baker 39
Pittsburgh – Krutko 4 run (Layne kick)
Pittsburgh – FG Layne 23
Pittsburgh – FG Layne 22


==== Week 5 (Sunday October 25, 1959): New York Giants ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 33,596
Referee:
TV announcers:
Scoring drives:
Pittsburgh – FG Layne 37
New York Giants – Gifford 77 pass from Conerly (Summerall kick)
New York Giants – Gifford 28 pass from Conerly (Summerall kick)
Pittsburgh – Dial 35 pass from Layne (Layne kick)
Pittsburgh – FG Layne 19
New York Giants – Huff 5 fumble run (Summerall kick)
Pittsburgh – FG Layne 17


==== Week 6 (Sunday November 1, 1959): Chicago Cardinals ====
at Comiskey Park, Chicago, Illinois
Game time:
Game weather:
Game attendance:
Referee:
TV announcers:


==== Week 7 (Sunday November 8, 1959): Detroit Lions ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 24,614
Referee:
TV announcers:
Scoring drives:
Detroit – Rote 1 run (Perry kick)
Pittsburgh – FG Layne 29
Detroit – FG Martin 27
Pittsburgh – Tracy 20 pass from Layne (Layne kick)


==== Week 8 (Sunday November 15, 1959): New York Giants ====
at Yankee Stadium, Bronx, New York
Game time:
Game weather:
Game attendance: 66,786
Referee:
TV announcers:
Scoring drives:
New York Giants – FG Summerall 21
New York Giants – FG Summerall 27
Pittsburgh – Orr 4 pass from Layne (Layne kick)
New York Giants – FG Summerall 29
Pittsburgh – Tracy 45 pass from Layne (Layne kick)


==== Week 9 (Sunday November 22, 1959): Cleveland Browns ====
at Cleveland Municipal Stadium, Cleveland, Ohio
Game time:
Game weather:
Game attendance: 68,563
Referee:
TV announcers:
Scoring drives:
Pittsburgh – Tracy 4 run (Layne kick)
Pittsburgh – Tracy 1 run (Layne kick)
Cleveland – Renfro 30 pass from Plum (kick failed)
Cleveland – Renfro 28 pass from Plum (Groza kick)
Cleveland – Renfro 70 pass from Plum (Groza kick)
Pittsburgh – Nagler 17 pass from Layne (Layne kick)


==== Week 10 (Sunday November 29, 1959): Philadelphia Eagles ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 22,191
Referee:
TV announcers:
Scoring drives:
Pittsburgh – FG Layne 17
Pittsburgh – Dial 12 pass from Layne (Layne kick)
Pittsburgh – Orr 19 pass from Layne (Layne kick)
Pittsburgh – Tracy 23 pass from Layne (Layne kick)
Pittsburgh – Nagler 2 pass from Layne (Layne kick)


==== Week 11 (Sunday December 5, 1959): Chicago Bears ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 41,476
Referee:
TV announcers:
Scoring drives:
Chicago Bears – Casares 3 run (kick failed)
Chicago Bears – Casares 1 run (Aveni kick)
Chicago Bears – Casares 1 run (Aveni kick)
Pittsburgh – Tracy 2 run (Layne kick)
Chicago Bears – Casares 1 run (Aveni kick)
Pittsburgh – Krutko 7 run (Layne kick)
Pittsburgh – Dial 25 pass from Layne (Layne kick)


==== Week 12 (Sunday December 12, 1959): Chicago Cardinals ====
at Forbes Field, Pittsburgh, Pennsylvania
Game time:
Game weather:
Game attendance: 19,011
Referee:
TV announcers:
Scoring drives:
Chicago Cardinals – FG Conrad 37
Chicago Cardinals – FG Conrad 20
Pittsburgh – Layne 9 run (Layne kick)
Pittsburgh – Dial 35 pass from Layne (Layne kick)
Pittsburgh – Dial 11 pass from Layne (Layne kick)
Pittsburgh – Tracy 16 pass from Layne (Layne kick)
Chicago Cardinals – Conrad 21 pass from Hill (Conrad kick)
Pittsburgh – Orr 16 pass from Layne (Layne kick)
Chicago Cardinals – Crow 7 pass from Hill (Conrad kick)


=== Standings ===
Note: Tie games were not officially counted in the standings until 1972.


== References ==