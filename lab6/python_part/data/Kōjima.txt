Kōjima (幸島) is a small island in the Sea of Hyūga off the shore of the city of Kushima in Miyazaki Prefecture, Japan. The island is approximately 13 km ESE and 20 km by road from the central built-up area of Kushima. It is approximately 300 m offshore and 30 hectares in area, and mainly forested.
Kōjima is best known as housing a field study site of the Japanese Primate Research Institute, where Japanese macaques are held in wild conditions for primatological study. The buildings of the field station are on the mainland so as to minimise disturbance of the monkeys' behaviour. Study of the monkeys began in 1947, and since 1952 all individuals have been marked so that the demographics of the population can be studied. Many investigations have been carried out, including studies of the changes that occur in social dominance over time. Kōjima is the site of one of the best-known studies in animal culture, in which it was reported that one monkey acquired various skills such as washing sweet potatoes in water, and that these skills then spread through the monkey troops by imitation.


== See also ==
Satsue Mito
Hundredth monkey effect


== References ==


== External links ==
Pictures of Kōjima (Japanese)