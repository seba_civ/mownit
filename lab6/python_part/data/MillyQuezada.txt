Milagros Quezada Borbón, known simply as Milly Quezada, is a Latin Grammy-winning Dominican Merengue music singer. She is a singer in Latin America; especially in those Latin American countries bordered by the Caribbean Sea, and in the eastern seaboard region of the United States. She is also known as Merengue Queen. She has won four Latin Grammy Awards.


== Biography ==
Milly Quezada was born in Santo Domingo, the daughter of two Cibao natives and sister of four musician brothers. Growing up in such a musical family, Quezada became interested in music as a toddler—she would sing along with her brothers for family, friends and small gatherings near her home.
She expressed an interest in developing her singing talents as a young girl, and, partly for this reason the Quezada family moved to New York City when she was still a child.
Milly spent her pre-teen and teen years in New York, a city that is well known as a school for Merengue and Salsa singers from its Latino barrios, e.g. Spanish Harlem. In New York, Quezada developed an in-depth knowledge of her favorite musical genre, the fastest of the Latin American popular dance music styles, sung in Spanish and native to the Dominican Republic (see also Son Cubano, Rumba, Salsa, Cha-Cha-Cha, and Punta for comparison, as well as Kompa, the style sung in kreyòl in neighboring Haiti). Merengue has been comically referred to as "the Latino weight-loss program" since it is an infectious rhythm, and the songs tend to inspire adherents to enthusiastic dancing. After graduating from high school in New York, she attended City College of New York, graduating with a university degree in communications.


=== Milly, Jocelyn y los Vecinos ===
During the 1980s, Quezada was the lead singer of a family group named Milly, Jocelyn y los Vecinos. The group enjoyed wild international success, producing 20 records, some of which were released in both vinyl and CD format, others only as CD's (as vinyl record album sales declined substantially by the end of the decade).


=== Solo career ===
Quezada later decided to embark on a solo career, moving to Puerto Rico where she also married and became a mother, enjoying even more success as a solo artist. Since the 1990s, Quezada has been either on tour or in recording studios almost constantly, earning several prestigious awards and public recognition such as keys to cities (see below).
In May 2005, she began a Dominican Republic tour that included a concert with the legendary Conjunto Quisqueya.
In 2007, Milly recorded a song called "Reencuentro" along with fellow Dominicans Sergio Vargas and Anaís. The original version and Merengue version of the song appear on Anais' album "Con Todo Mi Corazón"
In early 2009, Milly joined eleven other Dominican music legends on a national tour celebrating the merengue and bachata music traditions of the country. That tour was transformed into a Felix Limardo documentary entitled Sol Caribe. The movie features an original song, La Puerta del Nuevo Mundo that is a once in a lifetime collaboration of the twelve stars. The film was the closing feature at the III Dominican Republic Global Film Festival in November, 2009 and was attended by the President of the Dominican Republic.


== Discography ==


=== Milly y los Vecinos ===
Esta es Milly con los Vecinos (This is Milly with los Vecinos)
La gente de hoy (Today's People)
Pa' Dominicana (To Dominican Republic)
Los Vecinos en su momento (The Vecinos at Their Best Moment)
'Milly en Boleros (Milly into Boleros)
Acabando (Milly y los Vecinos album) (Kicking Some)
Nostalgia (Milly y los Vecinos album) (Nostalgia)
Esta Noche, Los Vecinos (Tonight, Los Vecinos!)
Milly en Salsa (Milly into Salsa, alongside Luis "Perico" Ortiz)
Dinastía (Milly y los Vecinos album)
Special Delivery
Etiqueta Negra (Milly y los Vecinos album) (Black Tie Affair)
Por Supuesto (Milly y los Vecinos album) (Of Course)
Ahora Es (Milly y los Vecinos album) (Now's the Time)
7+1= Vecinos
Flying Solo (Milly y los Vecinos album)
La Milly una noche (Milly, one Night)
Celebrando (Milly y los Vecinos album) (Celebrating)
Lo Mejor de Milly y los Vecinos (The Best of Milly y los Vecinos, a remix)
En tus manos (In Your Hands)


=== As a solo singer ===
Quezada has recorded five solo albums, receiving a Latin Grammy for Pienso asi, MQ and Aquí estoy yo.
Hasta siempre (Forever)
Milly vive (Milly Lives)
Tesoros de mi Tierra (Treasures from my land, where she had her first experience singing Bachata)
Milly, éxitos y más (Milly, Hits and More)
Pienso así (This is How I Think)
MQ (MQ)
Sólo Faltas Tú (You're the only one missing)
Aquí estoy yo (Here I am)


== City Keys ==
Quezada has received many awards during her career, such as the aforementioned Grammy. Arguably, one of the highest honors a person or celebrity can receive is the "key to a city," usually presented by that city's mayor. Here is a list of cities that have given Quezada that symbolic award:
Jersey City, New Jersey
Providence, Rhode Island
New York City, New York
West New York, New Jersey


== See also ==
List of Dominicans


== References ==


== External links ==
Official Site in Spanish
Official Facebook Page
Official Twitter
Official site in Myspace.com
Biography of Milly Quezada
Merengue music Stream / Musica Domincana, en Viva