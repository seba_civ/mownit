Painted Smiles is the name of a small record label run by Ben Bagley (1933-1998) and based in New York City. The first of this set of stereo albums were of the songs of his often satirical Shoestring Revues which were performed off-Broadway starting in the late 1950s. The main series of albums were anthologies of the songs of the top Broadway musical lyricists and composers from the 1920s through the 1940s, though the albums were produced during the 1960s and 1970s. Many of them are now available on CD.
All these albums were artistically of a series, being white with watercolors of showgirls on the cover - and notes filled with double-entendres beside the small photographs of the performers on the back.
Some of the artists featured on the Painted Smiles record label include Bobby Short, Blossom Dearie, Kaye Ballard, Cab Calloway, Barbara Cook, Richard Chamberlain, Anthony Perkins, Rex Reed, Dorothy Loudon, Katharine Hepburn, Elaine Stritch, Jerry Stiller, Chita Rivera, Ann Miller, Phyllis Diller, Alice Playten, and Gloria Swanson!
Some of the lyricists and composers include Richard Rodgers, Cole Porter, Kurt Weill, Irving Berlin, Noël Coward, Vincent Youmans, Vernon Duke, Johnny Mercer, Jerome Kern, Ira Gershwin, Harold Arlen, Alan Jay Lerner, Arthur Schwartz, Lorenz Hart, DeSylva, Brown and Henderson.


== External links ==
/ Painted Smiles on CastAlbums.org database
/ Painted Smiles Records on CastAlbums.org database


== See also ==
List of record labels