The Allen M. Sumner class was a group of 58 destroyers built by the United States during World War II. Another twelve ships were completed as destroyer minelayers. Often referred to as simply the Sumner class, this class was characterized by their twin 5-inch/38 caliber gun mounts, dual rudders, additional anti-aircraft weapons, and many other advancements over the previous Fletcher class. The Allen M. Sumner design was extended 14 feet (4.3 m) amidships to become the Gearing class, which was produced in larger numbers.
Completed in 1943–45, four were lost in the war and one was damaged so badly it was scrapped, but the surviving ships served in the US Navy into the 1970s. After being retired from the US fleet, 29 of them were sold to other navies, where they served many more years. Two still exist as museum ships, one in South Carolina, and one in Taiwan.


== Description ==
The first ship was laid down in May 1943, while the last was launched in April 1945. In that time the United States produced 58 Allen M. Sumner-class destroyers. The Allen M. Sumner class was an improvement of the previous Fletcher class, which were built from 1941 until 1944. In addition to three twin 5-inch/38 caliber gun mounts replacing the Fletchers' five single mounts, Sumners had twin rudders, giving them better maneuverability for ASW work when compared to Fletchers. The 5-inch guns were guided by a Mark 37 Gun Fire Control System with a Mark 25 fire control radar linked by a Mark 1A Fire Control Computer stabilized by a Mark 6 8,500 rpm gyro. This fire control system provided effective long-range anti-aircraft (AA) or anti-surface fire. The Allen M. Sumners also had much more short-range anti-aircraft armament than the Fletchers, with 12 40 mm guns and 11 20 mm guns compared with 8 40 mm and 7 20 mm for a typical late-war upgraded Fletcher. The initial design retained the Fletchers' heavy torpedo armament of 10 21-inch (533 mm) tubes in two quintuple mounts, firing the Mark 15 torpedo. As the threat from kamikaze aircraft mounted in 1945, and with few remaining Japanese warships to use torpedoes on, most of the class had the aft quintuple 21-inch torpedo tube mount replaced by an additional 40 mm quadruple mount for 16 total 40 mm guns.
The Allen M. Sumners achieved a 20% increase in 5-inch gun armament and almost a 50% increase in light AA armament on a hull the same length as a Fletcher, only 15 inches (38 cm) wider, and about 15 inches (38 cm) deeper in draft. The increase in standard displacement was only 150 tons, about 7.5%. Thus, the Allen M. Sumner class was a significant improvement in combat power at a small increase in cost.
See also Robert H. Smith-class destroyer minelayer (DM), twelve of which were built on hulls originally intended as Allen M. Sumners. Gearing-class destroyers were of the same design, modified with a 14-foot (4.3 m) midship extension to carry more fuel to extend the ships' range.


== Construction ==
Eighteen were built by Federal Shipbuilding and Drydock Company in Kearny, New Jersey. Fourteen were built by Bath Iron Works in Bath, Maine. Ten were built by Bethlehem Steel's Mariners Harbor shipyard on Staten Island. Six were built by Bethlehem Steel's Union Iron Works in San Francisco, California. Five were built by Bethlehem Steel in San Pedro, California. Five were built by Todd Pacific Shipyards in Seattle, Washington. USS Barton was the first ship of the class to be laid down and the first to be commissioned. USS Henley was the last commissioned.


== Service ==
The Allen M. Sumners served on radar picket stations in the Battle of Okinawa, as well as other duties, and had several losses. Cooper, Meredith, Mannert L. Abele, and Drexler were lost during the war, and Hugh W. Hadley was so badly damaged by a kamikaze attack that she was scrapped soon after the war ended. After the war most of the class (except some of the light minelayers) had their 40 mm and 20 mm guns replaced by up to six 3-inch/50 caliber guns (76 mm), and the pole mast was replaced by a tripod to carry a new, heavier radar. On most ships one depth charge rack was removed and two Hedgehog mounts added. One of the two quintuple 21-inch (533 mm) torpedo tube mountings had already been removed on most to make way for a quadruple 40 mm gun mounting and additional radar for the radar picket mission. 33 ships were converted under the Fleet Rehabilitation and Modernization II (FRAM II) program 1960-65, but not as extensively as the Gearings. Typically, FRAM Allen M. Sumners retained all three 5-inch/38 twin mounts and received the Drone Anti-Submarine Helicopter (DASH), two triple Mark 32 torpedo tubes for the Mark 44 torpedo, and two new single 21-inch torpedo tubes for the Mark 37 torpedo, with all 3-inch and lighter guns, previous ASW armament, and 21-inch torpedo tubes being removed. Variable Depth Sonar (VDS) was also fitted; however, ASROC was not fitted. Ships that did not receive FRAM were typically upgraded with Mk 32 triple torpedo tubes in exchange for the K-guns, but retained Hedgehog and one depth charge rack.
In Navy slang, the modified destroyers were called "FRAM cans", "can" being a contraction of "tin can", the slang term for a destroyer or destroyer escort.
Many Allen M. Sumners provided significant gunfire support in the Vietnam War. They also served as escorts for Carrier Battle Groups (Carrier Strike Groups from 2004) and Amphibious Ready Groups (Expeditionary Strike Groups from 2006). From 1965, some of the class were transferred to the Naval Reserve Force (NRF), with a partial active crew to train Naval reservists.


== Disposition ==

The ships served in the US Navy into the 1970s. DASH was withdrawn from anti-submarine warfare (ASW) service in 1969 due to poor reliability. Lacking ASROC, the Allen M. Sumners were left without a standoff ASW capability, and were decommissioned 1970-73, with most being transferred to foreign navies. The FRAM Sumners were effectively replaced as ASW ships by the Knox-class frigates (destroyer escorts prior to 1975), which were commissioned 1969-74 and carried a piloted helicopter, typically the Kaman SH-2 Seasprite, and ASROC. After the Allen M. Sumners were retired from the US fleet, seven were sunk by the US in fleet training exercises and 13 were scrapped, while 29 were sold to other navies (two for spare parts), where they served for many more years. 12 were sold to the Republic of China Navy and 2 were sold to the Republic of Korea Navy. 2 were sold to the Shah of Iran and 1 was sold to Turkey. 1 was sold to Greece. 2 were sold to Venezuela, 2 to Colombia, 2 sold to Chile, 5 sold to Brazil and 4 to Argentina.
Currently, only USS Laffey (DD-724) located at Patriot's Point, Charleston, South Carolina remains as a museum ship.


== Ships in class ==


== References ==


== External links ==
Allen M. Sumner-class destroyers at Destroyer History Foundation
GlobalSecurity.org
http://www.gyrodynehelicopters.com/sumner_class.htm
"Super Destroyer Packs Punch of Prewar Cruiser." Popular Mechanics, February 1945, p. 32.
NavSource Destroyer Photo Page
(Chinese)Old ships to be sold for scrap, including DDG-914, former DD-746 in US