RMS Etruria and Umbria were the last two Cunarders that were fitted with auxiliary sails. Etruria was built by John Elder & Co. of Glasgow, Scotland in 1884. Etruria and Umbria, by the standards of the time, were record breakers. They were the largest liners then in service, and they plied the Liverpool to New York Service. RMS Etruria was completed and launched in March 1885, twelve weeks later than Umbria.
Etruria had many distinguishing features that included two enormous funnels that gave an impression of huge power. She also had three large steel masts which when fully rigged had an extensive spread of canvas. Another innovation on Etruria was that she was equipped with refrigeration machinery, but it was the single screw propulsion that would bring the most publicity later in her career.
The ship epitomized the luxuries of Victorian style. The public rooms in First Class were full of ornately carved furniture and heavy velvet curtains hung in all the rooms, and they were cluttered with bric-a-brac that period fashion dictated. These rooms, and the First Class cabins, were situated on the Promenade, Upper, Saloon and Main Decks. There was also a Music Room, Smoke Room for gentlemen, and separate dining rooms for First and Second Class passengers. By the standard of the day, Second Class accommodation was moderate, but spacious and comfortable. RMS Etruria's accommodation consisted of 550 First Class, and 800 Second Class passengers. However late in 1892 this changed to 500 First Class, 160 Second Class, and 800 Third Class (Steerage) passengers.


== Service on the Atlantic ==

RMS Etruria was to start her regular service to New York from Liverpool, but the clouds of crisis were looming, and by the New Year of 1885 a crisis involving Russia's threat to invade Afghanistan was coming to a head. This was to bring Etruria's North Atlantic service to a halt temporarily, before she had even made her maiden voyage. On 26 March, Etruria, and RMS Umbria, found themselves chartered to the Admiralty. With the dispute reaching a settlement, Etruria was released from Admiralty service within a few days, although her sister was retained for six months.

On 25 April 1885, Etruria finally made her maiden voyage under the command of Captain McMicken. She made the Atlantic crossing calling at Queenstown (Cobh). On her very next crossing, westbound (Liverpool to New York), she won the prestigious Blue Riband (see the table below) and proudly flew the pennant for Cunard.
Later in the year Etruria was involved in a collision. On 20 September 1885, she was outward bound from New York and in Lower New York Bay, at anchor due to dense fog. The 4,276-ton cargo ship Canada, owned by the National Steamship Company of Limerick, collided with Etruria, on her starboard side. Canada scraped alongside Etruria, ripping away a portion of her rigging, but there were no casualties. Both ships continued on their voyages.


== Winston Churchill ==

In November 1895, 20-year-old Winston Churchill, a lieutenant in the 4th Hussars, snatched a few weeks' leave from his regiment to visit Cuba, with the aim of observing the Cuban Revolutionary War against Spain.
Getting there involved travelling by way of New York via Liverpool and Queenstown on Etruria. Thus, on 9 November, Winston Churchill arrived in New York harbour aboard Etruria, and first set foot in his mother's homeland and the city where she had been born and brought up. Three days later he travelled on to Cuba. Churchill returned to Britain early in 1896 travelling again on Etruria.
On 6 January 1900, Etruria left Liverpool, and one week later she arrived in New York. On the 13th engineers were inspecting the ship, and on examination of the propeller shaft, they found cracks that were not there when the ship left Liverpool. Her running mate had suffered a failure of her propeller shaft at sea in 1893, and to avoid the same fate Etruria was confined to her pier until a replacement shaft was shipped over from Britain. After this was done, and the new shaft had been fitted in New York, she departed on 17 February for the homeward bound service. In 1900 Etruria remained on the North Atlantic service whilst Umbria was requisitioned to carry troops to and from South Africa during the Boer War. By July 1900 both sisters were back on the North Atlantic service.


== A year to forget ==

In 1901 Etruria and her running mate were fitted with wireless, putting her at the forefront of this new technology. On 22 February 1902, Etruria left New York and was due to arrive in Queenstown on 1 March. On 26 February she radioed Umbria to pass on messages to one of her passengers. However, that evening her propeller shaft fractured, leaving her drifting helplessly. She tried with no success to radio Umbria again to report her predicament. In the days before the Titanic disaster, radio operators did not man their sets 24 hours a day. Eventually she managed to attract the attention of the Leyland ship William Cliff, by firing distress rockets. William Cliff stood alongside in an hour and stayed with her during the night whilst attempts were made to repair her. Etruria then made sail and William Cliff took her in tow; the ships headed for Horta, in the Azores, which were 500 miles to the south-east of her stricken position.
She arrived in the Azores on Sunday, 9 March, and on the 15th her passengers and mail were transferred onto the steamship Elbe, which had been chartered for the task on the 10th. It was summer 1902 before Etruria was repaired and back in service, but in October, after a particularly rough Atlantic crossing, her propeller shaft again showed serious cracks and she was taken out of service and waited in New York for yet another new shaft to be sent over and installed. It was 1 November before she set sail for home again; 1902 had been a very bad year for the ship.


== More bad luck ==
1903 did not start too well for Etruria either. On 28 February she was leaving New York and ran aground on sand and mud in the entrance to Gedney Channel. After she was refloated later the same day there was no damage found and she set off on her voyage to Liverpool.
On 10 October Etruria was only four hours out of New York when at 2:30 pm the ship was struck by a rogue wave. The wave was reported to be at least 50 feet high, and struck the ship on the port side. The wave carried away part of the fore bridge and smashed the guardrail stanchions. A number of First Class passengers were sitting in deck chairs close to the bridge, and they caught the full force of the water. One passenger, a Canadian, was fatally injured, and several other passengers were hurt.
In January 1907 two of Etruria's crew were killed as they tried to secure the lashings of the starboard anchor in very rough weather, during a westbound crossing.


== The end of Etruria's career ==
The two 23-year-old vessels were now getting to the point where technical progress had overtaken them. RMS Lusitania and RMS Mauretania were under construction, and due to enter service in late 1907.
On Wednesday 26 August 1908, RMS Etruria was moving astern from her pier in Liverpool to anchor opposite the Princes' Landing Stage, where her passengers would embark. A hopper crossing the Mersey came too close to Etruria and was violently rammed by her. Etruria's rudder and propeller were thrust deep into the hopper, almost severing it in two. However, being impaled on Etruria's propeller prevented the hopper from sinking. Both vessels drifted helplessly in the Mersey, and the hopper was violently crushed against the landing stage. This not only spelt the end for the hopper, but finished the career of Etruria as well. Her propeller, rudder and steering gear were seriously damaged, forcing the cancellation of her sailing to New York. Etruria's passengers were put up in hotels and then caught Umbria later in the week. Etruria was taken into dock, where temporary repairs were made.
She would not cross the Atlantic again, and after spending time laid up at Birkenhead, she was finally sold for £16,750 in October 1909. On 10 October 1910, the Mersey tug Black Cock towed Etruria to her final destination of Preston, Lancashire, where she was scrapped.


== The Blue Riband ==


== References ==


== External links ==
Cunard Heritage site
MaritimeQuest RMS Etruria Photo Gallery
(SS Etruria, 1885-1909 ; 7,718 tons)