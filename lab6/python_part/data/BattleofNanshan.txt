The Battle of Nanshan (南山の戦い, Nanzan no tatakai) was one of many vicious land battles of the Russo-Japanese War. It took place on 24–26 May 1904 across a two-mile-wide defense line across the narrowest part of the Liáodōng Peninsula, covering the approaches to Port Arthur and on the 116-meter high Nanshan Hill, the present-day Jinzhou District, north of the city center of Dalian, Liaoning, China.


== Background ==
After the Japanese victory at the Yalu River, the Japanese Second Army commanded by General Yasukata Oku landed on the Liaotung peninsula, only some 60 miles from Port Arthur. The Second Army was 38,500 strong and consisted of three divisions: the First Division (Tokyo), Third Division (Nagoya) and Fourth Division (Osaka). Landing was completed by 5 May 1904.
The Japanese intention was to break through this Russian defensive position, capture the port of Dalny, and lay siege to Port Arthur.
Russian Viceroy Yevgeni Alekseyev had been recalled to Moscow for consultations with Tsar Nicholas II. He had left Major-General Baron Anatoly Stoessel in command of Russian ground forces in the Kwantung Peninsula, and Admiral Wilgelm Vitgeft in control of the Russian fleet at Port Arthur. Since no direct orders had been left, the indecisive and incompetent Admiral Vitgeft allowed the Japanese landing to proceed unopposed.
General Stoessel had approximately 17,000 men and the 4th, 5th, 13th, 14th and 15th East Siberian Rifles, from which about 3,000 men of the 5th East Siberian Rifles under Colonel Nikolai Tretyakov were dug into fortified positions on Nanshan hill, where they planned to hold out despite knowing they would be greatly outnumbered. The reserve divisions were under command of Lieutenant-General Alexander Fok, a former police officer who had risen to his position through political patronage rather than experience or ability. The Russian forces had 114 pieces of field artillery, machine guns, and had dug a network of trenches and barbed wire. The Japanese were well aware of the fortifications, as a Colonel Doi of Japanese intelligence was one of the thousands of "Chinese laborers" recruited by the Russians to work on the project in 1903.


== The battle ==

On 24 May 1904, during a heavy thunderstorm, the Japanese Fourth Division under the command of Lieutenant General Ogawa Mataji attacked the walled town of Chinchou (modern-day Jinzhou District 金州), just north of Nanzan hill. Despite being defended by no more than 400 men with antiquated artillery, the Fourth Division failed on two attempts to breach its gates. Two battalions from the First Division attacked independently at 05:30 on 25 May 1904, finally breaching the defenses and taking the town.
With his flank thus secure, General Oku could then commence the main assault on the entrenched Russian forces on Nanshan Hill. The assault was postponed a day due to the weather. On 26 May 1904, Oku began with prolonged artillery barrage from Japanese gunboats offshore, followed by infantry assaults by all three of his divisions. The Russians, with mines, Maxim machine guns and barbed wire obstacles, inflicted heavy losses on the Japanese during repeated assaults. By 18:00, after nine attempts, the Japanese had failed to overrun the firmly entrenched Russian positions. Oku had committed all of his reserves, and both sides had used up most of their artillery ammunition.
Finding his calls for reinforcement unanswered, Colonel Tretyakov was amazed to find that the uncommitted reserve regiments were in full retreat and that his remaining ammunition reserves had been blown up under orders of General Fok. Fok, paranoid of a possible Japanese landing between his position and the safety of Port Arthur, was panicked by a flanking attack by the decimated Japanese Fourth Division along the west coast. In his rush to flee the battle, Fok had neglected to tell Tretyakov of the order to retreat, and Tretyakov thus found himself in the precarious position of being encircled, with no ammunition and no reserve force available for a counter-attack. Tretyakov had no choice but to order his troops to fall back to the second defensive line. By 19:20, the Japanese flag flew from the summit of Nanshan Hill. Tretyakov, who had fought well and who had lost only 400 men during the battle, lost 650 more men in his unsupported retreat back to the main defensive lines around Port Arthur.


== Results ==
The Russians lost a total of about 1,400 killed, wounded and missing during the battle. Although the Japanese did not win lightly, having at least 6,198 casualties, they could claim victory. Among the 739 dead was the eldest son of General Maresuke Nogi. The Japanese had fired 34,000 artillery shells during the battle – more than had been expended during the entire First Sino-Japanese War.
Due to lack of ammunition, the Japanese could not move from Nanshan until 30 May 1904. To their amazement, they found that the Russians had made no effort to hold the strategically valuable and easily defendable port of Dalny, but had retreated all the way back to Port Arthur. Although the town had been looted by the local civilians, the harbor equipment, warehouses and railway yards were all left intact.
After Japan occupied Dalny, a memorial tower was erected on top of Nanshan Hill with a poem by General Oku. The tower was demolished after the Pacific War, and only the foundation is left. A portion of a stone tablet with the poem is now displayed in the Lushun Prison, Dalian.


== References ==
Connaughton, Richard (2003). Rising Sun and Tumbling Bear. Cassell. ISBN 0-304-36657-9
Jukes, Geoffry. The Russo-Japanese War 1904–1905. Osprey Essential Histories. (2002). ISBN 978-1-84176-446-7.
Kowner, Rotem (2006). Historical Dictionary of the Russo-Japanese War. ISBN 0-8108-4927-5: The Scarecrow Press. 
Nish, Ian (1985). The Origins of the Russo-Japanese War. Longman. ISBN 0-582-49114-2
Sedwick, F.R. (1909). The Russo-Japanese War. Macmillan.


== External links ==
Russo-Japanese War Research Society


== Notes ==