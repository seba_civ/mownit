Robyn Young (born in Oxford in September 1975) is an English author of historical fiction. She is most widely known for her Brethren trilogy, set in the Middle Ages. Her books have been published in more than 15 languages.


== Biography ==
Young attended University of Sussex in Brighton, England where she completed her master's degree in Creative Writing, the Arts and Education.
She now lives and writes in Brighton full-time. In May 2007 Robyn was recognised as one of Waterstone's 25 "Authors of the future"; chosen from 100 authors nominated by publishers, editors and literary agents.
Young was one of the founding members of the "Historical Writers Association". She also took part in the inaugural HWA festival, together with Simon Scarrow and Michael Morpurgo.
Young's novels have been translated into 19 languages.


== Books ==


=== Brethren Trilogy ===
Brethren (Hodder & Stoughton, 2006)
Crusade (Hodder & Stoughton, 2007)
Requiem (Hodder & Stoughton, 2008) (known as The Fall of the Templars in the United States)


=== Insurrection Trilogy ===
Insurrection (Hodder & Stoughton, 2010)
Renegade (Hodder & Stoughton, 2012)
Kingdom (Hodder & Stoughton, 2015)


== Notes ==


== External links ==
Official website