Govinda Chandra Dev (1 February 1907 – 26 March 1971), known as G. C. Dev, was a Professor of Philosophy at the University of Dhaka. He was assassinated at the onset of Bangladesh Liberation War of 1971 by the Pakistan Army.


== Early life and education ==
Dev was born in the village of Lauta of the Panchakhanda Pargana (currently Beanibazar Upazila) of Sylhet District, East Bengal (now Bangladesh) on 1 February 1907. His ancestors were high-caste Brahmins who settled in Sylhet from Gujarat. After his father's death at an early age, Dev was raised by the local Christian missionaries. Dev passed the Entrance Examination in first division from Biani Bazar High English School in 1925. In 1927, he passed the Intermediate Examination from Ripon College, Kolkata. He received his Bachelor of Arts degree with honors from Sanskrit College in 1929. In 1931, he received his Master of Arts in Philosophy from the University of Calcutta. He graduated summa cum laude in both Bachelors and Masters. His Doctoral thesis was titled Reason, intuition and reality, and for this, he received a PhD degree from Calcutta University in 1944. The thesis dissertation was later published as a book titled Idealism and Progress.


== Career ==
Dev started his career as a lecturer in Ripon College in Kolkata. As the college was transferred from Calcutta to Dinajpur (now in Bangladesh) during World War II, he moved as well. In 1945, the college re-located to Kolkata, but Dev decided to stay back in Dinajpur as the founding principal of the new branch of Surendranath College (now Dinajpur Government College). Later, in July 1953, he joined the Department of Philosophy at Dhaka University. He also served as the house tutor of Dhaka hall (present Shahidullah Hall) in 1957 before being appointed as the provost of Jagannath Hall in the same year. Dev was appointed as the chairman of the department of philosophy in 1963. On 1 July 1967, Dev became a full professor. In the late 1960s he taught in Wilkes-Barre College in Pennsylvania, USA as a visiting professor. His admirers in the college founded The Govinda Dev Foundation for World Brotherhood for the propagation of Dev's humanist philosophy.
Dev was elected the general secretary of Pakistan Philosophical Congress in the 1960s and held the post till his death in 1971. He also set up a Philosophy House in Dhaka University to promote and practice his life-oriented and humanist philosophy, which became a center for intellectual and cultural activities in the university.
A lifelong bachelor, Dev donated the bulk of his belongings to the university in his lifetime. The donation facilitated the establishment of Center for Philosophical Studies (DCPS) at Dhaka University in 1980.


== Assassination ==

Dev was assassinated along with the husband of his adopted daughter by Pakistan army personnel involved in 1971 Dhaka University massacre on the black night of 25 March. At that time, Dev was residing in a university quarter near Jagannath Hall with his adopted daughter Rokeya Sultana and her husband Mohammad Ali. At around 11 p.m., the residents of the house were awakened by the sound of gunfire as the army raided the campus. As Rokeya remarked later, bullets were hitting the house like a hailstorm. The family took shelter in a small room and kept awake the whole night. In the morning of 26 a group of soldiers came to the door. They started banging on the door as they shouted in Urdu, "You son of an infidel, open the door." As Dev proceeded to open the door, the door collapsed on him. Upon entering the house, one of the soldiers hit on his head with his rifle and another one shot him on the chest. Dev feebly asked, "What do you want here, son?" In response, the soldiers shot a few more rounds at him. Two bullets hit him in the head and the other bullets hit him in the chest. Dev died immediately. The soldiers beat Rokeya mercilessly and killed her husband. They also plunged a bayonet into the professor's lifeless body. They later took the dead bodies to the grounds of Jagannath hall where the bodies of the other victims of the massacre were dumped.


== Bibliography ==
Dev wrote over a hundred articles in English and Bangla in national and international journals. He also published seven books in his lifetime –
Idealism and Progress (1952)
Idealism: A New Defence and A New Application (1958)
Amar Jibandarshan (The Philosophy of My Life) (1960)
Aspiration of the Common Man (1963)
The Philosophy of Vivekananda and the Future of Man (1963)
Tattvavidya-sara (1966)
Buddha: the Humanist (1969).
Two other books Parables of the East (1984) and My American Experience (1993) were published posthumously.


== See also ==
1971 Dhaka University massacre
1971 Bangladesh atrocities


== References ==


== Further reading ==
Bangalir Dorshan: Prachinkal Theke Somokal by Prof. Dr. Aminul Islam, Dept. of Philosophy, DU, (Maola Brothers, Dhaka, 2002), pp. 247–258


== External links ==
A tribute to Prof. G.C. Dev by Jahed Ahmed