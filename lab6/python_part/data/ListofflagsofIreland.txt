This is a list of flags which have been, or are still today, used in Ireland.


== Island of Ireland ==
The following flags have been used to represent the island of Ireland as a whole, either officially or unofficially.


== Northern Ireland ==


== Ireland ==


=== Defence Forces flags ===


==== Naval Service ====


==== Air Corps ====


==== Army ====


==== Defence Force Training Centre (DFTC) ====


==== Defence Forces Overseas and Organisations ====


=== Coast Guard ===


== Provincial flags ==


== County flags ==


== City and town flags ==


== Sporting flags ==


== Ensigns ==


== Historical military flags ==


== University flags ==


== Organisations ==


== Other flags ==


== See also ==
Cross-border flag for Ireland
GAA county colours
List of Northern Irish flags
Northern Ireland flags issue


== References ==


== External links ==
Symbols in Northern Ireland - Flags Used in the Region by Dara Mulhern and Martin Melaugh; illustrated article from CAIN Project (Conflict Archive on the INternet)
National Library of Ireland