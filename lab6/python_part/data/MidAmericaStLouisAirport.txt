MidAmerica St. Louis Airport (IATA: BLV, ICAO: KBLV, FAA LID: BLV) is a public use airport co-located on the grounds of Scott Air Force Base. It is located 14 nautical miles (16 mi, 26 km) east of the central business district of Belleville, in St. Clair County, Illinois, United States.
MidAmerica has operated as a joint use airport since beginning operations in November 1997 and is currently served by Allegiant Air with direct flights to Orlando Sanford International Airport, McCarran International Airport, St. Pete–Clearwater International Airport, and Punta Gorda Airport as of February 18, 2016. Direct flights to Jacksonville International Airport and Destin–Fort Walton Beach Airport will begin in June 2016.
As per Federal Aviation Administration records, the airport had 27,002 passenger boardings in 2008, 1,964 enplanements in 2009, and 1,183 in 2010. It was included in the National Plan of Integrated Airport Systems for 2011–2015, which categorized it as a primary commercial service airport based on enplanements in 2008 (more than 10,000 per year).


== History ==
MidAmerica St. Louis Airport was created to alleviate some crowding of Lambert-St. Louis International Airport, but has never had service from any major airline and has been criticized as a pork barrel project. Featured several times as a "Fleecing of America" segment on the NBC Nightly News, it was called a "Gateway to Nowhere" by Tom Brokaw, costing taxpayers $313 million. Supporters credit MidAmerica's additional runway with saving Scott AFB from closure during BRAC 2005. They also describe MidAmerica as a "Gateway to the World", citing a new cargo terminal and customs facility designed to attract international cargo. Congestion at Lambert-St. Louis has not been a problem since American Airlines reduced hub operations by fifty percent in 2003 and a new billion-dollar runway opened in 2006.


== Passenger airlines ==
Four passenger airlines have started operations at MidAmerica only to go out of business or close operations.
On August 16, 2000, Pan American Airways debuted at MidAmerica airport. Pan American Airways (Pan Am) offered non-stop service between Orlando Sanford and St. Louis. After the terrorist attacks of September 11, 2001, airline travel dropped significantly across the United States. This drop-off in air travel ultimately affected Pan American Airways service at the fledgling airport. Pan American Airways ceased operations at the airport December 3, 2001.
Great Plains Airlines filed for bankruptcy in January 2004 and ultimately ceased all operations.
TransMeridian Airlines began service at Mid-America on November 21, 2004. The Atlanta-based airline filed for bankruptcy on September 29, 2005, then ceased service to all destinations - including Mid-America.
Allegiant Air started service from the St. Clair County-owned airport on April 29, 2005. Allegiant Air offered flights to both McCarran International Airport in Las Vegas and Orlando Sanford International Airport until January 3, 2009. However, as stated previously, Allegiant Air resumed operations with twice-weekly direct flights between MidAmerica St. Louis Airport and Orlando/Sanford beginning on November 7, 2012. Since that time, direct flights were added to McCarran International Airport, St. Pete–Clearwater International Airport, and Punta Gorda Airpor. Direct flights to Jacksonville International Airport and Destin–Fort Walton Beach Airport will begin in June 2016.


== Passenger facilities ==
The passenger terminal was planned as an expandable facility that will provide passenger accommodations for the initial opening of the MidAmerica Airport. The passenger terminal is located in a 250-foot-wide by 700-foot long terminal building expansion envelope, bounded by a runway and parking lot. In the initial build-out, the terminal provides two upper level departure gates (both have jet bridges) with additional ground-level gates to serve smaller commuter aircraft.


== Airline and destinations ==
The following airline offers scheduled passenger service:


== Cargo airlines ==
Between late 2009 and August 2010, one air cargo company, LAN Cargo, used the airport to import flowers from Bogota, Colombia, totaling one flight each week until the county abruptly ended the flights.
Both Boeing Aerospace and Northbay Produce currently have facilities on site. With Northbay Produce mainly shipping blueberries from Michigan and Chile into the airport and using a completely refrigerated warehouse for packaging and distribution. Boeing uses the facility at MidAmerica for sub-assemblies to multiple military aircraft.
Boeing and Northbay have recently constructed additions to their facilities at MidAmerica Airport.


== Cargo facilities ==
Dual Category II Instrument Landing Systems
Simultaneous instrument approaches
Runway 14L/32R, 10,000 feet
Runway 14R/32L, 8,011 feet
Unconstrained Air Traffic Control and air space environment
Immediate Interstate access via I-64, I-44, I-55 and I-70
Air cargo facility development site ranging from 10 to 200+ acres
An initial air cargo ramp of 258,000 square feet or 5.9 acres
An air cargo terminal site adjacent to the ramp
Air rescue and fire Index C facility, capable of adjustment, as operational requirements dictate
New state-of-the-art control tower, 24-hours daily, seven days a week
New fuel farm
Aircraft handling services
Foreign Trade Zone
Enterprise Zone


== Runways ==
MidAmerica St. Louis Airport covers 7,003 acres (2,834 ha) and has two runways:
Runway 14L/32R: 10,000 by 150 feet (3,048 x 46 m), Surface: Concrete, ILS equipped.
Runway 14R/32L: 8,011 by 150 feet (2,442 x 46 m), Surface: Asphalt/Concrete, ILS equipped.
MidAmerica's construction has included creation of the 10,000-foot (3,000 m) 14L/32R (east) runway, adding 1,000 feet (300 m) to the existing west runway, adding passenger and cargo terminals on the east side of the facility and a 7,000-foot (2,100 m) taxiway connecting the two runways. A new air traffic control tower staffed by Air Force personnel was also constructed midway between the two runways.


== Maintenance and operations ==
The civil operations are administered by St. Clair County, Illinois, which also pays the maintenance costs for the east runway. Over half of all air operations at the facility utilize the eastern runway.


== Statistics ==
The FAA has not released calendar year 2015 enplanements yet, but airport officials noted enplanements exceeded 31,458, nearly double the 2014 total. In calendar year 2014, the airport was ranked 363 in the United States with 16,328 passenger enplanements. This was a 20.57% increase over 2013. In calendar year 2013, the dual-use facility was ranked 373 in the United States with 13,542 passenger enplanements. This placed it ninth in the state of Illinois. By comparison, Lambert-St. Louis International Airport was ranked 31 in the United States with over 6.21 million enplanements.
For the 12-month period ending December 31, 2010, the airport had 21,508 aircraft operations, an average of 58 per day: 66% military, 34% general aviation, and <1% scheduled commercial. At that time there were 35 military aircraft based at this airport.


== Public transportation ==


=== Light rail ===

Shiloh-Scott St. Louis MetroLink rail station links Scott Air Force Base with direct trains to downtown St. Louis on MetroLink's Red Line. One-ride and all-day tickets can be purchased from vending machines on the platforms. MetroLink lines provide direct or indirect service to St. Louis, the Clayton area, and Illinois suburbs in St. Clair County.


=== MetroBus ===
Five MetroBus lines serve Scott Air Force Base via Shiloh–Scott (St. Louis MetroLink) station.
12 O'Fallon Fairview Heights
15 Belleville Shiloh Scott
17X Lebanon – Mascoutah Express
21 Main & East Base Shuttles
512 Metrolink Station Shuttle


== References ==


== External links ==
MidAmerica St. Louis Airport, official site
Aerial image as of April 1998 from USGS The National Map
FAA Airport Diagram (PDF), effective April 28, 2016
FAA Terminal Procedures for BLV, effective April 28, 2016

Resources for this airport:
FAA airport information for BLV
AirNav airport information for KBLV
ASN accident history for BLV
FlightAware airport information and live flight tracker
NOAA/NWS latest weather observations
SkyVector aeronautical chart, Terminal Procedures