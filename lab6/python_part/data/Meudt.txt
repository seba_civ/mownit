Meudt is an Ortsgemeinde – a community belonging to a Verbandsgemeinde – in the Westerwaldkreis in Rhineland-Palatinate, Germany. It is the second biggest community in the Verbandsgemeinde of Wallmerod, a kind of collective municipality.


== Geography ==


=== Location ===
The community lies in the Westerwald between Montabaur and Westerburg.


=== Constituent communities ===
Meudt’s Ortsteile are Eisen, Ehringhausen and Dahlen, which together with the main centre, also called Meudt, make up the Ortsgemeinde of Meudt. Ehringhausen lies between Meudt and Hahn am See not far from Bundesstraße 8 and has about 40 inhabitants.


== History ==
With its first documentary mention in 1097 as muda or muede, the community is among the oldest settled centres in the Westerwald. Because of its older names of muda or muede it is assumed that the community is nevertheless older. The name muda suggests an old tollhouse (Maut is the German word for toll).
Meudt passed in 1564 from the County of Diez to the Electorate of Trier, and in 1803 to Nassau.


== Politics ==


=== Community council ===
The council is made up of 17 council members, including the extraofficial mayor (Bürgermeister), who were elected in a municipal election on 13 June 2004.


=== Coat of arms ===
The community’s arms were officially conferred on 22 January 1938 and the design is taken from the community seal borne since Nassau times (since 1816), and bears Nassau tinctures. The key stands for the community’s patron saint, Peter.


== Culture and sightseeing ==


=== Buildings ===
The town hall built in 1596 is the oldest maintained in the Westerwald. Notable is its roof construction with the cambered spire light. Also worth seeing are the Gangolfusbrunnen (spring), which is the Eisenbach’s source, and the Jewish cemetery.
The Gangolfushalle, newly opened in 2006, offers modern event space for over 500 seating places.


== Economy and infrastructure ==


=== Transport ===
West of the community is found Bundesstraße 255, linking Montabaur and Herborn, and to the east runs Bundesstraße 8 from Limburg an der Lahn to Hennef. The nearest Autobahn interchange is Montabaur on the A 3 (Cologne–Frankfurt), some 7 km away. The nearest InterCityExpress stop is the railway station at Montabaur on the Cologne-Frankfurt high-speed rail line.


== References ==


== External links ==
Meudt in the collective municipality’s Web pages (German)