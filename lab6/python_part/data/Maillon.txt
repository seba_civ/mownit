A maillon, maillon rapide or quick link is a metal link, similar to a carabiner. Maillons have a threaded sleeve which tightens over a thread, as opposed to a hinged gate like a carabiner, making them stronger, but more difficult to use. Like carabiners, maillons are available in a range of shapes and thicknesses (i.e. strengths), and often offer greater versatility over carabiners as their different shapes and lack of hinged gates allow them to be used in multi-directional load situations.
The word maillon comes from the French language, meaning "link".


== Usage ==
Maillons are used primarily in climbing and caving. In caving they are used to make secure and vital connections such as those when using single rope technique, or for attaching ropes to anchor points. In climbing they are used to construct leave-in-place abseil anchors. Maillons can also be used for fastening harnesses with a dual attachment point.


== Variations ==
The length of the gate opening, and of the maillon itself, can be varied. Supplierd typically have a short and long (symmetrical) oval in their range, as well as other shapes.
"D" maillon: the central component of a caving harness or single rope technique kit on which all items of equipment are attached. So called because it is shaped like the letter D.
"Delta" maillon: a variation of the D maillon, the delta maillon is triangular in shape, rather than semi-circular.


== References ==