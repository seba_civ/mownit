Reifenstein Abbey was a Cistercian abbey near the present village of Kleinbartloff in the Eichsfeld in Thuringia, Germany.


== First building ==
It was founded on 1 August 1162 by Count Ernst of Tonna-Gleichen, on a site then known as Albolderode, and was settled by monks from Volkenroda Abbey near Mühlhausen.
The abbey was economically successful and by the end of the thirteenth century had acquired about fifty estates in the neighbourhood. Little is known however of its internal affairs: even the sequence of the abbots is uncertain.
In 1521 Heinrich Pfeifer, a monk at Reifenstein, left the abbey to become a Lutheran. He preached rebellion in his native town of Mühlhausen, shared the leadership in the German Peasants' War in Thuringia with Thomas Münzer, and in May 1525, burnt Reifenstein Abbey to the ground. After the Battle of Frankenhausen he was captured near Eisenach and executed, dying impenitent. In 1524 there had only been six monks left in Reifenstein, which after the fire underwent a complete decline: in 1539 only one remained in the ruins, and shortly afterwards the site was completely deserted. In 1575 there was again a single monk, and in 1579, five or six, but they led so lawless a life that Reifenstein, according to a contemporary report, resembled a robbers' cave.


== Second building ==
The church was restored in 1582 and the monastery in 1585. The exemplary Abbot Philipp Busse (1589–1639) re-established discipline and order, but during the Thirty Years' War the abbey was pillaged seven times and almost reduced to ashes, Abbot Philipp was carried off as a prisoner, and six or seven monks were murdered. The few remaining monks sought shelter in caves, and begged bread from the peasants.


== Third building ==
The reconstruction of the abbey did not occur until Abbot Wilhelm Streit (1690–1721) took up office. By 1738 it had twenty-four members, and was able to survive the turmoil of the Seven Years' War. Between 1737 and 1743 the new abbey church was spectacularly rebuilt in the Baroque style.
The abbey was dissolved on 2 March 1803, and its territories and assets taken over by the Prussian State. The last abbot was Antonius Loffler (d. 1823).


== Post-dissolution ==
The premises were used for a variety of functions, mostly agricultural but also as a school of domestic science for young women. State ownership lasted until after World War II, when in 1951 the site was acquired by the University of Jena for use as a special clinic for tubercular and bone diseases. In 1964, it became the district hospital of the then district of Worbis and continues as a hospital to this day.
The imposing church however was entirely neglected and allowed to fall into ruin. Only the main doorway now remains.


== External links ==
(German) Brief history of Kloster Reifenstein
 This article incorporates text from a publication now in the public domain: Herbermann, Charles, ed. (1913). "Reifenstein". Catholic Encyclopedia. New York: Robert Appleton.