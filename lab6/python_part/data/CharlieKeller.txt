Charles Ernest "Charlie" Keller (September 12, 1916 – May 23, 1990) was a left fielder in Major League Baseball. From 1939 through 1952, Keller played for the New York Yankees (1939–43, 1945–49, 1952) and Detroit Tigers (1950–51). A native of Middletown, Maryland, he batted left-handed and threw right-handed. His ability to hit massive wall reaching fly balls, and home runs, earned him the nickname "King Kong".


== Career ==
For much of ten American League seasons, Keller joined with Joe DiMaggio and Tommy Henrich to form one of the best-hitting outfields in baseball history. A splendid all-round athlete at the University of Maryland, where he earned a degree in agricultural economics in 1937, Keller joined the Yankees in 1939 and quickly became the regular left fielder, with Henrich patrolling right field and DiMaggio in center.
Through much of his career, Keller was a feared slugger and a competent fielder. In his rookie season he hit .334 with 11 home runs and 83 RBI in 111 games. He hit three homers and batted .438 as the Yankees swept four games from the Cincinnati Reds in the World Series.
In his sophomore season, Keller hit .286 with 21 home runs, 93 RBI, 18 doubles and 15 triples. His most productive season came in 1941, when he hit .298 and posted career-highs in home runs (33), RBI (122), and triples (15).
Following service with the United States Merchant Marine in 1944 and 1945, Keller returned as a regular with the Yankees for the 1946 season. He collected 30 home runs, 29 doubles, and 10 triples, the second of his two 30-20-10 seasons.
Keller played part-time from 1947 to 1949 while troubled by a ruptured disc in his back. He was released by the Yankees before the 1950 season and signed a two-year contract with the Detroit Tigers, serving mostly as a pinch-hitter. In 1952 he re-signed with New York in September, appearing in two games, then was released in October, marking the end of his career.


== Legacy ==
In a 13-season career, Keller was a .286 hitter with 189 home runs and 760 RBI in 1170 games. A five-time All-Star selection, he compiled a career .410 on-base percentage and a .518 slugging average for a combined .928 OPS. In his four World Series appearances, he batted .306 with five home runs, and 18 RBI in 19 games.
His career Adjusted On-base Percentage plus Slugging (OPS+) of 152, the sum of his On-base percentage plus Slugging Average adjusted for era, stadium, and other cross-time considerations, places him number 28 on the all-time list, ahead of a retinue of dozens of Hall of Famers that includes Honus Wagner, Mike Schmidt, and Reggie Jackson.
Beginning with his first of two 30-20-10 seasons in 1941 only three other players have matched the feat: Joe DiMaggio (who had done it twice before that), Willie Mays, and Duke Snider. The list of players who managed it only once – which includes Mickey Mantle, Hank Aaron, Stan Musial, Rudy York, Ernie Banks, Dick Allen, Mike Schmidt, Andre Dawson, Dave Parker, Nomar Garciaparra, Vladimir Guerrero, and Jimmy Rollins – gives some indication of the power and versatility Keller brought to the game.
Following his retirement as a player, Keller founded Yankeeland Farm and had a successful career as a horse breeder – pacers and trotters – near his hometown of Middletown, Maryland. He named many of his horses after the franchises he played for: Fresh Yankee, Handsome Yankee, Yankee Slugger and Guy Yankee. He also benefited by owning syndicated shares of several stallions, which entitled him to free stud fees.
Keller was elected to the Frederick County and Maryland Sports Hall of Fame, the Kinston Professional Baseball Hall of Fame, the International League Hall of Fame and the University of Maryland Hall of Fame.
Charlie Keller died at his Frederick, Maryland farm, at age of 73.


== Facts ==
Because of his strength, Keller was dubbed "King Kong Keller", a nickname he never liked and seldom answered to.
In the third game of the 1939 World Series against the Cincinnati Reds, Keller became the first rookie to hit two home runs in a World Series game. Fellow Yankee Tony Kubek and St. Louis Cardinal Willie McGee would accomplish this same feat in the 1957 World Series against the Milwaukee Braves and the 1982 World Series against the Milwaukee Brewers respectively; coincidentally, their feats would also occur in a Game 3. Both Kubek and McGee would also accomplish their feats at Milwaukee County Stadium. In Game 1 of the 1996 World Series, Andruw Jones of the Atlanta Braves accomplished the feat by hitting 2 home runs at the old Yankee Stadium.
Keller got his start in baseball with the semi-pro Kinston Eagles. He was inducted in the Kinston Professional Baseball Hall of Fame in 1983.
Despite his power and high batting average, Keller was the first Yankees player to strike out over 100 times in a single season, whiffing 101 times in 1946.
Brother Hal Keller played catcher for the Washington Senators from 1949 to 1952, going on to serve in front-office positions with the Senators/Texas Rangers (1961–78), and Seattle Mariners (1979–85). He served as the Mariners' Vice President, Baseball Operations/General Manager from 1984 to 1985.
Wore #99 for the Yankees in 1952.


== References ==


== Sources ==
Career statistics and player information from Baseball-Reference
Baseball Library
The Deadball Era
Vintage Card Traders


== External links ==
Charlie Keller at Find a Grave