Plaza Sendero is a chain of shopping malls in Mexico. The shopping malls usually feature full service restaurants, banks, and clothing stores.


== Sendero properties ==
Sendero properties and major anchors.
Centro Comercial Sendero - San Nicolás de los Garza (Monterrey)
Soriana
Cinépolis multiplex
Catholic chapel

Las Torres Plaza Comercial - Ciudad Juárez (156,049 m2)
Plaza Periférico - Reynosa (179,042 m2)
Soriana
Cinépolis multiplex
Woolworth
Coppel
GNC

Plaza Sendero - Hermosillo (152,881 m2)
Soriana
Cinépolis
Coppel
Carl's Jr.
GNC
Burger King
Dairy Queen
Julio Cepeda Jugeterias
Woolworth

Plaza Sendero - Matamoros (146,394 m2)
Soriana
Cinépolis multiplex
Famsa
Coppel
Peter Piper Pizza
Carl's Jr.
GNC

Plaza Sendero - Saltillo (149,205 m2)
Soriana
Cimaco
Cinépolis
Toks
Peter Piper Pizza
Subway
Famsa
Coppel
Burger King
Julio Cepeda Jugueterias
GNC
Nutrisa

Plaza Sendero - Querétaro (128,781 m2)
Sendero - Ixtapaluca (180,647 m2)
Sendero - San Luis Potosí (137,979 m2)
Sendero - Toluca (189,384 m2)


== Proposed and under construction ==
Los Cabos
Veracruz
Acapulco
Villahermosa
Mérida
Apodaca
Tampico
Juárez
Ecatepec
Valladolid, Yucatán


== References ==