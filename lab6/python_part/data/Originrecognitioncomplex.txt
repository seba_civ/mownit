In molecular biology, origin recognition complex (ORC) is a multi-subunit DNA binding complex (6 subunits) that binds in all eukaryotes in an ATP-dependent manner to origins of replication. The subunits of this complex are encoded by the ORC1, ORC2, ORC3, ORC4, ORC5 and ORC6 genes. ORC is a central component for eukaryotic DNA replication, and remains bound to chromatin at replication origins throughout the cell cycle. ORC directs DNA replication throughout the genome and is required for its initiation. ORC bound at replication origins serves as the foundation for assembly of the pre-replication complex (pre-RC), which includes Cdc6, Tah11 (aka Cdt1), and the Mcm2-Mcm7 complex. Pre-RC assembly during G1 is required for replication licensing of chromosomes prior to DNA synthesis during S phase. Cell cycle-regulated phosphorylation of Orc2, Orc6, Cdc6, and MCM by the cyclin-dependent protein kinase Cdc28 regulates initiation of DNA replication, including blocking reinitiation in G2/M phase.
In yeast, ORC also plays a role in the establishment of silencing at the mating-type loci Hidden MAT Left (HML) and Hidden MAT Right (HMR). ORC participates in the assembly of transcriptionally silent chromatin at HML and HMR by recruiting the Sir1 silencing protein to the HML and HMR silencers.
Both Orc1 and Orc5 bind ATP, though only Orc1 has ATPase activity. The binding of ATP by Orc1 is required for ORC binding to DNA and is essential for cell viability. The ATPase activity of Orc1 is involved in formation of the pre-RC. ATP binding by Orc5 is crucial for the stability of ORC as a whole. Only the Orc1-5 subunits are required for origin binding; Orc6 is essential for maintenance of pre-RCs once formed. Interactions within ORC suggest that Orc2-3-6 may form a core complex.


== References ==


== Further reading ==
Stephen P. Bell and Anindya Dutta, DNA REPLICATION IN EUKARYOTIC CELLS, Annual Review of Biochemistry, 2002. doi:10.1146/annurev.biochem.71.110601.135425. A comprehensive review of molecular DNA replication.
This article incorporates text from the public domain Pfam and InterPro IPR007220
This article incorporates text from the public domain Pfam and InterPro IPR010748
This article incorporates text from the public domain Pfam and InterPro IPR008721