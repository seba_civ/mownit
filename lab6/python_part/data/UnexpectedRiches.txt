Unexpected Riches is a 1942 Our Gang short comedy film directed by Herbert Glazer. It was the 211th Our Gang short (211th episode, 122nd talking short, 123rd talking episode, and 42nd MGM produced episode).


== Plot ==
Weighing themselves on a penny machine, the gang receives a fortune card predicting that they will receive "unexpected riches." Acting upon this, the kids decide to dig for buried treasure, using a fradulent map provided by one of their wise-guy acquaintances. Though the treasure hunt comes a-cropper, the fortune card's prediction comes true in an unexpected fashion.


== Notes ==
This is the last appearance of George McFarland as Spanky, ending his eleven-year tenure with the series. Billie "Buckwheat" Thomas would remain the only holdover from the Hal Roach Our Gang era to stay with the series until its end in 1944.


== Cast ==


=== The Gang ===
Bobby Blake as Mickey
Billy Laughlin as Froggy
George McFarland as Spanky
Billie Thomas as Buckwheat


=== Additional cast ===
Barry Downing as Ken Reed
Emmet Vogan as Mr. Reed
Ernie Alexander as Mickey's father
Margaret Bert as Mickey's mother
Willa Pearl Curtis as Big Shot's mother
Symona Boniface as Person in Froggy's dream
Stanley Logan as Person in Froggy's dream
Ernestine Wade as Person in Buckwheat's dream


== See also ==
Our Gang filmography


== References ==


== External links ==
Unexpected Riches at the Internet Movie Database