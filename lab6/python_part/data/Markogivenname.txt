Marko is a masculine given name, a variation of Mark. People bearing the name include:
Prince Marko (c. 1335–1395), Serbian feudal lord in Macedonia
Marko Ahtisaari (born 1969), Finnish businessman and musician
Marko Albert (born 1979), Estonian triathlete
Marko Albrecht (born 1970; better known as Mark 'Oh), German DJ
Marko Anđelković (born 1984), Serbian football player
Marko Andić (born 1983), Serbian football player
Marko Antila (born 1969), Finnish film producer and director
Marko Anttila (born 1985), Finnish ice hockey player
Marko Arnautović (born 1989), Austrian football player
Marko Asell (born 1970), Finnish wrestler and politician
Marko Asmer (born 1984), Estonian racecar driver
Marko Baacke (born 1980), German Nordic combined skier
Marko Babić (disambiguation), multiple people
Marko Bajić (born 1985), Serbian football player
Marko Banić (born 1984), Croatian basketball player
Marko Barun (born 1978), Slovenian football player
Marko Baša (born 1982), Montenegrin football player
Marko Bašara (born 1984), Serbian football player
Marko Bašić (born 1984), Croatian football player
Marko Bedenikovic (born 1984), Canadian football player
Marko Bezjak (born 1986), Slovenian handball player
Marko Bezruchko (1883–1944), Ukrainian military commander
Marko Blažić (born 1985), Serbian football player
Marko Bošković (born 1982), Serbian football player
Marko Božič (born 1984), Slovenian football player
Marko Brainović (born 1920), Croatian water polo player
Marko Bruerović (1770–1823), Croatian writer
Marko Bulat (born 1973), Serbian pop-folk singer and musician
Marko Čakarević (born 1988), Serbian basketball player
Marko Car (disambiguation), multiple people
Marko Cavka (born 1981), American football player
Marko Čelebonović (1902–1986), Serbian painter
Marko Cepenkov (1829–1920), Macedonian folklorist
Marko Ćetković (born 1986), Montenegrin football player
Marko Cheremshyna (1874–1927), Ukrainian writer
Marko Ciurlizza (born 1978), Peruvian football player
Marko Čolaković (born 1980), Montenegrin football player
Marko Antonio Cortés Mendoza (born 1977), Mexican politician
Marko Đalović (born 1986), Serbian football player
Marko Dapcevich (born 1969), American politician
Štefan Marko Daxner (1822–1892), Slovak politician, lawyer and poet
Marko Dević (born 1983), Ukrainian football player of Serbian descent
Marko Dinjar (born 1986), Croatian football player
Marko Djokovic (born 1991), Serbian tennis player
Marko Djurdjević (born 1979), German illustrator and concept artist of Serbian descent
Marko Dmitrović (born 1992), Serbian football goalkeeper
Marko Đorđević (footballer) (born 1983), Serbian football player
Marko Đorđević (skier)
Marko Došen (1859–1944, Croatian politician
Marko Elsner (born 1960), Slovenian football player
Marko Filipović (born 1978), Bosnian Serb football player
Márkó Futács (born 1990), Hungarian football player
Marko Grgić (born 1987), Croatian football player
Marko Grubelić (born 1980), Serbian football player
Marko Attila Hoare (born 1972), Yugoslavian British historian
Marko Hranilović (died 1931), Croatian politician
Marko Hucko, Slovak ice hockey player
Marko Ilić (born 1985), Serbian football player
Marko Ivović, Serbian volleyball player
Marko Janjetović (born 1984), Croatian football player
Marko Jantunen (born 1971), Finnish ice hockey player
Marko Jarić (born 1978), Serbian basketball player
Marko Jesic (born 1989), Australian football player of Serbian descent
Marko Jovanović (disambiguation), several people
Marko Kantele (born 1986), Finnish darts player
Marko Kartelo (born 1981), Croatian football player
Marko Kauppinen (born 1979), Finnish ice hockey player
Marko Kemppainen (born 1976), Finnish sports shooter
Marko Kešelj (born 1988), Serbian basketball player
Marko Kiprusoff (born 1972), Finnish ice hockey player
Marko Kitti (born 1970), Finnish writer
Marko Klasinc (born 1951), Slovenian chess problemist
Marko Klok (born 1968), Dutch volleyball player
Marko Kmetec (born 1976), Slovenian football player
Marko Koers (born 1972), Dutch runner
Marko Koivuranta (born 1978), Finnish football player
Marko Kolsi (born 1985), Finnish football player
Marko Kon (born 1972), Serbian singer
Marko Kopilas (born 1983), Croatian football player
Marko Kovač (born 1981), Serbian architect and film director
Marko Kravos (born 1943), Slovenian writer and translator
Marko Kristal (born 1973), Estonian football player
Marko Krizin (1589–1619), Croatian saint
Marko Leko (1853–1932), Serbian chemist
Marko Lelov (born 1973), Estonian football player
Marko Lepik (born 1977), Estonian football player
Marko Lerinski (1862–1902; born as Georgi Ivanov Gyurov), Bulgarian military man
Marko Liefke (born 1974), German volleyball player
Marko Lihteneker (1959–2005), Slovenian ski mountaineer and mountain climber
Marko Liias (born 1981), American politician of Finnish descent
Marko Ljubinković (born 1981), Serbian football player
Marko Lomić (born 1983), Serbian football player
Marko Lopušina, Serbian journalist and publicist
Marko Lunder (born 1983), Slovenian football player
Marko Mäetamm (born 1965), Estonian artist
Marko Mäkinen (born 1977), Finnish ice hockey player
Marko Maksimović (born 1984), Bosnian football player
Marko Marić (born 1983), Croatian football player
Marko Marin
Marko Marin (footballer) (born 1989), German football player of Bosnian Serb origin
Marko Marin (professor) (born 1930), Slovenian theatre director and art historian

Marko Marinović (born 1983), Serbian basketball player
Marko Markov (born 1981), Bulgarian football player
Marko Markovski (born 1986), Serbian football player
Marko Marović (born 1983), Serbian football player
Marko Martin (born 1975), Estonian pianist
Marko Marulić (1450–1524), Croatian poet
Marko Matvere (born 1968), Estonian actor and singer
Marko Meerits (born 1992), Estonian football player
Marko Mesić (disambiguation), several people
Marko Milenkovič (born 1976), Slovenian swimmer
Marko Milić (disambiguation), several people
Marko Milinković (born 1988), Serbian football player
Marko Milivojević, Serbian drummer
Marko Miljanov (1833–1901), Serbian Montenegrin writer
Marko Milovanović (born 1982), Serbian football player
Marko Mirić (born 1987), Serbian football player
Marko Mitchell (born 1985), American football player
Marko Mitrović (born 1978), Serbian football player
Marko Mitrović (footballer born 1992), Swedish football player
Marko Mlinarić (born 1960), Croatian football player
Marko Mugoša (born 1984), Montenegrin football player
Marko Murat (1864–1944), Croatian painter
Marko Mušič (born 1941), Slovenian architect
Marko Muslin (born 1985), French football player of Serbian descent
Marko Myyry (born 1967), Finnish football player
Marko Natlačen (1886–1942), Slovenian politician and jurist
Marko Nešić (disambiguation) several people
Marko Nikolić (football coach) (born 1979), Serbian coach
Marko Nikolić (footballer) (born 1989), Serbian midfielder
Marko Nowak, German ice hockey player
Marko Orešković (1895–1941), Croatian partisan
Marko Orlandić (born 1930), Montenegrin politician
Marko Oštir (born 1977), Slovenian handball player
Marko Palavestrić (born 1982), Serbian football player
Marko Palo (born 1967), Finnish ice hockey player
Marko Pantelić (born 1978), Serbian football player
Marko Pavlović (born 1982), Serbian football goalkeeper
Marko Perković (born 1966), Croatian singer
Marko Perović (disambiguation), several people
Marko Perunicic (born 1979), Serbian composer, music arranger and producer
Marko Petkovšek (born 1955), Slovenian mathematician
Marko Pogačnik (born 1984), Slovenian artist and writer
Marko Pohlin (1735–1801), Slovenian philologist and writer
Marko Pomerants (born 1964), Estonian politician
Marko Popović (disambiguation), multiple people
Marko Pöyhönen, Finnish ice hockey player
Marko Pregl, Slovenian politician
Marko Pridigar (born 1985), Slovenian football player
Marko Pusa (born 1977), Finnish darts player
Marko Putinčanin (born 1987), Serbian football player
Marko Račič (born 1920), Slovenian runner
Marko Rajamäki (born 1968), Finnish football manager and former player
Marko Ranđelović (born 1984), Serbian football player
Marko Ranilovič (born 1986), Slovenian football player
Marko Rašo (born 1989), Croatian football player
Marko Rehmer (born 1972), German footballer
Marko Reijonen (born 1967; better known as Waldo), Finnish Eurodance musician
Marko Rudić (born 1990), Bosnia and Herzegovina alpine skier
Marko dos Santos (born 1981), Brazilian football player
Marko Šarlija (born 1982), Croatian football goalkeeper
Marko Savić
Marko Savić (born 1941), Serbian pianist
Marko Savić (born 1984), Serbian football player

Marko Savolainen (born 1973), Finnish bodybuilder
Marko Šćepović (born 1991), Serbian football player
Marko Sentić (born 1976), Croatian judoka
Marko Simeunovič (born 1967), Slovenian football player
Marko Šimić (disambiguation), several people
Marko Škop (born 1974), Slovak film director
Marko Škreb (born 1957), Croatian economist
Marko Snoj (born 1959), Slovenian linguist
Marko Sočanac (born 1978), Serbian football player
Marko Sopi (1938–2006), Albanian clergyman
Márkó Sós (born 1990), Hungarian football player
Marko Spittka (born 1971), German judoka
Marko Stanković (born 1986), Austrian football player of Serbian descent
Marko Stanojevic (born 1979), Italian rugby union footballer
Marko Stetner, Slovenian politician
Marko Strahija (born 1975), Croatian swimmer
Marko Šuler (born 1983), Slovenian football player
Marko Sušac (born 1988), Bosnian football player
Marko Šutalo (born 1983), Bosnian basketball player
Marko Tajčević (1900–1984), Croatian and Serbian composer and musician
Marko Tkalec (born 1977), Slovenian tennis player
Marko Todorović (1929–2000), Bosnian Serb actor
Marko Tomas (born 1985), Croatian basketball player
Marko Tomasović (disambiguation), several people
Marko Topić (born 1976), Bosnian football player
Marko Tredup (born 1974), German football player
Marko Tuomainen (born 1972), Finnish ice hockey player
Marko Tuomela (born 1972), Finnish football player
Marko Tušek (born 1975), Slovenian basketball player
Marko Tuulola (born 1971), Finnish ice hockey
Marko Valok (born 1927), Serbian football player
Marko Varalahti, Finnish strongman
Marko Vego (1907–1985), Croatian archaeologist
Marko Vejinović (born 1990), Dutch football player
Marko Veselica (born 1936), Croatian politician
Marko Vešović (born 1991), Montenegrin football player
Marko Vidojković (born 1975), Serbian writer
Marko Vidovic (born 1988), Serbian football player
Marko Vovchok (1833–1907), Ukrainian writer
Marko Vranić (born 1978), Serbian football player
Marko Vuckovic, American football players
Marko Vuoriheimo (born 1978; better known as Signmark), Finnish deaf rapper
Marko Wahlman (born 1969), Finnish hammer thrower
Marko Wiz, Slovenian politician
Marko Yli-Hannuksela (born 1973), Finnish wrestler
Marko Zaror (born 1978), Chilean martial artist, actor and stuntman
Marko Živić, Serbian television journalist (Marko Živić Show)
Marko Zorić (born 1980), Serbian football player


== See also ==
Marco (given name)
Markko (disambiguation)
Marcos (given name), Marcos or Markos