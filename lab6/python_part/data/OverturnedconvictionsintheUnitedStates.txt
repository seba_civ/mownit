This is a list of notable overturned convictions in the United States.


== Alabama ==
Blount County
Bill Wilson was convicted of the 1912 murder of his wife and child and sentenced to life in jail. He was exonerated in 1918 when they were both found living in Indiana.
Jackson County
The Scottsboro Boys were nine black juveniles convicted of an alleged 1931 rape of a white girl, eight of whom were initially sentenced to die by the electric chair. All were later either pardoned or had their convictions overturned.
Jefferson County
Anthony Ray Hinton was wrongfully convicted in 1985 of the murder two men in Birmingham and sentenced to death. The Supreme Court ruled in 2014 that his defence was inadequate and he was exonerated in 2015 by a Jefferson Court judge after spending nearly 30 years in jail.


== Arizona ==
Maricopa County
Ray Krone was sentenced to death for the 1991 murder of a bar manager. Bite marks found on the victim were said to match Krone's teeth. DNA tests exonerated Krone in 2001.
Debra Milke was convicted of ordering her son's murder in 1990. She was sentenced to death. Her conviction was vacated in 2013.


== Arkansas ==
Marion County
Charles Hudspeth was convicted of the 1887 murder of his lover's missing husband. Hudspeth was hanged in 1892, but the husband was found alive, living in Kansas in 1893.


== California ==
Alameda County
Huey Newton was convicted of the 1967 manslaughter of John Frey, an Oakland Patrolman. His conviction was overturned in 1970.
Los Angeles County
Francisco "Franky" Carrillo was convicted of the 1991 murder of Donald Sarpy and sentenced to life in jail. His conviction was reversed by the Los Angeles County Superior Court on March 14, 2011, after he had served twenty years in prison.
Geronimo Pratt, a Black Panther leader, was convicted in 1972 of the 1968 murder of a white school teacher and sentenced to life in jail. He was exonerated in 1997.


== Florida ==
Miami-Dade County
Joseph Shea was convicted of the 1959 murder of an airline clerk and sentenced to life in jail. He was retried and acquitted in 1966.
DeSoto County
James Joseph Richardson was convicted of the 1967 poisoning murder of seven of his children and sentenced to death. He was exonerated in 1989.
Gulf County
Wilbert Lee & Freddie Pitts were sentenced to death for the 1963 robbery and murder of two Port St. Joe gas station attendants. They were released and pardoned in 1975.
Leon County
Five black men were accused of murdering an off-duty sheriff's deputy in 1970. Two were convicted (David Keaton was given the death sentence) before evidence emerged that exonerated all five.


== Georgia ==
Chatham County
Samuel Scott and Douglas Echols. were convicted of a 1986 kidnapping, rape, and robbery. Scott was sentenced to life in jail plus 20 years and Echols was sentenced to five years. DNA tests exonerated them both in 2002.
Cobb County
The Marietta Seven were convicted of the 1971 murder of two physicians in Marietta. The seven were exonerated in 1975.
DeKalb County
Clarence Harrison was convicted of the 1986 kidnapping, robbery, and rape of a 25-year-old woman in Decatur and sentenced to life in jail. DNA tests exonerated him in 2004.
Floyd County
Marcus Dixon was convicted in 2003 of the rape of Kristie Brown, a 15-year-old schoolgirl. These charges were later overturned by the Georgia Supreme Court and dropped to statutory rape.
Fulton County
Leo Frank was convicted of the 1913 murder of a 13-year-old female employee in Atlanta. He was lynched in 1915, and posthumously pardoned in 1986.


== Illinois ==
Cook County
Majczek and Marcinkiewicz were convicted of the 1932 murder of a Chicago police officer and were each sentenced to 99 years in jail. They were exonerated in 1945 and 1950.
Gary Dotson was convicted of a 1977 rape after he was identified by the victim and sentenced to 25 to 50 years in jail. The victim recanted in 1985 and DNA tests cleared him in 1989.
The Ford Heights Four (two of which were sentenced to death) were convicted of the 1978 murder of an engaged couple. DNA tests exonerated them in 1996.
Anthony Porter was sentenced to death for the 1982 murder of an engaged couple. He was exonerated in 1989.
David Dowaliby was convicted of the 1988 murder of his stepdaughter and sentenced to 45 years in jail. His conviction was reversed in 1991 on the grounds of insufficient evidence.
Lawrence County
Julie Rea Harper was convicted of the 1997 murder of her 10-year-old son and sentenced to 65 years in prison. She was acquitted on retrial in 2006.


== Indiana ==
Floyd County
David Camm was tried twice and sentenced to life in prison for the murder of his wife and two children. He was acquitted upon retrial in 2013. DNA found at the scene linked Charles Boney to the murders. Boney is serving 225 years in prison for the murders.
St. Joseph County
Richard Alexander was convicted of committing two 1996 rapes. He was exonerated of the crimes in 2001.


== Louisiana ==
Orleans Parish
Shareef Cousin was sentenced to death for a 1995 murder. He was released in 1998.
Mychal Bell was sentenced to 22 years for assault. Overturned September 14, 2007
Gregory Bright was sentenced to life for a 1973 murder. He was released in 2003.
Curtis Kyles was sentenced to death in 1994 for the murder of a woman. He was released in 1998
Robert Jones was sentenced to life for a 1992 murder. Overturned November 21, 2015


== Maryland ==
Baltimore County
Michael Austin was convicted of the 1974 murder of a grocery store security guard and sentenced to life in prison. After 27 years in jail he was exonerated in 2001.
Kirk Bloodsworth was convicted of the 1985 rape and murder of a 9-year-old girl and sentenced to death. DNA tests exonerated him in 1993.


== Massachusetts ==
Norfolk County
Sacco and Vanzetti were convicted of a 1920 double homicide and robbery. They were executed in 1927. Massachusetts Governor Dukakis in 1977 issued a proclamation that "...that any stigma and disgrace should be forever removed from the names of Nicola Sacco and Bartolomeo Vanzetti..." and declared August 23, 1977 as Nicola Sacco and Bartolomeo Vanzetti memorial day.
Suffolk County
Lawyer Johnson was sentenced to death for a 1971 Roxbury murder. He was exonerated in 1982.


== Michigan ==
Macomb County
Ken Wyniemko was convicted of a 1984 rape and robbery of a 28-year-old woman and sentenced to 40–60 years in prison. DNA tests exonerated him in 2003.
Thomas and Raymond Highers were convicted in 1988 of first-degree murder in the 1987 slaying of Robert Karey, a known Detroit drug dealer and were both sentenced to life in prison without the possibility of parole. After serving over 25 years in prison, Wayne County Circuit Court Judge Lawrence Talon vacated the brothers' convictions based on new eye-witness testimony and released the pair on bond. The Wayne County Prosecutor's office said they intended to re-try the brothers, beginning in October 2013. In September 2013, despite maintaining that her office still thought the pair committed the crime, Wayne County Prosecutor Kym Worthy announced that her office was dismissing all charges against the pair.


== Missouri ==
Boone County
Ryan Ferguson was convicted of the 2001 murder of sports editor Kent Heitholt and sentenced to 40 years in jail. His conviction was overturned in 2013 and he was released after key witnesses recanted their testimony.
St. Louis County
Johnny Briscoe was convicted of a 1982 rape and robbery and sentenced to 45 years in jail. DNA tests exonerated him in 2006.
Lincoln County
Russ Faria was convicted of the 2011 murder of his wife, Betsy Faria, and sentenced to life in prison, with no chance of parole. His wrongful conviction was challenged and a new trial was ordered in June 2015. He was acquitted on November 6, 2015.


== New Jersey ==
Essex County
Bill MacFarland was convicted of the 1911 arsenic murder of his wife and sentenced to death in the electric chair. He was acquitted on retrial.
Mercer County
The Trenton Six were convicted of the 1948 murder of a Trenton shopkeeper and sentenced to death. All six were exonerated by 1952.
Passaic County
Rubin Carter, a middleweight boxer was convicted twice of a 1966 triple murder and sentenced to a double life sentence. His conviction was overturned in 1985 and the indictments withdrawn in 1987.


== New York ==
New York County
The Central Park Five were five Harlem teens convicted of the 1989 assault and rape of a jogger in New York's Central Park. They served their sentences but afterwards their convictions were set aside in 2002 when DNA evidence cleared them and another man confessed to the crime.
Westchester County
In 1998 Kian Daniel Khatibi was convicted of a double stabbing after detectives from the Village of Pleasantville falsely claimed that the victims had identified Kian as their attacker and then forwarded this false information to the Westchester County District Attorney. In 2008, Kian was released from prison as the truth unraveled and the conviction was overturned.
Jeffrey Mark Deskovic was convicted of the 1989 of rape and murder of a high school classmate and sentenced to 15 years-life in jail. He was exonerated in 2006 when the DNA from the crime was matched to another person.


== North Carolina ==
Forsyth County
Darryl Hunt was convicted of the 1984 rape and murder of a newspaper reporter and sentenced to life imprisonment. He was exonerated in 2004 after DNA evidence and the confession of the true killer.


== Ohio ==
Cuyahoga County
Dr. Sam Sheppard was convicted of the 1955 murder of his wife and sentenced to life in jail. He was acquitted on retrial in 1966.


== Oklahoma ==
Cleveland County
Thomas Webb III, sentenced to 60+ years, served 13 years before being exonerated by DNA evidence.
Oklahoma County
David Johns Bryson, sentenced to 85 years in jail, served 16 years before being exonerated by DNA evidence.
Curtis Edward McCarty, sentenced to death, served 11 years before being exonerated by DNA evidence.
Robert Miller, sentenced to death, served 10 years before being exonerated by DNA evidence.
Jeffery Pierce, sentenced to 65 years, served 14.5 years before being exonerated by DNA evidence.
Pontotoc County
Dennis Fritz, sentenced to life, served 11 years before being exonerated by DNA evidence.
Calvin Lee Scott, sentenced to 25 years, served 20 years before being exonerated by DNA evidence.
Ron Williamson, sentenced to death, served 11 years before being exonerated by DNA evidence.
Tulsa County
Timothy Durham, sentenced to 3,220 years, served 3.5 years before being exonerated by DNA evidence.
Arvin McGee, sentenced to 298 years, served 11 years before being exonerated by DNA evidence.


== Oregon ==
Clackamas County
Santiago Ventura Morales was convicted in 1986 of murdering a farm worker and sentenced to 10 years-life. He was exonerated in 1990.


== Pennsylvania ==
Dauphin County
Jay C. Smith was convicted of the 1979 murder of a schoolteacher and her two children and sentenced to death. Smith’s conviction was overturned in 1992.
Delaware County
Nicholas Yarris was sentenced to death for a 1981 rape and murder. DNA tests exonerated him in 2004. See The Fear of 13
Philadelphia County
Fred Thomas was convicted of the 1993 murder of a Federal Express truck driver and sentenced to death. The conviction was overturned in 2002.


== South Carolina ==
Chester County
Two black brothers Thomas Griffin and Meeks Griffin were convicted of the 1913 murder of John Q. Lewis, a Confederate veteran, and executed in the electric chair. They were pardoned in 2009.
Clarendon County
Fourteen-year-old black George Stinney was convicted of the 1944 murder of two white girls and executed in the electric chair within three months of his conviction. He was exonerated in 2014.


== Texas ==
Burleson County
Anthony Charles Graves was convicted of the August 18, 1992 mass murder of six people in Somerville, after being implicated in the crime by Robert Carter - the father of one of the victims. Carter was executed in May 2000 for his part in the crime - and in his final statement took sole responsibility for the crime: "To the Davis family, I am sorry for all of the pain that I caused your family. It was me and me alone. Anthony Graves had nothing to do with it. I lied on him in court. ... Anthony Graves don't even know anything about it". Anthony graves is also known as Death Row Exoneree 138.
After spending 12 years on Death Row, Graves' conviction was overturned on March 3, 2006 by the Fifth Circuit Court of Appeals when, in a unanimous opinion, a three judge panel held that the state’s case had hinged on Carter’s perjured testimony, and concluded that the Prosecutor, Charles Sebesta, had intentionally withheld evidence that could have helped Graves; most notably that Carter had recanted right before he testified at Grave's August 1992 trial.
Graves was held for an additional four years in solitary confinement in the Burleson County jail awaiting retrial until October 27, 2010, when he was released after all charges were dismissed by Burleson County Special Prosecutor Kelly Siegler, who concluded, "He’s an innocent man. There is nothing that connects Anthony Graves to this crime."
Graves was awarded $1.4 million by the State of Texas in June 2011 for his wrongful conviction.
Dallas County
Randall Dale Adams was sentenced to death for of the 1976 murder of a police officer. He was exonerated in 1989.
Cornelius Dupree was convicted of aggravated robbery, which was alleged to have been committed during a rape in 1979. He was sentenced to 75 years in prison and paroled during the summer of 2010. After DNA evidence cleared him of the crime, he was declared innocent in January 2011. His 30 years of imprisonment is the longest of any exonerated inmate in Texas.
Jefferson County
Joe Elizondo was convicted in 1984, by a jury, of the 1983 aggravated sexual assault of his 10 yr-old step-son Robert Bienvenue. He was sentenced to life in prison and fined $10,000. In the fall of 1983, Robert and his 8 yr-old brother, Richard, told the Port Athur police, that they had been sexually abused by their mother, Mary Ann, 27, and their step-father, Joe, 49. At the time, the boys’ father, Richard Bienvenue, Sr. had legal custody and the boys spent weekends with their mother and step-father. On Sept. 23, 1983, the Elizondo's were charged with sexually assaulting the boys. After they were arrested, the couple’s one-year-old daughter was removed from their custody and was later adopted by another family. In August 1984, Joe Elizondo went on trial in Jefferson County Criminal District Court, on a single count of assaulting Robert Bienvenue. Robert told the jury that both he and his brother were forced to watch sexually explicit videos and to engage in oral sex with Joe, have oral contact with Mary Ann’s breast and to have anal sex with both Joe and Mary. At times, Robert testified, another man and two women joined them. The jury was also shown a sexually explicit picture of a kangaroo that Robert had drawn at school, as well as a sexually suggestive note written to him by a female classmate. Robert’s teacher confiscated these items, prompting interrogation by Robert’s father and the police. During that questioning Robert detailed the alleged abuse. Robert’s step-mother—the wife of Richard Bienvenue, Sr.—testified that the boys had told her of the abuse.
Mary Ann Elizondo was found guilty of sexually assaulting Robert during a separate trial from Joe's in 1984, and she was sentenced to 20 yrs in prison. She pled no contest to a second charge and was sentenced to 35 years in prison, to be served concurrently with the 20-year term. After Joe and Mary Ann were convicted, Robert and Richard had no further contact with them. It wasn't until 1988, on Robert's 17th birthday, that he found a letter written by his mother, Mary Ann, and learned for the first time that Joe and Mary Ann were in prison. He then began writing to authorities, letting them know that he had lied when he accused his mother and step-father of sexual abuse. Mary Ann was released on parole in 1991. She then divorced Elizondo, and married someone else. While on parole, she was ordered into group therapy and yold she had to admit to sexually abusing her two sons. When she refused, she was jailed for 6 months, before being released again on parole. A Petition for a Writ of Habeas Corpus was filed, and in August 1995, a hearing was finally held, concerning the recantations of both boys. Robert Bienvenue testified that his father threatened to spank him and his brother every day for the rest of their lives if they refused to testify against Elizondo and their mother. He said their father wanted to retaliate against his ex-wife for marrying Elizondo. Richard Bienvenue also testified. Though he denied that their father forced them to lie, he said the abuse never occurred.
In 1995, the trial court found the recantations credible, set aside Joe Elizondo’s conviction and ordered a new trial. In December 1996, the Texas Court of Criminal Appeals upheld the trial court ruling. “Robert’s recantation not only voids his trial testimony which implicated (Joe Elizondo), but constitutes affirmative evidence of (Joe Elizondo’s) innocence,” the appeals court ruled. “We are convinced by clear and convincing evidence that no rational jury would convict him in light of the new evidence.” On June 23, 1997, Jefferson County District Attorney Paul McWilliams dismissed the charges against Joe Elizondo and he was released from prison that day. Mary Ann then sought to vacate her conviction. In November 2005, the Texas Court of Criminal Appeals, following the decision made in Joe Elizondo’s case, vacated her conviction and the charges were dismissed. Sadly, Joe Elizondo, who's health was poor in prison, died in 2003. In October 2008, Mary Ann (Barbosa), who had filed a state compensation claim, received her compensation of $370,833. It is unknown as to whether charges were ever filed against Richard Bienvenue, Sr., for causing his sons to falsely accuse sexual abuse against Joe and Mary Ann Elizondo.
Montgomery County
Clarence Brandley was sentenced to death for the 1980 rape and murder of a 16-year-old girl. He was exonerated in 1990.
Nueces County
Hannah Overton was sentenced to life without parole murdering her child when in fact he accidentally ingested a lethal amount of salt in Sept, 2007. On Sept 17, 2014, the Texas Court of Criminal Appeals reversed the conviction and remanded it back to the trial court.
Smith County
Kerry Max Cook was sentenced to death for the 1977 murder of a 21-year-old secretary. He was freed in 1997.
Travis County
Richard Danziger and Chris Ochoa were convicted of a 1988 rape and murder of a woman and sentenced to life in jail. DNA tests and the confession of the true murderer exonerated the pair in 2001.
Williamson County
Michael Morton was convicted of the 1986 murder of his wife, sentenced to life and spent 25 years in prison. Morton was exonerated in 2011 after DNA evidence proved another man had committed the murder. Also, other evidence turned up after the DNA tests showed that the prosecutor, Ken Anderson had withheld other evidence from the defense that would have excluded Morton from the crime. Morton's lawyers accused Anderson of failing to provide defense lawyers with this exculpatory evidence indicating that another man might have killed Morton's wife, including information that his 3-year-old son witnessed the murder and said his dad was not home at the time. Former prosecutor's Ken Anderson who was now a judge after a "court of inquiry" was filed for failing to deliver all evidence to the defense attorneys as required by law. Ken Anderson was forced to resign as judge and give up his Texas law license. On November 8, 2013, Anderson plead no contest to the charges as part of a plea bargain. He was sentenced to 10 days in jail.


== Virginia ==
Culpeper County
Earl Washington, Jr. was sentenced to death for a 1982 rape and murder. He was pardoned in 2000.


== Wisconsin ==
Sherman Booth was convicted in January 1855 of violating the Fugitive Slave Act. The Wisconsin Supreme Court declared the Federal law unconstitutional and ordered Booth freed. In 1859, the U.S. Supreme Court overruled the Wisconsin court's decision, Ableman v. Booth, ordering Booth arrested and confined.
Milwaukee County
Lawrencia Bembenek was convicted of the 1981 murder of her husband's ex-wife. She won the right to a new trial in 1992. She pleaded no contest and her sentence was commuted to time served. She was trying to get her original conviction overturned and be exonerated. On November 20, 2010 she died at a hospice facility in Portland, Oregon from liver and kidney failure. Friends, family and supporters have been trying to get her a posthumous pardon.


== See also ==
List of exonerated death row inmates
List of wrongful convictions in the United States


== References ==