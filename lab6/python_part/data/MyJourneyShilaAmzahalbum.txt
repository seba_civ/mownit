My Journey (Chinese: 心旅) is the debut Chinese studio album and sixth album by Malaysian award-winning international singer-songwriter, Shila Amzah, released on April 30, 2016, by Shilala (HK) Limited. Shila began preparing for the album during the same year after she participated I Am a Singer (season 2), and during a significant amount of media scrutiny. Shila was 26 years old at the time of the album's release and she is the first Malay artist to release a Chinese studio album at Beijing, China.
Musically, the album is mandopop music styled, and lyrically it speaks of romantic relationships and breakups, a couple of Shila wrote from experiencing a relationship before. Lyrics also touch on Shila's personal struggles in life. In contrast to Shila's previous work, the production of My Journey consists of drum programming, synthesizers, pulsating bass, processed backing vocals, and guitars.


== Background ==
Shila released her fifth album, Shila Amzah, on December 10, 2013. This album, My Journey marked a change in Shila's musical style with the experimentation of mandopop. Before Shila finally release a Chinese studio album, Shila suffered struggles with recording contracts with Shanghai Media Group before establishing her own major recording label, Shilala (HK) Limited. The album was delayed almost 2 years. Before this, Shila stated the album will be released by March 2016, but it was delayed to April 2016 because some of her songs were mastered (in the United States of America) late.


== Album packaging and release ==
My Journey was released on April 30, 2016, with seven tracks alongside three bonus tracks. Shila began teasing an announcement in March 2016. On April 22, 2016, Shila unveiled the album cover on Instagram featuring the date of her press conference.


== Singles ==
On May 28, 2015, Shila released her debut Chinese single in Hong Kong, "See You or Never" through her very own major recording label, Shilala (HK) Limited. The song was written by Liao Yu and composed by Shila herself. This single had been performed live during her Shila Amzah: The Symbol of Love, International Press Conference. The song became an instant hit on Weibo Music when the single reached 1 million downloads in just two days.
On October 1, 2015, Shila performed her second Chinese single in Hong Kong, "Goodbye" during her Shila Amzah Meet & Greet Hong Kong. On October 6, 2015, Shila released the single through Shilala (HK) Limited on Weibo Music and iTunes.
"Manusia" was released to contemporary hit radio on May 6, 2016, as the third single from the album.


== Track listing ==


== Release history ==


== References ==