Scopula helcita is a moth of the family Geometridae. It is found in Cameroon, the Republic of Congo, the Democratic Republic of Congo, Equatorial Guinea, Ghana, Nigeria, Sierra Leone, South Africa and Uganda.
The larvae feed on Oxyanthus unilocularis and Blighia unijugata.


== Description ==
Upper Side. Antennae black and setaceous. Head, thorax, and abdomen black, the two last having a row of white spots running along the middle, and another on each side down to the anus. Wings fine dark red. Almost half the anterior next the tips being black, with five oval white spots thereon; three of which being the largest are joined together, the other two, being small and behind, are at a little distance apart. Posterior wings with a broad black border running from the upper to the abdominal corners, whereon are placed eight oval white spots at equal distances, two, being the outermost, very small and close together.
Under Side. Palpi yellow. Tongue spiral. Legs, breast, and sides black, spotted and streaked with white. Abdomen yellow. Wings coloured and marked as on the upper side. Margins of the wings entire. Wing-span nearly 3½ inches (87 mm).


== Subspecies ==
Scopula helcita helcita (Ghana)
Scopula helcita contractimargo (Prout, 1916) (Uganda)
Scopula helcita dissoluta (Gaede, 1917) (Cameroon)


== References ==