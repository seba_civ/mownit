Ann-Elen Skjelbreid (born September 13, 1971) is a former Norwegian biathlete from the Hålandsdal area in the Norwegian municipality of Fusa. She is the sister of Liv Grete Skjelbreid Poirée and is married with fellow Norwegian biathlete Egil Gjelland


== Junior career ==
She began with biathlon when she was 11 years old, and won the national ski competition for her age calls at 14 years of age. After the secondary school she moved to Trysil to go on the advanced ski school there. She was named to the Norwegian national team for the 1990 World Junior Championships, and was a part of the team which won the silver medal. The following year she won her first international gold medal, on the sprint in the Junior World Championships in Hungary. In addition she earned another silver medal as a member of the relay team.


== Senior career ==
In 1994 Ann-Elen was named to the Norwegian elite national team, and was a part of the Norwegian team who won the silver medal in the team competition. She has later participated in many world championships, and has represented Norway in three olympic games: Lillehammer 1994, Nagano 1998, and Salt Lake City 2002.


== Achievements ==
Junior World Championship: Gold medal on the sprint 1991, silver medal on the relay 1990 and 1991
Senior World Championship: Gold medal for teams in 1995 and 1997, silver medal for teams in 1994 and 1998, silver medal on the sprint 1996, silver medal on the relay 1997 and bronze medal on the relay
Olympics: Bronze medal on the relay 1998, silver medal on the relay 2002
World Cup: 5 second places in individual races
Ann-Elen Skjelbreid also has one Norwegian title in Cross-country skiing from 1995 when she was a part of the team which won gold medal on the 3 x 5 km relay for the club Trysilgutten IL.


== References ==
Profile