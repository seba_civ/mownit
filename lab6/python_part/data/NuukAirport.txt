Nuuk Airport (Greenlandic: Mittarfik Nuuk; Danish: Godthåb Lufthavn; (IATA: GOH, ICAO: BGGH)) is an airport in Nuuk, the capital of Greenland. The airport is a technical base and focus city for Air Greenland, the flag-carrier airline of Greenland, linking the capital with several towns in western and south-western part of the country, including the airline hub at Kangerlussuaq Airport. With connections to Iceland, Nuuk Airport is also one of 6 international airports in Greenland but serves only destinations within Greenland and Iceland. International connections are made with flights to either Keflavík International Airport in Iceland or Kangerlussuaq Airport.
In the early 1960s, water planes of the newly established Air Greenland landed in Nuuk Port. In 1965, the airline invested in developing a more robust fleet based on the large Sikorsky S-61 helicopter, which continued to serve the town for the next four decades, even after the construction of an airport in Nuuk capable of serving the short takeoff and landing de Havilland Canada Dash-7 aircraft, which dominated at the airport since the 1970s.
The airport was constructed to serve the largest town in Greenland, yet due to space constraints at the location in a mountainous area and problems with the weather, it is unable to service large airliners or flights reaching Denmark. A large expansion of the airport is not an acceptable option also due to the approach over the urbanized area of the outlying districts of Nuuk, although the issue continues to be a subject of internal debate in Greenland.


== History ==


=== Water plane era ===
In the early 1960s, after the establishment of Air Greenland on 7 November 1960 as Grønlandsfly, Nuuk was served exclusively by the PBY Catalina water planes, with the aircraft using the waterways of the Nuuk Port as a landing site. In 1962 a PBY Catalina crashed near the port, killing 15 people on board.


=== Helicopter era ===

The tragedy was one of the factors leading to the decision to invest in a helicopter fleet. The Sikorsky S-61N machines—still in use in 2010—proved to be a more reliable mode of transport for the city, providing exclusive service for the Nuuk city for more than a decade—from the purchase date in 1965 until the late 1970s.
Even in the later era of the fixed-wing, turboprop plane domination, the S-61N helicopters continued to link Nuuk with the smaller Paamiut town, until the airport was built there in 2007, replacing the old heliport.


=== Regional airport network ===
Nuuk Airport was built in 1979, when the then newly formed Home Rule government decided to create a network of the STOL-capable domestic airports. The airport in the largest city in Greenland was a priority for the government, followed by Kulusuk Airport in Kulusuk in south-eastern Greenland, and Ilulissat Airport in Ilulissat, the largest town in the Disko Bay region of western Greenland. This constituted the first such wave of network expansion.


=== Network expansion ===
It wasn't until the 1990s that the network experienced another spurt of large-scale growth, when the airports in the remaining larger towns were built: Sisimiut Airport in Sisimiut and Maniitsoq Airport in Maniitsoq in central-western Greenland, Aasiaat Airport in the Disko Bay region, Upernavik Airport in Upernavik in northwestern Greenland, and Qaarsut Airport, an airport in Qaarsut, a settlement in the Uummannaq Fjord region; the airport serving both the village and the larger town of Uummannaq, located on the rocky Uummannaq Island.


=== 2000-onwards ===

The first international flights from Nuuk Airport were to Iqaluit in Nunavut, Canada. This connection was closed 13 years later, and for years afterwards international flights to Greenland were limited to Kangerlussuaq Airport in central western Greenland, 319 km (198 mi) to the north of Nuuk, an airport inherited from the U.S. Air Force when the former Sondrestrom Air Base was handed over to the then Home Rule government on 30 September 1992.

With the airport being limited to serving small planes, the possibilities for international connections remain limited. Reopening of the connection to Iqaluit was considered by Air Greenland in late 2009, but was later postponed until at least 2011. In order to compete with Air Iceland, which operates services to Nuuk, Narsarsuaq, Ilulissat, and all airports on the eastern coast, Air Greenland announced opening of new connections with Iceland, linking Nuuk and Narsarsuaq with Keflavík International Airport, later restricting it to Nuuk. Air Greenland's seasonal flights to Iqaluit in Canada finally resumed in summer 2012, but ended before summer 2015. The best way of flying between Nuuk and US/Canada is over Reykjavik (Air Iceland/Icelandair).

The de Havilland Canada Dash 8-Q200 turboprops, acquired in Spring 2010, are the newest planes in the Air Greenland fleet, and are based at the airport. Nuuk Airport is also home to the Beechcraft King Air B200 "Amaalik", used for air ambulance flights and occasional charters.
The airport is also used for various charter flights, such as airlifts to the summit of the 1,210 metres (3,970 ft) Sermitsiaq Mountain, a landmark of Nuuk, located on Sermitsiaq Island north of the airport. The airport is also used for shuttle flights for events like the 2010 Inuit Circumpolar Council general assembly in Nuuk. and for the 2016 Arctic Winter Games. In general the short runway is a problem for large events in Nuuk. Charter and extra flights from outside Greenland can not land in Nuuk (or any other city of Greenland), so Air Greenland need to shuttle passengers between Kangerlussuaq and Nuuk, using planes much smaller than the charter planes, often requiring a hotel night in Kangerlussuaq.


=== Runway expansion ===
Nuuk airport has one asphalt runway (05/23) 950 m × 30 m (3,117 ft × 98 ft) 283 ft (86 m) above sea level. The airport terminal and apron are built on a levelled platform on an undulating slope under the Quassussuaq mountain, with the runway platform artificially elevated to compensate for the scarp immediately to the west. The runway platform bed is composed of broken rock and rubble, topped with gravel, and protected by a low, wooden fence.

The northern end of the runway is less than 700 m (2,300 ft) from the shore of Nuup Kangerlua fjord. An expansion of the runway in that direction would require relocation of the connecting road, which climbs under the runway scarp. An often-discussed extension of the runway in the other direction would have brought the endpoint close to Qinngorput, the newest district of Nuuk, rapidly expanding in the late 2000s.
There are plans of extending the runway to 1,199 m (3,934 ft). There are also suggestions to extend it to 1,700 m (5,600 ft) or 2,200 m (7,200 ft) (the longest possible) which would allow direct flights to Denmark, but only with small jet planes, needing several daily flights. The extension issue is a long-standing topic of ongoing controversy in Greenland. Rough weather in the region is cited as life-threatening to larger airplanes, given the additional difficulty of approach in a mountainous region. After 2010 there are no aircraft that can be purchased, which have more than 30 seats and can use short runways like Nuuk. When they retire or more aircraft are needed, a longer runway is necessary.
Another suggested alternative, is to build a new airport on one of the islands of Angisunnguaq or Qeqertarssuaq, locations having less turbulence, and allowing 2,800 m (9,200 ft) runway needed for the large planes used today to Denmark. These are located a few kilometres south of Nuuk and would need a bridge or tunnel connection. Such a project could cost somewhere around 2–3 billion DKK.


=== Greenland hub ===
Unlike Nuuk Airport, the airport in Kangerlussuaq can serve large airliners, and remains the airline hub of Air Greenland, the flag-carrier of Greenland. But Kangerlussuaq has very few inhabitants and is mainly a place to change plane. A move to Nuuk would eliminate the plane change for many travellers, but would require a runway expansion to around 2,000 m (6,600 ft).
The airline is opposed to relocation of its hub, citing the costs of such a move and consistently favourable weather conditions at Kangerlussuaq, located deep inland, about 30 km (19 mi) from the edge of the Greenland ice sheet (Greenlandic: Sermersuaq). The airline argues that the infrastructure at Kangerlussuaq is good, and visibility is not hampered by the coastal fogs, storms, heavy snowfall, and frequent turbulence in particular.
Another option mentioned is to have Keflavik, Iceland as the international hub, close Kangerlussuaq, and expand Nuuk Airport runway slightly, so small jet planes can use it. (Air Iceland has already built up a Greenland network from Reykjavik, Iceland)
These well-grounded arguments for preserving the status quo pose a problem for the Government of Greenland, which oversees the development of the airport network through Mittarfeqarfiit, the airport administration authority. More than a quarter of Greenland's population lives in Nuuk, with the majority of the important institutions in the country located in the city, and the need to change planes at Kangerlussuaq is costly and time-consuming for passengers. The final destination of most travellers to Greenland is Nuuk, and for them it does not help that Kangerlussuaq has better weather. They could equally well wait for better Nuuk weather in Copenhagen or Iceland.


== Geography ==

The airport is located 2 NM (3.7 km; 2.3 mi) northeast of Nuuk Centrum. The former suburbs of Nuuk, such as Nuussuaq, Quassussuup Tungaa, and Qinngorput, incorporated into the town in the last decade, have brought the city closer to the airport. As of 2010 the airport is within walking distance of the nearest continuously inhabited area, its runway approximately 700 m (2,300 ft) from the University of Greenland campus.


== Airlines and destinations ==


== Facilities ==
Nuuk Airport has a passenger terminal, and a cargo terminal of Air Greenland. It serves as the technical base for Air Greenland. The airport is equipped with the distance measuring equipment.


=== Terminal ===

There are three gates in the terminal, located in the same area as the check-in desks and the waiting hall, with unrestricted access. The luggage conveyor belt is installed in a separate section of the terminal. The airport is closed on Sundays.


=== Ground transport ===
Line 3 of Nuup Bussii connects the airport with Nuuk Centrum, passing through the Nuussuaq and Quassussuup Tungaa districts on the way. Buses depart from the airport every hour during rush hours Monday to Friday. Taxis operated by Nuna Taxa are also available. Limited-time parking for private cars is available outside the terminal.


== Accidents and incidents ==
In 1973 a Sikorsky S-61N helicopter operated by Grønlandsfly crashed in waters about 40 km south of Nuuk. Possible main rotor failure. 15 killed, all on board, including passengers.
On 7 June 2008, an Eurocopter AS350 of Air Greenland crashed on the runway at Nuuk Airport. There were no injuries, but the helicopter was damaged beyond repair.
On 4 March 2011, an Air Iceland's Dash 8 landing gear collapsed while landing on the runway. No injuries, but some heavy damage to the aircraft.


== References ==


== External links ==
Official web site