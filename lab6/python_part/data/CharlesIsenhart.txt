Charles William Isenhart (born March 30, 1959 in Dubuque, Iowa) is a Democratic politician, representing the 27th District in the Iowa House of Representatives since 2008.


== Education ==
Isenhart graduated from Wahlert High School and in 1981 obtained his degree in political science and mass communications from Loras College. In 1984 he received his master's degree in journalism from Marquette University.


== Career ==
Outside politics Isenhart owns a small business called Common Good Services. From 1990-2007, he was executive director of the Dubuque Area Labor-Management Council.


== Organizations ==
Isenhart has been or is currently a member of the following organizations:


=== Professional ===
Labor and Employment Relations Association
National Business Coalition on Health
Iowa 2010 Strategic Planning Council
Iowans for a Better Future Board of Directors
Governor’s 21st Century Workforce Council
Federal Mediation and Conciliation Service Customer Council


=== Community ===
Dubuque Housing Commission
Dubuque Community Development Commission
Dubuque County Mental Health/Developmental Disabilities Stakeholders Committee
Dubuque Soccer Club
American Youth Soccer Organization
America’s River Soccer Classic
Dubuque Housing Coalition
Healthy Dubuque 2000
Crescent Community Health Center Planning Committee
Downtown Neighborhood Council
St. Raphael Cathedral Parish
Habitat for Humanity board
Project Concern
Dubuque Food Pantry
Dubuque Soccer Alliance


== References ==


== External links ==
Representative Charles Isenhart official Iowa General Assembly site
Charles Isenhart State Representative official constituency site

Profile at Project Vote Smart