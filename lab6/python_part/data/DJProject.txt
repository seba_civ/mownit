DJ Project is a Romanian dance music group, initially made up of Gino Manzotti (Handke Giuseppe) and DJ Maxx (Ovidiu Florea) as producers and Elena Baltagan as the vocalist. The group was formed in 2000 in Timișoara, with their first album, Experience, released in 2001.
At the MTV Europe Music Awards 2006, DJ Project were awarded with "Best Romanian Act" prize.
In 2006 and 2007 the group has received the award for the best dance group at the MTV Romania Awards.
At the end of June 2007 they released their sixth album, Două Anotimpuri.
In early 2008, Robbins Entertainment picked up DJ Project for their first American release, Before I Sleep. The album is dubbed under the name "Elena", the same advertising name used in England. The album was released on iTunes on March 18, 2008 and was available in CD format on April 1, 2008.
In 2009, Elena Baltagan (currently known as Ellie White) left the project for a solo career. Gino and Maxx found a replacement in Giulia (ex-Candy).


== Discography ==


=== Albums ===
Experience (DJ Project album)|Experience (2001)
Spune-mi tot ce vrei (2002)
Lumea ta (2004)
Șoapte (2005)
Povestea mea (2006)
Două Anotimpuri (2007)
Dj Project & Friends - In the club (2009)
Best of (2011)
Best of 15 ani (2015)


=== Singles ===
Te chem (2002)
Spune-mi tot ce vrei (2002)
Lumea ta (2004)
Printre vise (2004)
Privirea ta (2005) #1 Romanian Top 100
Șoapte (2005) #2 Romanian Top 100
Încă o noapte (2006) #1 Romanian Top 100
Esti tot ce am (2006)
Before I Sleep (2007)
Două anotimpuri (2007) #2 Romanian Top 100
Lacrimi de inger (2007)
Prima noapte (feat Giulia) (2008)
Departe de noi (2008)
Hotel (2009)
Over and over again (feat Deepside Deejays) (2009)
Miracle Love (2009)
Nu (feat Giulia) (2009)
Regrete (feat Giulia) (2010)
Mi-e Dor De Noi (2011) #1 Romanian Top 100
Crazy In Love (feat Giulia) (2012)
Bun Rămas (feat Adela) (2012)


== Notes ==


== External links ==
(Romanian) DJ Project - official site
DJ Project - Doua Anotimpuri
DJ Project on Discogs