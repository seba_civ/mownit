Charles A. "Charlie" Wilson, Jr. (January 18, 1943 – April 14, 2013) was a U.S. Representative for Ohio's 6th congressional district. He was a member of the Democratic Party. He previously served in the Ohio State Senate and the Ohio House of Representatives.


== Early life, education, and business career ==
Wilson was born on January 18, 1943 in Martins Ferry or Dillonvale, Ohio. He was a graduate of Ohio University and the Cincinnati College of Mortuary Science. He was a small business owner and was president of Wilson Funeral Homes and Wilson Furniture Store.


== Ohio legislature ==
Wilson ran for Ohio's 99th House District in 1996. He defeated William L. Thomas in the Democratic primary 54%–46%. He won the general election and re-election in 1998 (50%), 2000 (68%), and 2002 (62%).
In 2004, he ran for the Ohio Senate when incumbent Democrat Greg DiDonato of the 30th District decided to retire after redistricting. In the Democratic primary, he defeated State Representative Jerry Krupinski 67%–33%. He won the general election with 67% of the vote. When he decided to retire to run for the U.S. House of Representatives, his son Jason Wilson replaced him.


== U.S. House of Representatives ==
Wilson had offices in: Canfield, Wellsville, Marietta, Bridgeport and Ironton, Ohio.


=== Elections ===
2006

In 2006, incumbent Democratic U.S. Congressman Ted Strickland decided to retire to run for Governor of Ohio. Wilson decided to run for the open seat in Ohio's 6th congressional district. Ohio state law requires that a candidate for Congress submit 50 valid signatures from constituents in his district to qualify for a place on the primary ballot. When Wilson's signatures were verified by the Columbiana County Board of Elections, only 46 of the 93 signatures submitted could be verified as legal residents of the 6th district. As such, for the Democratic primary on May 2, 2006, Charlie Wilson's name did not appear on the ballot. Wilson's campaign launched a massive effort, aided by the national party and organized labor, to 'write-in' Charlie Wilson's name in the primary. The campaign was successful, with Wilson winning 66% of the Democratic vote against two opponents. Wilson defeated Republican State Representative Chuck Blasdel 62%–38%.
2008

Wilson defeated Republican Richard Stobbs 62%–33%.
2010

Wilson was defeated by Republican U.S. Air Force veteran Bill Johnson 50%–45%. Following the 2010 campaign, Wilson was criticized for giving his staff large bonuses with taxpayer money as he was ending his term. Congressman Wilson's staff payroll increased by 49.7% from the previous payroll quarter, indicating that his staff did indeed receive hefty taxpayer funded bonuses.
2012

In November 2011, Wilson filed to run a rematch against Johnson in the newly redrawn 6th Congressional District. The race for Ohio's 6th Congressional District was listed as one of the most competitive in the country. It was one of the 24 toss-up races in the New York Times 2012 House Race Ratings. Some of the major issues in the race were jobs and the economy, health care, and energy. On the issue of coal, Wilson told NPR that "We don't need to fire Obama and we don't need to stop the war on coal," in an interview on September 28th, 2012. Wilson's spokesman said the candidate was being sarcastic, calling the comments "the farthest thing from the truth. Charlie has fought against both administrations, both the Bush administration and the Obama administration in the battle for coal." When asked about the Supreme Court ruling on President Obama's health care law, Wilson said he viewed the tax as a way of encouraging people to buy insurance and was quoted saying: "I look at it as a way of directing people into what would be a good decision for them,"
On November 6, 2012, Wilson was defeated by Johnson 53% to 47% in the rematch of their 2010 race in a slightly more Republican-leaning district, drawn after the 2010 census.


=== Tenure ===
Blue Dog Coalition
After entering office, Wilson joined the Blue Dog Coalition, a group of moderate and conservative congressional Democrats. Wilson was named Blue Dog of the Week on April 2, 2007. Wilson voted "Yes" on the Senate version of the health care bill.
Medicaid tamper-resistant prescription pads
Along with Rep. Marion Berry (D-AR) and Rep. Mike Ross (D-AR), Wilson introduced H.R.3090 in July 2007 to counteract a provision tucked away in the Iraq Spending Bill. The provision required that all Medicaid prescriptions be written on "tamper-resistant pads" effective October 1, 2007. The provision was put in place to combat Medicaid prescription fraud, but it may have unintended consequences. For example, the pads may not be widely available, nor is there a good definition of what they are. If pharmacists fill prescriptions that are not written on the special pads they risk not getting reimbursed through Medicaid. Wilson's bill would require that only Class II narcotics prescriptions, like OxyContin, be written on tamper-resistant pads. "This will prevent the most dangerous fraud without preventing those in need from receiving their everyday medications," Wilson said.
While the above action in pending action by the Subcommittee on Health, a six-month delay in the effective date was passed as part of H.R. 3668.


=== Committee assignments ===
Committee on Financial Services
Subcommittee on Capital Markets, Insurance, and Government-Sponsored Enterprises
Subcommittee on Financial Institutions and Consumer Credit

Committee on Science and Technology
Subcommittee on Investigations and Oversight
Subcommittee on Space and Aeronautics


=== Caucus memberships ===
Congressman Wilson was a member of the Rural Caucus, Sportsmen's Caucus, and the Steel Caucus. He assumed a leadership position in the Steel Caucus, serving as a member of the executive board.


== Electoral history ==


== Personal life ==
Wilson had four sons and nine grandchildren. His son, Jason, served in the Ohio Senate.
On February 21, 2013, Wilson suffered a brain aneurysm while vacationing in West Palm Beach, Florida, and was put into a medically induced coma. In early March, he entered a rehabilitation facility in Florida and had been "doing much better". On April 13, Wilson was admitted to a hospital in Boynton Beach, Florida after feeling ill. He died on April 14 of complications from the earlier stroke. He was 70.


== References ==


== External links ==

Biography at the Biographical Directory of the United States Congress
Profile at Project Vote Smart
Financial information (federal office) at the Federal Election Commission