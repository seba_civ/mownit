A number of different and competing administrations used the name Administrative Committee (Greek: Διοικητική Επιτροπή) throughout 1832, each claiming responsibility for a different part of Greece, all after the dissolution of the Administrative Committee of 1831 of Augustinos Kapodistrias, Theodoros Kolokotronis, and Ioannis Kolettis


== Early 1832 ==
Kolletis, after naming his party as the "Constitution Party", set up camp in Perachora near Loutraki, forming a temporary government with the backing of the Fourth National Assembly at Argos. Members of this government were:
Ioannis Kolettis
Andreas Zaimis
Georgios Kountouriotis
(elsewhere, Alexandros Mavrokordatos is also listed)
It also had two secretaries/ministers:
Territorial Secretary, responsible for the free Greek territories, foreign affairs, internal affairs, security, finance, religion, and education. The position was held by Dimitrios Christidis
Secretary of the Military, responsible for all military and naval matters
However the Fifth National Assembly at Nafplion did not acknowledge it.


== March 1832 ==
With the departure of Augustinos Kapodistrias in March 1832, a new Administrative Committee was formed with:
Theodoros Kolokotronis
Ioannis Kolettis
Andreas Zaimis
Andreas Metaxas
Boudouris
An additional Committee is also mentioned in the records with the following members:
Georgios Kountouriotis, President
Demetrios Ypsilantis
Andreas Zaimis
Botsaris
Spyridon Trikoupis


== April 1832 ==
In April 1832, on the front page of the National Gazette, the official announcement of a new Administrative Committee is made with the following members:
Georgios Kountouriotis
Demetrios Ypsilantis
Andreas Zaimis
Ioannis Kolettis
Andreas Metaxas
Dimitris Plapoutas
Dimitrios Christidis, as Territorial Secretary


== May 1832 ==
In May 1832, in a conference in London a treaty was signed forming the Kingdom of Greece with Otto as its King.
The Fifth National Assembly confirmed the selection of Otto in July 1832.


== Aftermath ==
In January 1833, King Otto arrives in Greece and the executive power is transferred to the Regency, which kept the existing government with its members being:
Spyridon Trikoupis, President, Minister for Foreign Affairs and Merchant Marine
Alexandros Mavrokordatos, Minister of Finance
Christodoulos Klonaris, Minister of Justice
Iakovos Rizos Neroulos, Minister of Interior
Konstantinos Zografos, Minister of the Military
Dimitrios Voulgaris, Minister for the Navy
The above made up the first government under King Otto.


== Legacy ==
One of the most important accomplishments of the Administrative Committee was the commissioning of Stamatios Kleanthis and Eduard Schaubert to design the city plan of Athens.


== References ==