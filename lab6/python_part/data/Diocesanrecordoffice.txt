Originally within the United Kingdom the title of Diocesan Record Office would frequently have referred to a church-owned diocesan registry or chancery. This would have been where the episcopal registers, administrative papers and title deeds were preserved under the general superintendence of the diocesan chancellor.
In modern usage it generally refers to an approved repository, frequently operated by a local authority, which has been specially designated by a Church of England bishop for the collection and preservation of historic archives, both from the diocese and also from local ecclesiastical parishes.


== Historical background ==
The Parochial Registers and Records Measure 1929 permitted bishops to designate such places as approved repositories for parish records. A new Parochial Registers and Records Measure 1978 made it obligatory to designate at least one diocesan record office in each diocese of the Church of England.
Usually the diocesan record office will now be within a city record office or an ordinary county record office. However there are some exceptions: the Canterbury Cathedral Library and the Borthwick Institute for Archives at the University of York are similarly designated.
The Parochial Records and Records Measure relates solely to the Church of England. Other churches within the Anglican communion, in Scotland, Wales and elsewhere, normally operate in accordance with their own specialist legislation – as do many other Christian denominations. The status of various Welsh county record offices as diocesan record offices was the result of an agreement with the Representative Body of the Church in Wales, made in 1976, which gave Welsh parishes the option of claiming facsimile copies of their deposited registers. Prior to that date the only officially recognised Welsh repository had been the National Library of Wales, first designated in 1944.


== References ==


== External links ==
[1] Parochial Registers and Records Measure 1978: full text on legislation.gov.uk website.
[2] National Archives website (archival legislation): summary of Parochial Registers and Records Measures.