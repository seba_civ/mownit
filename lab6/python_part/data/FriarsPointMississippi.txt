Friars Point is a town in Coahoma County, Mississippi, United States. The population was 1,200 at the 2010 census, down from 1,480 at the 2000 census.
Situated on the Mississippi River, Friars Point was once a busy port town, and remains the only place in Coahoma County with public access to the river's shore.


== History ==

Friars Point is one of two hypothesized locations where Spanish explorer Hernando de Soto may have crossed the Mississippi River (the other is Commerce, Mississippi).
The town was founded in 1836 and originally called "Farrar's Point". When the town incorporated in 1852, its name was changed to "Friar's Point" to honor Robert Friar, an early settler, legislator, and businessman who sold fuel to passing steamboats. In 1850, the county seat was moved from the nearby town of Delta to Friars Point.
Strategically situated at a bend in the Mississippi River, Friars Point flourished before the Civil War as the largest shipping center for cotton south of Memphis.
During the Civil War, Union troops occupied and burned portions of the town. The Robinson-Slack-Marinelli House (now the Minie Ball House), which still stands and bears the mark of shelling from gunboats, was used as a headquarters by Union General Napoleon Bonaparte Buford. General William Tecumseh Sherman and Admiral David Dixon Porter used Friars Point as a rendezvous for 45 transport ships in December 1862, prior to attacking Vicksburg. Friars Point was also home to Confederate Brigadier General James L. Alcorn, whose grave and former plantation, Eagles Nest, are located a short distance east of the town. Alcorn turned from Whig to Republican after the war, and went on to become governor with the support of the large number of carpetbaggers who had settled in Friars Point.
The famous gunman and train robber Jesse James came to Friars Point one evening during the 1880s. He visited with the Methodist minister, then played poker and had some drinks at the local saloon. The next morning he was gone.
The Yazoo and Mississippi Valley Railroad was completed to Friars Point in 1887.
In 1888, the county jail at Friars Point was completely destroyed by fire, killing five prisoners.
Charles Lindbergh ran out of gas while flying his plane over Friars Point in 1924, and landed at a place he later called "The Haunted House".
As nearby Clarksdale grew in population and influence, it challenged Friars Point's hold on the county government. In 1892, Coahoma County was divided into two jurisdictions, one going to Friars Point and the other to Clarksdale. In 1930, the county seat was given exclusively to Clarksdale. Historian Lawrence J. Nelson wrote that by that point, "Friars Point had receded into a sleepy river community."
In the 1930s there was ferry service between Friars Point and Helena, Arkansas. The cost was one dollar for a car and driver and 25 cents per passenger.
On April 26, 2011, a tornado—part of the April 25–28, 2011 tornado outbreak—hit Friars Point. The tornado was classified as an EF-0, with estimated wind speeds of 75 mph (121 km/h).
Time Magazine wrote in 2013:

Once a thriving port town and the county seat, economic decline has left Friars Point with a lone elementary school, a few churches, a city hall, a post office, a small general store, a museum that opens only sporadically, a nightclub called Show T Boat where a man was shot to death in 2011, and a bank. The town no longer has a doctor or health clinic, a drug store, a sit-down restaurant, a recreational center, a library, or any businesses to speak of. Kids travel 15 miles to Clarksdale for junior and senior high school.


=== Delta blues ===
Muddy Waters said the only time he saw Robert Johnson play was on the front porch of Hirsberg's Drugstore in Friars Point. A crowd had gathered around Johnson, who was playing ferociously. "I stopped and peeked over," said Waters, "and then I left because he was a dangerous man." In a 1937 recording, Johnson sang, "Just come on back to Friars Point, mama, and barrelhouse all night long." In Johnson's "Traveling Riverside Blues" he sang, "I got womens in Vicksburg, clean on into Tennessee, but my Friar's Point rider, now, hops all over me."
The Mississippi Blues Commission placed a Blues Trail marker in Friars Point in recognition of musician Robert Nighthawk, who at various times called Friars Point home. In 1940, Nighthawk recorded "Friars Point Blues", singing of "going back to Friars Point, down in sweet old Dixie Land." When dedicating the marker, Governor Haley Barbour proclaimed:

This talented Mississippian made a huge contribution to development of that unique genre of music, the Mississippi blues. I am pleased Nighthawk's imprint on the blues scene, which is still heard through the tunes of modern-day blues artists, will be recognized with his inclusion on the Mississippi Blues Trail.

"Friar's Point" is a song on blues musician Susan Tedeschi's 1998 album Just Won't Burn.


== Geography ==
Friars Point is located in northern Coahoma County, close to the Mississippi River, though the river's channel has moved north over time, away from the town. The town is 9 miles (14 km) west of highways 49 and 61, and 13 miles (21 km) north of Clarksdale, the county seat. According to the United States Census Bureau, the town has a total area of 1.1 square miles (2.9 km2), of which 0.02 square miles (0.06 km2), or 2.10%, is water.


== Demographics ==
As of the census of 2000, there were 1,480 people, 476 households, and 348 families residing in the town. The population density was 1,324.0 people per square mile (510.2/km²). There were 508 housing units at an average density of 454.4 per square mile (175.1/km²). The racial makeup of the town was 93.85% African American, 5.95% White, 0.07% Pacific Islander, and 0.14% from two or more races. Hispanic or Latino of any race were 1.28% of the population.
There were 476 households out of which 42.0% had children under the age of 18 living with them, 27.3% were married couples living together, 39.9% had a female householder with no husband present, and 26.7% were non-families. 24.6% of all households were made up of individuals and 12.0% had someone living alone who was 65 years of age or older. The average household size was 3.11 and the average family size was 3.68.
In the town the population was spread out with 38.7% under the age of 18, 11.9% from 18 to 24, 24.9% from 25 to 44, 15.3% from 45 to 64, and 9.1% who were 65 years of age or older. The median age was 25 years. For every 100 females there were 79.6 males. For every 100 females age 18 and over, there were 68.9 males.
The median income for a household in the town was $17,750, and the median income for a family was $19,500. Males had a median income of $22,386 versus $16,898 for females. The per capita income for the town was $10,769. About 38.1% of families and 44.0% of the population were below the poverty line, including 53.4% of those under age 18 and 27.1% of those age 65 or over.


== Education ==
The town of Friars Point is served by the Coahoma County School District.


== In popular culture ==

Friars Point has been written about by both William Faulkner and Tennessee Williams. At various times, both writers vacationed at Uncle Henry's Inn on nearby Moon Lake.


== Notable people ==
Lewis E. Sawyer practiced law in Friars Point and became mayor; he was also a congressman from Arkansas.
Conway Twitty, country music singer who once held the record for having the most number-one-hit singles


== References ==


== External links ==
North Delta Museum
Friars Point Elementary School