Motherwell will compete in the Scottish Premier League, Scottish Cup, Scottish League Cup and UEFA Europa League during the 2010–11 season. This is the third consecutive season in which Motherwell have been involved in European competition, the first time this has happened in the club's 125-year history.


== Important Events ==
21 June 2010 – Draw for the UEFA Europa League second qualifying round. Motherwell draw Icelandic side, Breiðablik.
16 July 2010 – Draw for the UEFA Europa League third qualifying round. Motherwell draw Norwegian side, Aalesund.
21 June 2010 – Draw for the UEFA Europa League play-off round. Motherwell draw Danish side, Odense.
11 August 2010 – The club announce a new year-long sponsorship deal with Scottish telecommunications firm, Commsworld.
31 August 2010 – Draw for the Scottish League Cup third round. Motherwell draw Brechin City (away).
23 September 2010 – Draw for the Scottish League Cup quarter finals. Motherwell draw Dundee United (home).
5 October 2010 – Motherwell appoint new Vice-Chairman, Derek Weir, and report a modest profit for the sixth time in seven years.
28 October 2010 – Draw for the Scottish League Cup semi finals. Motherwell draw Rangers (tie to be played at Hampden).
22 November 2010 – Draw for the Scottish Cup Fourth round. Motherwell draw Dundee (away).
9 December 2010 – Manager Craig Brown resigns.
30 December 2010 – Motherwell appoint Stuart McCall as their new manager on a two-and-a-half year deal.
11 January 2011 – Draw for the Scottish Cup Fifth Round. Motherwell draw Stranraer (away).
19 January 2011 – Kenny Black is confirmed as the new assistant manager.
6 February 2011 – Draw for the Scottish Cup quarter finals. Motherwell draw Dundee United (away).
23 February 2011 – Chairman John Boyle announces that he will stand down as Motherwell chairman, and give up his shareholding for free. The club also consider switching to a wider form of ownership.
14 March 2011 – Draw for the Scottish Cup semi finals. Motherwell draw St Johnstone with the tie to be played at Hampden Park.
16 April 2011 – Motherwell defeat St Johnstone 3–0 at Hampden Park to progress to the 2011 Scottish Cup Final where they are to play Celtic, again at Hampden Park.
15 May 2011 – Motherwell finish their league campaign with a 4–0 defeat by Scottish Cup final opponents, Celtic, at Parkhead in their 52nd and penultimate competitive fixture of the season.
21 May 2011 – Motherwell finish their campaign with a 3–0 Scottish Cup final defeat against Celtic at Hampden Park.


== Transfers ==
For a list of Scottish football transfers in 2010–11, see transfers in season 2010–11


=== Summer Transfer Window (1 July – 1 September 2010) ===
In Permanent
Loans in
Loans out
Out Permanent
Released


=== Winter Transfer Window (1 January – 1 February 2011) ===
In Permanent
Out Permanent
Loans in
Loans out
Released


== Motherwell F.C. Season 2010–11 First-team Squad ==
As of 28 January 2011 Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Motherwell F.C. Season 2010–11 Statistics ==


=== Appearances ===
As of 21 May 2011


=== 2010–11 Motherwell Top scorers ===
Last updated on 21 May 2011


=== 2010–11 Motherwell Disciplinary Record ===
Last updated 26 July 2011
Last updated: 26 July 2011


== Results & fixtures ==
      Win       Draw       Loss


=== Friendlies ===


=== Scottish Premier League ===


=== UEFA Europa League ===


=== Scottish Cup ===


=== Scottish League Cup ===


== Competitions ==


=== Overall ===
Source: Competitions


=== SPL ===


==== Classification ====
Source: BBC Sport
Rules for classification: 1) points; 2) goal difference; 3) number of goals scored(C) = Champion; (R) = Relegated; (P) = Promoted; (E) = Eliminated; (O) = Play-off winner; (A) = Advances to a further round.
Only applicable when the season is not finished:(Q) = Qualified to the phase of tournament indicated; (TQ) = Qualified to tournament, but not yet to the particular phase indicated; (RQ) = Qualified to the relegation tournament indicated; (DQ) = Disqualified from tournament.


==== Results summary ====
Source: BBC Sport


==== Results by round ====
Source: BBC SportGround: A = Away; H = Home. Result: D = Draw; L = Loss; W = Win; P = Postponed.


==== Results by opponent ====
Source: 2010–11 Scottish Premier League article


== See also ==
List of Motherwell F.C. seasons


== Notes and references ==


== External links ==
Motherwell F.C. Website
BBC My Club Page
Motherwell F.C. Newsnow