A Mate-Demate Device (MDD) is a specialized crane designed to lift a Space Shuttle orbiter onto and off of the back of a Shuttle Carrier Aircraft. Four Mate-Demate Devices were built.


== Armstrong Flight Research Center MDD ==
The first MDD was located at NASA's Armstrong Flight Research Center on Edwards Air Force Base, California 34.958559°N 117.883818°W﻿ / 34.958559; -117.883818. Construction of the MDD was completed in late 1976. It was first used during mate-demate operations with the prototype Space Shuttle Enterprise during the five Approach and Landing Tests (ALT) in 1977. It was then used for post-landing and Shuttle Carrier Aircraft mating operations at NASA Armstrong following shuttle orbital missions that landed at Edwards. Fifty-four of the 135 shuttle missions landed at Edwards between STS-1 in 1981 through STS-128 in 2009. All but one (STS-3) of these flights landed at Edwards instead. This MDD was used to hoist a Space Shuttle orbiter onto a Shuttle Carrier Aircraft for the flight back to Florida. The MDD was dismantled in 2014 following the retirement of Space Shuttle.


== Kennedy Space Center MDD ==

The second MDD was located at the Kennedy Space Center in Florida 28.600666°N 80.679262°W﻿ / 28.600666; -80.679262. Its primary use was unloading the orbiter after its cross-country flight from Edwards.


== Orbiter Lifting Fixture ==

The third MDD was built for the planned Space Shuttle operations at Vandenberg Air Force Base in California. Upon cancellation of Vandenberg's shuttle program, the Orbiter Lifting Fixture (OLF) was disassembled and moved to USAF Plant 42 in Palmdale California where it was used during periodic orbiter refurbishments. This structure was demolished in 2008.


== Mobile MDD ==

On the rare occasions when an orbiter needed to be loaded or unloaded at a location where no MDD was available, a pair of cranes was used instead. Prior to its use in 2012 to exchange Discovery and Enterprise, the mobile MDD had been stored for over twenty years.


== MDDs in USSR ==
For the Energia-Buran programme there was built similar structures in USSR, named PKU-50, PU-100 and PUA-100.
For the time of the Buran first flight they were operated at LII, Baikonur Jubilee airfield and Bezymyanka airport.
They were capable of loading Buran (0GT cargo) or Energia components (1GT, 2GT and 3GT cargoes) on top of Myasishchev VM-T and Antonov An-225 aircraft.


== References ==