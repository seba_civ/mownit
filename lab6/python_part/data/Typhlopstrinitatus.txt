Typhlops trinitatus, commonly known as the Trinidad worm snake or Trinidad burrowing snake, is a harmless blind snake species endemic to Trinidad and Tobago. No subspecies are currently recognized.


== Description ==
T. trinitatus grows to a maximum total length (including tail) of 24 cm (9.4 in).


== Geographic range ==
Found mostly on the island of Tobago, T. trinitatus is known from widely scattered locations, and from a single location on the island of Trinidad, which happens to be the type locality. This is described as "Trinidad [County of St. George], ... Arima Road, 3 miles above [north of] Simla [Research Station]".


== See also ==
List of typhlopid species and subspecies
Typhlops by common name
Typhlops by taxonomic synonyms
Typhlopidae by common name
Typhlopidae by taxonomic synonyms


== References ==


== Further reading ==
Hedges SB, Marion AB, Lipp KM, Marin J, Vidal N. 2014. A taxonomic framework for typhlopid snakes from the Caribbean and other regions (Reptilia, Squamata). Caribbean Herpetology (49): 1-61. (Amerotyphlops trinitatus, new combination).
Richmond ND. 1965. A new species of blind snake, Typhlops, from Trinidad. Proc. Biol. Soc. Washington 78: 121-124. (Typhlops trinitatus, new species).


== External links ==
Amerotyphlops trinitatus at the Reptarium.cz Reptile Database. Accessed 29 January 2016.