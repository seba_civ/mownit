Nectocaris pteryx is a species of possible cephalopod affinity, known from the "early Cambrian" (Series 2) Emu Bay Shale and Chengjiang biota, and the "middle Cambrian" (Series 3, Stage 5) Burgess Shale.
Nectocaris was a free-swimming, predatory or scavenging organism. This lifestyle is reflected in its binomial name: Nectocaris means "swimming shrimp" (from the Ancient Greek νηκτόν, nekton, meaning "swimmer" and καρίς, karis, "shrimp"; πτέρυξ, pteryx, means "wing"). Two morphs are known: a small morph, about an inch long, and a large morph, anatomically identical but around four times longer.


== Anatomy ==

Nectocaris had a flattened, kite-shaped body with a fleshy fin running along the length of each side. The small head had two stalked eyes, a single pair of tentacles, and a flexible funnel opening out to the underside of the body. The funnel often gets wider away from the head. Internally, a chamber runs along the body axis, containing a pair of gills; the gills comprise blades emerging from a zig-zag axis. Muscle blocks surrounded the axial cavity, and are now preserved as dark blocks in the lateral body. The fins also show dark blocks, with fine striations superimposed over them. These striations often stand in high relief above the rock surface itself.


== Diversity ==
Although Nectocaris is known from Canada, China and Australia, in rocks spanning some 20 million years, there does not seem to be much diversity; size excepted, all specimens are anatomically very similar. Historically, three genera have been erected for nectocaridid taxa from different localities, but these 'species' – Petalilium latus and Vetustovermis planus – likely belong to the same genus or even the same species as N. pteryx. Within N. pteryx, there seem to be two discrete morphs, one large (~10 cm in length), one small (~3 cm long). These perhaps represent separate male and female forms.


== Ecology ==
The unusual shape of the nectocaridid funnel has led to its interpretation as an eversible proboscis, but this is difficult to reconcile with the fossil evidence. Instead, its fluid dynamics seem optimal for a role in jet propulsion, where it would have maintained an efficient slow flow of water over the large internal gills. The eyes of Nectocaris would have had a similar visual acuity to modern Nautilus (if they lacked a lens) or squid (if they did not).


== Affinity ==
The affinity of Nectocaris has been a controversial subject, and some scientists feel that it is still uncertain. The balance of evidence seems to suggest a relationship with the cephalopods, whether in an ancestral or derived position. The most pleasing interpretation – which is not without problems – is to interpret some of the features of Nectocaris as shared with the earliest cephalopods. Features that seem to be shared with the modern coleoids (which evolved much later) can then be attributed to convergence – but perhaps using the same underlying genetic machinery as modern coleoids do today.


== History of study ==

Nectocaris has a long and convoluted history of study. Charles Doolittle Walcott, the discoverer of the Burgess Shale, had photographed the one specimen he had collected in the 1910s, but never had time to investigate it further. As such, it was not until 1976 that Nectocaris was formally described, by Simon Conway Morris.
Because the genus was originally known from a single, incomplete specimen and with no counterpart, Conway Morris was unable to deduce its affinity. It had some features which were reminiscent of arthropods, but these could well have been convergently derived. Its fins were very unlike those of arthropods.
Working from photographs, the Italian palaeontologist Alberto Simonetta believed he could classify Nectocaris within the chordates. He focussed mainly on the tail and fin morphology, interpreting Conway Morris's 'gut' as a notochord – a distinctive chordate feature.
The classification of Nectocaris was revisited in 2010, when Martin Smith and Jean-Bernard Caron described 91 additional specimens, many of them better preserved than the type. These allowed them to reinterpret Nectocaris as a primitive cephalopod, with two tentacles instead of the 8 or 10 of modern cephalopods. The structure previous researchers had identified as an oval carapace or shield behind the eyes was suggested to be a soft funnel, similar to the ones used for propulsion by modern cephalopods. The interpretation would push back the origin of cephalopods by at least 30 million years, much closer to the first appearance of complex animals, in the Cambrian explosion, and implied that – against the widespread expectation – cephalopods evolved from non-mineralized ancestors.
A later analysis claimed to undermine the cephalopod interpretation, stating that it did not square with the established theory of cephalopod evolution. According to these authors, Nectocaris is best treated as a member incertae sedis of the arthropod group Dinocaridida (which includes the anomalocaridids), but they stopped short of formally changing the classification. However, it is straightforward to demonstrate that an anomalocaridid affinity is not supported. Other studies have yet to put forward a more plausible affinity, and alternatives to the cephalopod affinity are fraught with difficulties. Whether Nectocaris represents a derived or basal cephalopod, and whether it belongs in the stem or crown group, it is more parsimonious to interpret its distinctive features as homologues of their equivalent in cephalopods – even if the absence of a shell in Nectocaris is an independent feature.


== References ==


== Further reading ==

3D animations and a more detailed consideration are available at "Nectocaris pteryx". Burgess Shale Fossil Gallery. Virtual Museum of Canada. 2011. 
The taxonomy and history of Nectocaris are discussed by Brian Switek on his blog entry: Brian Switek (July 5, 2011). "Nectocaris: What the heck is this thing?". Laelaps. 
BBC News coverage of the Smith & Caron's original redescription is presented here: Katia Moskvitch (May 27, 2010). "Mystery fossil is ancestor of squid". BBC News. 
A blog article supporting Mazurek & Zatoń's view is presented here: Christopher Taylor (January 14, 2011). "So nice when people agree with you". Catalogue of Organisms. 
Conway Morris, S. (1997). The Crucible of Creation: the Burgess Shale and the rise of animals. Oxford University Press. ISBN 0-19-286202-2. 


== External links ==
 Media related to Nectocaris at Wikimedia Commons