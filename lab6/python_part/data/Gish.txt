Gish is the debut album by American alternative rock band The Smashing Pumpkins, released in May 1991 through Caroline Records. Frontman Billy Corgan described Gish as a "very spiritual album".
Despite initially peaking at only number 195 on the Billboard 200 upon its release, Gish received positive reviews from critics, and was eventually certified platinum (one million copies shipped) by the RIAA.


== Recording ==
Gish was recorded from December 1990 to March 1991 in Butch Vig's Smart Studios in Madison, Wisconsin with a budget of $20,000. Vig and Billy Corgan worked together as co-producers. The longer recording period and larger budget were unprecedented for Vig, who later remembered,

(Corgan) wanted to make everything sound amazing and see how far he could take it; really spend time on the production and the performances. For me that was a godsend because I was used to doing records for all the indie labels and we only had budgets for three or four days. Having that luxury to spend hours on a guitar tone or tuning the drums or working on harmonies and textural things . . . I was over the moon to think I had found a comrade-in-arms who wanted to push me, and who really wanted me to push him.

The inclusion of a massive production style reminiscent of ELO and Queen was unusual for an independent band at the time. Whereas many albums at the time used drum sampling and processing, Gish used unprocessed drum recordings, and an exacting, unique guitar sound. Billy Corgan also performed nearly all of the guitar and bass parts on the record, which was confirmed by Vig in a later interview.
The album's sessions, lasting 30 working days, were brisk by Pumpkins' standards, largely because of the group's inexperience. The recording sessions put an intense strain on the band, with bassist D'arcy Wretzky later commenting that she did not know how the band survived it, and Corgan explaining he suffered a nervous breakdown.


== The album ==
Regarding the album's thematic content, Corgan would later say,

The album is about pain and spiritual ascension. People ask if it's a political album. It's not a political album, it's a personal album. In a weird kind of way, Gish is almost like an instrumental album—it just happens to have singing on it, but the music overpowers the band in a lot of places. I was trying to say a lot of things I couldn't really say in kind of intangible, unspeakable ways, so I was capable of doing that with the music, but I don't think I was capable of doing it with words.

"I Am One", "Rhinoceros", "Daydream", and "Bury Me" were previously recorded as demos by the band in 1989. All four songs were re-recorded for Gish.
The following songs were written and recorded for Gish but did not make the final cut:
"Blue" (released on Lull and Pisces Iscariot. A demo version appears on the 2011 Deluxe reissue of Gish)
"Obscured" (originally a B-side on "Today", re-released on Pisces Iscariot. This version could be a re-recorded version according to Billy Corgan's notes about this song on Pisces Iscariot)
"Slunk" (released on Lull)
"Why Am I So Tired" (released on Earphoria and on the 2012 Deluxe reissue of Pisces Iscariot)
"Jesus Loves His Babies" (A rough mix was released on the 2012 Deluxe reissue of Pisces Iscariot)
"La Dolly Vita" (originally the B-side to "Tristessa", re-released on the 2012 Deluxe version of Pisces Iscariot, and a slightly different mixed version on the Deluxe reissue of Gish.)
"Pulseczar" (released on Earphoria and released on the 2012 Deluxe reissue of Gish.)
"Smiley" (released on Peel Sessions and a demo version appears on the 2011 Deluxe reissue of Gish.)
"Crawl" (released on the 2012 Deluxe reissue of Pisces Iscariot.)
"Purr Snickety" (released as a "B-Sides Session Outtake" from the Gish sessions on the 2012 Deluxe reissue of Pisces Iscariot.)


=== Title ===
The album was named after silent film icon Lillian Gish. In an interview, Corgan said, "My grandmother used to tell me that one of the biggest things that ever happened was when Lillian Gish rode through town on a train, my grandmother lived in the middle of nowhere, so that was a big deal..." Later, Corgan joked that the album was originally going to be called "Fish", but was changed to "Gish" to avoid comparisons to jam band Phish.


== Reception ==


=== Commercial performance ===
Gish spent one week on the Billboard 200, peaking at number 195 (later re-peaking at number 146 upon its 2011 re-release); however, the album reached number one on the College Music Journal chart, which tracks airplay and popularity on college radio stations. It also had a six-week run on the New Zealand Albums Chart, peaking at number 40. Despite an inauspicious start, the album sold 100,000 copies in less than a year, far exceeding the expectations of indie label Caroline Records, a subsidiary of Virgin Records. The album was certified gold on March 14, 1994. Until the release of The Offspring album Smash in 1994, Gish was the highest-selling independently released album of all time. Gish would later be reissued under the Virgin label, and was certified platinum on February 5, 1999.


=== Critical response ===
Gish was met with largely enthusiastic reviews. On the month of its release, Chris Heim of the Chicago Tribune credited producer Butch Vig for helping the band achieve a "clearly defined" and "big, bold, punchy" sound for the album. Heim also indicated that the varied styles of the album would be a good addition to the alternative music culture of Chicago at the time—a culture that was sometimes perceived as inaccessible for new bands. Jon Pareles of The New York Times picked up on the eclectic mix of musical style on Gish as well, complementing its "pummeling hard rock", "gentle interludes", and "psychedelic crescendos". In an end-of-year recap of 1991 releases, Heim noted that the album constituted a "smashing local success story" for the Chicago area. Greg Kot, also of the Tribune, called Gish "perhaps the most audacious and accomplished" of all 1991 albums released by local bands; in an article later that year, Kot listed the album among the best of 1991. Rolling Stone called it "awe-inspiring" with "meticulously calculated chaos" and a "swirling energy".
Many substantive reviews of Gish emerged only with the 1993 release of Siamese Dream, when mainstream critics took their first look into the back-catalog of a band whose popularity was exploding. Derek Weiler of the Toronto Star noted that songs on Gish contained "either galloping riffs or trippy feedback hazes" and that the latter were especially effective and entertaining.
In 1992, Gish and the Smashing Pumpkins earned recognition at the Chicago Musician Awards, for which local music publication Illinois Entertainer polled readers and Chicago music industry figures such as critics, writers, and club owners. In separate polls, readers and industry figures chose Gish as the "best local album". Jimmy Chamberlin and James Iha won individual honors for their performances on the album, and the band as a whole earned the "best hometown national act" award.


== Release history ==
The first mastering of Gish on CD was from Digital Audio Tape and appeared on Caroline Records, a subsidiary of Virgin Records. In 1994, after the success of follow-up Siamese Dream, the album was given a slight remaster and redesign and was reissued on the Virgin label. Both editions credit Howie Weinberg as mastering engineer. In 2008, The Smashing Pumpkins announced a 17th anniversary box set re-release of the album that would include older bonus material, but this set experienced delays. After finally negotiating the rights, Gish was re-issued in November 2011, being remastered on CD and Vinyl with extra tracks and packaging.


== Track listing ==
All songs written and composed by Billy Corgan, except where noted. 


== Personnel ==
Those involved in the making of Gish are:
The Smashing Pumpkins
Jimmy Chamberlin – drums
Billy Corgan – vocals, guitar, bass, production
James Iha – guitar, vocals
D'arcy Wretzky – bass, vocals, lead vocals on "Daydream", layouts
Additional musicians
Mary Gaines – cello on "Daydream"
Chris Wagner – violin and viola on "Daydream"
Production
Bob Knapp – photography
Michael Lavine – photography
Butch Vig – production, engineering
Doug "Mr. Colson" Olson – engineering
Howie Weinberg – mastering (1991 and 1994 releases)
Bob Ludwig – mastering (2011 remaster)


== Chart positions ==


== References ==


== External links ==
Gish at AllMusic
Gish at MusicBrainz (list of releases)