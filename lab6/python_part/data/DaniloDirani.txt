Danilo Dirani is a racing driver from Brazil. He was born on January 10, 1983 in São Paulo. In his karting career (between the years of 1992 and 2002), Danilo won 29 different championships in South America.
In 2002, Danilo moved to Formula 3 and Formula Three Sudamericana, and finished runner-up to Nelson Piquet Jr. Dirani won just 2 races, compared to Piquet's thirteen. But in 2003, Danilo won 14 out of the 18 races to win the championship.
In 2004, Dirani crossed the Atlantic Ocean and came to British soil for the British version of Formula 3, and finished 5th in the championship, with 2 wins, both at Croft and 146 points. He finished behind Piquet Jr, Adam Carroll, James Rossiter and Clivio Piccione.
Danilo moved to the P1 Motorsport team for the 2005 British F3 International Series, and again had 2 wins in the opening two races at Donington Park, and he also got his first pole position in British F3 at the same race meeting. He scored 14 more points than 2004, but finished one place lower in the standings in 6th, behind Álvaro Parente, Charlie Kimball, Mike Conway, Marko Asmer and Dan Clarke.
Dirani was added to the Honda Racing F1 development roster for the 2005 season, together with other drivers from the British F3, as a form of marketing for the championship.
In 2006, Danilo crossed the Atlantic Ocean back to America. He competed in the Champ Car Atlantic Series, and finished in 7th position in the championship.
Dirani is currently racing for DF Motorsports in Brazilian Fórmula Truck. He is also competing in karting races in Brazil and USA since 2007.


== Racing record ==


=== Career summary ===


== External links ==
Danilo Dirani Official Site (Portuguese)
Danilo Dirani on Twitter