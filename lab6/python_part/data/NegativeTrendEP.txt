Negative Trend is the debut EP and historically the only stand-alone official release by the American punk rock band Negative Trend.


== Production ==
Produced by Debbie Dub and the band, Negative Trend was recorded and mixed in June 1978, in a process which took only two days.


== Release ==
Negative Trend was originally released in September 1978 on the label Heavy Manners in 7-inch vinyl disc format. Only 1,000 copies of this edition were pressed.


== Critical reception ==
Matt Whalley of AllMusic retrospectively lauded the EP as being some of America's finest punk music and credited it as being "a surprisingly powerful and gifted piece of American punk history". He gave it 4 out of 5 stars, concluding that Negative Trend "is a complete package that puts one of America's least appreciated punk bands on display".


== Reissues ==
In December 1983, Negative Trend was reissued by Subterranean Records, this time in 12-inch vinyl disc format, under the title We Don't Play, We Riot and featuring alternate cover art, taken from a Negative Trend flyer done by band's bassist Will Shatter for a gig, shared with Readymades and Avengers, held at the Mabuhay Gardens on December 30, 1977.
In April 2006, a new edition, remastered by Steven Tupper at Fantasy Studios in Berkeley, California, was released on CD via Henry Rollins' label 2.13.61 Records.
Rollins said in 2005 about this release:

"About 25 years ago, I found a single ... that has remained one of my favorite 7" records of all time: The Negative Trend EP. ... Over the years I would see the Negative Trend record here and there, mostly bootlegged versions of the 7" and the occasional copy of the 12" re-release. At one point in the late 90's, I asked the band's drummer Steve DePace what was happening with the Negative Trend record and told him if I ever found out where the [master] tapes were, I was going to put that record out so everyone could hear it. Early this year, I found them and we're releasing the EP on CD. ... The tape held up over all these years and the mastering is fantastic. The Negative Trend EP never sounded better. I am so excited to be releasing this record on my label."

All previous editions went out of print.
As of 2006, individual tracks were made available as downloadables digital audio files.
In late 2013, 35 years after its debut, Negative Trend was remastered, again at Fantasy Studios, but this time by George Horn, for a reissue, in its original 7-inch vinyl disc format and cover art, on Superior Viaduct, an archival record label.


== Re-recordings ==
In November 1978, "Mercenaries", "Meathouse" and "Black and Red" were re-recorded, along with the new songs "I Got Power" and "Atomic Lawn", all of them as demo versions, by the third lineup of Negative Trend, featuring Richard Elerick (pka Rik L Rik) and Tim Mooney, in a six-hour session produced by Robbie Fields from Posh Boy Records at Media Art Studios in Hermosa Beach, California. This was the last time the band went into a recording studio.
In mid-1979, the L.A.-based Upsetter Records issued the Tooth and Nail compilation, featuring Chris Desjardins and Rik L Rik's remixed versions of the Posh Boy demo recordings of "Mercenaries" and "I Got Power".
Following that, Posh Boy decided to remix all five demos from the November 1978 session, adding bass overdubs, as well as a new guitar track on "Atomic Lawn", by Jay Lansford. Shortly after, Posh Boy issued the single "Meat House", initially conceived for Negative Trend, as the first Rik L Rik stand-alone release after he quit the band. This 7-inch pressing on white vinyl featured the Posh Boy remixed versions of "Meathouse" and "I Got Power". In the summer of 1979, the five Posh Boy remixes were included on the label's compilation LP Beach Blvd. The recordings were credited to Rik L Rik.
In 1982, Rik L Rik, backed by members of Gleaming Spires (an offshoot of Sparks), re-recorded "Mercenaries" and "I Got Power" for the soundtrack of a horror short film titled "The Bishop of Battle", starring Emilio Estevez as a video game-addicted teenager who listens to punk rock. Written by Christopher Crowe and directed by Joseph Sargent, the story was originally conceived and shot for the short-lived Universal Television TV series Darkroom, but was never broadcast because its producers deemed it too intense for TV audiences. However, in 1983, the year after the show was canceled, the tale was included, along with other three previously unaired Darkroom episodes, in Sargent's anthology film Nightmares, released theatrically in September of that year. The music featured in the movie has never been released on a soundtrack album.
In 1997, Rik L Rik re-recorded "Meathouse" and "I Got Power" with the New Jersey band Electric Frankenstein for their album Rock 'n' Roll Monster released by the Australian label Au Go Go Records.
The following year, the live EP Electric Frankenstein with Rik L Rik, issued by the Spanish label Munster Records, featured live versions of "Meathouse" and "I Got Power".
In November 2011, Posh Boy compiled the two Negative Trend tracks on Tooth and Nail, along with their five cuts (credited to Rik L Rik) from Beach Blvd, as a downloadable digital audio collection titled November 1978.


== Track listing ==
Where it is necessary, songwriting credits are listed in the format lyrics/music.


== Personnel ==


== Notes ==


== References ==


== Further reading ==
Reviews
Dub, Debbie (July 1984). We Don't Play, We Riot. Maximumrocknroll (15).
Addison, Anne (Early 1985). We Don't Play, We Riot. Unsound 2 (1).


== External links ==
Official
Negative Trend, official website. negativetrend.net. Retrieved May 5, 2015.
Articles
Sideleau, Brandon (December 3, 2005). "2.13.61 releases Negative Trend EP". punknews.org. Retrieved May 29, 2015.
Reviews
Whalley, Matt. "We Don't Play, We Riot: AllMusic Review by Matt Whalley". allmusic.com. Retrieved May 26, 2015.
Sideleau, Brandon (February 4, 2005). "Negative Trend: We Don't Play, We Riot (1982)". punknews.org. Retrieved May 9, 2015.
Alvarado, Jimmy. "Negative Trend: Self-titled: 7” EP". razorcake.org. Retrieved August 18, 2015.
"Negative Trend - s/t 7"". superiorviaduct.com. Retrieved May 29, 2015.
Tolmach, Beth (October 28, 2013). "Negative Trend: "Black and Red"". adhoc.fm. Retrieved March 14, 2016.
Erich (November 14, 2006). "Negative Trend- s/t 7"EP (Heavy Manners, USA, 1978)". goodbadmusic.com. Retrieved May 12, 2015.
Stormy (July 31, 2009). "Negative Trend - 1978-1979 Discography". bloggedquartered.blogspot.mx. Retrieved May 27, 2015.
Images
"Negative Trend 7"". negativetrend.net. Retrieved April 7, 2016.
"Negative Trend 7" Insert". negativetrend.net. Retrieved April 7, 2016.
Negative Trend, cover art. recordcollectorsoftheworldunite.com. Retrieved May 26, 2015.
We Don't Play, We Riot, 1983 12" EP release cover art 1. discogs.com. Retrieved May 26, 2015
We Don't Play, We Riot, 1983 12" EP release cover art 2. discogs.com. Retrieved May 26, 2015
Negative Trend (EP), 2006 remastered CD EP edition cover art. discogs.com. Retrieved March 8, 2016.
Databases
Negative Trend (EP). discogs.com. Retrieved May 5, 2015.
Negative Trend (EP). rateyourmusic.com. Retrieved May 12, 2015.
Negative Trend (EP). punkygibbon.co.uk. Retrieved February 2, 2016.