The College Ystrad Mynach is a former college of further education based in Ystrad Mynach in Caerphilly county borough, Wales. It had over 13,000 students ranging from school leavers to adult mature students. It taught mainly vocational courses from entry level to Degree (in collaboration with Cardiff Metropolitan University, and the University of South Wales).
In 2013, Ystrad Mynach College merged with Coleg Morgannwg to become Coleg y Cymoedd.


== Courses ==
The college offered courses in the following areas:
ACCESS to Higher Education
Art
A/S Levels & A2 A Levels
Basic Skills
Beauty therapy & Holistic Therapy
Business studies
Catering & Hospitality
Computing
Construction
Child Care
Education & Training
Engineering
English for Speakers of Other Languages
GCSE's
Hairdressing
Health & Social Care
ACS Gas & Environmental and Renewable courses
Higher Education
Office Technology
Motor Vehicle
Public Services
Sport
Travel & Tourism
Welsh language
Welsh Baccalaureate


== History ==
The college was opened in 1959 to meet the needs of the local coal mining industry. Since the 1970s the curriculum diversified to assist the diversification and development of the locality. The College worked closely with Caerphilly County Borough Council to improve the economic future of the County Borough.


== Building Work ==
The College Ystrad Mynach underwent extensive building work to areas of its campus in recent years making it one of the most modern and hi-tech campuses in Wales.
2001 - A Block

Housing the main reception, administration offices and some teaching areas, A Block had a complete refurbishment in 2001. The refurbishment costing £2.5 million transformed the block from its previous dated state which was first built in 1959 into one of the most modern campus buildings in Wales.
2002 - Cafe Quarter Officially opened by former Caerphilly AM Ron Davies, a new Cafe Quarter was completed in 2002 providing students with improved modern catering facilities.
2003 - S Block Graddfa Site
Following the merging of Lewis School sites into one location at Pengam, The College purchased a site previously used by Lewis School that was located adjacent to The College Ystrad Mynach (previously Graddfa Secondary Modern School). With student enrolments for The College Ystrad Mynach increasing in the years before 2003, the Graddfa site provided much needed additional space. Following months of extensive renovations to bring the site to an acceptable standard, the site was used as a teaching area from August 2003, housing GCSE, A Level, Education and Training, Languages and Art Provision. The College Ystrad Mynach acquired a grant during the renovation work to build additional changing rooms. These changing rooms serve not only The College but the numerous sports teams that use Ystrad Mynach park for activities on Saturday’s and Sunday’s.
2004 - D Block

A 3 storey state of the art Technology Building titled ‘D Block’ was completed in January 2004 at a cost of £2 million replacing a previous 2 storey building that had been standing since 1959. Officially opened by Roger Jones OBE, Chairman Welsh Development Agency, the building transformed The College’s engineering provision to the highest standards. The block currently serves Construction, Electronics, Aerospace, Electrical and Design courses.
2009-2010 - B Block

Building work commenced in June 2009 on a new B Block facility costing £7.2M which houses 13 classrooms, a new library, improved catering facilities and public areas for learners, exhibitions, competitions and public events. College Principal Bryn Davies commented “B Block will provide the college with much needed space for the 13,000 learners that walk through its door each year. ‘Since 2000, the college has grown extensively in both student numbers and provision. The new facility will allow us provide our learners with leading edge teaching facilities and much needed social areas for our learners.’ Said Bryn Davies, Principal of the college.


== Principals ==

Glyn Sumption. 1959 – 1971
Cynon Parry. 1971 – 1984
Don Brooks. 1985 – 1997
Bryn Davies. 1997 – 2013


== Campuses ==

The main campus is in Ystrad Mynach. Since 2001 The College has invested heavily in its estate.
The College Rhymney - North of the Caerphilly County Borough and serves the top end of the Valley. Its aim is to develop the basic and intermediate skills of the area.
The College @ Centres – Based in High Street locations in Aber Valley, Bargoed, Blackwood & Caerphilly, they provide ‘drop in’ training in IT in an informal environment.
The College @ Tredomen – Located within the Tredomen Business & Technology Centre, provides training for both public sector and private sector companies delivering both “tailor made” and off the shelf courses. The College also runs a number of courses throughout the community in venues such as Community Centres and Schools.


== Scholars Restaurant ==

Scholars Restaurant at The College Ystrad Mynach opened in May 2007 by S4C TV Chef Dudley Newberry. It is open during term time on Wednesdays, Thursdays and Fridays and can cater for groups of up to 70. Bookings can be made by calling The College.


== Hair and Beauty Salons ==
Hair and Beauty Salons are situated in both the Ystrad Mynach and Rhymney campuses. Members of the public are welcome to book into the salons for a range of treatments undertaken by students, whilst supervised by qualified hair & beauty staff at a reasonable price. As a training environment clients must remember that treatments may take longer than usual.


== External links ==
The College Ystrad Mynach Website