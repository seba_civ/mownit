Luthea Salom is a Spanish indie folk singer-songwriter born in Barcelona and based in New York City. She has released four albums. Her most recent CD The Little Things We Do was released on 8 October 2013 by Subterfuge Records reaching number one on iTunes Spain (Alternative) with her single Hey! Wake Up.
Her work has been shortlisted for the Grammy Awards and she was a finalist for Rolling Stone Magazine contest Termómetro RS with her song "Tomorrow". With her videos Blank Piece of Paper and Be Me she has won several awards in numerous contests like International Motion Art Awards by AI-AP (American Illustration and American Photography) as well as International Songwriting Competition, Crank Your Cred, Festival de Cine de Zaragoza and Musiclip. In 2011, she was invited to perform at the annual Christmas Party organized by RAC 1 at Palau de la Musica Catalana. She has appeared on many Spanish National TV shows broadcast on RTVE, EITB, TV3 and Canal 33 as well as Canal Nou, La Otra, Telemadrid among others, and also on Spanish National Radio Stations including RNE: Radio 3, Radio 4, REE; Onda Cero, COM Radio, Cadena Ser, Catalunya Radio, ICat FM and RAC 1.
In September 2014 Luthea Salom was asked by Spanish actress Leticia Dolera to write the soundtrack for her first movie as a scriptwriter and director. The film originally titled Requisitos para ser una persona normal won 5 prizes on the 18th edition of Festival de Malaga, and it has been in theaters across Spain since June 4, 2015.


== History ==
Luthea Salom was born in the Catalan city of Barcelona. Her parents emigrated to Fredericton, Canada, when Luthea was a baby, they lived there for several years before returning to their home town in Catalonia where they opened an art gallery. Luthea started performing and writing music at the age of 13 and she soon appeared in several festivals around Spain. As a teenager she moved to London where she joined several local bands as a guitar player and backing vocalist. In 1999 she recorded a demo of her own songs and was soon spotted by Frank Andrada the former- Apple Records (Spain) AR, who signed her to his independent label.
Luthea Salom's first two CDs were produced by Grammy Award Winning producer Malcolm Burn. Following their release, by 2009, she had more than 200,000 visits on her MySpace page.
After opening for such acts as Norah Jones and Alanis Morissette she released her first album Out and Without. After the release she was invited to open for Emmylou Harris at a Benefit Concert held in Deep River, Ottawa (Canada) in 2003. Her second album Sunbeam Surrounded by Winter was on the Grammy Awards 2008 Ballot in 9 categories including Best New Artist and Album of the Year. National Spanish Television said, "Inspiring, original and with a great sense of style, this is just some of the adjectives that the media has dedicated to Luthea Salom...Luthea's folk songs are characterised by a very colorful acosustic pop and at times sentimental. This is the best way to describe songs like "Like A River", which showscases the talent of this singer. She stands out in the her field due to her optimism."
In 2010 Luthea was made an endorsee by Ovation Guitars, Sonor and Kala Brand Music Co. in the USA and worldwide. In 2012 she was made an endorsee by Guild Guitar Company.
In June 2011 Luthea Salom won the Myspace 'Crank Your Cred' award by International Songwriting Competition for the month of June 2011, with her song "Blank Piece of Paper" entered in the Folk/Singer-Songwriter and Music Video categories.


=== 2001: Out of Without ===
Luthea Salom met up for the first time with Grammy Award Winning producer Malcolm Burn in his Clouet Street studios located in New Orleans during the summer of 2000. In September they started recording her debut CD that was released by Mondicor in 2001. Producer and multi-instrumentalist Ethan Johns (Rufus Wainwright, Ryan Adams, Ray LaMontagne, Kings of Leon, Laura Marling among others) recorded guitars and drums. Acclaimed cellist Jane Scarpantoni (Suzanne Vega, Indigo Girls, Ray LaMontagne, Sarah McLachlan, R.E.M., Lou Reed among others) also appears on several songs on the album.


=== 2007: Sunbeam Surrounded By Winter ===
In 2004 Malcolm Burn and Luthea Salom started recording her second CD, which was originally titled Incessant Spinning, in Kingston, New York, at Maison Bleue Studio and in Skopje, Macedonia, with members of Project Zlust. The CD came out in 2005 and was sold out in Spain. It was then re mastered and re released in 2007 under the title Sunbeam Surrounded By Winter. Luthea was invited to take part of compilation CD Discovering Bowie produced by Valentí Adell. They recorded "Rebel Rebel" and the single was added to Sunbeam Surrounded By Winter as a bonus track.
In 2007 London-based independent music publishers Fairwood Music (David Bowie, JJ Cale, Noel Hogan) discovered and signed Luthea Salom. John Deane of Fairwood Music has been quoted as saying that Luthea has created timeless melodies and well-crafted lyrics. Her creativity, approach and originality will resonate through contemporary music for years to come.
Redwood Entertainment Inc. signed Luthea Salom in 2008 and released and promoted her album in the USA leading her songs to be on broadcast radio across the country on several college radio stations and also NPR & Sirius, with songs "Rebel Rebel", "Dragonfly" and "Like A River".
In 2008 Sunflower Group Entertainment discovered and signed Luthea Salom to their publishing company based in New York City.
Spain's Noticias Clave says "Excellent North American singer/songwriter Luthea Salom is back with a new production filled by her developed folk-rock style which wraps up her vital lyrics that speak both about living in peace and being a rebel. Lyrics are essential in the work of this Canadian-New York-Barcelona citizen, but for those who can't understand English it is still possible to enjoy her musical repertoire, which evolves from her song writing, to the last note of the arrangements, and through her attractive, direct and nuanced interpretation."
Considered a remarkable CD with a unique combination of moving melodies and thoughtful lyrics, in 2008 the CD was launched in the USA and it soon attracted the attention of specialized media entering the ballots for The Grammy Awards in 2008 in 9 categories: Best New Artist, Album Of The Year, Best Pop Vocal Album, Record Of The Year, Song Of The Year, Producer Of The Year Non-Classical, Best Engineered Album Non-Classical, Best Female Pop Vocal Performance and Best Instrumental Arrangement Accompanying Vocalist.


=== 2010: Kick In The Head ===
After touring Spain, Portugal, the USA and Germany, in November 2009 Luthea Salom entered Vapor Studio (Barcelona, Catalonia) to record her third CD which was produced by Valentí Adell and mixed and mastered in the USA by Chris Badami. The CD, titled Kick in the Head, came out digitally in July 2010 hitting the stores in several European countries in October 2010. In April 2011 it sold out in Spain.
Spain's Noticias Clave says Kick in The Head Luthea Salom's latest album "shows her as a talented artesan of songs that she delivers with a personal touch and rock-folk color, where the lyrics (in English) find the best expressive vehicle in her voice – spirited and full of nuances – and her clear diction. The simplicity of the arrangements, seasoned with details that enhance all instrumental expression, contributes to the enjoyment of a successful musical discourse, with good final-touches around a voice that believes and makes us believe in what she says."
Germany's Kultur Cottbus says,"Luthea Salom's bittersweet style is very convincing with her cute voice, almost cheeky lyrics, and her spare guitar-based music. After this song you have already fallen in love with her. Without exaggeration I proudly say that Luthea Salom's "Kick in the Head" is already the best music discovery of the year." Other reviews from Spain include, Noticiario Indie which says, "Kick in the Head is in full swing now, not only all over the news but also in everyone's mouth," and Lagartoon Musica Alternativa states,"A great artist with a wonderful career ahead."
In April 2011 Kick in the Head entered the ballots for Independent Music Awards in Spain in seven categories with songs Blank Piece of Paper and Happy.
In June 2011 Luthea Salom won the Myspace 'Crank Your Cred' award by International Songwriting Competition for the month of June 2011, with her song "Blank Piece of Paper" entered in the Folk/Singer-Songwriter and Music Video categories.
In 2012 Kick in the Head was shorlisted for the Grammy Awards in three categories with songs Blank Piece of Paper and Happy.
Her video Blank Piece of Paper won the International Motion Art Award in 2012. And her video Be Me was a finalist and semi-finalist for Festival Internacional de Cine de Zaragoza and Musiclip.
Luthea's song Tomorrow was a finalist for Rolling Stone Magazine's awards "Termómetro RS" in September 2012.
Luthea's music video from the single Blank Piece of Paper is playing on rotation on MTV Spain and Sol Musica and viewable on YouTube.
In 2015 Blank Piece of Paper became the theme song for new popular TV series Cites which premiered on Catalan TV channel TV3 on April 27, 2015. The song has been on the Spanish iTunes charts since.


=== 2013: The Little Things We Do ===
The Little Things We Do (Subterfuge Records, 2013) was recorded in Sabadell (Barcelona) at Vapor Studio and produced by Valentí Adell during the months of November & December 2012 and January & February 2013. It was soon spotted by the major independent label Subterfuge Records who decided to first release a presentation EP Never Blue" in July 2013 prior to the full album in the fall of 2013.
The song Hey! Wake Up was chosen for online promotional campaigns by Addicted to Dior, Oysho and it later appeared on the crowdfounded documentary 1001 Formas de tomar café directed by actress Leticia Dolera for Nespresso. The single made it up to number one on iTunes Spain Alternative charts in November 2013. In June 2014 the single was featured in the first episode of El Chiringuito de Pepe, a new TV series airing on Telecinco which reached an audience of over 4.6 million people.
Luthea's songs Crazy and The Way Things Are were used by designer Chie Mihara for the promotional campaigns for her FW 2013/2014 shoe collection. The Way Things Are was also the theme song for Jaume Barberà's new TV program Retrats which airs on Canal 33 (Catalan Television).
Songs The Way Things Are, Let's Stay In Bed, Crazy, Hey! Wake Up, Wondering and True Lovers also appeared on new TV series Cites airing on Catalan TV Station TV3. The whole album has been on the Spanish iTunes charts since the show airing.


== Discography ==


=== Studio albums ===
Out of Without (Mondicor, 2001)
Incessant Spinning (Out Music, 2004)
Sunbeam Surrounded By Winter (Out Music, 2007)
Kick in the Head (Femme Sutil, 2010)
The Little Things We Do (Subterfuge Records, 2013)
Requirements to Be a Normal Person - Original Motion Picture Soundtrack (Subterfuge Records, 2015)


=== Singles ===
Laugh About It (Mondicor, 2001)
Die To Live (Out Music, 2004)
Dragonfly (Out Music, 2005)
Like A River (Out Music, 2006)
Rebel Rebel (Out Music, 2007)
Be Me (Femme Sutil, 2010)
Blank Piece of Paper (Femme Sutil, 2011)
Happy (Femme Sutil, 2011)
Never Blue (Subterfuge Records, 2013)
Hey! Wake Up (Subterfuge Records, 2013)
By Your Side (Subterfuge Records, 2015)


=== Compilation albums and collaborations ===
Discovering Bowie (Out Music, 2006)
Yellow Taxi Lounge (Out Music, 2006)
Sounds Like Cafe (Foghorn Media, 2007)
Casa Bonita (Casa Bonita Productions, 2007)
Made in Spain Vol. 2 (Rne 3, 2008)
Día de la Música (FNAC, 2008)
Heart Broken Open (Robert Oakes, 2009)
Quickstars Allstars Volume 3 (Quickstar Productions, 2010)
Sounds Like Café (Fammedia, 2011)
Stereoparty 2013 (Subterfuge Records, 2013)
Stereoparty 2014 (Subterfuge Records, 2014)
Stereoparty 2015 (Subterfuge Records, 2015)


== References ==


== External links ==
Official website