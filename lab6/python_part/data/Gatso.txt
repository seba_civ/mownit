Gatso is the brand that Gatsometer BV use on their speed cameras and red light cameras. The most commonly encountered Gatso speed cameras emit radar beams to measure the speed of a passing vehicle. If it is travelling above the preset trigger speed, one or two photographs are taken (depending on the device's setting, which generally depends on the requirements of the local jurisdiction). These use a powerful flash, to show the rear of the vehicle, its registration plate, and calibration lines on the road (in many jurisdictions). Newer installations used digital cameras which have limited exposure latitude compared to wet film, these installations use an auxiliary flash placed close to the position where a speeding vehicle would exit the radar beam and the first photograph would be taken.


== Operation ==
Gatso installations in the UK and in Queensland, Australia are characterised by a measurement strip on the road surface, which is a series of white lines painted on the road, which are used with two photographs taken by the camera. If the camera is set to take two photographs, with a known time interval between them, this time interval will be 0.5 or 0.7 seconds - 0.5 is better for high speed roads and 0.7 better for low speed roads. The vehicle's position, relative to the white road markings in the two photographs, can be used to calculate the vehicle speed. In the UK, the evidence is only admissible in court if the speed measured by the radar and the speed calculated from the distance travelled between the photographs agrees within 10%. The speed indicated by the radar unit is too unreliable to be used as the sole means of evidence as it is prone to error due to multiple reflections etc. Further, it does not distinguish between multiple vehicles in shot.


== UK deployment ==

The first red light cameras were introduced in an initiative in the City of Nottingham in 1988 following a triple fatal road traffic accident at a traffic light controlled road junction.
The Department of Transport took an interest and a trial was sponsored by that Department involving the Home Office and the Metropolitan Police. The operational base was at the West London Traffic Unit. Roger Reynolds, a Police Sergeant (later to become President of the Royal Photographic Society) undertook operational trials of the equipment and, by adjusting the camera controls, managed to use Colour film for the first time replacing black and white film. Reynolds made the first successful use of the Gatso camera on the A316 road at Twickenham Bridge in 1992.
The first controlled junction was at the Hanger Lane Gyratory, on the A406 North Circular Road southbound at its junction with the A40 Western Avenue. This junction was then followed by the A501 Marylebone Road at its junction with Gloucester Place. A further installation was commissioned on the eastbound A40 at its junction with Long Lane (Hillingdon). After the camera units were deployed there was a significant improvement in accident rates in their vicinity.
The Police Camera team developed paper handling systems using (then) current process and later laid down systems to incorporate conditional offer fixed penalty systems. The processes developed by the team formed the basis of similar systems, used and modified by other police forces. Also a member of the Team travelled to Hong Kong and assisted in the introduction of cameras there.
Speed cameras were also evaluated by the Metropolitan police team for their speed limit enforcement capabilities before the system was rolled out. The linear markings suggested by the team are now an integral part of the camera site marking.
The Team that introduced the system and the engineers of the Department of Transport, as road safety specialists, were keen to use the equipment to change driver behaviour and thus improve road safety. However strategic control of cameras later moved to the newly created Safety Camera Partnerships who appear to have restricted warnings and reduced prosecution thresholds.
Gatso cameras in the UK previously had deployment requirements, in common with all fixed speed camera types operated in the UK by Safety Camera Partnerships (SCPs) under the National Safety Camera Programme (NSCP). They had to be marked, made visible, located in places with a history of serious accidents, and where there was evidence of a speeding problem, or where was a local community concern.
Since April 2007 however, such requirements have been removed. It is still recommended that cameras be made visible and roads with fixed ones, clearly signed, but they can now be placed at any location, regardless of its crash history. Cameras operated solely by the Police, outside the NSCP schemes, do not need to comply with visibility requirements.
The funding arrangements for SCPs also changed in April 2007. SCPs no longer keep the funds from speeding fines, instead an annual 'road safety grant' is given to local authorities directly, who can choose whether or not to invest it in these partnerships.
Concerns have been raised that the powerful flash used when the cameras trigger may dazzle drivers travelling in the opposite direction. The flash on the Truvelo Combi is fitted with a magenta gel to obviate this issue.


== Gatsometer BV ==
It is manufactured by the Dutch company GATSOmeter BV. The company's history started with a device invented by champion rally driver Maurice Gatsonides. He used the device to record his speeds as he drove through various curves and other road configurations, so that he could review his line and speed so as to optimise his performance in future races. The company was later formed to market the devices as police speed enforcement tools and remains largely a family concern with two Gatsonides among the five directors.
The company's products, particularly the "Gatsometer 24", have achieved such a high level of market penetration in certain European countries that the term "Gatso" has become synonymous with "speed camera" across a significant proportion of Europe. Historically, the company has had less success marketing its products in the USA, where it operated initially through agents or by selling its cameras to other companies that would integrate them into larger systems for customers. The most notable installed base of Gatsometer products in the USA is that in and around Washington DC. However, Gatsometer BV now has an American subsidiary, in the form of GATSO-USA, which now markets the company's products direct and competes with the other speed/red-light camera companies active in the North American market.


== See also ==
Safety Camera Partnership
Truvelo Combi


== References ==


== External links ==
Official website