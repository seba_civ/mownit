Marco Reus (German pronunciation: [ˈmaʁkoː ˈʁɔʏs]; born 31 May 1989) is a German professional footballer who plays as an attacking midfielder, winger or striker for German Bundesliga club Borussia Dortmund and the Germany national team. Reus is known for his versatility, speed and technique.
Reus spent his youth career at Borussia Dortmund, prior to leaving for Rot Weiss Ahlen. He has played for three clubs in his senior career, most notably—and with the most influence—in Borussia Mönchengladbach of the Bundesliga. Reus plays primarily as a left attacker for BVB; however, he is capable of playing on the right also and through the middle, due to his ability to closely control the ball with both feet. 2012 was his most successful season when, scoring 18 and assisting eight, he helped Borussia Mönchengladbach secure a place in the following season's UEFA Champions League. Reus agreed a move to his home club Borussia Dortmund at the end of that season. Reus wears number 11 for Dortmund.
With Dortmund, Reus won the 2013 DFL-Supercup. Reus was the Footballer of the Year in Germany in 2012 and was on the UEFA Team of the Year in 2013. Franz Beckenbauer spoke about Reus, along with Mario Götze, saying, "...as a classic duo there is nobody better than the prolific Reus and Götze." In 2013, Reus was ranked as the fourth best footballer in Europe by Bloomberg. In 2016, Reus was featured in a music video titled "Aubameyang" which was released by his teammate Pierre-Emerick Aubameyang's younger brother.


== Early career ==
Reus was born in Dortmund. He began to play football for his hometown club Post SV Dortmund in 1994 and joined the youth ranks of Borussia Dortmund in 1996. He played for Borussia Dortmund until he left for the U-19 team of Rot Weiss Ahlen in the summer of 2006. During his first year there, he played as an attacking midfielder and was featured in five games for the club's second team, which played in the Westphalia league at the time. He scored a goal in each of his first two games. The following year, he was able to break into Ahlen's first team, which played in the German third division at the time. He started twice and was featured in 14 matches, scoring two goals. One of his goals came on the last day of the season and propelled the team being promoted to the 2. Bundesliga.
In 2008–09, as a 19-year-old, he had his definitive breakthrough as a professional football player, playing 27 games and scoring four goals.


== Club career ==


=== Borussia Mönchengladbach ===

On 25 May 2009, Reus signed a four-year contract with Bundesliga club Borussia Mönchengladbach. On 28 August 2009, he scored his first Bundesliga goal in a game against Mainz 05 after a 50-metre solo run, and since then became a prolific goalscorer for his club under Lucien Favre. At the start of the 2011–12 season, Reus began the season in fine form, scoring seven goals in 12 matches. His contract with Gladbach was set to expire in 2015 and reportedly had a buy-out clause of €18 million, and he mentioned that his role model was Arsenal and Czech national team midfielder Tomáš Rosický, who himself played for Borussia Dortmund for six years before moving to the English Premier League club in 2006.


=== Borussia Dortmund ===


==== 2012–13 season ====
On 4 January 2012, Reus signed with his former club Borussia Dortmund for a transfer fee of €17.1 million on a five-year deal that will keep him at the club until July 2017. He spoke about his transfer saying, "I've made the decision to take the next step forward in the coming season. I'd like to play for a club who can challenge for the league title and guarantee me Champions League football. I see this chance in Dortmund." Reus officially re-joined Dortmund on 1 July 2012. In Reus' Bundesliga debut with Dortmund on 24 August, he scored a goal as his new side completed a 2–1 win over Werder Bremen. On 29 September, Reus scored two goals for Dortmund in a 5–0 rout of his former club Borussia Mönchengladbach, pushing the champions to the top of the Bundesliga table through six games.
On 3 October, in Reus' first ever Champions League appearance, he opened the scoring as Dortmund earned a 1–1 draw away to Manchester City. He then opened the scoring for the German champions in their 2–2 draw with Real Madrid at the Santiago Bernabéu on 6 November, netting on a spectacular volley after a kick-down from teammate Robert Lewandowski. In Dortmund's following Champions League match, on 21 November, Reus scored Dortmund's first goal in a 4–1 defeat of AFC Ajax at the Amsterdam Arena, securing qualification for the Round of 16 as Group D winners.
On 16 February 2013, Reus scored a hat-trick, netting all the goals in Dortmund's thumping of Hessian side Eintracht Frankfurt. On 11 May, Reus scored a late brace against Wolfsburg to help Dortmund draw the match after being two goals down.


==== 2013–14 season ====

On 27 July 2013, in the 2013 DFL-Supercup against Bayern Munich, Reus netted the game's opening goal and later rounded off the scoring, as Borussia Dortmund claimed a 4–2 victory to lift the trophy.
On 18 August, Reus converted a penalty kick which rounded off the scoring as Dortmund defeated Eintracht Braunschweig 2–1 at the Signal Iduna Park in the second game of Dortmund's 2013–14 league campaign. He then scored a brace against SC Freiburg, one from the penalty spot, and started the season hitting impressive form. Reus had confirmed he would be Dortmund's penalty taker for the season, though despite winning a penalty against 1860 München in the DFB Pokal, he allowed teammate Pierre-Emerick Aubameyang in taking the penalty which propelled the Borussia-based club into the next round of the competition. On 1 November 2013, Reus scored a goal for Dortmund in a 6–1 win against Stuttgart in the Bundesliga.
On 25 February 2014, Reus scored a goal in a 4–2 win for Dortmund against Zenit Saint Petersburg in the first leg of the Round of 16 in the UEFA Champions League. On 29 March, he then scored a hat-trick in a 3–2 win for Dortmund against Stuttgart in the Bundesliga. On 8 April, Reus scored twice in Borussia Dortmund's second leg 2–0 win against Real Madrid, though Dortmund were eventually knocked out of the competition after losing 3–2 on aggregate. Reus finished the season with 23 goals and 18 assists in all competitions.


==== 2014–15 season ====
In the second match of the 2014–15 Bundesliga against Augsburg, Reus scored one goal and helped set up another. Dortmund went to win the match by 3–2. In September 2014, he was diagnosed with ankle injury. On 22 October 2014, Reus returned from injury and contributed a goal and assist in their 4–0 Champions League group stage away win against Galatasaray. On 1 November, he scored the only goal for Dortmund in their 2–1 away loss against their rivals Bayern Munich.
Despite prior injury concerns, Reus started Dortmund's match away to SC Paderborn 07 on 22 November and scored to put them 2–0 up. He was stretchered off in the second half, however, after which Paderborn equalised for a final score of 2–2. The injury ruled him out until January 2015.
On 10 February 2015, Reus signed a contract extension with Dortmund, keeping him at the club until 2019.


==== 2015–16 season ====
On 5 August 2015, Reus scored a goal in their 5–0 win against Wolfsberger AC to advance into the 2015–16 Europa League play-off round. On 15 August, he then opened the 2015–16 league campaign with a goal and an assist in a 4–0 home win against his former club Borussia Mönchengladbach. On 28 August 2015, he scored a hat-trick in their 7–2 home win against Odds BK to qualify for the 2015–16 UEFA Europa League.
On 20 April 2016, Reus was one of three goalscorers as Borussia won 3–0 away at Hertha BSC in the semi-final of the DFB-Pokal.


== International career ==

On 11 August 2009, Reus made his under-21 team debut in a friendly match against Turkey. On 6 May 2010, he earned his first call-up to the senior team for a friendly match against Malta on 14 May 2010. On 11 May 2010, he withdrew from the squad due to a leg injury picked up in the last game of the season against Bayer Leverkusen. On 7 October 2011, he made his debut against Turkey. He scored his first goal for the team on 26 May 2012 in a 5–3 defeat to Switzerland. On 22 June, he scored in the UEFA Euro 2012 quarter-final against Greece, his first start for Germany in the tournament.
Reus established himself as a regular member of Joachim Löw's side in the 2014 FIFA World Cup qualification campaign, scoring five goals and registering three assists in six matches. He was named in Germany's squad for the World Cup finals, but was ultimately forced to withdraw after suffering an ankle injury in the team's 6–1 warm-up win against Armenia on 6 June.
Reus made his international comeback in Germany's first post-World Cup friendly against Argentina on 3 September 2014. However, the world champions were defeated 4–2 by the side they had beaten in the World Cup final two months earlier.
During qualification for UEFA Euro 2016, Reus made four appearances, scoring once in a 2–0 defeat of Georgia in Tbilisi.


== Career statistics ==


=== Club ===
As of 30 April 2016
1.^ Includes UEFA Champions League and UEFA Europa League
2.^ Includes Relegation playoff and DFL-Supercup.


=== International ===
As of 29 March 2016


=== International goals ===
Scores and results table. Germany's goal tally first:


== Honours ==


=== Club ===
Rot Weiss Ahlen
Regionalliga: 2007–08
Borussia Dortmund
DFL-Supercup: 2013, 2014; Runner-up: 2012
UEFA Champions League Runner-up: 2012–13
DFB-Pokal Runner-up: 2013–14, 2014–15


=== Individual ===
kicker Bundesliga Team of the Season: 2011–12, 2012–13, 2013–14
Bundesliga Breakthrough of the Season: 2011–12
Footballer of the Year in Germany: 2012
Goal of the Month (Germany): January 2012, June 2012, September 2012
UEFA Team of the Year: 2013
Bundesliga top assists: 2013–14
Borussia Dortmund 'Player of the Year': 2013–14
UEFA Champions League Team of the Season: 2013–14


== Personal life ==
In December 2014, Reus was fined over £500,000 for driving without a licence, having been issued with speeding tickets on at least five occasions since 2011 without authorities knowing that he was not licensed. When convicted, Reus said, "The reasons I did it are something I cannot really understand." Prior to his conviction, he had appeared in commercials for cars and petrol.


== References ==


== External links ==
Official website
German national team profile (German)
Marco Reus profile at Fussballdaten
Marco Reus at National-Football-Teams.com
Marco Reus – FIFA competition record
Marco Reus – UEFA competition record
Marco Reus at ESPN FC
Kicker profile (German)
Bundesliga profile