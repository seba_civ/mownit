Gamla (Hebrew: גמלא‎) or Gamala was an ancient Jewish city on the Golan Heights, believed to have been founded as a Seleucid fort during the Syrian Wars. The site of a Roman siege during the Great Revolt of the 1st century CE, Gamla is a symbol of heroism for the modern state of Israel and an important historical and archaeological site. It lies within the current Gamla nature reserve and is a prominent tourist attraction.


== History ==
Situated at the southern part of the Golan, overlooking the Sea of Galilee, Gamla was built on a steep hill shaped like a camel's hump, from which it derives its name (Gamla meaning 'camel' in Aramaic). Although the site was inhabited since the Early Bronze Age, the city appears to have been founded as a Seleucid fort during the Syrian Wars (3rd century BCE) which later became a civilian settlement. Jews inhabited it from the last quarter of the 2nd century BCE, and it was annexed to the Hasmonean state under king Alexander Jannaeus in c. 81 BCE.
Josephus Flavius, Commander of Galilee during the Jewish Revolt against Rome, in 66 CE fortified Gamla as his main stronghold on the Golan. Josephus gives a very detailed topographical description of the city, which he also referred to as Gamala, and the steep ravines which precluded the need to build a wall around it. Only along the northern saddle, at the town's eastern extremity, was a 350 meters-long wall built. It was constructed by blocking gaps between existing houses and destroying houses that lay in its way.
Initially loyal to the Romans, Gamla turned rebellious under the influence of refugees from other locations. It was one of only five cities in the Galilee and Golan who stood against Vespasian's legions, reflecting the cooperation between the local population and the rebels. At the time of the revolt, the town minted its own coins, probably more as a means of propaganda than as currency. Bearing the inscription "For the redemption of Jerusalem the H(oly)" in a mixture of paleo-Hebrew (biblical) and Aramaic, only 6 of these coins have ever been found.
Josephus also provides a detailed description of the Roman siege and conquest of Gamla in 67 CE by components of legions X Fretensis, XV Apollinaris and V Macedonica. The Romans first attempted to take the city by means of a siege ramp, but were repulsed by the defenders. Only on the second attempt did the Romans succeed in breaching the walls at three different locations and invading the city. They then engaged the Jewish defenders in hand-to-hand combat up the steep hill. Fighting in the cramped streets from an inferior position, the Roman soldiers attempted to defend themselves from the roofs. These subsequently collapsed under the heavy weight, killing many soldiers and forcing a Roman retreat. The legionnaires re-entered the town a few days later, eventually beating Jewish resistance and completing the capture of Gamla.
According to Josephus, some 4,000 inhabitants were slaughtered, while 5,000, trying to escape down the steep northern slope, were either trampled to death, fell or perhaps threw themselves down a ravine. These appear to be exaggerated and the number of inhabitants on the eve of the revolt has been estimated at 3,000 - 4,000.


== Excavations ==
Abandoned after its destruction, Gamla was identified in 1889 by Konrad Furrer with the site of Tel ed-Dra', in the Ruqqad river-bed. It was only properly identified in 1968 by surveyor Itzhaki Gal, after the Israeli conquest of the Golan Heights during the Six-Day War. It was excavated by Shemaryahu Gutmann and Danny Sion on behalf of the Israeli Department of Antiquities between 1977 and 2000. The excavations have uncovered 7.5 dunnams, about 5% of the site, revealing a typical Jewish city featuring ritual baths, Herodian lamps, limestone cups and thousands of Hasmonean coins.
The Gamla excavations also revealed widespread evidence for the battle that took place at the site. About 100 catapult bolts have been uncovered, as well as 1,600 arrowheads and 2,000 ballista stones, the latter all made from local basalt. This is a quantity unsurpassed anywhere in the Roman Empire. Most were collected along and in close proximity to the wall, placing the heavy fighting in the vicinity and the Roman siegecraft to the north east of the town. Next to a heavy concentration of the stones, the excavators have identified a man-made breach in the wall, probably made by a battering ram.
About 200 artifacts excavated at Gamla have been identified as the remains of Roman army equipment. These include parts of Roman lorica segmentata, an officer's helmet visor and cheek-guard, bronze scales of another type of armor, as well as Roman identification tags. A Roman siege-hook, used both for stabbing and hooking onto the wall, was found in the breach.
Only one human jawbone was identified during the exploration of Gamla, raising questions regarding the absence of human remains despite the widespread evidence of a battle. A tentative answer is discussed by archaeologist Danny Syon, who suggests that the dead would have been buried at nearby mass graves that are yet to be found. One such mass grave has been found at Yodfat, which had suffered the same fate as Gamla at the hands of Vespasian's legions.
Artifacts from Gamla are on the display at the Golan Archaeological Museum, including arrowheads, ballista stones, clay oil lamps and coins minted in the town during the siege. A scale model and film are used to describe the conquest and destruction of the Jewish town and all of its inhabitants.


=== Synagogue ===

The remains of one of the earliest synagogues is situated inside the city walls. It was built of dressed stone with pillared aisles. Measuring 22 by 17 meters, its main hall was surrounded by a Doric colonnade, its corner columns were heart-shaped, and it was entered by twin doors at the south west. A ritual bath was unearthed next to it. On the eve of Gamla's destruction, the synagogue appears to have been converted to a dwelling for refugees, as testified by a number of meager fireplaces and large quantities of cookpots and storage jars found along its northern wall. Situated next to the city wall, 157 ballista stones were collected from the synagogue's hall alone and 120 arrowheads from its vicinity. The synagogue is thought to date from the late 1st century BCE and is among the oldest synagogues in the world.
The chronology of the synagogue was challenged by Ma'oz in 2012. His interpretation is that it was built about 50 CE and a miqveh was added in 67 CE. The earlier "miqveh" was, in Ma'oz' understanding, a water cistern.


== Present-day Gamla ==
Today Gamla is an archaeological site and a nature reserve. It is also home to a large nesting population of Griffon vultures. The nature reserve also contains some 700 Neolithic Dolmens, several of which can be viewed from the entry road.


== See also ==
First Jewish Revolt coinage
Herodium
Machaerus
Masada


== Gallery ==


== References ==


=== Notes ===


=== Bibliography ===
Berlin, Andrea M.; Overman, J. Andrew (2002). The First Jewish Revolt: Archaeology, History and Ideology. Routledge. ISBN 0-415-25706-9. 
Josephus, Flavius. William Whiston, A.M., translator (1895). The Works of Flavius Josephus – Antiquities of the Jews. Auburn and Buffalo, New York: John E. Beardsley. Retrieved 16 October 2010.
Josephus, Flavius. William Whiston, A.M., translator (1895). The Works of Flavius Josephus – The Wars of the Jews. Auburn and Buffalo, New York: John E. Beardsley. Retrieved 16 October 2010.
Rocca, Samuel (2008). The Forts of Judaea 168 BC – AD 73. Oxford, United Kingdom: Osprey Publishing. ISBN 978-1-84603-171-7. 


== External links ==
Gamla on the Israel Antiquities Authority website.
Gamla on the Israel Nature and Parks Authority website.
Vultures of Gamla