Expo/Vermont is an at-grade light rail station in the Los Angeles County Metro Rail system, located on Exposition Boulevard at Vermont Avenue, in the Exposition Park District of Los Angeles. This station is served by the Expo Line.


== Service ==


=== Metro Rail service ===
Expo Line service hours are approximately from 4 AM to 12:30 AM daily. Service resumed Saturday, April 28, 2012. Regular scheduled service resumed Monday, April 30, 2012.


== Location and design ==
Expo/Vermont station is located in the median of Exposition Boulevard, on both sides of Vermont Avenue. The location is at a busy intersection in the West Adams District.


== Attractions ==
The intersection of Expo/Vermont has three major destinations:
University of Southern California main campus (northeast corner)
Exposition Park (southeast corner)
Natural History Museum
Memorial Coliseum (home of USC Trojans Football)
Masjid Omar Ibn Al-Khattab mosque (northwest corner).
The station has "far-side" platforms: this means that the platforms are positioned on opposite sides of the intersection, and trains always stop at the platform after crossing the intersection.
The station's art, commissioned by the Los Angeles County Metropolitan Transportation Authority, was created by artist Jessica Polzin McCoy. Entitled Neighborhood Portrait: Reconstructed, the 24-panel installation includes photographs that take the viewer inside of the homes of West Adams residents. After constructing collages from her photos, McCoy then created intricate watercolor paintings of each collage. Artisans at Montreal-based Mosaika Art & Design worked closely with the artist to translate her watercolors into hand-glazed ceramic mosaic panels.


== History ==
Originally a stop on the Los Angeles and Independence and Pacific Electric railroads, it closed on September 30, 1953 with closure of the Santa Monica Air Line and remained out of service until re-opening on Saturday, April 28, 2012. It was completely rebuilt for the opening of the Expo Line from little more than a station stop marker. Regular scheduled service resumed Monday, April 30, 2012.


== References ==


== External links ==
 Media related to Expo / Vermont (Los Angeles Metro station) at Wikimedia Commons
Metro Expo Line Construction Authority
Project Website, Metro Rail Expo Corridor, Phase 1 to Culver City