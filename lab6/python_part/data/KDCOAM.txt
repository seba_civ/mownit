KDCO (1340 AM) is a radio station licensed to Denver, Colorado. The station is owned by Victor Michael, Jr., through licensee Kona Coast Radio, LLC, and airs a sports format, branded as "Mile High Sports".
The station was launched by Frederick W. Meyer, a wholesale grocer, in 1940 as KMYR with 250 watts of power. The station's sale in 1956 brought a call letter change to KDEN. In December 1993, after an eight-month period off the air 1340 AM became KKYD "Radio AAHS", the Denver outlet of the first nationwide network of radio programs for children. The downfall of Radio AAHS came when the Walt Disney Company established a competitor, Radio Disney. After the sign-off of Radio AAHS in January 1998, Children's Broadcasting Corporation needed programming for the network of stations until it could find buyers. KKYD and the other nine CBC-owned and operated Radio AAHS affiliates became an outlet for "Beat Radio", which broadcast electronic dance music 12 hours a night until late October 1998. KKYD then switched to a religious format, which it aired until sale from Catholic Radio Network to Colorado Public Radio. The announcement of CPR's purchase February 9, 2001, also mentioned plans to convert the station from commercial to non-commercial status.
In September 2000, CPR acquired the intellectual properties of then-classical station KVOD-AM 1280. In March 2001, KVOD replaced KCFR at 90.1 FM, with KCFR being moved to 1340 AM. On July 9, 2008, KCFR moved back to 90.1 FM, with KVOD moving to the newly acquired 88.1 FM frequency. The changes were made as CPR wanted to go FM-only by the end of the year; until a buyer could be found for 1340, CPR continued to operate it as a simulcast of KCFR.
On October 31, 2011, CPR launched OpenAir on 1340 AM, billing it "New Music from Colorado Public Radio." It has a broad-based format that focuses on current independent artists with a great deal of current Colorado talent. OpenAir also streams online at www.openaircpr.org. With the change, new KVOQ call letters were adopted.
On July 24, 2015, Colorado Public Radio announced that it would swap KVOQ to Victor Michael's Cedar Cove Broadcasting, Inc. in exchange for KRKY-FM; Michael also forgave $100,000 in debt as part of the swap. On July 27, 2015, KVOQ changed their format to sports, branded as "Mile High Sports". Its previous OpenAir format is now heard on KVOQ-FM 102.3.
On August 28, 2015, the station changed its call sign to the current KDCO.
Effective January 20, 2016 Cedar Cove Broadcasting traded KDCO to Kona Coast Radio, LLC in exchange for translators K214DW and K206EO. Kona Coast Radio is also owned by Victor Michael.
On April 28, 2016 KDCO began simulcasting on FM translator K284CI 104.7 FM Denver.


== References ==


== External links ==

Query the FCC's AM station database for KDCO
Radio-Locator Information on KDCO
Query Nielsen Audio's AM station database for KDCO
Query the FCC's FM station database for K284CI
Radio-Locator information on K284CI