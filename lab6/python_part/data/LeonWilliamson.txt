Leon Williamson is a New Zealand former rugby league referee. An international referee, Williamson also controlled Auckland Rugby League, New Zealand Rugby League, Rugby Football League Championship and Toyota Cup matches.


== Referee career ==
Williamson took up refereeing in 1996. Prior to this he was a kickboxer and was ranked 19th in the world in the 61kg super-lightweight division.
Williamson served as an international referee and refereed in the 2008 World Cup, 2009 European Cup and 2009 Four Nations. In the 2009 Four Nations he was appointed to referee the final between Australia and England. He has also controlled various one off international matches.
Williamson retired from international matches in 2010, his last international appointment was controlling the match between the New Zealand Māori rugby league team and the England national rugby league team on 16 October.
In 2013 Williamson lost a tooth during a match after a spectator threw a rock at him.


== Personal life ==
Leon currently resides in Wattle Downs, in the South of Auckland. He is married to Vicky and together have two children, twins Brooke and Dillon. Williamson is a plumber by trade and runs his own company, Rescue Plumbing. In his spare time, Williamson enjoys fishing, riding his motorcycle and spending quality time with his family.


== References ==


== External links ==
championshipstats.rlfans.com stats