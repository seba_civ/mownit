The Department of Infrastructure and Regional Development is an Australian Public Service department of the Government of Australia charged with the responsibility for national policies and programs that promote, evaluate, plan and invest in infrastructure; foster an efficient, sustainable, competitive, safe and secure transport system; and ensure a strong and liveable Australia by focusing on effective local government and external territories administration and regional development that enables communities to achieve economic and social dividends. Matters dealt with by the department include: infrastructure planning and coordination; transport safety; land transport; civil aviation and airports; maritime transport including shipping; administration of Australian territories; constitutional development of the Northern Territory and the Australian Capital Territory; regional programs; regional development; local government matters; and regional policy.
The head of the department is the Secretary of the Department of Infrastructure and Regional Development, currently Mike Mrdak, who reports to the Minister for Infrastructure and Transport, currently the Hon. Darren Chester MP, the Minister for Regional Development, Senator the Hon. Fiona Nash, and the Minister for Territories, Local Government and Major Projects, currently the Hon. Paul Fletcher MP.
The department is headquartered in the Canberra central business district at Infrastructure House and the neighbouring building to Infrastructure House.


== Operational activities ==
In an Administrative Arrangements Order made on 18 September 2013, the functions of the department were broadly classified into the following matters:
Infrastructure planning and co-ordination
Transport safety, including investigations
Land transport
Civil aviation and airports
Transport security
Maritime transport including shipping
Major projects office, including facilitation and implementation of all non-Defence development projects
Administration of the Jervis Bay Territory, the Territory of Cocos (Keeling) Islands, the Territory of Christmas Island, the Coral Sea Islands Territory, the Territory of Ashmore and Cartier Islands, and of Commonwealth responsibilities on Norfolk Island
Constitutional development of the Northern Territory
Constitutional development of the Australian Capital Territory
Delivery of regional and territory specific services and programmes
Planning and land management in the Australian Capital Territory
Regional development
Matters relating to local government
Regional policy and co-ordination


== Prominent business units in the department ==


=== Bureau of Infrastructure, Transport and Regional Economics ===
The Bureau of Infrastructure, Transport and Regional Economics (BITRE) within the department provides economic analysis, research and statistics on infrastructure, transport and regional development issues to inform Australian Government policy development and wider community understanding. BITRE employs around 30 staff, including statisticians, economists and policy analysts. BITRE was first established in 1970 as the Bureau of Transport Economics by the Cabinet.


=== Office of Transport Security ===
The Office of Transport Security (OTS), a business division within the department, is the Australian Government’s preventive security regulator for the aviation and maritime sectors, and its primary adviser on transport security. The OTS head office is in Canberra, and regional offices are situated in Brisbane, Sydney, Melbourne, Adelaide and Perth.


== Structure and staff ==

The department is administered by a senior executive, comprising a Secretary and several Deputy Secretaries.
The current Secretary is Mike Mrdak, appointed on 29 June 2009. Mr Mrdak began his public sector career in 1988 as a Graduate with the then Department of Transport and Communications. He holds a bachelor's degree in arts along with a graduate diploma in economics.
The department has a staff of around 994 people (estimate for 2013–14), of which around 836 are employed in Canberra and 15 are based overseas. Staff are employed as part of the Australian Public Service under the Public Service Act 1999. The workforce of the department has a reasonably even gender distribution (54% male, 46% female), but at more senior levels this ratio decreases. Around two-thirds of the department holds a bachelor's degree or higher.
The department works closely with several Australian Government agencies within its portfolio, including:
the Australian Transport Safety Bureau (ATSB);
the Australian Rail Track Corporation;
Airservices Australia;
the Australian Maritime Safety Authority;
the Civil Aviation Safety Authority (CASA); and
the National Transport Commission.


== Budget and Finance ==
In the department's 2013–14 budget statements, expenses are categorised as either departmental or administered expenses. Departmental expenses are those within the control of the relevant agency, whereas administered expenses are those administered on behalf of the Government. Expenses can be broken down as follows:


=== Audit of expenditures ===
The department's financial statements are audited by the Australian National Audit Office.


== History ==
The Department of Infrastructure and Regional Development was formed by way of an Administrative Arrangements Order issued on 18 September 2013 and replaced the majority of the functions previously performed by the former Department of Infrastructure and Transport and some of the functions previously performed by the former Department of Regional Australia, Local Government, Arts and Sport; with the exception of the arts functions that were transferred to the Attorney-General's Department and the sports functions that were assumed by the Department of Health and Ageing.


=== Departments with responsibility for infrastructure and transport ===


=== Departments with responsibility for regional development and local government ===


== See also ==

List of Australian Commonwealth Government entities
Minister for Infrastructure and Regional Development
Transportation in Australia


== References ==


== External links ==
Department of Infrastructure and Regional Development Website