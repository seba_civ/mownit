The term Máchimoi (Greek: μάχιμοι, plural; μάχιμος, máchimos, singular) commonly refers to a broad category of ancient Egyptian low-ranked soldiers which rose during the Late Period of Egypt (664–332 BCE) and, more prominently, during the Ptolemaic dynasty (323–30 BCE).


== History ==


=== Herodotus and the Late Period ===
He earliest attestation of this term given to native Egyptian warriors came from Herodotus – who visited Egypt during the first Persian domination (Manetho's 27th Dynasty) – and since him this term has been usually translated simply as "warriors" or "fighting men". It should be noted, however, that the same term was used by him, referring to Asiatic troops employed by the Persians. Herodotus provided some informations about the Egyptian máchimoi, claiming that they were literally a closed caste of warriors who were forbidden to practice other activities outside of combat and were provided twelve arourai of tax-free land as a reward for their services. Herodotus also recognizes two categories of máchimoi, called hermotybies and kalasiries, which were distinct by their districts of origin; he also claims that the two categories were composed by 160,000 and 250,000 soldiers respectively.
As well as Herodotus, also other Greek authors such as Plato and Diodorus Siculus, reports that the máchimoi were deployed in many battles during the Late Period. Pharaoh Apries send them against Cyrene but after their defeat, they proclaimed general Amasis as pharaoh and served him against Apries in 570 BCE. Egyptian máchimoi also fought at Plataea in 479 BCE. During the 30th Dynasty, Egyptian máchimoi were widely used against the Achaemenid Empire: according to Diodorus, pharaoh Teos sent 80,000 of them in his expedition in the Near East in c.360/358 BCE and his nephew Nakhthorheb (the future Nectanebo II) was their commander. Nectanebo II himself later relied on these soldiers before the second Persian conquest of Egypt in 343 BCE.


=== Ptolemaic Period ===

Máchimoi were still present during the Ptolemaic period, and most scholars considers them as the direct successors of their Late Period counterparts; Ptolemaic máchimoi are mostly still seen as a caste of native-Egyptian, land-granted, low-ranked warriors whom, with the passing of time, takes on increasingly important roles alongside the Greek army likely since the battle of Raphia in 217 BCE, and exerted increasing social pressure on the Ptolemies and was responsible for various rebellions and uprisings.
Curiously, under the Ptolemies the name máchimoi is attested only on documents while during the Late Period they were mentioned exclusively on Greek literary works: for example, Diodorus clearly calls máchimoi the Egyptian soldiers of pharaoh Teos, but not their Ptolemaic counterparts.
The earliest mention of máchimoi on Ptolemaic documents is dating back to the reign of Ptolemy II Philadelphus (261 BCE) and refers to guard duties; this was not uncommon, as from many documents it seems that they sometimes were guards and sometimes had purely army duties. However, the most famous document mentioning them is the Rosetta Stone (Greek text, row 19) made under Ptolemy V Epiphanes (196 BCE), which refers to an amnesty for some deserted máchimoi.


== 2013 reinterpretation ==
A 2013 paper by historian Christelle Fischer-Bovet revised many of the traditional claims about the máchimoi. She challenged most of Herodotus' description, pointing out that ancient Egyptians never used similar caste systems, and both the total numbers of máchimoi and of the lands given to them are almost certainly unsustainable, suggesting that Herodotus unintentionally merged professional military officers with a militia composed by commoners who were called to arms if necessary, and attributed to the whole group an elite status not much different from that of Greek Spartiates. Fischer-Bovet also perceived a discontinuity between Late Period and Ptolemaic máchimoi and criticized the aforementioned traditional rendering of the latter group; historical documents mentioning Greek máchimoi during the Ptolemaic Period proves that they were not exclusively native Egyptians as usually thought, suggesting that the term was rather an indicator of their military role (for example, the pike-bearing máchimoi epilektoi or the mounted máchimoi hippeis) and/or of the amount of land received (a pentarouros, for example, was a máchimos granted with five arourai of land) and not of their ethnicity. In this regard, she accepts the idea that the máchimoi were the lowest level of the military hierarchy, but their socio-economic status was still higher than that of the average peasant.


== References ==


== Sources ==
Christelle Fischer-Bovet (2013), "Egyptian warriors: the Machimoi of Herodotus and the Ptolemaic Army". The Classical Quarterly 63 (01), pp. 209–36, doi:10.1017/S000983881200064X.
Werner Huß, Ägypten in hellenistischer Zeit: 332–30 v. Chr. Beck, München 2001, ISBN 3-406-47154-4, pp. 20–31; 47–53.