Peter Bullock (6 July 1937 – 5 April 2008) was a soil scientist whose initial work in the field of soil micromorphology preceded an interest in land degradation. His advocacy of the need to treat soil as a sustainable resource led to his appointment to the Intergovernmental Panel on Climate Change. Bullock contributed to the reports of the IPCC, which was awarded the Nobel Peace Prize in 2007.


== Life and career ==

Peter Bullock was born in Kinlet, Bridgnorth, Shropshire and attended Bridgnorth Grammar School. Bullock graduated from the University of Birmingham in 1958 with a BA in Geography. After graduating from Birmingham University he joined the Soil Survey of England and Wales (SSEW) as a surveyor, before completing a master's in agricultural chemistry at the University of Leeds. Awarded a Fulbright scholarship, he completed his doctorate at Cornell University and worked for the United States Department of Agriculture, before returning England in 1967. He took the post of head of the mineralogy section of the SSEW where he became a world expert in soil micromorphology. By the 1980s he was head of research, and was instrumental in the survey's transfer to Cranfield Institute of Technology and its continued existence in face of government threats of withdrawal of funding.

Appointed director of the Soil Survey and Land Research Centre, his involvement in national and international scientific and advisory bodies grew: his role as special advisor to the Royal Commission on Environmental Pollution's inquiry into the sustainable use of soil led to an invitation to join the Intergovernmental Panel on Climate Change (IPCC). He contributed to the reports of the IPCC, which was awarded the Nobel Peace Prize in 2007. He demonstrated the role played by soil in the Earth's ecosystem and the impact of climate change on land degradation. Bullock was contributing lead author, along with Henri Le Houérou, to chapter 4 ('Land Degradation and Desertification') of Working Group II's contribution to the Second Assessment Report of the IPCC. As emeritus professor of land resource management of Cranfield University, he was instrumental in the establishment of the World Soil Survey Archive and Collection (WOSSAC) and in the development of the Soil-Net educational resource.
Bullock was married to Patricia and had two children. He died of liver cancer in 2008.


== References ==

Hallett, Stephen (1 July 2008). "Obituary: Peter Bullock". The Guardian. Retrieved 8 February 2009. 
"Professor Peter Bullock". The Times. 19 May 2008. Retrieved 8 February 2009. 
Stoops, Em. Prof. Dr. G. (20 April 2008). "In memoriam Prof. Dr. Peter Bullock" (PDF). Purdue University Agronomy Dept. Retrieved 8 February 2009. 


== External links ==
Soil-Net
WOSSAC