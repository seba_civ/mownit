The Bureau of Indian Standards (BIS) is the national Standards Body of India working under the aegis of Ministry of Consumer Affairs, Food & Public Distribution, Government of India. It is established by the Bureau of Indian Standards Act, 1986 which came into effect on 23 December 1986. The Minister in charge of the Ministry or Department having administrative control of the BIS is the ex-officio President of the BIS.
The organisation was formerly the Indian Standards Institution (ISI), set up under the Resolution of the then Department of Industries and Supplies No. 1 Std.(4)/45, dated 3 September 1946. The ISI was registered under the Societies Registration Act, 1860.
As a corporate body, it has 25 members drawn from Central or State Governments, industry, scientific and research institutions, and consumer organisations. Its headquarters are in New Delhi, with regional offices in Kolkata, Chennai, Mumbai, Chandigarh and Delhi and 20 branch offices. It also works as WTO-TBT enquiry point for India.


== Association with international standards bodiesEdit ==
BIS is a founder member of International Organisation for Standardization (ISO)
It represents India in the International Organization for Standardization (ISO), the International Electrotechnical Commission (IEC) and the World Standards Service Network (WSSN).


== Standard formulation and promotionEdit ==
One of the major functions of the Bureau is the formulation, recognition and promotion of the Indian Standards. As on 31 August 2013, 19067 Standards formulated by BIS, are in force. These cover important segments of economy, which help the industry in upgrading the quality of their products and services.
BIS has identified 14 sectors which are important to Indian Industry. For formulation of Indian Standard, it has separate Division Council to oversee and supervise the work. The Standards are regularly reviewed and formulated in line with the technological development to maintain harmony with the International Standards.


== LaboratoriesEdit ==
To support the activities of product certification, BIS has a chain of 8 laboratories. These laboratories have established testing facilities for products of chemical, food, electrical and mechanical disciplines. Approximately, 25000 samples are being tested in the BIS laboratories every year. In certain cases where it is economically not feasible to develop test facilities in BIS laboratories and also for other reasons like overloading of samples, equipment being out of order, the services of outside approved laboratories are also being availed. Except for the two labs, all the other labs are NABL (National Accreditation Board for Testing and Calibration Laboratiories) accredited. It operates a laboratory recognition scheme also.


== Product Certification SchemeEdit ==
Product Certifications are to be obtained voluntarily. For, some of the products like Milk powder, Drinking Water, LPG Cylinders, Thermometers etc., certification is mandatory. Because these products are concerned with health and safety.


== For foreign manufacturersEdit ==
All foreign manufacturers of products who intend to export to India are required to obtain a BIS product certification license. Towards this, BIS launched its Product Certification Scheme for overseas manufacturers in the year 1999. Under the provisions of this scheme, foreign manufacturers can seek certification from BIS for marking their product(s) with BIS Standard Mark. If or otherwise, the foreign manufacturer has not signed an MoU with BIS, it has to set up a liaison office in India with the permission of Reserve Bank of India. Otherwise, an authorised representative or agent needs to be appointed by the foreign firm.


== Scheme for Indian importersEdit ==
Indian importers who intend to get Certification Mark may apply for the license. However, the assessment visit is paid to the original product manufacturer.


== Management System CertificationEdit ==
Management System Certification Scheme IS/ISO 9001.
Environmental Management System Certification Scheme IS/ISO 14001.
Occupational Health and Safety Management System Certification Scheme IS 18001.
Hazard Analysis and Critical Control Scheme IS/ISO 22000.
Service Quality Management System Certification Scheme IS 15700.


== National Institute of Training for Standardization (NITS)Edit ==
It is a training institute of BIS which is set up in 1995. It is functioning from Noida, Uttar Pradesh, India.
The primary activities of NITS are:-
In-House and Open Training Programme for Industry
International Training Programme for Developing Countries (Commonwealth countries)
Training Programme to its employees.


== Grievance CellEdit ==
If any customer reports about the degraded quality of any certified product at Grievance Cell, BIS HQs, BIS gives redressal to the customer.


== Small Scale Industry Facilitation CellEdit ==
SSI Facilitation Cell became operational since 26 May 1997. The aim of the Cell is to assist the small scale entrepreneurs who are backbone of the Indian industry. It has an incentive scheme to promote such units to get certified with ISI Mark.


== National Building Code of India, 2005Edit ==
It is a comprehensive building code for regulating the building construction activities across the country which came with the contribution of around 400 experts. The code was first published in 1970 at the instance of the Planning Commission of India.
Draft Addition Preliminary Draft Amendment No. 1 to NBC 2005 Part 11 Approach to Sustainability The BIS has put into circulation a preliminary draft amendment as Part-11 to NBC. The BIS will accept comments, reaction from people in large till 15 March 2013,[1]. See attachments Click to Download PDF


== Indian Standards Bill, 2015Edit ==
On June 17, 2015, the Union Cabinet chaired by Prime Minister Narendra Modi, gave its approval to introduce a new Bureau of Indian Standards Bill [of 2015] into the Parliament. The Bill was passed on 3 December 2015 by the Lok Sabha and on 8 March 2016 by the Rajya Sabha. The new Bill will repeal the existing Bureau of Indian Standards Act, 1986.
The main objectives of the proposed legislation are:-
To establish the Bureau of Indian standards(BIS) as the National Standards Body of India.
The Bureau to perform its functions through a governing council, which will consist of President and other members.
To include goods, services and systems, besides articles and processes under the standardization regime.
To enable the government to bring under the mandatory certification regime for such articles, processes or service which it considers necessary from the point of view of health, safety, environment, prevention of deceptive practices, consumer security etc. This will help consumers receive ISI certified products and will also help in prevention of import of sub-standard products.
To allow multiple types of simplified conformity assessment schemes including self-declaration of conformity (SDOC) against any standard which will give multiple simplified options to manufacturers to adhere to standards and get a certificate of conformity, thus improving the 'ease of doing business'.
To enable the Central Government to appoint any authority in addition to the Bureau of Indian Standards, to verify the conformity of products and services to a standard and issue certificate of conformity.
To enable the Government to implement mandatory hallmarking of precious metals articles.
To strengthen penal provisions for better effective compliance and enable compounding of offences for violations.
To provide recall, including product liability of products bearing the Standard Mark, but not conforming to relevant Indian Standards.
Repeal of the BIS Act of 1986.
The Bureau of Indian Standards Act 2016 received the assent of the President on 21 March 2016


== See alsoEdit ==
Certification marks in India
ISI mark
BIS hallmark
Agmark
FPO mark


== ReferencesEdit ==
Indian Institute of Architects, Northern Chapter See the Draft


== External linksEdit ==
Indian Standard Codes for Civil Engineering
IS codes for brickwork
IS codes for concrete work
IS codes for soil and Foundation engineering
IS codes for Measurements
BIS Official Website
www.StandardsBIS.in
mirror and remixing of the safety standards done by Public.Resource.Org