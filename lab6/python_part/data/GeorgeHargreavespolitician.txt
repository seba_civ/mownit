James George Hargreaves (born 14 July 1957), known as George Hargreaves or J. G. Hargreaves, is a religious minister, political campaigner, former leader of the Christian Party, and former music producer and songwriter.


== Early life ==
Of Trinidadian descent, Hargreaves grew up in Islington. He has said that as a child his life was saved by a firefighter who rescued him from his burning house. He was educated at King's College London, the University of Oxford, and at the University of St Andrews.


== Musical career ==
Hargreaves attended Woolverstone Hall, a boarding school owned by the Inner London Education Authority. While still at school, he formed a band named "Snap" with Tony Ajai-Ajagbe. The duo were signed by a local studio and released a single in 1974, but were dropped when it proved unsuccessful. They were later employed as songwriters by Jobete Music, and Hargreaves moved into producing.
In the 1980s, Hargreaves and Ajai-Ajagbe worked at Magnet Records. They wrote the theme tune for Pebble Mill at One and Hargreaves promoted pop acts including Sinitta, Yazz and Five Star. He also wrote and produced several disco records for Sinitta, including "So Macho" and "Cruising", which were popular in the gay community. He told Scotland on Sunday that "So Macho" was intended "...for women to dance round their handbags to and for the gay scene to go mad to on poppers" and that "I was never gay, but I had a lot of lovely friends in the gay scene."


== Religious and political activities ==

Hargreaves later worked as a DJ before moving to the Isle of Man as a tax exile. While living there, he decided to devote his life to Christianity. He became a Pentecostal minister and obtained a Post-graduate Diploma in Theology and a master's degree in Anthropology from the University of Oxford.
At the 1997 general election, Hargreaves stood as the Referendum Party candidate for Walthamstow. In 2002, he was a founder member of the Christian Peoples Alliance (CPA) and served as Acting Chair of its Hackney branch.
In 2004, Hargreaves was a founder of the East London Christian Choir School in Hackney, an independent school which used the Accelerated Christian Education programme. In the same year, he founded Operation Christian Vote as an alternative to the CPA. The party stood in every British region at the European election, 2004, focussing its campaign on opposition to abortion.
Hargreaves then stood for the party at the Birmingham Hodge Hill by-election, 2004, where he took only 90 votes. He fared better at the 2005 general election in Na h-Eileanan an Iar, where he took 7.6% of the votes cast and beat the Conservative Party candidate. Hargreaves was also involved with Christian Voice.
In 2006, Hargreaves formed the Scottish Christian Party, for which he stood in the Dunfermline and West Fife by-election, 2006, taking 1.2% of the vote.
The Scottish Christian Party regards homosexuality as a sin and campaigns against gay activism. Hargreaves personally funded the Employment Tribunals of nine firefighters who were suspended after refusing to distribute leaflets at a gay pride march. He was involved in protests against Jerry Springer: The Opera, claiming that "Jerry Springer proved the greatest rallying point for Christian activism in the past 10 years". The party had many other policies, including a proposal that Scottish criminals should be placed in jails in developing countries.
The Scottish Christian Party put up candidates in every region in the Scottish Parliament election, 2007. Hargreaves' movement was regarded by the rival Christian People's Alliance as a Pentecostal movement. At the election, he headed the party's list in the Glasgow electoral region, aiming in particular to unseat openly bisexual Scottish Green Party Member of the Scottish Parliament Patrick Harvie. Harvie had recently asked the police to investigate allegedly homophobic comments by the Archbishop of Glasgow and was described by Hargreaves as a "gay fundamentalist".
Hargreaves also founded the Welsh Christian Party to contest the Welsh Assembly election, 2007. He campaigned to replace the Flag of Wales with the Flag of Saint David, claiming that the red dragon on the Welsh flag was "nothing less than the sign of Satan".
Hargreaves stood for the Christian Party at the Haltemprice and Howden by-election, 2008, where he asked the Haltemprice and Howden electorate to use their vote to demand a referendum on the European Union, which he believes is the greatest threat to our civil liberties". He received 76 votes, coming 16th out of 26 candidates.
In August 2008, Hargreaves fronted the Channel 4 programme Make Me a Christian. He has been strongly criticised by many, including the magazine New Humanist, Charlie Brooker and Lucy Bannerman of the Herald.
He had planned to stand in the Western Isles in the 2010 general election, but withdrew his candidacy in February 2010 after his wife's cancer returned. However, he stood as a candidate for Barking in the 2010 general election and even shared a debate with Nick Griffin on Genesis TV.
In February 2015, Simon Gilbert of the Coventry Telegraph learned that UKIP had planned to select him to stand as a candidate for Coventry South in the 2015 general election. However, following pressure from local members as a result of the local media coverage, the party performed a U-turn and decided to keep their original candidate.


== Elections contested ==
UK Parliament elections
Scottish Parliament elections (Electoral Additional Region)
European Parliament elections


== References ==


== External links ==
J.G. Hargreaves discography