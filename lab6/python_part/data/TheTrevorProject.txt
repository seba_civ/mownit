The Trevor Project is an American non-profit organization founded in 1998 and the leading national organization focused on suicide prevention efforts among lesbian, gay, bisexual, transgender and questioning and other queer (LGBTQ+) youth. Through a toll-free telephone number, it operates "The Trevor Lifeline", a confidential service that offers trained counselors. The project also provides guidance and vital resources to parents and educators in order to foster safe, accepting and inclusive environments for all youth, at home and at school.


== History ==
The project was founded in 1998 in West Hollywood, California, by James Lecesne, Peggy Rajski, and Randy Stone. They are the creators of the 1994 Academy Award-winning short film Trevor, a dramedy about Trevor, a gay thirteen-year-old boy who, when rejected by friends because of his sexuality, makes an attempt to take his life. When the film was scheduled to air on HBO television in 1998, the filmmakers realized that some of the program's young viewers might be facing the same kind of crisis as Trevor, and began to search for a support line to be broadcast during the airing. They discovered that no such helpline existed, and decided to dedicate themselves to forming what was, in their view, a much-needed resource: an organization to promote acceptance of LGBTQ youth, and to aid in crisis and suicide prevention among that group.
The Trevor Lifeline was established with seed funds provided by The Colin Higgins Foundation and HBO's license fee. As a result, it became the first nationwide, around-the-clock crisis and suicide prevention helpline for LGBTQ youth. The project also provides online support to young people through the project's website, as well as guidance and resources to educators and parents.
In November 2009, the project was contracted by the Tulare County Suicide Prevention Task Force, located in Tulare County, California. With this agreement it the first time the project had received public funds. In June 2009, seven Tulare County volunteers completed The Trevor Project Lifeguard Workshop Facilitator training. Lifeguard workshops have been done in schools in Tulare County municipalities including Dinuba, Lindsay, Porterville and Visalia, as well as Hanford, located in adjacent Kings County.
The Trevor Project has been supported by a wide variety of celebrities, including Ellen DeGeneres, Kathy Griffin, Shay Mitchell, Daniel Radcliffe, Neil Patrick Harris, James Marsden, Chris Colfer, Kim Kardashian, Darren Criss, Dianna Agron, George Takei, Anderson Cooper and Tyler Oakley.


== Projects ==


=== The Trevor Lifeline ===

The Trevor Lifeline is the only nationwide, around-the-clock crisis and suicide prevention helpline for LGBTQ youth in the United States. The lifeline is a free and confidential service that offers hope and someone to talk to, 24/7. The lifeline's trained counselors listen and understand without judgment and can refer callers to supportive local organizations and groups; this information is also available on the project's website.
On August 10, 2009, the project announced that it received a major cash donation to support the lifeline from actor Daniel Radcliffe, star of the Harry Potter film series. Radcliffe said in a statement:

"I am very pleased to begin my support of the Trevor Project, which saves lives every day through its critical work. It's extremely distressing to consider that in 2009 suicide is a top-three killer of young people, and it's truly devastating to learn that LGBTQ youth are up to four times more likely to attempt suicide than their heterosexual peers."


=== Ask Trevor ===
"Ask Trevor" is an online non-time-sensitive question-and-answer resource for young people with questions surrounding sexual orientation and gender identity.


=== TrevorChat ===
TrevorChat is a free, confidential, live and secure online messaging service provided by The Trevor Project. TrevorChat is only intended to assist those who are not at risk for suicide.


=== TrevorSpace ===
TrevorSpace is an online social networking community for LGBTQ youth ages 13 through 24, along with their friends and allies. Youth can create personal profiles and connect with other young people throughout the country, as well as find resources within their communities. TrevorSpace is carefully monitored by administrators designated by the project to ensure all content is age appropriate, youth-friendly and factual. TrevorSpace links members to The Trevor Project's home page, where information about The Trevor Lifeline, "Dear Trevor," and other resources is available. The software used to create TrevorSpace was donated to the project by Tim Gill, an American software entrepreneur and philanthropist.


=== Palette Fund Internship Program ===
Through the Palette Fund Internship Program, the project provides for five internships in both its Los Angeles, California, and New York City, New York, offices. Interns are placed among the program, communication and development departments and provides an opportunity for young people to learn about working in the non-profit sector. Palette Fund internships are specifically designed to introduce young leaders to the LGBTQ movement.


=== Youth Advisory Council ===
The Youth Advisory Council serves as a liaison between youth nationwide and the project, as it relates to young people and the issues surrounding suicide, sexuality and gender identity. The council submits recommendations to the project in an effort to increase Trevor visibility and best serve the LGBTQ youth population.


=== School workshops ===
The project's Lifeguard Workshop Program uses a structured, age-appropriate curriculum to address topics around sexuality, gender identity, the impacts of language and behavior, and what it means for young people to feel different. The program also teaches young people to recognize depression and suicide amongst their peers, the impacts of language and behavior on LGBTQ youth, and suicide prevention skills in schools.


=== Tyler Oakley's fundraiser ===
On February 10, 2014, Tyler Oakley started a fundraiser to collect $150,000 to the Trevor Project. Oakley set the deadline for the money to be collected somewhere by the time of his birthday, March 22, but the goal was reached after only six days. Oakley later extended the fundraiser and more than $462,000 had been raised as of March 29, well above the intended amount. On March 31, the fundraiser was finalized, reaching a total of $525,754. Oakley's initiative was praised by many people and organizations worldwide.


== Awards ==
The Trevor Project uses annual events to honor individuals and businesses that have been leaders in supporting LGBT rights and advocated against bullying and hate crimes. In 1998, they held the first Cracked Xmas in Los Angeles. In 2001, a New York City-based annual event was added using entertainers to raise financial resources for the organization. As of 2009, Cracked Xmas was the group's largest annual fundraiser.
Trevor Hero Award
2005 - Tony Kushner
2006 - Michael Cunningham
2007 - Nathan Lane
2008 - Alan Cumming
2009 - Dustin Lance Black
2010 - Vanessa Williams and Daniel Radcliffe
2011 - Lady Gaga
2012 - Katy Perry
2013 - Cindy McCain & Jane Lynch
2014 - Robert Greenblatt & Arianna Huffington
Trevor Life Award
The Trevor Life Award honors a person who is an inspiration to LGBTQ youth. Recipients include:
2002 - Armistead Maupin
2003 - Rosie O'Donnell
2004 - Debra Messing, and Megan Mullally
2005 - Marc Cherry
2006 - Roseanne Barr
2007 - Ellen DeGeneres
2008 - Sigourney Weaver
2009 - Neil Patrick Harris
2010 - Kathy Griffin
Trevor Hope Award
The Trevor Hope Award is presented to businesses that have demonstrated support for the LGBTQ community and "has increased the visibility and understanding of LGBT issues." It was first presented in 2004. Recipients include:
2004 - Wells Fargo
2005 - LPI Media
2006 - HBO
2007 - Clear Channel for its commitment to LGBTQ causes including "its ranking as a 'best place to work' for LGBTQ employees by the Human Rights Campaign's Corporate Equality Index."
2008 - Lifetime
2009 - AT&T
2010 - Levi Strauss & Co.
Trevor Commitment Award/Trevor 2020 Award
Awarded to businesses that support LGBT rights, started in 2007.
2007 - Bravo
2008 - MTV's The N (now called TeenNick)
2009 - CNN
2010 - Macy's
2011 - Google
2012 - MTV
2014 - Yahoo!
Trevor Youth Innovator Award
Awarded to LGBTQ or straight people under the age of 25 who work to support, inspire, and empower LGBTQ youth. Recipients include:
2013 - Adam White
2014 - Tyler Oakley
2014 - Skylar Kergil


== See also ==

Athlete Ally
List of LGBT-related organizations
Suicide among LGBT youth
Tyler Oakley


== References ==


== External links ==
thetrevorproject.org, The Trevor Project's official website
Trevor at the Internet Movie Database, the 1994 short film that inspired the creation of The Trevor Project