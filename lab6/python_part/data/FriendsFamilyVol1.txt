Friends & Family, Vol. 1 was an album containing various artists' material including Suicidal Tendencies and Infectious Grooves. It was released by Suicidal Records in 1997.
It was sub-titled "Epic Escape" because Suicidal Tendencies – whose singer Mike Muir runs Suicidal Records – had exited the Epic Records label on which they had released much of their late 1980s and early 1990s material.
The Suicidal Tendencies tracks, "Scream Out" and "We Are Family", were eventually re-recorded and ended up on their next album, Freedumb in 1999.
The Cyco Miko track, "Big Fat Baby", was a re-recording of a track called "Lost My Brain (Once Again)" on the 1995 album of the same name.


== Track listing ==


== Credits ==


=== Suicidal Tendencies and Infectious Grooves ===
Mike Muir – vocals
Mike Clark – guitar
Dean Pleasants – guitar
Josh Paul – bass
Brooks Wackerman – drums


=== Cyco Miko ===
Mike Muir – vocals
Adam Siegel – guitar
Dave Kushner – guitar
Dave Silva – bass
Greg Saenz – drums


=== The Funeral Party ===
Claudia Ashton – vocals
Fiendly – guitar and keyboards
Dosage – guitar
Splinters – bass
Sam Pokeybo – drums


=== Creeper ===
Mike "Milkbone" Jensen – vocals
Mike Clark – guitar
Michael Alvarado – drums
@&*% – bass


=== Musical Heroin ===
The Freakazoid Twins
Freaky Deaky – all instruments
Deaky Freaky – all instruments


=== All tracks ===
Recorded at Titan Studios
Tracks 1 – 6 produced by Suicidal Tendencies
Tracks 9, 10, 13, and 14 produced by The Freakazoid Twins
All songs engineered and additional production by Michael Vail Blum
Executive produced by Albert Rouillard
Tracks 1 – 5 mixed by Paul Northfield
All other songs are rough mixes
Mastered by Brian Gardner at Bernie Grundman Mastering


== External links ==
Suicidal Tendencies official website