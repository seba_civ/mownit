Francis Raymond "Frank" Baker, OBE (born 27 January 1961) is a British diplomat and civil servant. Since 2014, he has been Ambassador to Iraq. From 2010 to 2014, he was Ambassador to Kuwait.


== Early life ==
Baker was born on 27 January 1961 to Raymond and Pamela Baker. He was educated at Dartford Grammar School, then an all-boys state grammar school in Dartford, Kent.


== Diplomatic career ==
Baker joined the Foreign and Commonwealth Office (FCO) in 1981. For the first two years of his career, he worked in the Personnel Operations Department. From 1983 to 1986, he undertook his first posting abroad as Third Secretary at the British Embassy in Panama City, Panama. From 1986 to 1991, he was based at the British Embassy in Buenos Aires, Argentina; first he was Third Secretary, and later he served as Second Secretary. In 1991, he returned to England, and was a desk officer (ranking as a Second Secretary) in the Human Rights Policy Department of the FCO for two years. From 1993 to 1996, he was First Secretary at the British Embassy in Ankara, Turkey; he was awarded an OBE for this posting.
From 1996 to 1998, Baker was on secondment to the US Government and was based in Washington, D.C.. In 1998, he was Head of the Iraq Section in the Middle East Department, FCO. Then, from 1998 to 2000, he was Private Secretary to the Minister of State for Foreign Affairs. From 2000 to 2003, he was Head of the Africa Department (Equatorial) in the FCO. From 2003 to 2007, he was once more based in Washington, D.C.; first, between 2003 and 2004, as Counsellor (Political/Military), and then, between 2004 and 2007, as Counsellor (Foreign and Security Policy). From 2002 to 2010, he was back working at the FCO, this time as Deputy Director Middle East.
Baker took up his first ambassadorial appointment in 2010. From February 2010 to July 2014, he was the British Ambassador to the State of Kuwait. In August 2014, he was announced as the next British Ambassador to the Republic of Iraq. He took up the appointment in September 2014.


== Personal life ==
Baker is married to Maria Pilar Fernandez. Together, they have two children; a boy and a girl.


== Honours ==
In the 1997 Queen's Birthday Honours, Baker was appointed an Officer of the Order of the British Empire (OBE) in recognition of his service as First Secretary of the British Embassy in Ankara, Turkey. He is a recipient of the Order of Kuwait (Special Class).


== References ==