Sri Lanka ( Sinhalese:ශ්‍රී ලංකා Sri lank Tamil:இலங்கை Ilaṅkai) officially the Democratic Socialist Republic of Sri Lanka is an Island in the Indian Ocean which has been known under various names over time. Under British colony, the island was known as Ceylon from 1815-1948.


== Lanka ==
Lanka was the classical name bestowed on the island by the indian epic Ramayana. A possible theory is that the name Lanka is derived from the tamil word "ilanku" (இலங்கு), which means "to shine" or "to glitter", thus means Lanka "that which glitters". It earned this name on account of the glittering of the gold and gems found on its surface.
Another theory states that the word Lanka simply means any island. It is still widely used by the aborigines of Central and Eastern India to mean an island and especially an islet in a river. The word is considered as belonging to Austro-asiatic languages. The Veddas, the aborigines of Sri Lanka who are Austro-Asiatic in origin, might have rendered the name Lanka to the island. As it is the biggest island in the South Asian context, Lanka probably became an exclusive term for it.
Lak-vaesiyaa in Sinhala means an inhabitant of the island of Lanka. Lak-diva in E'lu (old Sinhala) means the island of Lanka.Another traditional Sinhala name for Sri Lanka was Lakdiva, with diva also meaning "island". A further traditional name is Lakbima. Lak in both cases is derived again from Lanka. The same name was adopted in Tamil as Ilankai, the Tamil language commonly adds /i/ before initial /l/, especially in borrowed words.
The name of Sri Lanka was introduced in the context of the Sri Lankan independence movement, pushing for the independence of British Ceylon during the first half of the 20th century. The name was up of the Marxist Lanka Sama Samaja Party, foundet in 1935. The Sanskrit honorific Sri was introduced in the name of the Sri Lanka Freedom Party (Sinhalese: ශ්‍රී ලංකා නිදහස් පක්ෂය Sri Lanka Nidahas Pakshaya), founded in 1952. The Republic of Sri Lanka was officially adopted as the country's name with the new constitution of 1972, and changed to "Democratic Socialist Republic of Sri Lanka" in the constitution of 1978.


== Ceylon, Serendip and related names ==
 The name Ceylon has a complicated history going back to antiquity. Deriving from the Old-Tamil word Cerentivu which means literary "Island of Cheras", a dravidian term for nagas meaning "hill", the Island was through the word Cerentivu known by the Roman as Serendivis and by the Arabs as Serendip. From this, the Greeks called the island Sielen Diva. From the word Sielen, many European forms were derived: the Sinhalese : Sinhalee Latin Seelan, Portuguese Ceilão, Spanish Ceilán, French Selon, Dutch Zeilan, Ceilan and Seylon, and of course the English Ceylon. Ptolemy called the Island Salike, and the inhabitants Salai.
Sri Lanka have was also been known as Helabima. Helabima means "Land of Helas", which is a name that Sinhalese are called. Siṃhāla is attested as a Sanskrit name of the island of Ceylon (Sri Lanka), comparatively late, in the medieval Bhagavata Purana and Rajatarangini, but it must date to antiquity because of its reflection in the names for the island in Greco-Roman geography. The name is sometimes glossed as "abode of lions", and attributed to a supposed former abundance of lions on the island.


== Eelam ==
The Tamil name is Eelam (Tamil: ஈழம், īḻam; also spelled Eezham, Ilam or Izham).
The etymology of the name is uncertain. The most favoured explanation derives it from a word for the spurge (palm tree), via the application to a caste of toddy-drawers, i.e. workers drawing the sap from palm trees for the production of palm wine. The name of the palm tree may conversely be derived from the name of the caste of toddy drawers, known as Eelavar, cognate with the name of Kerala, from the name of the Chera dynasty, via Cheralam, Chera, Sera and Kera.
The stem Eela is found in Prakrit inscriptions dated to 2nd century BC in Sri Lanka in personal names such as Eela-Barata and Eela-Naga. The meaning of Eela in these inscriptions is unknown although one could deduce that they are either from Eela a geographic location or were an ethnic group known as Eela. From the 19th century onwards, sources appeared in South India regarding a legendary origin for caste of toddy drawers known as Eelavar in the state of Kerala. These legends stated that Eelavar were originally from Eelam.
There have also been proposals of deriving Eelam from Simhala. Robert Caldwell (1875), following Hermann Gundert, cited the word as an example of the omission of initial sibilants in the adoption of Indo-Aryan words into Dravidian languages. The University of Madras Tamil Lexicon, compiled between 1924 and 1936, follows this view. Peter Schalk (2004) has argued against this, showing that the application of Eelam in an ethnic sense arises only in the early modern period, and was limited to the caste of "toddy drawers" until the medieval period.


== Taprobana, Tamraparni ==

Tamraparni is according to some legends the name given by Prince Vijaya when he arrived on the island. The word can be translated as "copper-colored leaf", from the words Thamiram (copper in Sanskrit) and Varni (color). Another scholar states that Tamara means red and parani means tree, therefore it could mean "tree with read leaves". Tamraparni is also a name of Tirunelveli, the capital of the Pandyan kingdom in Tamil Nadu. The name was adopted in Pali as Tambaparni.
The name was adopted into Greek as Taprobana, called by Megasthenes in the 4th century BC. The Greek name was adopted in medieval Irish (Lebor Gabala Erenn) as Deprofane (Recension 2) and Tibra Faine (Recension 3), off the coast of India, supposedly one of the countries where the Milesians / Gaedel, ancestors of today's Irish, had sojourned in their previous migrations.
The name remained in use in early modern Europe, alongside the Persianate Serendip, with Traprobana mentioned in the first strophe of the Portuguese national epic poem Os Lusíadas by Luís de Camões. John Milton borrowed this for his epic poem Paradise Lost and Miguel de Cervantes mentions a fantastic Trapobana in Don Quixote.


== See also ==
Place names in Sri Lanka


== References ==


== External links ==
Ancient Names of Sri Lanka