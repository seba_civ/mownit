The San Gabriel slender salamander (Batrachoseps gabrieli) is a species of salamander. It has a worm-like body, a large head and large limbs, and an elongate cylindrical tail of less than 1.5 times its body length. An adult salamander is between 3 and 5 cm long. It has a black dorsum with white, coppery, and orange blotches, and an immaculate black venter. It may have red spots on tail.
B. gabrieli is similar to the related species B. pacificus and B. nigriventis.
This species is only known from the San Gabriel Canyon system, and typically lives above 1,000 meters in the San Gabriel Mountains of Los Angeles County.


== References ==
Hammerson (2004). Batrachoseps gabrieli. 2006. IUCN Red List of Threatened Species. IUCN 2006. www.iucnredlist.org. Retrieved on 10 May 2006. Database entry includes a range map and a brief justification of why this species is listed as data deficient
This article is based on a description from "A Field Guide to the Reptiles and Amphibians of Coastal Southern California", Robert N. Fisher and Ted J. Case, USGS, http://www.werc.usgs.gov/fieldguide/index.htm.