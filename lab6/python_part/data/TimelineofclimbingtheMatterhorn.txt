
== Timeline ==


=== 1857 ===
July: First attempt (from Breuil) by Jean-Antoine Carrel, Jean-Jacques Carrel, and Amé Gorret.


=== 1858 ===
August: Attempt by J. A. and J. J. Carrel. The Grande Tour is reached (3800 m).


=== 1860 ===
July 2: Attempt from Zermatt (East face) by Charles, Alfred and Sandbach Parker (c. 3500 m)
August: Attempt by John Tyndall, Vaughan Hawkins, J. J. Carrel (3960 m)


=== 1861 ===
July 4: Second attempt by the Parker brothers (c. 3570 m)
August 29: J. A. and J. J. Carrel. The Crête du Coq is reached (4032 m)
August 29–30: First attempt by Edward Whymper and Swiss guide (Lion ridge)


=== 1862 ===
January: First winter attempt (from Zermatt East face) by Thomas Stuart Kennedy guided by Peter Perren and Peter Taugwalder (c. 3350 m)
July 7–8: Attempt by E. Whymper, Reginald J. S. MacDonald, Johann zum Taugwald, Johann Krönig, Luc Meynet (3657 m)
July 9–10: Attempt by E. Whymper, R. J. S. MacDonald, J. A. Carrel, Pession (3960 m)
July 18–19: Solo attempt by E. Whymper (4084 m)
July 23–24: Attempt by E. Whymper, J. A. Carrel, César Carrel, L. Meynet (4008 m)
July 25–26: Attempt by E. Whymper, L. Meynet (4102 m)
July 27–28: Attempt by J. Tyndall, Johann Joseph Bennen, Anton Walters, J. A. Carrel, C. Carrel. The Shoulder is reached (Pic Tyndall, 4258 m)


=== 1863 ===
August 10–11: Attempt by E. Whymper, J. A. Carrel, C. Carrel, L. Meynet and two porters (4047 m)


=== 1865 ===
June 21: Attempt from the south east face (Furggen couloir) by E. Whymper, Michel Croz, Christian Almer, Franz Biner, L. Meynet (3414 m)
July: Attempt by J. A. Carrel, C. Carrel, Charles Gorret, Jean-Joseph Maquignaz. Pic Tyndall, 4258 m
July 14: First ascent (Hörnli ridge) by E. Whymper, Francis Douglas, Charles Hudson, Douglas Hadow, M. Croz, P. Taugwalder (father), Peter Taugwalder (son). Death of Douglas, Hudson, Hadow and Croz on the descent.
July 17: 2nd ascent: First ascent from the Italian side by J.-A. Carrel and Jean-Baptiste Bich. A. Gorret, of the 1857 attempt, and Jean-Augustin Meynet stop just short of the summit.


=== 1867 ===
August 15: 3rd ascent. Second ascent from Breuil by J. A. Carrel, J. B. Bich, and Salomon Meynet guiding Florence Crauford Grove.
September 13: 4th ascent: First direct ascent of the Lion ridge as it is climbed today by Jean-Joseph and Jean-Pierre Maquignaz.
October 1–3: 5th ascent: From Breuil. J. J. and J. P. Maquignaz, C. Carrel and François Ansermin guiding William Leighton Jordan.


=== 1868 ===
July 25: 6th ascent: Second ascent via the Hörnli ridge by Josef Marie Lochmatter and Peter Knubel guiding Julius Elliot.
July 28: 7th ascent: First traverse of the summit (Lion and Hörnli ridges) by J. Tyndall, J. J. and J. P. Maquignaz.
August 4: 8th ascent Second traverse of the summit (Hörnli and Lion ridges) by J. J. Maquignaz, Victor Maquignaz and Elie Pession guiding François Thioly and O. Hoiler.
August 3–4: 9th ascent: P. Knubel, Hans Baumann, Peter Bernett guiding George Edward Foster.
August 8: 10th ascent: J. M. Lochmatter, P. Knubel and Niklaus Knubel guiding Paul Güssfeldt.


=== 1869 ===
July 20: 15th ascent: J. A. Carrel, J. B. Bich, Alphonse Payot and Michel Payot guiding James Eccles.
August 26: 16th ascent: Ascent of the Lion ridge by Joseph, Pierre and Emmanuel Maquignaz and B. Bich guiding Robert Boothby Heathcote; it was on this occasion that the guides fixed at the last bit the rope ladder which was called the Echelle Jordan, from the name of its donor.


=== 1871 ===
July 22: First ascent by a woman: Lucy Walker reached the summit with her father Frank Walker and Frederick Gardiner, guided by Heinrich and Melchior Anderegg, N. Knubel, P. Knubel and P. Perren.
September 5: First traverse by a woman: Meta Brevoort with W.A.B. Coolidge guided by Chr. Almer, Ulrich Almer and N. Knubel.


=== 1872 ===
July 26: Traverse Breuil-summit-Zermatt in 18 hours by J. J. Maquignaz and Anton Ritz guiding J. Jackson.


=== 1876 ===
July 23: First ascent without mountain guides by John Brise Colgrove, Albert Harold Cawood and Arthur Cust.


=== 1879 ===
September 3: First ascent via the Zmutt ridge by Albert F. Mummery, Alexander Burgener, Johann Petrus and Augustin Gentinetta.One hour later William Penhall, Ferdinand Imseng and Louis Zurbrücken finished the first ascent (partially) over the west face (significantly overlapping the Zmutt Ridge route)

September 6: Second ascent of the Zmutt ridge by Baumann with guides J. Petrus and Émile Rey.


=== 1880 ===
July 16: A. Burgener, Benedikt Venetz and A. F. Mummery attempted the first ascent of the Furggen ridge. At the level of the Swiss Shoulder they were forced to traverse along the east face to the Swiss ridge, to climb to the summit by the ordinary route.


=== 1882 ===
March 16: First winter ascent (Lion ridge) by J. A. Carrel, J. Baptiste Carrel and Louis Carrel guiding Vittorio Sella. Descent via the Hörnli ridge (17th).


=== 1889 ===
Ascent by Achille Ratti (later Pope Pius XI)


=== 1895 ===
August 25: Luigi Amedeo, Duke of the Abruzzi, A. F. Mummery and J. Norman Collie made the ascent with the guide Josef Pollinger.
August 31: First descent of the Zmutt ridge by Matthias Zurbriggen and J. Pollinger guiding Lily Bristow.


=== 1898 ===
First solo ascent (Hörnli ridge) by Wilhelm Paulcke.


=== 1906 ===
September 1: First solo ascent of the Zmutt ridge by Hans Pfann.


=== 1911 ===
September 9: First ascent via the Furggen ridge by Mario Piacenza with guides Jean-Joseph Carrel and Joseph Gaspard.
January 31: First winter ascent via the Hörnli ridge by Charles Francis Meade with the guides Josef Lochmatter and J. Pollinger.


=== 1923 ===
August 12: Attempt on the north face by Alfred Horeschowsky and Franz Piekielko. The summit was reached via the Hörnli ridge.


=== 1928 ===
August: Attempt on the north face by Kaspar Mooser and Victor Imboden.


=== 1931 ===
August 1: First ascent of the north face by the brothers Franz and Toni Schmid.
October 15: First ascent of the south face by Enzo Benedetti with guides Louis Carrel and Maurice Bich.


=== 1932 ===
September 19: First complete ascent of the east face by Enzo Benedetti and Giuseppe Mazzotti with guides Louis and Lucien Carrel, M. Bich and Antoine Gaspard.


=== 1934 ===
Paul Petzoldt traversed the Matterhorn and then retraced his route over the summit on the same day.


=== 1936 ===
First winter solo ascent via the Hörnli ridge by Giusto Gervasutti.


=== 1948 ===
March 25: First winter ascent via the Zmutt ridge by Henri Masson and Edmund Petrig.


=== 1950 ===
August 19: First ascent by a cat (Hörnli ridge). It climbed up entirely unaided, but made its descent in a guide's rucksack.


=== 1959 ===
July 22: First solo ascent of the north face by Dieter Marchart.


=== 1961 ===
Fast ascent by Ian Angell in 3 hours and 25 minutes.


=== 1962 ===
February 4: First winter ascent of the north face by Hilti von Allmen and Paul Etter.
August 13: First complete ascent of the west face by Renato Daguin and Giovanni Ottin. All faces and ridges have been completely ascended


=== 1965 ===
February 22: In winter and alone, Walter Bonatti climbs a direct new route on the north face, the "North Face Direct". Bonatti set off on February 18, accompanied by three friends in order to simulate an ordinary day of alpine skiing. He prepared himself in secret behind a boulder, then for the next four days struggled against solitude, cold and technical difficulties.
July 13: Yvette Vaucher, climbing with her husband Michel Vaucher, becomes the first woman to climb the Matterhorn's north face.


=== 1969 ===
Ascent of the north face by Jean Troillet in 4 hours and 10 minutes.


=== 1970 ===
August Attempted ascent of the Swiss side of the Matterhorn by Japanese climbers Michio Oikawa and Masayuki Kobayashi who disappeared. Remains found in Sept 2014 and identified in August 2015


=== 1971 ===
December 23: First winter ascent via the south face by Arturo Squinobal and Oreste Squinobal.


=== 1975 ===
February 28: First winter ascent via the east face by René Arnold, Guido Bumann and Candide Pralong.


=== 1977 ===
February 16: First winter solo ascent of the Schmid route on the north face by Tsuneo Hasegawa.


=== 1978 ===
January 11: First winter ascent of the west face by Rolando Albertini, Marco Barmasse, Innocenzo Menabreaz, Leo Pession, A. Squinobal, O. Squinobal and Augusto Tamone. Death of Rolando Albertini.


=== 1990 ===
Mountain guide Ulrich Inderbinen makes his 370th and last ascent of the Matterhorn, before his 90th birthday.


=== 1992 ===
August 20: Hans Kammerlander and Diego Wellig climb the Matterhorn four times in 23 hours and 26 minutes. The route they followed was: Zmutt ridge–summit–Hörnli ridge (descent)–Furggen ridge–summit–Lion ridge (descent)–Lion ridge–summit–Hörnli ridge (descent)–Hörnli ridge–summit–Hörnli Hut (descent).


=== 1994 ===
March 11: Second ascent of the North Face Direct (Bonatti route) by Catherine Destivelle (started on 8th).


=== 1995 ===
August 17: Bruno Brunod sprints up and down the Lion ridge from Breuil, ascending and descending 2469 vertical meters in 3hrs and 14 minutes.


=== 2007 ===
September 6: Mountain guides Simon Anthamatten and Michael Lerjen, both of Zermatt, ascended and descended the Hörnli ridge in a record retour (combined time of ascent and descent) of 2 hours and 33 minutes (ascent: 1 hour and 40 minutes, descent: 53 minutes).


=== 2009 ===
January 13: Ueli Steck climbs the classic Schmid route on the north face in 1 hour and 56 minutes.
June: Swiss mountain guide Jean Troillet climbs a 600 meters new route on the north face.


=== 2011 ===
August 23: Andreas Steindl runs 2915 vertical meters from Zollhaus, Zermatt, up the Matterhorn over the Hörnli ridge in a record time of 2 hours and 57 minutes.


=== 2013 ===
August 21: Kilian Jornet sprints up and down the shortest ridge of the Matterhorn in 2 hours and 52 minute, breaking Brunod's Lion ridge run from Breuil by 22 minutes 


=== 2015 ===
April 22: Dani Arnold climbs the classic Schmid route on the North Face in a one-hour and 46 minutes, 10 minutes faster than Ueli Steck in 2009.


== References ==

Helmut Dumler and Willi P. Burkhardt, The High Mountains of the Alps, London: Diadem, 1994