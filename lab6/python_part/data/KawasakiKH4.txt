The Kawasaki KH-4 was a light utility helicopter produced in Japan in the 1960s as a development of the Bell 47 that Kawasaki had been building under licence since 1952. The most visible difference between the KH-4 and its forerunner was its new and enlarged cabin. This was fully enclosed (although the side doors were removable) and provided seating for three passengers side-by-side on a bench seat behind the pilot's seat. The helicopter was provided with a new control system, revised instrumentation, and larger fuel tank.
A total of 211 KH-4s were built, including four that were modified from existing Bell 47Gs. The vast majority of these were bought by civil operators, although some were purchased by the military forces of Japan and Thailand.


== Operators ==
 Japan
Japan Maritime Self-Defense Force
 Thailand
Thai Air Force


== Specifications ==

Data from Jane's All The World's Aircraft 1966–67
General characteristics
Crew: One pilot
Capacity: 3 passengers
Length: 9.93 m (32 ft 7¼ in)
Main rotor diameter: 11.32 m (37 ft 1½ in)
Height: 2.84 m (9 ft 4 in)
Main rotor area: 100.6 m2 (1,083 ft2)
Empty weight: 816 kg (1,800 lb)
Gross weight: 1,293 kg (2,850 lb)
Powerplant: 1 × Lycoming TVO-435-B1A horizontally opposed six cylinder, 200 kW (270 hp) each
Performance
Maximum speed: 169 km/h (105 mph)
Cruising speed: 140 km/h (87 mph)
Range: 400 km (248 miles)
Endurance: 4 hours  6 min
Service ceiling: 5,640 m (18,500 ft)
Rate of climb: 4.3 m/s (850 ft/min)


== References ==

Taylor, John W. R. (1966). Jane's All The World's Aircraft 1966–67. London: Sampson Low, Marston & Company. 
Taylor, Michael J. H. (1989). Jane's Encyclopedia of Aviation. London: Studio Editions. p. 557. 
Simpson, R. W. (1998). Airlife's Helicopters and Rotorcraft. Ramsbury: Airlife Publishing. pp. 123–25.