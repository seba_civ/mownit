Rupert Hughes (January 31, 1872 – September 9, 1956) was an American novelist, film director, Oscar-nominated screenwriter, military officer, and music composer. He was the brother of Howard R. Hughes, Sr. and uncle of billionaire Howard R. Hughes, Jr. His three volume scholarly biography of George Washington broke new ground in demythologizing Washington and was well received by historians. A staunch anti-Communist, in the 1940s he served as president of the American Writers Association, a group of anti-Communist writers.


== Early life ==
Hughes was born on January 31, 1872 in Lancaster. Rupert spent his early years in the Lancaster area until age seven when the family moved to Keokuk, Iowa, where his father established a successful law practice.
Rupert Hughes' first published a poem while still a child growing up in Lancaster. After receiving his basic public education in Keokuk and at a private military academy near St. Charles, Missouri, Rupert Hughes attended Adelbert College in Ohio, earning a B.A. in 1892 and M.A. in 1894. Originally intending a career teaching English Literature, Hughes later attended Yale University, earning a second M.A. degree in 1899.


== Career ==


=== As writer ===

By the time of his Yale degree Rupert Hughes had already given up the idea of a staid life in academia for a new career as an author. His first book, 1898's The Lakerim Athletic Club, came from a serialized magazine story for boys. Hughes often blurred the lines of job description in his early years, working at various times as a reporter for the New York Journal and editor for various magazines including Current Literature, all the while continuing to write short stories, poetry, and plays.
His first published novel not originally serialized elsewhere was The Whirlwind, published in 1902. Believed to be partly influenced by wartime adventures of his father, the book was set in Civil War-era Missouri.
Hughes moved to London, England in 1901 where he edited The Historians' History of the World, then returned to New York City to help edit the Encyclopædia Britannica from 1902 to 1905.
Some of Rupert Hughes most notable early writing involved music. His American Composers (1900), Love Affairs of Great Musicians (1903), Songs by Thirty Americans and Music Lovers' Cyclopedia (1914) were all well received by the public and critics alike. Hughes was a musician and composed several songs including ones for his first venture as a playwright, the musical comedy The Bathing Girl (1895). In recognition of his musical efforts Hughes was elected an honorary member of the Alpha chapter of Phi Mu Alpha Sinfonia music fraternity at the New England Conservatory in Boston in 1917.
In addition to novels, Rupert Hughes was a prolific writer of short stories, with varying numbers well over one hundred credited to him. In a Little Town (1917) allowed Hughes to draw on his small-town roots with fourteen short stories about fictionalized people around Keokuk. In 1920 Harper published Mama and other Unimportant People, a collection of short stories and novelettes which contained the critically acclaimed short story The Stick-in-the-Muds Also in the collection was The Father of Waters, which would be designated as, and republished in, The World's 50 Best Short Novels, a ten-volume compilation published by Funk & Wagnalls in 1929.
Hughes' was an essay writer for popular magazines in the 1930s and endorsed the Technocracy movement "Technocracy to the Rescue" Rupert Hughes.
George Washington
In January 1926, Hughes was asked to speak at a meeting of the Sons of the American Revolution in Washington D.C.. During the speech he advocated for more truth in the portrayal of the nation's first President, pointing out such fables as chopping down a cherry tree, and drawing from Washington's own diary to illustrate some of the man's more human, if less savory, traits and activities. Some in the crowd heckled Hughes during his speech and later gave a disingenuous report of its content to a newspaper. The story rapidly spread across America, with the misquoted Hughes lambasted by everyone from newspaper editors to religious figures and temperance leaders coast-to-coast.
Hughes began the first of a projected four-volume biography of Washington in October 1926. Based on extensive research, George Washington: The Human Being and the Hero covered his life up to the age of thirty. Volume two, George Washington: The Rebel and the Patriot (1927), examined Washington's life prior to and in the early years of the American Revolution from 1762 to 1777. The third volume, George Washington: Savior of the States, 1777–1781 (1930) further examined Washington as a military leader during some of the revolutions darkest days. Hailed by historians as a groundbreaking work, it repaired much of the damage done to Hughes reputation. An intended fourth volume covering George Washington and his role as the first President of the USA was never completed.


=== Playwright ===
Rupert Hughes first foray into the tough world of New York City theater was a failure. In 1895, with financial backing from his father, Hughes and a business partner staged the aforementioned The Bathing Girl at the Fifth Avenue Theater. However it lasted only one performance. He persevered however, and between 1902 and 1909 no less than six Hughes-penned plays were staged by touring companies across the United States and in London, England. Rupert Hughes cast is second wife, Adelaide Mould Bissell alongside a young Douglas Fairbanks in his first New York theater role in the 1908 production All for a Girl. His 1909 play The Bridge, starring Guy Bates Post, ran in New York for a respectable thirty-three performances before going on tour for three years. Hughes' next effort, 1910's Two Women starred the famed stage actress Leslie Carter made forty-seven performances before also touring extensively. Excuse Me, a comedy farce based on a train trip, premiered in February, 1911 and was one of the years biggest hits in New York that year. It would tour world-wide, including Australia, and later twice be made into movies. A stage version of the novel Tess of the Storm Country followed, and in 1920 Hughes' final play, The Cat Bird, starring John Drew, Jr.. In 1921 his novel The Old Nest (1912), based upon his family and early life, was adapted into a movie. Its success led Hughs to move to Hollywood and join the burgeoning motion picture industry in 1923. The behind-the-scenes goings on of show business provided ample fodder for Hughes novel, 1922's Souls for Sale, a scathing look at Hollywood scandals of the era. Rupert Hughes greatest success in Hollywood came in 1928 when he was nominated for an Academy Award for Best Original Screenplay for The Patent Leather Kid. In the 1940s he served as president of the American Writers Association, a group of anti-Communist writers.


=== Military service ===
Rupert Hughes enlisted in the New York National Guard as a private in 1897, serving in the famous 69th New York regiment, the "Fighting 69th". When President Woodrow Wilson sent U.S. troops to Mexico in 1916 in pursuit of bandit Pancho Villa, Hughes, now a Captain, and the 69th were one of the regiments assigned to the mission. With America's entry into World War I the following year Rupert Hughes expected to see service in France, but a slight hearing impairment prevented him from overseas duty and he was assigned to work in Military Intelligence in Washington, D.C. in early 1918 and promoted to Major. While still a Captain, Hughes designed and patented a new type of trench knife for use by the U.S. Army. Containing a spring-loaded blade that extended via push-button, it was similar to what would later be considered a "switchblade". The Hughes Trench Knife was evaluated as a potential military arm by a panel of U.S. Army officers from the American Expeditionary Force (AEF) in June 1918. Unfortunately, after testing the board found the Hughes design to be of no value, and it was never adopted. Hughes remained on active duty until mid-1919, meanwhile continuing his writing career in off-duty hours.
Rupert Hughes continued his part-time military career after moving to California, joining the state militia. He was a key member of its reorganization in 1940 into the California Army National Guard and as Colonel commanded one of its regiments from 1941 to 1943. At age 71 and with health becoming frail, Colonel Hughes was passed over for service in a combat zone again and retired from military service.


== Personal life ==

His first marriage, to Agnes Wheeler Hedge in 1893, ended in divorce in 1903. However the couple did have one child, daughter Elspeth, born in 1897. His second marriage, to actress Adelaide Manola Mould Bissell took place in 1908. She starred in his stage production All for a Girl that same year. However, several years later, in December, 1923, she died of an apparent suicide while on tour in Hai Phong, French Indochina. Rupert Hughes final marriage, to Elizabeth Patterson Dial, took place the next year, 1924. The third Mrs. Hughes however, died from an accidental overdose of sleeping pills in 1945. Hughes daughter from his first marriage died a few months later.
Rupert Hughes health began to fail in the late 1940s, leading to a non-fatal stroke in 1953. He suffered a fatal heart attack while working at his desk on September 9, 1956. Rupert Hughes is buried in Forest Lawn Memorial Park, Glendale in suburban Los Angeles, California.


== Works ==
The Dozen from Lakerim" (1899), New York, The Century Co.
Famous American Composers (1900)
The Biographical Dictionary of Musicians (1903)
Excuse Me! (1911), novel
What will people say? (1914), novel, New York, Harper & Brothers
The Last Rose of Summer (1914), novel, New York, Harper & Brothers
We can't have everything! (1917), novel, New York, Harper & Brothers
In a Little Town" (1917), 14 short stories, New York, Harper & brothers.
The Cup of Fury, a novel of cities and shipyards" (1919), New York, Harper & brothers
Within These Walls (1923)
Destiny (1925), novel
George Washington: The Human Being and the Hero (1926)
Washington 1789---1933 Roosevelt, article from Cosmopolitan March (1933)
The Complete Detective (1950)
The Triumphant Clay (1951), novel
The War of the Mayan King (1952, his final novel)
The Love Affairs of Great Musicians, Volume 1 and Volume 2
Hughes wrote and directed the silent film Reno (1923). His short story "The Mobilization of Johanna" was filmed as Johanna Enlists (1918). His "Don't Call Me Madame" was filmed as Tillie and Gus (1933). Another one of his stories was filmed as Miss Fane's Baby Is Stolen (1934).


== References ==


== External links ==
Rupert Hughes' rebuttal of the John Gano baptism of George Washington legend in Time magazine on 26 September 1932.
Works by Rupert Hughes at Project Gutenberg
Works by or about Rupert Hughes at Internet Archive
Works by Rupert Hughes at LibriVox (public domain audiobooks) 
Rupert Hughes at IMDB