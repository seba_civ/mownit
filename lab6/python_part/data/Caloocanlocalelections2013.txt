Local elections in Caloocan City was held on May 13, 2013 within the Philippine general election. The voters elected for the elective local posts in the city: the mayor, vice mayor, the two Congressmen, and the councilors, six in each of the city's two legislative districts.


== Mayoral and vice mayoral election ==
The Incumbent Mayor Enrico Echiverri is term-limited and ineligible for reelection, and running for Congress instead, His son League of the Barangay National President RJ Echiverri is his party's nominee. RJ is running under Liberal Party with his running mate former vice mayor Luis Varela who serve as vice mayor 2001-2010.
1st Dist. Rep.Oscar Malapitan, who term-limited and ineligible for reelection, announce his bid in mayoralty race under the UNA and his running mate is businessman Antonio "Nani" Almeda.
Other for mayor is Independent candidates Macario Asistio, Jr. who became mayor from 1988 until 1995 and Diony Guillarte. Other candidate for Vice Mayor is Rey Malonzo, who became vice-mayor of the city from 1992 until 1995 then mayor from 1995 until 2004, Councilor Maca Asistio who run under PMP and Independent Mariano Yu


== Congressional Election ==

There will be two candidates for the congressman or district representative post of each of the districts of Caloocan City. The city is divided into two congressional districts: the first district and the second district.
The Incumbent First District Representative Oscar Malapitan is term-limited and running for mayoral race instead. Oscar Malapitan son incumbent Coun. Dale Gonzalo "Along" Malapitan, is the UNA candidate for 1st district congressional seat. He will facing off with incumbent Mayor Enrico "Recom" Echiverri who term-limited in Mayor and candidate of Liberal. Other is the Following Independent Candidate, Roberto Guanzon who became representative from 1995 until 1998, Maria Hernando, Milagros Libuton, Sandro Limpin, Imelda Pengson and Sirgea Villamayor.
For the second district, The incumbent Rep.Mitzi Cajayon is running under NUP bolted from Lakas-CMD, her opponents is Luis Asistio who became 2nd District representative from 1992 until 2001 and 2001 until 2004 and running under NPC. another is incumbent Vice Mayor Edgar Erice who became representative from 2001-2004 and running under Liberal. Other candidates is Carlos Cabochan of Kapatiran and independent Adoracion Garcia.


== Results ==


=== Mayor ===


=== Vice mayoral election ===


=== Philippine House of Representatives elections ===


=== 1st District ===


=== 2nd District ===
Cajayon is the incumbent.


=== City Council elections ===
Each of Caloocan City's two legislative districts elects six councilors to the City Council. The six candidates with the highest amount of votes wins the seats per district. Some who are running are the same names from 2010.


==== Administration coalition (Team RJ) ====


==== Primary opposition coalition (Team Oca) ====


==== Results ====


=== 1st District ===


=== 2nd District ===


== External links ==
Official website of the Commission on Elections
Official website of National Movement for Free Elections (NAMFREL)
Official website of the Parish Pastoral Council for Responsible Voting (PPCRV)