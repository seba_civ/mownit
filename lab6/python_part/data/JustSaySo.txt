"Just Say So" is the lead single released from Irish singer-songwriter Brian McFadden's third studio album, Wall of Soundz. The single was released on 9 April 2010, and features American singer-songwriter, Kevin Rudolf. The video for the song was shot in The Ivy Nightclub in Sydney, Australia in March 2010. The video was done in one shot, and lip-synced backwards, to allow for McFadden to still be in sync while the video goes backwards.


== Track listing ==
Australian Digital Download
"Just Say So" (Featuring Kevin Rudolf) - 3:13 (Brian McFadden, Robert Conley)


== Critical reception ==
The song received mostly positive reviews from critics. Access All Areas stated "Just Say So is a ridiculously catchy electro-pop track with heavy auto tune and dance beats." Tim Byron of The Vine gave it a mixed review saying "So it's not very surprising that the backing music on Just Say So sounds almost entirely synthesised; synth drums, synth bass and synthy keyboards. The AutoTune vocal effect on McFadden's vocals is so prominent that it not only destroys anything distinctive in the voice, but also makes the lyrics hard to hear. Kraftwerk wish their music could sound this mechanical."


== Chart performance ==
"Just Say So" debuted at No. 1 on the ARIA Singles Chart and stayed in that position for three weeks. It spent seven weeks in the ARIA top ten and thirteen weeks in the top 50 and gained a platinum accreditation. The song became McFadden's second number one single in Australia.


=== Year-end charts ===


== References ==