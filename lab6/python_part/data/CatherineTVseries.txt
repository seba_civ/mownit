Catherine is a 2014 Russia-1 television mini-series starring Marina Aleksandrova as Russian empress Catherine the Great. It tells the story of princess Sophie Friederike Auguste who later came to power and become Empress of Russia following a coup d'état and the assassination of her husband, Peter III. Russia was revitalized under her reign, growing larger and stronger than ever and becoming recognized as one of the great powers of Europe.
It was premiered on November 24, 2014 on Channel Russia-1
During the broadcast, the series held the top spot TV Rating of films and TV series in Russia and became one of the most popular TV series in Russian Federation.


== Plot summary ==
In 1745 in Saint Petersburg, Empress Elizabeth is childless, and she appoints her nephew Peter Fedorovich as the heir to Russian throne. But what it means to leave the country in his hands? As he was born in Prussia and could hardly speak Russian.
Elizabeth decides to marry him with a princess, and when he would have a son she planned to take the boy to educate him to become the future emperor. Elizabeth chooses a wife for her nephew, German Princess Sophie Friederike Auguste von Anhalt-Zerbst-Dornburg. Sophia Frederike is sent to a distant and foreign land hoping to find happiness, but faced with the intrigues and plots of the Russian Imperial yard, the indifference of the spouse and the diabolical plans of the Empress. The girl took the name Catherine Alexeevna (Yekaterina) and takes everything into her own hands to save herself and her children from the deadly danger, as the emperor Peter III desires to send her from the palace.
Soon, in July 1762, barely six months after becoming emperor, Peter commits the political error of retiring with his Holstein-born courtiers and relatives to Oranienbaum, leaving his wife in Saint Petersburg. On July 8 and 9, the Leib Guard revolted, deposed Peter from power, and proclaimed Catherine the new monarch. The bloodless coup d'état succeeds and Catherine becomes the new empress, and thus the Catherinian Era and the Golden Age of the Russian Empire has begun.


== Cast ==


== See also ==
Catherine The Great, Channel One Russia's version


== References ==


== External links ==
Official Website