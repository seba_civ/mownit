Albertadromeus is an extinct genus of orodromine thescelosaurid ornithopod dinosaur known from the upper part of the Late Cretaceous Oldman Formation (middle Campanian stage) of Alberta, Canada. It contains a single species, Albertadromeus syntarsus.


== Etymology ==
The composite term Albertadromeus is derived from the name of the Canadian province "Alberta", and the Greek word dromeus (δρομεύς) meaning "runner", a reference to its inferred cursorial nature; thus "runner from Alberta". The specific name, syntarsus is derived from Greek words "syn" (συν) meaning "together" and "tarsus" (ταρσός) meaning "ankle", hence "together-ankle" a reference to the condition where its distal fibula is fused to its distal tibia. This dinosaur was described and named by Caleb Marshall Brown, David C. Evans, Michael J. Ryan & Anthony P. Russell in 2013 and the type species is Albertadromeus syntarsus.


== Description ==
The holotype specimen of Albertadromeus TMP 2009.037.0044 consists of two dorsal vertebrae, a caudal vertebra, cervical ribs, ossified tendons, the left tibia and fibula, an incomplete right fibula, and a fragmentary metatarsal and ungual. The skull is unknown in this genus. The authors note that despite the few bones recovered, their moderate quality of preservation nonetheless provides enough morphological detail to allow for diagnosis on the species level. Its elongate tibia has been strongly correlated with cursorial habits. The tibia of Albertadromeus is slightly more elongate than that of Orodromeus, and significantly more elongate than that of Parksosaurus (ROM 804), Thescelosaurus (USNM 7757 and RSM P 1225.1), Hypsilophodon (NMHUK R5830), Dryosaurus (YPM 1876), and Stegoceras (UALVP 002). Alberadromeus was a small-bodied, bipedal, cursorially adapted (built to run) ornithopod.


=== Diagnosis ===
According to Brown et al. (2013) Albertadromeus can be distinguished based on the following characteristics:
The distal fibula is reduced to a thin sheet of bone that is fused to the anterior surface of tibia for the distal one-third of its length (shared with heterodontosaurids)
The lateral condyle of proximal tibia is strongly bilobed (shared with Changchunsaurus, Eocursor, Hypsilophodon, Jeholosaurus, Lesothosaurus, Orodromeus, Oryctodromeus, Stormbergia)
A prominent cnemial crest that projects dorsally (and anterolaterally), resulting in a posterior sloped dorsal extremity of the tibia (also present in Gasparinisaura and Micropachycephalosaurus).


== Classification ==
Brown et al. (2013) erected a new taxon Orodrominae to differentiate those species that are more closely related to Orodromeus from those more closely related to Thescelosaurus. The validity of this new taxon is supported by the following: (a) the foramen magnum is between 20% and 30% of the width of the occipital condyle; (b) the pubis is articulated with a sacral rib; (c) there is a sharp and pronounced scapular spine; and (d) the fibular shaft is ‘D’-shaped in cross-section. The new taxon Orodrominae includes the newly discovered dinosaur Albertadromeus, Zephyrosaurus, Orodromeus and Oryctodromeus. Phylogenetic analyses suggest that Albertadromeus forms a clade with Zephyrosaurus and Orodromeus, with Oryctodromeus being less closely related. The following is a cladogram based on the phylogenetic analysis conducted by Brown et al. in 2013, showing the relationships of Albertadromeus:


== Paleoecology ==


=== Provenance and occurrence ===
The remains of the type specimen of Albertadromeus was recovered in the Canal Creek locality in the Upper Oldman Formation, which is part of the Belly River Group in Alberta, Canada. The specimen was collected in a two-meter-thick fine to very fine sandstone unit that was deposited during the Campanian stage of the Cretaceous period, approximately 77 to 76 million years ago. This specimen is housed in the collection of the Royal Tyrrell Museum of Palaeontology in Drumheller, Alberta.


=== Fauna and habitat ===
Studies suggest that the paleoenvironment of the Oldman Formation was an ancient coastal plain. This formation has produced the remains of the theropods Saurornitholestes, Daspletosaurus, Troodon, and Dromaeosaurus, the ceratopsids Albertaceratops, Chasmosaurus, Anchiceratops, and Coronosaurus, the hadrosaurids Brachylophosaurus, Gryposaurus, Parasaurolophus, and Corythosaurus, as well as other dinosaurs that shared that their paleoenvironment with Albertadromeus.


== References ==