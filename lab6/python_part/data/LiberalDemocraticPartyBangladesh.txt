The Liberal Democratic Party (Bengali: লিবারেল ডেমোক্রেটিক পার্টি) is a political party in Bangladesh.
Liberal Democratic Party formed on October 26, 2006 by former President of Bangladesh Dr. A. Q. M. Badruddoza Chowdhury, Dr. Oli Ahmad Bir Bikram, and 24 other former Members of Parliament and ministers from the Bangladesh Nationalist Party (BNP). The party was formed after abolishing 'Bikolpo Dhara'. Within 2 months of its formation, the party was threatened with partition by some senior leaders who were against the decision of the party forming a coalition with Awami League. Senior Presidium members of LDP were also unhappy over some allegations of financial corruption with election nominations process and being forced to accept the son of party President Dr Badruddoza Chowdhury as a Presidium member. Soon after the rift forced Dr. B. Chowdhury to find his own way with his followers back to Bikalpa Dhara again. Thereafter, Dr. Oli Ahmad was elected as President and Former Speaker Sheikh Razzak Ali as Executive President of the party.
During the army backed caretaker government regime of 2007-2008 most of the senior politicians were either in jail (on corruption & various other charges) or escaped abroad or voluntarily resigned from politics to avoid prosecution. As a testimonial to his honesty & integrity, Dr. Oli Ahmad was the only senior statesman of Bangladesh who didn't have to face any such disgraceful situation at the time. He also had the courage to point out & challenge any misdeeds & mistakes of the then government.
From its inception LDP was a strong ally of the Grand Alliance. However, after grand alliance leader Sheikh Hasina Wajed back tracked on her earlier promises on the number of Parliamentary seats to be shared, LDP came out of the grand alliance and contested the 2008 elections on their own. In the absence of main opposition, the Four Party Alliance, LDP effectively became the voice of opposition in Parliament. Often Dr. Oli Ahmad would have to carry out the responsibilities of opposition leader - including delivering closing speech with the Prime Minister on conclusion of each parliamentary session, including budget sessions.
With the misrule of Grand Alliance it became very difficult for LDP to continue supporting the government, however, when caretaker government system was abolished it became impossible. In keeping with his nature of protesting any misdeed, Dr. Oli Ahmad BB has lent support to the major opposition in their demands for holding free and fair elections under caretaker government. Currently the LDP under leadership of Dr. Oli Ahmad BB has joined the 18 Party Alliance to revive democracy in Bangladesh.
Consequently, the AL government has taken various oppressive measures against him & his party to muffle their voice. Dr. Oli Ahmad BB removed as the Chairman of Parliamentary Standing Committee for Ministry of Planning. Despite Oli Ahmad being the elected representative from Chittagong-13 constituency, all development work in that area were to be supervised by a selected female MP from another constituency. 7 litigations were initiated against him, including charges brought under explosive substance act and causing arson in a KPI area. He was not given bail in one such fabricated case and was sent to jail for 16 days. A life-threatening attack was carried out on him by AL hooligans also. Furthermore, many cases were lodged against leaders, activists and supporters of LDP. Hundreds of them were jailed in such politically motivated cases.


== Leadership ==
President - Dr. Oli Ahmad Bir Bikram MP
Secretary General - Dr. Redwan Ahmed (Former State Minister & MP)


== Alliances ==
From 2006 to 2008, LDP worked closely with Grand Alliance. With Four Party Alliance abstaining from parliament, LDP effectively played the role of opposition in parliament. However, after caretaker government system was abolished unilaterally by the AL government, LDP felt that possibility of free and fair election in the future was threatened. 2010 onwards, LDP started working closely with the Four Party Alliance to ensure strengthening of democratic process. Eventually 18 Party Alliance was formed with LDP as a senior member.


== Electoral Performance ==
Since formation, LDP has faced one national election which was in 2008. Without association with the major two alliances at the time LDP is the only party to gain a seat in the Parliament independently. This limited success was mainly due to the betrayal of Grand Alliance leadership in not keeping their earlier promises.
Whereas, for the elections of 2007, LDP candidates where nominated for 28 seats (this election was postponed by the caretaker government). However, by 2008 the Four Party Alliance was considerably weakened, and Grand Alliance leader Sheikh Hasina was not inclined to share so many seats with LDP anymore. This betrayal was made clear only 40 days ahead of the election date, which gave LDP leadership very little time to prepare and strategise for elections on their own. President of LDP Dr. Oli Ahmed Bir Bikram won for the sixth time from Chittagong-13.
LDP has had some success in local government also, with a few elections for Municipality Mayor, Upazilla Chairman, Union Chairman and Ward Councilors being elected by popular vote.


== Associated Organisation ==
1. Democratic Freedom Fighter’s Party.
2. Democratic Youth Party.
3. Democratic Religious Leaders Party.
4. Democratic Women’s Party
5. Democratic Cultural Party
6. Democratic Farmer’s Party
7. Democratic Volunteer’s Party
8. Democratic Youth Women Party
9. Democratic Student Party


== Organisation in Foreign Country ==
1. Liberal Democratic Forum


== References ==


== See also ==
Dr. Oli Ahmad Bir Bikram