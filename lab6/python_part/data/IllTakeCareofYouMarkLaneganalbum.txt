I'll Take Care of You is the fourth solo album by former Screaming Trees vocalist Mark Lanegan. This album consists of cover songs.
It features Lanegan's interpretation of songs from a wide variety of songwriters, including Tim Rose, Tim Hardin, Booker T. Jones, and Buck Owens.
I'll Take Care Of You also includes the traditional song "Little Sadie."
A review from NME said: "it's probably his finest, most tenderly-delivered work to date." [1]
Pitchfork Medias Neil Lieberman said this about the album: "in this collection, Lanegan's managed to tug on the timeless threads that hold the patchwork of American music together. And that's certainly something to consider"[2]
The title song was featured in an episode of Veronica Mars's third season entitled "There's Got To Be A Morning After Pill".


== Track listing ==
"Carry Home" - 3:00 (Jeffrey Lee Pierce)
"I'll Take Care of You" - 2:50 (Brook Benton)
"Shiloh Town" - 3:22 (Tim Hardin)
"Creeping Coastline of Lights" - 3:20 (Leaving Trains)
"Badi-Da" (Fred Neil) - 3:21
"Consider Me" - 3:49 (Eddie Floyd, Booker T. Jones)
"On Jesus' Program" - 2:45 (Overton Vertis Wright)
"Little Sadie" - 3:23 (Traditional)
"Together Again" - 2:34 (Buck Owens)
"Shanty Man's Life" - 3:12 (Stephen Harrison Paulus)
"Boogie Boogie" - 2:04 (Tim Rose)


== Personnel ==
Mark Lanegan - Vocals
Mike Johnson - Guitar
Steve Berlin - Organ, flute, piano
Mark Boquist - Drums
Greg Calbi - Mastering
Van Conner - Bass
Brett Eliason - Engineer
Martin Feveyear - Organ, percussion, piano, producer, engineer, mixing, wurlitzer
Mark Hoyt - Guitar (acoustic), guitar (electric), vocals
Jeff Kleinsmith - Layout design
David Krueger - Violin
Barrett Martin - Percussion, vibraphone
Mark Pickerel - Drums
Ben Shepherd - Bass
Chris Takino - A&R
Stanford Wilson - Photography


== References ==