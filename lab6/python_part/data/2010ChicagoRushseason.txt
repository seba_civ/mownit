The 2010 Chicago Rush season was the tenth season for the franchise in the Arena Football League. The team was coached by Mike Hohensee and played their home games at Allstate Arena. For the 10th consecutive season, the Rush qualified for the playoffs. In the conference semifinals, they lost to the Milwaukee Iron, 54–64.


== Standings ==
y - Clinched divisionx - Clinched playoff berth


== Regular season schedule ==
The Rush opened their season on the road against the Barnstormers on April 2. Their first home game of the season will take place on April 9 as they host the Gladiators. They concluded their regular season in Week 17 visiting the Shock on July 23, one week earlier than the rest of the league, as the Rush had their second of two bye weeks in Week 18.
All times are EDT


== Playoff schedule ==
All times are EDT


== Regular season ==


=== Week 1: at Iowa Barnstormers ===

In the league's season opener, the Rush were the first to score when DeJuan Alfonzo intercepted a pass and took it 9 yards back for a touchdown. The Rush held a 17–0 lead after the 1st quarter. The 2nd quarter saw more of the two teams trading scores, but the Rush still led the game at halftime 34–22. The Rush were the only team to score in the 3rd quarter, a 14-yard touchdown pass from Russ Michna to Allen Turner. The Barnstormers were able to cut the Rush's lead to 10 points in the 4th quarter, but Chicago was able to close them out, winning the game by a final score of 61–43.
The Chicago defense forced six turnovers, including four on Iowa's first four possessions of the game. Quarterback Russ Michna threw for 261 yards and 6 touchdowns. Samie Parker led in the receiving department with 104 yards and 2 touchdowns. Josh Ferguson and Nygel Rogers each had two interceptions.


=== Week 2: vs. Cleveland Gladiators ===

The entire game was closely contended, with the score tied at the end of each of the first three quarters, and neither team led by more than a touchdown all night. With 45 seconds left, and the game tied 56–56, quarterback Russ Michna led the Rush on a 7-play, 33-yard drive that ended with a 12-yard field goal by Chris Gould to take the lead with two seconds in the game. After a touchback following the ensuing kickoff, the Gladiators completed a pass, but failed to even get the ball past midfield, ending the game. Michna finished with 315 passing yards, completing 22 of 27 passes with 8 touchdowns.


=== Week 3: at Arizona Rattlers ===


=== Week 4: vs. Utah Blaze ===

The Rush improved to 4–0 by defeating the winless Blaze at home. Utah took the ball first and scored a touchdown, but it was the only time they would lead in the game. On their first play from scrimmage, the Rush found the end zone on a 34-yard passing touchdown. When Utah got the ball again, the Rush's Clifton Smith blocked a field goal attempt, and then scored a touchdown on a 1-yard run on Chicago's ensuing drive. At halftime, the Rush led 28–14. They received the ball to begin the 3rd quarter and marched up the field for a touchdown, scored on a 3-yard running play. Chicago attempted an onside kick following this score, but it went out of bounds, putting the Blaze 10 yards away from the end zone. Utah only needed one play to score another touchdown and cut their deficit to 35–20, but the Rush did not allow the game to get any closer the rest of the night, winning 63–40.
Russ Michna threw for 330 yards, a season high, and 7 total touchdowns. For the second consecutive week, Nichiren Flowers was the leading receiver with 129 yards and 3 touchdowns. Chicago held Utah to 40 points, a season-best for the Rush, and the defense stopped three fourth-down attempts by the Blaze offense.


=== Week 5: BYE ===


=== Week 6: at Milwaukee Iron ===

At halftime, the game was tied at 27–27, but the Rush allowed 34 points in the 3rd quarter alone, a deficit that proved to large to overcome, resulting in the first loss of the season for Chicago. After giving up a touchdown to Milwaukee on the opening drive of the 3rd quarter, the Rush turned the ball over on four consecutive possessions. Two of those giveaways were passes by Russ Michna that were intercepted and returned for touchdowns, while another was a fumble recovered by the Iron in the end zone. At the start of the 4th quarter, the Rush were in a 61–34 hole. Losing by a final score of 71–48, it was the most points the Rush had given up in a single game since 2004. The team's 6 turnovers tied a franchise record set in 2001. Michna's 354 passing yards surpassed the season-high he had totaled in the previous week, and had six touchdowns in the game, four in the air and two on the ground. Kenny Higgins had 157 yards and 2 touchdowns on 9 catches, leading all receivers.


=== Week 7: vs. Iowa Barnstormers ===


=== Week 8: at Dallas Vigilantes ===


=== Week 9: at Cleveland Gladiators ===


=== Week 10: vs. Jacksonville Sharks ===


=== Week 11: at Utah Blaze ===


=== Week 12: vs. Milwaukee Iron ===


=== Week 13: vs. Orlando Predators ===


=== Week 14: at Tampa Bay Storm ===


=== Week 15: vs. Arizona Rattlers ===

After a close win against Arizona, the Rush clinched a playoff berth.


=== Week 16: vs. Dallas Vigilantes ===


=== Week 17: at Spokane Shock ===


=== Week 18: BYE ===


== Playoffs ==


=== National Conference semifinals: at Milwaukee Iron ===


== References ==