The Zamagna (in Italian; Zamanja, Džamanjić or Zamanjić in Serbo-Croatian) was one of the noble families (post-Roman patriciates) of the Republic of Ragusa.


== History ==


=== Middle Ages ===
The Zamagna received patrician status in 1172. According to Ragusan sources, the family had moved to Ragusa from Zahumlje (Chelmo), and had once hailed from Apulia in southern Italy.(Zamagna, di Chelmo Balislavo. ixiti di Puglia, venuti 1172) The name of the progenitor of the family, Balislavo, shows a Slavic origin.


=== Early modern period ===
Marino Zamagna (Croatian: Marin Zamanja; fl. 1533)
Bernardo Zamagna (Croatian: Brno Zamanja; 1735-1820)


== Džamanjić in Austria ==
The aristocracy was known as nobili and was given to the following members of the family in 10 November, 1 and 16 December 1817, for Luko, Marijana, Matija, Frano Matej, and Matej, after the fall of the Republic at the hands of the Austrian Empire.


== References ==


== Sources ==
Adelslexikon des österreichischen Kaisertums 1804-1918 p,212 (4758-4759-4760), Peter Frank-Döfering
Der Adel in Kärnten, Krain und Dalmatien" J.Siebmacher großes Wappenbuch Band 29 Dalmatiner Adel, page 23 and 90.
Heinrich Graf Džamanjić, k u k LinSchiffLt YG Pula, Gefürstete Grafschaf Görz u. Gradiska Küstenland mit Triest, High-Life-Almanach : Adressbuch der Gesellschaft Wiens und der österreichischen Kronländer.1913, p. 151
Nikola Zamanjić, k k BezObKmsr = Zadar, Königreich Dalmatien, High-Life-Almanach : Adressbuch der Gesellschaft Wiens und der österreichischen Kronländer.1913, p. 389