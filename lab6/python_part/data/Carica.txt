Carica is a genus of flowering plants in the family Caricaceae including C. papaya, the papaya (syn. C. peltata, C. posoposa), a widely cultivated fruit tree native to the American tropics.
The genus was formerly treated as including about 20-25 species of short-lived evergreen pachycaul shrubs or small trees growing to 5–10 m tall, native to tropical Central and South America, but recent genetic evidence has resulted in all of these species other than C. papaya being reclassified into three other genera.


== Species ==
Carica papaya (Papaya)


=== Species Previously in the Genus ===
Most of the other species have been transferred to the genus Vasconcellea, with a few to the genera Jacaratia and Jarilla, as follows:
Carica baccata = Vasconcellea microcarpa subsp. baccata
Carica candamarcensis = Vasconcellea cundinamarcensis (Mountain papaya)
Carica candicans = Vasconcellea candicans (Mito)
Carica caudata = Jarilla heterophylla
Carica cauliflora = Vasconcellea cauliflora
Carica cestriflora = Vasconcellea cundinamarcensis
Carica chilensis = Vasconcellea chilensis
Carica crassipetala = Vasconcellea crassipetala
Carica cundinamarcensis = Vasconcellea cundinamarcensis
Carica dodecaphylla = Jacaratia spinosa
Carica glandulosa = Vasconcellea glandulosa
Carica goudotiana = Vasconcellea goudotiana
Carica heterophylla = Vasconcellea microcarpa subsp. heterophylla
Carica horovitziana = Vasconcellea horovitziana
Carica longiflora = Vasconcellea longiflora
Carica mexicana = Jacaratia mexicana
Carica microcarpa = Vasconcellea microcarpa
Carica monoica = Vasconcellea monoica
Carica nana = Jarilla nana
Carica omnilingua = Vasconcellea omnilingua
Carica palandensis = Vasconcellea palandensis
Carica parviflora = Vasconcellea parviflora
Carica pentagona = Vasconcellea ×heilbornii (Babaco)
Carica pubescens = Vasconcellea pubescens (Mountain papaya or Chilean Carica)
Carica pulchra = Vasconcellea pulchra
Carica quercifolia = Vasconcellea quercifolia
Carica sphaerocarpa = Vasconcellea sphaerocarpa
Carica spinosa = Jacaratia spinosa
Carica sprucei = Vasconcellea sprucei
Carica stipulata = Vasconcellea stipulata
Carica weberbaueri = Vasconcellea weberbaueri


== Notes ==


== References ==
Germplasm Resources Information Network: Carica
Germplasm Resources Information Network: Carica species list and synonymy
Aradhya, M. K. et al. (1999). A phylogenetic analysis of the genus Carica L. (Caricaceae) based on restriction fragment length variation in a cpDNA intergenic spacer region. Genet. Resources Crop Evol. 46: 579–586.
Badillo, V. M. (2000). Carica L. vs. Vasconcella St. Hil. (Caricaceae) con la rehabilitacion de este ultimo. Ernstia 10: 74–79.
Van Droogenbroeck, B. et al. (2002). AFLP analysis of genetic relationships among papaya and its wild relatives (Caricaceae) from Ecuador. Theoret. Appl. Genet. 105: 289–297.


== External links ==
IUCN Red List entry
 "Carica". Encyclopedia Americana. 1920.