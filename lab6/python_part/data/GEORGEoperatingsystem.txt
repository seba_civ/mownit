GEORGE was the name given to a series of operating systems released by International Computers and Tabulators (ICT) in the 1960s, for the ICT 1900 series of computers. These included GEORGE 1, GEORGE 2, GEORGE 3, and GEORGE 4.
Initially the 1900 series machines, like the Ferranti-Packard 6000 on which they were based, ran a simple operating system known as executive which allowed the system operator to load and run programs from a Teletype Model 33 ASR based system console.
In December 1964 ICT set up an Operating Systems Branch to develop a new operating system for the 1906/7. The branch was initially staffed with people being released by the end of work on the OMP operating system for the Ferranti Orion. The initial design of the new system, named George after George E. Felton head of the Basic Programming Division, was based on ideas from the Orion and the spooling system of the Atlas computer.
(In public it was claimed that George stood for GEneral ORGanisational Environment, but contemporary sources say that was a backronym).
In July 1965 a team from ICT was present at a seminar at NPL describing the CTSS operating system developed for MIT's Project MAC. They decided that the ICT would need to provide multi-access facilities, known to ICT as MOP, "Multiple Online Processing". In November 1965 H. P. Goodman, head of the Operating Systems Branch attended the Fall Joint Computer Conference in Las Vegas where plans for Multics were initially described. Some of the Multics features discussed influenced future development of George, notably the tree structured filestore.
Towards the end of 1965 ICT marketing requested that a simpler operating system be made available quickly, especially for the smaller members of the range. It was decided that two smaller systems, known as George 1 and George 2 be released rapidly, and the larger operating system was renamed George 3.


== GEORGE 1 & 2 ==
George 1 was a simple batch processing system, Job descriptions were read from cards or paper tape which controlled the loading and running of programs, either loaded from cards or paper tape or magnetic tape. The job control language allowed definition of the peripherals and files to be used and handling of exception conditions. The job description would be checked for errors before the job was run. George used the trusted program facilities provided by executive to run the user programs.
George 2 added the concept of off line peripheral handling (spooling). Several different modules, running in parallel, allowed overlapping of input, processing and output operations:
Jobs were read from cards or paper tape to temporary files on magnetic disk or tape by an input module.
A central module executed the user programs, taking input from the temporary input files and writing program output to temporary files.
An output module wrote the temporary output files to physical printers and punches.
A module was also available for entering jobs from remote job entry stations, the output of the job could be printed on the remote printer.
If the installation was large enough multiple copies of the central module could be run, allowing multiple jobs to be processed in parallel.
The George 2 job control language allowed use of stored macros with conditional facilities.
George 2 provided no file system, the system and user programs relied on the facilities provided by executive. Files on disk were accessed by unique 12 character names and no security other than a "do not erase" bit was provided.
MINIMOP could be run simultaneously with GEORGE 2 on the same machine, to provide on-line time-sharing facilities.


=== Example George 2 batch job ===
Here is a, somewhat artificial, example batch for George 2:
The batch starts with a job description which specifies a job name, the account code used by George for billing and a user name:

JOB PLAN4JOB,30174,BRIAN

The job first loads the program #XPLT from a disk file named PROGRAM COMP (XPLT is the assembler). The document SOURCE is used as input to #XPLT on a virtual card reader CR0.

IN ED(PROGRAM COMP)
LOAD #XPLT
IN CR0(SOURCE)
ENTER 1

If #XPLT finishes with the message HALT OK then the job continues at label 1A, otherwise the job displays COMPILATION ERRORS and jumps to 5END.

AT HALTED OK,GO TO 1A
DISPLAY 'COMPILATION ERRORS'
GO TO 5END

At label 1A the program #XPCK is loaded and run with an in-line document available on its virtual card reader. (XPCK is the linker, or "consolidator" in ICL terminology). (The in-line document is the text between the line IN CR0/JD and the terminator ???*).

1A IN ED(PROGRAM COMP)
LOAD #XPCK
IN CR0/JD
*IN ED(SEMICOMPILED)
*OUT ED(PROGRAM TEST)
*LIST
???*
ENTER 1
AT DELETED HH,GO TO 2A
DISPLAY 'CONSOLIDATION ERRORS'
GO TO 5END

If #XPCK finishes without error then the program #HLWD is run.

2A IN ED(PROGRAM TEST)
LOAD #HWLD
ENTER 0
5END END
****

After the job a source document is read in, this will be used as input to the job.

DOC SOURCE
PROG(HWLD)
STEER(LIST,OBJECT)
OUTE(SEMICOMPILED(0))
WSF(HWLD)
PLAN(CR)
#PRO           HWLD40/TEST
#LOW
MESS           12HHELLO WORLD
#PRO
#ENT           0
      DISTY    '11/MESS'
      DEL      2HOK
#END
ENDPROG
****

Finally the end of batch is signaled. At this point all the jobs in the batch will be run in order.
All output from the batch will be printed on the system printer.

END BATCH

In a real application the job would probably use a stored macro and be much simpler, this example has been written out longhand in an effort to show some of the features of the JCL.


== GEORGE 3 & 4 ==
GEORGE 3 was the main version of the operating system series for the larger machines of the 1900 series. Initially it was released for the 1906/7, eventually it was made available for models down to the 1902T. In contrast to George 1&2 which ran as user mode programs under executive George 3 was a full operating system, leaving only low-level peripheral and interrupt handling to a cut-down version of executive.
George 3 was implemented as a small memory resident part and a collection of chapters (overlays) which were loaded into and removed from memory as needed. Chapters were strictly location independent, allowing best use of memory. Internally George used cooperative multitasking, context switches could take place at any chapter change (call from one chapter to another), or at other specified places in the code. User level code was run using preemptive multitasking, context switches were forced on I/O operations or clock ticks.
George was written in a special assembler, GIN (George INput), which had richer conditional compilation and macro facilities than the standard PLAN assembler. Macros were heavily used by the code to reduce the effort of programming such a large system in assembly language. In later versions the macro features of GIN were used to add structured programming features to the code. Writing the system was estimated to have taken 75 programmer-years of effort.


=== Job control ===
George 3 was a mixed batch and online system. Jobs could be run from cards or tape in the same manner as George 2, or interactively from MOP (Multiple Online Processing) terminals, either simple Teletype Model 33 ASR terminals or block mode VDU terminals.
The job control language was the same on terminals or in batch jobs and included conditional operations and macro operations. In contrast to Unix systems the job control language was part of the operating system rather than being a user level shell process.
A job could only have one program loaded in to memory at a time, but one job could start other jobs to run concurrently, if system resources and site policy would permit. The system would swap user programs out of memory while they were waiting for input or output if other activities required memory to run.


=== File store ===
George 3 provided a tree structured file store, inspired in part by Multics.
Every user of the system had a home directory with as many sub directories as needed under it. A users home directory could be accessed directly, for example the directory for user JOHN could be referred to as :JOHN, or by a full path, for example if JOHN was in the computer science department his home directory might be :MASTER.USERS.COMPSCI.JOHN.
Access control lists were used for security, a user could permit or deny any user or group of users access to his files or directories.
The file store was two-level, files could be either currently on disk, or, if the system was low on disk space they would be archived to tape. If an attempt was made to access a currently off line file the job would be suspended and the operators requested to load the appropriate tape. When the tape was made available the file would be reloaded and the job resumed.


=== George 4 ===
Starting with the 1904A, a paging unit was available for larger 1900 processors and George 4 was written to take advantage of it. George 4 remained compatible with George 3. (It was common to alternate George 3 and 4 on the same machine and filestore, running George 3 during the day for small, interactive workloads and George 4 at night for large, memory intensive, jobs.)
George 4 introduced the concept of a sparse program, a program that had an addressing space larger than its allocated memory and read-only (pure) data and code regions. New versions of the consolidator (linker) and compilers were provided to use these facilities.
The source code of George 3 and 4 were the same; conditional compilation facilities of the GIN assembler were used to select which version of the system was being compiled.
As the 1900 paging feature was not emulated by the 2900 series machines used by later George installations, George 4 fell out of use before George 3.


=== Examples ===
Here are some simple examples of George use


==== Example batch job ====
The job is modelled on the George 2 job above, and like that job is somewhat artificial as in real use most of the work would be done by a pre-stored macro command.
The job would be read in from a card or paper tape reader. With minor changes (removal of the first "JB" command) it could be stored in a file and run from an interactive (MOP) terminal.
As with the George 2 example the job starts with a JOB command (all built-in commands had a long form and a two letter abbreviation, here "JB" is the abbreviation for "JOB"). The JOB command gives a job name, the user to bill for the job, :BRIAN, and the terminator for the job, "####".

JB PLAN4JOB,:BRIAN,T####

WHENEVER (WE) a command fails with error the job will continue at label 5CE for error recovery. The MAXSIZE (MZ) of memory used by this job will be 20K words.

WE COMERR,GO 5CE
MZ 20K

The CREATE (CE) command is used to make a file, in this case a temporary workfile, "!". The INPUT (IN) command then copies all text up to the terminator, "////" into the workfile.

CE !
IN !,T////
PROG(HWLD)
STEER(LIST,OBJECT)
OUTE(SEMICOMPILED)
WSF(HWLD)
PLAN(CR)
#PRO           HWLD40/TEST
#LOW
MESS           12HHELLO WORLD
#PRO
#ENT           0
      DISTY    '11/MESS'
      DEL      2HOK
#END
ENDPROG
////

The LOAD (LO) command loads PROGRAM XPLT (the assembler) from the directory :LIB, it is then started by the RESUME (RM) command. If the run does not HALT with the output LD the job continues at label 1F for error handling.

LO :LIB.PROGRAM XPLT
RM
IF NOT HAL(LD),GO 1F

The ASSIGN (AS) command is used to connect virtual card reader unit 0 to the workfile created above, which is then erased by the ERASE (ER) command. (The erase will be delayed until the file is closed).

AS *CR0,!
ER !

A new workfile is created and the virtual line printer unit 0 assigned to it.

CE !
AS *LP0,!

When PROGRAM XPLT is run it will try to open the disk file in the OUTE directive, We want it to use a temporary workfile so we ask George to MONITOR the open, stopping execution and allowing us to provide the workfile:

MN ON, OPEN

The program in memory (PROGRAM XPLT) is started at location 21.

EN 1
IF NOT MONITOR(OPEN), GO 1F

A new, direct access, workfile is created with 128 word buckets and an initial size of 40K words. The virtual disk channel *DA2 is assigned to it. The program is RESUMED.

CE !(*DA,BUCK1,KWOR40)
AS *DA2,!(WRITE)
RM

If it HALTs with the output OK the job continues at label 1A, if not an error message is displayed and the job exits.

IF HAL(OK),GO 1A
1F DP 0,COMPILATION ERRORS
GO 5EX

The DELETE (DL) command deletes the assembler from memory.

1A DL

Yet another workfile is created to hold the instructions for the linker. As the linker instructions must end with a line "****" the default terminator is used for the INPUT command.

CE !
IN !
*IN ED(SEMICOMPILED)
*OUT ED(PROGRAM TEST)
*LIST
****

The linker, :LIB.PROGRAM XPCK is loaded and initialised.

LO :LIB.PROGRAM XPCK
RM
IF NOT HAL(LD),GO 2F

The virtual card reader is attached to the workfile holding the linker instructions, which is then erased.

AS *CR0,!
ER !

The virtual lineprinter is then assigned in append mode to the last but one workfile created and not yet erased (workfiles are held in a stack, "!" is the top of the stack, "!1" the one under that and so on). The LISTFILE (LF) command is used to print the file on the system printer (the listing will start when the file is closed). The file is then erased (the erase will be delayed until the listing is finished). The virtual disk channel *DA1 is assigned to the top workfile (holding the assembler output) and yet another workfile is created for the linker.

AS *LP0,!1(APPEND)
LF !1,*LP,PA
ER !1
AS *DA1,!
ER !
CE !(*DA,BUCK1,KWOR10)
AS *DA13,!(WRITE)
ER !

A file is created to hold the linker output and attached to virtual disk channel *DA14. The linker is then started at location 21 and if it finishes with the message HH the job continues at label 2A, otherwise an error message is displayed and the job exits.

CE PROGRAM HWLD(*DA,BUCK1,KWOR5)
AS *DA14,PROGRAM HWLD(WRITE)
EN 1
IF DEL(HH),GO 2A
2F DP 0,CONSOLIDATION ERRORS
   GO 5EX

At label 2A the program written by the linker is loaded into memory and run starting at location 20, a success message is displayed and the job exits.

2A LO PROGRAM HWLD
EN 0
DP 0,JOB COMPLETED
GO 5EX

If any command failed the WHENEVER command given at the start of the job will force a jump to label 5CE which displays an error message and exits.

5CE DP 0,COMMAND ERROR IN JOB

When the job gets to label 5EX if it has a currently loaded program it is deleted from memory and the ENDJOB (EJ) command terminates the job.

5EX IF COR,DL
EJ ALL

The end of the job is signalled by the terminator string defined by the JOB command.

####


==== Example MOP session ====
All user input is shown in lower case. All output from George is in upper case.
The user types control-A on an idle Teletype attached to George, George replies with its identification banner and prompt (the time, followed by the invitation to type, a back-arrow. The user then logs in using the LOGIN (LN) command. He is prompted for his password, which will be echoed as the terminal is connected in half duplex mode with local echo. The job then starts.

THIS IS GEORGE 3 MARK 8.67 ON 21MAR11
21.21.23← ln :john,mopjob
TYPE PASSWORD← password
STARTED :JOHN,MOPJOB,21MAR11, 21.21.35 TYPE:MOP

A directory is created with the MAKEDIR (MK) command and the current directory is changed to the new one with the DIRECTORY (DY) command.

21.21.35← mk hellodir
21.28.10← dy hellodir

The system macro NEWCOPYIN is used to read from the tape serial number 123457. As the NEWCOPYIN macro loads a program the session becomes fully started (if the system was heavily loaded it might wait at this point).

21.28.16← newcopyin (123457)
21.28.32 JOB IS NOW FULLY STARTED
21.28.32 0.03 CORE GIVEN 4736
WAITING FOR MT 123457

Apparently the system operator couldn't find the tape and used the CANTDO command to refuse to load it, the NEWCOPYIN fails.

ERROR IN PARAMETER 2  IN OL IN NEWCOPYIN: MT (123457) CORRECTLY IDENTIFI
ED BUT NOT AVAILABLE
DISPLAY: ERROR IN NEWCOPYIN . MACRO ABANDONED
21.28.58 FREE *CR0, 0 TRANSFERS
21.28.58 0.05 DELETED,CLOCKED 0.00
0.05 :DELETED
END OF MACRO

The user tries again with the correct serial number this time. When the tape becomes available he is prompted for the file to load. The list of files is terminated by "****".

21.28.58← newcopyin (123456)
21.32.21 0.06 CORE GIVEN 4736
WAITING FOR MT 123456
21.32.34 USED U31 AS *MT0, MT (123456,HELLOTAPE(0/0))
← hello,hello(/plan)
← ****
21.32.52 FREE *CR0, 2 TRANSFERS
DISPLAY :    1 PARAMETER ACCEPTED
DISPLAY
0.08: MONITOR
DISPLAY : INPUT TAPE *  123456.
DISPLAY
0.08: MONITOR
21.32.52 FREE *FH0, 1 TRANSFERS
21.32.52 FREE U31,8 TRANSFERS
0.10 :DELETED : OK
21.32.52 0.10 DELETED,CLOCKED 0.00
END OF MACRO

The file has been loaded from tape. The LISTFILE (LF) command is used to examine its contents

21.32.52← lf hello
#PRO           HWLD40/TEST
#LOW
MESS           12HHELLO WRLD
#PRO
#ENT           0
      DISTY    '11/MESS'
      DEL      2HOK
#END

There seems to be an error, so the user uses the EDIT (ED) command to fix it. The editor subcommand TC is used to position to the line containing "WRLD", the R command replaces "WRLD" by "WORLD", then the E command writes out the file.

21.33.01← ed hello

EDITOR IS READY

0.0← tc/wrld/
2.0← r/wrld/world/
2.29← e

The system macro PLANCOMP is used to compile the file HELLO(/PLAN) to PROGRAM HELO

21.43.46← plancomp *cr hello(/plan),*idhelo
FILES ALREADY ONLINE: :LIB.SUBGROUPS-RS(1/V3) :LIB.PROGRAM XPCK(1/V12K)
:LIB.PROGRAM XPLT(1/V8C)
21.43.58 0.58 CORE GIVEN 18944
0.58 :HALTED : LD
DISPLAY : START JOB   HELO,
OPEN       *DA2    N   CA   1641   M=#00100   FN=SEMICOMPILED
1.00: MONITOR
21.43.58 FREE *CR0, 8 TRANSFERS
DISPLAY : COMP OK 84     #HELO
21.43.58 FREE *DA2, 9 TRANSFERS
1.01 :DELETED : FI #XPCK
21.43.58 FREE *TR0, 7 TRANSFERS
21.43.58 FREE *LP0, 83 TRANSFERS
21.43.58 1.01 DELETED,CLOCKED 0.00
21.43.59 1.07 CORE GIVEN 11392
21.43.59 FREE *CR0, 5 TRANSFERS
21.43.59 FREE *DA14,20 TRANSFERS
21.43.59 FREE *DA1, 9 TRANSFERS
21.43.59 FREE *DA2, 2 TRANSFERS
21.43.59 FREE *DA13,7 TRANSFERS
1.07 :DELETED : HH
21.43.59 FREE *LP0, 32 TRANSFERS
21.43.59 FREE *DA15,0 TRANSFERS
21.43.59 1.07 DELETED,CLOCKED 0.00
DISPLAY: PLAN COMPILATION/CONSOLIDATION OKAY
END OF MACRO

The newly compiled PROGRAM HELO is loaded into memory by the LOAD (LD) command, then started with the ENTER (EN) command. It displays the traditional message then deletes itself from memory.

21.43.59← lo program helo
21.44.06← en
21.44.07 1.09 CORE GIVEN 64
DISPLAY : HELLO WORLD
1.09 :DELETED : OK
21.44.07 1.09 DELETED,CLOCKED 0.00

Todays arduous work being finished, the user logs out with the LOGOUT (LT) command. The mill time and money used and remaining are displayed.

21.44.07← lt
MAXIMUM ONLINE BS USED 252 KWORDS
21.44.12 1.09 FINISHED : 0 LISTFILES
BUDGET       USED      LEFT
TIME(M)        70   -97797
MONEY          35    80327
21.44.12←


=== Source code ===
George was distributed in a form that allowed a site to modify large parts of the system. A compilation of the system was started, then interrupted just before the end and dumped to magnetic tape. The GIN compiler allowed the compilation to be continued from this point at the user site, possibly modifying code already compiled.
Versions of George 3 before release 8 were provided in binary form. Any modifications needed to the system were made as binary patches. To simplify the process most George chapters included an empty MEND area at the end.
Starting with release 8 the source of George was distributed with the binary, both on magnetic tape and microfiche. A system of source level patches, known as MENDITS was used to modify the system and an existing chapter could be completely replaced by the new modified chapter.
The George user group set up a "MEND exchange scheme" to share interesting modifications to George. Some modifications were distributed freely, others were available for a fee. When ICL produced a new version of George they would sometimes include modifications produced by the users.
For the last released version, 8.67, most of the patches from the MEND exchange scheme were included in the standard George source, switched off by conditional compilation. They can be turned on as part of the standard process of tailoring George for a site.


=== Documentation ===
GEOrge was well documentated internally in a series of looseleaf folders, distributed as an initial version plus amendments. Eventually all the original pages were replaced, so any new copy of the manuals consisted of a box of empty looseleaf folders and a pile of amendments. The first amendment was a list of contributors, and the technical reason for the amendment was described as "to keep everyone happy".


=== Modified Versions ===
A modified version of George 3 was supplied to the University of Manchester Regional Computer Centre (UMRCC). This linked George 3 to a CDC Cyber machine, to which George supplied the offline I/O and Job queueing functions. Online support was supplied by both ICL and Cyber for both hardware and software. The Cyber support team worked in an office with the name "Cybermen" on the door.


=== End of life ===


==== Obsolescence ====
With the release of ICL's "new range", the 2900 series with its VME operating system George became obsolete. However, due to the legacy of investment in software for George, ICL released options to run 1900 series software, including George, on 2900 series machines, initially the Direct Machine Environment (DME), later the Concurrent Machine Environment (CME) which allowed simultaneous running of 1900 and 2900 code on the same system.
New versions of George 3 continued to be released for the 2900. The last version was 8.67, released in 1983.
As of 2005 at least one site in Russia was still running George 3 under DME.
In November 2014 George 3 was run on a reconditioned ICL 2966 at the National Museum of Computing.


==== Emulation ====
David Holdsworth and Delwyn Holroyd obtained copies of George 3 issue tapes when the last live site in the UK, at British Steel, was being decommissioned and wrote an emulator for the 1900 hardware and executive that allows running of George on Microsoft Windows and Linux as part of a project for the Computer Conservation Society. The emulator includes an emulation of Executive and a Java emulation of an ICL7903 Communications Processor making it possible to run MOP sessions by telnetting to (in this case) port 2023.

George 3 Executive Emulator
by David Holdsworth & Delwyn Holroyd
Build: May 15 2014

Memory size: 256K
Exec command: DA GEORGE3A
Waiting for a console telnet connection on port 1900

ICL 7903 Communications Controller emulator
by David Holdsworth & Delwyn Holroyd
Build: Feb 23 2014
-? for usage info

Listening for TTY connections on port 2023 - 4 available
Listening for VDU connections on port 7181 - 4 available
Listening for host connection on port 7903

Tests with the emulator show that George 3 is Y2K compliant.


== References ==


== Further reading ==
Oestreicher, M. D.; Bailey, M. J.; Strauss, J. I. (1 November 1967). "GEORGE 3---A general purpose time sharing and operating system". Communications of the ACM 10 (11): 685–693. doi:10.1145/363790.363806.