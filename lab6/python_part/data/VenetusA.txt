Venetus A is the more common name for the tenth century AD manuscript catalogued in the Biblioteca Marciana in Venice as Codex Marcianus Graecus 454, now 822.
Venetus A is the most famous manuscript of the Homeric Iliad; it is regarded by some as the best text of the epic. As well as the text of the Iliad, Venetus A preserves several layers of annotations, glosses, and commentaries known as the "A scholia", and a summary of the early Greek Epic Cycle which is by far the most important source of information on those lost poems.


== Contents ==
Venetus A contains the following in one volume:
a full text of the Iliad in ancient Greek
marginal critical marks, shown by finds of ancient papyri to reflect fairly accurately those that would have been in Aristarchus' edition of the Iliad
damaged excerpts from Proclus' Chrestomathy, namely the Life of Homer, and summaries of all of the Epic Cycle except the Cypria
two sets of marginal scholia on the Iliad:
the "A scholia", derived largely from the work of Aristarchus
some "D scholia", discussing difficulties in the meanings of words
among the above, a very few exegetical scholia (exegetical scholia are far more characteristic of the "B" and "T" scholia)


== Origins ==
None of the works on which the scholia in Venetus A are based survive. As a result, the task of tracing their contents to their sources is extraordinarily difficult and obscure. The study of the Iliadic scholia is a significant ongoing research topic in Homeric scholarship.
The A scholia, for which Venetus A is by far the most important source, derive from the so-called "VMK" (Viermännerkommentar, "four-man commentary"), named for the four ancient scholars Aristonicus, Didymus, Herodian, and Nicanor. The main source for the A scholia was probably a compilation of their work, rather than each of the four men's work individually. Because all four of these scholars worked in the tradition of the Alexandrian scholar Aristarchus, much of the A scholia can be traced back to Aristarchus himself.
The relationship between the A scholia and other branches of the Iliadic scholia, however, is much more debatable and confused. A text which does not survive, known as "ApH" for its authors "Apion and Herodorus", is key to all reconstructions of this relationship. Eustathius in his own commentary on the Iliad frequently refers to "Apion and Herodorus" as a source, and a comparison between them shows that the relationship between "ApH" and the A scholia is a close one.
Two stemmata or "family trees" for Venetus A may be summarised from the work of van der Valk and Erbse respectively:
Of the two, Erbse's viewpoint tends to be the more highly regarded.
Another important source that feeds into A is a group of scholia on mythographical and allegorical topics, derived from Porphyry's Homeric Questions. The current standard edition of the Iliad's scholia, that of Erbse, omits these scholia.
On the origins of the Proclean Chrestomathy which is partially preserved in Venetus A, see also Epic Cycle, Eutychius Proclus.


== History ==
Venetus A was created in the tenth century AD. All text on the manuscript dates to the same period, including the Iliad text, critical marks, and two sets of scholia in different writing styles. The twelfth century Byzantine scholar and archbishop Eustathius, even if he never saw the manuscript itself, certainly knew texts which were closely related to it; see Origins above (Eustathius cites "Apion and Herodorus" as a source in his own commentary about seventy times).
At some point Venetus A was transported to Italy, but how and when this happened is uncertain. At one point it was thought that Giovanni Aurispa brought it there. In 1424, in a letter to Traversari in Venice, he mentioned four volumes which he had brought back from Greece:

Aristarchum super Iliade in duobus voluminibus, opus quoddam spatiosum et pretiosissimum; aliud commentum super Iliade, cuius eundem auctorem esse puto et illius quod ex me Nicolaus noster habuit super Ulixiade.

Aristarchus on the Iliad in two volumes, a large and very precious work; another commentary on the Iliad; I think Aristarchus was the author of that, as well as of the one on the Odyssey that our friend Niccolò Niccoli got from me.

Aurispa already owned the "two volumes" in 1421; this suggests that he may have brought them back from a trip to Greece in 1413. For a long time it was thought that these two volumes were Venetus A and Venetus B. More recently, however, it has been pointed out that the Venetus A and B manuscripts list multiple authors as their sources, not just Aristarchus, and Aurispa would be unlikely to have ignored this distinction. One scholar has suggested that Aurispa's two volumes were in fact Laurentianus LIX 2 and 3, a two-volume copy of Eustathius' Iliad commentary corrected in Eustathius' own hand, and in which the title is erased.
Venetus A came into the possession of Cardinal Bessarion, the Greek immigrant and scholar, and the man most directly responsible for the Western rediscovery of Greek literature in the Renaissance. Bessarion collected over a thousand books in the fifteenth century, including the only complete text of Athenaios' Deipnosophistai; the autograph of Planudes' Greek Anthology; and Venetus A.
In 1468, Bessarion donated his library to the Republic of Venice, and the library was increased by further acquisitions from Bessarion until his death in 1473. This collection became the core of the Biblioteca Marciana. Bessarion made a condition that scholars wishing to consult the library should deposit books, but no attempt to enforce this was made until 1530.
The earliest known scholar to have used Venetus A as a source is Martinus Phileticus in the 1480s; in this he was followed by Vettore Fausto in 1546 or 1547.
In 1554, Bessarion's library was transferred to the building designed for it by Sansovino, the Biblioteca Sansoviniana. It remains there today.
After that, Venetus A was largely forgotten until Villoison rediscovered and published it, along with the "B scholia" from Venetus B (= Codex Marcianus Graecus 453, now 821), in 1788. This was the first publication of any Iliadic scholia other than the "D" scholia (the scholia minora). The A and B scholia were a catalyst for several new ideas from the scholar Friedrich August Wolf. In reviewing Villoison's edition, Wolf realised that these scholia proved conclusively that the Homeric epics had been transmitted orally for an unknown length of time before appearing in writing. This led to the publication of his own seminal Prolegomena ad Homerum, which has set the agenda for much of Homeric scholarship since then.
Most recently, Amy Hackney Blackwell has a brief article in Wired on the just-concluded month-long effort to digitize Venetus A at the Biblioteca Marciana in Venice (May 2007). This work has resulted in the publication of high-resolution images of each folio of the manuscript, including details of significant areas and ultraviolet images of badly faded text; the images are published under a Creative Commons License and are available for viewing and downloading from the Center for Hellenic Studies of Harvard University.


=== Publication of A scholia ===
Villoison, 1788: A and B scholia
Bekker, 1825–26: A and B scholia
Heyne, 1821–27: D scholia or "scholia minora"
Lehrs, 1848: Herodian (reconstructed from VMK)
Friedländer, 1850: Nicanor (reconstructed from VMK)
Friedländer, 1853: Aristonicus (reconstructed from VMK)
Schmidt, 1854: Didymus (reconstructed from VMK)
Karl Wilhelm Dindorf and Maass, 1875–1888: A, B, and T scholia
Nicole, 1891: Ge scholia
Comparetti, 1901: facsimile edition of Venetus A
Erbse, 1969–1988: all Iliad scholia, except D scholia and mythographical/allegorical scholia derived from Porphyry
Van Thiel, 2000: D scholia or "scholia minora"


== See also ==
Epic Cycle
Homer
Homeric scholarship
Scholia


== References ==


== Further reading ==
Allen, T. W. (1931), "The Homeric Scholia", Proceedings of the British Academy 17 (London)
Erbse, H., various articles: see list in Classical Review 11 (1961), 109 n. 1
Erbse, H. (1960), Beiträge zur Überlieferung der Iliasscholien, Zetemata 24 (Munich)
Erbse, H. (1969–88), Scholia Graeca in Homeri Iliadem (Berlin), ISBN 3-11-002558-2, ISBN 3-11-003882-X, ISBN 3-11-004641-5, ISBN 3-11-005770-0, ISBN 3-11-006911-3, ISBN 3-11-009530-0, ISBN 3-11-011314-7
Labowsky, L. (1979), Bessarion's Library and the Biblioteca Marciana (Rome)
Van der Valk (1963–64), Researches on the Text and Scholia of the Iliad, 2 vols. (Leiden)


== External links ==
Online edition of Comparetti—facsimile of Comparetti's 1901 facsimile edition of Venetus A
Online edition of Villoison—facsimile of Villoison's 1788 edition of the Iliad and its scholia
NewScholiasts—project for translating the Iliad scholia into English