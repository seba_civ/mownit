Master of Adventure: The Worlds of Edgar Rice Burroughs is a book by Richard A. Lupoff that explores the work of Edgar Rice Burroughs, the creator of Tarzan and author of numerous science fiction, fantasy, and adventure novels. The book is one of the few major works of criticism covering the work of Edgar Rice Burroughs, and it helped create renewed interest in Burroughs's work during the 1960s.

The book was first published in hardcover by Canaveral Press in 1965 under the title Edgar Rice Burroughs: Master of Adventure. A revised and enlarged second edition with a new introduction by the author was published in paperback by Ace Books in 1968, with a third "centennial edition," adding a new introduction by the author and also in paperback, following from the same publisher in 1975. Both Ace editions featured cover art by Frank Frazetta, that of 1968 being recycled from the publisher's earlier edition of Burroughs's The Beasts of Tarzan, and that of 1975 being new.

A fourth edition, again revised and enlarged with new content, was published as a trade paperback by Bison Books in 2005. The work was retitled Master of Adventure: The Worlds of Edgar Rice Burroughs for the fourth edition. New content in the 2005 edition includes a foreword by fantasy author Michael Moorcock, an admirer of Burroughs's work, a new foreword by the author, an essay by Phillip R. Burger, a consultant to Edgar Rice Burroughs, Inc.; corrected text, and an updated bibliography.
All four editions include a preface by Henry Hardy Heins and twelve black and white illustrations, including four by Al Williamson and Reed Crandall, two by Crandall alone, and six by Frank Frazetta. For the fourth edition the frontispiece illustration by Williamson and Crandall, "Edgar Rice Burroughs and His Most Famous Creations," was colorized and reused as the cover illustration.
The book includes outlines for all of Burroughs's major novels and covers several aspects of his body of work, including influences upon his work, works influenced by Burroughs, and later works by Burroughs that never came to fruition due to his service in World War II as a war correspondent and declining health later in life. Lupoff also offers a recommended reading list of essential Burroughs novels that is notable for its placement of the relatively obscure Burroughs novels The War Chief and The Mucker at third and fourth place respectively.


== Reception ==
While the response to both editions of the book has been generally favorable, it remains controversial among some Burroughs aficionados, particularly for Lupoff's argument that the Barsoom series was heavily influenced by Lieut. Gullivar Jones: His Vacation (1905) and The Wonderful Adventures of Phra the Phoenician (1890) by Edwin L. Arnold.


== References ==
Edgar Rice Burroughs: Master of Adventure title listing at the Internet Speculative Fiction Database


== External links ==
Publisher's website for Master of Adventure
Google books preview