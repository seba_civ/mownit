Paul McKay (born 19 November 1996) is a Scottish professional footballer who plays as a Centre Back for Leeds United after joining in January 2016 from Doncaster Rovers.
McKay was born in Glasgow and played youth football with the Doncaster Rovers Academy alongside his studies at Hill House School, before starting his professional career with the club.


== Personal life ==
McKay's father is football agent, Willie McKay and his twin brother, Jack McKay is also a professional footballer with Leeds United.


== Club career ==


=== Doncaster Rovers ===


==== Senior career ====
McKay made his professional debut for Doncaster on 1 September 2015 in Doncaster Rovers First Round win in the Johnstone's Paint Trophy against Burton Albion, McKay started the game which saw Doncaster win 5-3 on penalties after a 0-0 draw.


==== Ilkeston FC (loan) ====
In October 2015, Paul and his twin brother Jack went out on loan to Ilkeston to gain first team experience.


=== Leeds United ===
On 11 January 2016, Paul and his brother Jack both joined Championship side Leeds United for undisclosed fees on 2 and half year contracts.


== International career ==
Although McKay has yet to make an appearance for an international side at any level, he is eligible for Ireland, England and Scotland.


== Career statistics ==
As of match played 3 May 2014


== Honours ==


== References ==


== External links ==
Paul McKay career statistics at Soccerbase