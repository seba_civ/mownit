Viktor Þór William Jensen (born 30 December 1987 in Hammersmith, London, England) is an Icelandic racing driver, with dual (British) nationality. His father is Canadian-born British radio DJ David Jensen.
Viktor read Mechanical Engineering at Imperial College London and is currently living in Hong Kong.


== Early career ==
Viktor debuted in kart racing at 11 years old, winning his first novice race. Over the next five years he quickly progressed through the ranks, culminating in competing at the CIK-FIA European ICA finals. Due to dual Icelandic/British nationality, he represented England in 2001 and 2002 at the Fathom Internations Challenge, himself winning the race in 2001.


== Stepping into Cars ==
In 2003, Viktor started to delve into the world of single-seater racing cars, attending the Formula BMW scholarship course in Valencia, Spain. Whetting his appetite to progress into cars, Viktor then tested a number of cars including Formula Ford, Formula Renault and Caterham Superlights. One of the cars tested was a Formula Palmer-Audi car. The FPA car impressed, and it was decided that 2004 would see Viktor competing in the Formula Palmer-Audi Championship.
In 2007 Viktor raced in the British Formula Three International Championship with the Alan Docking Racing team and in 2008 raced in the same series for the Nexa team.


== References ==


== External links ==
Official Website - www.viktorjensen.co.uk