"Vitamin L" is a song by the American pop rock band B.E. Taylor Group. It is the fifth track on the band's second studio album, Love Won the Fight, and also the third single pulled from that album. In a departure of the norm of B. E. Taylor singing lead vocals, Joey d'Amico sings lead on this track. It also heavily uses synthesizers and a brass sections as the main instruments. The guitar's role is minimized to a steady strumming pattern with sustained power chords in the chorus. The lyrics tell the story of a man whose medical condition can only be treated with an unstable drug, called Vitamin L, that reacts "when boy meets girl". In the last verse it is revealed the drug was, in fact, love, and the man is cured of his ailment when he falls in love with a woman.
This song is the B. E. Taylor Group's most popular song, as it peaked at #66 on the Billboard Hot 100 in 1984 and stayed on the chart for 8 weeks. The song also spawned a popular music video that scored heavy airplay on MTV. B. E. and Rick Witkowsi are featured as the doctor and pharmacist in the video, respectively. The song also is a near constant at live performances.
B. E. Taylor later re-recorded this song on his first solo album, Try Love, as more of a rock song, getting rid of the synthesizers and making the bass, lead guitar, and drums much more involved. The lyrics in the verses have entire lines changed as well, mildly changing the story in the process, e.g. "A chemical reaction occurs/Every time that boys meets girl" to "It could keep us all in touch/But we can never take too much", erasing the idea that the vitamin is an unstable drug. Three whole lines in the third verse are changed, but these do not change the story in any way.


== Track listing ==
A. Vitamin L (Club Version) (4:46)
B. Vitamin L (LP Version) (5:29)


== Mary Kay Place's 1976 song Vitamin L ==
"Vitamin L." was also the title of a popular 1976 country song by Emmy award-winning actress Mary Kay Place on her Grammy-nominated album Tonite! At the Capri Lounge Loretta Haggers. She also performed the song on the cult television series Mary Hartman, Mary Hartman, portraying the character Loretta Haggers, which was loosely based on Dolly Parton.


== Links ==
Mary Kay Place's Vitamin L:
http://www.ivsee.net/re/index.php?u=Z7kjfbrRcctjX0bI13WoDcLV4Lw%2Fru%2Bj9ajGyDSatSw92iLlEm9ctYAL0vYjEhaX32cRnRCdJMj4Kx%2Br%2FLtUvfrCz%2BHtIIhUqrZSL0l5oZfxnfer0RVJijVPhzGQg3NSp2WpGUJamC9v0L8ml0bwXUzHZuFCwyP71qyDYmreQ3K%2Fp%2FSpQq%2FyR0xJb9ZjHynxAlLiejlr9V1A%2BZBJoYBMMOa1wWgpQg%3D%3D