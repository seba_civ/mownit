Beth Wymer (born October 5, 1972) is a former NCAA champion gymnast. While attending the University of Michigan, she won the NCAA event championship in the uneven bars three consecutive years from 1993-1995 and was a first-team All-American four times in the uneven bars, twice in the all-around, and once in the balance beam. In 1995, Wymer was named Big Ten Athlete of the Year. She was inducted into the University of Michigan Athletic Hall of Honor in 2006.


== Youth ==
A native of Sylvania, Ohio, Wymer’s parents were both gymnasts, and they introduced her to the sport at age 5. In 1994, Wymer recalled: "My parents put me in gymnastics, actually. I had so much energy and was always bouncing around. I think I was kind of annoying. So they started me in gymnastics to burn off all that energy." At age 8, she began to study gymnastics under Yusaku Hijioka at the Sunrise Gymnastics Academy in Toledo, Ohio. Wymer graduated from Southview High School in 1991 and was one of the most highly-recruited gymnasts in the United States. Wymer later described her high school regimen as follows:

"I would get out of high school at noon, and train. We had to write letters to the superintendent and principal and convince them that this would be a good thing for me. At that time I was elite and competing with the nation's best."

Despite being an elite gymnast, Wymer did not make the 1988 U.S. Olympic team, finishing 26th at the 1988 U.S. nationals. By 1992, the 18-year-old Wymer decided to concentrate on a college career at the University of Michigan. In 1992, Wymer told the Detroit Free Press of her disappointment:

"I probably won't even watch gymnastics during the Olympics. I always wonder how far I could have gotten if I had put 100 percent of my energy into gymnastics. Maybe when I get older, I'll be able to watch."

Wymer recalled that her "mistake" was dividing her high school time between gymnastics, student government, cheerleading and boyfriends. "I wanted to be involved in everything, but I regret that," she said. "I wish I had the mind and body I have today when I was younger. But I have to stop dwelling on the past or I'll drive myself nuts."


== University of Michigan ==
In 1992, the University of Michigan women's gymnastics teams was at a low point, having gone 0-13 in the conference in 1989 and 1-10 in 1988. Coach Bev Plocki’s conversion of the Michigan gymnastics program received a major boost when she succeeded in recruiting Wymer to Michigan. In recruiting Wymer, Plocki told her: "You can go to one of the three dominant schools (Utah, Georgia or Alabama) and be another All-America on their list of All-Americas. Or, you can come to Michigan and be the first All-America." Wymer described her decision to attend Michigan as follows:

"I had always loved Michigan, had attended their gymnastics camps since I was 13 years old. Michigan wasn't even on the map as far as gymnastics was concerned. I was recruited by the No. 1 college team, Utah, very heavily. I had some other big schools interested. I went on a recruiting trip at Michigan. Coach Plocki said, 'you will be just one of many at other schools, and here, you'll be the one that puts Michigan on the map. 'I thought, that's a big challenge.'"

Wymer won the Big Ten Conference individual all-around championship all four years at Michigan (1992-1995) and also helped the Michigan win the Big Ten team championship all four years. Michigan’s women gymnastics team was 116-9 over her four years, and 45-1 in the Big Ten. The Wolverines finished ninth in the NCAA championships during Wyme’s sophomore year, fourth in her junior year and second in her senior year. She won the NCAA event championship in the uneven bars three consecutive years from 1993-1995, and was selected as a first-team All-American four times in the uneven bars, twice in the all-around, and once in the balance beam. Wymer broke U-M records for every individual event and received the first perfect 10 on the vault in school and conference history. In 1995, Wymer was named Big Ten Athlete of the Year. In her gymnastics career, she scored more than ten perfect 10.0’s on three out of the four events. Michigan’s coach, Bev Plocki said of Wymer, "She is the most intense person I have ever met. As an athlete, she obsesses with perfection. She has all the awards, but is still very much a team player. She's like a Baptist minister or something, the way she gets everyone fired up."
Wymer received her Bachelor's degree from the University of Michigan in 1995 and a Master's Degree in kinesiology in 1996.


== Family and post-collegiate career ==
Since January 2005, Wymer has been counseling clients in nutrition, lifestyle changes and personal training. Wymer has two daughters, Lindsay and Madison, and has also worked as a construction estimator. In 2006, she became the first woman gymnast to be inducted into the University of Michigan Athletic Hall of Honor. Wymer has also published an informational pocket nutritional book.


== See also ==
University of Michigan Athletic Hall of Honor


== References ==