The Dido class was a class of sixteen (including five in the Bellona sub-class) light cruisers built for the Royal Navy. The design was influenced by the Arethusa-class light cruisers. The first group of three ships was commissioned in 1940, the second group (six ships) and third group (two ships) were commissioned in 1941–1942. The Bellona subclass ships were commissioned between 1943 to 1944. Most members of the class were given names drawn from classical history and legend.


== Armament ==

The class were intended to be armed with ten 5.25 inch (133 mm) guns in five twin turrets, which were of the same circular design as the secondary armament in the King George V-class battleships. A shortage of the guns, due to difficulties in manufacturing them, led to the first group being armed with only eight 5.25 inch guns in four twin turrets. The fifth twin turret was added later to Dido only. The first group was also armed with a 4 inch (102 mm) gun for firing star shell and two quadruple QF 2 pounder (40 mm) "pom-poms".
The second group had the five twin 5.25 inch guns but did not have the 4 in (102 mm) gun. The third group's armament was changed due to the shortage of 5.25 in guns, being armed with eight 4.5 inch (113 mm) guns in four twin turrets instead. The 4.5 inch gun was better suited to the primary anti-aircraft role of the Dido class. The 4.0 inch (102 mm) gun was also fitted and the 2 pounder armament was increased from eight to ten.
The Bellona subclass differed in appearance somewhat from their predecessors. They had eight 5.25 inch (133 mm) RP10MkII guns in four twin turrets and had greatly improved anti-aircraft armament, with twelve 2 pounder guns and twelve Oerlikon 20 mm cannons. The bridge of the Bellona class was lowered by one deck compared to the previous three groups, which allowed full radar control to be fitted to the 5.25 inch (133 mm) turrets and 2 pounders, due to the decreased topweight. These ships used the HACS high angle fire control system. The two funnels were more upright than the raked ones of the original Dido class.


== Service ==
In World War II, the Dido class saw much action, including the battle of Cape Matapan, the Second Battle of Sirte, Operation Torch, Operation Overlord and the battle of Okinawa, as well as many other duties in the Mediterranean and Pacific. The class lost five ships during the war (Bonaventure, Charybdis, Hermione, Naiad, and Spartan); in addition Scylla was badly damaged by a mine and declared a Constructive Total Loss. The post-war survivors continued in service; all were decommissioned by the 1960s. Bellona, Black Prince and Royalist were lent to the Royal New Zealand Navy post-World War II. In 1956, Diadem was sold to Pakistan and renamed Babur.


== Ship modifications ==


=== Didos ===
Bonaventure completed with only four twin 5.25 in turrets because of shortages and received a 4 inch starshell gun in X position. She had received a radar set before October 1940 but was otherwise unaltered.
Naiad completed with five turrets. She received five single 20 mm in September 1941 and had radar Type 279 by this time.
Phoebe completed with four turrets and was fitted with a 4 inch in Q position forward of the bridge. The latter was landed during her refit between November 1941 and April 1942 at New York, along with the .5 inch machine guns and Type 279 radar, while a quadruple 2 pdr supplanted the 4 inch and eleven single 20 mm guns were fitted. Radars were now Type 281, 284 and 285. The A turret was temporarily removed at the end of 1942 after torpedo damage. During repairs in the first six months of 1943, all three quadruple 2 pdr were landed, as were seven single 20 mm, to be replaced by three quadruple 40 mm Bofors guns and six twin 20 mm. Radar Type 272 was also fitted. A turret was replaced in July 1943. Her light anti-aircraft weaponry in April 1944 was twelve 40 mm (3 × 4) and sixteen 20 mm (6 × 2, 4 × l).
Dido had four turrets and a 4 inch similar to Phoebe. The 4 in and the machine guns were removed in the latter half of 1941 at Brooklyn Navy Yard, when Q 5.25 in turret was shipped and five single 20 mm were fitted. In the early summer of 1943 three single 20 mm were exchanged for four twin 20 mm and the radar outfit was altered by the addition of Types 272, 282, 284 and 285. April 1944 lists show only eight 20 mm.

Euryalus completed with her designed armament. In September 1941 the .5 in MGs were landed and five single 20 mm fitted. Two more were added by September 1942. By mid-1943 two single 20mm had been removed and four twin 20 mm shipped. The type 279 radar was replaced by types 272, 281, 282 and 285. In a long refit from October 1943 to July 1944, Q turret was replaced by a quadruple 2 pdr and two twin 20 mm were fitted. Radar 271 and 272 were removed and types 279b, 277 and 293 fitted.
Hermione also completed as a five-turret ship. She had the .5 in MGs removed in October/November 1941 and received five single 20 mm.
Sirius completed with five turrets and five 20 mm. She had received two more 20 mm by mid-1943. One of these was landed at Massawa at the end of 1943 and two 40 mm Bofors Mk III were fitted. She is listed as having only seven 20 mm as light AA in April 1944. By April 1945 she had two Mk III 40 mm fitted and had landed two single 20 mm.
Cleopatra was completed with two 2-pounders in 1942 in lieu of the .5 in MGs but these were removed in the middle of the year and replaced by five 20 mm. A sixth 20 mm was added in mid-1943. During repairs between November 1943 and November 1944, Q turret was removed, as were two quadruple 2 pdr and five 20 mm. Three quadruple 40 mm Bofors and six twin 20 mm were fitted and the singles numbered four.
Argonaut completed with four single 20 mm in lieu of the .5 in MGs. She had Q turret removed during repairs in 1943/44, and lost the four single 20 mm. She received a quadruple 2 pdr in lieu of the 5.25 in, and had five twin 20 mm fitted. By April 1944 her light AA comprised three quadruple 2 pdr, six twin power-operated 20 mm and five single. By the end of the war with Japan she had received five 40 mm Bofors and three single 40 mm Bofors Mk III.
Scylla completed with four twin 4.5 in Mk III in UD MK III mountings because of a shortage of 5.25 in mountings. The forward superstructure was considerably modified to accommodate these and also to increase crew spaces. Her light AA on completion was eight single 20 mm. Six twin power-operated 20 mm were added at the end of 1943.
Charybdis also completed with four twin 4.5 inch, and had in addition a single 4 in Mk V for-ward of X mounting. Her light AA at completion was four single 20 mm and two single 2 pdr. The 4 inch starshell gun and two single 2 pdr were removed and replaced by two twin and two single 20 mm, probably in 1943.


=== Bellonas ===

Spartan received no alterations as far as is known.
Royalist was converted to an escort carrier squadron flagship immediately on completion, when an extra two twin 20 mm were fitted as well as four single 20 mm. She was the only ship to receive an extensive postwar modernisation ordered for the RN but was later sold to New Zealand. Plans were drawn up to fully modernise the four improved Didos with either four twin 3 inch L70 guns or Mk 6 4.5 inch guns. However, that would have required building new broad beamed Didos (as was seriously considered in 1950-54). This was because the magazines of the Royalist type could hold only enough 3 inch ammunition for 3 minutes and 20 seconds of continuous firing. The refit of Royalist was shortened and that of Diadem abandoned because new steam turbines were regarded as both necessary and unaffordable. Royalist′s reconstruction, like that of Newfoundland, incorporated much of the RN's late 1940s and early 1950s view of a desirable cruiser. Royalist′s 5.25 inch armament was given some of the improvements of the final 5.25 inch mounts built for Vanguard, but not the extra space or power ramming. Also added was a secondary armament of 3 STAAG auto twin 40mm, new 293, 960M radar and Type 275(2 sets) DP fire control for the 5.25 guns, and a lattice mast. (Royalist was loaned to the RNZN in 1956, in exchange for Bellona).
Bellona had four single 20 mm added by April 1944, and received an extra eight single 20 mm by April 1945. (She was loaned to the RNZN after the war and operational 1946-52, the twin Orelikons had been replaced by 6 single MK3P 40mm in the RNZN unique kiwi electric powered mount, but Bellona was never actually fitted with 6 standard tacymetric directors, requested by the RNZN for controlling the Bofors. The RNZN mothballed the quad pom pom mounts for manning reasons, but maintained the single oerlikons on HMNZS Bellona.).
Black Prince and Diadem also received eight single 20 mm, and had a further two twin 20 mm by early 1945. (Black Prince was loaned to the RNZN after the war and was operational briefly in 1947 before part of her crew mutinied and were discharged,and after a 1952 refit with 8 single Mk3P 40mm which were electric powered, like the RN Mk 9 and 6 single Oerlikon and operated till 1955, which included a visit to the 1953 Fleet Review at Spithead. Diadem was sold to Pakistan in 1956, after a modest refit with 293 and 281 radar and standardised 40mm twin and single light AA guns. Her 5.25" guns were fired in Pakistan's brief war with India in 1961. Diadem became a cadet training ship in 1962 and was renamed Babur).


== Ships in class ==


== In popular culture ==
HMS Ulysses, a 1955 novel by Alistair Maclean, is based on Royalist′s late war cruises on Arctic convoys to Murmansk. MacLean puts emphasis on the high quality radar fit and the ship's primary air defense role from the start. Also noted is the ship's high trial speed of supposedly near 39 knots; the exceptional difficulty of the crew sleeping in these small cruisers, a problem for every Royalist crew from 1943 to 1965; and its supplementary anti-submarine capability.
Sailor of the King (called Single-handed in the USA), the 1953 film adaptation of C.S. Forester's novel Brown on Resolution, used the Dido-class cruiser HMS Cleopatra to represent two fictional Royal Navy ships. The first ship, "HMS Amesbury", is sunk in a heroic battle with the more powerful German raider "Essen". ("Essen" was represented by HMS Manxman with large mock-up gun turrets.) Cleopatra reappears as "HMS Stratford", and triumphs at the end of the film. The two battle sequences depict the ship firing her guns and torpedoes in some detail.


== See also ==
Atlanta-class cruiser : contemporary American cruiser of similar size, role and configuration


== Notes ==


== References ==
Colledge, J. J.; Warlow, Ben (2006) [1969]. Ships of the Royal Navy: The Complete Record of all Fighting Ships of the Royal Navy (Rev. ed.). London: Chatham Publishing. ISBN 978-1-86176-281-8. OCLC 67375475. 
Dido class at Uboat.net
Bellona class at Uboat.net
WWII cruisers


== External links ==
Gunnery Layout of a Dido-class cruiser. from Gunnery Pocket Book 1945 placed online courtesy of Historic Naval Ships Association
Newsreel video of HMS Scylla fighting the Luftwaffe while protecting convoy PQ18
Our Navy in Action; newsreel footage of Dido-class cruisers engaging Axis aircraft and Italian battleships during the Battle of Sirte on 22 March 1942
Short video clip of a Dido-class cruiser in action