Ponciano Ponzano y Gascón (19 January 1813 — 15 September 1877) was a Spanish neoclassical sculptor. A prolific artist, in his day he was highly esteemed. His work is now largely forgotten, although it can still be seen in many public locations.


== Early years ==
Ponciano Ponzano was born in Zaragoza on 19 January 1813. His father was a guard at the Royal Academy of Fine Arts of San Luis, and he grew up among works of art. As a child he showed a great love of drawing and painting. He studied sculpture in the neoclassical style under José Álvarez Cubero at the Academy of San Fernando in Madrid. In 1832 he was awarded a gold medal. Impressed by a historical relief that he had made, the Academy of San Fernando awarded him a grant to study in Rome. There he was able to study sculpture from the Greco-Roman era under masters such as Bertel Thorvaldsen and Pietro Tenerani, and achieved some success with his own dramatic works. One of these, Ulysses recognized by Eurycles, gained such good reviews that on his return to Spain in 1838 he was named an academic of merit.


== Later career ==
On his return to Spain Ponzano established a workshop in Madrid. He received many commissions, particularly for busts of members of the government and the royal family. He was appointed the official sculptor of the court of Queen Isabella II of Spain, and his career blossomed. He became a prolific sculptor of reliefs, statues and memorials. In 1848 he won a competition to decorate the pediment of the Congress of Deputies. In 1862 he was commissioned by the Queen to create the Pantheon of the Princes of the Monastery of El Escorial, but he was unable to complete the work. He was appointed a professor of the Madrid School of Fine Arts in 1871. Ponzano died on 15 September 1877 in Madrid.
In his day Ponciano Ponzano was one of the most highly regarded of Aragonese sculptors. He moved in high social circles, and was considered to be of excellent moral character. However, his work has been criticized as not being authentically creative. The neoclassical tradition was essentially sterile and he lived in a period when Spanish and European sculpture was in decline, with little innovation. Due to changes of fashion, his work has now been largely forgotten, although it is still preserved in private collections and in public monuments such as San Lorenzo de El Escorial.


== Sample works ==
Ponzano created a large group The Flood for Francisco de Borja Queipo de Llano, 8th Count of Toreno. It depicts a naked man climbing with his mother carried on his shoulder to save her from the flood. The facade of the Congress of Deputies centers on a sculpture of Spain embracing the constitutional state, represented by a woman with her arm around a young girl. Surrounding the pair are figures that represent in allegorical form Justice and Peace, Science, Agriculture, Fine Arts, Navigation, Industry, Commerce and so on. He also executed two bronze lions for the building's access stairway in a more realistic manner. Ponzano made the tympanum of the portal for the church of St. Jerome in neo-medieval style. He was also responsible for decorating the auditorium of the Central University at San Bernardo in Madrid. The Lope de Vega Theater, Valladolid by architect Jerónimo de la Gándara, which was inaugurated on 8 December 1861, has a pediment that holds a medallion with the likeness of Lope de Vega sculpted by Ponciano Ponzano. He won many commissions for portraits. Some of these busts were more realistic because he was able to pay more attention to nature and was less distracted by representing a symbolic character. Examples are his busts of Dr. Eusebio Lera y Aznar and of José de Madrazo y Agudo.


== Gallery ==


== References ==