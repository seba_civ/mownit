Bryce Nathaniel Harlow (August 11, 1916 – February 18, 1987) was a congressional staff member, army officer, advisor to U.S. presidents, and corporate lobbyist.


== Early life ==
He was born in 1916 in Oklahoma City, Oklahoma, the son of Victor E. Harlow, who had served as a college professor, college president, and publisher of Harlow's Weekly, which focused on Oklahoma political, social and economic issues. Bryce graduated from the University of Oklahoma in 1936 at age 19, followed by two years of graduate work in government at the University of Texas at Austin and the University of Oklahoma. He then went to Washington, D.C., where he served on Capitol Hill as assistant librarian of the U.S. House of Representatives and then as an aide to Congressman Wesley Disney.


== Military career ==
In 1940, he joined the army reserve, and in July 1941, he was placed on active duty as an officer in the public information office of Army Chief of Staff Gen. George C. Marshall. He then served as the army's congressional liaison under Wilton B. Persons. He rose to the rank of lieutenant colonel.


== Corporate career ==
After the war, Harlow returned to Oklahoma City twice to serve as vice president of Harlow Publishing Company in 1946-47 and 1951–52, but both times, he was recruited back to positions in Washington. In 1947, he joined the staff of the House Armed Services Committee, and in 1948, he became the head of the staff. In 1950, he became disenchanted with the Democratic Party and switched his registration to Republican.


== Government career ==
In 1953, newly elected President Dwight D. Eisenhower established a congressional liaison office in the White House and appointed Wilton B. Persons as its head. Persons suggested Bryce Harlow join the staff, and Eisenhower was enthusiastic. Harlow resisted at first, wanting to stay in Oklahoma, but after a personal call from the President, he decided to accept. He was soon writing speeches for the President and went on to hold a variety of titles in the White House through Eisenhower's two terms.
Harlow also wrote campaign speeches for Richard M. Nixon in 1960, but Nixon was defeated by John F. Kennedy. In 1961, Harlow was hired by Procter & Gamble to create their first governmental relations office in Washington, D.C. His connection with the company was through Neil H. McElroy, former Procter & Gamble president and now chairman, who had served as Eisenhower's Secretary of Defense. Harlow was the company's main representative to Congress and federal agencies until his retirement in 1978. At the time of his retirement, he was called the "unofficial dean of Washington corporate representatives." He continued to be active in presidential politics and took leaves of absence from Procter & Gamble to work with Nixon from 1968 to 1971 and again from 1973 to 1974. After assisting with Nixon's successful presidential campaign in 1968, he was one of Nixon's first White House appointees, starting with the title of assistant to the president for legislative and congressional affairs. Before long Harlow was elevated to counselor to the president with cabinet rank.
Harlow later served as an informal advisor to Presidents Gerald Ford and Ronald Reagan.


== Honors ==
Harlow received many honors. In 1960, he received the Minuteman Award, the highest honor of the Reserve Officers Association. He was inducted into the Oklahoma Hall of Fame in 1977. In 1981, President Reagan awarded him the Medal of Freedom. In 1990, the Fund for American Studies established the Bryce Harlow Institute on Business and Government Affairs, a summer program at Georgetown University.


== Bryce Harlow Foundation ==
In 1982, the Bryce Harlow Foundation was established. The Foundation has awarded the Bryce Harlow Award to an individual who has worked to advance business-government relations and "whose integrity, dedication and professionalism echo the work and life of the late Bryce Harlow." Recent recipients of the award have included Senators Mark Warner, Joseph Lieberman, Jon Kyl, Rob Portman, and Daniel Inouye.

The Bryce Harlow Foundation also promotes integrity in professional advocacy through workshops and advocacy forums at educational institutions in Washington, D.C. The foundation selects approximately twenty part-time Washington, D.C. area graduate students each year to receive the Bryce Harlow Fellowship. The current president of the Bryce Harlow Foundation is Barbara Faculjak.


== Notes ==


== References ==
Burke, Bob and Ralph G. Thompson (2000). Bryce Harlow: Mr. Integrity, Oklahoma Heritage Association (ISBN 9781885596154)
Mayer, Michael S. (2010). "Harlow, Bryce N." The Eisenhower Years, Presidential Profiles. American History Online. Facts On File, Inc. (accessed February 22, 2014)


== External links ==

Bryce Harlow Foundation
Bryce N. Harlow Collection at the Carl Albert Center
Papers of Bryce N. Harlow at the Dwight D. Eisenhower Presidential Library
Encyclopedia of Oklahoma History and Culture - Harlow, Bryce
Obituary, New York Times