The Men's 470 was a sailing event on the Sailing at the 2012 Summer Olympics program in Weymouth and Portland National Sailing Academy. Eleven races (last one a medal race) were scheduled and completed. 54 sailors, on 27 boats, from 27 nations competed. Ten boats qualified for the medal race.


== Race schedule ==


== Course areas and course configurations ==

For the 470 course areas P (Portland),N (Nothe), W (West) and S (South) were used. The location (50° 35.19’ N, 02° 26.54’ W) points to the center Portland course area, the location (50° 36.18’ N 02° 25.98’ W) points to the center of the Nothe course area, the location (50° 37.18’ N 02° 23.55’ W) points to the center of the West course area and the location (50° 35.71’ N 02° 22.08’ W) points to the center of the South course area. The target time for the course was 60 minutes for the races and 30 minutes for the medal race. The race management could choose from many course configurations.


== Weather conditions ==


== Final results ==

Legend: DSQ – Disqualified; OCS – On the course side of the starting line; 
QUA – Qualified for next phase;
Discard is crossed out and does not count for the overall result.


== Daily standings ==


== Notes ==


== Other information ==


== Further reading ==
"Digital Library Collection (Official Olympic Reports 1896 - 2008)". Digital Library Collection at la84.org. la84foundation. Retrieved 3 March 2014. 
"London 2012". Olympic.org. International Olympic Committee. 


== References ==