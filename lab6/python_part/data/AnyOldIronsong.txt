"Any Old Iron" is a British music hall song written by Charles Collins, Fred E. Terry and E.A. Sheppard. The song was made famous by Harry Champion, who sang it as part of his act and who later recorded it.


== Other versions ==
In 1957, Peter Sellers recorded a rendition in a voice he created for The Goon Show, Willium "Mate" Cobblers. It reached No. 17 in the UK Singles Chart that year.
It was performed on The Muppet Show by Kermit the Frog, joined by Fozzie Bear, two whatnots, and the audience. [Season 2, Episode 14 (Elton John)]
It was performed by Roger Daltrey, accompanied by The Chieftains, in Belfast, Northern Ireland in 1992. A recording of the performance appears on the album An Irish Evening.
It was included in The Boy Friend, a 1954 musical by Sandy Wilson.
It was also sung by Lonnie Donegan.
The Barron Knights used the tune and some of the lyrics of the song in a satire on punk, in the late 1970s.
Snuff (band) included the song as a previously unreleased track on their 2005 compilation: Six Of One, Half A Dozen Of The Other 1986-2002.
British comedian Bill Bailey, along with the BBC Concert Orchestra, as part of his live show "Bill Bailey's Remarkable Guide to the Orchestra", mixed the song in as part of a "Cockney Arrangement" of the "William Tell Overture".
The song was used, with different lyrics, in television advertisements for Hammerite and Smoothrite paint.
There is a sequence in the movie "Yellow Submarine" when the real Beatles are confronted with their look-alikes trapped in a blue glass sphere, inspiring John to go off on an abstruse invocation of Einstein's Theory of Relativity, to which Paul's response is to sing 'Any old Ein! Any old Ein! Any any any old Einstein!'
Scunthorpe United ("The Iron") have adopted this song, and it is played before all of their home games.
Top Gear Series 18 sang the song in jest as Richard drove a classic Morgan through a corner.


== References ==