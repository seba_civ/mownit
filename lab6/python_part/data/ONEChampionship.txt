ONE Championship (previously known as ONE Fighting Championship or ONE FC) is a Singapore-based mixed martial arts (MMA) promotion which was launched on 14 July 2011 by multimillionaire entrepreneur Chatri Sityodtong and former ESPN Star Sports senior executive Victor Cui. According to CNBC, ONE Championship is Asia's largest sports media property with a global broadcast to over one billion homes in 75 countries. It is widely recognized as the biggest MMA promotion in Asia and notable fighters on the roster include Shinya Aoki, Bibiano Fernandes, Ben Askren, Roger Gracie, Brandon Vera, Jadamba Narantungalag and Jens Pulver.
The first ONE Championship event was held on 3 September 2011 at the Singapore Indoor Stadium. The promotion has since expanded rapidly across the Asian region and gained in popularity. ONE Championship has held over 20 events to date with shows being held in Jakarta, Manila, Kuala Lumpur, Dubai, Taipei, Phnom Penh, Beijing, Guangzhou and Yangon as well as Singapore.
ONE Championship has a ten-year broadcast deal with Fox Sports Asia  (formerly known as ESPN Star Sports) and has signed notable partnerships with Disney, Kawasaki, Marvel and Facebook.


== History ==


=== Formation ===
ONE Championship was launched as "ONE FC" at a press conference in Singapore on 14 July. The ONE FC fighters unveiled at the press conference included Anuwat Kaewsamrit, Eduard Folayang, Yodsanan Sityodtong, Ole Laursen, Leandro Issa, Orono Wor Petchpun and Rolles Gracie.
Victor Cui stated that, "ONE Fighting Championship marks a significant inflectionpoint in the sport of mixed martial arts in Asia. By our first event on September 3, we will be in more than 500 million homes across Asia and it is only the beginning. Asia has been the birthplace and home to martial arts for the last 5,000 years and ONE Fighting Championship has a vision of bringing mixed martial arts to the 3.9 billion people living in Asia. I am excited by the dominant leadership position of ONE Fighting Championship in Asia and the future of mixed martial arts in this region of the world. MMA is the fastest growing sport on the planet. We intend to showcase some of the most adrenaline-filled, exciting fights in Asia, featuring the best Asian fighters.".


=== Inaugural event ===
The first show, ONE Fighting Championship: Champion vs. Champion, took place at the 12,000 capacity Singapore Indoor Stadium on 3 September 2011. The headline fight was initially scheduled to be Ole Laursen vs Eduard Folayang but Laursen suffered an injury in training and was replaced by A Sol Kwon.
The card also featured a matchup between UFC veterans Phil Baroni and Yoshiyuki Yoshida as well as Gregor Gracie, Leandro Issa, Zorobabel Moreira and Vuyisile Colossa. Folayang suffered a broken nose but beat Kwon by unanimous decision in the main event and was awarded a $5,000 USD 'Performance of the Night' bonus.


=== International expansion ===
ONE FC's first event of 2012 was ONE Fighting Championship: Battle of Heroes and it took place at the BritAma Arena on 11 February with Felipe Enomoto submitting Ole Laursen in the main event. During the course of the year the promotion moved into two more new countries with ONE Fighting Championship: Destiny of Warriors at Stadium Negara in Kuala Lumpur on 23 June, ONE Fighting Championship: Pride of a Nation at Araneta Coliseum in Manila on 31 August as well as a second card in Singapore, ONE Fighting Championship: Rise of Kings on 6 October.


=== Ten-year ESPN Star Sports partnership ===
On 30 January 2012 ONE FC held a press conference in Singapore to announce a ten-year media partnership with ESPN Star Sports which was described as the largest MMA media deal in Asian history. ONE FC events have since been broadcast, either live or on a delayed broadcast, on the ESPN and Star Sports channels. After ESPN Asia was rebranded as Fox Sports Asia in 2014 ONE FC events would be shown across Asia on the Fox Sports channels.


=== First female fight ===
On 31 March 2012 ONE FC held its first female fight with Nicole Chua submitting Jeet Toshi by submission at ONE Fighting Championship: War of the Lions at the Singapore Indoor Stadium.


=== Signing Shinya Aoki and Bibiano Fernandes ===
On 28 June 2012 it was announced that ONE FC had signed two of the most successful fighters in the history of Asian MMA. Reigning Dream (mixed martial arts) champions Shinya Aoki and Bibiano Fernandes both put pen to paper on long term deals with the organization  with the latter rejecting a rival offer from the UFC.


=== ONE FC Network ===
On 26 October 2011 CEO Victor Cui announced the formation of the ONE FC network, an alliance of camps and promotions in the Asia and Oceania continents which includes DREAM, GLORY, URCC, Cage Fighting Championship, BRACE, ROAD Fighting Championship, Team Lakay Wushu, Tiger Muay Thai and MMA, Evolve Mixed Martial Arts, PAK MMA (Pakistan) and Fairtex Gym. Contracted fighters are allowed to compete for any promotion in the network which notably enabled GLORY Sports International presents Dream 18 & Glory 4 Tokyo ~ Special 2012 ~ New Year's Eve, to feature ONE fighters Shinya Aoki, Bibiano Fernandes and Melvin Manhoef.


=== Appointment of Matt Hume ===
On 2 August 2012 ONE FC announced that Matt Hume, who was the rule director and official trainer to both PRIDE Fighting Championships and Dream (mixed martial arts), had been appointed as the Vice President of Operations and Competition. He had previously served as the head official for ONE FC.


=== Introduction of ONE FC titles ===
On 6 October 2012 at ONE Fighting Championship: Rise of Kings ONE FC titles were introduced for the first time. Kotetsu Boku became the organization's first ever lightweight champion after beating Zorobabel Moreira while Soo Chul Kim beat Leandro Issa to win the bantamweight belt.


=== Signing Ben Askren ===
On 9 December 2012 ONE FC announced the signing of undefeated Bellator welterweight champion, two time NCAA Division 1 Wrestling Champion and US Olympian Ben Askren. Askren had been widely expected to sign with the UFC but after a public falling out with Dana White he rejected an offer from the WSOF to sign a 'lucrative' six fight deal with ONE FC.


=== Appointment of Rich Franklin ===
On 28 May 2014 it was announced that former UFC middleweight champion Rich Franklin had become the company's vice president.


=== Further international expansion ===
After returning to Kuala Lumpur, Singapore, Jakarta and Manila in 2013 ONE FC put on shows in several new territories in 2014. On 11 July ONE Fighting Championship: War of Dragons took place at the Taipei Arena in Taipei, on 11 July ONE Fighting Championship: War of Dragons took place at the Taipei Arena in Taipei, On 29 August ONE Fighting Championship: Reign of Champions took place at the Dubai World Trade Centre in Dubai and on 12 September ONE Fighting Championship: Rise of the Kingdom took place at the Koh Pich Theatre in Phnom Penh.


=== Expansion into China ===
On 20 June 2014 ONE FC announced that it would be holding ten events a year on mainland China after signing a breakthrough deal with AMC Live Group. The first of these events took place at the Olympic Sports Centre (Beijing) on 19 December and was titled ONE Fighting Championship: Dynasty of Champions (Beijing)


=== Manny Pacquiao ===
Victor Cui confirmed on 4 August 2014, that eight division boxing world champion Manny Pacquiao had purchased an undisclosed number of shares in the company.


=== Rebranding ===
On 13 January 2015, the promotion announced it would be rebranded as ONE Championship'. According to Victor Cui, the name change was made mostly for linguistic and cultural reasons.


== Rules ==
ONE Championship utilizes the Global MMA Rule Set that combines the PRIDE Rules with the Unified Rules of MMA; allowing for soccer kicks and permitting the use of elbows, stomp kicks to the body and legs but not to the head of a ground opponent. Knees are allowed at any time during the fight, whether the opponent is standing or on the ground.


=== Rounds ===
Each bout will be 3 rounds x 5 minutes per round with a 1-minute break in between rounds. Championship bouts will be 5 rounds x 5 minutes per round with a 1-minute break in between rounds.


=== Judging ===
In the event that a bout goes the distance, it will go to a judges’ decision. The 3 judges will score the bout in its entirety, not round-by-round. Judges will utilize the following ONE judging criteria in descending order of importance to determine the winner of the bout:
Near KO or Submission
Damage (Internal, Accumulated, Superficial)
Striking combinations and cage generalship (Ground control, Superior positioning)
Earned takedowns or takedown defense


=== Cage ===
ONE Championship uses a circular cage.


=== Weight Divisions ===
ONE Championship currently uses ten different weight classes:


== Television broadcast ==
ONE Championship is broadcast by domestic television channels in Asia including MNC International and Indosiar in Indonesia, MediaCorp Channel 5 in Singapore, ABS-CBN Sports+Action ATC @ IBC 13 and GMA 7 in the Philippines, MyTV in Cambodia, and TV9 in Malaysia.
In the American continent, live events of ONE are currently shown on Esporte Interativo and BandSports in Brazil, and The Fight Network in Canada.
On 30 January 2012 it was announced that ONE Championship has signed a deal which would see it being broadcast by ESPN Star Sports for the next 10 years, the longest deal of its kind in the history of Asian MMA. ONE made its PPV debut at ONE Fighting Championship: Rise of Kings via iN DEMAND, Avail-TVN, DirecTV and Dish. ONE events air live on Fox Sports from August 2014 and previously on Star Sports due to re-branding except in China and South Korea where it maintains the Star Sports brand.
It also airs live on Setanta in Australia and on Premier Sports and Box Nation in the UK, Physique TV in the Middle East and Africa. Other TV channels around the world showing live ONE events include Fight Channel in Croatia, Fight Time in Russia, Fox Sports Africa, Setanta Ireland and KombatSports.lu in France.


== Awards ==
In 2012 ONE was nominated for 'Promotion of the Year' at the World MMA Awards alongside the UFC, Bellator, Invicta FC and Strikeforce.
In 2013 ONE won the award for 'Best Engagement Strategy for a Male Audience' from Marketing Magazine.


== ONE Championship events ==


=== Production team ===
ONE CEO is Victor Cui who used to be a senior executive at ESPN Star Sports. Matches are made by matchmaker and Vice President of Operations, Matt Hume. In 2014, ONE Championship announced that Rich Franklin had taken on a role as Vice President in ONE Championship. The head referee is Yuji Shimada and referees are Olivier Coste and Joey Lepiten. The regular colour commentators for the television broadcast are Steve Dawson and Mitch Chilson, although Josh Thomson, Jason Chambers, Bas Rutten and Renzo Gracie have appeared as guest commentators. The event announcer and "Voice of ONE Championship" is Lenne Hardt and the cage announcer is Anthony Suntay. Christine Hallauer, Mel Tan, Kim Hayul and Han Jieun are ring girls.


=== Fighter salary ===
Fighter salaries in Asia are not publicly disclosed, unlike in the US so information about ONE FC's fighter purses is not in the public domain. However, in January, 2014 Ben Askren revealed that he was getting paid a minimum of $50,000 USD per fight as well as a $50,000 USD win bonus.
A cash bonus known as the "ONE Warrior bonus" was introduced on 9 July 2014 and implemented for the first time at ONE Fighting Championship: War of Dragons on 11 July 2014. An award of $50,000 USD is given out at the end of certain events to the fighter who impresses the most in terms of:
thrilling the fans with exciting action,
demonstrating an incredible warrior spirit,
exhibiting amazing skill, and
delivering a phenomenal finish.
Victor Cui stated that bonuses would be handed out on a discretionary basis: "If a few fighters impress me, then I will hand out the bonus to a few fighters. If no one impresses me, then no one will get it which since ONE show.. has never been handed out ever again cause none of the fighters impress me. Extraordinary performance deserves extraordinary rewards. Ordinary performances deserve ordinary rewards."


== Current champions ==

World Champions


== Tournament Winners ==
Malaysian National Champions
Cambodia Grand Prix Champions
Beijing National Champions
Guangzhou National Champions
Myanmar National Champions


== External links ==
Official website
List of Events on Sherdog


== References ==