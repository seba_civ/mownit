The Northern Dynasties tombs of Ci County, Handan, Hebei, China consist of the tombs of members of the royalty and nobility of the Eastern Wei and Northern Qi, two of the Northern Dynasties of Chinese history. The tombs, dating from 386-581, were discovered in 1974. In 1992, the State Council named the group a national key cultural relic.


== Discovery ==
In 1974, Dong Weiyao's Tomb of Zhao was discovered. In 1975, the Tomb of Northern Qi Yao June was discovered and 136 pottery figurines were unearthed. 1979, the discovery of Tomb of Princess Ru Ru and 1056 pottery figurines as well as some pottery horses and ceramic camels. Among the eight golden articles, there are two Byzantine coins which is useful for the study of the relationships between China and Byzantium during that period. There is 320-square-metre mural along the tomb massage, and is about the main content is about the troop that shows the dignity of an emperor's funeral. From 1987 to 1989，Tomb of Emperor of Northern Qi Wenxuan was discovered and more than 1800 pottery figurines were unearthed.


== Connotation ==
The large amount of pottery figurines discovered in Tomb of Princess Ru Ru actually represents the politics of that time. Ru Ru became a strong national in the north in the last years of the Northern Wei. The large amount of funeral objects is not only the representation of the noble life of luxury and the princess identity of honor but also the respect to Ru Ru. It is the politics that behind funeral objects. The pottery figurines that unearthed are mainly Terracotta soldiers which shows the social realism that people suffered from warfare. There are many different people from different nationalities in character figurines which shows wishes of the ruler of ethnic fusion. National amalgamation is the main subject of that time.


== Construction of figures ==
The height of the pottery figurines is usually short. The smallest one is only two inches tall and the big one will be one foot tall. The highest figurine is 1.6 metres high that was discovered in Tomb of Emperor of Northern Qi Wenxuan. Because the scale of tombs that time is small but puts a lot of objects in it and also relates to its financial missteps and other social elements. There are many sorts of sculptures, such as men, animals, quart pots etc. There are about 20 different shapes of humans and they are depicted very carefully even more carefully than Terracotta Army and displays national features.


== Gallery ==


== References ==