Nyoro people are Bantus from Uganda, Eastern Africa. Their kingdom is Bunyoro.


== Name ==
They are mentioned under various names in various sources. Some of their names include Bakitara, Banyoro, Bunyoro, Gungu, Kitara, Kyopi, Nyoros, Ouanyoro, Runyoro, Vouanyoro, Wanyoro.


== Language ==
Their native language is nyoro (or sometimes called runyoro). It is a Bantu language, with 667 000 native speakers as of 2002.


== See also ==
Ethnic groups in Africa


== References ==


== Other sources ==


=== Books ===
David Kihumuro Apuuli, A thousand years of Bunyoro-Kitara Kingdom : the people and the rulers, Fountain Publishers, Kampala, 1994, 144 p.
John Beattie, The Nyoro state, Clarendon Press, Oxford, 1971, 280 p.
A.B.T. Byaruhanga-Akiiki, Religion in Bunyoro, Kenya Literature Bureau, Nairobi, 1982, 256 p. (texte remanié d'une thèse soutenue à l'Université Makerere en 1971)
Brian Kingzett Taylor, The Western Lacustrine Bantu (Nyoro, Toro, Nyankore, Kiga, Haya and Zinza, with sections on the Amba and Konjo), International African Institute, Londres, 1962, 159 p.
John Roscoe, The Bakitara or Banyoro (Mackie ethnological expedition to Central Africa), Gregg, Farnborough, 1968, 370 p. (fac simile de l'éd. de 1923, Cambridge University Press)


=== Discographie ===
Royal Court Music from Uganda (Ganda, Nyoro, Ankole), collecteur Hugh Tracey, Sharp Wood Productions, 1998 (enregistrement 1950-1952)
'Music Of Africa Series No. 8 The Uganda Protectorate', Collected and recorded by Hugh Tracey, DECCA LF1173


== External links ==
(French) « Nyoro (peuple d'Afrique) » (notice RAMEAU, BnF)