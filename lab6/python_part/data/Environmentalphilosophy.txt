Environmental philosophy is a branch of philosophy that is concerned with the natural environment and humans' place within it. It asks crucial questions about human environmental relations such as "What do we mean when we talk about nature?" "What is the value of the natural, that is non-human environment to us, or in itself?" "How should we respond to environmental challenges such as environmental degradation, pollution and climate change?" "How can we best understand the relationship between the natural world and human technology and development?" and "What is our place in the natural world?" As such, it uniquely positions itself as a field set to deal with the challenges of the 21st Century. Environmental philosophy includes environmental ethics, environmental aesthetics, ecofeminism, environmental hermeneutics, and environmental theology. Some of the main areas of interest for environmental philosophers are:

Defining environment and nature
How to value the environment
Moral status of animals and plants
Endangered species
Environmentalism and Deep Ecology
Aesthetic value of nature
Restoration of nature
Consideration of future generations


== Contemporary issues ==
Modern issues within environmental philosophy include but are not restricted to the concerns of environmental activism as well as the questions raised by environmental science and technology. These include issues related to the depletion of finite resources and other harmful and permanent effects brought on to the environment by humans, as well as the ethical and practical problems raised by philosophies and practices of environmental conservation, restoration, and policy in general. At the same time environmental philosophy deals with the value human beings attach to different kinds of environmental experience, particularly how experiences in or close to non-human environments contrast with urban or industrialized experiences, and how this varies across cultures.


== Modern history ==
Environmental Philosophy re-emerged as a major social movement in the 1970s. The movement was an attempt to connect with humanity's sense of alienation from nature in a continuing fashion throughout history. This was very closely related to the development at the same time of ecofeminism, an intersecting discipline. Since then its areas of concern have expanded significantly.
The field is today characterized by a notable diversity of stylistic, philosophical and cultural approaches to human environmental relationships, from personal and poetic reflections on environmental experience and arguments for panpsychism to Malthusian applications of game theory or the question of how to put an economic value on nature's services. A major debate arose in the 1970s and 80s was that of whether nature has intrinsic value in itself independent of human values or whether its value is merely instrumental, with ecocentric or deep ecology approaches emerging on the one hand versus consequentialist or pragmatist anthropocentric approaches on the other.
Another debate that arose at this time was the debate over whether there really is such a thing as wilderness or not, or whether it is merely a cultural construct with colonialist implications. Since then, readings of environmental history and discourse have become more critical and refined. In this ongoing debate, a diversity of dissenting voices have emerged from different cultures across the globe questioning the dominance of Western assumptions, helping to transform the field.
In recent decades, there has been a significant challenge to deep ecology and the concepts of nature that underlie it, some arguing that there is not really such a thing as nature at all beyond some self-contradictory and even politically dubious constructions of an ideal other that ignore the real human-environmental interactions that shape our world and lives. This has been alternately dubbed the postmodern, constructivist, and most recently post-naturalistic turn in environmental philosophy. Environmental aesthetics, design and restoration have emerged as important intersecting disciplines that keep shifting the boundaries of environmental thought, as have the science of climate change and biodiversity and the ethical, political and epistemological questions they raise. Today, environmental philosophy is a burgeoning and increasingly relevant field.


=== Deep ecology movement ===

In 1984, George Sessions and Arne Naess articulated the principles of the new Deep Ecology Movement. These basic principles are:
The well-being and flourishing of human and non-human life have value.
Richness and diversity of life forms contribute to the realization of these values and are also values in themselves.
Humans have no right to reduce this richness and diversity except to satisfy vital needs.
The flourishing of human life and cultures is compatible with a substantial decrease in the human population.
Present human interference with the nonhuman world is excessive, and the situation is rapidly worsening.
Policies must therefore be changed. These policies affect basic economic, technological, and ideological structures. The resulting state of affairs will be deeply different from the present.
The ideological change is mainly that of appreciating life quality (dwelling in situations of inherent value), rather than adhering to an increasingly higher standard of living. There will be a profound awareness of the difference between big and great.
Those who subscribe to the foregoing points have an obligation directly or indirectly to try to implement the necessary changes.


== See also ==
Environmental Philosophy (journal)
Environmental Values
Environmental Ethics (journal)
List of environmental philosophers
Environmental hermeneutics


== References ==


=== Notes ===


=== Further reading ===
Armstrong, Susan, Richard Botzler. Environmental Ethics: Divergence and Convergence, McGraw-Hill, Inc., New York, New York. ISBN 9780072838459.
Benson, John, 2000. Environmental Ethics: An Introduction with Readings, Psychology Press.
Callicott, J. Baird, and Michael Nelson, 1998. The Great New Wilderness Debate, University of Georgia Press.
Derr, Patrick, G, Edward McNamara, 2003. Case Studies in Environmental Ethics, Bowman & Littlefield Publishers. ISBN 0-7425-3136-8
DesJardins, Joseph R., Environmental Ethics Wadsworth Publishing Company, ITP, An International Thomson Publishing Company, Belmont, California. A Division of Wadsworth, Inc.
Devall, W. and G. Sessions. 1985. Deep Ecology: Living As if Nature Mattered, Salt Lake City: Gibbs M. Smith, Inc.
Drengson, Inoue, 1995. "The Deep Ecology Movement," North Atlantic Books, Berkeley, California.
Foltz, Bruce V., Robert Frodeman. 2004. Rethinking Nature, Indiana University Press, 601 North Morton Street, Bloomington, IN 47404-3797 ISBN 0-253-21702-4
Keulartz, Jozef, 1999. The Struggle for Nature: A Critique of Environmental Philosophy, Routledge.
LaFreniere, Gilbert F, 2007. The Decline of Nature: Environmental History and the Western Worldview, Academica Press, Bethesda, MD ISBN 978-1933146409
Light, Andrew, and Eric Katz,1996. Environmental Pragmatism, Psychology Press.
Mannison, D., M. McRobbie, and R. Routley (ed), 1980. Environmental Philosophy, Australian National University
Matthews, Steve, 2002. A Hybrid Theory of Environmentalism, Essays in Philosophy, 3. Onlinehttp://commons.pacificu.edu/cgi/viewcontent.cgi?article=1038&context=eip
Næss, A. 1989. Ecology, Community and Lifestyle: Outline of an Ecosophy, Translated by D. Rothenberg. Cambridge: Cambridge University Press.
Oelschlaeger, Max, 1993. The Idea of Wilderness: From Prehistory to the Age of Ecology, New Haven: Yale University Press, ISBN 978-0300053708
Pojman, Louis P., Paul Pojman. Environmental Ethics, Thomson-Wadsworth, United States
Sherer, D., ed, Thomas Attig. 1983. Ethics and the Environment, Prentice-Hall, Inc., Englewood Cliffs, New Jersey 07632. ISBN 0-13-290163-3
Vasconcelos, Vitor Vieira "The Environment Professional and the Touch with Nature." Qualit@s, v 1, n 1, 2010.
VanDeVeer, Donald, Christine Pierce. The Environmental Ethics and Policy Book, Wadsworth Publishing Company. An International Thomson Publishing Company
Vogel, Steven, 1999. "Environmental Philosophy After the End of Nature," Environmental Ethics 24 (1):23-39
Weston, 1999. "An Invitation to Environmental Philosophy," Oxford University Press, New York, New York.
Zimmerman, Michael E., J. Baird Callicott, George Sessions, Karen J. Warren, John Clark. 1993.Environmental Philosophy: From Animal Rights to Radical Ecology, Prentice-Hall, Inc., Englewood Cliffs, New Jersey 07632 ISBN 0-13-666959-X


== External links ==
"American Wilderness Philosophy" entry in the Internet Encyclopedia of Philosophy