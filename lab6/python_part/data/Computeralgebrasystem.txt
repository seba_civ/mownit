A computer algebra system (CAS) is a software program that allows computation over mathematical expressions in a way which is similar to the traditional manual computations of mathematicians and scientists. The development of the computer algebra systems in the second half of the 20th century is part of the discipline of "computer algebra" or "symbolic computation", which has spurred work in algorithms over mathematical objects such as polynomials.
Computer algebra systems may be divided in two classes: the specialized ones and the general purpose ones. The specialized ones are devoted to a specific part of mathematics, such as number theory, group theory, or teaching of elementary mathematics.
General purpose computer algebra systems aim to be useful to a user working in any scientific field that requires manipulation of mathematical expressions. To be useful, a general purpose computer algebra system must include various features such as
a user interface allowing to enter and display mathematical formulas
a programming language and an interpreter (the result of a computation has commonly an unpredictible form and an unpredictible size; therefore user intervention is frequently needed)
a simplifier, which is a rewrite system for simplifying mathematics formulas
a memory manager, including a garbage collector, needed by the huge size of the intermediate data, which may appear during a computation
an arbitrary-precision arithmetic, needed by the huge size of the integers that may occur
a large library of mathematical algorithms
The library must cover not only the needs of the users, but also the needs of the simplifier. For example, the computation of polynomial greatest common divisors is systematically used for the simplification of expressions involving fractions.
This large amount of required computer capabilities explains the small number of general purpose computer algebra systems. The main ones are Axiom, Macsyma, Magma, Maple, Mathematica and Sage.


== History ==
Computer algebra systems began to appear in the 1960s, and evolved out of two quite different sources—the requirements of theoretical physicists and research into artificial intelligence.
A prime example for the first development was the pioneering work conducted by the later Nobel Prize laureate in physics Martinus Veltman, who designed a program for symbolic mathematics, especially High Energy Physics, called Schoonschip (Dutch for "clean ship") in 1963. Another early system was FORMAC.
Using LISP as the programming basis, Carl Engelman created MATHLAB in 1964 at MITRE within an artificial intelligence research environment. Later MATHLAB was made available to users on PDP-6 and PDP-10 Systems running TOPS-10 or TENEX in universities. Today it can still be used on SIMH-Emulations of the PDP-10. MATHLAB ("mathematical laboratory") should not be confused with MATLAB ("matrix laboratory") which is a system for numerical computation built 15 years later at the University of New Mexico, accidentally named rather similarly.
The first popular computer algebra systems were muMATH, Reduce, Derive (based on muMATH), and Macsyma; a popular copyleft version of Macsyma called Maxima is actively being maintained. Reduce became free software in 2008. As of today, the most popular commercial systems are Mathematica and Maple, which are commonly used by research mathematicians, scientists, and engineers. Freely available alternatives include Sage (which can act as a front-end to several other free and nonfree CAS).
In 1987, Hewlett-Packard introduced the first hand held calculator CAS with the HP-28 series, and it was possible, for the first time in a calculator, to arrange algebraic expressions, differentiation, limited symbolic integration, Taylor series construction and a solver for algebraic equations. In 1999, the independently developed CAS Erable for the HP 48 series became an officially integrated part of the firmware of the emerging HP 49/50 series, and a year later into the HP 40 series as well, whereas the HP Prime adopted the Xcas system in 2013.
The Texas Instruments company in 1995 released the TI-92 calculator with a CAS based on the software Derive; the TI-Nspire series replaced Derive in 2007. The TI-89 series, first released in 1998, also contains a CAS.


== Symbolic manipulations ==
The symbolic manipulations supported typically include:
simplification to a smaller expression or some standard form, including automatic simplification with assumptions and simplification with constraints
substitution of symbols or numeric values for certain expressions
change of form of expressions: expanding products and powers, partial and full factorization, rewriting as partial fractions, constraint satisfaction, rewriting trigonometric functions as exponentials, transforming logic expressions, etc.
partial and total differentiation
some indefinite and definite integration (see symbolic integration), including multidimensional integrals
symbolic constrained and unconstrained global optimization
solution of linear and some non-linear equations over various domains
solution of some differential and difference equations
taking some limits
integral transforms
series operations such as expansion, summation and products
matrix operations including products, inverses, etc.
statistical computation
theorem proving and verification which is very useful in the area of experimental mathematics
optimized code generation
In the above, the word some indicates that the operation cannot always be performed.


== Additional capabilities ==
Many also include:
a programming language, allowing users to implement their own algorithms
arbitrary-precision numeric operations
exact integer arithmetic and number theory functionality
Editing of mathematical expressions in two-dimensional form
plotting graphs and parametric plots of functions in two and three dimensions, and animating them
drawing charts and diagrams
APIs for linking it on an external program such as a database, or using in a programming language to use the computer algebra system
string manipulation such as matching and searching
add-ons for use in applied mathematics such as physics, bioinformatics, computational chemistry and packages for physical computation
Some include:
graphic production and editing such as computer generated imagery and signal processing as image processing
sound synthesis
Some computer algebra systems focus on a specific area of application; these are typically developed in academia and are free. They can be inefficient for numeric operations compared to numeric systems.


== Types of expressions ==
The expressions manipulated by the CAS typically include polynomials in multiple variables; standard functions of expressions (sine, exponential, etc.); various special functions (Γ, ζ, erf, Bessel functions, etc.); arbitrary functions of expressions; optimization; derivatives, integrals, simplifications, sums, and products of expressions; truncated series with expressions as coefficients, matrices of expressions, and so on. Numeric domains supported typically include real, integer, complex, interval, rational, and algebraic.


== Use in education ==
There have been many advocates for increasing the use of computer algebra systems in primary and secondary school classrooms. The primary reason for such an advocacy is that computer algebra systems represent real-world math moreso than paper-and-pencil or hand calculator based mathematics. This push for increasing computer usage in mathematics classrooms has been supported by certain boards of education. It has even been mandated in the curriculum of certain regions.
Computer algebra systems have been extensively used in higher education. Many universities offer either specific courses on developing their use, or they implicitly expect students to use them for their course work. The companies that develop computer algebra systems have pushed to increase their prevalence among university and college programs.
CAS-equipped calculators are not permitted on the ACT, the PLAN, and in some classrooms though it may be permitted on all of College Board's calculator-permitted tests, including the SAT, some SAT Subject Tests and the AP Calculus, Chemistry, Physics, and Statistics exams.


== Mathematics used in computer algebra systems ==
Symbolic integration via e.g. Risch algorithm or Risch-Norman algorithm
Hypergeometric summation via e.g. Gosper's algorithm
Limit computation via e.g. Gruntz's algorithm
Polynomial factorization via e.g., over finite fields, Berlekamp's algorithm or Cantor–Zassenhaus algorithm.
Greatest common divisor via e.g. Euclidean algorithm
Gaussian elimination
Gröbner basis via e.g. Buchberger's algorithm; generalization of Euclidean algorithm and Gaussian elimination
Padé approximant
Schwartz–Zippel lemma and testing polynomial identities
Chinese remainder theorem
Diophantine equations
Quantifier elimination over real numbers via e.g. Tarski's method/Cylindrical algebraic decomposition
Landau's algorithm
Derivatives of elementary and special functions. (e.g. See Incomplete Gamma function.)
Cylindrical algebraic decomposition


== See also ==

List of computer algebra systems
Scientific computation
Statistical package
Automated theorem proving
Artificial intelligence
Constraint-logic programming
Satisfiability modulo theories


== References ==


== External links ==
Definition and workings of a computer algebra system
Curriculum and Assessment in an Age of Computer Algebra Systems - From the Education Resources Information Center Clearinghouse for Science, Mathematics, and Environmental Education, Columbus, Ohio.
Richard J. Fateman. "Essays in algebraic simplification". Technical report MIT-LCS-TR-095, 1972. (Of historical interest in showing the direction of research in computer algebra. At the MIT LCS web site: [1])