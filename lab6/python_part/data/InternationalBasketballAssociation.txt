The International Basketball Association (IBA) was founded by Alexandria, Minnesota entrepreneur Thomas Anderson in 1995. Anderson traveled the Upper Midwest searching for franchise owners for a couple of years before the league began play with five teams in the 1995-1996 season. The original owners of franchises in the league were George Daniel (Black Hills Posse-Rapid City, SD), John Korsmo, Al Gardner, and Al Hovland (Fargo Beez), Jeff McCarron (St. Cloud Rock 'n Rollers), Bill Sorenson (Dakota Wizards - Bismarck) and Earl Barish (Winnipeg Cyclones). Earl Barish of Winnipeg directed the IBA as League President and the league eventually grew to ten franchises. The founder, Thomas Anderson was granted a franchise along with partner Curt Zimbleman in Minot, North Dakota (Magic City Snowbears) for the 1996-1997 season. In the fall of 2001, CBA and IBL teams merged with the IBA and purchased the assets of the defunct CBA, including its name, logo and records from the bankruptcy court and restarted operations, calling itself the CBA. This group continues to operate today with the Dakota Wizards the only surviving founding member.


== IBA League Membership ==


== League Championship ==
The IBA merged with the International Basketball League and the Continental Basketball Association (CBA) to "restart" the CBA for the 2001-2002 season.


== Awards ==


=== Most valuable player ===
1995-96 - Isaac Burton (Black Hills Posse)
1996-97 - Dennis Edwards (Black Hills Posse)
1997-98 - Andrell Hoard (Winnipeg Cyclone) and Mike Lloyd (Mansfield Hawks)
1998-99 - Andrell Hoard (Winnipeg Cyclone) and Mike Lloyd (Mansfield Hawks)
1999-00 - Brian Green (Dakota Wizards)
2000-01 - Lonnie Cooper (Des Moines Dragons)


=== Coach of the year ===
1995-96 - Duane Ticknor (Black Hills Posse)
1996-97 - Duane Ticknor (Black Hills Posse)
1997-98 - Duane Ticknor (Black Hills Posse)
1998-99 - Darryl Dawkins (Winnipeg Cyclone) and Kevin Mackey (Mansfield Hawks)
1999-00 - Duane Tickor (Dakota Wizards)
2000-01 - Dave Joerger (Dakota Wizards) and Mike Born (Des Moines Dragons)


== Notable players ==
Chris Andersen
Damon Jones
Garth Joseph
Anthony Taylor


== See also ==
Continental Basketball Association


== External links ==
International Basketball Association History at Association for Professional Basketball Research