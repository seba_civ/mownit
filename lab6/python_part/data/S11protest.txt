S11 refers to a series of protests against meetings of the World Economic Forum on 11, 12 and 13 September 2000 in Melbourne, Australia, where approximately 10,000 people of many ages and a wide cross section of the community were involved. One of the groups involved in the protests called itself the S11 Alliance [1]. This group was dominated by various socialist parties. The success of the protest led them to the creation of the M1 Alliance on 1 November 2000 in preparation for the next year's May Day events [2] and the S26 Alliance [3], in solidarity with protest against the International Monetary Fund/World Bank meetings in Prague (26 September 2000). The other main organising network for the protest was the autonomist & anarchist 's11 AWOL' [4].
The protest was part of the anti-globalisation movement, and closely modeled after the 1999 WTO protests in Seattle. This movement has been motivated by a number of concerns about globalization, including loss of national sovereignty, environmental impact, and the impact of foreign debt and multi-national corporations on third world countries.
The protest was notable as the first major anti-globalization action to take place in Australia. As with previous events in other parts of the world, the event was characterized by civil disobedience and often festive direct action, and by an aggressive police response. Protesters linked arms and some chained themselves together in an effort to prevent delegates from getting into the meetings [5]. The police reportedly responded with pepper spray (which in Victoria is not permitted to be used by police at demonstrations), and Green Party of Aotearoa New Zealand MP Nandor Tanczos reported that he and other protesters were victims of police brutality [6].


== Police Response ==
During the S11 protests there was a significant media presence, and some reporters and photographers were attacked by police during their attempts to break up the protest. This resulted in a higher degree of reporting than that which normally occurs at similar protests, and a large amount of photographic and video evidence of the event was available. In a report regarding the police response, the Ombudsman's Report [7] stated "It was obvious that the event had been one of the most photographed events in recent memory and there was likely to be a great deal of video footage."
The majority of the police removed their name tags, a contravention of Victoria Police Operating Procedures. This prevented individual police from being identified in later hearings regarding police actions.
Video footage showed punches and overhead baton strikes being used against the demonstrators, and several were stomped or kicked while upon the ground and/or dragged by their hair. Several demonstrators were struck without warning while not engaged in violent activity. These were all violations of police procedures.
During a "baton charge", police struck camera operators from the Seven Network and SBS while they were filming. This footage was shown on the respective stations. Photographers from The Age and Herald Sun newspapers claimed that their camera equipment was removed from them and smashed by police during the charge.
A woman was run over by an unmarked police car with uniformed officers inside. The protesters claimed that the car was stationary until it suddenly accelerated over the woman and sped off. Police claimed that the protesters surrounded a moving car and were rocking and damaging it. However, the footage taken by a nearby camera crew clearly bore out the protesters' version of events.
The 40 person Legal Observer Team present over the three days made two conclusions in its report [8] published after the event:
1) It is the conclusion of the Pt’chang Legal Observer Team, that, in the almost complete absence of police attempts to arrest individual protesters who broke the law, individual police officers were taking opportunities afforded by the chaotic nature of the event, crowd numbers, isolation or the lack of accountability to senior police, and effectively meting out their own ‘summary’, extra judicial punishment’ to individual protestors. Buoyed by, and perhaps made fearful by the mediagenerated myths of ‘violent S11 protestors’ and briefed by their commanding officers of much the same, some police officers were able to take full advantage of every opportunity to assault, intimidate and harass individual protesters whilst on duty.
2) The Legal Observer Team strongly asserts that the Victorian Police command decisions to deploy the level of force observed during these attempts to clear access points was both entirely unjustified and unprovoked and poorly and dangerously executed. The decision to use the fully equipped Force Response Unit, with full body and face protection, and lines of mounted police to carry out a simple objective of clearing an access point stands out as particularly unjustified when a procedure of arresting people engaged in unlawful obstruction could have been implemented with no injuries to people and nor threat towards individual police. It is the conclusion of the Legal Observer Team that the use of batons, surprise formation charges and the use of containment lines of mounted police a) was potentially lethal and resulted in an incredibly high level of serious personal injuries amongst protestors present at these incidents; and b) served to create a highly emotive, dangerous and provocative climate during and immediately after each manoeuvre.


== See also ==
Anti-globalization movement
List of demonstrations against corporate globalization
Legal observer
Police Brutality


== External links ==
s11.org website
s11 AWOL website
GreenLeft coverage of the S11 protest
A Psychologists' Report on the Post-Traumatic Effects of Police Violence on Protestors at the World Economic Forum
S11 Protestors Keeping Delegates Out
An Activist's Diary: The Battle Of Melbourne
S11, S26, M1
World Economic Forum on 11 September 2000
Protests against World Economic Forum at Melbourne Crown Casino 11 - 13 September
Photos from the s11 protest
Police claim S11 violence - but only pies
Police Threaten to Shutdown Anti-WTO Websites
S11 legal support team
Pt'chang Legal Observer Team Report: World Economic Forum Protests
[9] Ombudsman's Report
Beating Up. A Report on Police Batons and the News Media at the World Economic Forum, Melbourne, September 2000 Dr. Bernard Barrett, Historian
s11.org website - National Library Pandora Archive dated 14 September 2000