Karum (Akkadian: kārum "quay, port, commercial district", plural kārū, from Sumerian kar "fortification (of a harbor), break-water") is the name given to ancient Assyrian trade posts in Anatolia (modern Turkey) from the 20th to 18th centuries BC. The main centre of karum trading was at the ancient town of Kanesh.


== Assyrian settlements ==

Early references to karū come from the Ebla tablets; in particular, a vizier known as Ebrium concluded the earliest treaty fully known to archaeology, known variously as the "Treaty between Ebla and Aššur" or the "Treaty with Abarsal" (scholars have disputed whether the text refers to Aššur or to Abarsal, an unknown location). In either case, the other city contracted to establish karū in Eblaite territory (Syria), among other things.
Sargon the Great, who likely destroyed Ebla soon after this, is said in a much later Hittite account to have invaded Anatolia to punish Nurdaggal the king of Burushanda for mistreating the Akkadian and Assyrian merchant class in the karū there. However, this is not given the weight of a contemporary source.
During the second millennium BC, Anatolia was under the sovereignty of Hatti city states and, later, the Hittites. By 1960 BC, Assyrian merchants had established the karū, small colonial settlements next to Anatolian cities which paid taxes to the rulers of the cities. There were also smaller trade stations which were called mabartū (singular mabartum). The number of karū and mabartū was probably around twenty. Among them were Kültepe (Kanesh in antiquity) in modern Kayseri Province; Alişar Hüyük (Ankuva (?) in antiquity) in modern Yozgat Province; and Boğazköy (Hattusa in antiquity) in modern Çorum Province. (However, Alişar Hüyük was probably a mabartum.) But after the establishment of the Hittite Empire, the karū disappeared from Anatolian history.


== Trade ==

In the second millennium BC, money was not yet invented, and Assyrian merchants used gold for wholesale trade and silver for retail trade. Gold was considered eight times more valuable than silver. But there was one more metal, amutum, which was even more valuable than gold. Amutum is thought to be the newly discovered iron and was forty times more valuable than silver. The most important Anatolian export was copper, and the Assyrian merchants sold tin and clothing to Anatolia.


== Legacy ==
The name Karum is given to an upscale shopping mall in Çankaya district of modern day Ankara, Turkey. This is a reference to the presence of karū in Asia minor, since the very early days of history. Another mall in Ankara's Bilkent district is also given the name Ankuva. This is also a reference to archaeological discoveries of various karū in Central Anatolia.


== References ==