Europark Idroscalo Milano is an amusement park in Segrate, Italy, near Milan. Opened to the public for the first time in 1965 (as Lunapark Milano), the park is a popular stop on Via Rivoltana near Idroscalo Lake. It is open seven days a week from March through September and on weekends throughout the year.


== Attractions ==
Within the triangular park layout are roller coasters, a midway (alongside railroad tracks that form the back boundary of the property), a house of mirrors, a Ferris wheel, bumper cars (one for adults, "Miniautos" for school age children, and "Baby Karts" for small children), merry-go-rounds (and similar rides), two flume rides, house of horrors, go-karts, a trip simulator, and various other fantasy and thrill rides. Each ride requires the purchase of a ticket (prices vary between one and ten euros per trip, depending on the ride itself).
In addition, the park features a restaurant and a pub. Near the center of the park grounds is an enclosed "Playground" including plastic tunnels through which small children could crawl while their parents watch through a screen.


== History ==
Lunapark Milano opened its gates for the first time in 1965 for a limited (45 day) season. In response to its growing success, its season expanded to two months in 1966 and three months in 1967. Lights were erected in the park in 1968, allowing the park to stay open into September.
Lunapark in the 1960's by Paolo Monti

In 1970, Lunapark Milano adopted its current schedule (open seven days a week March through September) and every weekend throughout the year as it declared itself to be a "permanent park." In the ensuing years, attendance grew rapidly as rides and other attractions were added to the grounds. The increasing cost of the new attractions caused a rift between the owners of the park and the concessionaires, culminating in a change of management (afterwards called "Luna Park Management") in 1977. The relationship between management and concessions became free of contention as the vendors agreed to pay higher rent as management continued to add attractions through 1980.
By 2002, the finances of the park became precarious to the point of the management giving the concessionaires an ultimatum: anybody not renewing his lease would be evicted immediately and the attraction dismantled. The evictions were then deferred for one year; Lunapark Milano closed in 2003, seemingly permanently. New owners took over management of the park and renamed it Luna Europark Idroscalo Milano in 2004.
Since then, an upgrading that the park had not seen for two decades resulted in a restoration of the crowds that were lost since the early 1970s. Technology – both in rides and in communications – was added to the park's offerings as it started to exhibit motion simulators and an adult-oriented house of horrors. Live entertainment made its appearance in Europark Idroscalo Milano, as did larger and flashier rides, including the 30 meters (100 feet) tall "Super Frisbee."


== References ==