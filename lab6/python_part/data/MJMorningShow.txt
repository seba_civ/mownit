"The MJ Morning Show" was a morning radio show that originated from 93.3 FLZ in Tampa, Florida and was also broadcast in cities such as St. Louis, Jacksonville, and Melbourne. The show was previously known as "The MJ and BJ Morning Show" until former cohost BJ Harris left the show. The show featured Todd Schnitt's MJ persona and sidekicks Froggy, Hurricane "Hitman" Stevens, Fester and Meredith. Dave "Dave the Dwarf" Flood also made random appearances on the show.
On January 19, 2012, Schnitt announced that after 18 years, he was stepping away from the morning show, in order to concentrate his energy on his afternoon talk show. His final broadcast as MJ Kelli was on February 17, 2012


== Previous Crew Members ==


=== "Hurricane" Stevens ===
Hurricane Stevens is the executive technical producer and air personality of the morning show. He is also the webmaster, handling the majority of the content that is posted on the show's official website. He and MJ have been on the show since its inception on February 7, 1994. He started at WFLZ-FM in 1990 as "Boner."


=== Fester ===
Fester, formerly known as Moose, is an additional cast member of the MJ Morning Show. In addition to his role on the morning show, Fester (using the name “Tony Fatso”) hosts his own radio show called "On the Grill", heard on Saturday mornings, WFLA, which focuses on barbecue and grilling. He also makes occasional guest appearances on the nationally syndicated Cigar Dave Show, also based out of WFLA. Outside of the radio stations, Fester is also a franchisee of "Planet Beach," a tanning salon in Tampa, FL. As part of a stunt involving frying a turkey in a van, he was reportedly fired on December 18, 2009, but this turned out to be untrue. His Saturday show "On the Grill" aired its final episode on April 16, 2011.


=== Froggy 2 ===
Froggy returned to the show on Wednesday August 16, 2006, after approximately eighteen months away. After originally leaving of the show, he went to Orlando to do a nighttime radio show. He then returned to Tampa to do an evening radio show on another Clear Channel station, 98 Rock (WXTB). Froggy makes fake prank calls, involving a few characters such as Rob Ripple, a roid raged father who can apparently pick up buildings and loves to suplex while flexing his arms to intimidate his prank victims. Another character commonly heard on the morning show is Dementia Don, a 97-year-old man with dementia whose decision making is affected by his mental disorder. Other characters portrayed by Froggy include, but are not exclusive to, Maniac Marty, Hal Herman, Donald (an old man who his "neighbor" never helps), and Biscuit the Bodyguard.


=== Meredith ===
Meredith joined the cast of the show in late 2007, after serving as its affiliate producer in Jacksonville at 97.9 KISSFM.


=== Dave The Dwarf ===
Dave "Dave the Dwarf" Flood is a 3-foot, 2-inch dwarf who appears on the show in-studio or on the phone. Flood regularly makes appearances at clubs, parties and other events around the Tampa Bay area. He has one daughter.


=== BJ Harris ===
BJ Harris was the co-host of the MJ & BJ Morning Show until February 7, 2001. Harris now works as the co-host of The Alice Morning Show on 105.9 FM KALC in Denver. Harris appeared in studio with MJ for two reunion shows on Oct. 22 and 23, 2009.


=== Hoover ===
Hoover took over as executive producer after Joey B. left the first time.


=== Froggy 1 ===
Froggy #1 worked at WFLZ on the MJ & BJ Show until 2002 when he relocated with his wife and kids to Miami. Froggy has since been at sister station Y100. Froggy was the executive producer for Kenny and Footy in the Morning on Y100. When Kenny Walker was let go from Y100, Footy retired . Elvis Duran & The Morning Zoo (simulcast from sister station WHTZ Z100 in New York City) began airing on Monday, May 22, 2006, making Miami its first syndicated market. Froggy became a part of the Elvis Duran show, the only cast member not located in the Z100 studios in New York City.


=== Flunkie ===
Flunkie worked with Joey B in Philadelphia before being recommended to MJ for this show. Flunkie played the sounds and music for the show, and also captured audio clips from television and news shows that he played during the broadcast when MJ discusses the stories behind them.


=== Jabberjaw ===
Jabberjaw joined the MJ Morning Show in 2005 as an intern and was later hired as an assistant producer. Originally from Wisconsin and commonly checked with for pronunciations of Wisconsin cities, she often provided stories for discussion. She was fired and left the show on Friday August 11, 2008. She then went to The Bubba The Love Sponge show,


=== Joey B ===
Joey B was the executive producer of the show. On January 4, 2006, he officially returned to the show as executive producer after working at stations in Miami and Philadelphia. He also spent 1998–2001 as the producer of the MJ & BJ Morning Show. He is also referred to by MJ as Executive Reducer instead of his position as Executive Producer.


== Music heard on the show ==
The show's background music included theme songs from game shows such as The $10,000 Pyramid, The Price Is Right (used the 1972-2007 theme), Match Game (used the alternate theme from the 1973-82 CBS and syndicated versions), the FOX reality show American Idol, Wheel of Fortune (used the 1983-89 theme on rare occasions), and the original 1976-85 ABC and syndicated versions of Family Feud with Richard Dawson (used the win cue edit).
One of the show's past opening themes was the same as on the NBC drama Third Watch. Sound effects on the show included a buzzer from the 1999 revival of Feud and the bell and original losing horns from Price. When comedian Drew Carey replaced Bob Barker as the host of Price in 2007, the theme and sound effects from Barker's version were still heard on the show.


== References ==


== External links ==