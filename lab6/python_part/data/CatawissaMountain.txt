Catawissa Mountain is a mountain in Columbia County, Pennsylvania, in the United States. Its peak height is 1,873 feet (571 m) above sea level, making it the fifth-highest mountain in Columbia County. Streams that flow near the mountain include Roaring Creek, Catawissa Creek, and some of its tributaries. The mountain is near Nescopeck Mountain and Little Mountain. Sandstones and rock formations such as the Pocono Formation occur on Catawissa Mountain.
The community of Catawissa is near Catawissa Mountain. There have also been several roads on or near the mountain in the past. In more recent times, all terrain vehicle trails were established there, as was Pennsylvania State Game Lands Number 58. There are vernal pools on the mountain, which are used as breeding grounds for amphibians. The mountain is part of a much larger tract of forest.
Catawissa Mountain is located in southeastern Catawissa Township, southwestern Main Township, eastern Roaring Creek Township, among other places.


== Geography ==

Catawissa Mountain's southeastern edge is at the border between Luzerne County and Columbia County. From here, it runs northwest through Beaver Township and Roaring Creek Township to the community of Catawissa. A spur of the mountain can be said to cross the Susquehanna River and turn west into Montour County, where it ceases to be a mountain.
The peak height of Catawissa Mountain is 1,873 feet (571 m) above sea level. However, numerous places on the mountain rise to over 1,500 feet (460 m) above sea level. The mountain is the fifth-highest mountain in Columbia County, behind Red Rock Mountain, Central Mountain, Chimneystack Rock, and Buck Mountain.
The community of Catawissa is close to Catawissa Mountain. Main Township and Locust Township are also located near the mountain.


== Geology ==
Catawissa Mountain is relatively short and rounded, especially compared to nearby mountains such as Huntington Mountain, which are straight and narrow ridges. Catawissa Mountain has some rocky outcroppings.
There are two zigzagging outcroppings of the Pocono Formation on Catawissa Mountain. These outcroppings pass through the mountain and south into Schuylkill County. They also have spurs and knobs at each point where they zigzag. The Northumberland Syncline passes through a spur of the mountain. The mountain also has an elbow which is part of a double synclinal that heads eastward to Nescopeck Mountain.
A coarse and hard sandstone can be found on Catawissa Mountain. Sandstone of the Pocono Formation occurs on the summit of the mountain.
Catawissa Mountain is part of a chain of mountains running from beyond Dauphin to Carbondale. The mountain is located near both Nescopeck Mountain and Little Mountain. The meeting point of Catawissa Mountain and Nescopeck Mountain is on the edge of a canoe valley.


== Hydrology ==
Several tributaries of the lower reaches of Catawissa Creek originate on Catawissa Mountain. A number of these tributaries are affected by acid deposition. Roaring Creek is also close to the western side of the mountain.
Catawissa Creek cuts a gap between Catawissa Mountain and Nescopeck Mountain at the community of Mainville. The creek also flows parallel to Catawissa Mountain downstream of this gap.
There are vernal pools on Catawissa Mountain in Beaver Township and Roaring Creek Township.


== History and etymology ==
The word Catawissa comes from the Algonquin word Gatawissi, meaning "growing fat".
There was at least one Native American village in Catawissa, near Catawissa Mountain, and possibly several villages during different time periods. Historically, a road known as the Mine Gap Road climbed up Catawissa Mountain. In the past, a road led from the valley of the Schuylkill River to Catawissa via the southern part of the mountain. The Reading Road also passed from Catawissa to Philadelphia via the mountain.
J.H. Beers stated in his 1915 book Historical and Biographical Annals of Columbia and Montour Counties, Pennsylvania that the height of Catawissa Mountain was estimated to be 1,600 feet (490 m). The same author described the mountain as "majestic" and "towering" in his book.
Since at least 2001, the Columbia-Montour Amateur Radio Club has had a receiver on Catawissa Mountain.


== Biology ==
Catawissa Mountain is one of several places in Columbia County that has unbroken tracts of forest. It is part of a forested corridor between the Susquehanna River and Moosic Mountain.
Amphibians use the vernal pools on Catawissa Mountain as breeding grounds, including American toads, wood frogs, and spotted salamanders.
In Pennsylvania State Game Lands Number 58, the forests on Catawissa Mountain are hardwood/heath forests. Trees in these forests include red maple, chestnut oak, scarlet oak, mountain laurel, black huckleberry, and lowbush blueberry. Numerous plant species inhabit the mountain's vernal pools. These include cranberry, cinnamon fern, royal fern, tussock sedge, pitch pine, and numerous others.


== Recreation ==
The Pennsylvania State Game Lands Number 58 are located on Catawissa Mountain. Use of all terrain vehicles on the mountain is increasing, as of 2004. The Columbia County Natural Heritage Inventory of 2004 suggested blocking all terrain vehicle trails near the mountain's vernal pools.


== See also ==
Knob Mountain (Pennsylvania)
North Mountain (Pennsylvania)


== References ==


== External links ==
Topographical map of the peak of Catawissa Mountain