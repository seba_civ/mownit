WSLA is a Sports Talk outlet serving the New Orleans area. The station, which is owned by Mapa Broadcasting, operates at 1560 kHz with 1 kW/Daytime-only power and is licensed to Slidell, Louisiana.


== History ==
WSLA AM 1560, originally WSDL, was founded by George Mayoral (a New Orleans radio and television pioneer and member of the NAB Hall Of Fame) in the 1960s. It began broadcasting as WBGS, owned by Bill Garrett, a Slidell Chevrolet dealer, in 1963. The station's studios and transmitter were originally housed on the showroom floor of the Chevrolet dealership on Pontchartrain Drive. As WSDL, the station was sold in the 1980s and became WSLA on February 27, 1989. It was later repossessed by the Mayoral family in 1993. WSLA has been through various talk, political, local high school and college sports formats. WSLA remains one of the last independent stations in the New Orleans DMA, completely owned by New Orleans indie MAPA Broadcasting. WSLA is a primary outlet for high school football in the Slidell/St. Tammany Parish area. In recent years the station has been the New Orleans market host for the Houston Astros.
WSLA's offices, transmitter, and tower are located in Slidell, LA. The station experienced flooding and some roof damage during Hurricane Katrina, all of which has since been repaired.


== External links ==

Query the FCC's AM station database for WSLA
Radio-Locator Information on WSLA
Query Nielsen Audio's AM station database for WSLA