Founded in 2005 by Valérie-Anne Giscard d'Estaing, Photo12 Galerie is dedicated to humanistic contemporary photography.


== Specialized in humanistic contemporary photography ==
The gallery is made up of two exhibition areas occupying around 120 m2 (1,300 sq ft), and respectively located at 10 and 14 rue des Jardins Saint-Paul in the historic heart of the 4th arrondissement of Paris, the Marais. The gallery is part of the Marais Photo District, a community of galleries located around the Maison européenne de la photographie (European House of Photography, MEP).
Photo12 Galerie is specialized in figurative contemporary photography, representing artists such as Benno Graziani, the artistic duo Clark and Pougnaud, Jean-Marie Périer, Norman Seeff, Christopher Thomas, Yury Toroptsov and Martin Usborne. The gallery also takes a real interest in the blurring of photographic and video genres.


== Exhibitions and international fairs ==
Photo12 Galerie participates in several international fairs including Photo Shanghai, Art Élysées Paris, Fotofever Paris, Photo L.A. or Art Paris Art Fair and organizes regular exhibitions in its premises and outdoors, in numerous cultural institutions and galleries.


== References ==


== External links ==
http://www.galerie-photo12.com/EN/