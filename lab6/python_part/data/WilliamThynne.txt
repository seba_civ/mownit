William Thynne (died 1546) was an English courtier and editor of Geoffrey Chaucer's works.


== Life ==
Thynne's family bore the alternative surname of Botfield or Boteville, and he is sometimes called "Thynne alias Boteville". In 1524 he was second clerk of the kitchen in the household of Henry VIII, and by1526 he had become chief clerk of the kitchen, with full control of royal banquets. The office was connected with the board of green cloth, and its holder enjoyed an official lodging at Greenwich.
The king showed Thynne favour, in grants. On 20 August 1528 he became bailiff of the town and keeper of the park of Bewdley. On 21 July 1529 he was appointed customer of wools, hides, and fleeces in the port of London, and on 8 October 1529 receiver-general of the earldom of March and keeper of Gateley Park, Wigmoresland. In 1531 Thynne obtained from the prior and convent of Christchurch, near Aldgate in London, a lease of the rectorial tithe of Erith in Kent, and in a house there he passed much of his life.
In 1533, Thynne became one of the cofferers of Queen Anne Boleyn, and on 27 March 1533 the king made him a gift of oak-trees. In a document dated 16 April 1536 Thynne was described as clerk comptroller of the royal household, and a reference was made to him in 1542 as "clerk of the Green Cloth". He died on 10 August 1546, and was buried in the church of All Hallows Barking, with a brass to his memory.


== Works ==
Thynne studied the works of Chaucer, collecting manuscripts of the poems. He published at the press of Thomas Godfray the first major collected edition in a two-columned folio, dedicated in Thynne's name to Henry VIII (according to John Leland, the preface was by Sir Bryan Tuke, a colleague of Thynne at the board of green cloth). The title was The workes of Geffray Chaucer newly printed, with dyvers workes which were never in print before. He included spurious works, but he printed an improved text of the Canterbury Tales, and he included some works for the first time. A second edition of Thynne's collective edition of Chaucer's works was printed by W. Bonham in 1542, and to it Thynne added the spurious work The Plowman's Tale (anti-Catholic and probably a contemporary text). It was excluded from Thynne's edition of 1532, but had been printed separately by Godfray before 1535.
A poem The Pilgrim's Tale in a volume of miscellaneous verse The Courte of Venus published between 1536 and 1540, and which was assigned by John Bale to Chaucer, may have no connection to Thynne.
In 1561 John Stow brought out a revised version of Thynne's edition of Chaucer. Thomas Speght originated another edition (1598).


== Will and family ==
Thynne's will, dated 16 November 1540, was proved on 7 September 1546. His wife Anne, daughter of William Bond, clerk of the green cloth, was sole executrix and chief legatee. The overseers were Sir Edmund Peckham, cofferer of the king's household, and the testator's nephew, Sir John Thynne. Anne then married Sir Edward Broughton, and Hugh Cartwright. She died intestate before 1572. Their son Francis Thynne was a herald. He criticied Speght's work, and defended his father (letter first printed in Henry John Todd's Illustrations of Chaucer of 1810.


== Notes ==

Attribution
 This article incorporates text from a publication now in the public domain: Lee, Sidney, ed. (1898). "Thynne, William". Dictionary of National Biography 56. London: Smith, Elder & Co.