Olive leaf is the leaf of the olive tree (Olea europaea). Although olive oil is well known for its flavor and health benefits, the leaf has been used medicinally in various times and places. Olive leaf and olive leaf extracts (OLE), are now marketed as anti-aging, immunostimulator, antioxidant, cardio protective, blood sugar regulating, anti-inflammatory and antibiotic agents. Though there is some laboratory evidence for these effects in biological standardization experiments (i.e., bioassays), clinical evidence in humans is inconclusive.
Clinical evidence has been conflicting regarding any blood pressure lowering effect of carefully extracted olive leaf extracts. Bioassays support its antibacterial, antifungal, and anti-inflammatory effects at a laboratory level. A liquid extract made directly from fresh olive leaves gained international attention when it was shown to have an antioxidant capacity almost double green tea extract and 400% higher than vitamin C.


== Historical References of Olive Leaves ==
Historically the benefits of Olive Leaves have been widely cited, and been used by various cultures for both medicinal and nutritional uses.  In the Mediteraranean region olive leaves have been used for the treatment of complaints including high blood pressure, inflammation, arthritis, fever and high blood sugar.  The phenolic compounds of the olive tree are concentrated in the leaves, and contain phenolic compounds including oleuropein, hydroxytyrosxol, tyrosol, luteolin, rutin, caffeic acid, catechin and apigenin.


== Leaf Characteristics ==
The silvery green leaves are oblong, measuring 4–10 centimetres (1.6–3.9 in) long and 1–3 centimetres (0.39–1.2 in) wide. When consumed, leaves have an astringent bitter taste.


== Active compounds ==
The primary active compounds in unprocessed olive leaf are believed to be the antioxidants oleuropein and hydroxytyrosol, as well as several other polyphenols and flavonoids, including oleocanthal. Elenolic acid is a component of olive oil and olive leaf extract. It can be considered as a marker for maturation of olives. Oleuropein, together with other closely related compounds such as 10-hydroxyoleuropein, ligstroside and 10-hydroxyligstroside, are tyrosol esters of elenolic acid.


== Benefits of Olive Leaf ==


=== Antioxidant ===
Plants in the Mediterranean have developed high levels of antioxidants as a protection mechanism against environmental stressors. Olive leaf extract is a potent antioxidant, and in 2007 a study evaluated 55 medicinal herbs, and uncovered that the olive leaf had one of the highest levels of antioxidants tested.
Fresh Olive Leaf Extract is known to contain over 100 phytochemicals, with at least 12 already showing antioxidant properties.  Oleuropein and hydroxytyrosol are the most well known antioxidants in Olive Leaf Extract, and give the tonic its signature bitter taste.


=== Immune Support ===
Olive leaf has been traditionally used to support the immune system. Common conditions where olive leaves have been used to improve the immune system include colds and flu, with traditional evidence also suggesting that cold sores and urinary tract infections issues improved with the use of Olive Leaf.
Olive leaf has also been suggested to support innate immunity through their interactions with macrophages. Olive leaf has been suggested to stimulate macrophages and increase the production of nitric oxide. Therefore by increasing the effectiveness of macrophages, the immune systems first line of defense against pathogens is also protected. 


=== Cardio Protection ===
Olive Leaves and Olive Leaf Extract has been suggested to help with supporting normal blood pressure, cholesterol and blood sugar levels. Olive leaf extract has been used to reduce blood pressure for centuries. Olive leaf is still one of the most widely used herbs for hypertension in Morocco.  Olive leaf is thought to reduce blood pressure through a number of different mechanisms, including, vasodilation, reducing inflammation and oxidative stress.   Early studies highlight that Oleuropein functions by acting on the smooth muscle surrounding blood vessels, which reduces the tension of blood vessels, thereby allowing more blood to flow through and subsequently lowering blood pressure. 


== Soaps and cosmetics ==
Olive leaf extracts are sometimes used in skin creams and other cosmetics for application to the skin. Olive leaf extract has been shown to provide antioxidant and antibacterial effects that can prolong the shelf life of cosmetic preparations. 


== Notes ==