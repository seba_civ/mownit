Naum Semyonovich Sorkin (Russian: Наум Семёнович Соркин; 11 February 1899 – 16 January 1980) was a Soviet military officer and diplomat.
A Red Army veteran of the Russian Civil War, Sorkin was sent to Mongolia as an artillery instructor for the Mongolian People's Army in 1923, where he later served as a consular official in Altanbulag and first secretary at the Soviet embassy in Ulan Bator in 1926-1931. The chief of the Soviet General Staff's Special Operations Department in 1939-1941, he served as intelligence chief for the Far Eastern Front in 1941-1945 and the 1st Far Eastern Front following the Soviet declaration of war on the Japanese Empire in August 1945.
Promoted to major-general in 1944, he graduated from the Voroshilov General Staff Academy in 1952 and was an instructor at the Mozhaysky Military Academy of Aeronautical Engineering until his retirement in 1958.


== Life and military career ==
Naum Sorkin was born in Alexandrovsk (now Zaporizhia), a town in the Yekaterinoslav Governorate of the Russian Empire (now Zaporizhia Oblast, Ukraine) to Jewish parents. The town fell into the southeastern part of the Pale of Settlement, the westernmost region of the empire where Jews were permitted permanent residence, and Naum's father was a local official.
Naum Sorkin joined the Bolshevik Party and Red Army in 1919 and took part in the Russian Civil War. He graduated from an artillery commanders' course in Kharkov in 1920 and attended the Higher Artillery School in 1922-1923. Sorkin was dispatched to Soviet-allied Mongolia in 1923, where he was an artillery instructor until 1926. He next held posts in Mongolia as a Soviet consular official in Altanbulag and first secretary at the Soviet embassy in Ulan Bator from 1926 to 1931.
His later assignments in the Soviet Union were with the People's Commissariat for Military and Naval Affairs and Revolutionary Military Council from June 1933 to June 1935 and with the People's Commissariat for Foreign Affairs from June 1935 to June 1936, and he was appointed deputy chief of the 9th Department of the Red Army's Intelligence Directorate (responsible for information concerning Mongolia and Xinjiang) until May 1939. The Intelligence Directorate's 9th Department became the Red Army General Staff's Department of Special Tasks, with Sorkin selected to serve as acting head from May 1939 to February 1941.
Moved to the Soviet Far East, where he was promoted to major-general in 1944, Sorkin was the chief of the intelligence section of the staff of the Far Eastern Front in 1941-1945. He was assigned as intelligence section chief for the staff of the 1st Far Eastern Front at the time of the Far Eastern Front's temporary division into Army General Kirill Meretskov's 1st Far Eastern Front and Army General Maxim Purkayev's 2nd Far Eastern Front on the day of the Soviet entry into the Asian Theatre of World War II in August 1945.
Major-General Sorkin was again appointed intelligence chief for the staff of the Far Eastern Front following its post-war recreation in 1945, then transferred to teach at the Military Diplomatic Academy from 1947 to 1950. He graduated from the Voroshilov General Staff Academy in 1952 and subsequently worked at the Mozhaysky Military Academy of Aeronautical Engineering from 1952 until 1958.
Major-General Sorkin retired from the Mozhaysky Academy and active service in 1958, having spent nearly forty years in the Soviet military. He published a memoir about his 1920s experiences in Mongolia in 1970.
He died in Leningrad, having bequeathed nineteen fine art pieces by Russian painters from his personal collection to the Smolensk State Museum and Preserve.
His military decorations included the Order of Lenin, as well as two Red Banner and three Red Star orders.


== Works ==
Memoir:
«В начале пути: Записки инструктора монгольской армии»
(Starting the Journey: Notes of an Instructor of the Mongolian Army, V nachale puti: Zapiski instruktora mongolskoy armii). Moscow: Nauka, 1970.


== References ==