In public-key cryptography, the Station-to-Station (STS) protocol is a cryptographic key agreement scheme based on classic Diffie–Hellman that provides mutual key and entity authentication.
In addition to protecting the established key from an attacker, the STS protocol uses no timestamps and provides perfect forward secrecy. It also entails two-way explicit key confirmation, making it an authenticated key agreement with key confirmation (AKC) protocol.
STS was originally presented in 1987 in the context of ISDN security (O'Higgins et al. 1987), finalized in 1989 and generally presented by Whitfield Diffie, Paul C. van Oorschot and Michael J. Wiener in 1992. The historical context for the protocol is also discussed in Diffie (1988).


== Description ==
Deployment of STS can take different forms depending on communication requirements and the level of prior communication between parties. The data described in STS Setup may be shared prior to the beginning of a session to lessen the impact of the session's establishment.
In the following explanations, exponentiation (Diffie–Hellman) operations provide the basis for key agreement, though this is not a requirement. The protocol may be modified, for example, to use elliptic curves instead.


=== STS Setup ===
The following data must be generated before initiating the protocol.

An asymmetric signature keypair for each party 
Required for authentication. The public portion of this keypair may be shared prior to session establishment.
Key establishment parameters 
The specification of a cyclic group p and a generator g for that group. These parameters may be public. Menezes, van Oorschot & Vanstone (1997) note that for additional security each party may supply its own parameters.

Sharing this data prior to the beginning of the session lessens the complexity of the protocol.


=== Basic STS ===
Supposing all setup data has been shared, the STS protocol proceeds as follows. If a step cannot be completed, the protocol immediately stops. All exponentials are in the group specified by p.
Alice generates a random number x and computes and sends the exponential gx to Bob.
Bob generates a random number y and computes the exponential gy.
Bob computes the shared secret key K = (gx)y.
Bob concatenates the exponentials (gy, gx) (order is important), signs them using his asymmetric (private) key B, and then encrypts the signature with K. He sends the ciphertext along with his own exponential gy to Alice.
Alice computes the shared secret key K = (gy)x.
Alice decrypts and verifies Bob's signature using his asymmetric public key.
Alice concatenates the exponentials (gx, gy) (order is important), signs them using her asymmetric (private) key A, and then encrypts the signature with K. She sends the ciphertext to Bob.
Bob decrypts and verifies Alice's signature using her asymmetric public key.
Alice and Bob are now mutually authenticated and have a shared secret. This secret, K, can then be used to encrypt further communication. The basic form of the protocol is formalized in the following three steps:

(1) Alice → Bob : gx
(2) Alice ← Bob : gy, EK(SB(gy, gx))
(3) Alice → Bob : EK(SA(gx, gy))


=== Full STS ===
Setup data can also be incorporated into the protocol itself. Public key certificates may be sent in steps 2 and 3 if the keys are not known in advance.

(1) Alice → Bob : gx
(2) Alice ← Bob : gy, CertB, EK(SB(gy, gx))
(3) Alice → Bob : CertA, EK(SA(gx, gy))

If system-wide key establishment parameters are not used, the initiator and responder may create and send their own parameters. In this case, parameters should be sent with the exponential.

(1) Alice → Bob : g, p, gx
They must also be verified by Bob to prevent an active attacker from inserting weak parameters (and thus a weak key K). Diffie, van Oorschot & Wiener (1992) recommend against special checks to prevent this and instead suggest including the group parameters in Alice's certificate.


== Variations ==
The variations mentioned here are from the original STS paper. See the following references for other, more significant variations.
Bellare, M.; Canetti, R.; Krawczyk, H. (1998), "A modular approach to the design and analysis of authentication and key exchange protocols", Proceedings of the 30th Annual Symposium on the Theory of Computing 
RFC 2412, "The OAKLEY Key Determination Protocol".
ISO/IEC 117703, "Mechanisms Using Asymmetric Techniques", (1999).


=== Authentication-only STS ===
A simplified form of STS is available that provides mutual authentication but does not produce a shared secret. It uses random number challenges instead of the above Diffie–Hellman technique.
Alice generates a random number x sends it to Bob.
Bob generates a random number y.
Bob concatenates the random numbers (y, x) (order is important) and signs them using his asymmetric key B. He sends the signature along with his own random number to Alice.
Alice verifies Bob's signature.
Alice concatenates the random numbers (x, y) (order is important) and signs them using her asymmetric key A. She sends the signature to Bob.
Bob verifies Alice's signature.
Formally:

(1) Alice → Bob : x
(2) Alice ← Bob : y, SB(y, x)
(3) Alice → Bob : SA(x, y)

This protocol can be extended to include certificates as in Full STS.


=== STS-MAC ===
In cases where encryption is a not viable choice in session establishment, K can instead be used to create a MAC.

(1) Alice → Bob : gx
(2) Alice ← Bob : gy, SB(gy, gx), MACK(SB(gy, gx))
(3) Alice → Bob : SA(gx, gy), MACK(SA(gx, gy))

Blake-Wilson & Menezes (1999) note that this variation may be preferable to original STS ("STS-ENC") in any case because
.. the use of encryption to provide key confirmation in STS-ENC is questionable — traditionally the sole goal of encryption is to provide confidentiality and if an encryption scheme is used to demonstrate possession of a key then it is shown by decryption, not by encryption.
The paper goes on to counsel, however, that using K for both a MAC and as the session key violates the principle that keys should not be used for more than one purpose, and presents various workarounds.


== Cryptanalysis ==
STS-MAC is vulnerable to some unknown key-share attacks, whereby an active attacker can inject her own identity into the session establishment procedure in place of either initiator or responder. Blake-Wilson & Menezes (1999) present the vulnerabilities and some solutions.


== References ==
Blake-Wilson, S.; Menezes, A. (1999), "Unknown Key-Share Attacks on the Station-to-Station (STS) Protocol", Public Key Cryptography, Lecture Notes in Computer Science 1560, Springer, pp. 154–170 
Diffie, W. (1988), "The first ten years of public-key cryptography", Proceedings of the IEEE (IEEE) 76 (5): 560–577, doi:10.1109/5.4442 
Diffie, W.; van Oorschot, P. C.; Wiener, M. J. (1992), "Authentication and Authenticated Key Exchanges", Designs, Codes and Cryptography (Kluwer Academic Publishers) 2 (2): 107–125, doi:10.1007/BF00124891 
Menezes, A.; van Oorschot, P. C.; Vanstone, S. (1997), Handbook of Applied Cryptography, CRC Press, pp. 519–520 
O'Higgins, B.; Diffie, W.; Strawczynski, L.; do Hoog, R. (1987), "Encryption and ISDN – A Natural Fit", 1987 International Switching Symposium (ISS87)