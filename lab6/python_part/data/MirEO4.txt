Mir EO-4 (also called Principal Expedition 4) was the fourth long-duration expedition to the Soviet space station Mir. The expedition began in November 1988, when crew members Commander Aleksandr Volkov and Flight Engineer Sergei Krikalev arrived at the station via the spacecraft Soyuz TM-7. The third crew member of EO-4, Valeri Polyakov, was already aboard Mir, having arrived in August 1988 part way through the previous expedition, Mir EO-3.
The expedition lasted for five months, and at its conclusion Mir was left unmanned until the launch of Mir EO-5 in September 1989. This ended a continuous habitation of the space station which began in February 1987, with the arrival of the crew of Mir EO-2.


== Background ==
The previous long-duration expedition to Mir, EO-3, was intended to break the record for spaceflight duration of 326 days set by Yuri Romanenko during Mir EO-2. The physician Valeri Polyakov was sent to the station part way through EO-3 so that he could observe the cosmonauts at the end of their record duration flight. Polyakov then stayed aboard Mir to observe the EO-4 crew.


== Crew ==
Volkov, the only crew member who had been to space before, had one previous spaceflight. It was a two-month mission in 1985 to the space station Salyut 7, launched by the spacecraft Soyuz T-14. This expedition, Salyut 7 EO-4, was intended to be 6 months long, but the Commander Vladimir Vasyutin became ill, and the mission was shortened, forcing the cosmonauts to leave the station unmanned. Volkov became father of the first ever second-generation cosmonaut, when his son Sergey Volkov became Commander of the International Space Station's Expedition 17 in 2008.
This was Krikalev's first spaceflight, and he went on to have a further five missions to space. As of 2010 he holds the record for the longest cumulative time spent in space, at 803 days. His most recent spaceflight was in 2005, as Commander of the International Space Station's Expedition 11.
Polyakov, a physician, arrived at the station part way through the previous expedition, Mir EO-3. He had never been in space before, but would later have one more spaceflight, which would span three Mir expeditions: EO-15, EO-16, and EO-17. His second spaceflight, launched on Soyuz TM-18 in January 1994, would last 437 days, and as of 2010, still holds the record for the longest ever spaceflight.


=== Backup Crew ===
Aleksandr Viktorenko (Commander)
Aleksandr Serebrov (Flight Engineer)
German Arzamazov (Research Doctor)


== Mission highlights ==
During this mission experiments in 16 different fields were performed as topografic and spectrografic research of the Earth surface, biological and medical research (including blood and heart-tests).


=== Launch ===

The French president at the time, François Mitterrand, insisted on attending the launch of the Soyuz TM-7, of which Frenchman Chrétien was a crew member. This caused the launch to be delayed by four days, and hence reducing Chrétien's time in space. Mitterrand flew to the launch site in Baikonur in a Concorde.
On November 28, two days after launch, the Soyuz TM-7 spacecraft docked to Mir, and for the first time six cosmonauts were simultaneously aboard the complex.


=== Mir Aragatz ===

From November 28 to December 21, 1988, there were six people aboard the station: the three crew members of EO-4, Titov and Manarov who were finishing EO-3, and the visiting French cosmonaut Jean-Loup Chrétien who had launched aboard Soyuz TM-7 on 26 November. Chrétien's flight was referred to as the French Aragatz mission. This was Chrétien's second spaceflight, his first was a short mission to Salyut 7, which was launched with the spacecraft Soyuz T-6, and lasted for about a week. After 24.8 days aboard the station, Chrétien returned to Earth aboard Soyuz TM-6 on 21 December.
A highlight of the Aragatz mission was the spacewalk which was performed by Volkov and Chrétien on 9 December 1988, and lasted for 5 hours and 57 minutes. It was the first spacewalk conducted by someone not from the Soviet or U.S. space programs. Its purpose was to install the 240 kg French experimental deployable structure, known as ERA, and a panel of material samples.
Return of Soyuz TM-6
On December 15, 1988, Titov and Manarov's 359th day in space, the EO-3 crew officially broke the spaceflight duration record by the required 10%; the record had previously been set by Yuri Romanenko, who had a 326-day spaceflight aboard Mir during EO-2. Both the French mission and EO-3 ended when Chrétien, Titov, and Manarov landed in the spacecraft Soyuz TM-6 on December 21, which occurred six hours after undocking from Mir.


=== Progress 39 ===

On December 27, shortly after Soyuz TM-6 departed, the expedition's first Progress resupply spacecraft docked with the station, two days after its launch. This model of resupply spacecraft, Progress 7K-TG, had been used for resupply missions to Soviet space stations since 1978. Progress 39 delivered 1,300 kg of supplies to the EO-4 crew, and remained docked to the station for 42 days.
During this time, Progress 39 was used to boost the orbit of the space station. This was necessary due to the greater than normal atmospheric drag. The extra drag was caused by atmospheric expansion, which in turn was caused by the solar maximum occurring at the time, during solar cycle 22. The boost changed the Perigee and Apogee of the station from 325 km and 353 km AMSL to 340 km and 376 km, respectively. After the boost, Krikalev reported that he was unable to visually detect the change in altitude.
The EO-4 crew filled Progress 39 with waste and excess equipment used during the Aragatz mission, and then the spacecraft undocked on February 7, and was intentionally destroyed during atmospheric reentry later that day.


=== Progress 40 ===
On February 10, the next resupply spacecraft, Progress 40 was launched, and it docked with Mir two days later. It remained docked to Mir for 18 days. During this time, it was announced that due to delays in the production of the Kristall module, the launch of the next Mir module, Kvant-2, would also be delayed. It was also announced that as a result, Mir would be left unmanned following EO-4.


=== Progress 41 ===
The final Progress resupply spacecraft of the expedition docked with the station on March 19, and remained docked for 33 days. Progress 41 was the second last Progress spacecraft to use the original design (the last being Progress 42, which resupplied Mir EO-6 in 1990). The updated design would be called Progress-M, and its first flight would be Progress M-1 in August 1989, just prior to the arrival of the Mir EO-5 crew in September.


=== Leaving Mir unmanned ===
All three crew members left Mir on board Soyuz TM-7 on 27 April 1989, bringing EO-4 to an end. For Volkov and Krikalev the mission lasted for 151d 11h 08m and for Polyakov 240d 22h 34m.


== References ==