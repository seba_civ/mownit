All information based on Sabrina, the Teenage Witch (TV series).


== Books ==
A series of books were published based on the series, including some directly based on specific episodes (most notably the first book which incorporates a large amount of exact dialogue from the pilot episode). There were fifty-two regular volumes, not including specials or companions.


=== Original series ===
Sabrina the Teenage Witch
Showdown at the Mall
Good Switch Bad Switch
Halloween Havoc
Santa's Little Helper
Ben There, Done That
All You Need is a Love Spell
Salem on Trial
A Dog's Life
Lotsa Luck
Prisoner of Cabin 13
All That Glitters
Go Fetch
Spying Eyes
Harvest Moon
Now You See Her, Now You Don't
Eight Spells a Week (Super Edition)
I'll Zap Manhattan
Shamrock Shenanigans
Age of Aquariums
Prom Time
Witchopoly
Bridal Bedlam
Scarabian Nights
While the Cat's Away
Fortune Cookie Fox
Haunts the House
Up, Up, and Away
Millennium Madness (Super Edition)
Switcheroo
Mummy Dearest
Reality Check
Knock on Wood
It's a Miserable Life!
Pirate Pandemonium
Wake-up Call
Witch Way Did She Go?
Milady's Dragon
From the Horse's Mouth
Dream Boat
Tiger Tale
The Witch That Launched a Thousand Ships
Know-It-All
Topsy-Turvy
Hounded by Baskervilles
Off to See the Wizard
Where in the World is Sabrina Spellman?
Witch Glitch
The Truth Hurts
What a Doll
Christmas Crisis
Now and Again


==== Specials ====
Sabrina Goes to Rome
Sabrina Down Under
Ten Little Witches


=== Salem's Tails (companion series) ===
Cat TV
Teacher's Pet
You're History
King of Cats
Dog Day Afternoon
Psychic Kitty
Cat By the Tail
Feline Felon
Happily Ever After
Gone Fishin'
Worth a Shot
Rulin' the School
Kitty Cornered
Mascot Mayhem


== CD ==


=== Soundtrack ===
Sabrina, the Teenage Witch – The Album is a soundtrack album released on October 27, 1998, by Geffen Records. It contains music featured in the TV series, including one song recorded by Melissa Joan Hart. The album was certified Gold by the RIAA.


=== Track listing ===
"Walk of Life" – Spice Girls
"Abracadabra" – Sugar Ray
"Hey, Mr. DJ (Keep Playin' This Song)" – Backstreet Boys
"One Way or Another" – Melissa Joan Hart
"Kate" – Ben Folds Five
"Show Me Love" (Radio edit) – Robyn
"Giddy Up" – 'N Sync
"Slam Dunk (Da Funk)" – Five
"Magnet & Steel" – Matthew Sweet
"So I Fall Again" – Phantom Planet
"I Know What Boys Like" – Pure Sugar
"Smash" – The Murmurs, Jane Wiedlin, Charlotte Caffey
"Doctor Jones" (Metro 7" edit) – Aqua
"Soda Pop" – Britney Spears
"Amnesia" (Radio remix) – Chumbawamba
"Blah, Blah, Blah" (Sabrina's Theme) – The Cardigans


== DVD releases ==
CBS DVD (distributed by Paramount) has released seasons 1–7 of Sabrina, the Teenage Witch on DVD in Regions 1, 2 and 4. Sabrina Goes to Rome, which aired early in the third season as a made-for-TV-movie, was included on the seventh season set. On the DVD sets, most sequences featuring music has been altered, seeing as it would have been costly to obtain the rights to many songs used in the series. This is most evident on the Season 7 DVD set, during which Sabrina works for Scorch, a music magazine. Almost all musical performances have been cut from the DVD release. Sabrina Down Under, which aired in the fourth season as a sequel to Sabrina Goes to Rome, was never released on any of the season sets. The 1996 film of the same name was released on VHS and DVD in the U.S. and Canada. A complete box set that features all 7 seasons was released in the UK on May 7, 2012.
On February 16, 2016, CBS DVD will release Sabrina, the Teenage Witch- The Complete Series on DVD in Region 1.


=== Seasons ===
* While the DVD sleeve states that the season features 21 episodes (and the series collection packaging states that it features all 162 episodes), the DVD actually features all 22 episodes; the final two episodes of the season (and thus, the series) are simply combined into one.
** The complete collection released in Australia was a JB Hi-Fi-exclusive release which was available for approximately two months before being discontinued. While not specifically being a box set (the collection was a package of all seven seasons placed in a cardboard box), it was offered for a much cheaper price than buying all seven seasons individually ($99, compared to a combined price of around $240 for the individual sets).


=== TV movies ===


== References ==


== External links ==