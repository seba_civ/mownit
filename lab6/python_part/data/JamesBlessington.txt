James Blessington (28 February 1874 – 18 April 1939) was a Scottish football player and manager.
Born in Linlithgow, West Lothian, Blessington began his playing career with several Edinburgh sides, namely Leith Hibernians, Hibernian F.C. and Leith Athletic before moving to Glasgow to join Celtic in 1892. He spent six seasons at Celtic Park, earning three League titles. He moved to England in 1898, playing for Preston North End, Derby County, Bristol City, Luton Town and Leicester Fosse. He played for Leicester between 1903 and 1909, acting as player-manager for the last two years, becoming that club's first manager. He was in charge for a total of 84 games.
Blessington was also capped four times by the Scottish national team between 1894 and 1896, during his time with Celtic. He also made five appearances for the Scottish League representative side, scoring one goal.
Blessington then moved to Ireland, where he helped coach Belfast Celtic and also assisted the Irish Amateur Athletic Association as a handicapper. He later moved to Newton Abbot in Devon, where he ran a pub.


== References ==