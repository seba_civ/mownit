The Theban Tomb TT29 is located in Sheikh Abd el-Qurna, part of the Theban Necropolis, on the west bank of the Nile, opposite to Luxor. It is the burial place of the Ancient Egyptian noble Amenemopet called Pairy who was Vizier and Governor of Thebes.
Amenemopet called Pairy was the son of Ahmose Humay (TT224) and Nub. His wife was called Weretmaatef. Amenemopet called Pairy was the brother of Sennufer (TT96), and he had a son named Paser.
In the hall of the tomb six scenes are shown:
Amenemopet and his wife make offerings before the god Ra
Amenemopet offers to the ka of Amenhotep II
Texts detailing the duties of the vizier
Amenemotep is shown passing judgement as vizier.
The other 2 scenes contain more texts.
In the passage connected to the hall offerings to Amenemopet and his family are shown. He is depicted with his parents, his brother Sennufer and wife Senetnay, and his son Paser.


== See also ==
N. de Garis Davies, Nina and Norman de Garis Davies, Egyptologists


== References ==


== External links ==
Scans of Norman and Nina De Garis Davies' tracings from Theban Tomb 29 (external).