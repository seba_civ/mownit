A sicilicus was an old Latin diacritical mark,  ͗, like a reversed C (Ɔ) placed above a letter and evidently deriving its name from its shape like a little sickle (which is sicilis in Latin). The ancient sources say that during the time of the Republic it was placed above a geminate consonant to indicate that the consonant counted twice, although there is hardly any epigraphic and paleographic evidence available from such an early time. When such geminate consonants began to be represented during classical times by writing the letter twice, the sicilicus naturally fell into disuse in this function, but continued to be used to indicate the doubling of vowels as an indication of length in the developed form of the apex. It has been suggested that Plautus alludes to the sicilicus in the prologue to Menaechmi.
In Unicode, it is encoded as U+0357  ͗  COMBINING RIGHT HALF RING ABOVE (HTML &#855;).


== See also ==
Open O, although this is a full letter, and not a diacritic placed above a letter
Antisigma, although this is a full letter, and not a diacritic placed above a letter
Apex (diacritic), used for long vowels instead of long consonants
Apostrophe, whose shape is derived from it
Comma (punctuation), whose shape is similar
Latin spelling and pronunciation


== References ==
Lewis and Short Latin Lexicon


== Notes ==