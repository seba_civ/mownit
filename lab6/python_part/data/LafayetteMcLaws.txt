Lafayette McLaws (/ləˈfeɪ.ɛt/ lə-FAY-et; January 15, 1821 – July 24, 1897) was a United States Army officer and a Confederate general in the American Civil War. He served at Antietam and Fredericksburg, where Robert E. Lee praised his defense of Marye's Heights, and at Gettysburg, where his division made successful assaults through the Peach Orchard and Wheatfield, but was unable to dislodge Union forces from Cemetery Ridge. After the Knoxville Campaign, he was court-martialed for inefficiency, though this was overturned for procedural reasons. Finally he was sent to his native Georgia to resist Sherman's March to the Sea, but had to retreat through the Carolinas, losing many men through desertion, and is presumed to have surrendered with Joseph E. Johnston in April 1865.
McLaws remained bitter about his court-martial, especially as the charges had been filed by James Longstreet, his friend and classmate at West Point, with whom he had served for years. Although he defended Longstreet against Lost Cause proponents who blamed him for losing the war, McLaws never fully forgave Longstreet for his actions.


== Early life ==
McLaws was born in Augusta, Georgia. He graduated from the United States Military Academy in 1842, placing 48th out of 56 cadets. McLaws served as an infantry officer in the Mexican-American War, in the West, and in the Utah War to suppress the Mormon uprising. While at Jefferson Barracks, Missouri, he married Emily Allison Taylor, the niece of Zachary Taylor, making him a cousin-in-law of future Confederates Richard Taylor and Jefferson Davis.


== Civil War ==


=== 1861–62 ===
At the start of the Civil War, resigning as a U.S. Army captain, McLaws was commissioned a major in the Confederate States Army. He was quickly promoted to colonel of the 10th Georgia Infantry regiment; then quickly again to brigadier general in brigade and division command in the Seven Days Battles; then, on May 23, 1862, to major general. He joined his childhood friend in Augusta and fellow West Point of '42 classmate, Maj. Gen. James Longstreet's First Corps in the Army of Northern Virginia as 1st Division commander and stayed with Longstreet for most of the war.
During Robert E. Lee's 1862 Maryland Campaign, McLaws's Division was split from the rest of the corps, operated in conjunction with Maj. Gen. Thomas J. "Stonewall" Jackson, and captured Maryland Heights at Harpers Ferry. He marched his division to Sharpsburg, Maryland, and defended the West Woods in the Battle of Antietam. Lee was disappointed in McLaws's slow arrival on the battlefield. At the Battle of Fredericksburg, McLaws's Division was one of the defenders of Marye's Heights, and he satisfied Lee with his ferocious defensive performance.


=== 1863–65 ===
At Chancellorsville, while the rest of Longstreet's corps was detached for duty near Suffolk, Virginia, McLaws fought directly under Lee's command. On May 3, 1863, Lee sent McLaws's Division to stop the Union VI Corps under Maj. Gen. John Sedgwick marching toward Lee's rear. He did accomplish this, but Lee was disappointed that McLaws had not attacked more aggressively and caused more harm to Sedgwick's corps, instead of letting him escape across the Rappahannock River. When Lee reorganized his army to compensate for Jackson's mortal wounding at Chancellorsville, Longstreet recommended his subordinate for one of the two new corps commands, but both men were disappointed when Lee chose Richard S. Ewell and A. P. Hill instead. McLaws requested a transfer, but it was denied.
On the second day of the Battle of Gettysburg, July 2, 1863, McLaws commanded the second division to step off in Longstreet's massive assault on the Union left flank. He achieved great success (at a high cost in lives) in the areas known as the Wheatfield and the Peach Orchard, but the army as a whole was unable to dislodge the Union forces from their positions on Cemetery Ridge. His division did not participate in Pickett's Charge the next day, despite Longstreet's command of that assault.
McLaws accompanied Longstreet's corps to Tennessee to come to the aid of General Braxton Bragg's Army of Tennessee. He arrived too late to lead his division at Chickamauga, where it was led by Brig. Gen. Joseph B. Kershaw, but he did participate in the Chattanooga Campaign. In the Knoxville Campaign later in 1863, Longstreet relieved McLaws for the failure of the attack on Fort Sanders, citing "a want of confidence in the efforts and plans which the Cmdg Genl has thought proper to adopt." In a letter to Confederate Adjutant and Inspector General Samuel Cooper, on December 30, Longstreet submitted three charges of "neglect of duty" but did not request a court-martial because McLaws's "services might be important to the Government in some other position." (In that same letter, he requested a court-martial for Brig. Gen. Jerome B. Robertson, who had been charged with "incompetency" by his division commander.) McLaws also wrote to Cooper on December 30, disputing Longstreet's charges and requesting a court-martial to clear his name. Cooper forwarded Longstreet's letter to Secretary of War James Seddon and to Confederate President Jefferson Davis with the annotation that Longstreet was not authorized to relieve and reassign officers under his command without a formal court-martial. Davis ordered the court-martial of both generals, although he opposed relieving McLaws until a successor could be appointed.
The courts-martial of Robertson and McLaws convened in Morristown, Tennessee, on February 12, 1864, with Maj. Gen. Simon B. Buckner serving as president of the court. The proceedings suffered delays as witnesses—including Longstreet—were not available to appear as scheduled, in some cases because Longstreet granted them leaves of absence. Cooper's office published the court's findings on May 5, exonerating him on the first two specifications of neglect of duty, but finding him guilty of the third—"failing in the details of his attack to make arrangements essential to his success." McLaws was sentenced to 60 days without rank or command, but Cooper overturned the verdict and sentence, citing fatal flaws in the procedures of the court, ordering McLaws to return to duty with his division. However, on May 18, McLaws was assigned by the War Department to the Defenses of Savannah in the Department of South Carolina, Georgia, and Florida.
McLaws was bitter about his fate, claiming that Longstreet had used him as a scapegoat for the failed Knoxville Campaign. Writing in his memoirs many years after the war, Longstreet expressed regret that he had filed charges against McLaws, which he described as happening "in an unguarded moment." In time, the animosity healed between the two Confederate veterans, but McLaws never fully forgave Longstreet for his actions.
McLaws left the First Corps, and since Lee would not accept him for command in Virginia, he proceeded to Savannah, which he was unable to defend successfully against Maj. Gen. William T. Sherman's March to the Sea in late 1864.
McLaws next saw active service opposing Sherman's advance into the Carolinas. At the Battle of Rivers' Bridge on February 2, 1865, his command resisted the advance of the Army of the Tennessee into South Carolina. His forces delayed the Federal crossing of the Salkehatchie River until they found other crossings and turned his right flank. McLaws led a division under Lt. Gen. William J. Hardee at the Battle of Averasborough, commanding the Confederate third line of defense, and at the Battle of Bentonville. His division was little engaged at Bentonville because of vague orders. In the aftermath of these battles, McLaws had problems with the discipline of his division, holding multiple roll calls per day to prevent desertion and looting. When Gen. Joseph E. Johnston reorganized the army, McLaws lost his command assignment. He was assigned command of the District of Georgia after Bentonville. He may have surrendered with Johnston's army in North Carolina on April 26, 1865; however, there is no record of his parole. On October 18, 1865, McLaws was pardoned by the U.S. government.


== Postbellum career ==
After the war, McLaws worked in the insurance business, was a tax collector for the IRS, served as Savannah's postmaster in 1875-76, and was active in Confederate veterans' organizations. Despite his wartime differences with Longstreet, McLaws initially defended Longstreet in the post-war attempts by Jubal Early and others to smear his reputation. Just before his death, however, his opinion changed about the lost cause movement, and he began speaking out about Longstreet's failures at Gettysburg.
Lafayette McLaws died in Savannah and is buried there in Laurel Grove Cemetery. He is the posthumous author of A Soldier's General: The Civil War Letters of Major General Lafayette McLaws (2002).


== Memorial ==
McLaws Circle, part of the Kingsmill development of Anheuser-Busch in James City County, Virginia, near Williamsburg, was named in his honor in the 1970s. In 1861, then Lt. Col. McLaws played a key role in the construction nearby of the Williamsburg Line, 4 miles of defensive works across the Virginia Peninsula, which played a crucial role in the Battle of Williamsburg of the 1862 Peninsula Campaign.


== See also ==

List of American Civil War generals


== Notes ==


== References ==
Bradley, Mark L. This Astounding Close: The Road to Bennett Place. Chapel Hill: University of North Carolina Press, 2000. ISBN 0-8078-2565-4.
Eicher, John H., and David J. Eicher. Civil War High Commands. Stanford, CA: Stanford University Press, 2001. ISBN 0-8047-3641-3.
Quigley, Robert D. Civil War Spoken Here: A Dictionary of Mispronounced People, Places and Things of the 1860s. Collingswood, NJ: C. W. Historicals, 1993. ISBN 0-9637745-0-6.
Tagg, Larry. The Generals of Gettysburg. Campbell, CA: Savas Publishing, 1998. ISBN 1-882810-30-9.
Wert, Jeffry D. General James Longstreet: The Confederacy's Most Controversial Soldier: A Biography. New York: Simon & Schuster, 1993. ISBN 0-671-70921-6.


== Further reading ==
Lafayette McLaws Papers, Southern Historical Collection, The Wilson Library, University of North Carolina at Chapel Hill.
McLaws, Lafayette. A Soldier's General: The Civil War Letters of Major General Lafayette McLaws. Edited by John C. Oeffinger. Chapel Hill: University of North Carolina Press, 2002. ISBN 978-0-8078-6047-2.