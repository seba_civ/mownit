Nikolai Konstantinovich Borschevsky (Russian: Николай Константинович Борщевский; born January 12, 1965 in Tomsk, Soviet Union) is a retired professional ice hockey player from Russia, and the current head coach of the Atlant Moscow Oblast of the KHL. Nicknamed "Stick" due to his diminutive frame, he was a star in the Soviet Union and went on to play in the National Hockey League for the Toronto Maple Leafs, Calgary Flames and Dallas Stars. Despite a successful NHL debut in 1992–93, he never achieved the same level of success in North America, with injuries limiting his effectiveness. He retired in 1998 after a second stint with Spartak Moscow.
Canadian actor Mike Myers named his dog Borschevsky in his honour. It was reported that Borschevsky's favourite drinks in Canada were rye and ginger and Red Cap Ale.
Canadian hockey youtuber Steve Dangle named his bird Nikolai Birdschevsky after him.


== Russian career ==
He spent the majority of his career playing in the Soviet Union, becoming a mainstay with Dynamo Moscow and later rivals Spartak Moscow. At Dynamo, he became a regular with the team and showed signs of future stardom, recording a high of 11 goals and 18 points in 37 games in 1987–88.
Two years later, he moved to Spartak where he blossomed into a star. His goal totals improved every year and he scored at a point-a-game pace, leading the team in scoring in each of the three years he spent at Spartak. His success at club level later translated to success at the international level, where he figured prominently for the Unified Team that won gold at the 1992 Winter Olympics, scoring seven goals in eight games.


== NHL career ==
His success at Spartak and at Albertville drew the attention of NHL scouts, who began to look more attentively at Russian hockey after the dissolution of the Soviet Union allowed for an influx of Russian hockey players to North America. The Leafs made him their third choice, 77th overall in the 1992 NHL Entry Draft. He made an immediate impact upon arriving to the NHL, scoring 34 goals and netting 40 assists in 78 games for Toronto in 1992–93, including the Game 7 overtime winner for the Leafs in their division semi-final series with the Detroit Red Wings. Despite his early success, he was limited to 45 games in 1993–94 due to injuries, although he managed to record 14 goals and 35 points. During the 1994–95 NHL lockout he returned to Spartak for nine games recording six points, but his return to the Leafs once hockey resumed saw him limited to 19 games where he recorded only five points- none of them goals. Borschevsky was traded at the trade deadline that year to the Calgary Flames for a sixth-round pick, where he lasted another eight games and picked up five more points (none goals). He failed to appear in a single game in Calgary's first round playoff loss to the San Jose Sharks, leading to his departure in the offseason to the Dallas Stars.
In Dallas, he managed to score for the first time in the NHL since the 1993–94 season (just once though), but injuries again curtailed his season, allowing him to dress only in 12 games. His NHL career over, Borschevsky subsequently moved to Kölner Haie of the Deutsche Eishockey Liga later in the 1995–96 season, appearing in eight games and recording four assists, before finishing his career with Spartak, winning the Russian scoring title in 1996–97 before retiring after a 27-point campaign in 1997–98.


== Post-retirement ==
In 1998, Borschevsky opened a hockey school in Mississauga, Ontario, running weeknight sessions at the Hershey Centre. By 2000, the school had relocated to Concord, Ontario, just north of Toronto, Ontario. The school was geared towards minor hockey players, aged 5 to 14. The program was largely successful, allowing Borschevsky to run four hour practice sessions every night of the week, catering to players of skills levels ranging from "recreational" to "AAA". As part of the school's extended program, Borschevsky and his hockey school trainers travelled to Detroit, Michigan, Guelph, Ontario, and Huntsville, Ontario each year to host his renowned camps remotely. These camps were well received by locals due to their unique and intense style. Certain sessions featured guest appearances by former and current NHL players, namely Danny Markov, Nik Antropov, and Boris Mironov.
Each summer, Borschevsky hosted tune-up camps for NHL players and prospects. These were attended by personalities such as Nik Antropov, Alexander Radulov, Andrew Cogliano, and Steve Montador. Borschevsky always stressed an attacking, finesse-oriented brand of hockey, which was offered by very few, if any, Ontario-based hockey programs.
Borschevsky's hockey programs were shut down in 2006 when he accepted a coaching job in the Russian Super League.


== Coaching career ==
Between 2004 and 2006, Borschevsky coached AAA minor hockey in the Greater Toronto Hockey League. He spent two seasons as the head coach of the Toronto Red Wings, and the Wexford Raiders, achieving mediocre results.
During the 2006–2007 Russian Super League season, Borschevsky served as the head coach of Lokomotiv Yaroslavl, succeeding Vladimir Yurzinov. In 34 regular season games, he guided the squad to a 14–14–6 record, before being replaced by incoming head coach, and former Canadian NHLer, Paul Gardner. He served the rest of the season as the team's assistant coach, seeing the squad through to the second round of the Super League playoffs.
Borschevsky served as the head coach of Lokomotiv-2, the farm club of the Super League team. The club finished the regular season in third place, boasting a 23–17 record, but was eliminated in the quarter final round of the playoffs by Khimik.
He later accepted a job as head coach with the Atlant Moscow Oblast.


== Career statistics ==


== International statistics ==


== External links ==
Nikolai Borschevsky's player profile at NHL.com
Nikolai Borschevsky's biography at Legends of Hockey
Nikolai Borschevsky's career statistics at The Internet Hockey Database
Nikolai Borschevsky profile at Eurohockey.com
Lokomotiv Yaroslavl Hockey Club