Tom Boonen (Dutch pronunciation: [ˈtɔm ˈboːnə(n)]; born 15 October 1980) is a Belgian professional road bicycle racer who won the 2005 world road race championship. He is a member of the Etixx–Quick-Step team, and is a single-day road specialist with a strong finishing sprint. He won the cycling monuments Paris–Roubaix 4 times and the Tour of Flanders 3 times, among many other prestigious victories, such as prevailing 5 times in the E3 Harelbeke, winning 6 stages of the Tour de France and winning the Overall title of the Tour of Qatar 4 times. 


== Career ==


=== Early years ===
At the start of 2002 Boonen rode for U.S. Postal Service, finishing third in Paris–Roubaix after an early breakaway. Fellow Belgian Johan Museeuw had escaped to a solo victory. Team captain George Hincapie crashed in a slippery section of the course leaving Boonen to ride for himself. Boonen's performance led Museeuw – his childhood hero – to declare Boonen his successor.
Boonen said US Postal did not give him enough chances to ride for himself. Towards the end of the year he said he would leave, despite being under contract, and joined Quick-Step–Davitamon at the start of 2003. The 2003 season, however, did not go well, with lacklustre performance due to fatigue and knee injury. Museeuw was team leader for the spring classics.
During the 2004 season Boonen won the E3 Prijs Vlaanderen, Gent–Wevelgem and the Scheldeprijs. He also won two stages of the Tour de France including the final stage in Paris, as Museeuw did in 1990.


=== 2005: Winning Ronde, Roubaix and Worlds ===

In 2005 Boonen won the Tour of Flanders, Paris–Roubaix and the E3 Prijs Vlaanderen, and came second in the Omloop "Het Volk" behind teammate Nick Nuyens. He was first to win the Tour of Flanders, Paris–Roubaix, and the World Cycling Championship in the same season.
In the Tour of Flanders Boonen appeared to be the strongest sprinter in the final group. However, he attacked a few kilometers from the finish to the surprise of others and stayed away. Erik Dekker said: "I'm happy that I am near the end of my career, since with a cyclist like Boonen the spring classics will be rather boring the coming years". In Paris–Roubaix, Boonen entered the velodrome in the leading trio, and waited until the last moment before outsprinting George Hincapie and the Spaniard, Juan Antonio Flecha.
In the Tour de France, Boonen won the second and third stages, taking the lead in the points classification. He retired after stage 11 due to injuries sustained in crashes. On 25 September Boonen became the 21st Belgian road world champion. He won the race in Madrid, after the leading six riders were caught. He outsprinted Alejandro Valverde to become the first Belgian since Museeuw, in 1996, to wear the rainbow jersey. He came second in the 2005 UCI ProTour rankings.
At the end of the year Boonen won several awards: Kristallen Fiets (Crystal Bicycle), Vélo d'Or (Golden Bicycle), Trofee voor Sportverdienste (Trophy For Sporting Merit), Belgian Sportsman of the year and Belgian Sports Personality of the Year.


=== 2006 ===

In 2006, Boonen won the Tour of Flanders and came second in Paris–Roubaix the following week. Leif Hoste, Peter Van Petegem and Vladimir Gusev placed second to fourth at Roubaix but were disqualified for riding through a closed level-crossing before a train passed. This promoted Boonen to second, behind Fabian Cancellara.
Boonen won the second and the third stages of the Tour of Belgium. Before the Tour de France he claimed himself to be the strongest and smartest sprinter. However, he did not win a stage in the first week, beaten by Robbie McEwen and Óscar Freire. However he wore the yellow jersey for the first time, losing it in the first time trial to Sergei Honchar. Boonen abandoned the Tour during the 15th stage – 187 km from Gap to l'Alpe d'Huez – when he was unable to reach the summit of the Col du Lautaret.
Boonen won three stages of the Eneco Tour of Benelux but could not keep his title at the world championship, held on a circuit that was hillier than in Madrid 2005. Paolo Bettini won and Boonen came ninth.


=== 2007 ===
In his 2007, Boonen won five stages of the Tour of Qatar and came second in the general classification behind teammate Wilfried Cretskens. He won Kuurne–Brussels–Kuurne and E3 Prijs Vlaanderen but he didn't win one of the five cycling monuments. His best placing was third in Milan–San Remo.
Boonen won stages 6 and 12 of the Tour de France in the absence of Alessandro Petacchi and Robbie McEwen. He won the points classification in the Tour de France, the first Belgian since Eddy Planckaert in 1988 to do so.


=== 2008 ===

Boonen began 2008 by winning four stages and the overall and points classifications in the Tour of Qatar. In the Tour of Flanders, he took on a defensive role when his teammate Stijn Devolder escaped and won. A week later, he outsprinted Fabian Cancellara and Alessandro Ballan in the final 500m to win the Paris–Roubaix. On 10 June 2008, reports said Boonen was negotiating a team place for him and other riders Bouygues Télécom at Bouyges, a French team. Its sporting director, Jean-René Bernaudeau, confirmed the report. Wilfried Cretskens and Kevin Hulsmans were named as the others involved.
Negotiations ended when Boonen tested positive for cocaine. Cocaine was not a performance-enhancing drug and Boonen faced no sanctions by the UCI or WADA. He apologized to his Quick Step manager, Patrick Lefévère at a press conference next day. Lefévère said Quick Step kept its confidence in him. But Boonen was barred from the Tour of Switzerland and the Tour de France. In February 2009 a Belgian court found him guilty of cocaine use but decided against sanctions, saying he has "been punished enough".


=== 2009 ===

Boonen began 2009 by winning a stage and the overall and points classifications in the Tour of Qatar. He also won Kuurne–Brussels–Kuurne for the second time. In the Tour of Flanders he had to take on a defensive role when his teammate Stijn Devolder escaped and won for the second time. The following week Boonen won Paris–Roubaix for the third time in his career.
On 27 April, Boonen tested positive for cocaine for the third time (the first, in November 2007, had not previously been made public). He was suspended by his team, Quick-Step, on 9 May. He began racing again in the Critérium du Dauphiné Libéré. In June, he won the national championship. After initiating legal proceedings he was allowed to compete in the Tour de France, just one day before the start on 3 July 2009. He pulled out, due to illness on 18 July, before the 15th stage.
He returned to racing in the Eneco Tour where he won the third stage by beating Tyler Farrar in the sprint. After that he entered the Vuelta a España to prepare for the final part of the season. There, he finished second in the prologue behind Cancellara. He crashed in the seventh stage, a 30 km time trial, losing by 1m 03s and ended the day second overall behind Cancellara. He withdrew during the 13th stage, due to the lasting effects of his crash in the seventh stage.
He finished his season with a second place in Paris–Tours, beaten in a sprint of three by fellow countryman and defending champion Philippe Gilbert.


=== 2010 ===

Boonen became third in the Tour of Qatar, winning two stages, then won stage five of the Tour of Oman. He won the second stage of Tirreno-Adriatico, before finishing second to Óscar Freire in Milan–San Remo. Boonen came second to Fabian Cancellara in the E3 Prijs Vlaanderen – Harelbeke, a result replicated at the Tour of Flanders. He came fifth in Paris–Roubaix the following week.
Boonen missed most of the rest of the season – including the Tour de France, the Belgian and the world championships – due to tendinitis in his left knee caused by crashes at the Tour of California and the Tour de Suisse. He returned to racing in October at the Circuit Franco-Belge and Paris–Tours.


=== 2011 ===
Boonen began the season by winning the opening stage of the 2011 Tour of Qatar. He won Gent–Wevelgem, came fourth in the Tour of Flanders and dropped out of Paris–Roubaix after crashing. Boonen also crashed on stage five of the Tour de France. His injuries forced him to abandon on stage seven. Boonen fell again in the Vuelta a España, which made him miss the world championship.


=== 2012 ===

Boonen began 2012 season by winning stage seven of his first race, the Tour de San Luis. In February, he won the Tour of Qatar, winning two stages and the points classification, and finished second to Sep Vanmarcke in Omloop Het Nieuwsblad.
Boonen won the second stage of Paris–Nice. He won the E3 Harelbeke and Gent–Wevelgem two days later. He was favourite for the Tour of Flanders, which he won in a sprint against Filippo Pozzato and Alessandro Ballan. His third victory equalled those of Achiel Buysse, Fiorenzo Magni, Eric Leman and Johan Museeuw. His fourth win in Paris–Roubaix equalled Roger De Vlaeminck. Boonen was first to win the Tour of Flanders and Paris Roubaix double twice. He is also the first to win E3 Harelbeke, Gent–Wevelgem, Tour of Flanders and Paris–Roubaix in the same year.
Boonen returned to racing at the Tour of California. He won the national championship title in June, taking the tricolor jersey from Philippe Gilbert.
Boonen skipped the Tour de France to prepare for the Olympic road race, riding the shorter Tour of Poland instead. He crashed in the first stage and withdrew on the fifth with a broken rib,. He recovered in time for the Olympics, and came 28th.
Boonen won the first edition of the two-day stage race World Ports Classic, winning the first stage in a sprint. He won the points classification and the overall lead after coming third on the second stage. One week later Boonen won Paris–Brussels.


=== 2013 ===

In January, Boonen spent a week in hospital with a serious infection after suffering a wound on his elbow. He returned to action in February in the Tour of Oman but could finish only 83rd in the General Classification. In March, he retired from both Gent–Wevelgem and the Tour of Flanders following crashes. He did not take the start of Paris–Roubaix when a fractured rib was diagnosed. Boonen won his first race of the year at the Heiste Pijl, an event not classified by the UCI, then was the victor of the second stage of the Tour de Wallonie in July.


=== 2014 ===

The season started well for Boonen as he took the second place overall behind his teammate Niki Terpstra and the points classification jersey in the mostly flat Tour of Qatar. His next feat came at Kuurne–Brussels–Kuurne, where he was part of a breakaway of 10 containing 4 of his teammates and 3 Belkin Pro Cycling riders. The breakaway made it home and Boonen had the better of Moreno Hofland in the sprint by a slim margin. He placed well in Paris–Roubaix and the Tour of Flanders, coming in tenth and seventh position, respectively.


=== 2015 ===

At the 2015 Omloop Het Nieuwsblad Boonen made the decisive break with teammates Niki Terpstra and Stijn Vandenbergh, along with Ian Stannard (Team Sky). With 4.5 km remaining Boonen attacked but was gradually brought back by Stannard. After Terpstra's immediate counter-attack failed Boonen was unable to follow Stannard's own attack, and finished third as Stannard outsprinted Terpsta for victory. On March 9 Boonen crashed out of Paris–Nice, suffering a dislocated shoulder which ruled him out of the rest of the classics season. Boonen returned to racing in late April, at the Tour of Turkey, where his role was to lead-out his teammate Mark Cavendish. He was preparing in Turkey for his first appearance in the Giro d'Italia. He abandoned the Giro after Stage 13 to participate to the Tour of Belgium, where he won the opening stage by outsprinting Arnaud Démare.
Boonen's season was brought to an end by a crash on the second stage of the Abu Dhabi Tour in October, which left him unconscious. He sustained a temporal fracture from the accident. After initially being told by doctors that it would take six months to recover, in a newspaper interview in December Boonen stated that he was training well two months after the crash without any trouble. However the accident had left him with permanent damage to his hearing. He also said that he was "100 per cent certain" that he would compete in motor racing after retiring from competitive cycling, with the aim of competing in the 24 Hours of Zolder.


=== 2016 ===
After enduring a relatively quiet series of performances through most of the cobbled classics, Boonen finished second at Paris–Roubaix, being pipped on the line by Mat Hayman. Despite not clinching the win, Boonen's aggressive performance in the race was acclaimed by former Paris–Roubaix champions Bernard Hinault and Gilbert Duclos-Lassalle, who described him as "a warrior" and "magnificent" respectively.


== Personal life ==
Boonen used to live in Balen, in the Flemish Region of Belgium until moving to Monaco in late 2005. He stayed there a few years until deciding to move back to Belgium in early 2012. In 2015 his longtime girlfriend Lore gave birth to twin girls. He tweeted the news saying:
“Our family has been extended with two little princesses. Valentine and Jacqueline both weigh 2.4 kg. The babies and mom are doing fine.”


== Palmarès ==


=== Monuments results timeline ===
DNF = Did not finish
— = Did not compete


== Notes ==


== References ==


== External links ==
Official website
Tom Boonen profile at Cycling Archives
Tom Boonen profile at Cycling Quotient
Tom Boonen profile at ProCyclingStats

Tom Boonen at Sports Reference
Quick.Step-Innergetic Cycling Team