Jack & Diane is a 2012 American romantic horror film written and directed by Bradley Rust Gray, and starring Riley Keough and Juno Temple. Olivia Thirlby and Ellen Page were originally cast for the lead roles in 2008 before the project was postponed.


== Plot ==
Diane suffers from chronic nosebleeds. She checks herself in a mirror and transforms into a monster before falling unconscious. Earlier, she walks in the streets trying to borrow a cellphone in order to call her twin sister Karen. Having no luck, she enters a clothing store and asks to use a phone. There, she meets a girl of her age, Jack, who is clearly smitten by Diane. Diane's nose starts bleeding again and Jack gives her a hand. Jack then takes Diane to a night club. When they get there, Diane seems nauseous, having lost a lot of blood from her nose, and she goes to the restroom. This scene is parallel to the beginning of the film because of the mirror. She gains consciousness, meets up with Jack and the girls passionately kiss.
By morning, the girls part ways and Jack is uncritically hit by a car. Back at Diane's home, she is reprimanded by her aunt Linda and tells her she is grounded. Diane strikes back by telling her she's not her mother. Both of the girls feel misunderstood back in their homes. Diane visits Jack in her home. Jack confides in Diane about her sentimental cassette tape from her late brother. One day, Jack visits Diane in her home where the girls get into an argument when Linda intentionally tells Jack about Diane leaving for school in France.
After Jack leaves her, Diane suffers from desolation and she starts to unravel. Meanwhile, Jack hangs out with a colleague, Tara. They have an intimate moment but Jack rebuffs her shortly thereafter. The next day, Diane hangs out with one of Jack's friend, Chris, and Jack is startled. The girls play a game where whoever wins would get to stay. Jack wins and Diane is forced to go home. Jack gets back to Chris where he finds a video of Diane's sister Karen getting raped on an adult website. Feeling sorry for her sister, Jack finally reconciles with Diane in her home.
Over the next few days, the girls finally spend a lot of time together. One night, Diane has a dream of transforming into a monster and devouring Jack's heart. She wakes up only to find Jack on her side with blood gushing out of her nose. As the days narrow down before Diane leaves for school, the bond between the two girls starts to turn debilitating. The girls go to a locker room where the lights suddenly go off. Afraid of the dark, Jack seeks help from Diane whom she had given a disposable camera before the lights went off. The couple are separated and Diane searches for Jack by flashing through her camera. While doing this, Diane encounters the monster for which she had previously dreamed of transforming into. Soon after she is found sobbing by Jack. The two girls console each other, finally letting go of their fears. Jack goes to see Diane on her last day before she departs for school and the girls' relationship is compromised.
After a few weeks, Diane receives a gift from Jack which is revealed to be the sentimental cassette tape from Jack's late brother. She turns the song on and listens to it profoundly, reminiscing about her encounter with Jack.


== Cast ==
Riley Keough as Jack
Juno Temple as Diane/Karen
Dane DeHaan as Chris
Haviland Morris as Jack's mother
Cara Seymour as Aunt Linda
Kylie Minogue as Tara
Lou Taylor Pucci as Tom
Neal Huff as Jerry
Michael Chernus as Jaimie


== Relation to the song ==
The John Mellencamp song "Jack & Diane" is not used as a soundtrack for the film. The only known similarity between the song and the film is that they both happen to be about teenagers in love (although the song is about a teenage boy named Jack and a teenage girl named Diane, whereas in the film, they are both girls).


== Reception ==
Jack & Diane received mixed to negative reviews; it currently holds a 13% rating on Rotten Tomatoes and a 45/100 rating on Metacritic, signifying "mixed or average reviews".


== References ==


== External links ==
Official website
Jack & Diane at the Internet Movie Database
Jack & Diane at Box Office Mojo
Jack & Diane at Rotten Tomatoes
Jack & Diane at Metacritic