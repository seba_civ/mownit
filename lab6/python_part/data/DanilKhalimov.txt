Danil Takhirovich Khalimov (Kazakh: Данил Тахирович Халимов; born July 6, 1978 in Nizhny Tagil, Russian SFSR) is a retired amateur Kazakh Greco-Roman wrestler, who competed in the men's middleweight category. He won two silver medals each in the 74-kg division at the 2002 Asian Games in Busan, South Korea and at the 2004 Asian Wrestling Championships in Almaty, and later scored a fifth-place finish at the Summer Olympics in Athens, representing Kazakhstan. Khalimov also trained full-time for Professional Sport Club Daulet in Almaty, under his personal coach Anvar Sagitov.
Khalimov qualified for his naturalized Kazakh squad in the men's 74 kg class at the 2004 Summer Olympics in Athens. Earlier in the process, he finished fourth from the 2003 World Wrestling Championships in Créteil, France, and then captured the silver medal at the Asian Championships to guarantee a spot on the Kazakh wrestling team. He ousted Georgian-born wrestler Yasha Manasherov of Israel in his opening match by a quick 8–0 margin, and then brushed aside Olympic veteran José Alberto Recuero of Spain with a tough 3–2 decision in the prelim pool to secure his place for the next round of the competition. Khalimov lost in overtime to Switzerland's Reto Bucher in the quarterfinal match at 0–3, but easily scored a triumph in a fifth-place playoff against two-time Olympic champion Filiberto Azcuy of Cuba, who withdrew from the tournament because of an injury.


== References ==


== External links ==
Profile – International Wrestling Database