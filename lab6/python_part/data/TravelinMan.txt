"Travelin' Man" is an American popular song, best known as a 1961 hit single sung by Ricky Nelson. Singer-songwriter Jerry Fuller wrote it with Sam Cooke in mind, but Cooke's manager was unimpressed and did not keep the demo, which eventually wound up being passed along to Nelson. His version reached No. 1 on the Billboard Hot 100. Its b-side, "Hello Mary Lou", reached No. 9 on the same chart.


== Plot ==
The song details the loves of a world traveler with an eye for beautiful women. Songwriter Fuller has described it as a "girl in every port" song. The women in each locale are referenced by a word or phrase associated with the location. The women were: a "pretty señorita" in Mexico, an Eskimo in Alaska, a fräulein in Berlin, a china doll in Hong Kong, and a Polynesian in Waikiki. There were others as well, "in every port ... at least one," mentioned obliquely during the opening verse. The song was produced by Joe Johnson who was also famous for The Champs recording of "Tequila". Joe was the owner of 4 Star Record Company and Challenge Records in Nashville.


== Covers ==
A cover was released by Jacky Ward in 1982, reaching #32 on the US country chart.


== See also ==
List of Hot 100 number-one singles of 1961 (U.S.)
The Wanderer (Dion song)


== References ==