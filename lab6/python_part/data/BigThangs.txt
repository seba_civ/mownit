Big Thangs is a compilation presented by American rapper and producer, Ant Banks. It was released July 8, 1997 on Priority Records. The album contains no tracks on which Ant Banks himself raps but instead is a compilation of tracks that Ant Banks produced and composed, performed by various West Coast rap artists. It peaked at number 4 on the Billboard Top R&B/Hip-Hop Albums and at number 20 on the Billboard 200.


== Track listing ==
"Intro" (featuring Otis & Shug)
"Big Thangs" (featuring Too Short and Ice Cube)
"Coolin in the Luff" (featuring Chuey Gomez)
"Can't Stop" (featuring E-40 and Mack 10)
"West Riden" (Intro by Young Jock, featuring King T and Spice 1)
"Hard Knox" (featuring J-Dubb, WC, Otis & Shug)
"Gamblin' wit' Ice-T" (featuring Ice-T)
"4 tha Hustlas" (featuring Too Short, 2Pac, MC Breed, Otis & Shug)
"Time Is Tickin'" (featuring Bad-N-Fluenz, Otis & Shug)
"Cutaluff" (Intro by Dr. Dre, featuring Slink Capone)
"Hoo-Ride Ant Banks" (featuring B-Legit, Mean Green, MC Ant and Baby DC)
"Make Money" (featuring CJ Mac, Mac Dee, Otis & Shug)
"Playa Paraphernalia" (Intro by Coolio, featuring J-Dubb and Rappin' 4-Tay)
"Fien" (featuring Allfrumtha I)
"You Want Me Back" (featuring Audrian, J-Dubb and CJ Mac)
"Outro"


== Chart history ==


== References ==