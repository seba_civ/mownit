Nikolai Nikolaevich Baskakov (Russian: Никола́й Никола́евич Баска́ков; May 8, 1918, Astrakhan, Soviet Russia – October 14, 1993, Saint Petersburg, Russia) was a Soviet, later Russian, painter, a member of the Saint Petersburg Union of Artists (before 1992 known as the Leningrad branch of Union of Artists of Russian Federation), who lived and worked in Leningrad, regarded as one of the leading representatives of the Leningrad school of painting, most famous for his genre and portrait painting.


== Biography ==
Nikolai Nikolaevich Baskakov was born May 8, 1918 in a village seven kilometers from the Astrakhan city on the Volga River. His father, Nikolai Evlampievich Baskakov was a carpenter, his mother Evdokiya Vasilievna was a housewife raising children their children- nine sons and two daughters.
In 1933–1939 Baskakov studied under the famous Russian art educator Pavel Alekseevich Vlasov in the Astrakhan School of Art. In 1939 he was drafted into the Red Army, having served in the Far East. There, in 1943, he participated in an art exhibition, his work "Khabarovsk, the year 1942" was one among the thirty-eight works of artists of Khabarovsk that had been presented in the exhibition in Moscow.

In 1945 Nikolai Baskakov went to Leningrad and entered the Department of Painting of the Leningrad Institute of Painting, Sculpture and Architecture, where he studied under Leonid Ovsannikov, Boris Fogel, Genrikh Pavlovsky, and Alexander Zaytsev.
In 1951 Nikolai Baskakov graduated from the Ilya Repin Institute of Arts under the supervision of the Boris Ioganson's workshop, together with Alexei Eriomin, Mikhail Kaneev, Maya Kopitseva, Anatoli Levitin, Avenir Parkhomenko, Arseny Semionov, Mikhail Trufanov, Boris Ugarov, and other young artists of the time. His graduation work had been the historical painting "Lenin and Stalin in Smolny".
Since 1951, Baskakov had been a permanent exhibitor of the Leningrad Art exhibitions, where he presented his work among with others pieces of art by the leading painters of fine arts in Leningrad. He painted historical paintings and war-depicting works, genre scenes, portraits, landscapes and made sketches of every-day life. Baskakov worked in the technique of oil painting. In the same year together with artists Grigory Chepets and Mikhail Trufanov he performed his first large order - the painting "Presenting of the Order of Lenin to the Kirov factory", for which he was admitted as a member of the Leningrad Union of Soviet Artists.
In 1950-1960s, Baskakov also created paintings "On a floating fish factory" (1954), "A Winter" (1955), "Masha" (1956), the "A Letter" (1957), "On the River" (1958), "Grandma and granddaughter", "A Teacher", "Summer" (all 1960), "Pig farm workers", "Chiefs on the farm" (both 1961), "Plasterers" (1964), and others. Notable among these are the paintings, which dates back to the dramatic events of wartime. Baskakov creates images filled with deep domestic drama, among them paintings "Music" (1957), "During the war", (1967), "On the Russian land" (1968), "A Victory" (1974), "Conversation with son", "Day of Victory" (both 1980).
In 1956 Baskakov created the painting "Lenin in the Gorki", where Lenin was depicted sitting in a chair on a summer day in the garden. The author's approach is seen in large genre painting "Lenin in the Kremlin" (1960). Later Baskakov created the paintings "Lenin on vacation" (1970) and "On New Year's celebration in Gorky" (1972).
Baskakov gained fame also as a portraitist. Among his the best known works created in this genre were "Guitarist" (1949), "Portrait of a Mother" (1955), "Portrait of a tractor driver" (1956), "Portrait of a Fisherman" (1959), "The Son of a shepherd" (1962), "Girl with willows" (1967), a large series of portraits of the workers of the Kirov factory, created in 1960–1980 years, "Schoolgirl", "Hunter Grisha Eriomin" (both 1971), "Grandma with the grandchildren" (1972), "The guy from Dudinka", "Old Nganasanka" (both 1973), "A worker Fadeev with pupils", "Collective Farm keeper Mitrich" (both 1975), "Portrait of an old Uzbek woman" (1977), "Sisters" (1979), "Portrait of a welder" (1980), "Portrait of Vasily Shukshin", "Portrait of the Artist" (both 1984), "A Group portrait of Kirov factory workers" (1985), "A Portrait of the poet Nikolai Rubtsov" (1988), and others.
For the art of Nikolai Baskakov typical appeal to the themes of large public sound and to the image of a contemporary, disclosed in a picture-portrait. He was most known for his impressive portraits of contemporaries painted in realistic style.
The West discovered the art of Nicholas Baskakov in 1989, when his portraits and genre paintings were first shown in Paris at the auctions of the Leningrad school artists. Subsequently many of his works were acquired by American private collections.
In 1991 Saint Petersburg publishing house "Khudozhnik RSFSR" issued a monograph by V. Kirillov, devoted to creation of Nikolai Baskakov. Nikolai Baskakov was elected a full member of Peter's Academy of Sciences and Arts. In 1989-1992 his paintings were exhibited in France on auctions of Russian paintings L'École de Leningrad and others.
Nikolai Nikolaevich Baskakov died on October 14, 1993, in Saint Petersburg at the seventy-six year of life. His paintings reside in State Russian Museum, in Art museums and private collections in the Russia, Japan, in the U.S., Germane, England, France, and other countries.


== See also ==
Lenin in Kremlin (painting)
Fine Art of Leningrad
Leningrad School of Painting
House of creativity «Staraya Ladoga»
List of Russian artists
List of 20th century Russian painters
List of painters of Saint Petersburg Union of Artists
List of the Russian Landscape painters
Saint Petersburg Union of Artists


== References ==


== Gallery ==


== Exhibitions ==


== Sources ==
Central State Archive of Literature and Arts. Saint Petersburg. F.78. Inv.8/2. C.208.
Весенняя выставка произведений ленинградских художников 1954 года. Каталог. Л., Изогиз, 1954. С.8.
Осенняя выставка произведений ленинградских художников 1956 года. Каталог. Л., Ленинградский художник, 1958. С.6.
1917 — 1957. Выставка произведений ленинградских художников. Каталог. Л., Ленинградский художник, 1958. С.9.
Всесоюзная художественная выставка, посвящённая 40-летию Великой Октябрьской социалистической революции. Каталог. М., Советский художник, 1957. С.11.
Осенняя выставка произведений ленинградских художников 1958 года. Каталог. Л., Художник РСФСР, 1959. С.7.
Шведова В. Над чем работают ленинградские художники // Художник. 1959, № 9.
Выставка произведений ленинградских художников 1960 года. Каталог. Л., Художник РСФСР, 1963. С.7.
Выставка произведений ленинградских художников 1960 года. Каталог. Л., Художник РСФСР, 1961. С.9.
Выставка произведений ленинградских художников 1961 года. Каталог. Л., Художник РСФСР, 1964. С.9.
Осенняя выставка произведений ленинградских художников 1962 года. Каталог. Л., Художник РСФСР, 1962. С.8.
Ленинград. Зональная выставка 1964 года. Каталог. Л, Художник РСФСР, 1965. C.10.
Третья республиканская художественная выставка «Советская Россия». Каталог. М., Министерство культуры РСФСР, 1967. С.19.
Весенняя выставка произведений ленинградских художников 1969 года. Каталог. Л., Художник РСФСР, 1970. C.8.
Выставка произведений ленинградских художников, посвященная 25-летию победы над фашистской Германией. Каталог. Л., Художник РСФСР, 1972. C.6.
Художники народов СССР. Биографический словарь. Том первый. — М: Искусство, 1970. — с.298-299.
Весенняя выставка произведений ленинградских художников 1971 года. Каталог. — Л: Художник РСФСР, 1972. — с.7.
Наш современник. Каталог выставки произведений ленинградских художников 1971 года. Л., Художник РСФСР, 1972. С.8.
Наш современник. Вторая выставка произведений ленинградских художников 1972 года. Каталог. Л., Художник РСФСР, 1973. C.5.
Наш современник. Третья выставка произведений ленинградских художников 1973 года. Каталог. — Л: Художник РСФСР, 1974. — с.7.
Дмитренко А. Мир современника. // Ленинградская правда, 1975, 12 октября.
Наш современник. Зональная выставка произведений ленинградских художников 1975 года. Каталог. Л., Художник РСФСР, 1980. C.12.
Портрет современника. Пятая выставка произведений ленинградских художников. Каталог. Л., Художник РСФСР, 1983. C.6.
Изобразительное искусство Ленинграда. Каталог выставки. Л., Художник РСФСР, 1976. C.14.
Выставка произведений ленинградских художников, посвящённая 60-летию Великого Октября. Л., Художник РСФСР, 1982. С.12.
Осенняя выставка произведений ленинградских художников. 1978 года. Каталог. Л., Художник РСФСР, 1983. С.6.
Справочник членов Союза художников СССР. Том 1. М., Советский художник, 1979. C.98.
Зональная выставка произведений ленинградских художников 1980 года. Каталог. Л., Художник РСФСР, 1983. C.10.
Artists of the USSR. Biography Dictionary. Volume 1. Moscow, Iskusstvo Edition, 1970. P.298–299.
Vianor A. Kirillov. Nikolai Nikolaevich Baskakov. Leningrad, Khudozhnik RSFSR, 1991.
Charmes Russes. Auction Catalogue. Paris, Drouot Richelieu, 15 Mai 1991. P.48-51.
L' École de Leningrad. Auction Catalogue. Paris, Drouot Richelieu, 21 Decembre 1990. P.74-77.
L' École de Leningrad. Auction Catalogue. Paris, Drouot Richelieu, 25 Novembre 1991. P.24-29.
Peinture Russe. Catalogue. Paris, Drouot Richelieu, 18 Fevrier, 1991. P.7,68-71.
Peinture Russe. Catalogue. Paris, Drouot Richelieu, 26 Avril, 1991. P.7,47-50.
Matthew C. Bown. Dictionary of 20th Century Russian and Soviet Painters 1900-1980s. - London: Izomar, 1998. ISBN 0-9532061-0-6, ISBN 978-0-9532061-0-0.
Vern G. Swanson. Soviet Impressionism. Woodbridge, England, Antique Collectors' Club, 2001. P.186–188. ISBN 1-85149-280-1, ISBN 978-1-85149-280-0.
Sergei V. Ivanov. Unknown Socialist Realism. The Leningrad School. Saint Petersburg, NP-Print Edition, 2007. P.9, 20, 24, 357, 388-401, 403-406, 439. ISBN 5-901724-21-6, ISBN 978-5-901724-21-7.
Artists of Peter's Academy of Arts and Sciences. Saint Petersburg, Ladoga Edition, 2008. P.22–23.
Nikolai Baskakov. Lenin in Kremlin // 80 years of the Saint Petersburg Union of Artists. The Anniversary Exhibition. Saint Petersburg, 2012. P.203.
Иванов С. Инвестиции в советскую живопись: ленинградская школа // Петербургские искусствоведческие тетради. Вып. 31. СПб, 2014. С.60.


== External links ==
Paintings of Nikolai Baskakov on site "Unknown Socialist Realism. Quests and Discoveries"
Painting of Nikolai Baskakov on VIDEO "Children and Youth in Painting of 1950-1980s. The Leningrad School. Part 2"
The Leningrad School of Painting. Historical Outline
Chronology of the Leningrad School of Painting