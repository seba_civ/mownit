Cosimo Damiano Lanza (born 12 April 1962) is an Italian pianist, harpsichordist and composer.


== Biography ==
Born in Leporano, Southern Italy, his artistic training was developed under the leadership of distinguished teachers, for the piano Michele Marvulli and Jorg Demus, for the harpsichord Christopher Steinbridge and Pierre Hantai. He has been invited to give concerts and master classes in the world's major institutions. Some of his performances were recorded by RAI Italian Television. In addition to the interest in early music, he has followed with great perseverance the modern and contemporary repertoire (Luigi Dallapiccola, Luciano Berio, Karlheinz Stockhausen, John Cage). He also works with young musicians.
He served as chairman or member of the jury at international piano competitions Dinu Lipatti, Luciano Gante, Arcangelo Speranza, Premio Italia, the International Piano Competition of Barletta. Since 1996 he is the director of the Accademia Musicale Mediterranea di Taranto and art-director of the International Music Competition for Youth Dinu Lipatti at the Romanian Academy in Rome and Premio Italia Olimpo Pianistico in Taranto. Lanza has also recorded works by Frédéric Chopin, Robert Schumann and John Cage. For the publishing house Bèrben of Ancona, he has published 6 Preludi per pianoforte (Natale sul 904 - Ondine - Nafplion - Camminando - Profili Nostalgici - Crescendi Sensoriali) Ed. No. 3346, listed in the National Central Library of Florence.
National Central Library (Florence)


== Professional collaborations ==
Lanza has held master classes and courses in music institutions worldwide, including: Brigham Young University in the United States of America, Conservatory of Murcia in Spain, Conservatory of Gap in France, International Musikschule to Munich in Germany, Accademia Musicale Mediterranea - Taranto, Accademia Umbra - Perugia in Italy, International Academy of Arts - Rome, European Music Academy - Naples, Teatro Comunale - Sirolo, Conservatory Tchaikovsky - Nocera Terinese, AEMAS Suor Orsola Benincasa University of Naples in Italy.


== Awards ==
During his career he has received several awards and official recognitions:
2010 The Bronze Medal - President of the Italian Republic Giorgio Napolitano
2009 Award Saturo d'Argento.
2008 Prize Tebaide d'Italia


== References ==
Free scores by Cosimo Damiano Lanza at the International Music Score Library Project Project Petrucci LLC.
http://opac.sbn.it/opacsbn/opac/iccu/scheda_authority.jsp?bid=IT\ICCU\CFIV\148826
http://opac.bncf.firenze.sbn.it/opac/controller?action=search_byautoresearch&query_fieldname_1=vidtutti&query_querystring_1=CFIV148826
6 Preludi per Pianoforte (Natale sul 904 - Ondine - Nafplion - Camminando - Profili Nostalgici - Crescendo Sensoriali) Ed. No. 3346 Bèrben http://www.berben.it/Data/Catalogo_Generale_2013.pdf pag. 89
REBUZZI Roberto Edgardo - Degni di Nota - TritticodiemozionI - Leporano, Amministrazione Comunale, 1993.
REBUZZI Roberto Edgardo - A Quattr' Occhi - Taranto, Edizione Accademia Internazionale dei Dioscuri, 2009.


== External links ==
Cosimo Damiano Lanza sul sito dell'Accademia Musicale Mediterranea
Cosimo Damiano Lanza Biografia su Circuitomusica.it