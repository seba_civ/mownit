Dawn Primarolo, Baroness Primarolo, DBE PC (born 2 May 1954), is a British Labour Party politician who was the Member of Parliament for Bristol South from 1987 until 2015, when she stood down. She was Minister of State for Children, Young People and Families at the Department for Children, Schools and Families from June 2009 to May 2010 and a Deputy Speaker of the House of Commons from 2010. She was appointed Dame Commander of the Order of the British Empire (DBE) in the 2014 Birthday Honours for political service. She was nominated for a life peerage in the 2015 Dissolution Honours.


== Early life and career ==
Born in London, Primarolo was raised in Crawley, West Sussex, where she attended Thomas Bennett comprehensive school. She then studied at Bristol Polytechnic as a bookkeeper and legal secretary. Returning to London, in 1973 she joined the Labour Party whilst employed as a legal secretary in an east London Law Centre.
After marrying, she moved back to Bristol south to raise her son. She then studied for a social science degree at Bristol Polytechnic, where she gained a BA (Hons). Whilst working, she then continued her studies at the University of Bristol, conducting Ph.D research into women and housing.
Becoming involved in her local community, Primarolo belonged to various women’s groups and was active in CND, a founder member of Windmill Hill City Farm, and a school governor.


== Political career ==
Active in her local Labour Party, in 1985 she was elected to Avon County Council, where she acted as vice chair of the Equal Opportunities Committee.


=== Westminster ===
Primarolo was first elected to Parliament at the 1987 general election, after the constituency party de-selected Michael Cocks, the sitting MP.
She found fame in 1989 by asking Margaret Thatcher if the only hope for low-paid women was "to follow her example and find herself a wealthy husband". She was reading out a question on behalf of Ann Clwyd, at the time, who had "lost her voice".
At the time of her election, she was regarded as a hard left-winger and is still often referred to by the media as "Red Dawn", but became a New Labour loyalist and "absolutely loyal to New Labour", leading Andrew Roth of The Guardian to say she has "changed from 'Red Dawn' to 'Rosy Pink'"; as part of this change, she has moved from support of CND, the rise of which originally encouraged her into politics, to voting for the renewal of Britain's Trident nuclear defence. She also unsuccessfully lobbied the Soviet government of Mikhail Gorbachev to rehabilitate Leon Trotsky. She is a former member of Avon County Council.


=== Frontbench ===
Primarolo has held the following positions:
1992–1994: Opposition Spokesman for Health
1994–1997: Opposition Spokesman for the Treasury
1997–1999: Financial Secretary to the Treasury
1999–2007: Paymaster General
2007–2009: Minister of State for Public Health
2009–2010: Minister of State Children and Young People
Despite campaigning against the first Gulf War in 1991, she voted in favour of the Iraq War in 2003, and against any investigation into the invasion after it had taken place. On other 'key issues' (as described by TheyWorkForYou), she has voted in favour of ID cards and increased university tuition fees.
As Paymaster General, Primarolo was responsible for the administration of the Tax Credits system, intended to provide working families with financial support. However, the administration of this system received significant criticism, including allegations that some families were left less well off as a result. In 2003, a Treasury select committee member accused her of "losing control of [her] department" after it became known that Inland Revenue buildings under Primarolo's purview had been sold to tax-haven companies. This came shortly after she had "insisted ... the Child tax credit scheme was a 'success'", despite Inland Revenue staff walking out in protest against the pressure under which they were placed. She was also responsible for introducing the controversial IR35 tax rules which were designed to tax "disguised employment" at a rate similar to employment. The measure was controversial as it was seen by some as unfair. Primarolo was also the longest serving Paymaster General in the office's 200-year history. Primarolo was named Chair of the Code of Conduct Group upon its establishment by ECOFIN in March 1998.
In 2005, PM Tony Blair was forced to apologise after a report by the Parliamentary Ombudsman that Primarolo had failed to give Parliament accurate information. Primarolo admitted at the same time that she had been fully aware "about the extent of the problems".
As Minister of State for Public Health she was responsible for health improvement and health protection issues including such areas as tobacco, obesity, drugs and sexual health, as well as international business, pharmacy and research and development.
On 5 June 2009 Primarolo was moved again, this time succeeding Beverley Hughes as Minister of State for Children, Young People and Families at the Department for Children, Schools and Families. This gave her the right to attend cabinet when her responsibilities were on the agenda.
Primarolo's abilities as a minister have been questioned, with former Prime Minister Tony Blair revealing in his autobiography A Journey that he did not think she was "right for government" but had to give her a job because she was one of Gordon Brown's key allies; and political commentator Danny Finkelstein arguing that she was "contender no. 1" for title of "Labour's worst Minister". Jonathan Powell, Blair's Chief of Staff, is reported as saying "We fired Dawn Primarolo about ten times. And each time Gordon (Brown) insisted we put her back."


=== Deputy Speaker ===
Primarolo joined the Shadow Cabinet as Shadow Minister for Children when Labour entered opposition in May 2010. In September 2010 she became Deputy Speaker of the House of Commons. In November 2011 she announced her intention to stand down from Parliament at the next general election.
Primarolo was created a life peer taking the title Baroness Primarolo, of Windmill Hill in the City of Bristol on 26 October 2015.


== Personal life ==
Primarolo married UNISON regional secretary Ian Ducat in Bristol in 1990. On 13 May 2007, it was alleged that John Reid "sexually harassed" Primarolo during her early years in Parliament.


== References ==


== External links ==
Bristol South Labour Party

Profile at Parliament of the United Kingdom
Contributions in Parliament at Hansard 1803–2005
Current session contributions in Parliament at Hansard
Voting record at Public Whip
Record in Parliament at TheyWorkForYou
Profile at Westminster Parliamentary Record
Profile at BBC News Democracy Live
The Rt Hon Dawn Primarolo MP Department of Health (archived)