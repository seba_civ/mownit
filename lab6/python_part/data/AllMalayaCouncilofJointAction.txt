The All-Malaya Council of Joint Action (AMCJA) was a coalition of political and civic organisations in Malaya formed to participate in the development of a constitution for post-war Malaya in preparation for independence and to oppose the Constitutional Proposals for Malaya (also known as the Federation Proposals or the Anglo-Malay Proposals) which eventually formed the basis of the Federation of Malaya Agreement.


== History ==


=== Background ===

In seeking to solve some of the administrative incoherence in the pre-war British ruled Malaya, a policy of constitutional development which incorporated the twin goals of constitutional unity and a common citizenship within Malaya was developed as the basis for eventual self-rule and independence of the territory. The first proposal called for the Federated Malay States and Unfederated Malay States to be joined into a larger federation styled the Malayan Union. It was expected that Penang and Malacca would be severed from the Straits Settlements to join the new federation while Singapore remained a separate Crown Colony.
Significant Malay opposition to the Union was spontaneous and widespread as it was seen as a departure from the traditional pro-Malay policies of the British and the removal of sovereignty of the Malay rulers while a significant majority of non-Malays were generally divided or indifferent to the proposals. The preoccupation with post-war rebuilding and the lack of an existing Malaya-centric political discourse meant that even the community most likely to view Malaya as their home like the Straits Chinese and second generation non-Malays failed to appreciate the implications of the Union until it was abandoned by the British. Only openly anti-colonial movements like the radical Malayan Communist Party (MCP) and the more moderate Malayan Democratic Union (MDU), established by English educated left-leaning middle-class intellectuals in Singapore in 1945, emerged to support the proposal with the caveat that Singapore was included in the Union.
With the widespread opposition among the Malays, the British administration entered into secret negotiations with the Malay aristocracy and the United Malay National Organisation (UMNO) as they were unwilling to allow the Malay opposition to the Union develop into an anti-British attitude in the same way that had happened in the Dutch East Indies where the locals were engaged in an open armed rebellion against the Dutch. When news that the British had agreed to the demands of the conservative Malays and the Anglo-Malay Proposals included institutionalised handicaps against the non-Malay community and the absence of a road map towards Malayan independence, a united front was mulled to oppose the proposals.


=== United front proposed ===
On 19 November 1946, a meeting was held to discuss the formation of a united front. Attending this meeting were:
Following a telegrammed suggestion by Tan Cheng Lock, three central principles were adopted:
A united Malaya including Singapore
A popularly elected Central Government and popularly elected State councils
A citizenship granting equal rights to all who made Malaya their permanent home and the object of their undivided loyalty


=== Formation ===
On 14 December 1946, the MDU sponsored a meeting in Singapore which was participated by the Malay Nationalist Party (known by its Malay acronym PKMM), the Malayan Indian Congress (MIC) and various other groups to:

"provide the machinery for the various communities, through their organisations and associations, to reach agreement on all points connected with the future constitution of Malaya, thus avoiding the dangers of separated and self-interested representation"

The immediate result of this meeting was the formation of the Council for Joint Action (CJA) comprising the MDU, PKMM, MIC, the General Labour Union (later to split into the Singapore Federation of Trade Unions or SFTU and the Pan Malayan Federation of Trade Unions or PMFTU), the Singapore Clerical Union, the Straits Chinese British Association (SBCA), the Singapore Indian Chamber of Commerce, the Singapore Tamil Association and the Singapore Women's Federation operating on the three principles adopted during the November meeting. Tan Cheng Lock was appointed the chairman with MDU's Paul Eber as Secretary-General. A memorandum of protest was sent by the CJA to Arthur Creech Jones, a trade unionist who was then the Under-Secretary of State for the Colonies for the British government on 16 December of the same year with the announcement that the CJA intended to boycott the Consultative Committee established to discuss and implement the Anglo-Malay Proposals.
This CJA was expanded on 22 December 1946 with the inclusion of the Pan Malayan Federation of Trade Unions, the Clerical Unions of Penang, Malacca, Selangor and Perak, the Selangor Indian Chamber of Commerce, the Selangor Women's Federation, the Malayan New Democratic Youth's League, the Malayan People's Anti-Japanese Ex-Comrades Association, the Singapore Chinese Association and the Peasant's Union. A press conference in Kuala Lumpur announced the formation of the Pan-Malayan Council of Joint Action (PMCJA) with Tan Cheng Lock re-elected chairman with MDU's Gerald de Cruz as Secretary-General.
The PMCJA sought to gain recognition from to act as the sole representative body with which the British government would negotiate with a view to amending the constitution in accordance with the wishes of that part of the populace that has not been consulted so far. Nonetheless, the negotiated Anglo-Malay proposals were published as a government White Paper on 24 December 1946 together with a note from the Governor, Edward Gent, that the proposals were conditionally accepted by the British government provided that

"all interested communities in Malaya have had full and free opportunity of expressing their views"


== Major activities ==


=== People's Constitution ===
Opposition to the Anglo-Malay proposal increased with demonstrations being held across Malaya and consultations were held to prepare an alternative set of proposals to be tabled to the British government. On 22 February 1947, a coalition of Malay organisations opposed to the Anglo-Malay agreement and led by the MNP, Pusat Tenaga Ra'ayat (PUTERA), was formed and by March of the same year the PMCJA had established a coalition with PUTERA known as PUTERA-PMCJA. The PUTERA-PMCJA adopted a total of 10 principles; including the three original principles of the CJA; as the basis of their constitutional proposals:
Due to concerns about the implications of the term Pan-Malayan which groups like the Tan Kah Kee and Lee Kong Chian led Associated Chinese Chambers of Commerce (ACCC) considered to be indicative of communist domination and the MNP considered to include only non-Malays, the name of the PMCJA was changed to become the All Malaya Joint Council for Action or AMCJA in August 1947 and the PUTERA-PMCJA coalition became known as PUTERA-AMCJA.
The PUTERA-AMCJA constitutional proposals was adopted by the constituent members of the both coalitions on 10 August 1947 and presented to the public on 21 September as the People's Constitutional Proposals. A summary of the differences between the People's Constitutional Proposals and the Revised Constitutional Proposals published in July 1947 based on the report of the Government Consultative Committee are:


=== All Malaya Hartal ===
The ACCC considered the Revised Constitutional Proposals as being autocratic and irresponsible and it threatened to delay the independence of Malaya indefinitely. A decision was made to co-operate with PUTERA-PMCJA (later PUTERA-AMCJA) because it had exhausted all constitutional channels of appeal (the ACCC was a participant in the Government Consultative Committee) and appeal to the British Parliament for the establishment of a Royal Commission to review and reverse the Revised Constitutional Proposals.
Agitation against the Revised Constitutional Approvals grew throughout September with a successful hartal organised in Malacca and Ipoh in protest. Emboldened by the success, the ACCC decided to launch a country-wide strike and PUTERA-AMCJA was invited to support the strike. A decision was made to hold the strike, to be known as the All Malaya Hartal, on 20 October 1947 to coincide with the opening of the session of the British Parliament where the Revised Constitutional Proposals were to be tabled and debated.
The hartal turned out to be a major success although UMNO held counter demonstrations in the more rural areas like Senggaram and Bagan Datoh contributing to the rise in ethnic tensions and the cancellation of the planned strike in those areas.
The hartal was also successfully carried out in Singapore, which received the prominent support of the Singapore Chinese Chamber of Commerce led by its chairman Lee Kong Chian.


== Decline and dissolution ==
Despite the success of the All Malaya Hartal, the government granted no concessions and differences began to emerge between the ACCC and PUTERA-AMCJA. A second Hartal was planned for 1 February 1948 but was aborted when financial support from the ACCC was not forthcoming and was reduced to isolated strikes by the PMFTU. Kuomintang sympathizers had also begun to lobby for the withdrawal of ACCC support from the PUTERA-AMCJA due to the intensification of the Chinese civil war.
The implementation of the Federation of Malaya constitution based on the Revised Constitutional Proposals on 1 February 1948 and the decision of the MCP to launch an armed rebellion marked the beginning of the end for the PUTERA-AMCJA coalition and AMCJA as a whole. With the declaration of the nationwide emergency, the constituent organisations either withdrew from the coalition, went underground, or in the case of the MDU, voluntarily dissolved itself and the AMCJA ceased to exist as a body.
Mainstream political developments in Malaya in the following decade came to be dominated by conservative and pro-British groups with a distinctive impact on the historical development of independent Malaya, and later Malaysia, for the next few decades.


== References ==