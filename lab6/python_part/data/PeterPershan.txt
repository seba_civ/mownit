Peter S. Pershan is a prominent American physicist.


== Education and career ==
Peter Pershan earned his B.S. at Polytechnic Institute of Brooklyn in 1956 and his Ph.D. in Physics at Harvard University in 1960 for nuclear magnetic resonance under the supervision of Nicolaas Bloembergen. After a short postdoctoral appointment with Bloembergen he was appointed an assistant professor at Harvard University, where he is now a Frank B. Baird, Jr. Professor of Science, at both Physics Department and Division of Engineering and Applied Sciences.


== Research ==
Pershan began his career in nuclear magnetic resonance; however, before moving on to other things, he and Bloembergen produced some of the first papers on non-linear optics, a field for which Bloembergen later received Nobel Prize in Physics in 1981.
He remained active in this field until the early 1980s when he wrote a book on liquid crystals and moved into the then-new field of synchrotron radiation.
Along with Jens Als-Nielsen, Pershan developed the first synchrotron X-ray reflectometer for the study of the horizontal free surface of a liquid, and carried out the first synchrotron measurements on liquid surfaces at HASYLAB, DESY in 1982. The liquid surface spectrometers now at Advanced Photon Source and the National Synchrotron Light Source are all variations of the HASYLAB instrument.
Since 1982 Pershan has led the field in exploration of such diverse liquid surfaces as superfluid helium, water, and liquid metals. Aside from sabbaticals at the University of Paris, Brookhaven National Laboratory, and Risø National Laboratory Pershan has been at Harvard University (where he has been a faculty member since 1961) since he arrived as a graduate student in September 1956.


== Books ==
In addition to over 240 scientific articles Peter Pershan has published two books:
"Structure of Liquid Crystal Phases", World Scientific, 1988 (ISBN 9971506688)
"Liquid Surfaces and Interfaces: Synchrotron X-ray Methods" (with Mark Schlossman), Cambridge University Press, 2012. (ISBN 9780521814010)


== External links ==
Peter Pershan's web page
Peter Pershan's profile at Harvard