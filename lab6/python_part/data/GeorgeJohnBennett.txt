George John Bennett (1800–1879) was for nearly 40 years a Shakespearian actor on the London stage, notably Covent Garden and Drury Lane.


== Life ==
He was the son of the eminent popular comedian George Bennett and Harriet Morland, the daughter of an ancient family in Westmorland (parents: Jacob Morland of Killington, Dorothy Brisco of Kendal, and sister, Lady Shackerley of Somerford Hall). Both parents acted for the Norwich Company of Comedians. He was born in Ripon in Yorkshire on 9 March 1800. At the age of 18, he acted at the Lynn Theatre in Norfolk under the management of Messrs. Elliston and John Brunton.


== The Provincial circuits ==
From the Lynn theatre, he went to the Theatre at Newcastle, where he played with great success under the eminent tragedian Macready. After two years of wandering from theatre to theatre, from Richmond to North and South Shields, his popularity increasing wherever he went, he finally settled in the York circuit, where his reputation as an actor was permanently confirmed. He remained there under the management firstly of Mrs. Mansell and next of Mrs. Fitzgerald.
While on the York circuit, and after having received numerous invitations from other companies, he decided, in 1820, to join the Bath Company. This Theatre was the provincial school of histrionic art, and to any actor of ability, at once assured an eventual London engagement. In 1822, he made his first appearance on the London boards, at the Covent Garden Theatre, having selected the part of Richard III for his debut. In this role, however, his success, though unequivocal, did not meet his admirers’ expectations. Unfortunately for Bennett, Edmund Kean had already made the part of Richard very much his own, and the crowds were not ready to accept a new Richard; even William Macready had struggled to convince the audience in this part.
He subsequently played the character of Hubert in “King John” where he was well received. He would play this character for the rest of the season to crowded houses. The next season, he was offered the part of Hotspur, which he had originally selected for his debut, and he alternated this character with Mr. Young for the remainder of the season, playing also Iago, Jaffier, Cassius, Edmund in “King Lear,” “and Joseph Surface in “ The School for Scandal.”
His name was becoming increasingly well-known, as character after character was performed with increasing success, each role showing that Mr. Bennett never neglected an opportunity to study, and this helped the extraordinary natural genius he had demonstrated earlier. Amongst the roles added to his repertoire were the Duke of Malfi, in Miss Mitford’s ”Julian,” Figaro in “The Marriage of Figaro,” Frankenstein in “ Presumption,” Telaxo in ” Cortez,” Caspar in ’ Der Freischütz.” and Old Foster in ‘Woman never Vexed'.
Both at this theatre and at the Royal English Opera House, Mr. Bennett played the characters of Hotspur, Romeo, Jaques, Carlos, Sir Reginald, and Cassius in “Julius Caesar.” In Caesar, his portrayal of the hasty yet warm-hearted friend of Brutus, was unrivalled, while his faithful representation of the man who “beareth anger as a flint bears fire and straight is cold again” was masterly in the extreme. At a later period, he was cast for Brutus in the same tragedy. Here he exhibited that peculiar versatility, or rather capability, of adapting himself to the spirit of his part, which had characterised his previous roles, and led to his lasting reputation.


== Bennett in London - Drury Lane ==
George Bennett later accepted an engagement at Drury Lane, in 1825, then under the management of Mr. Elliston, with whom he had worked in Norfolk in the earlier part of his career. This gentleman was quick to discover the rapid progress the young Bennett had made in only such a short period, both in his profession and his steadily increasing popularity. As a result, he was to be found performing in Kenny’s “ Dream of Benyowski,” and likewise adding Sir Kenneth of Scotland, in ”The Knights of the Cross,’ Iacliimo, Wilford, Bassiano, and Faulkland, to his repertoire.


== In Ireland ==
In 1826, he went to Dublin for two years, where he wooed the audiences nightly, and filled the Theatre to overflowing. While in Ireland, he also wooed and married Jane Daly, in Cork in 1828. They would go on to have 7 children, including actresses Fanny and Jane.


== At Covent Garden ==
Upon his return to England, he was immediately offered an engagement by Charles Kemble, who was then at Covent Garden. He chose to accept the engagement, and after the failure of the management, accepted the proposals first of Mr. Macready, and then of Mr. Phelps, performing his usual roles.


== At Sadler's Wells ==
Mr. Bennett was next invited by Samuel Phelps to act for the "prettiest and most renowned of London Suburban Theatres, Sadler’s Wells,” where he was soon as big a favourite with the audiences as he had been on the more legitimate London stages. As a result of the very good and well-deserved reputation of Mrs Warner and that of Phelps, Sadler's Wells had become a fashionable theatre. At Sadler's Wells, Bennett played Cassius in “Julius Caesar”, Ludovico in ”Evadne,” Angus in “Feudal Times,” Felton in “ Saville of Haystead,” and Bossola, in ” The Duchess of Malfi,” where "his solemn intonation and high conception of the character created a furore in the scene with Miss Glyn". He played with great success in “King and No King,” Bessus, Captain Poop in “The Honest Man’s Fortune,” and Caliban, in “The Tempest.”


== As Caliban ==
It was as Caliban that perhaps Mr. Bennett showed his true ability to conceive a role. In this part, as in the many other parts he played, he showed that he was an essentially poetic actor. Indeed, as a "Dramatic poet of no ordinary merit", he was well qualified for the role.
Trevor R Griffiths, in his article ('This Island's mine': Caliban and Colonialism(1983)) said that the importance of Bennett's interpretation was recognised by P MacDonnell, on two accounts: i) through engagements to repeat the part for Phelps in 1847 and 1849, and at the Surrey in 1853, and ii) through universal praise, well encapsulated in the Era's response to the Surrey Revival: 'Even Caliban,with all his grossness and hideous deformity, is a poetical character, and Mr George Bennett . . . gave to it great breadth and vigour, without a particle of vulgarity' (9 October).
Griffiths attributes

"Part of Bennett's success [to] his close attention to the text, which is exemplified in his adopting long nails and high foreheads in response to Caliban's offer to dig for pignuts with his long nails and his fear of being turned into a low-browed ape. [...] Bennett's attention to detail and presentation of much of Caliban's complexity was enough to move MacDonnell to declare that his performance was an example, like Macklin's Shylock, of how 'some of the characters drawn by Shakspere, were never altogether understood, till the excellence of the histrionic art developed them' and to express a truly Romantic 'degree of pity for the poor, abject, and degraded slave'. He believed that Prospero was partly to blame for Caliban's behaviour, since he imprudently placed 'this wild and untutored creature' in a position which made his rape attempt more feasible. Furthermore he argued that Caliban 'amidst the rudeness of his nature and possessing an exterior ugly and misshapen . . . stimulated to revenge, by the severity he suffers . . . has withal, qualities of a redeeming nature'. From this perception it was but a small step for MacDonnell to make a link with a moral obligation to civilize the natives: Bennett delineated 'the rude and uncultivated savage in a style, which arouses our sympathies in behalf of those, whose destiny, it has never been, to enjoy the advantages of ~ivilisation'.H~e re, then, we have, in MacDonnell's response to Bennett's subtlety, the germ of an idea which was to grow in importance under the stimulus of the popularity of Darwinian and Imperialist theories. Although the readoption of Shakespeare's text and Bennett's sensitivity to Caliban's complexity led the way for a gradual displacement of the traditional comic wild man associated with the Dryden-Davenant versions, progress was by no means regular, and there were many simplistic interpretations after Bennett's breakthrough."

Virginia Mason Vaughan wrote

In the privacy of their studies, Coleridge and Hazlitt were free to respond to Shakespeare's original text. Yet on stage, both at Drury Lane and at Covent Garden, the Kemble-Dryden-Davenant version persisted. William Charles Macready played Prospero in 1821, 1824, and again in 1833, but he did so unhappily. Later he described the acting version he was forced to use as a "melange that was called Shakespeare's Tempest, with songs interpolated by Reynolds among the mutilations and barbarous ingraftings of Dryden and Davenant." Macready found the performances tedious and lamented that his role was a "stupid old proser of commonplace which the acted piece calls Prospero." It is not surprising then that when in 1838 Macready revived the Tempest as Shakespeare had originally conceived it, the new production confirmed the romantic critics' more sympathetic conceptions of Caliban.
Caliban was by then a more important character, played by George Bennett, an actor who excelled in tragic as well as comic roles. Besides Caliban, he was remembered for performances of Sir Toby Belch, Pistol, Enobarbus, Bosola, and Apemantus. Bennett's performance inspired at least one member of the audience to see Caliban in a fresh light. Bennett, argued Patrick MacDonnell, delineated "the rude and uncultivated savage, in a style, which arouses our sympathies." ... Bennett began the stage tradition of lunging at Prospero during the opening confrontation, then recoiling from a wave of the magic wand, and finally writhing in impotent fury. Here the modern Caliban, victim of oppression, was born.


== Bennett the writer ==
His own play, Justiza, was first accepted by Webster, then the lessee of the Haymarket Theatre but, owing to a combination of circumstances, was not produced there. It was finally brought out by Charlotte Cushman, at Birmingham. Bennett's second play, Retribution, was founded on perhaps the worst of Sir Walter Scott's poems, "Rokeby". Both as author and actor Mr. Bennett "deserves our warmest mede of approbation". "His careful study, poetic conception of character, and faithful delineation, alike combine to elevate him to a distinguished niche in the dramatic shrine."
He has also written a number of other works, including The Pedestrian's Guide through North Wales and The Albanians.


== Funny mishap with Die-again Macready ==
Plighted troth: a Dramatic Tale by CF Darley, a very long, Elizabethan-style drama on a religious controversy (Catholic versus Protestant), was Macready's unluckiest misjudgment. Believing the gloomy, turgid affair to be a work of 'fine thoughts expressed in massive language', he had approached it with confidence. But it lasted only a single night. Darley had called his chief character Gabriel Grimwood: Eliza Grimwood happened to be the name of a girl brutally murdered in Waterloo Bridge Road, and the gallery fastened on this. 'Look here, old bloke,' somebody shouted, 'who cut Eliza's throat?' The louder the gallery laughed, the colder Macready's acting became. During the first scene, George Bennett sprained his ankle, and whenever he limped on, the pit told him to "hook it, to the hospital!" At the end of the play, after Elton had stabbed Macready with a table-knife and the dead man had fallen beneath the table, Bennett - emerging from a bed and limping down to look at the body - trod heavily on Macready's hand. 'Beast!' cried the tragedian. 'Beast of Hell!' He lay down angrily, and died all over again, and at length the curtain fell to hysterical laughter.


== Bennett's retirement ==
Bennett left the stage in 1862, and took up photography in Chepstow.
Fellow actor, Frederick Robinson said of Bennett’s retirement: “This was George Bennett’s last season at Sadler’s Wells and his retirement made a great gap in the company which was never filled. He played all the principal heavy parts, in many of which he had no rival. His Caliban and his Bessus in Beaumont and Fletcher’s “a King or No King” were great performances, as were also his Pistol in Henry V and his Stout in “Money”. I have never seen his performance of these parts approached, and I have seen some very good actors play them.” New York Times
For the times he lived to a very good age, and died aged 79 on 21 September 1879 of "senile decay and exhaustion" (as per his death certificate)


== Bennett's Acting Daughters ==
Miss Fanny Bennett (Sadler's Wells, Theatre Royals Edinburgh, Bristol and Bath), who married Charles Thomas Burleigh (real name: Thomas Burrows Kirk), Stage Manager - Old Vic, London Theatre manager (Holborn Theatre), actor and contemporary of Squire Bancroft and Sir Henry Irving
Miss Jane Bennett
Popular misconception in literature and newspapers of his day, and notably the Dictionary of National Biography (DNB) that Miss Julia Bennett, who later became Mrs Julia Bennett Barrow, and acted firstly at the Hay Market, before moving on to the American stage, was his daughter. In fact, her father was William Bennett Esq.


== Notes ==


== References ==
THE PLAYERS, A Dramatic, Musical and Literary Journal, VOL. IV—No. 82., Saturday, 20 July 1861, copy kept at National Portrait Gallery, Heinz Gallery, London
Theatrical Times, Vol 1, June to December, 1846, p242
Parish records for England and Wales; 1841, 1851 UK censuses and Miss Julia Bennett's marriage certificate to Jacob Barrow in 1840 (NB: Jacob Barrow son of Simon Barrow of Bath was listed on the 1841 census at Walcot Bath living with his father, other records including 1848 newspaper reports show that Jacob Barrow married Julia Bennett on the 2nd Sep 1848 at St. James's Church Piccadilly).
Backstage database (lead universities: University of Kent, Canterbury and University of Bristol)
Trevor R Griffiths, 'This Island's mine': Caliban and Colonialism, The Yearbook of English Studies, Vol. 13, Colonial and Imperial Themes Special Number. (1983)
Virginia Mason Vaughan; "Something Rich and Strange": Caliban's Theatrical Metamorphoses; Shakespeare Quarterly; Vol. 36, No. 4. (Winter, 1985), pp. 390–405.
JC Trewin, The journal of William Charles Macready


== Further reading ==
Samuel Phelps and Sadler's Wells Theatre Shirley S. Allen (Wesleyan, 1971) ISBN 0-8195-4029-3