Friedrich-Wilhelm von Chappuis (13 September 1886 – 27 August 1942) was a General der Infanterie who commanded the XXXVIII. corps during World War II. He was also a recipient of the Knight's Cross of the Iron Cross. The Knight's Cross of the Iron Cross was awarded to recognise extreme battlefield bravery or successful military leadership. Friedrich-Wilhelm von Chappuis committed suicide on 27 August 1942.


== Awards and decorations ==
Iron Cross (1914)
2nd Class
1st Class

Wound Badge (1914)
in Black

Knight's Cross of the Royal House Order of Hohenzollern with Swords
Honour Cross of the World War 1914/1918
Iron Cross (1939)
2nd Class (22 September 1939)
1st Class (4 October 1939)

Eastern Front Medal
Knight's Cross of the Iron Cross on 15 August 1940 as Generalleutnant and commander of 15. Infanterie Division 
Knight of Honour of the Order of St. John


== References ==


=== Citations ===


=== Bibliography ===


== External links ==
Lexikon der Wehrmacht
World War 2 Awards.com
Friedrich-Wilhelm von Chappuis @ Axis Biographical Research at the Wayback Machine (archived November 16, 2010)