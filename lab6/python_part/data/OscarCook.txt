Richard Martin Oscar Cook, known as Oscar Cook (March 17, 1888--February 23, 1952), was a British author of novels, non-fiction works and short stories with a supernatural theme.


== Life ==
Cook worked as a Government official in British North Borneo from 1911 until 1919. In December 1914 he was Assistant District Officer at Semporna, and it was at this time that he compiled a vocabulary of Bajau words. He later held District Officer posts.
On returning to Britain, he wrote an autobiographical account of his time in Borneo. It was suggested that he should approach the Curtis Brown literary agency and the book was allocated to Christine Campbell Thomson, an agent there. It was she who gave the book its title, Borneo: Stealer Of Hearts and placed with Hurst & Blackett who published it in 1924. At the time it was considered one of the most authoritative books on Borneo. At the same time, Cook was writing short stories concerned with supernatural themes, several of which were directly influenced by his time in Borneo. By 1934 an autobiographical note accompanying one of his short stories stated that since his return to England he had been an author, editor, publisher, actor, secretary to a dramatic school, and in business.
Cook married Christine Campbell Thomson and they were divorced in 1938. Cook died on the twenty-third of February, 1952.


== Publications ==


=== Non fiction ===
Borneo: the Stealer of Hearts Hurst and Blackett London 1924


=== Novels ===
The Second Wave 1930 or earlier, translated into the Dutch as Gij zult niet (set on a rubber estate near Mount Kinabalu, North Borneo, with a theme of adultery)


=== Short stories ===
Golden Lilies (Hutchinson's Adventure-Story Magazine, September 1922; Keep On The Light, 1933; More Not At Night, ed. Christine Campbell Thompson, Arrow 1961, 1963)
Si Urag of the Tail (Hutchinson's Adventure-Story Magazine, January 1923; Weird Tales, July 1926; You'll Need A Night Light, ed. Christine Campbell Thompson Selwyn & Blount September 1927; A Century Of Creepy Stories, Hutchinson 1934; 50 Strangest Stories Ever Told, Odhams, 1937; Still Not At Night, Arrow 1962, Creepy Stories Bracken 1994)
On the Highway (Weird Tales, January 1925)
The Great White Fear (Hutchinson's Adventure-Story Magazine, June 1925; Grim Death Selwyn & Blount, 1932; A Century of Creepy Stories Hutchinson, 1934; Creepy Stories Bracken, 1994)
The Creature of Man (Weird Tales, November 1926)
The Sacred Jars (Weird Tales, March 1927. Reprinted as When Glister Walks in Gruesome Cargoes, Selwyn and Blount July 1928; A Century Of Creepy Stories, Hutchinson 1934; 50 Strangest Stories Ever Told, Odhams, 1937; Not At Night: Tales That Freeze The Blood, Arrow, 1960, 1962; Creepy Stories Bracken, 1994)
Piecemeal (By Daylight Only, October, 1929; Weird Tales, Feb 1930; Not At Night Omnibus, 1937; The Second Pan Book of Horror Stories, Pan 1960)
Boomerang (Switch on the Light Selwyn & Blount, 1931; A Century of Creepy Stories, Hutchinson, 1934; The Second Pan Book of Horror Stories, Pan, 1960; Creepy Stories, Bracken, 1994. Dramatised by Rod Serling as the Night Gallery television series episode "The Caterpillar", first broadcast 1 March 1972)
His Beautiful Hands (At Dead of Night Selwyn & Blount November 1931; Not At Night Omnibus, Selwyn and Blount 1937; The Pan Book of Horror Stories, ed Herbert Van Thal, 1959)
Dog Death (Terror By Night, 1934)
The Crimson Head-Dress (Nightmare By Daylight, 1936)


== External links ==
Cambridge University Library Royal Commonwealth Society holding - Cook's Bajau vocabulary
Discussion page about Cook's short stories.
Oscar Cook (1924, Reprint 2007) Borneo: The Stealer of Hearts