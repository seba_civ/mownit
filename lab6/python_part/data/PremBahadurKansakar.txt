Prem Bahadur Kansakar (Devanagari: प्रेम बहादुर कंसकार) (1918–1991) was a Nepalese fighter for democracy and linguistic rights, author and scholar of Nepal Bhasa. He devoted himself to serving his mother tongue and collecting and preserving ancient manuscripts, and was the founder of Asa Archives, the only public archives in Nepal.


== Early life ==
Kansakar (alternative name: Prem Bahadur Kasāh; Devanagari: प्रेम बहादुर कसाः) was born in Kathmandu to a family of merchants. He received his primary education in Kathmandu at Jagat Lal Master's school, Masjid Imambara (where he learned Urdu and Persian) and Durbar High School. After passing Class 8 from Durbar High, he went to Patna, India to join Class 9. In 1940, he passed the Matric exams and enrolled in Patna College.


== Political career ==
In Kathmandu during his college holidays, he came into contact with Ganga Lal Shrestha through Dharma Ratna Yami, and was greatly impressed by the young revolutionary. Kansakar went back to Patna to continue his studies, and in 1941 was informed by Yami that Ganga Lal had been martyred. In 1942, moved by Ganga Lal's death, Kansakar became a member of the Radical Democratic Party founded by Indian nationalist M. N. Roy.
Returning to Kathmandu, he got involved in the underground struggle against the autocratic Rana regime to bring democracy in Nepal. In 1944, he formed the Nepal Democratic Association (नेपाल प्रजातन्त्र संघ) which organized a civil disobedience movement across the Kathmandu Valley in 1948.
Kansakar also worked to develop education, and helped to establish a library Pradipta Pustakalaya in 1946, Shanti Nikunja School and Padmodaya School, where he also taught.
He left his teaching job to travel to Kolkata, India to join other Nepalese democracy fighters, but was stopped on the way and confined to Kathmandu. In 1947, he slipped out of the country and went to Varanasi to attend a convention of the Nepal Rastriya Congress, a political party of Nepalese freedom fighters.
In 1948 in Kolkata, Mahendra Bikram Shah and Kansakar established the Nepali Democratic Congress as president and general secretary respectively. In 1950, the party merged with the Nepali Rastriya Congress founded by Bishweshwar Prasad Koirala to become the Nepali Congress which decided to launch an armed struggle against the Ranas.
In 1951, the Rana regime was overthrown and democracy established in Nepal. Kansakar became the leader of a new political party, Janavadi Prajatantra Sangh ("People's Democratic Union"), which was formed subsequently.
The Ranas ruled Nepal from 1846 until 1951. During this time, the Shah king was reduced to a figurehead and the prime minister and other government positions were hereditary. Jang Bahadur Rana established the Rana dynasty in 1846 by masterminding the Kot massacre in which about 40 members of the nobility including the prime minister and a relative of the king were murdered. Tyranny, debauchery, economic exploitation and religious persecution characterized Rana rule.


== Language activist ==

In 1950, Kansakar and a fellow writer Madan Lochan Singh established a literary society named Chwasa Pasa (meaning "Pen Friend") in Kolkata. Its objective was to bring together Nepal Bhasa writers living in exile in India and bring out publications in the language as it was forbidden to do so in Nepal.
In 1951, after the Rana dynasty was overthrown and democracy established in Nepal, the ban on publishing in Nepal Bhasa ended and Kansakar returned to Kathmandu. Chwasa Pasa also moved to Kathmandu and devoted itself to publishing books and magazines in Nepal Bhasa.
In 1956, Kansakar helped to set up Nasa Khala, a cultural organization. Its objective was to research, present and preserve Nepal's classical and folk dance and music. Nasa Khala provided training in dance and music and performed stage shows.
He was also the founder of the Nepal Bhasa Dictionary Committee which started worked on a dictionary of the classical language in 1980. The dictionary was published in 2000, and the committee received the Nikkei Asia Prize in 2001.
In 1987, Asa Archives was established. Named after Kansakar's father Asha Man Singh Kansakar, it preserves ancient manuscripts, palm leaf documents and books collected from private sources. Kansakar donated his personal collection to the archives to which donations from other people were later added. Most of the collections are in Nepal Bhasa and Sanskrit languages.


== Publications ==
Kansakar was also a story writer and essayist. He has been credited with bringing a transformation in Nepal Bhasa essays. Nhupukhu (a collection of essays) and Swanma (children's stories) are two of his notable works.
He collected old songs and ballads and has published Matenaya mye ("Love songs") and Bakham-mye ("Narrative Poems").
Kansakar was the chief editor of Situ (Devanagari: सितु) (meaning "Holy Grass"), a bimonthly literary magazine in Nepal Bhasa. It was published by Chwasa Pasa and was in publication from 1964-1991.


== References ==


== External links ==
Asa Archives