Thomas de Grey, 6th Baron Walsingham (29 July 1843 – 3 December 1919) was an English politician and amateur entomologist.


== Biography ==

Walsingham was the son of Thomas de Grey, 5th Baron Walsingham, and Augusta-Louisa, daughter of Sir Robert Frankland-Russell, 7th Baronet. He was born on Stanhope Street in Mayfair, the family's London house. He was educated at Eton and Trinity College, Cambridge. He sat as Conservative Member of Parliament for West Norfolk from 1865 until 1870, when he succeeded to the title and estates of his father, and entered the House of Lords. From 1874 to 1875 he served as a Lord-in-Waiting (government whip) in the second Conservative government of Benjamin Disraeli. From 1870 on he also ran the family's estate at Merton, Norfolk, served as trustee of the British Museum and performed many other public functions.
Walsingham was a keen lepidopterist, collecting butterflies and moths from a young age, and being particularly interested in Microlepidoptera. His collection was one of the most important ever made, which after his purchase of the Zeller, Hofmann and Christoph collections contained over 260,000 specimens. He donated it to the Natural History Museum, along with his library of 2,600 books.
Walsingham was elected a fellow of the Royal Society in 1887, and was a member of the Entomological Society of London, serving as President on two occasions. He married three times, but left no heir, and was succeeded as Baron by his half-brother. He married his third wife, Agnes Dawson, in 1914. Her daughter was Margaret Damer Dawson.
On 30 August 1888, Lord Walsingham had a remarkable day shooting on Blubberhouses Moor, Yorkshire when he killed 1070 grouse. The day started at 05:12 with the first of twenty drives, assisted by two teams of forty beaters, two loaders and three guns. During the sixteenth drive he shot 94 grouse in 21 minutes; a killing rate of one every13 seconds. The last drive finished at 18:45 and his Lordship managed to shoot fourteen on the walk home!


== Cricket ==
Walsingham was a first-class cricketer from 1862 to 1866. Recorded on scorecards as T de Grey, he played in 15 matches, totalling 380 runs with a highest score of 62 and holding 9 catches. He was mainly associated with Marylebone Cricket Club (MCC) and Cambridge University, also representing the Gentlemen in a Gentlemen v Players match in 1863.


== References ==

Bibliography
Salmon, Michael A. The Aurelian Legacy. ISBN 0-946589-40-2. 


== External links ==
Hansard 1803–2005: contributions in Parliament by Thomas de Grey
http://www.leighrayment.com
http://www.leighrayment.com/commons.htm