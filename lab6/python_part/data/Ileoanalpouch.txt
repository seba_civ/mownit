In medicine, the ileal pouch-anal anastomosis (IPAA), also known as an ileo-anal pouch, restorative proctocolectomy, ileal-anal pullthrough, or sometimes referred to as a j-pouch, s-pouch, w-pouch or an internal pouch (or Kock pouch), is a surgically constructed internal reservoir; usually situated near where the rectum would normally be. It is formed by folding loops of small intestine (the ileum) back on themselves and stitching or stapling them together. The internal walls are then removed thus forming a reservoir. The reservoir is then stitched or stapled into the perineum where the rectum was. The procedure retains or restores functionality of the anus with stools passed under voluntary control of the patient.


== Reasons for pouch construction ==
Ileo-anal pouches are constructed for people who have had their large intestine surgically removed due to disease or injury. Diseases and conditions of the large intestine which may require surgical removal include:
Ulcerative colitis
Crohn's disease
Familial adenomatous polyposis
Colon cancer
Toxic megacolon
There is debate about whether patients suffering from Crohn's disease are suitable candidates for an ileo-anal pouch due to the risk of the disease occurring in the pouch, which could make matters even worse. An alternative to an ileo-anal pouch is an ileostomy.
Ulcerative colitis is cured if the large intestine is removed and the patient has the pouch created, but Crohn's disease often inflames the pouch and the adjacent small intestine after this surgery is performed. When this happens, it can result in the return of symptoms and dysfunction of the pouch.
In some cases where the pouch was formed to manage colitis, inflammation can return to the pouch in a similar way to the original inflammation in the colon. This is known as pouchitis.


== History ==
The surgical procedure for forming an ileo-anal pouch (jpouch) was pioneered by Sir Alan Parks at St. Mark's Hospital in London in the early 1980s. The pouch was also known as a Parks' Pouch but more commonly is referred to as a 'Jpouch'. The Jpouch was developed as an alternative to the ileostomy. An ileostomy is where, in the absence of the colon, the small intestine is brought through the abdomen and sewn down forming an opening called a stoma. The stomal opening is where incontinent intestinal waste is emptied into a bag worn by the patient.


== Surgical procedure ==
In this surgical procedure the ileum is attached to the anus after the rectum has been removed.
In a J-pouch anastomosis, a 12-inch section of the small intestine is formed into a J-shaped pouch in order to replace the function of the rectum and store stool until it can be eliminated. This procedure is similar to the side-to-end coloanal anastomosis but a larger pouch is formed.
In a Side-to-end coloanal anastomosis a side of the colon is attached to the anus after the rectum has been removed. A section of the colon about 2 inches long is formed into a mini-pouch in order to replace the function of the rectum and store stool until it can be eliminated. This procedure is similar to the J-pouch coloanal anastomosis but a much smaller pouch is formed.
The entire procedure can be performed in one operation, but is usually split into two or three. When done as a two-step, the first operation (step one) involves a proctocolectomy (removal of the large intestine and rectum), and fashioning of the pouch. The patient is given a temporary defunctioning ileostomy (also known as a "loop ileostomy"). After a period of usually 6–12 weeks the second step (sometimes called the "takedown") is performed, in which the ileostomy is reversed. The reason for the temporary ileostomy is to allow the newly constructed pouch to fully heal without waste passing through it, thus avoiding infection.
Some surgeons prefer to perform a subtotal colectomy (removing all the colon except the rectum), since removal of the rectum can lead to complications with the anal sphincters. When a colectomy is performed as an emergency (which can arise from toxic megacolon and other complications), or when the patient is extremely ill, the colectomy and pouch construction are performed in separate stages, resulting in a three-part surgery.


== Pouch behavior ==


=== Bowel motions ===
Immediately after the surgery is complete, the patient tends to pass liquid stool with frequent urgency, and he or she may have 8 to 15 bowel movements per day, but this eventually decreases with time. Because the ileo-anal pouch is a considerably smaller reservoir than the colon, patients tend to have more frequent bowel motions; typically 6-8 times a day. Also because the ileum does not absorb as much water as the colon, the stools tend to be less formed, and sometimes fluid. The normal pouch output is described to be of a consistency similar to porridge. People who find that the consistency remains loose or who experience bowel movements too frequently usually take loperamide or codeine phosphate to thicken the stool and slow the bowel movement.
Very rarely, patients have reported constipation with the pouch; laxatives are successful for encouraging bowel movement in this case.
Because the ileum does not absorb as much of the gastric acid produced by the stomach as the colon did, pouch output also tends to burn the anal region slightly, and many patients find it helpful to wash the area regularly, sometimes using protective barrier cream.


=== Diet ===
Because more water is lost through pouch output, patients can get dehydrated easily and can also suffer salt deficiency. For this reason, some are encouraged to add extra salt to meals. Persistent dehydration is often supplemented with an electrolyte mix drink.
Many patients choose to eat more white carbohydrates, because this thickens the pouch output and reduces the risk of dehydration or the aforementioned burning of the anal region. It is also common among pouch-owners to eat little and often, or "graze", rather than having three large meals a day. Some patients avoid eating much after 6-7pm to avoid having to get up during the night.
Immediately after surgery, patients are encouraged to eat low fiber, high protein/carbohydrate meals, but after the pouch function has settled, most are able to reintroduce a fully varied diet. There are some foods that are known to irritate the pouch, however, and though they may be introduced carefully, are best avoided immediately following surgery.
Increased stool output can be caused by fibrous foods (such as pulses, green leaves, raw vegetables etc.) and also by spicy foods, alcohol and caffeine.
Anal irritation can be caused by nuts, seeds, citric acid, raw fruit and spicy food.
Increased gas can be caused by fizzy drinks, milk, beer, broccoli, cauliflower, sprouts, cabbage etc.
Increased odor can be caused by foods such as fish, onions, garlic and eggs.


=== Diseases/Disorders of the ileal anal pouch ===
surgery related/mechanical complications (examples: fistulas, strictures)
inflammatory or infections disorders (examples: pouchitis, cuffitis)
functional disorders (examples: irritable pouch syndrome, pelvic floor dysfunction)
dysplasia or neoplasia (examples: adenomas , cancers)
systemic or metabolic disorders (examples: malnutrition, anemia)


== Pouchitis ==

Pouchitis is an inflammation of the ileo-anal pouch, which occurs particularly in cases where the pouch has been created to manage colitis. The symptoms are normally somewhat similar but less acute than those of colitis, and include (sometimes bloody) diarrhea, urgency or difficulty in passing stools, and, in few cases, pain. The standard treatment for pouchitis is a 7 to 10 day course of a combination ciprofloxacin and metronidazole.


== See also ==
Ileostomy


== References ==

Notes
B.B. McGuire, A.E. Brannigan: Ileal pouch–anal anastomosis. In: The British Journal of Surgery. Band 94, Nr. 7, 2007, ISSN 0007-1323, S. 812-823 (PDF-file; 256 KB)
S.P. Bach, N.J.M. Mortensen: Revolution and Evolution: 30 Years of Ileoanal Pouch Surgery. In: Inflammatory Bowel Diseases. Band 12, Nr. 2, 2006, ISSN 1078-0998, S. 131-145 (PDF-file; 240 KB)
Patient Information Leaflet, St. Mark’s Hospital, Middlesex, UK


== External links ==
Red Lion Group
The J-Pouch Group
UC Story to Jpouch Life Support Pages
IA - The Ileostomy and Internal Pouch Support Group
IPAA Procedure