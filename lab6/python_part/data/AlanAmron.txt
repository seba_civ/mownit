Alan Amron (born November 20, 1948) is an American inventor who holds 39 United States patents.


== Inventions ==
Noteworthy Amron inventions include:
The Photo Wallet, invented by Amron for his company VideoChip Technologies, was the first handheld battery-operated digital photo frame. It could display JPG and MPG files, and read Microsoft Excel and Microsoft Word documents. The product was licensed to Nikon.
Amron invented the first battery-operated water gun.
Amron has invented and designed a "First Down Laser Line" system, which would extend the concept of the computer-generated first down yellow line seen on-screen during televised football games by projecting such a line on the physical field at the stadium. Amron met with the NFL in 2003 and again in 2009, and in 2013 a league spokesman said "We have not been convinced that it would work for us, but we are open to further discussion after the season." A similar Leading Mark Laser invented by Amron was used in the 2013 and 2014 NCAA National track and field championships at the University of Oregon.


== References ==