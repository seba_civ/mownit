Little Mac Ski Hill, known as Little Mac, is a community-operated ski area adjacent to Mackenzie, British Columbia, Canada in the northern Rocky Mountain Trench. The area has one tow lift and vertical differential of 90 m (295 ft). The longest run is 210 m (689 ft). In addition to downhill skiing, the hill also has areas for tobogganing and snowboarding.
Little Mac Ski Hill is funded by the District of Mackenzie and is maintained through the municipality of Mackenzie. All matters regarding Little Mac Ski Hill is handled by public works in Mackenzie. Little Mac is one of the main tourist attractions to Mackenzie, British Columbia, even though the hill itself is very small, the close proximity to the town of Mackenzie makes it a hit for families and children of all ages.
It was announced on July 25, 2008 that the ski area would receive $160,000 from the Towns for Tomorrow program of the provincial government to rebuild the Little Mac Ski Chalet. Though not directly part of the ski area, there are 32 km (20 mi) of groomed and lit cross-country ski trails immediately accessible from the town of Mackenzie, with three specially built warming huts.


== References ==
Northern BC Ski Hills & Resorts, WorldWeb.com site
Pat Bell, MLA, website
MackenzieBC.com website
Mackenzie page at HelloBC.com (BC Tourism)
District of Mackenzie website