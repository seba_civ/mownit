Best Western International, Inc., operator of the Best Western Hotels & Resorts brand, operates about 4,100 hotels. The chain, with its corporate headquarters in Phoenix, Arizona, operates 2,163 hotels in North America. Founded by M.K. Geurtin in 1946, the brand currently operates around 2,200 hotels in North America. David Kong is the president and CEO of Best Western, and Dorothy Dowling is the chief marketing officer.
In 1964, Best Western took the first step towards global expansion when Canadian hotel owners joined the system. Best Western then expanded to Mexico, Australia, and New Zealand in 1976.
In 2002, Best Western International launched Best Western Premier in Europe and Asia. (The other hotels in the chain were known as Best Western.) In 2011, the chain's branding system-wide changed to a three-tiered system: Best Western, Best Western Plus, and Best Western Premier. Since it no longer operates under a single brand, Best Western concurrently modified its slogan in 2011 from "the world's largest hotel chain" to "the world's largest hotel family."


== History ==

Best Western began in the years following World War II. At the time, most hotels were either large urban properties or smaller family owned roadside hotels. In California, a network of independent hotel operators began making referrals of each other to travelers. This small and informal network eventually grew into the modern Best Western hotel chain founded by M.K. Guertin in 1946.

The name "Best Western" was a result of most of their properties originally being in the United States west of the Mississippi River. From 1946 to 1964, Best Western had a marketing partnership with Quality Courts, the forerunner of the chain known today as Quality Inns, whose properties were located mostly east of the Mississippi River, not in direct competition with Best Western. This partnership made sense geographically, but was not successful in the long run, and was eventually abandoned. In 1964, Best Western launched an expansion effort of its own operations east of the Mississippi using the moniker "Best Eastern" for those properties with the same typestyle and Gold Crown logo as "Best Western." By 1967, the "Best Eastern" name was dropped and all motels from coast-to-coast got the "Best Western" name and Gold Crown, a move that would further enhance an already successful marketing brand into the "World's Largest Hotel Chain" by the 1970s.
Best Western's "Gold Crown" logo was introduced in 1964 and would continue with a few minor revisions over the next 32 years until it was replaced by the current blue and yellow logo in 1996. in 2015, Best Western introduced a new family of logos, replacing the core logo and adding signage for Best Western Plus and Best Western Premier.


=== Legal dispute ===
Best Western used to call itself a cooperative membership association, and as such could be seen as a co-op. Around 1985, it abandoned the "cooperative" terminology after courts insisted on calling it a franchisor despite its nonprofit status. The most dramatic example of this was Quist v. Best Western Int'l, Inc., 354 N.W.2d 656 (N.D. 1984), in which the North Dakota Supreme Court decided that Best Western was a franchisor and had to comply with the appropriate laws and regulations.


=== Best Western UK ===
Best Western UK began in 1978 when Interchange Hotels of the United Kingdom consisting of independent hoteliers from key locations in the UK elected to trade under the brand name Best Western United Kingdom, effectively an affiliate of Best Western International in the US. Now there are around 280 Best Western hotels within the United Kingdom.


=== Best Western Australia and New Zealand ===
In 1981, Homestead Motor Inns of Australia affiliated with Best Western. This move put 'International' after the Best Western name. The company has since been known as Best Western International.
In early 2007, Best Western Australia took over the rights to operate Best Western properties in New Zealand from the previous company, the Motel Federation of New Zealand. This was a bold but beneficial move for the brand as it made way for better quality properties to be brought into the brand. Currently, Best Western Australia has 205 properties in the group (11 in New Zealand and 194 in Australia).


=== Best Western Burma ===
As of 21 May 2013, Best Western International has announced that it will take over management of the Green Hill Hotel in Yangon, Myanmar. The acquisition will give the hotel Best Western's brand name and place presence in Burma as Best Western's first hotel establishment in the country.


== Business model ==
Best Western charges its franchisees a rate that is based on an initial cost plus a fee for each additional room. Best Western also publishes a list of standards that each hotel needs to maintain.
The hotels are allowed to keep their independent identity. Though they must use Best Western signage and identify themselves as a Best Western hotel, the hotels are allowed the option of using their own independent name as part of their identity (for example Best Western Adobe Inn, in Santa Rosa, New Mexico or the Best Western Berkshire Inn, in Bethel, Connecticut).
In the US, the properties can either be traditional roadside motels, motor inns, or full-service hotels. There are also many smaller "mini-chains" that are owned by the same management within Best Western; for example at one time the Best Western Midway Hotels found in the Midwestern United States. Outside the United States, the properties are mainly hotels. More than 90 percent of Best Western hotels in Europe have three or four-star ratings.
Best Western provides reservation and brand identity services for all of its worldwide hotels. It has multilingual reservation centers in Phoenix and Milan, Italy.
In the United Kingdom, an additional service fee is imposed on telephone bookings through the use of a premium rate telephone number.


=== Differentiation ===

In 2011, Best Western changed its branding chain-wide (after starting a similar idea in 2002 in some areas of Europe and Asia) with three levels of progressively more amenities and features: Best Western, Best Western Plus, and Best Western Premier. A Best Western can have the basics, e.g. a in-room coffeemaker, free local calls, available breakfast, cable television, and free WiFi, etc. A Best Western Plus would have those amenities of a Best Western plus a fitness room, swimming pool, business room, and onsite laundry room. A Best Western Premier would have all those amenities plus onsite dining, high-quality furnishings, premium towels and bath products, an onsite shop for snacks or essentials, meeting rooms, and an LCD television with HD channels.


== See also ==

List of hotels
List of motels


== References ==