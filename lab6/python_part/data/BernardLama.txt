Bernard Pascal Maurice Lama (French pronunciation: ​[bɛʁnaʁ laˈma]; born 7 April 1963) is a French football coach and former goalkeeper. He was born in the Indre-et-Loire département but grew up in French Guiana. He spent a number of years in goal for Lille and Paris Saint-Germain. He was also a member of the France national team that won the 1998 World Cup and the Euro 2000.


== Club career ==


=== Early years ===
Lama left Guiana in 1981 to come to metropolitan France, without his father's consent but determined to become a professional footballer, where he was first signed by Division 1 club Lille. He was loaned to Abbeville in 1982, where he failed to make a single appearance, and he returned to Lille after just one year, only to be loaned again to Besançon. He did get some play time that year, and his performances were convincing enough for Lille to agree to keep him for the following season. From then on, Lama played regularly, and even scored a goal (a penalty kick during the 8–0 win over Laval on 31 May 1989), but was never the indisputable number 1 keeper he aspired to be, and so he decided to leave. He spent one year in Metz, and then took part in Brest's last season in first division. Lama actually played every single league game during those two seasons. He then spent one year with RC Lens, where his performances attracted the attention of Paris Saint-Germain, who offered him a five-year contract.


=== The golden years ===
In Paris, Lama had to replace the iconic Joël Bats, and his impressive performances quickly showed he was up for the task. His career took a new dimension when he took part in European competitions, and even got called up for the French national team. He won the Coupe de France twice in 1993 and 1995, and in 1996, his most prestigious trophy yet, the European Cup Winners' Cup.


=== Downfall ===
Despite an interest from La Liga giants FC Barcelona, Lama decided to stay in Paris, and had an excellent start of season that was unfortunately cut short by a serious knee injury in September 1996 after he saved a penalty against Cannes, having not conceded a single goal so far that season. Shortly after his return, he received a two-month ban for consumption of cannabis in February 1997. This led Paris SG to look for a new goalkeeper, and Lama was notified during the summer 1997 (while he was still suspended) that Christophe Revault would be the starting goalkeeper for the following season and that Lama was free to look for a new destination.
Lama found himself without a club at the start of the 1997–98 season, training with the PSG reserve team. During the winter transfer window and only six months away from the World Cup, he finally agreed on a short term contract with Premier League club West Ham. Though signed on 21 December, he did not start in a match until 2 March against Arsenal, whereupon he kept a clean sheet. He would finish the season with twelve appearances in The Hammers' colours. His performances were good enough to interest West Ham in making him a permanent signing, but they were unable to retain the player after the World Cup and he re-signed with Paris Saint-Germain.


=== End of career ===
After six months spent in England, he made the most of the change of president in Paris (Charles Biétry replacing Michel Denisot) to return to the capital's club, where he spent two more seasons. Despite a more than decent 1999–00 season, he was notified that, due to Paris' youth policy, his services were no longer needed and that Lionel Letizi would replace him as the club's first choice goalkeeper. He quickly found a new club, Rennes, where he spent a very good season, at the end of which he expressed his desire to fulfill his childhood's dream and play for a Brazilian club. Unfortunately for him, no club expressed any interest in signing him, which led to his announcing the end of his career.


== International career ==
His debut with the France national team was on 17 February 1993 against Israel in a 4–0 victory, and he would go on to win 44 caps for his nation. Lama played in the Euro 1996 finals, and was a substitute at the Euro 2000 finals. He also won the 1998 FIFA World Cup as a substitute of Fabien Barthez but didn't play a single match.


== Coaching career ==
On 21 July 2006, Lama was appointed coach of the Kenya national team. Kenya, however, lost on his debut to Eritrea on 2 September 2006, during an African Nations Cup qualifier, and he quit just two months later, citing the lack of professionalism of the Kenya Football Federation, and was replaced by Tom Olaba.


== Honours ==
Paris Saint-Germain:
Division 1 (1994)
Coupe de France (1993, 1995)
Cup Winners Cup (1996)

France national team:
1998 FIFA World Cup
Euro 2000

Personal awards:
France Football's 1994 best French player award
He was made Chevalier (Knight) of the Légion d'honneur in 1998


== See also ==
List of sportspeople sanctioned for doping offences


== References ==


== External links ==
French Profile, stats and pictures of Bernard Lama