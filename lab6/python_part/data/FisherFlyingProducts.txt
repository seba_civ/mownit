Fisher Flying Products is a Canadian aircraft manufacturer that produces kits for a wide line of light aircraft. The company's kits all feature wooden construction with aircraft fabric covering. Many of the designs are reproductions of classic aircraft, such as the company's 80% Fisher R-80 Tiger Moth that is based upon the de Havilland Tiger Moth.


== History ==
Founded by Michael E. Fisher and Wayne Ison in the early 1980s as Lite Flite Inc, Aero Visions and later Fisher Aero Corporation, ownership of Fisher Flying Products was later passed to Gene and Darlene Jackson-Hanson in 1984. The company was originally based in South Webster, Ohio and later Edgeley, North Dakota, USA. In 2007 the Jackson-Hansons decided to sell the company and retire.
The company was purchased by Paul Riedlinger and moved to Woodbridge, Ontario, Canada. By early 2009 the new owner had re-established the manufacturing operation and commenced producing kits, starting with the Dakota Hawk and the FP-202. By late 2009 all kits were once again available.
The first two designs the company built were the Fisher Flyer, which incorporated a new fuselage and tail and the existing wings from the UFM Easy Riser hang glider and the Fisher Barnstormer, a negative stagger biplane. Plans and kits for the latter design were offered in the early 1980s.
On 15 January 2014 president Paul Riedlinger announced that the company was for sale. In md-2014 the company was sold to Dave Hertner owner of Effectus AeroProducts. He is planning to continue on producing Fisher Flying Products kits and providing support for the Fisher Flying Community.


== Aircraft ==

The company currently has 15 different designs available and had over 3,500 aircraft flying in 2007.


== References ==


== External links ==
Official website