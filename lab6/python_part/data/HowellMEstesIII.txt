General Howell M. Estes III (born December 16, 1941) served as commander in chief, North American Aerospace Defense Command (CINCNORAD), United States Space Command (USCINCSPACE), and commander, Air Force Space Command (COMAFSPC), headquartered at Peterson Air Force Base, Colorado. As commander, the general was responsible for the air sovereignty of the United States and Canada, providing tactical warning and attack assessment, directing space control and support operations, directing satellite control, warning, space launch and ballistic missile operations missions.


== Background ==
General Estes graduated from the United States Air Force Academy in 1965. He has commanded both operational and maintenance squadrons, the Air Force's only stealth fighter unit, an air division and numbered air force. Prior to his current position, he served as director for operations (J-3), the Joint Staff, the Pentagon, Washington, D.C. He also co-authored "Strategic and Doctrinal Implications of Deep Attack," the concept of operations for the Defense of Central Europe. A command pilot with more than 4,500 flying hours, he flew 169 combat missions as an F-4 pilot during the Vietnam War. During the Gulf War he served as deputy chief of staff for operations, Strategic Air Command. He retired on October 1, 1998.


== Education ==
1965 Bachelor of science degree in military science, United States Air Force Academy, Colorado Springs, Colorado
1975 Master of arts degree in public administration, Auburn University, Alabama
1975 Air Command and Staff College, Maxwell Air Force Base, Alabama
1975 National Military Commonwealth degree, Auburn University, Alabama
1983 National War College, Fort Lesley J. McNair, Washington, D.C.


== Assignments ==
June 1965 - November 1966, student, undergraduate pilot training program, Moody Air Force Base, Georgia
November 1966 - April 1967, student, F-4 qualification training, MacDill Air Force Base, Florida
April 1967 - May 1969, F-4D pilot, 335th Tactical Fighter Squadron, Seymour Johnson Air Force Base, North Carolina
May 1969 - May 1970, F-4E commander, 34th Tactical Fighter Squadron, Korat Royal Thai Air Force Base, Thailand
May 1970 - April 1972, F-4E pilot and chief of standardization and evaluation, 32nd Tactical Fighter Squadron, Camp New Amsterdam (now Soesterberg Air Base), Netherlands
May 1972 - July 1973, weapons tactics staff officer, Headquarters U.S. Air Forces in Europe, Lindsey Air Station, West Germany
July 1973 - August 1974, command briefing officer, Headquarters U.S. Air Forces in Europe, Ramstein Air Base, West Germany
August 1974 - August 1975, student, Air Command and Staff College, Maxwell Air Force Base, Alabama
August 1975 - January 1979, air operations staff officer, Europe-NATO Division, Office of the Deputy Chief of Staff for Plans and Operations, Headquarters U.S. Air Force, Washington, D.C.
February 1979 - August 1982, commander, 20th Tactical Fighter Training Squadron; then commander, 35th Equipment Maintenance Squadron; then assistant deputy commander and subsequently deputy commander for maintenance; 35th Tactical Fighter Wing, George Air Force Base, California
August 1982 - July 1983, student, National War College, Fort Lesley J. McNair, Washington, D.C.
July 1983 - June 1984, deputy assistant director for joint and National Security Council matters, directorate of plans, Office of the Deputy Chief of Staff for Plans and Operations, Headquarters U.S. Air Force, Washington, D.C.
June 1984 - January 1986, commander, 4450th Tactical Group, Nellis Air Force Base, Nevada
January 1986 - June 1987, special assistant to the chief of staff, Supreme Headquarters Allied Powers Europe, Brussels, Belgium
June 1987 - August 1988, commander, 14th Air Division, Strategic Air Command, Beale Air Force Base, California
August 1988 - July 1991, assistant deputy chief of staff for plans and programs; deputy chief of staff for plans and resources; then deputy chief of staff for operations, Headquarters Strategic Air Command, Offutt Air Force Base, Nebraska
July 1991 - August 1992, director of plans, deputy chief of staff for plans and operations, Headquarters U.S. Air Force, Washington, D.C.
August 1992 - October 1994, commander, 7th Air Force, Pacific Air Forces; deputy commander, U.S. Forces Korea; commander, Air Component Command, Republic of Korea and U.S. Combined Forces Command; commander, U.S. Air Forces Korea; and deputy commander in chief, United Nations Command, Osan Air Base, Republic of Korea
October 1994 - August 1996, director for operations (J-3), the Joint Staff, Washington, D.C.
August 1996 - 1998, commander in chief, North American Aerospace Defense Command and United States Space Command, and commander, Air Force Space Command, Peterson Air Force Base, Colorado


== Flight information ==
Rating: Command pilot
Flight hours: More than 4,500
Aircraft flown: A-7, F-4, F-16, F-117 and EC-135


== Awards and decorations ==
  Defense Distinguished Service Medal with two oak leaf cluster
  Air Force Distinguished Service Medal with oak leaf cluster
  Defense Superior Service Medal
  Legion of Merit
  Distinguished Flying Cross with oak leaf cluster
  Meritorious Service Medal with three oak leaf clusters
  Air Medal with 10 oak leaf clusters
  Air Force Commendation Medal with two oak leaf clusters


== References ==


== External links ==

U.S. Air Force bio