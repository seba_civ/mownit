"The Harvest" is the second episode of season one of the WB Television Network television series Buffy the Vampire Slayer. It was written by executive producer Joss Whedon and directed by John T. Kretchmer. The episode originally aired in feature-length format alongside part one, "Welcome to the Hellmouth", on March 10, 1997, and attracted 3.4 million viewers. As such, and even though it is typically shown as two separate episodes in reruns, the action continues directly from the cliffhanger ending of "Welcome to the Hellmouth", as Buffy Summers (Sarah Michelle Gellar) struggles to save Jesse McNally's (Eric Balfour) life.


== Plot ==
Luke (Brian Thompson) is about to finish off Buffy, who is trapped in a stone coffin, but she repels him with the silver cross that a mysterious stranger (David Boreanaz) had given her earlier that evening. She then escapes the mausoleum and saves her new friends Xander (Nicholas Brendon) and Willow (Alyson Hannigan) from vampires in the graveyard. However, Darla (Julie Benz) has already taken Jesse McNally (Eric Balfour), and she and Luke now inform The Master (Mark Metcalf) about Buffy's unusual fighting abilities and knowledge of the supernatural, and it is suspected that she may be a new Slayer. The Master decides to use Jesse as bait.
In the library, Giles (Anthony Stewart Head) and Buffy reveal to Xander and Willow the supernatural world of vampires, demons and Slayers, and that Buffy is the latest Slayer to endow powers chosen to fight supernatural threats. Willow accesses the city council's plans for Sunnydale's tunnel system, and Buffy realizes that there must be a means of access to it from the crypt where she fought Luke and Darla. Willow expresses her "need to help" now that she knows about the supernatural, and so continues assisting Giles with research. Xander, however, is hurt when Buffy declines his help in physically tracking down Luke.
Willow is researching the Master and his minions in the computer lab when she overhears Cordelia (Charisma Carpenter) badmouthing Buffy, and is then insulted by Cordelia when she attempts to defend her new friend. Willow exacts revenge by convincing the computer-illiterate Cordelia to press the DEL key to "deliver her assignment", thus deleting it from the system.
The dark stranger appears again just as Buffy is about to enter the tunnel system in the crypt, and tells her his name is Angel. He gives her directions to The Master's lair, but pointedly refrains from wishing her luck until she is out of earshot. Xander catches up with Buffy in the tunnels, having decided to follow her anyway despite her earlier attempts at discouraging him. They find Jesse seemingly alive and unhurt, but he leads them to a dead-end in the tunnel system and then reveals that he has already been turned into a vampire. Buffy and Xander barely manage to escape.
Underground, the Master is unhappy about their escape and punishes the vampire Colin by violently poking his eyes out. Luke then drinks the Master's blood, turning Luke into "the Vessel". Back at the library, Giles explains what he and Willow have discovered; that an ancient vampire, The Master, arrived in Sunnydale with his minions 60 years ago. He intended to open the Hellmouth, which is below Sunnydale - a portal between this reality and another, demonic reality - but he was swallowed by an unexpected earthquake and is now trapped in a church that is buried underground. If the Hellmouth opens, demons will invade the Earth. Tonight is a once-in-a-century opportunity called The Harvest in which, by choosing one of his minions to drink The Master's blood and marking him with the ritual's symbol, the vampire king will draw strength from each of this minion's victims, until he is powerful enough to finally break free from his confinement and resume what he has started decades ago. To prevent this, Buffy and her gang of friends must kill the Vessel. They wonder where the vampires might attack so as to ensure the optimum number of victims, and Xander suggests The Bronze, as he realizes that the vampires have been using it as their feeding ground.
On her way there with the others, Buffy stops by her house to pick up some additional weapons before the fight, but is promptly grounded by her mother (Kristine Sutherland), who has received a call from Principal Flutie (Ken Lerner) about Buffy skipping classes and is now terrified that the same events which led to Buffy being expelled from her previous school are happening again. Knowing that she cannot abide by her mother's wishes, Buffy collects her weapons from a secret compartment in a chest (the top layer of which is filled with stereotypical girlie items so as not to arouse suspicion) and climbs out of the second-floor window.
Luke and other vampires burst into The Bronze to begin feeding. Buffy arrives just in time to save Cordelia, who was newly attracted to Jesse because of the greater confidence he had gained as a vampire. Buffy notices the Vessel mark on Luke’s forehead, and begin to fight him. Xander confronts Jesse with a stake, but is spared the choice of either killing or being killed by his former best friend when a fleeing woman accidentally pushes him into Jesse so that the stake pierces the vampire's heart. Darla knocks Giles to the ground and is about to bite him, but Willow saves him by pouring holy water on Darla. Buffy smashes a window so that extra light pours into the room, and Luke is momentarily stunned because he thinks it is daylight, allowing Buffy to stake him and so prevent The Master from rising. Angel watches as the now-leaderless vampires flee from The Bronze; he is clearly impressed by Buffy's success.
The next morning, despite Xander's expectation, Cordelia exemplifies Sunnydale residents' denial towards the paranormal, and thus nothing apparently changes. Giles warns Buffy and her friends that the Hellmouth will continue to act as a magnet for demonic forces and that many more battles lie ahead, a fact which they accept more nonchalantly than he would like.


== Production and writing ==
Whedon hoped to include actor Eric Balfour in the title credits to shock viewers when his character dies. Unfortunately, the show could not afford the extra set of title credits at the time.


=== Writing ===
Although it is never revealed in the series, the Master's real name as listed in the script of this episode is Heinrich Joseph Nest.
Joss Whedon noted in the commentary for this episode that he felt horrible for giving Eric Balfour so many lines with "s" in them, which he had to recite while in "Vamp-face". He had such a difficult time getting the lines out around the prosthetics that new ones were designed for vampire characters required to speak frequently; the older style ones were given to vampire lackeys with few or no lines in future episodes.


=== Continuity ===


==== Arc significant ====
Jesse's death and transformation would result Xander to develop resentment towards vampires, which leading him to openly discriminate Buffy's subsequent vampire love interests Angel and Spike, and favors Riley Finn, Buffy's first human love interest.
The Scooby Gang is formed.


== Broadcast and reception ==
"The Harvest" originally aired in feature-length format alongside part one, "Welcome to the Hellmouth", on March 10, 1997 on The WB. The broadcast attracted 3.4 million viewers.
Noel Murray of The A.V. Club gave "The Harvest" a grade of B+, writing that it helped set the precedent for the series. However, he criticised some of the action scenes for a "lapse into mano-a-mano action-horror clichés" and felt that the resolution was anti-climactic. A review from the BBC stated that the episode was allowed "more time to breathe" than "Welcome to the Hellmouth", and praised the fight choreography and the developing characters.


== References ==


== External links ==
"The Harvest" at the Internet Movie Database
"The Harvest" at TV.com