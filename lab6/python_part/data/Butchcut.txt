A butch cut is a type of haircut in which the hair on the top of the head is cut short in every dimension. The top and the upper portion of the back and sides are cut the same length, generally between 1⁄4 and 3⁄4 inch, following the contour of the head.
A butch cut may also be called a short brush cut or brush cut. A butch cut that is cut at less than 1⁄4 inch on top may be referred to as a burr. Butch cuts are traditionally groomed with hair control wax, commonly referred to as butch wax.


== Styling ==

A variant form may have a slight graduation of the top hair longer from back to front or a quickly graduated bit of hair at the front hairline to achieve a little flip up of the hair at the forehead. The hair below the upper portion of the sides and back of the head is tapered short or semi-short with a clipper, in the same manner as a crew cut.


== See also ==
Buzz cut
Crew cut
Flattop
Ivy League
Regular haircut


== References ==


== Bibliography ==
Sherrow, Victoria (2006). Encyclopedia of hair. Greenwood Press. 
Thorpe, S.C. (1967). Practice and Science of Standard Barbering. Milady Publishing Corporation. 
Trusty, L. Sherman (1971). The Art and Science of Barbering. Wolfer Printing Co. 


== External links ==
Butch Cut High School Football Player, Lawrence, Kansas , 1960
Butch Cut High School Football Player, Lawrence, Kansas , 1960
Butch Cut High School Football Player, Lawrence, Kansas , 1960
Butch Cut Central High School Student, Little Rock, Arkansas, 1958
Butch Cut Track and Field Athlete, Summer Olympics, Rome, Italy, 1960