The 2005 Nicky Rackard Cup began on Saturday, 18 June 2005. 2005 was the first time the Nicky Rackard Cup was introduced into the All-Ireland Senior Hurling Championship. It was devised by the Hurling Development Committee to encourage some of the so-called "weaker" hurling counties and to give them the chance of playing more games. It is in effect a "Division 3" for hurling teams in Ireland. The final was played on Sunday, 21 August when London beat Louth in the final at Croke Park, Dublin.


== Format ==
Twelve teams participated in the "Nicky Rackard Cup 2005". The teams were divided into three groups of four based on geographical criteria. These groups were:
Group 3A: Sligo, Tyrone, Donegal and Fermanagh
Group 3B: Louth, Cavan, Armagh and Leitrim
Group 3C: London, Warwickshire, Longford and Monaghan


== Results ==


=== Group 3A Results ===


=== Group 3B Results ===


=== Group 3C Results ===


=== Knock Out Stage ===
The runners-up in groups 3B and 3C played each other with the winner playing the runner up in group 3A. The winner of that match joined the three group winners in the semi-finals.


== The Final ==
The 2005 Nicky Rackard Cup final was used as a curtain raiser for the semi-final of the 2005 Liam McCarthy Cup. London ranout winners on the day by a margin of 15 points. London Manager Mick O'Dea described it as 'the best day' of his life as his captain Meath native Fergus McMahon lifted the inaugural Nicky Rackard Cup after a 5-08 to 1-05 victory.
The Exiles, who staved off relegation from Division Two in 2005, powered out to their 15-point victory after teenager Ger Smith's 53rd-minute goal had reduced the deficit for Louth back to three points.
Two goals in the space of a minute from Barry Shortall and Kevin McMullan set London up for a 2-04 to 0-05 half-time lead. Although the Leinster men had dominated possession, their inability to take scores, which was surprising given the 12-61 tally accumulated from their previous four games, blighted their play.
London were similarly guilty, hitting eleven wides in the opening half to Louth's six. London's goals proved crucial. Shortall swept home a brilliantly delivered sideline cut from Brian Foley on 14 minutes, while seconds later, Antrim man McMullan pounced on a mistake by Louth defender Aidan Carter to bulge the net.
Ten scoreless minutes into the second half, Gary Fenton re-opened the scoring for a 2-05 to 0-05 London lead. Louth's Declan Byrne then pulled a goal chance into the side-netting, but the Reds deservedly found a way past Exiles 'keeper JJ Burke when teenager Smith scrambled home their only goal, and also what proved to be Louth's only score of the second half.
In slippery conditions, Division Three side Louth were always up against it and London cut loose in the closing quarter.
On 57 minutes, substitute Sean Quinn drove through and flicked a superb handpass for McMullan to fire home his second goal of a 2-01 haul. Four minutes later, Quinn kicked in London's fourth goal and the result was put beyond doubt when corner forward Dave Burke scored a fifth on 67 minutes. Burke clipped over a 65 and Gary Fenton added another point before the final whistle.
London: JJ Burke; E Phelan, T Simms, B Forde; J Dillon, F McMahon, B Foley 0-1; M Harding 0-01 (1f), M O'Meara; D Smyth, J Ryan, J McGaughan; D Bourke 1-04 (3f), B Shortall 1-00, K McMullan 2-01.
Subs: E Kinlon (for Smyth 36 mins), G Fenton 0-01 (for O'Meara 36 mins), S Quinn 1-00 (for Shortall 55 mins), P Doyle (for Phelan 68 mins), P Finneran (for McMullan 70 mins).
Louth: S Smith; D Black, A Carter, S Darcy; R Byrne, P Dunne, D Mulholland; D McCarthy, S Callan 0-02; T Hilliard, J Carter, D Byrne; G Smith 1-01 (1f), D Dunne 0-01, N McEneaney 0-01.
Subs: G Collins (for R Byrne h/t), S Byrne (for J Carter 53 mins), A Mynes (for McEneaney 65 mins), N Byrne (for Darcy 71 mins).
Referee: T Mahon (Fermanagh).