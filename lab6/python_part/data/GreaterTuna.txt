Greater Tuna is the first in a series of four comedic plays (followed by A Tuna Christmas, Red, White and Tuna and Tuna Does Vegas), each set in the fictional town of Tuna, Texas, the "third-smallest" town in the state. The series was written by Jaston Williams, Joe Sears, and Ed Howard. The plays are at once an affectionate comment on small-town, Southern life and attitudes but also a withering satire of same. Of the four plays, Greater Tuna is the darkest in tone.
The plays are notable in that two men play the entire cast of over twenty eccentric characters of both genders and various ages. Greater Tuna debuted in Austin, Texas, in the fall of 1981, and had its off-Broadway premiere in 1982. St. Vincent Summer Theatre produced the play in 2000, and No Name Players produced it in 2002.
Charles H. Duggan produced national tours of "Greater Tuna", "A Tuna Christmas" and "Red, White & Tuna" for twenty-six years. Williams and Sears regularly tour the country to perform all four plays, with Howard directing.
An HBO Special of Greater Tuna, produced by Embassy Television and Norman Lear, was aired in 1984. Due to modern videography techniques, the various characters portrayed by Sears and Williams were able to be seen en masse for the first time. However, due to copyright conflicts between Embassy Television and the original copyright claimants (Joe Sears, Jaston Williams & Ed Howard) the broadcast was limited to only 3 months in 1984. It has never been broadcast again. Rare VHS taped copies have been sold on eBay in excess of $100.
Sears and Williams did command performances of both Greater Tuna and A Tuna Christmas at the White House for President George H. W. Bush and Barbara Bush.
According to the play's official web site, by 1985, Greater Tuna was the most-produced play in the United States. A videotaped performance of Greater Tuna (but not the Lear HBO production) is available on VHS and DVD.


== Partial cast of characters ==
Performed by Williams
Charlene Bumiller - Daughter of Hank and Bertha Bumiller, and sister to Stanley and Jody
Jody Bumiller - Youngest child of Bertha Bumiller, followed constantly by "eight to ten dogs"
Stanley Bumiller - fresh from reform school; twin to Charlene, and he later talks to the dead judge gloating how he killed him
Vera Carp - Town snob and vice president of the Smut-Snatchers of the New Order
Petey Fisk - Employee of the Greater Tuna Humane Society
Didi Snavely - Owner of Didi's Used Weapons ("If we can't kill it, it's immortal")
Arles Struvie - A disc jockey at radio station OKKK
Chad Hartford- A reporter from Houston who comes to interview Bertha
Phinas Blye- A politician from Indiana who runs for City Council every election
Harold Dean Lattimer- OKKK's weatherman
Helen Bedd - waitress, Tastee Kreme Diner
Performed by Sears
Bertha Bumiller - wife of Hank and mother to Jody, Stanley, and Charlene; member of the Smut Snatchers of the New Order
Hank Bumiller- Husband of Bertha
Pearl Burras - Aunt to Bertha, who is, as Petey Fisk claims, addicted to killing dogs (in the play, called canicidal thumbitus)
Leonard Childers - Station Manager of OKKK for his talk show, "Leonard on the Line"
Sheriff Givens- Believes in old-fashioned jails
Elmer Watkins, head of the local chapter of the KKK, dedicated to making the town safe "for the right kind of people"
Yippy the dog- Pet of the month for five weeks in a row, yips a lot, and no one wants to adopt him
R.R. Snavely - UFOlogist, town drunk, and husband to Didi
The Reverend Spikes, president of the Smut Snatchers of the New Order
Thurston Wheelis - A disc jockey at radio station OKKK
Inita Goodwin - cook, Tastee Kreme Diner


== Honors for Greater Tuna ==
Joe Sears
Following a December 1994 run of A Tuna Christmas on Broadway, Joe Sears received a 1995 Tony Award nomination for best actor in a play.
Nominee, Outstanding Lead Actor in a Touring Production, Helen Hayes Awards Non-Resident Acting, 1985, 1987
Jaston Williams
San Francisco Bay Area Critics Award
L.A. Dramalogue Award


== References ==


== External links ==
Official Web Site
Greater Tuna at the Internet off-Broadway Database
A Secret History of Tuna
Interview with Jaston Williams, June 18, 1995. University of Texas at San Antonio: Institute of Texan Cultures: Oral History Collection, UA 15.01, University of Texas at San Antonio Libraries Special Collections.