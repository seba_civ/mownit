José Luis Rivera Guerra (born October 7, 1973 in Mayagüez, Puerto Rico) is a Puerto Rican politician formerly affiliated with the New Progressive Party (PNP). He was a member of the Puerto Rico House of Representatives from 2000 to 2013, representing the 17th District.
In 2011, Rivera Guerra was in the middle of a scandal, when it was revealed there were some irregularities regarding the registration and permits of his residence. Although the Ethics Committee of the House of Representatives initially dismissed the complaint, the case caused a public uproar among local newspapers and received major media attention. As a result, Governor Luis Fortuño and the PNP referred the case to the Department of Justice of Puerto Rico, and disqualified Rivera Guerra from running in the 2012 general election.
After being removed from the party, Rivera Guerra started to campaign as a write-in candidate.He received more than seven thousand votes from the voters of Aguadilla and Moca. He was still unsuccsseful to win.


== Early years and studies ==
José Luis Rivera Guerra was born in Mayagüez on October 7, 1973. He studied on the island's public school system, graduating from the José de Diego High School on 1991. In 1997, he received a Bachelor's degree in Chemistry, with a concentration in Mathematics from the University of Puerto Rico at Mayagüez. He then passed the licensing exam to be a chemist.


== Professional career ==
Rivera Guerra served in the United States Army Reserve for eight years. Professionally, he has worked as a chemist with Procter & Gamble and as a Science teacher at the Liceo Aguadillano. He also directed the recycling program for Aguadilla.


== Political career ==
Rivera Guerra was first elected to the Puerto Rico House of Representatives at the 2000 elections becoming the youngest legislator elected in the history of that district. He was elected to represent District 17. During his first term, he was one of the PNP members involved in a controversial riot at the Office of Women Affairs, which resulted in the detainment of several party leaders.
In 2003, Rivera Guerra won the primaries of his party and was again reelected at the general elections in 2004. That same year, he had to be expelled from a session after an aggressive outburst in which he broke a glass desk by hitting it.
For the 2008 PNP primaries, Rivera Guerra became a close ally of candidate Luis G. Fortuño when he ran against former Governor and PNP president Pedro Rosselló. Rivera Guerra ran against Ernesto "Junior" Robledo and two other candidates in his district. Both Fortuño and Rivera Guerra prevailed in the primaries and in the general alections.


=== Public scandal ===
At the end of 2011, Ernesto "Junior" Robledo presented various complaints against Rivera Guerra claiming irregularities in properly registering his residence with the Property Tax Collection Center (CRIM), the Puerto Rico Electric Power Authority (PREPA) and the Puerto Rico Aqueducts and Sewers Authority (PRASA). Rivera Guerra denied the allegations and Governor Luis Fortuño defended him.
Although initially, the House Ethics Committee had dismissed the issue, some irregularities and contradictions on the versions arose. As a result, Speaker Jenniffer González evaluated the case individually and decided to send it back to the Ethics Committee. The scandal caused a public uproar among the people and the case received major media attention.
On January 23, 2012, the House Ethics Committee determined that Rivera Guerra had committed no crime, but recommended that he be disciplined with a 10-day suspension due to his negligence in the issues regarding his properties. Again, this caused indignation among the people in the island, who made public manifestations against the decision. Minutes after the Ethics Committee made his decision, Governor Fortuño sent Rivera Guerra's case to the Department of Justice and said he would be evaluating his candidacy for the upcoming elections. On February 18, 2012, the Board of Directors of the PNP voted to disqualify Rivera Guerra from participating in the upcoming primaries. Still, Rivera Guerra campaigned as a write-in candidate for the elections, but was unsuccessful.
On November 30, 2012, a court issued a warrant for the arrest of Rivera Guerra. The charges were construction of a pool without informing it to the Ethics Office, and living in his residence without the proper permits. On July 11, 2013, Rivera Guerra was sentenced to 40 days of community service, and was ordered to pay multiple fines that totaled $2,900.


== Personal life ==
Rivera Guerra is married and has three children.
He is an avid athlete, dedicating himself to martial arts and cycling. In March 2011 he suffered major injuries that kept him away from attending House sessions for several weeks when he was run over by a car while he was riding a bicycle and exercising.


== References ==


== External links ==
José Luis Rivera Guerra on CamaraDeRepresentantes.org