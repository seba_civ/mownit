In 1977 Mercedes-Benz introduced a new van/truck (a.k.a. transporter), called T1 internally. Other designations were series TN / T1N ("Transporter Neu" / "Transporter 1Neu") and Bremer Transporter, since the vehicle was built in the Transporter-Plant in Bremen, Germany, first. In the years 1983/1984 production went - piece by piece - to the Transporter-Plant-Düsseldorf (city in the Rhineland, Germany). The internal chassis-designations (Baumuster, "BM") are: 601 (2,55-2,8t GVWR), 602 (3,2-3,5t GVWR) and 611 (4,6t GVWR).
The TN/T1 was available as a minibus or fitted with a cargo box body or flat cargo bed. A double cab version was offered in the latter two configurations. Three wheel bases were available, with gross weight ratings ranging from 2.55 to 4.6 tonnes. Mercedes-Benz-built gasoline or diesel engines were available as powerplant options for the rear-wheel drive chassis. The best known 4x4 Versions of the Mercedes TN/T1 were made by Iglhaut by adjusting parts of the G-Wagen to the TN/T1-chassis. The TN/T1 van was also used as a campervan conversion, being much larger than the Volkswagen Transporter. Its closest European competitors were the Volkswagen LT and the Ford Transit. In 1995, after 18 years of production, the TN/T1 van series was discontinued, succeeded by the T1N "Sprinter".
In Philippines, Mercedes-Benz T1 rebadged version, Togo Atlas is locally made by Morales Motors. The Atlas also offered as minibus or chassis cab for jeepneys and utility trucks.


== History ==
The TN/T1 van model series included 207 D, 208, 307 D and 308. They debuted in April 1977. The original line was composed of two engines and four weight classes, as follows:
207 D, 208 - gross weight 2,550 kg (5,622 lb) or 2,800 kg (6,173 lb)
307 D, 308 - gross weight 3,200 kg (7,055 lb) or 3,500 kg (7,716 lb)
207 D, 307 D - four-cylinder Diesel engine with 2404 cc and 65 hp (48 kW) engine OM 616, nearly same engine as in Mercedes-Benz 240D (W123) and 240GD (460).
208, 308 - four-cylinder petrol engine with 2307 cc and 85 hp (63 kW) engine M 115, nearly same engine as in Mercedes-Benz 230 -"Lowcompression-Export-Version".
Market share was almost 90% for the Diesel engine and a little more than 10% for the petrol engine. While the petrol engine was mainly used for ambulances, firetrucks, special vans for cold areas, commercial buyers preferred the Diesel engine for its lower fuel consumption and best reliability. In the UK, originally only diesel engines were on offer, but as the British market was 80 percent petrol at the time sales were less than scintillating. For 1982 petrol models were added, and sales immediately increased by 80 percent. The body styles were panel van, different versions of window vans, pickup and pickup with double cab. Already the power outputs for the 4-cylinder engines were very good, so the 307D was one of the fastest 3,5 ton (GVWR) Diesel-Vans in Europe of the late 1970s. Low gearing also meant that the vans were capable pullers, able to make best use of the power available whilst returning surprisingly good fuel economy for the large size and weight of vehicle.
In September 1981 the 407 D, 409 D and 410 were added with a gross weight of 4,600 kg (10,141 lb). The 409 D had a bigger and more powerful Diesel engine with five cylinders, 2998 cc and 88 hp (65 kW). It was the OM 617 engine which was also used in the Mercedes-Benz 300D passenger car and 300GD G-Wagon. Other revisions throughout the production were minor, the OM616 engine having a modified cylinder head and less bore which lowered the capacity from 2404 cc to 2399 cc, but producing slightly more power (72 bhp) and using 5-speed-gearboxes instead of the early 4-speed-versions.
In autumn 1988, two new diesel engines - called OM 601/23 and OM 602/29 - were offered, awarded the "Diesel 1989" award.
After 18 years of production, 970,000 of the Bremer Transporter were produced. In 1995 the Mercedes-Benz T1N Sprinter was launched, replacing the T1.


== Models ==

TN / T1 Diesel (1977-1995) 
* with automatic transmission only, ** for Belgium only, *** for Italy only.

TN / T1 Petrol (1977-1995) 
TN / T1 Electro (1978-1980?) 
The internal chassis-designations: 601 - 2,55-2,8 t (207 D/208/208 D/209 D/210/210 D) 602 - 3,2-3,5 t (307 D/308/308 D/309 D/310/ 310 D) 611 - 4,6 t (407 D/408 D/409 D/410 D/410)

Engine type:
601.0/602.0/611.0 - gasoline engine
601.3/602.3/611.3 - diesel engine OM61X
601.4/602.4/611.4 - diesel engine OM60X

Body type:
6XX.X1 - high-bed
6XX.X2 - low-bed
6XX.X6 - van (delivery van, box-type)
6XX.X7 - bus (station wagon)

Wheelbase:
6XX.XX1/6XX.XX6 - 3050 mm
6XX.XX2/6XX.XX7 - 3350 mm
6XX.XX8 - 3700 mm

Example: 601.426 - 209 D/210 D, low bed, wheelbase 3050 mm.


== The OM616 & Mercedes-Benz T1 (Tempo Traveller) in India ==

In 1982, Bajaj Tempo, now Force Motors, signed a deal with Daimler Benz to manufacture the OM616 Mercedes engine under licence in India for fitting on its line of vehicles. This Mercedes engine gave the company a technological edge over other Indian manufacturers led to the success of several Bajaj Tempo models.
The collaboration with Mercedes Benz (Daimler Benz) was further strengthened with the signing of the collaboration for the manufacture of the Tempo Traveller in 1986. This vehicle range was based on the popular Mercedes-Benz T1 range.
In 1987, a plant designed and built to the specification of Mercedes Benz was inaugurated at Pithampur for the production of the Tempo Traveller. The vehicle is currently in production and is one of the most popular people movers in the Indian market.
Some variants based on the Tempo Traveller (Mercedes-Benz T1) are the high roof / low roof versions, the short chassis / long chassis versions and the Excel series of commercial transport vehicles.
The Mercedes OM 616 or its variants still power most light commercial vehicles of Bajaj Tempo, including the tempo traveller (though for the traveller, there is option of a derated version BSIV Compliant 2.2 FMTECH used in Force One SUV, which is based on OM611) and the new Excel series of trucks. Bajaj Tempo is also at present assembling other Mercedes engines and supplying it to Mercedes Benz India Ltd.


== References ==


== External links ==
 Media related to Mercedes-Benz T1 at Wikimedia Commons