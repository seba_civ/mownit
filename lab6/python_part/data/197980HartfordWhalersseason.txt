The 1979–80 Hartford Whalers season was the Whalers' first season in the National Hockey League. Along with the Edmonton Oilers, Quebec Nordiques, and Winnipeg Jets, the Whalers were one of four World Hockey Association franchises that joined the NHL. The Whalers season was Gordie Howe's final season in the NHL.


== Offseason ==


=== NHL Amateur Draft ===
Hartford's draft picks from the 1979 NHL Entry Draft which was held at the Queen Elizabeth Hotel in Montreal, Quebec, on August 9, 1979.


=== NHL Expansion Draft ===


==== Reclaimed players ====
Reclaiming of players: The 17 existing NHL teams were allowed to reclaim any rights to former WHA players they held. The four incoming franchises, however, were allowed to protect up to two goaltenders and two skaters, voiding their NHL rights. These players were considered "priority selections" in the expansion draft. Gordie Howe was one of two special cases (the other being Wayne Gretzky, for the Edmonton Oilers), as a gentlemen's agreement between the Hartford Whalers and the Detroit Red Wings, which held his rights, led to the Wings declining to reclaim Howe.
These are Hartford players whose NHL rights were reclaimed when the WHA merged with the NHL. This list is incomplete.


==== Whalers selections ====


== Regular season ==


=== October ===
The Whalers played their first ever National Hockey League game on October 11, 1979 against the Minnesota North Stars at the Met Center in Bloomington, Minnesota. Gordie Roberts scored the Whalers first goal in franchise history, as the Whalers lost to the North Stars 4-1. Two nights later, on October 13, the Whalers earned their first point, as they tied the Pittsburgh Penguins 3-3. The Whalers would end their season starting four game road trip with a 0-2-2 record.
On October 19, the Whalers played their first ever home game at the Springfield Civic Center, and were led by Mark Howe, as he scored two goals, including the Whalers first ever home goal, as the Whalers defeated the Los Angeles Kings 6-3 in front of 7,626 fans. Goaltender John Garrett made 18 saves for the victory.
Hartford earned their first road win on October 31, as they defeated the Toronto Maple Leafs 4-2 at Maple Leaf Gardens in Toronto, Ontario.
Overall, the Whalers had a 2-4-4 record for eight points in October, sitting in fourth place in the Norris Division.


=== November ===
After winning their final game of October, the Whalers extended their winning streak to three games, as they defeated the Toronto Maple Leafs for the second time in a row, this time by a 5-3 score at home with Gordie Howe leading the way with two goals, followed by a big 7-2 victory on the road against the Colorado Rockies to even their overall record to 4-4-4. The Black Hawks stopped the Whalers winning streak on November 9 as Chicago won the game 4-2 at the Civic Center.
On November 19, goaltender Al Smith stopped all 23 shots against the Edmonton Oilers to earn the first shutout in Whalers history, as Hartford won the game 4-0 at home.
The Whalers continued to hover around the .500 mark throughout the month, and by the end of November, they had an 8-8-6 record, earning 22 points and remained in fourth place in the Norris Division, just one point behind the Pittsburgh Penguins for third.


=== December ===
The Whalers opened the month with two ties on the road, as they skated to a 4-4 tie against the Montreal Canadiens at the Montreal Forum on December 1, followed by a 3-3 draw against the Washington Capitals at the Capital Centre to improve their record to 8-8-8 for the season.
Hartford would then lose their next five games, including being shutout for the first time in team history, as on December 9 they lost 3-0 to the Edmonton Oilers at Northlands Coliseum. The Whalers would snap their five game losing streak on December 19, with a 5-4 win over the Washington Capitals, however, the club would end December on a four game winless streak (0-3-1).
For the month, the Whalers had a record of 1-8-3 in 12 games, and an overall record of 9-16-9, getting 27 points, as the team slipped into the Norris Division cellar, two points behind the Detroit Red Wings.


=== January ===
Hartford's winless streak would continue into January, as after the Whalers went 0-3-1 to finish the month of December, the club would open January with a 0-4-1 record, extending their streak to nine games. Since December 1, the Whalers had a 1-12-4 record.
The team would snap out of their slump on January 12, as both Jordy Douglas and Blaine Stoughton scored twice, leading the Whalers to a 6-4 win over the Detroit Red Wings. This began a four game winning streak for Hartford, as the team would then defeat the Pittsburgh Penguins 7-1, Chicago Black Hawks 5-3 and Winnipeg Jets 7-2.
The Montreal Canadiens ended the Whalers winning streak on January 24, as they defeated Hartford 7-2, followed by a 3-2 loss to the New York Islanders two nights later, however, the Whalers would win their final two games of the month, including a 6-1 victory over the Atlanta Flames, and an 8-2 win over their rivals, the Boston Bruins to close out the month.
The Whalers put together a record of 6-6-1 in January, improving to 15-22-10 in the season, however, the club remained in last place in the Norris Division, five points behind the Detroit Red Wings.


=== February ===
After dropping their first game of the month against the Atlanta Flames, the Whalers would then go on a six game homestand, putting together a 3-1-2 record, highlighted by a 5-3 victory over the New York Islanders, led by Jordy Douglas and his four goals in their final game at the Springfield Civic Center on February 3. In their first game at the Hartford Civic Center on February 6, Mark Howe had two goals and two assists, leading the Whalers to a 7-3 win over the Los Angeles Kings in front of 14,448 fans.
After a loss on February 16 by a 5-3 score at Maple Leaf Gardens in Toronto, the Whalers put together a three game winning streak, scoring six goals in each of their wins. The Quebec Nordiques snapped the Whalers winning streak on February 26 by defeating Hartford 9-5. Hartford lost again the next night against the Boston Bruins by a 6-3 score, however, the team ended the month with a 3-0 win over the St. Louis Blues.
The Whalers put together their first winning month in team history, as the team went 7-5-2 in 14 games, and improved to 22-27-12 on the season, earning 56 points, and back into fourth place in the Norris Division, three points ahead of the Detroit Red Wings, and only one point behind the Pittsburgh Penguins for third.


=== March/April ===
Hartford won their first game of March by a score of 6-3 against the St. Louis Blues, however, the team would then go winless in their next four games (0-2-2). The Whalers snapped out of their slump with two straight wins, defeating the Vancouver Canucks 3-1 on March 13, followed by a 4-1 victory over the Colorado Rockies two nights later on March 15.
After losing to the Minnesota North Stars 6-1 on March 16, the team would then tie their next three games, each by a 5-5 score, twice against the Montreal Canadiens and once against the Philadelphia Flyers to extend their winless skid to four games. Hartford then lost two more games in a row, which included a 7-0 loss to the Winnipeg Jets, making it a six game winless streak.
Hartford's winless streak would extend to eight games, as they tied the Vancouver Canucks 4-4, followed by a 2-2 tie against the Los Angeles Kings. The streak would continue, as Hartford then lost 5-4 to the Toronto Maple Leafs on April 1, followed by a 6-4 loss to the Pittsburgh Penguins the following day, extending the Whalers streak to 10 games.
Hartford ended their winless streak on April 4 in convincing fashion, as the Whalers routed the Quebec Nordiques by a 9-2 score. Two nights later, in their last game of the season, the Whalers defeated the Detroit Red Wings 5-3, highlighted by a Gordie Howe goal in his last ever regular season game in front of 10,687 fans at the Civic Center.
Overall, the Whalers finished their inaugural season with a 27-34-19 record, earning 73 points, and finished in fourth place in the Norris Division, earning a playoff berth. Their 73 points tied the Pittsburgh Penguins, however, the Penguins held the tie breaker, as Pittsburgh had 30 wins, and only one point behind the second place Los Angeles Kings. The Whalers finished 10 points ahead of the last place Detroit Red Wings.


=== Gordie Howe ===
When the WHA folded in 1979, the Hartford Whalers joined the NHL and the 51-year-old Howe signed on for one final season playing in all 80 games of the schedule, helping his team to make the playoffs with fifteen goals. One particular honor was when Howe, Phil Esposito, and Jean Ratelle were selected to the mid-season all-star game by coach Scotty Bowman, as a nod to their storied careers before they retired. Howe had played in five decades of all-star games and he would skate alongside the second-youngest to ever play in the game, 19-year-old Wayne Gretzky. The Joe Louis Arena crowd gave him a standing ovation twice, lasting so long, he had to skate to the bench to stop people from cheering. He had one assist in his side's 6–3 win.


=== Final standings ===
Note: GP = Games played, W = Wins, L = Losses, T = Ties, Pts = Points, GF = Goals for, GA = Goals against
Note: Teams that qualified for the playoffs are highlighted in bold.


== Schedule and results ==


== Player statistics ==


=== Skaters ===
Note: GP = Games played; G = Goals; A = Assists; Pts = Points; +/- = Plus/minus; PIM = Penalty minutes


=== Goaltending ===
Note: GP= Games played; W= Wins; L= Losses; T = Ties; SO = Shutouts; GAA = Goals against average


== Playoffs ==
The Whalers opened the 1980 Stanley Cup Playoffs against the Montreal Canadiens. Montreal finished the regular season with a 47-20-13 record, earning 107 points and had the best record in the Norris Division. The Canadiens had also won the last four Stanley Cup championships.
The series opened on April 8 at the Montreal Forum in Montreal, Quebec, and the Canadiens opened the scoring 5:50 into the game, when Yvon Lambert beat Whalers goaltender Al Smith to take a 1-0 lead. The Canadiens extended the lead to 2-0 when Brian Engblom beat Smith before the end of the first period. In the second period, Montreal continued to score, as Rick Chartraw made in 3-0 for the Canadiens, followed by a goal by Guy Lafleur to make it 4-0 after two periods. Yvon Lambert scored his second goal of the game midway through the third period, making it 5-0 Montreal, then the Canadiens Steve Shutt scored a powerplay goal at 13:15 of the third, making it 6-0 for the Canadiens. Hartford finally managed a goal late in the game, as Mark Howe scored with 4:59 remaining in the game, spoiling the shutout, as the final score was 6-1 for Montreal, and the Canadiens took a 1-0 series lead.
John Garrett got the start in goal for the Whalers in the second game, however, it was the Canadiens who scored early, as Doug Jarvis scored 30 seconds into the game, making it 1-0. Less than a minute later, Rejean Houle scored another for Montreal, as the Canadiens had a 2-0 lead 1:20 into the game. Before the period was over, the Canadiens added another, as Yvon Lambert had his third goal of the series, making the score 3-0. The Canadiens Gaston Gingras opened the scoring 1:33 into the second period, giving the Habs a 4-0 lead. Ray Neufeld finally got the Whalers on the board, as Marty Howe and Gordie Howe set him up with a goal, as the Whalers cut into the lead, making 4-1. The Canadiens responded quickly though, as Bob Gainey scored 24 seconds later, making it 5-1 for the Canadiens. The Whalers Gordie Roberts made it 5-2 midway through the period, however, the Canadiens continued the route, as they scored three more goals in the period, making it 8-2 after two periods. Hartford's Marty Howe and Gordie Howe each scored goals in the third period, making the final score 8-4 for Montreal, and a 2-0 series lead for the Canadiens.
The series moved the Hartford Civic Center for the third game, making it the first home playoff game in team history in front of 14,460 fans. Goaltender Al Smith returned to the Whalers net, and Hartford opened the scoring when Pat Boutette scored an unassisted shorthanded goal at the 7:03 mark of the first period, giving the Whalers their first lead of the series. The Canadiens responded five minutes later on a goal by Guy Lafleur, as the score was 1-1 after a period of play. The Whalers Tom Rowe broke the tie 1:27 into the second period, making it 2-1 for the Whalers, however, once again, the Canadiens tied it 2-2, after a goal by Yvon Lambert, his fifth of the series, less than five minutes later. The two teams remained tied heading into the third period. Montreal's Rejean Houle scored early in the third, giving the Habs their first lead of the game, however, Tom Rowe of the Whalers tied the score midway through the period with his second goal of the game. Neither team could score again, sending the game into overtime. In the extra period, the Canadiens Yvon Lambert scored his second goal of the game, and sixth of the series, only 29 seconds into overtime, as Montreal won the game 4-3, and swept the series 3-0. This game marked the final game of Gordie Howe's career, as the 52-year-old retired after the season.
Montreal Canadiens 3, Hartford Whalers 0


== Awards and records ==
1980 NHL All-Star Game
Gordie Howe, Forward


== Roster ==


== References ==
Whalers on Hockey Database