Tigers Ultras is the name of the ultras group of Greek football team Asteras Tripolis F.C. and basketball team S.E.F.A. Arkadikos BC, based in Tripoli, Greece. Found in 2006, it is the first fan base dedicated to any Arcadian team. Their logo is the face of a tiger in front of a yellow star (which is Asteras’ logo).


== History ==
Tigers Ultras firstly appeared supporting Asteras, in 2006. The group was created by some Asteras Tripolis fans, in a cafeteria in Tripoli. At the time, the team was competing in the second tier of Greek football, the Football League. They would attend the games from Gate 6 of the two-stand, at the time, Theodoros Kolokotronis Stadium. In 2008, two new stands were built, located behind the goals. It was then when Tigers where transferred in the stand, near the train station. With the construction of the new stands, the team decided to change the number allocation of the gates. Tigers Ultras would be attending the games from Gate 2, Sector C. In 2008, they started attending Arkadikos games, in the Tripoli Indoor Hall.


== Political views ==
Tigers Ultras are a no political (neither right-wing nor left-wing) ultras group. They support localism, which explains the Greek flags and the Theodoros Kolokotronis banners often seen at the stands.


== References ==