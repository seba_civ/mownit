The Social-National Assembly of Ukraine (S.N.A.) is an assemblage of the ultra-nationalist and neo-Nazi radical organizations and groups founded in 2008 that share the social-national ideology and agree upon building a social-national state in Ukraine. It is located on the far right of the Ukrainian politics and built around the "Patriot of Ukraine". In late November 2013 both the S.N.A. and the "Patriot of Ukraine" entered in an association with several other Ukrainian far-right groups which lead to the formation of the Right Sector. The S.N.A. is also reported to be close to Svoboda, and Yuriy Zbitnyev, the leader of the nationalist political party "Nova Syla" (New Force). The S.N.A.'s activities are largely Kiev-based.
In short, the S.N.A. is an aggregation of small and large groups of the Ukrainian Neo-Nazis, right-wing nationalists, direct action radicals, violent street extremists and also some patriotic youth with militaristic and authoritarian leaning which was created by Andriy Biletsky, who leads both the S.N.A. and the "Patriot of Ukraine", to fulfill his long-reaching political aspirations.


== History ==


=== 2000s ===
The S.N.A. was founded in 2008 and maintained relations with the wider social-nationalist movement in Ukraine. In the late 2000s, Ukrainian president Victor Yushchenko and the Our Ukraine bloc bolstered the S.N.A. and other far-right groups by supporting an explicitly nationalist view of Ukrainian history. Following the 2009 death of Maksym Chaika, an S.N.A member who was killed in a fight with antifascists in Odessa, Yushchenko supported the far-right interpretation of Chaika's death, describing him and others as heroes and victims driven to violence for a just cause.
In 2010, the Ukrainian Helsinki Human Rights Union reported on attacks by the S.N.A and "Patriot of Ukraine" against Vietnamese and other foreign market stalls in Vasylkiv. Most attacks were carried out by youth and targeted Vietnamese, Uzbeks and Gypsies. According to the S.N.A website, they drove foreigners from the market within two weeks and replaced them with Ukrainians. The S.N.A states that some of their victims were hospitalized. Later that year, Ukrainian authorities shut down an S.N.A music festival near Kiev that promoted neo-Nazism and chauvinism among Ukrainian youth. The music glorified the skinhead movement, Nazi aesthetics and the harassment of minorities.
In August 2011, the Netherlands Institute of Human Rights warned about the growth of extremist organizations including the S.N.A and "Patriot of Ukraine", noting repeated attacks against foreigners and visible minorities. The institute also noted the government's inability or unwillingness to deal with extreme-right movements in Ukraine.
In August 2011 Ukrainian police announced that they thwarted a bomb attack planned for the commemoration of Ukrainian Independence. The "Patriot of Ukraine", a part of the S.N.A, declared that some of their members had been detained by police but maintained no connection with any terrorist plan. Spokespersons for the S.N.A and "Patriot of Ukraine" insisted that criminal action against them was a pretext for SBU repression against their organizations.


=== Involvement in Maidan ===

In 2013, the S.N.A, "Patriot of Ukraine" and Autonomous Resistance all increased in popularity, contributing to the growth of Svoboda as well. The Social National Assembly helped to create an umbrella radical organization - the Right Sector (Pravy Sector). Other openly radical anti-semitic groups operating in Ukraine including the "White Hammer" and "C14", a neo-Nazi wing of Svoboda, joined it.
During the 2014 Ukrainian revolution, the militants from the S.N.A and the "Patriot of Ukraine" were on the front lines of the street riots in Kiev. According to Igor Krivoruchko, the leader of the Kiev's S.N.A. branch, its members started clashes with the police near the Presidential Administration Building (Kiev) and also initiated the Hrushevskoho Street riots. They seized and burned on February 18, 2014 the central office of the ruling party - the Party of Regions - in Kiev. A bystander, 57-year-old IT engineer who tried to stop the attackers from entering the server room, was beaten to death.
Oleh Odnoroshenko, the S.N.A and "Patriot of Ukraine" ideologue and also one of the "Right Sector" leaders, stated in February 2014 that the "Right Sector" would be hesitant to enter into the government following the departure of Viktor Yanukovych. Odnoroshenko thought that the politicians would try to use the Right Sector credibility and popularity while pursuing their own agendas.
At the end of April 2014, S.N.A members marched with burning torches to the Independence Square and came into conflict with the Self-defense of the Maidan units. During the fight, the S.N.A. and Self-defense of the Maidan activists used rubber bullet guns and tear gas, and ambulances later arrived to treat wounded.
Oleh Odnoroshenko volunteered to the press that the S.N.A members organized the attack on the Russian embassy in Kiev on June 14, 2014.


=== Azov Battalion ===
In March 2014 the Social-National Assembly created a volunteer group, Azov Battalion. In April, members were wounded in combat against separatists in eastern Ukraine. During the first week of May, Kiev granted it official status and began delivering weapons.
On May 6, Azov Battalion captured Donetsk People's Republic defense minister Igor Kakidzyanov. The next day the S.N.A. announced that it was interrogating the captive, and Radical Party (Ukraine) leader Oleh Lyashko posted photographs of him naked and bound. Lyashko confirmed that Azov had captured Kakidzyanov and some other separatist leaders.
On June 13, Azov Battalion stormed separatists' barricades in Mariupol and seized control of the city center after a six-hour battle.
The group has been assigned to patrol the Azov Sea coastline and prevent arms smuggling.


== Ideology ==
According to the founder of the organization, Biletsky states that Social-Nationalism is based on three pillars: Racism, Socialism and Great Power. The ideology stands in a strong opposition to any form of liberalism or democracy. Under Socialism in Social Nationalist ideology means third position in economics, authoritative power, and fair distribution among national producers. According to Biletsky, the main distinction between Nazism and Social Nationalism is the fact that Nazism is more socialist, while Social Nationalism is more nationalist. At the same time justification in Social Nationalism over Nationalism lays in importance of a social revolution to achieve national revolution.
Political scientist Anton Shekhovtsov, Foreign Policy journalist Alec Luhn and Haaretz journalist Lolita Brayman describe the S.N.A as a far-right, neo-Nazi or racist group. The S.N.A is also a "street combat movement" hostile to ethnic and social minorities: according to researchers and its own website it has carried out physical attacks against them.
Over half the membership of the Azov Battalion, a Social-National Assembly military group, is composed of Russian-speaking eastern Ukrainians. A ministerial adviser, Anton Gerashchenko, denies neo-nazi allegations, stating, "The Social-National Assembly is not a neo-Nazi organization… It is a party of Ukrainian patriots."


== See also ==
Right Sector
Patriot of Ukraine
Svoboda
Tryzub


== References ==


== External links ==
S.N.A.Official site
S.N.A. Twitter account