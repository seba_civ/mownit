The Rashidun Caliphs (meaning "Rightly Guided", "Righteously Guided", "Righteous" Caliphs; Arabic: الخلفاء الراشدون‎ al-Khulafāʾu ar-Rāshidūn), often simply called, collectively, "the Rashidun", is a term used in Sunni Islam to refer to the 30-year reign of the first four caliphs (successors) following the death of the Islamic prophet Muhammad, namely: Abu Bakr, Umar, Uthman ibn Affan and Ali of the Rashidun Caliphate, the first caliphate. The concept of "Rightly Guided Caliphs" originated with the later Abbasid Caliphate based in Baghdad. It is a reference to the Sunni imperative "Hold firmly to my example (sunnah) and that of the Rightly Guided Caliphs" (Ibn Majah, Abu Dawood).


== History ==
The first four Caliphs who ruled after the death of Muhammad are often described as the "Khulafāʾ Rāshidūn". The Rashidun were either elected by a council (see the election of Uthman and Islamic democracy) or chosen based on the wishes of their predecessor. In the order of succession, the Rāshidūn were:
Abu Bakr (632–634 CE).
Umar ibn al-Khattab, (Umar І, 634–644 CE) – Umar is often spelled Omar in some Western scholarship.
Uthman ibn Affan (644–656 CE) – Uthman is often spelled Othman (or Osman) in some non-Arabic scholarship.
Ali ibn Abi Talib (656–661 CE) – During this period however, Muawiyah ibn Abi Sufyan (Muawiyah I) controlled the Levant and Egypt regions independently of Ali.
In addition to this, there are several views regarding additional rashidun. Hasan ibn Ali, the eldest grandson of Muhammad, briefly succeeded Ali ibn Abi Talib as caliph in 661 CE and is recognized by several historians as part of the Rashidun. Hasan ibn Ali abdicated his right to the caliphate in favour of Muawiyah I in order to end the potential for ruinous civil war.

Umar ibn Abdul Aziz (Umar ІІ), who was one of the Umayyad caliphs, has often been regarded by Sunni historians as one of the Rashidun, as quoted by Taftazani. More rarely, the Ottoman caliph Fatih Sultan Mehmed (Mehmed II) is also sometimes regarded to be among the rightly guided caliphs. In the Ibadi tradition however, only the first two caliphs, Abu Bakr and Umar are considered to be the "Two Rightly Guided Caliphs".
Ibn Hajar al-Asqalani also includes the Abbassid caliphs, including Harun al-Rashid, in his enumeration.


=== Abu Bakr ===

Abu Bakr (Abdullah ibn Abi Qahafa) (Arabic: عبد الله بن أبي قحافة‎, translit. 'Abdullāh bin Abī Quhāfah, c. 573 CE unknown exact date 634/13 AH) was a senior companion (Sahabi) and the father-in-law of Muhammad. He ruled over the Rashidun Caliphate from 632-634 CE when he became the first Muslim Caliph following Muhammad's death. As caliph, Abu Bakr succeeded to the political and administrative functions previously exercised by Muhammad, since the religious function and authority of prophethood ended with Muhammad's death according to Islam. Abu Bakr was called Al-Siddiq (The Truthful) and was known by that title among later generations of Muslims. He prevented the recently converted Muslims from dispersing, kept the community united and consolidated Islamic grip on the region by containing the Ridda, while extending the Dar Al Islam all the way to the Red Sea.


=== Umar ibn al-Khattab ===

Umar (Arabic: عمر بن الخطاب‎, translit. `Umar ibn al-Khattāb, c. 586–590 – 644) c. 2 Nov. (Dhu al-Hijjah 26, 23 Hijri) was a leading companion and adviser to Muhammad, and became the second Muslim caliph after Muhammad's death and ruled for 10 years. He succeeded Abu Bakr on 23 August 634 as the second caliph, and played a significant role in Islam. Under Umar the Islamic empire expanded at an unprecedented rate ruling the whole Sassanid Persian Empire and more than two thirds of the Eastern Roman Empire. His legislative abilities, his firm political and administrative control over a rapidly expanding empire and his brilliantly coordinated multi-prong attacks against the Sassanid Persian Empire that resulted in the conquest of the Persian empire in less than two years, marked his reputation as a great political and military leader. Among his conquests are Jerusalem, Damascus, and Egypt. He was killed by a Persian captive.


=== Uthman ibn Affan ===

`Uthman ibn `Affan (Arabic: عثمان بن عفان‎) (c. 579 – 17 July 656) was one of the companions of Muhammad. Uthman was born into the Umayyad clan of Mecca, a powerful family of the Quraysh tribe. He became caliph at the age of 70. Under his leadership, the empire expanded into Fars (present-day Iran) in 650 and some areas of Khorasan (present-day Afghanistan) in 651, and the conquest of Armenia was begun in the 640s. His rule ended when he was assassinated.
Uthman is perhaps best known for forming the committee which compiled the basic text of the Quran as it exists today, based on text that had been gathered separately on parchment, bones and rocks during the life time of Muhammad and also on a copy of the Quran that had been collated by Abu Bakr and left with Muhammad's widow after Abu Bakr's death. The committee members were also reciters of the Quran and had memorised the entire text during the lifetime of Muhammad. This work was undertaken due to the vast expansion of Islam under Uthman's rule, which encountered many different dialects and languages. This had led to variant readings of the Quran for those converts who were not familiar with the language. After clarifying any possible errors in pronunciation or dialects, Uthman sent copies of the sacred text to each of the Muslim cities and garrison towns, and destroyed variant texts.


=== Ali ibn Abi Talib ===

Ali was the cousin of Muhammed and grew up in the same household. He was the second person after Khatija, the first wife of Muhammed, to accept Islam in Makkah. He was only 10 years old at the time of his conversion. At the age of 21, he married Muhammed's youngest daughter to Khatija, Fatima and became the son-in-law of Muhammed. He had three sons and two daughters with Fatima; Hassan, Hussain, Muhsin, Umme-kulsum and Zainab. Muhsin died in childhood. He was a scribe of the Quran and kept a written copy of it. He memorized verses from the Quran as soon as they were revealed. During the Khalifat of Uthman, Umar and Abu Bakr, he was part of the Majlis-e-Shura and took care of Madina in their absence.
After the death of Uthman, Medina was in political chaos for a number of days. After four days, when the rebels who assassinated Uthman felt that it was necessary that a new Khalifa should be elected before they left Medina, Many of the companions approached Ali to take the role of caliph, which he refused to do initially. The rebels then offered Khalifat to Talha and Zabair who also refused. The ansars also declined their offer to choose a new Kahlifa. Thus, the rebels threatened to take drastic measures if a new Khalifa was not chosen within 24 hours. To resolve the issue, all Muslim leaders gathered at the mosque of the Prophet. They all agreed that the best person who fit all the qualities of a Caliph was Ali. Therefore, Ali was persuaded into taking the post. Talha and Zubair and some others then performed Bayyat at Hazrat Ali's hand followed by a general Bayyat on 25th Zil Hajj 656 CE.
After his appointment as caliph, Ali dismissed several provincial governors, some of whom were relatives of Uthman, and replaced them with trusted aides such as Malik al-Ashtar. Ali then transferred his capital from Medina to Kufa, the Muslim garrison city in what is now Iraq. The capital of the province of Syria, Damascus, was held by Mu'awiyah, the governor of Syria and a kinsman of Uthman, Ali's slain predecessor.
His caliphate coincided with the First Fitna or civil war when Muslims were divided over who had the legitimate right to occupy the caliphate, and which was ended, on the whole, by Mu'awiyah's assumption of the caliphate.
Ali was assassinated, and died on the 21st of Ramadan in the city of Kufa (Iraq) in 661 CE by Abdur Rehman ibn Muljim, a Kharijite who was later pardoned and left by Ali's son Imam Hassan (Muhammad's grandson) according to the will of Ali.


== Military expansion ==

The Rashidun Caliphate greatly expanded Islam beyond Arabia, conquering all of Persia, besides Syria (637), Armenia (639), Egypt (639) and Cyprus (654).


== Social policies ==
During his reign, Abu Bakr established the Bayt al-Mal (state treasury). Umar expanded the treasury and established a government building to administer the state finances.
Upon conquest, in almost all cases, the caliphs were burdened with the maintenance and construction of roads and bridges in return for the conquered nation's political loyalty.


=== Civil activities ===
Civil welfare in Islam started in the form of the construction and purchase of wells. During the caliphate, the Muslims repaired many of the aging wells in the lands they conquered.
In addition to wells, the Muslims built many tanks and canals. Many canals were purchased, and new ones constructed. While some canals were excluded for the use of monks (such as a spring purchased by Talhah), and the needy, most canals were open to general public use. Some canals were constructed between settlements, such as the Saad canal that provided water to Anbar, and the Abi Musa Canal to provide water to Basra.
During a famine, Umar ibn al-Khattab ordered the construction of a canal in Egypt connecting the Nile with the sea. The purpose of the canal was to facilitate the transport of grain to Arabia through a sea-route, hitherto transported only by land. The canal was constructed within a year by 'Amr ibn al-'As, and Abdus Salam Nadiv writes that "Arabia was rid of famine for all the times to come."
After four floods hit Mecca after Muhammad's death, Umar ordered the construction of two dams to protect the Kaaba. He also constructed a dam near Medina to protect its fountains from flooding.


=== Settlements ===
The area of Basra was very sparsely populated when it was conquered by the Muslims. During the reign of Umar, the Muslim army found it a suitable place to construct a base. Later the area was settled and a mosque was erected.
Upon the conquest of Madyan, it was settled by Muslims. However, soon the environment was considered harsh, and Umar ordered the resettlement of the 40,000 settlers to Kufa. The new buildings were constructed from mud bricks instead of reeds, a material that was popular in the region, but caught fire easily.
During the conquest of Egypt the area of Fustat was used by the Muslim army as a base. Upon the conquest of Alexandria, the Muslims returned and settled in the same area. Initially the land was primarily used for pasture, but later buildings were constructed.
Other already populated areas were greatly expanded. At Mosul, Arfaja al-Bariqi, at the command of Umar, constructed a fort, a few churches, a mosque and a locality for the Jewish population.


== Muslim views ==
The first four caliphs are particularly significant to modern intra-Islamic debates: for Sunni Muslims, they are models of righteous rule; for Shia Muslims, the first three of the four were usurpers. It is prudent to note here that accepted traditions of both Sunni and Shia Muslims detail disagreements and tensions between the four rightly guided caliphs.


=== Sunni perspectives ===
They are called so because they have been seen as model Muslim leaders by Sunni Muslims. This terminology came into a general use around the world, since Sunni Islam has been the dominant Islamic tradition, and for a long time it has been considered the most authoritative source of information about Islam in the Western world.
They were all close companions of Muhammad, and his relatives: the daughters of Abu Bakr and Umar were married to Muhammad, and three of Muhammad's daughters were married to Uthman and Ali. Likewise, their succession was not hereditary, something that would become the custom after them, beginning with the subsequent Umayyad Caliphate. Council decision or caliph's choice determined the successor originally.
Sunnis have long viewed the period of the Rashidun as exemplary and a system of governance—based upon Islamic righteousness and merit—they seek to emulate. Sunnis also equate this system with the worldly success that was promised by Allah, in the Quran and hadith, to those Muslims who pursued His pleasure; this spectacular success has further added to the emulatory appeal of the Rashidun era.


=== Shia tradition ===
According to Shia Islam, the first caliph should have been Ali followed by the Shia Imams. Shia Muslims support this claim with the Hadith of the pond of Khumm. Another reason for this support for Ali as the first caliph is because he had the same relationship to Muhammad as Aaron (Hārūn) had to Moses (Mūsa). This is because of the Hadith or saying of Muhammed, "You (Ali) are to me as Harun was to Musa, except that there will be no prophet after me". Starting with Muhammad to Ali to the grandsons of Muhammad, Hasan ibn Ali and Hussein ibn Ali (Muhammad had no surviving sons of his own) and so on.


== Timeline ==
Note that a caliph's succession does not necessarily occur on the first day of the new year.


== See also ==
Hadith of the ten promised paradise
The Four Companions


== Notes ==


== External links ==
 Media related to Rashidun Caliphs at Wikimedia Commons