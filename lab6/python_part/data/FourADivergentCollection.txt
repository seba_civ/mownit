Four: A Divergent Collection is a collection of five short stories from the Divergent trilogy, told from Tobias Eaton's (Four) perspective, and written by Veronica Roth. The first story of the collection, Free Four: Tobias Tells the Divergent Knife-Throwing Scene, was released as an e-book on April 23, 2012. The second story, The Transfer, was released on September 3, 2013. The third story titled The Initiate,  the fourth story The Son and the fifth and final story The Traitor were released on July 8, 2014. Simultaneously with the release of last three short stories, a collected edition of the five short stories titled Four: A Divergent Collection was released on July 8, 2014, which also features three exclusive scenes.


== Background ==
According to Roth, these stories serve as a prequel to the Divergent trilogy. She said that "The e-shorts will address Tobias's backstory and some of the mysterious things going on in the world before Tris chose Dauntless. Together, they form a kind of Tobias-centered Divergent prequel, though there is some overlap."
Talking about her decision to write from the perspective of Four, Roth said, "In my mind, he has a distinct history and a complex psychology, so there’s always a lot to draw from when he’s on the page, and it’s an opportunity for me to break from Tris’s sparse, straightforward voice and try to introduce just a little bit more poetic language, I also know, from many tweets and blog comments and in-person comments, that Divergent readers are interested in hearing more of his voice, which made him an obvious candidate! My readers have given me so much enthusiasm and honesty and wisdom. The opportunity to write something that I knew they wanted to read was really appealing to me."
In July 2014, Roth revealed that she initially wrote Divergent from Four's point of view and at that time the character Tris was not present in the story. She explained that "it was the super rough draft. I've been calling it the "proto-draft." It happened before the rough draft, which sounds crazy. It was just was a couple of pages from his perspective, and it just didn't work in the same way that Tris's voice worked because she's kind of surprising as the narrator of that particular story. But as I wrote the series, he became more complicated and his story started to feel urgent also. So, it's great that they let me keep writing about him!"


== Free Four: Tobias Tells the Divergent Knife-Throwing Scene ==

Free Four is the first story of the series and retells the events of chapter thirteen of Divergent. It was released on April 23, 2012.


== The Transfer ==
The Transfer features Four as a 16-year-old before his Choosing Day Ceremony and as a Dauntless initiate. It was published by Katherine Tegen Books/HarperCollins and released on September 3, 2013.


=== Plot ===
16-year-old Tobias Eaton, as per his father, Marcus's instruction, picks the choices that lead him to pass for Abnegation during his Aptitude Test. His meeting with a factionless man who seems to know his deceased mother, however, startles his choice, especially when the abusive Marcus beats him for lying about a gift his mother gave to him. Tobias eventually chooses Dauntless on Choosing Day, shocking Marcus.
Tobias is introduced to his instructor, Amar, and his fellow initiates, including Erudite transfer Eric. In a slight change, the initiates now have to face their fears right after making the jump from the roof. Tobias' four fears: his acrophobia, claustrophobia, remorse for shooting innocents, and his father's abuses, astonishes Amar. Since no other Dauntless has that low of a number of fears, Amar gives Tobias his new name: "Four".


=== Cover ===
The cover of The Transfer features a circle of train cars, which are colliding in four different places. Joel Tipple, who designed this cover, is also designing the covers for rest of the series.


=== Reception ===
BiblioFiend in its review said that "The Transfer gave an immense amount of depth to a character who always seemed so secretive and hard. Getting inside Four’s mind was a great way to understand his motives and reasoning for choosing to leave Abnegation."


== The Initiate ==
The Initiate reveals Four's life as a Dauntless initiate. It was published by Katherine Tegen Books/HarperCollins and released on July 8, 2014.


=== Plot ===
Tobias "Four" Eaton goes through his Dauntless initiation tests. His first task has him forge a Dauntless tattoo. His next task is to fight his archrival, Eric, whom he manages to defeat. As he begins bonding with his Dauntless-born fellow initiates, Ezekiel "Zeke" and Shauna, he begins to forgo his isolationist and quiet Abnegation principle and starts embracing the lifestyle of a Dauntless.
In Four's next test, he faces his fears again, but manages to recognize that it is artificial and escapes from it in an unorthodox manner. Amar tells him that he also passed his test like Four, warning the latter not to do the test with that method again. The two are forced to do the test by Erudite leader, Jeanine Matthews. Though Four is careful, Amar is found dead the next day, which everyone but Four attributes to suicide. Four threatens Eric for his supposed part in causing Amar's death before the test announcement, in which Four ranks first. While celebrating his achievement, Four sees what appears to be his mother boarding a factionless train in the middle of the night.


=== Cover ===
The cover of The Initiate features a circle of syringes filled with blue coloured serum, all of them pointing at the centre of the circle. Like all the previous covers in the Divergent trilogy, Joel Tipple designed this cover.


== The Son ==
The Son explores Four's life as a Dauntless member. It was published by Katherine Tegen Books/HarperCollins and released on July 8, 2014.


=== Plot ===
The recently initiated Dauntless member, Tobias "Four" Eaton, is offered a chance to be promoted to the position of leader by Max, competing with his archenemy, Eric. He suspects that something foul is going on ever since the mysterious death of his mentor, Amar, for being a Divergent, which he links to Erudite, led by Jeanine Matthews, controlling Dauntless for a secret purpose. At the same time, he receives a note which leads him to reunite with his factionless mother, Evelyn, long presumed dead. Though their meeting is rough, Evelyn tells Four that there is a conspiracy going on and that she is still welcoming him to join her.
Four eavesdrops on Max's conversation with Jeanine which confirms his suspicion that Jeanine intends to use whomever is selected to be leader as a pawn. He intentionally fails during the final interview for the job in favor of becoming instructor for the new initiates. This allows Eric to win and implement his harsher suggestion for the next initiation tests. Four then decides to keep contact with his mother by delivering her a letter through an aide: the factionless man whom he met after his Aptitude Test several weeks before.


=== Cover ===
The cover of The Son features a circle of dark inky cloud, with lightning striking at two different points of circle. Like all the previous covers in the Divergent trilogy, Joel Tipple designed this cover.


== The Traitor ==
The Traitor narrates early parts of Divergent from Four's point of view. It was published by Katherine Tegen Books/HarperCollins and released on July 8, 2014.


=== Plot ===
Two years after the events of The Son, Tobias "Four" Eaton is working as the instructor for Dauntless transfer initiates, including Tris Prior. For two years, he has been secretly spying on Max and Jeanine Matthews and keeping contact with his mother, Evelyn. Four learns that Max and Jeanine are planning on an invasion to Abnegation. Evelyn responds nonchalantly when Four brings it up, and makes Four reconsider his position on his old faction.
Throughout the story, Four begins to get closer with Tris, helped by his admiration of her bravery and selflessness. He becomes protective of her, growing concerned when he learns that she is also a Divergent, and also saves her from an attack by her desperate fellow initiates. He also eventually allows her to know more about him by going through his fear landscape together. Four realizes that he will have to betray his new faction to protect his old faction: by attempting to warn his father, Marcus, about the upcoming invasion, which the latter quickly brushes off. Seeing another option, he looks to Tris for assistance.


=== Cover ===
The cover of The Traitor features a greenish circle of crows flying away. Like all the previous covers in the Divergent trilogy, Joel Tipple designed this cover.


== Exclusive scenes ==
The book also features three exclusive scenes that retell the moments when Tobias "Four" Eaton first becomes acquainted with Beatrice "Tris" Prior.


=== First jumper–Tris! ===
Four, in his second year as instructor, is waiting for the initiates to jump through the hole into the net of rope alongside his fellow instructor, Lauren, who bets that the first jumper is yet another Dauntless-born initiate. Surprisingly for the two of them, the first jumper is a transfer, and a "Stiff", Abnegation. When he asks for her name, she hesitates and he feels she is familiar to him in some way. She eventually answers "Tris", and Four then proclaims, "First jumper–Tris!".


=== Careful, Tris ===
Four introduces his pupils to the Pit. When the curious Candor transfer, Christina, bogs him on questions about his name and the Pit, he answers coldly in an attempt to make his pupils more serious about Dauntless. During the introduction in dinner, Four sits next to Tris, Christina, Will, and Al. Eric acts friendly to him and dismisses Tris as a "Stiff", prompting Tris to question about Four's old faction. He again responds coldly, but to his surprise, she gives a bold response. He responds back with "Careful, Tris" and leaves, realizing that Tris is the daughter of his father's friend. Andrew Prior.


=== You look good, Tris ===
Four is drunk with Zeke and begins laughing at a silly joke near the Pit. He approaches Tris, who is with her friends, and begins flirting with her. She puts up the defensive, but when Four interjects with "You look good, Tris" while touching her, she laughs and says that he should do that while sober. Four then thinks that being close with Tris helps him better to escape the harsh reality than alcohol.


== References ==


== External links ==
Four: A Divergent Collection on Google Books
Free Four: Tobias Tells the Divergent Knife-Throwing Scene on Google Books
The Transfer: A Divergent Story on Google Books
The Initiate: A Divergent Story on Google Books
The Son: A Divergent Story on Google Books
The Traitor: A Divergent Story on Google Books
Veronica Roth's official website