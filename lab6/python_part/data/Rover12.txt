The Rover 12 was a name given to several medium-sized family cars from the British Rover car company between 1905 and 1948.


== Rover 10-12 (1905-1907) ==
The Rover 10-12 was the fourth model Rover made, following their 8 hp, 6 hp and 16 hp. It was launched in November 1905 and displayed at that year's Olympia Motor Show. It had a four-cylinder, water-cooled, side-valve, monobloc engine of 1767 cc.


== Rover 2-cylinder 12 (1909-1910) ==
The new Rover 12 used a two-cylinder, water-cooled, side-valve engine of 1624 cc. The chassis and rear axle of this car would go on to be used in modified form in the 1912 cars.
Many of these cars saw use as taxis.


== Rover sleeve-valve 12 (1910-1912) ==
A supplementary model of Rover 12 with a Knight-Rover two-cylinder, water-cooled, sleeve-valve engine of 1882 cc with a two-seater body was announced in October 1910 for the Olympia Motor Show though Rover's intention to produce it was published in mid-1910. Daimler engines were at that time cast in pairs of cylinders, this engine was half a four-cylinder 1909 22 hp Daimler.


=== The Knight-Rover engine ===
The connecting rods for the two cylinders are side by side on the same crank pin making firing points equidistant. The engine is mounted directly on the chassis frame. It can be seen from the illustration the rearmost bracket rolls over the flywheel making the rear mountings much further apart from the front set than would be allowed by mounting the engine direct from the very short block. Magneto, distributor and water pump are driven by skew gear from the front of the crankshaft as are the pinions and chains which drive the valve shafts operating the sleeve valves. The two jet carburettor incorporates a silencer on its air intake. The first jet is used only at starting or at idle.
A supplementary air supply controlled by a foot pedal operated at will by the driver provides an oil mist from inside the engine's crankcase to lubricate the tops of the valve sleeves. A three-plunger pump driven by the valve shaft forces oil from the sump to troughs under and in the path of the big ends. With each revolution the big ends dipping into the troughs throw oil to all parts of the engine. The dashboard has a telltale to warn if the pump is not working properly.


=== Chassis ===
The chassis was entirely new. The metal to metal multi-plate clutch requires no lubrication. Two plates are phosphor bronze, the others are steel. There is a double universal joint (a sliding block allows for withdrawal of the clutch) between the clutch and the gearbox. The gearbox is mounted on a separate underframe, all its shafts run on ball bearings. There are three forward speeds. The propellor shaft is fully enclosed and has a ball and fork joint at its forward end. Final drive is by overhead worm.


=== Road test by The Motor ===
The test car was equipped with a four-seated torpedo body. The compact (106 inch) wheelbase left ample legroom for driver and passengers, there was space for luggage behind the front seat in front of the passengers. A spare gallon tin of oil and a tin of petrol was carried on one running board and a picnic hamper was strapped to staples on the running board on the other side, along with a large accumulator to run the electric lamps, all leaving ample room for ingress and egress through either door. "There was a slight tendency (for the rear wheels) to skid on very greasy roads." It was not necessary to add any water to the radiator during the ten days of the test. The Knight-Rover sleeve-valve engine was so silent the driver was often asked if it was running. "Would not hesitate to recommend it". "One of our passengers went off without saying a word and bought a similar car within 24 hours."


=== Road test by The Automotor Journal ===
"A nice smooth running little car". During acceleration there is a low periodic noise as of escaping steam which disappears once under way and the car runs silently. Smooth running requires a very particular adjustment of the ignition and throttle controls. Rover maintains its prejudice against foot operated accelerators. The engine will pick up from standstill on top gear if the clutch is made to slip a little. There is a very powerful clutch stop which requires a gear change to be made in one rapid movement. If the change is missed it will be necessary to stop the car to engage any gear. If Rover fitted an accelerator pedal it would satisfy all opinions.


== Rover Clegg 12 (1912-1923) and 14 (1924) ==
The new Rover 12 was first displayed at the November 1911 Olympia Motor Show as a 4-cylinder supplement to the 2-cylinder sleeve valve cars. It was designed by Owen Clegg who had joined Rover from Wolseley. It had a four-cylinder, water-cooled, side-valve engine of 2297 cc with an SU carburettor made by Rover under licence. Around 5000 were made before World War I. The transmission was three-speed, separate from the engine, and drove the rear axle which had a worm drive. Semi elliptic leaf springs were fitted front and rear. Rear wheel brakes, operated by the hand lever were fitted, with a transmission brake operated by the foot pedal.
Post war, in 1919, the engine got a detachable cylinder head and electric starter. A 6 in (152 mm) longer wheelbase chassis became an option in 1921. Advertisements in 1922 quote open two- and four-seat, saloon, limousine coupé and drophead coupé bodies being available at prices from £625 to £800.
In 1924 the model name was changed to the Rover 14 and a four-speed gearbox fitted although the three-speed remained an option on the open models. There was no change to the engine but the new name was claimed to more closely represent the treasury rating of 13.9 hp. A Weymann fabric bodied saloon was added to the range. The last of the cars was made in 1924 after around 13,000 had been made.


== Trial run 1931-1933 ==


=== Rover Pilot ===
Spencer Wilks was given almost a free hand to make improvements from the time he began his work at Rover. Intending to cater for the market for then fashionable multi-cylindered engines, big cars might have 16-cylinders, Wilks asked Hillman engine designer B H Thomas to design a new small 6-cylinder engine to fit between Rover's Wild designed 10/25 4-cylinder and Poppe's 2 or 2½-litre 6-cylinder. Thomas's design was effectively a 10/25 engine with two more cylinders though dimensions differed to make it fit within the desired tax rating. It would form the basis for Rover's other engines for the 1930s. In its original version the new engine was described as "nice but quite gutless". It was at first squeezed onto the old scarcely altered Rover 8, 9/20, 10/25 chassis.
Rover's new car was announced 21 August 1931 described in advertisements as: a 12-horsepower six-cylinder having a four-speed gearbox with a silent third gear, capable of 65 to 70 mph and 28 to 30 miles per gallon A de luxe model included safety glass in all windows.


=== Road test ===
"The engine exerts its power with an air so modest as to be deceptive" declared Autocar in its report timed to match the release of the Pilot . Gear changing, said Autocar, is easy, it takes carelessness to make any noise. The intermediate gears are "unexpectedly quiet".
Frame revisions
In February 1932 "following a reorganisation of the company's management" Rover announced strengthening of the Pilot and Family Ten chassis by using heavier gauge material and re-designed cross members to improve torsional rigidity. These improvements were, they said, the outcome of lengthy testing on New Zealand's and Australia's roughest roads carried out to make the cars suitable for overseas use. The Pilot scuttle was made wider to give more foot room. The crankcase had been enlarged to provide a larger oil capacity, compression ratio lifted and petrol was now supplied from a new rear tank by Autovac. All Pilot brakes now had a vacuum servo. Springing had also been improved. Safety glass was now standard throughout the Pilot range. There were detail improvements in the de luxe car now trimmed in hide.


=== Speed Pilot and Pilot 14 ===
It was announced in July 1932 that the engine's size had been expanded to 1577 cc and 14 fiscal horsepower. Fitted with three carburettors and with a claimed output of 35 horsepower it was sold as Rover's Pilot 14 and in long wheelbase tuned form Speed Pilot.
Special plant was installed at Rover's works to spray the inside of all body panels with asbestos to ensure quietness fire-proofing etc. and insulation from extremes of heat and cold.
Also in July 1932 the Pilot received the wholly new chassis of the Ten Special, its flexible engine mountings, Startix and the new gearbox and freewheel. The new brakes were Lockheed hydraulic with large diameter drums, they were self-compensating and self-lubricating. The Speed Pilot's special engine had a high efficiency head and a higher compression ratio and a special camshaft. The wheelbase was 8 inches longer.
Radiator cap mascots
Viking head mascots fitted to Pilots and Speed Pilots had the wings on the helmets swept back almost to the horizontal.
Underslung frame
But the chassis frame which previously rose over the back axle was now straightened and set to run beneath the back axle. While this limited suspension movement it allowed lower bodywork with a more sporting appearance.


=== Road test of Pilot 14 ===
Under test by The Times its correspondent wrote that the 1577 cc car's great charm lay in its quietness at all times, furthermore the engine was silky and the intermediate gears produced no hum. Pick-up was good in view of the size of the engine and the weight of the car. The suspension permitted wheel bounce. The clutch pedal was too close to the steering column. Twelve months later a subsequent road test reported that fresh modifications allowed the engine a more spontaneous response. Wheel bounce remained a problem.


=== Success ===
The success of Spencer Wilks' Pilot set the style for Rovers until 1949. The New Deal cars came out twelve months later in late summer 1933.


== Rover 12 P1 (1934-1936) ==

The new 12 launched at the end of August 1933 was part of the new range introduced by the Wilks brothers. It had a new 1496 cc, four-cylinder engine producing 53 bhp. The chassis was also new but based on the one seen on the 1933 Speed Pilot and was "underslung" going under the rear axle. The four-speed transmission featured a freewheel mechanism. The suspension was conventional with rigid axles and half elliptic leaf springs all round. The early cars had a 112 in (2,845 mm) wheelbase but this was stretched by 3 in (76 mm) for 1935 with the extra space giving the rear passengers more leg room. A top speed of 70 mph and economy of 24 mpg was claimed in contemporary advertisements.
The bodies were traditionally built with steel panels fitted to a wooden frame. A six light Saloon (£278), four light Sports Saloon with small external boot (£298) and four-seat tourer (£288) bodies were available. 5775 of this version were made. This car became known to enthusiasts as the P1.


== Rover 12 P2 (1937-1948) ==
An updated version appeared in 1937 with mainly styling changes but the chassis was stiffened and Girling rod brakes replaced the hydraulic ones that had been fitted to earlier cars. There were no more tourers pre war but 200 were made in 1947 and 1948 with the first four bodies by Rover and the remaining 196 by AP Coachbuilders of Coventry. The 1938 models had fixed bonnet sides and for 1939 synchromesh was added to the top two ratios on the gearbox. Disc wheels became an option to wire wheels in 1939 and standard on post war models.
11,786 were made pre war and 4840 after.
This final Rover 12 model was part of the Rover P2 range, along with Rover 10, Rover 14, Rover 16 and Rover 20 variants.
The final cars were made in 1948 and there was no real replacements as subsequent models featured larger engines.


== Notes ==


== References ==