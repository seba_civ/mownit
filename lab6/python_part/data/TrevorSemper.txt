Jaron Trevor Semper (born 7 June 1970) is a former West Indian cricketer. Semper was a right-handed batsman who bowled both right-arm off break and right-arm medium pace. He was born on Montserrat.
In 2003, Semper played club cricket in England for Harlow Cricket Club, while in 2005 he played for Woodford Wells Cricket Club. In 2006, Montserrat were invited to take part in the 2006 Stanford 20/20, whose matches held official Twenty20 status. Semper made his Twenty20 debut for Montserrat in their first-round match against Guyana, with their first-class opponents winning the match by 8 wickets. Semper scored 7 runs with the bat, before he was dismissed by Andre Percival. In Guyana's innings, he took the wicket of Ramnaresh Sarwan to finish with figures of 1/22 from four overs.
Following this appearance for Montserrat, he continued to play club cricket in England for Woodford Wells later in during the 2006 English summer. In January 2008, Montserrat were invited to part in the 2008 Stanford 20/20, where Semper made two further Twenty20 appearances, in a preliminary round match against the Turks and Caicos Islands and in a first round match against Nevis. Against the Turks and Caicos Islands, he took the wicket of Errion Charles, finishing with figures of 2/10 from four overs. He wasn't required to bat in Montserrat's nine wicket victory. Against Nevis, he bowled two expensive overs which conceded 30 runs without a wicket. In Montserrat's unsuccessful chase of 185, he top scored with 35, before being run out by Akito Willett. In 2010, he played one club match in England for North Middlesex Cricket Club.


== References ==


== External links ==
Trevor Semper at ESPNcricinfo
Trevor Semper at CricketArchive