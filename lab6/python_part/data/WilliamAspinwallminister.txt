William Aspinwall (fl. 1648–1662), was a nonconformist English minister.


== Biography ==
Aspinwall was one of the nonconforming ministers ejected in 1662, was of the Lancashire Aspinwalls, and so has a gleam on his name in relation to Edmund Spenser's Rosalind.
He was the son of Matthew Aspinwall, of Trumflet, Kirk Sandall, Yorkshire. He matriculated at Magdalene College, Cambridge in 1654, and had for tutor Joseph Hill. He proceeded B.A. in 1657, but having obtained orders, went no further. His first living was Maghull, in Lancashire. In the Lancashire 'Harmonious Consent' of 1648, which denounces 'endeavours used for the establishing a universal toleration,' his name appears ('William Aspinwal, preacher of God's word at Mayhall') in a long list of signatories, headed by 'Richard Heyricke, warden of Christ Colledg in Manchester,' and including Hollingworth, Alexander Horrocks, John Angier, and indeed the foremost ministers of the county and time. These men had come to persuade themselves that 'the establishing of a toleration would make us [the English people] become the abhorring and loathing of all nations.'
Aspinwall left his cure in 1655–6 to be ordained at Mattersey, Nottinghamshire, and was in that year inducted to Mattersey, in the church at Clayworth, in the same county, along with John Cromwell, B.A., and two others. He was ejected by the Act of Uniformity in 1662. Upon his ejection he turned farmer at Thurnscoe, in Yorkshire. There was 'a good house,' and it became a nonconformist meeting-place. Two other ejected ministers, Tricket and Grant, sojourned with him. Whether farming did not prosper, or the usual persecution drove him away, is uncertain, but in a short time he is traced once more in his native Lancashire. There Calamy states he died; but Samuel Palmer (Nonconformist's Memorial iii. 99) corrects this, and gives extracts from a letter dated Cockermouth, 16 April 1724, by which it would seem that he became minister of a 'dissenting congregation' in that town. The old presbyterian congregation there was afterwards merged in the 'congregational,' but in Lewis's History of the Congregational Church, Cockermouth, being Selections from its own Records (1870), Aspinwall's name nowhere occurs: nor have recent inquiries succeeded in finding the slightest memorial of him in Cockermouth, although the existence of the presbyterian church there has been thoroughly verified. The date of his death is not given.


== Works ==
The following books were published by him:
A Discourse of the Principal Points touching Baptism, so far as Scripture Light directs.
The Legislative Power Christ's Peculiar Prerogative.
A Presage of sundry Sad Calamities yet to come.
The Abrogation of the Jewish Sabbath or the Sabbath of the 7th Day of the Week.
Palmer is strangely inaccurate in the following addition to Calamy: 'There is a small folio volume of sermons on the whole Epistle of Paul to Philemon, with the name of William Aspinwall prefixed, which the editor supposes to be by the same person. It is a valuable work' (Nonconf. Mem. iii. 100). 'Valuable' certainly; but it does not consist of 'sermons,' and the author was not Aspinwall, but William Attersoll. Our William Aspinwall (as also Peter Aspinwall, of Heaton, Lancashire) is sometimes confounded with William Aspinwall, the ejected minister of Formby, who afterwards conformed, as well as with a contemporary quaker divine (of the same names) who had been persecuted in New England, and wrote vehemently of his wrongs and tenets.


== References ==

 "Aspinwall, William". Dictionary of National Biography. London: Smith, Elder & Co. 1885–1900.