Andrew Hopper Q.C. is a British solicitor who practises on his own account in a niche practice concerned with professional regulation and discipline, principally in relation to solicitors.
He first acted for a solicitor before the Solicitors Disciplinary Tribunal in 1975; between 1979 and 2002 he advised and acted for the Law Society in disciplinary and regulatory proceedings, in matters before the Tribunal, the Divisional Court, the Court of Appeal and before the Master of the Rolls, and was the principal prosecutor in the Tribunal for over a decade.
He now has an exclusively defence practice, but also acts for the Solicitors Disciplinary Tribunal itself when it becomes involved as a party to litigation, principally in claims for judicial review.
Hopper is joint general editor of Cordery on Legal Services,the principal authority on the law and practice affecting the regulation of the supply of legal services, including all aspects of the professional obligations and liabilities of solicitors and the other legal professions, and is co-author of the Solicitor’s Handbook 2008, 2009, 2011 and 2012.
Hopper was admitted as solicitor in 1972, was appointed Queen's Counsel (Q.C.) in 2001, being the fifth solicitor appointed as QC. According to Chambers and Partners Hopper is "the oracle of all things relating to solicitors’ disciplinary proceedings".
Hopper advised Andrew Crossley of ACS:Law who was referred to the Solicitors Disciplinary Tribunal in relation to claims against people suspected of copyright infringement using peer-to-peer file sharing.


== References ==