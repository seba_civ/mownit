Hans-Adam II (Johannes Adam Ferdinand Alois Josef Maria Marco d'Aviano Pius; born 14 February 1945) is the reigning Prince of Liechtenstein. He is the son of Franz Joseph II, Prince (Fürst) of Liechtenstein (1906–1989) and his wife Countess Georgina von Wilczek (1921–1989). He also bears the titles Duke of Troppau and Jägerndorf, and Count Rietberg.


== Powers ==
A referendum to adopt Hans-Adam's revision of the constitution to expand his powers passed in 2003.
On 15 August 2004 Prince Hans-Adam II formally turned the power of making day-to-day governmental decisions over to his eldest son, the Hereditary Prince Alois, as a way of beginning a dynastic transition to a new generation. Legally, Hans-Adam remains Head of State.
In July 2012 the people of Liechtenstein overwhelmingly rejected a proposal to curtail the political power of the princely family. Despite an almost year-long campaign by those who proposed the changes, 76% of those voting in a referendum supported the Prince's power to veto the outcome of future referendums. Legislators, who serve on a part-time basis, rose in the prince's defence on 23 May, voting 18 to 7 against the citizens' initiative. Prince Hans-Adam reacted to the result: "It is with joy and gratitude that the Princely House of Liechtenstein has taken note that a large majority of the population would like to continue the hitherto so successful 300-year partnership between the people and the Princely House." The prince had threatened to quit the country if the 2003 referendum did not result in his favor.


== Personal wealth ==
Prince Hans-Adam owns LGT banking group and has a family fortune of $7.6 billion and a personal fortune of about $4.0 billion, making him one of the world's richest heads of state, and Europe's wealthiest monarch. He owns an extensive art collection, much of which is displayed for the public at the Liechtenstein Museum in Vienna.


== Personal life ==
Hans-Adam descends in the direct male line from three of the previous fourteen Princes of Liechtenstein, and from another three in the female line. His native language is German, but he is also fluent in English and French.
On 30 July 1967, at St. Florin's in Vaduz, he married his second cousin once-removed, Countess Marie Aglaë Kinsky von Wchinitz und Tettau (born 1940) who, upon her husband's accession to the throne, became Her Serene Highness The Princess of Liechtenstein. Their official residence is at Vaduz Castle, which overlooks the capital.
They have four children and 15 grandchildren:
Hereditary Prince Alois (b. Zürich, 11 June 1968) He married Duchess Sophie of Bavaria on 3 July 1993, 4 children:
Prince Joseph Wenzel Maximilian Maria of Liechtenstein, Count Rietberg (born 24 May 1995 in London)
Princess Marie-Caroline Elisabeth Immaculata of Liechtenstein, Countess Rietberg (born 17 October 1996 in Grabs, Switzerland)
Prince Georg Antonius Constantin Maria of Liechtenstein, Count Rietberg (born 20 April 1999 in Grabs)
Prince Nikolaus Sebastian Alexander Maria of Liechtenstein, Count Rietberg (born 6 December 2000, in Grabs)

Prince Maximilian of Liechtenstein (b. St Gallen, 16 May 1969); He married Angela Gisela Brown civilly in Vaduz on 21 January 2000 and religiously in New York City, New York, in the Church of St. Vincent Ferrer, on 29 January 2000, 1 child:
Prince Alfons Constantin Maria (b. London, 18 May 2001)

Prince Constantin Ferdinand Maria (b. St Gallen, 15 March 1972), married civilly in Vaduz on 14 May 1999 and religiously in Číčov, Slovakia, on 18 July 1999 Countess Marie Gabriele Franziska Kálnoky de Kőröspatak (b. Graz, 16 July 1975), 3 children:
Prince Moritz Emanuel Maria (b. New York City, 27 May 2003)
Princess Georgina Maximiliana Tatiana Maria (b. Vienna, 23 July 2005)
Prince Benedikt Ferdinand Hubertus Maria (b. Vienna, 18 May 2008)

Princess Tatjana Nora Maria (b. St Gallen, 10 April 1973), married in Vaduz on 5 June 1999 Matthias Claus-Just Carl Philipp von Lattorff (b. Graz, 25 March 1968), 7 children:
Lukas Maria von Lattorff (b. Wiesbaden, 13 May 2000)
Elisabeth Maria Angela Tatjana von Lattorff (b. Grabs, 25 January 2002)
Marie Teresa von Lattorff (b. Grabs, 18 January 2004)
Camilla Maria Katharina von Lattorff (b. Monza, 4 November 2005)
Anna Pia Theresia Maria von Lattorff (b. Goldgeben, 3 August 2007)
Sophie Katharina Maria von Lattorff (b. Goldgeben, 30 October 2009)
Maximilian Maria von Lattorff (b. Goldgeben, 17 December 2011)

In 1969, Hans-Adam graduated from the University of St. Gallen with a Licentiate (equivalent to a master's degree) in Business and Economic Studies.
The Prince is an honorary member of K.D.St.V. Nordgau Prag Stuttgart, a Catholic students' fraternity that is a member of the Cartellverband der katholischen deutschen Studentenverbindungen. The Prince donated $12 million in 2000 to found the Liechtenstein Institute on Self-determination (LISD) at Princeton University's Woodrow Wilson School of Public and International Affairs. In his childhood he joined the Pfadfinder und Pfadfinderinnen Liechtensteins in Vaduz. He is also a former member of the Viennese Scout Group "Wien 16-Schotten". He is a member of the World Scout Foundation.
Today he and his wife are patrons of Pfadfinder und Pfadfinderinnen Liechtensteins.
He is the 1,305th Knight of the Order of the Golden Fleece in Austria.


== Viewpoints and book ==
Prince Hans-Adam II has written the political treatise The State in the Third Millennium (ISBN 9783905881042), which was published in late 2009. In it, he argues for the continued importance of the nation-state as a political actor. He makes the case for democracy as the best form of government, which he sees China and Russia as transitioning towards although the path will be difficult for these nations. He also declared his role in a royal family as something that has legitimacy only from the assent of the people. He stated that government should be limited to a small set of tasks and abilities, writing that people "have to free the state from all the unnecessary tasks and burdens with which it has been loaded during the last hundred years, which have distracted it from its two main tasks: maintenance of the rule of law and foreign policy.”

In an interview, recorded in November 2010, Hans-Adam said that he saw certain problems with aspects of the US Constitution, such as the lack of direct democracy. This statement came across as ironic, considering the expansion of monarchical powers during his reign. He also said, "I am sitting here and that's because Americans saved us during World War II and during the Cold War. So I am very grateful to them."
Prince Hans-Adam II offered a major contribution to the study of self-determination in the Foreword to a "Sourcebook, on Self-Determination and Self-Administration," edited by Wolfgang F. Danspeckgruber and Sir Arthur Watts, ISBN 1-55587-786-9, 1997; and in the Encyclopedia Princetoniensis.
In September 2011, ahead of a referendum on decriminalising abortion, Alois announced that he would veto any relaxation of the ban on abortion, whatever the voters decided in the referendum. In Liechtenstein, women wanting to end a pregnancy have to travel to neighbouring Germany or Austria.


== Titles, styles and honours ==


=== Titles and styles ===
14 February 1945 - 13 November 1989: His Serene Highness The Hereditary Prince of Liechtenstein
13 November 1989 - present: His Serene Highness The Prince of Liechtenstein
The official title of the Prince is Fürst von und zu Liechtenstein, Herzog von Troppau und Jägerndorf, Graf zu Rietberg, Regierer des Hauses von und zu Liechtenstein. (Prince of and at Liechtenstein, Duke of Troppau and Jägerndorf, Count at Rietberg, Sovereign of the Houses of and at Liechtenstein) There is a distinction between the German titles of a reigning Fürst and non-reigning descendants of a Fürst who are titled Prinz.


=== Honours and awards ===


==== National honours ====
 Liechtenstein: Sovereign Knight Grand Cross of the Order of Merit of the Principality of Liechtenstein, Grand Star
 Liechtenstein: Sovereign Recipient of the 70th Birthday Medal of Prince Franz Joseph II


==== Foreign honours ====
Austria
 Austrian-Hungarian Imperial and Royal family: 1,305th Knight with Collar of the Order of the Golden Fleece
 Austria: Grand Cross of the Decoration of Honour for Services to the Republic of Austria, Grand Star

 Bavarian Royal Family: Knight Grand Cross of the Order of Saint Hubert


=== Awards ===
 Austria: Honorary degree of the University of Innsbruck
 Romania: Honorary degree of the Babeș-Bolyai University


== Ancestry ==


== See also ==

Line of succession to the Liechtensteiner throne
List of monarchs of Liechtenstein
Prince of Liechtenstein Foundation
Princely Family of Liechtenstein


== References ==


== External links ==
Official website
2004: Royal power handover – Prince Alois says democracy still strong Real Audio sound file from the BBC.
Interview with Peter Robinson of Uncommon Knowledge
[2]