Area codes 306 and 639 are the telephone area codes in the Canadian province of Saskatchewan, encompassing the province. The incumbent local exchange carrier in 306 and 639 is SaskTel.
The main area code, 306, is one of the original 86 area codes assigned in 1947, in the contiguous United States and the then-nine-province extent of Canada. Saskatchewan's second area code, 639, was introduced in 2012, and is an overlay of 306.
Despite Saskatchewan's small population, 306 was on the verge of exhaustion due to Canada's inefficient number allocation system. All competitive local exchange carriers are allocated exclusive access to at least one three-digit prefix – comprising 10,000 numbers each – for every rate centre where they plan to offer service, even for the smallest hamlets. While each competing carrier did not necessarily need that many numbers in a particular market, unused numbers were not available for use elsewhere once they were assigned to a particular carrier and rate centre. Since number pooling is not used as a relief measure in Canada, this resulted in thousands of wasted numbers. The number shortage was exacerbated by the proliferation of cell phones and other mobile devices requiring unique telephone numbers, particularly in Regina and Saskatoon.
Unlike what was previously done in jurisdictions such as neighbouring Alberta, It was decided to implement the new area code as an overlay rather than a split. Overlays have become the preferred method of area code relief in Canada; no area codes have been split in the country since 1999. While this had the effect of allocating 15.8 million numbers to a province of just over a million people, SaskTel and other carriers wanted to spare existing subscribers, particularly in rural areas, the burden and expense of changing their numbers. 10-digit-dialing began in 2012, and became mandatory throughout the province in May 2013. Until the implementation of 639, Saskatchewan was the last of Canada's original NPAs where seven-digit dialling had not been broken.


== Communities included ==
Arborfield: (306) - 769
Balgonie: (306) - 702 771
Beauval: (306) - 288
Borden: (306) - 997
Carrot River: (306) - 768 401
Central Butte: (306) - 796
Dalmeny: (306) - 254
Dog River: (306) - 776
Esterhazy:(306) - 701 745
Estevan: (306) - 308 339 340 415 421 461 471 634 636 637 687, (639) - 450 534
Foam Lake: (306) - 272
Fort Qu'Appelle: (306) - 331 332 804 900
Francis: (306) - 245
Humboldt: (306) - 231 289 320 366 367 368 598 682 944, (639) -535
East Lloydminster: (306) - 307 603 820 821 825 830
Grayson & Crooked Lake: (306) - 794
Kamsack:(306) - 542 591
Kindersley: (306) - 379 430 460 463 512 604 806 838 965 967 968, (639) - 539
Kinistino: (306) - 864
Lloydminster: (306) - 214 307 603 808 820 821 825 830 875, (639)- 536 840
Lumsden: (306) - 731, (639)-392
Meadow Lake: (306) - 234 236 240 304 819, (639) -537
Meath Park: (306) - 929
Melfort: (306) - 275 277 346 752 863 920 921
Melville: (306) - 607 705 707 720 728 730 748 760
Milestone: (306) - 436, (639) - 440 450
Mistatim: (306) - 889
Moose Jaw: (306) - 214 313 556 624 630 631 681 684 690 691 692 693 694 704 756 905 972 983 990, (639) - 538
Nipawin:(306) - 862
North Battleford: (306) - 208 317 386 407 440 441 445 446 470 480 481 490 499 817 906 937
Pilot Butte, Saskatchewan: (306) - 584 586 781
Porcupine Plain, Saskatchewan: (306) - 278
Prince Albert: (306) - 314 557 703 763 764 765 904 922 930 940 941 953 960 961 970 980 981 987, (639)- 314 533 760
Prud'homme: (306) - 654 (639) - 653
Radisson: (306) - 827, (639)-913
Regina: (306) - 201 205 206 209 216 337 347 351 352 359 450 501 502 503 509 510 517 519 520 522 523 525 526 527 529 530 531 533 535 536 537 539 540 541 543 545 546 550 551 552 559 564 565 566 569 570 580 581 584 585 586 591 596 719 721 737 751 757 761 766 775 777 779 780 781 787 789 790 791 797 798 807 908 910 924 926 936 949 988 992 993 994 995 999, (639)-221 316 444 528 560 571 590 600 601 602 603 604 605 606 607 608 609 625 640 641 642 643 644 645 646 647 648 649 739 740 888 915 955 997 999
Rocanville: (306) - 645
Rose Valley: (306) - 322
Rouleau: (306) - 776, (639)-394
Saskatoon: (306) - 200 202 203 220 221 222 227 229 230 241 242 244 249 250 251 260 261 262 270 280 281 290 291 292 321 341 343 361 370 371 373 374 380 381 382 384 385 477 491 514 518 612 649 651 652 653 655 657 659 664 665 667 668 683 700 713 715 716 717 803 844 850 866 875 880 881 899 902 912 914 931 933 934 938 952 955 956 964 966 974 975 977 978 979 985 986 996 998 (639)-220 317 470 471 480 630 635 887 916 996 998
Sheho: (306) - 849
St. Isidore-de-Bellevue: (306) - 423
Strasbourg: (306) - 725, (639)-914
Swift Current: (306) - 315 437 712 741 750 770 772 773 774 778 816 907 971 973, (639) - 541
Tisdale: (306) - 873
Watrous: (306) - 917 946, (639)-395
Weyburn: (306) - 405 504 509 809 842 848 861 869 870 891 897 908
Wynyard: (306) - 554
Yorkton: (306) - 316 521 620 621 641 708 782 783 786 818 828 890 909, (639) - 540


== See also ==
List of North American Numbering Plan area codes


== External links ==
NANPA planning letter PL-439 with diagrams and maps showing application of 639.
CNA exchange list for area +1-306
CNA exchange list for area +1-639
Telecom archives
Area Code Map of Canada


== Citations ==