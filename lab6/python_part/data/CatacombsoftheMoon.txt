"Catacombs of the Moon" is the eleventh episode of the second series of Space: 1999 (and the thirty-fifth overall episode of the programme). The screenplay was written by Anthony Terpiloff; the director was Robert Lynn. The original title was "The Catacombs of the Moon". The final shooting script is dated 18 May 1976, with amendments dated 26 May, 9 June, 14 June and 17 June 1976. Live-action filming took place Monday 21 June 1976 through Tuesday 6 July 1976. Production was halted for two days (starting 2 July) when the fire effects used in the Osgood vision sequences got out of hand and the fire brigade had to be called in.


== Story ==
It is 1196 days after leaving Earth orbit, and geologist Patrick Osgood leads a mining team through the network of caves beneath the Alpha complex. Their mission is to find tiranium, the rare mineral which is an essential component of the Moonbase life-support system. Osgood orders an excessive amount of explosive charges deployed around a particular rock formation to hasten the operation. When his team disagrees with this dangerous short-cut, Osgood storms off to do it himself. The tiranium search has personal meaning for the angry geologist—the mineral also has medical applications and is needed to complete an artificial heart for his dying wife, Michelle.
There is a malfunction; the hyper-nitro charges detonate prematurely and Osgood is caught in the blast. Semi-conscious, he drifts into the latest in a series of recurring nightmare-visions he has been having—where Moonbase Alpha is destroyed by a raging fire. The visions begin with the same image...Michelle Osgood, lying helpless in a canopy bed out on the lunar surface. As her husband watches, blazing energy rains down from space and the surrounding Alpha buildings burst into flame. The bed is rapidly encircled by a wall of fire. Unable to move, the frantic Osgood shouts for her to rise and save herself. She struggles, but is too weak. Michelle and the bed are then consumed by the inferno...
When Osgood recovers from this altered state, he is told by his team that there is still no sign of tiranium present. Disgusted with this latest failure, he proceeds to the Medical Centre, where Michelle is confined to bed. The geologist barges in, demanding a private visit with his wife. Always a religious man, he has become a fanatic since Michelle's diagnosis of refractory heart disease. He recounts to her his visions of all Alpha perishing in flame. She will be saved along with him...but only if she has complete and utter faith in him.
As if fulfilling Osgood's prophecy, the Moon travels into an area of intense heat. Even with full-strength air-conditioning, temperatures in the surface buildings soon average a sweltering 40° Celsius. With instruments unable to determine a cause, John Koenig takes Eagle One on a reconnaissance mission to locate the source of this 'heat-wave'. After his departure, Helena tests the artificial heart for Michelle—the tenth to date. The Dorfman cardiac prosthesis, perfected in 1987, was designed to have valves coated with tiranium. However, the Alphans' supply of tiranium is low and none can be spared for this project. Forced to use less-than-adequate substitutes, Helena is battling her own desperation and the cynicism of her collaborator, Doctor Ben Vincent, as attempt after attempt fails.
Osgood joins them, to behold this latest 'miracle of science'. The heart is activated, functions erratically, and fails. Osgood denounces these scientific attempts to give life as blasphemy. After he exits, the doctors debate whether his growing instability is the sign of a breakdown...or, in view of his recent prophetic visions, a more metaphysical influence. Osgood returns to the care unit to visit his now-sleeping wife. At her bedside, he is inspired by the recollection of a past conversation of theirs, where the secular-minded Michelle commented her faith is in their love. After formulating a plan of action, he leaves, convinced he will be her saviour.
From deep space, Koenig checks in. Like Alpha, his instruments record no cause for the ever-rising temperatures. He gives Tony Verdeschi command authority in his absence (as the ship will soon pass out of communications range). Helena interrupts to plead Michelle's case; all she needs are a few grains of tiranium to complete the heart. Koenig refuses. With the strain placed on life-support by the present emergency, the scarce mineral is more valuable than a single life. Outwardly, Koenig expresses his confidence in Helena and her abilities to find an alternate solution—inside, he is wracked with guilt.
The temperature in Alpha continues to rise, reaching 45° Celsius on Level A. Verdeschi orders all supplies of the heat-sensitive hyper-nitro relocated to the relatively cooler storerooms on the lowest subterranean level. During the transfer, Osgood unexpectedly charges in. The man is obviously distraught and Verdeschi tries to sympathise with his old friend. Obsessed with his role as prophet, Osgood will only discuss his visions. The security chief tries to humour him, but the manic geologist lashes out. He lays out the guards and grabs Verdeschi's sidearm.
A passing Maya witnesses the scuffle. She transforms into a large Alsatian dog and tears at Osgood's arm with her fangs until he drops the gun. Wounded, the fanatic runs off. Evading the guards, he gains access to an explosives storeroom on another level. Verdeschi stations sentries throughout the Medical Section, figuring the geologist will try to approach his wife. While visiting the patient, he turns on the charm, downplaying Osgood's irrational behaviour to spare the weakening Michelle any further stress. Verdeschi relates the details of his encounter with Osgood to Helena. While doing so, he questions why Osgood was at the A-Level explosives storeroom in the first place and orders Security to place guards around the other storage facilities.
Osgood is discovered in a woman's quarters, bloodied and half-conscious. While conveyed by stretcher team to Medical, he experiences another vision...Michelle, lying in the canopy bed on the lunar surface, surrounded by fire. This time, he can move and tries to run to her aid—but finds himself making no headway. As before, she is too weak to rise and she perishes in the flames...
Helena tests an eleventh model of the Dorfman heart, utilising an alloy that preceded the use of tiranium—with only limited success. As expected, it fails to function for any significant length of time. Despondent, she is called away to casualty reception. There, she finds Osgood, with multiple lacerations on his arm and weak from blood loss. As she examines him, the geologist reveals a belt of explosive charges (stolen from the explosives storage room) strapped around his torso—if he releases the detonator pin gripped in his hand, the charges explode. He allows Helena to bind his wounds, then demands to see Michelle. He intends to flee with her to the catacombs...before Moonbase is destroyed in the coming holocaust.
Knowing that any physical exertion could kill Michelle, Helena begs him not to take her. A new heart is her only salvation and the doctor promises to get tiranium for it. Osgood rejects her heresy, and husband and wife leave the care unit together. In space, Eagle One encounters a dark nebula. After passing through and out the other side, Koenig and crew make visual contact with the heat source—an enormous disc of super-heated plasma and flaming hydrogen madly spinning around a gravitational centre. The fire-storm is on a trajectory for the Moon, travelling faster than the Eagle's maximum speed.
Reversing course, Koenig radios in the horrifying forecast—the plasma-storm will impact on the lunar surface in the vicinity of Alpha. Praying a working heart might make Osgood see reason, Helena makes one last attempt to persuade Koenig to release tiranium. His reply is lost, as storm interference severs communications. She turns to Verdeschi for a decision. He allows her to take what she needs, half-joking that since Osgood's apocalyptic vision is coming true, they will have no need for it. When Michelle's hospital name-plate is discovered deep in the caverns, Verdeschi goes underground to hunt for his friends.
Intimately familiar with the catacombs, Osgood manages to keep ahead of the search parties. But his exhaustion combined with Michelle's debilitated condition has them resting too frequently. He removes the belt of explosives for fear of blowing them up by accident. Finally, Osgood is half-carrying, half-dragging his nearly dead wife and collapses. He slips into the trance-state...Michelle is again trapped in the ring of fire. Osgood wills her to save herself. This time, she rises to her feet, drawing strength from their love. He strides through the flames unharmed and, taking her in his arms, they walk safely out of the inferno...
Heart number twelve is constructed with tiranium-coated valves. The test apparatus is switched on and the prosthesis functions perfectly. Literally crying with relief, Helena calls Verdeschi with the news and implores him to find Michelle. As he proceeds, the cavern is rocked by a tremendous blast; a super-heated plasma flare spun off from the fire-storm has impacted on the surface, setting an Alpha building ablaze. The storm has already passed Koenig's ship and is bearing down on the Moon. Between the heat and the barrage of plasma streams, Moonbase is sustaining considerable damage.
The surface explosions cause rock falls and cave-ins in the catacombs, hindering the search. Verdeschi finally sights the fugitives; when he shouts out the news of the heart, Osgood smashes the security chief's hand-torch with a well-aimed rock and the couple slips away in the darkness. Osgood's knowledge of the cave network gives him a distinct advantage. To even the odds, Verdeschi calls Maya down into the catacombs. She transforms into a Bengal tiger and guides the search party through the tunnels with her night-vision.
Michelle can go no further and crumples to the ground. The Maya/Tiger corners Osgood while Verdeschi attends to the girl. A blast brings down the cavern ceiling on top of the would-be prophet. Sending Michelle back with Maya and the guards, Verdeschi begins the long process of digging his friend out from under the rock fall. The girl is rushed to the operating theatre, where Helena performs the procedure under less-than-ideal conditions. In spite of the perfectly functioning heart, Michelle's condition remains critical. With her husband missing, she has lost the will to live.
When Verdeschi enters the care unit with a stretcher bearing a battered and bloodied Osgood, Michelle's vital signs improve. Husband and wife exchange a loving glance, then Osgood looks ceilingward and declares, 'It's over.' In his mind's eye, he sees the plasma fire-storm inexplicably veer away from the Moon and speed off into space.
With the temperature back to normal and damage repairs underway, Helena unwinds by watching Verdeschi manipulate his home-made brewing apparatus. He toasts the recovery of Mr and Mrs Osgood with a glass of his latest creation. She abstains, musing on the events of the past few days. They speculate if some sort primal intelligence existed in the fire-storm, it could have somehow made contact with Osgood's unbalanced mind. The storm's behaviour could have been influenced by the geologist's subconscious impulses; if so, this communion could also explain Osgood's temporary gift of clairvoyance.
Eagle One returns and Koenig contacts Helena on touchdown. Welcoming him home, she cheerfully thanks him for the tiranium she used. He is initially surprised by this unexpected confession. Her obvious delight at having saved Michelle's life defuses the situation though, and Koenig returns the smile of the woman he loves.


== Cast ==


=== Starring ===
Martin Landau — Commander John Koenig
Barbara Bain — Doctor Helena Russell


=== Also Starring ===
Catherine Schell — Maya


=== Featuring ===
Tony Anholt — Tony Verdeschi
Zienia Merton — Sandra Benes


=== Guest Stars ===
James Laurenson — Patrick Osgood
Pamela Stephenson — Michelle Osgood


=== Also Featuring ===
Jeffery Kissoon — Doctor Ben Vincent
Lloyd McGuire — First Mining Engineer
Brendan Price — First Security Guard (Morgan)
Alan Hunter — Eagle One Co-Pilot (Bill)
Nova Llewellyn — First Alphan Woman


=== Uncredited Artists ===
Saul Reichlin — Second Mining Engineer
Karen Ford — Nurse
Felicity York — Second Alphan Woman
Robert Reeves — Peter
Quentin Pierre — Second Security Guard
Jenny Cresswell — Command Centre Operative


== Music ==
The score was re-edited from previous Space: 1999 incidental music tracks composed for the second series by Derek Wadsworth and draws primarily from the scores of "The Exiles" and "One Moment of Humanity".


== Production Notes ==
"Catacombs of the Moon" is a 'Helena Double-Up' script and was produced simultaneously along with "The AB Chrysalis". Series regulars Barbara Bain, Tony Anholt and Zienia Merton carried the majority of the action. Martin Landau's brief scenes were confined to the Eagle command module; his only interaction with Bain occurred via TV monitor in the epilogue. Catherine Schell's two small contributions amounted to a cameo appearance. The episode is set primarily in the standing Alpha sets with moderate filming in the Stage 'M' cavern sets originally built for the "The Metamorph".
This episode featured the return of Zienia Merton after a three-month absence; the actress had left the programme after filming "One Moment of Humanity" because of her reduced involvement (sometimes only a half-day's work) and her new non-contracted 'day-player' status. Merton recounts that Barbara Bain had telephoned and asked if she would consider meeting with Gerry Anderson and Fred Freiberger to discuss her returning to the series. Bain's motivation was that she and Martin Landau would be on holiday for the majority of the shooting of an uncoming episode ("The Beta Cloud"). As this would result in a very brief appearance by the series' stars, the Landaus feared long-term fans would be alienated when tuning in and not seeing any familiar faces from the first series. Guaranteed a more involved role in the series (though still no contract), Merton accepted the offer. From this time forward, her character was referred to on-screen only by the diminutive 'Sahn' (sometimes 'Sahn Benes' in script character lists).
In the shooting script dated 18 May 1976, Koenig's co-pilot, Bill, was supposed to be Astronaut Bill Fraser. Budgetary limitations led to John Hug's replacement with one-off actor Alan Hunter. Early drafts of the script had Helena and Ben Vincent referring to their prosthesis as the 'Bergman' heart, as it was identical to the mechanical heart implanted in the late Victor Bergman. One line of dialogue read 'Victor might have lived forever—given the chance.'


== Novelisation ==
The episode was adapted in the second Year Two Space: 1999 novel Mind-Breaks of Space by Michael Butterworth and J. Jeff Jones published in 1977. The authors made minor changes in the story, the most obvious being the two Maya transformation sequences feature decidedly extraterrestrial animals: a long-bodied beast with natural armour plating and trap-like mouth would come to Verdeschi's aid in the explosives storeroom and later, a headless, bipedal bat-creature would use its sonar to navigate the Security team through the darkness of the catacombs. The novel would also give Koenig and Maya more exposure than did the actual shooting script and transformed the character of 'Sahn' into a young Indian man.


== References ==


== External links ==
Space: 1999 - "Catacombs of the Moon" - The Catacombs episode guide
Space: 1999 - "Catacombs of the Moon" - Moonbase Alpha's Space: 1999 page