The Four Lords of the Diamond is a series of four science fiction novels by author Jack L. Chalker. Each volume of the series primarily follows a duplicate of a government agent as he lands on his prison planet and begins to both investigate the menace to the civilized worlds and find his position in his new society. The duplicates realize the stagnancy and corruption of the Confederacy, the intergalactic government in the series, and question their position as tools of the hierarchy. As the series progresses, the primary agent experiences each of his counterparts' divergent experiences and begins to question his beliefs as well. Like much of Chalker's work, the series deals with the effects physical transformations have on a character's personality.


== Plot ==
The Warden Diamond is a system of four planets, ruled by their own lords, collectively called "The Four Lords of the Diamond". Each planet of the Diamond has its own special "Warden Organism", a symbiotic microorganism that lives within the inhabitants of the planets. However, the organisms destroy their host when he or she leaves the Warden Diamond, making the planet system the ideal prison colony for the Confederacy, a massive space empire.
An android clone that successfully infiltrated a government facility on a central world and downloaded vital defense information, transmits the information to a source and is destroyed in the process. The Confederacy discovers and tracks the clone towards the Warden Diamond, whose four lords are cooperating with an alien race to plan a mutiny against the Confederacy.
The government sends its best agent to investigate. Through technological advances, the government duplicates the personality of the agent (who remains unnamed throughout the series) and implants "him" into four brain-dead host bodies. The four hosts are then sent to four different planets in the Warden system and have no choice but to fulfill their assignment of locating and defeating each of the Four Lords, delaying the expected alien invasion and finding out vital information on the infiltrators.


== Style ==
Jack L. Chalker's style in the writing of this four book series is that of formula fiction of itself, in that it extensively copies its narrative from book to book, even word for word. Each book opens with a short story about some way the aliens are disrupting the Confederacy, then shifts to the background story of the Confederacy learning of the aliens and of the agent being recruited, briefed and awakening on the prison ship. The story of that is identical in each book. From there, it is, in the first three books, purely an adventure story involving the copied agent trying to assassinate the local Lord, getting a girlfriend and learning a lot about himself in the process. Each of the first three books then close with a discussion between the Agent in the picket ship and his computer partner/overseer. Only the concluding book, Medusa: A Tiger by the Tail, departs a bit from this.


== Volumes ==
Lilith: A Snake in the Grass (Del Rey 1981, ISBN 0-345-29369-X)
Cerberus: A Wolf in the Fold (Del Rey 1981, ISBN 0-345-31122-1)
Charon: A Dragon at the Gate (Del Rey 1982, ISBN 0-345-29370-3)
Medusa: A Tiger by the Tail (Del Rey 1983, ISBN 0-345-29372-X)


== References ==