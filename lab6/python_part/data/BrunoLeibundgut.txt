Bruno Leibundgut (* 1. April 1960 ) is a Swiss astronomer born in Basel. He has become well known especially because of discoveries regarding the Supernova Cosmology. He was a member of the High-z Supernova Search Team and participated in the establishing of the Very Large Telescope.


== Career ==
Leibundgut finished his PhD at the University of Basel in 1988 with the English written paper Light Curves of Supernovae Type I. He is the author or co-author of dozens of scientific papers dedicated for example to the supernovae or the universe in general.
Till March 2013 he worked as the Science Director of the ESO. Since then he has been having his sabbatical travelling around Europe holding lectures about the formation of the elements after the Big Bang, modern astronomical observatories or the structure of the universe.


== Awards ==
2007: Gruber Prize in Cosmology (co-recipient with High-z Supernova Search Team)
2011: Nobel Prize in Physics was awarded to Leibundgut's colleagues Brian P. Schmidt and Adam Riess, from the High-z Supernova Search Team for the work done by that collaboration.
2015: Breakthrough Prize in Fundamental Physics, shared with Brian P. Schmidt, Adam Riess, and the High-Z Supernova Search Team.


=== References ===