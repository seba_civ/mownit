RAF Usworth was a Royal Air Force station near Sunderland. In 1958 the station was closed and the airfield became Sunderland Airport. Following the closure of the airport in 1984, the site has since been redeveloped as a manufacturing facility for Nissan cars.


== History ==


=== Early history ===
In October 1916, the airfield that subsequently became Sunderland Airport started as a Flight Station for 'B' Flight of No. 36 Squadron of the Royal Flying Corps (RFC), and was originally called Hylton (after nearby Hylton Castle), although when being prepared it was known as West Town Moor. Due to an increase in German bombing raids and the heavier commitment of Royal Naval Air Service (RNAS) aircraft in France, the Royal Flying Corps was given the task of Home Defence, setting up a number of squadrons, with flights spread over the length of the British coastline.
The coast in North East England between Whitby and Newcastle was protected by No. 36 (Home Defence) Squadron, equipped with the B.E.2c and B.E.12. On 1 February 1916, the squadron was formed at Cramlington, outside Newcastle, commanded by Captain R. O. Abercromby. In addition to the main aerodrome, flights were detached to Seaton Carew and Ashington as well as Hylton.
An area of land just north of the River Wear between Washington and Sunderland was set aside for the new landing field.
36 squadron was tasked with the defence of the coast between Whitby and Newcastle. On 27 November 1916, a patrol of B.E.2cs flying from Seaton Carew intercepted two groups of Zeppelin airships over the North East coast. Lieutenant I.V. Pyrott destroyed LZ34 that crashed into the mouth of the Tees; the sight of this caused the other airships to turn back. The only other action that the squadron was involved in was the unsuccessful attack on Zeppelin L42 over Hartlepool on New Years Day 1918.
After the end of the First World War, the area around Usworth was returned to non-flying use. In the manner of many First World War aerodromes, Usworth languished unused for over a decade, apart from at least one visit by Alan Cobham's Flying Circus, until being re-activated on 17 March 1930.


=== The 1930s ===
The newly re-opened airfield was sited alongside the B1289 road between Washington and Sunderland, with the flying field to the south of the road. The South Camp also housed the Squadron Office, pilots huts, armoury, photographic hut, bombing training aids, and the firing butts alongside the railway. The airfield had been designed to accommodate one squadron of the recently expanded Auxiliary Air Force. This was to be No.607 (County of Durham) Bomber Squadron. It was initially proposed to erect canvas-covered Bessonneau hangars at the South Camp, but these were rejected in favour of the erection of one large Lamella hangar. North Camp was provided with living quarters and dining facilities for Officers, NCOs and airmen.
In September 1932, the airfield was ready to receive personnel under the command of Leslie Runciman, later Viscount Runciman. The Auxiliary pilots and groundcrew of No 607 squadron came from all walks of life locally and were trained by a nucleus of regular RAF personnel, including 2 Qualified Flying Instructors. The following month the first aircraft, a DH Gipsy Moth and two Avro 504N trainers, arrived for flying training to commence. In December 1932, the first operational equipment for the squadron, the Westland Wapiti day bomber, arrived. Evenings and weekends were busy when the part-time airmen turned out for training. Training continued in earnest, and in June 1934 the squadron proudly flew nine of its Wapiti aircraft in formation past its first Honorary Air Commodore, the Marquis of Londonderry.
On 24 May 1934, at the Empire Air Day, the County Durham public had been given their first opportunity to view the work of the squadron at close hand when the station was opened. This show attracted 1,300 visitors to the station, although it is reported that almost 5,000 other spectators watched the flying display from outside the airfield. Empire Air Days were staged on an annual basis, with the next one occurring on 25 May 1935. Proceeds from these events went to the RAF Benevolent fund, with admission being one shilling for adults and three pence for children. On the day, the visitors were able to see fourteen aircraft on the station, including Avro 504s and Wapitis. Highlights of the day included formation flying by the Wapitis, and the opportunity of sitting in a Wapiti for a small fee.
When Usworth opened its gates to the public on the RAF's Empire Air Day in May 1936, the flying display of all twelve of No. 607 Squadron Wapitis attracted a crowd of 3,500 people. In addition to these aircraft, some new shapes in the form of the Avro Tutor and Hawker Hart trainer were present with No. 607 Squadron. This show probably marked the last public appearance of the Wapiti. This type of aircraft continued to fly with No. 607 Squadron through until January 1937, even though on 23 September 1936 No. 607 Squadron had been re-designated as a fighter squadron. With the squadron becoming a fighter unit, the Wapitis were given up for the faster and more graceful Hawker Demons that started to arrive in late September 1936. From 26 February 1937, a regular squadron, No. 103(B) flew Hawker Hinds from Usworth alongside No. 607. The only regular squadron, No. 103 Squadron, was active during the week, leaving the airfield free for the Auxiliary boys and their Demons at the weekends.


=== The Second World War ===


==== New construction projects ====
Further development of the airfield commenced in September 1939, when work was started on laying two concrete runways. In addition to the laying of the runways, the airfield was expanded to the south, east and west by taking in adjoining fields. The new 2,800 feet (850 m) long runway was laid north-east to south-west, with another of similar length on a north-south heading. A new perimeter track was laid around the airfield boundary with eight dispersal pens, each capable of taking a twin-engined aircraft. Also along the track were thirty four hard standings for single-engined fighters, and nine slightly larger hard standings. Three of the old pre-war Callendar hangars were dismantled, leaving just the Lamella and one Callendar hangar opposite the main gate. Many additional buildings were constructed between the airfield and the road. An Operations room for the stations new role was built near the Lamella hangar. This Operations room was later supplemented by an underground Battle Headquarters near the Cow Stand Farm corner of the airfield.
On the North Camp, much of the vacant land was taken up with new accommodation blocks for the expected large influx of personnel, including WAAFs. A new WT/RT station was set up to supersede the old hut. On both camps, numerous air raid shelters were constructed. To assist in the defence of the airfield, a series of dispersed sites were set up over a wide area around the airfield. These sites included a searchlight camp at the top of Ferryboat lane, and small AA gun posts out on the Birtley road, above the old quarries at the bottom end of Boldon Bank and along the disused railway line towards North Hylton. A large gunsite was set up near Downhill Farm, and on the Birtley road, well away from the station, a decontamination centre was built. Most of these dispersed sites were to be manned by members of the Durham Light Infantry and the Royal Artillery.
This work effectively rendered the airfield unusable, and thus No. 607 Squadron moved north to RAF Acklington, which they shared with the newly formed No. 152 Squadron. This squadron was also flying Gladiators, coded UM-, before receiving Spitfires in December of that (?) year. However, it would not be long before 607 Squadron would be called to action as they were to move to Merville between 10/15 November 1939, as part of the Air Component of the British Expeditionary Force.
Despite the lull in flying activity, Usworth was designated a Sector Fighter Station in No. 13 Group, Fighter Command, and thus controlled the reporting of all raids on the area. The work on the runways continued into 1940, and was much hampered by severe frosts that delayed the reopening for flying until the end of March 1940. It was expected that the airfield would once again become the home for No.607 squadron. However, on 11 May 1940 new residents moved in from RAF Church Fenton. At 15:35 fifteen Spitfires from No.64 squadron arrived at Usworth, and were dispersed around the airfield, becoming the first wartime residents one month after receiving its new Spitfires. The pilots were accommodated in the Officers Mess. The stay of No.64 Squadron was to be extremely brief, for the threat to the South of England resulted in all of the aircraft departing for Kenley at 16:00 on 16 May 1940. Had it not been for this threat, Usworth may well have become a Spitfire base.


==== Battle of Britain ====
Newly equipped with Hurricanes, No. 607 Squadron returned home, when ten aircraft arrived from RAF Croydon on 4 June 1940 to re-assemble and re-arm after their big show in France. The squadron received additional Hurricanes to replace those lost in France. During its time in France the squadron had been responsible for destroying over 70 aircraft of the Luftwaffe. Even at Usworth, danger was ever present with two aircraft being destroyed in flying accidents, these being N2704 on 26 June, and another on 9 September.
On 15 August 1940, a large formation of Heinkel He 111s of KG26, escorted by Me 110s of I/ZG76, were detected approaching the east coast. The Hurricanes of 607 Squadron, now back at Usworth, encountered the German formation over Whitley Bay, and accounted for at least two He 111s. Spitfires from No. 72 squadron at Acklington had met them to the northeast of the Farne Islands, and although heavily outnumbered, claimed several destroyed.


=== Post-war period ===
Flying did return to Usworth when No. 31 Gliding School formed sometime in 1944, giving elementary flying training to cadets of the Air Training Corps, from local squadrons in the north-east. An Aircrew Disposal Unit arrived on 24 June 1944, being responsible for finding posts for tour-expired aircrew, many from overseas. This unit was supplemented from 10 August 1944 when No. 2739 & No. 2759 Squadrons of the Royal Air Force Regiment took up residence. Their stay was brief as they departed for overseas duty on 18 September 1944. The departure of the RAF regiment was followed quickly by that of the Aircrew Disposal Unit which relocated to Coventry on 22 September 1944. Once again the airfield reverted to care and maintenance under the control of No 14 Maintenance Unit, based at Carlisle. This unit retained control of Usworth through to 1952 and stored various items at the airfield, including parachutes and engines. At one stage several hundred Cheetah XIX engines were stored in their packing cases in the Lamella hangar.
In 1952 Airwork Ltd took over the maintenance and flying training for the Chipmunks of Durham University Air Squadron and 23rd RFS plus the Anson 21s of 2 Basic Air Navigation School(both serving the RAFVR)the CFI was 'Ernie Lancaster'. Airwork themselves flew 2 Proctors, 2 Oxfords and a DH Rapide.
The Chipmunks were kept in the east end of the Lamella hangar at night and pushed out on to the tarmac during the day to allow minor maintenance to be carried out. Majors were carried out for all aircraft in the west end of the Lamella hangar.
When the government of the day closed half the VR air stations Usworth was closed. Sunderland council then bought it and a small airline flew for a short time.


=== Sunderland Airport ===
On 3 July 1962, RAF Usworth was purchased by Sunderland Corporation for £27,000, and reopened as Sunderland Airport. Sunderland Corporation re-laid the runways and renovated the hangar, and in June 1963, Sunderland Flying Club came into being. On 28 June 1964, an Open Day and commemorative ceremony took place to celebrate the rebirth of what was then Sunderland Airport.


== Current status ==
The main site of the former Sunderland Airport/RAF Usworth is now the Nissan Motor Manufacturing UK car plant and the North East Aircraft Museum is also located immediately north of the factory.


== See also ==
List of former Royal Air Force stations


== References ==
This article is based on the text found at www.neam.co.uk, which was submitted here and thus released under the GFDL by the copyright holder.


=== Citations ===


=== Bibliography ===


== External links ==
norav
Official site of Squadron leader John Sample