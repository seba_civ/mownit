Gymnasiarch (Latin: gymnasiarchus, from Greek: γυμνασίαρχος, gymnasiarchos), which derives from Greek γυμνάσιον (gymnasion, gymnasium) + ἄρχειν, archein, to lead, was the name of an official of ancient Greece whose rank and duties varied widely in different places and at different times.
In Classical Athens during the fifth and fourth centuries B.C.E., a gymnasiarch was chosen annually from each tribe to bear the expenses of the torch races (see Lampadephoria). The duties included the payment of all expenses connected with the training of the competitors, and the office was one of the most expensive of the public services demanded by Athens of her wealthy citizens. The name seems to imply that the gymnasiarch had also certain rights and duties in the gymnasia during the training of the youths, but there is no definite information on this subject.
After the establishment of Macedonian power, there was a change at Athens. One gymnasiarch was chosen annually, and his office was one of great dignity. He had the general oversight of order and discipline in the gymnasium of the epheboi and sometimes financed heavy expenses from his own purse. The same name was given to rich epheboi, who undertook for a longer or shorter period, generally one month, to bear certain heavy charges for their comrades, such as the expenses of festivals, or of furnishing the oil needed in the gymnasium.
Outside of Athens and the states which copied her gymnastic system, the term denoted either magistrates who had charge of the gymnastic and literary instruction, or those who have to provide for certain expenses connected with the gymnasium or festivals, either from their own property or from the public funds. There was a wide diversity in details.


== Notable gymnasiarchs ==
Claudia Metrodora


== References ==
 Gilman, D. C.; Thurston, H. T.; Colby, F. M., eds. (1905). "Gymnasiarch". New International Encyclopedia (1st ed.). New York: Dodd, Mead.  This work in turn cites:
Glatz, “Gymnasiarchia,” in Daremberg and Saglio, Dictionnaire des antiquités (Paris, 1896)


== Further reading ==
Athletics in the Ancient World By E. Norman Gardiner Page 78 ISBN 0-19-814211-0
City government in Hellenistic and Roman Asia minor By Sviatoslav Dmitriev Page 227 ISBN 0-19-517042-3