A total of 147 teams entered the 1994 FIFA World Cup qualification rounds, competing for 22 of the total 24 spots in the final tournament. The United States, as the hosts, and Germany, as the defending champions, qualified automatically. The draw was made in New York on 8 December 1991.
For the first time, 3 teams would qualify from the African zone, due to the performance of African teams in the previous tournament. The 24 spots available in the 1994 World Cup would be distributed among the continental zones as follows:
Europe (UEFA): 13 places, 1 of them went to automatic qualifier Germany, while the other 12 places were contested by 38 teams (including Israel).
South America (CONMEBOL): 3.5 places, contested by 9 teams. The winner of the 0.5 place would advance to the intercontinental play-offs (against a team from CONCACAF or OFC).
North, Central America and Caribbean (CONCACAF): 2.25 places, 1 of them went to automatic qualifier United States, while the other 1.25 places were contested by 22 teams. The winner of the 0.25 place would advance to the intercontinental play-offs (against a team from OFC).
Africa (CAF): 3 places, contested by 40 teams.
Asia (AFC): 2 places, contested by 30 teams.
Oceania (OFC): 0.25 place, contested by 7 teams. The winner of the 0.25 place would advance to the intercontinental play-offs (against a team from CONCACAF).
A total of 130 teams played at least one qualifying match. A total of 497 qualifying matches were played, and 1446 goals were scored (an average of 2.91 per match).


== Continental zones ==
To see the dates and results of the qualification rounds for each continental zone, click on the following articles:
Europe (UEFA)
Group 1 – Italy and Switzerland qualified.
Group 2 – Norway and Netherlands qualified.
Group 3 – Spain and Republic of Ireland qualified.
Group 4 – Romania and Belgium qualified.
Group 5 – Greece and Russia qualified.
Group 6 – Sweden and Bulgaria qualified.
South America (CONMEBOL)
Group 1 – Colombia qualified. Argentina advanced to the CONMEBOL–CONCACAF/OFC intercontinental play-off.
Group 2 – Brazil and Bolivia qualified.
North, Central America and Caribbean (CONCACAF)
Mexico qualified. Canada advanced to the CONCACAF–OFC intercontinental play-off.
Africa (CAF)
Group 1 – Nigeria qualified.
Group 2 – Morocco qualified.
Group 3 – Cameroon qualified.
Asia (AFC)
Saudi Arabia and South Korea qualified.
Oceania (OFC)
Australia advanced to the CONCACAF–OFC intercontinental play-off.


== Intercontinental play-offs ==
For the first time, there would be two rounds of play in the intercontinental play-offs. The teams from CONCACAF and OFC would first play against each other on a home-and-away basis. The winner would then play against the team from CONMEBOL on a home-and-away basis. The winner would qualify.


=== CONCACAF–OFC intercontinental play-off ===

The aggregate score was tied at 3–3, however, Australia advanced 4–1 on penalties to the CONMEBOL–CONCACAF/OFC intercontinental play-off.


=== CONMEBOL–OFC intercontinental play-off ===

Argentina qualified by the aggregate score of 2–1.


== Qualified teams ==

The following 24 teams qualified for the 1994 FIFA World Cup:
(h) – qualified automatically as hosts
(c) – qualified automatically as defending champions


== Notes ==
Brazil lost a World Cup qualifying match for the first time ever on 25 July 1993, when they were defeated 2–0 by Bolivia in La Paz. Brazil had played 31 World Cup qualifying matches (24 wins, 7 draws) before.
The entire team of Zambia was killed in a plane crash in Gabon carrying them to a qualifier against Senegal. The nation virtually had to build a new team from scratch, but the new team was a formidable opponent, missing out on USA 1994 by one point. Kalusha Bwalya was the only survivor, due to being in the Netherlands rather than on the flight.
San Marino's Davide Gualtieri scored the fastest goal ever in World Cup competition: the opener against England after just 8.3 seconds [1], although England went on to win the game 7–1. The English radio commentary on that day went as follows:

"Welcome to Bologna on Capital Gold for England versus San Marino with Tennent's Pilsner, brewed with Czechoslovakian yeast for that extra Pilsner taste... and England are one down."

Emil Kostadinov scored Bulgaria's goal winner against France in the dying seconds of their last World Cup qualifier match on 17 November 1993 (the last day of qualifying) at Parc des Princes in Paris. Bulgaria got the points and qualified for the finals, while denying France a ticket, as they would have qualified with only a draw. The team was heavily criticized for the loss, especially David Ginola, who mistakenly gave away the ball possession to the Bulgarians with poor pass that would lead to the last goal of Bulgaria. Les Bleus, with 2 home matches left, needed either to beat Israel or obtain a point against Bulgaria to guarantee qualification, but lost both matches.
In what is now considered a historic result, Colombia trounced Argentina 5–0 in Buenos Aires on the last match of the South American qualifiers. This result sent Colombia straight into the World Cup, while Argentina were forced to play Australia at the CONMEBOL/CONCACAF/OFC playoff to qualify.
Japan also conceded a last-minute goal against Iraq in their final match, thus denying themselves a place in the finals and handing the spot to their rivals South Korea. The incident is called the Agony of Doha in Japan.
There were only nine participants in the CONMEBOL qualification because Chile could not take part in qualification as it was still banned by FIFA.
On the last day of qualifying, reigning European champions Denmark lost 1–0 against Spain, subsequently moving from 1st to 3rd place and missing qualification. Spain's goal scored on a header by Fernando Hierro was controversial as Spanish midfielder José Bakero fouled Danish goalkeeper Peter Schmeichel, who mistakenly came out to collect the incoming cross from a corner kick, but did not see the ball. The referee, who had mistakenly awarded Spain the corner kick, did not see the foul and allowed the goal to stand. Ireland finished 2nd in the group with the same points and goal difference as Denmark, but qualified due to having scored more goals. Despite the mistakes of the referee, the Danes were not given a rematch.
This was the first and, to date, only time that Argentina qualified for a FIFA World Cup after playing qualifying play-offs.
For the first (and, as of 2014) only time, all four UK teams (England, Scotland, Wales and Northern Ireland) did not qualify.
For the last time in the history, one or some of football's most successful nations did not qualify.
Only 37 of the entered teams actually competed in the qualification tournament of the European zone: Germany qualified for the 1994 FIFA World Cup automatically as defending champions and Liechtenstein withdrew before the draw was made. Additionally, Yugoslavia was suspended by FIFA due to UN sanctions stemming from the Yugoslav wars.
Greece qualified for the first time, beating Russia in their last match in Athens, in front of 70.000 spectators, in a game that featured perhaps the largest number of fireworks being set alight in a football match. The game had to stop for 20 minutes after Nikos Machlas scored the winning goal, due to the thick smoke.


== References ==


== External links ==
1994 FIFA World Cup USA Preliminaries
RSSSF - 1994 World Cup Qualification