Pyjamas, or pajamas (American English), often shortened to PJs, jimmies, jimjams or jammies, can refer to several related types of clothing. Pyjamas are loose-fitting, two-piece garments derived from the original garment and worn chiefly for sleeping, but sometimes also for lounging, also by both sexes. More generally, pyjamas may refer to several garments, for both daywear and nightwear, derived from traditional pyjamas and involving variations of style and material.
The word pyjama  was incorporated into the English language via the British Empire from c. 1800 through the Bengali payjāmā, itself from the Persian word pāy-jāmeh (پايجامه lit. "leg-garment"). The original pyjāmā are loose, lightweight trousers fitted with drawstring waistbands worn by Muslims in India and adopted by Europeans there.


== History ==
The word pyjama was incorporated into the English language via Bengali. The word originally derives from Persian پايجامه pāyjāmeh meaning 'leg-garment'. The word originally referred to loose trousers tied around the waist.
The worldwide use of pyjamas (the word and the garment) is the result of British presence in India in the 18th and 19th centuries, and British influence on the wider Western world during the Victorian era. Pyjamas had been introduced to England as "lounging attire" as early as the 17th century, then known as mogul's breeches (Beaumont and Fletcher) but they soon fell out of fashion again. The word pajama (as pai jamahs, Paee-jams and variants) is recorded in English use in the first half of the 19th century, but they only become a fashion in Britain and the Western world as a sleeping attire for men in the Victorian period, from about 1870.
Hobson-Jobson: A Glossary of Colloquial Anglo-Indian Words and Phrases (1886) summarizes the state of usage at the time (s.v. "pyjammas"):


== Types ==


=== Traditional ===
Traditional pyjamas consist of a jacket-and-pants combination made of soft fabric, such as flannel; The jacket element usually has a placket front and its sleeves have no cuffs. For a number of reasons (increased freedom of movement, aesthetic appeal, etc.) many men opt to sleep or lounge barechested in just the pajama pants.
In colloquial speech, these traditional pyjamas are often called PJs, jim jams, or jammies; while in South Asia and South Africa, they are sometimes referred to as night suits.
Some pyjamas feature a drop seat (also known as a trap door or butt flap): a buttoned opening in the seat, designed to allow the wearer to conveniently use a toilet. Drop seats were very common on pyjamas made before the 1950s, but today they are rather rare.


=== Contemporary ===
Contemporary pyjamas are derived from traditional pyjamas. There are many variations in style such as short sleeve pyjamas, pajama bottoms of varying length, or, on occasion, one-piece pajamas, and pajamas incorporating various materials.
Chiefly in the US, stretch-knit sleep apparel with rib-knit trimmings are common. Usually worn by children, these garments often have pullover tops (if two-piece) or have zippers down the fronts (if one-piece), and may also be footed.
Although pajamas are usually distinguished from non-bifurcated sleeping garments such as nightgowns, in the US, they can sometimes include the latter, as in babydoll pyjamas.


=== Daywear ===
Pajamas may today refer to women's combination daywear, especially in the US where they became popular in the early 20th century, consisting of short-sleeved or sleeveless blouses and lightweight pants. Examples of these include capri pajamas, beach pajamas, and hostess pajamas.


== Construction ==
 Pajamas are usually loose fitting and designed for comfort, using soft materials such as cotton or the more luxurious silk or satin. Synthetic materials such as polyester and Lycra are also available.
Pajamas often contain visual references to a thing that may hold some special appeal to the wearer. Images of sports, animals, balloons, polka dots, flowers, stripes, plaids, foulards, paisleys and other motifs may all be used to decorate them. Pajamas may also be found in plainer designs, such as plaid or plain gray, but when worn in public, they are usually designed in such a way that makes their identity unambiguous. Older styles of children's pajamas have been depicted as having a square button-up flap covering the buttocks.


== Sociology ==
Pajamas are often worn with bare feet and sometimes without underwear. They are often worn as comfort wear even when not in bed.
Some people wear pajamas in public, whether for convenience or as a fashion statement.
In 2006 the gulf state of UAE banned local government workers from wearing pajamas to work.
The Tesco supermarket in St Mellons, Cardiff, United Kingdom started a ban on customers wearing pajamas in January 2010.
In January 2012, a local Dublin branch of the Government's Department of Social Protection advised that pajamas were not regarded as appropriate attire when attending the office for welfare services.
In January 2012, Michael Williams, a commissioner in Caddo Parish, Louisiana, proposed an ordinance prohibiting people from wearing pajamas in public. Caddo Parish already has a law against wearing sagging pants below the waist, but Williams is pushing for a law against pajama pants after seeing a group of young men wearing loose fitting pajama pants that were about to show their private parts. According to Williams, "The moral fiber in our community is dwindling. If not now, when? Because it's pajama pants today, next it will be underwear tomorrow.” 
Williams’ concerns are reflected in many school and work dress codes. Mount Anthony Union High School in Bennington, Vermont, banned pajamas in 2011, concerned that they could be a safety hazard.


== Gallery ==


== See also ==
Blanket sleeper/Footie Pajamas
Doctor Denton
Sleepover
Nightgown
Sleep


== References ==


== External links ==
Gao Yubin, The Pyjama Game Closes in Shanghai. New York Times. May 14, 2010. Accessed May 18, 2010.