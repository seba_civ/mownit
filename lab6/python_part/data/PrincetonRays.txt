The Princeton Rays are a minor league baseball team in Princeton, West Virginia, USA. They are an Advanced Rookie-level team in the Appalachian League and have been an affiliate of the Tampa Bay Rays since September 27, 1996. The Princeton franchise began play in the Appalachian League in 1988 and was previously affiliated with the Pittsburgh Pirates (1988–89), was a co-op team known as the Princeton Patriots (1990), and then was affiliated with the Cincinnati Reds (1991–96) before joining forces with Tampa Bay. The Princeton Rays play their home games at H.P. Hunnicutt Field, which originally opened in 1988 (and was completely rebuilt in 2000 on the same site) and seats 1,950 fans. The team plays an annual 68-game schedule that traditionally extends from mid-June through the end of August. Through the completion of the 2012 season, the franchise has seen 57 former players move on to play regular season major league baseball that at one time wore the Princeton uniform. This list of players includes big names such as Brandon Backe, Rocco Baldelli, Carl Crawford, Jonny Gomes, Josh Hamilton, Seth McClung, Pokey Reese, Matt Moore, Wade Davis, Desmond Jennings, Jeremy Hellickson, Jason Hammel, and Jared Sandberg. NFL quarterback Doug Johnson (1997 P-Rays) and current NBA referee David Guthrie (1995 Princeton Reds) also played professional baseball for Princeton teams. The team is operated on a not-for-profit basis.
Previously known as the Princeton Devil Rays, the P-Rays announced on December 2, 2008 that they would update their name, logo, colors, and uniforms as their parent club had done the previous year.


== Roster ==


== References ==


== External links ==
Princeton Rays