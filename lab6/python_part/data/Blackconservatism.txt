Black conservatism is a political and social philosophy rooted in communities of African descent that aligns largely with the conservative ideology around the world. It often emphasizes traditionalism, patriotism, independence and self-sufficiency, free market capitalism, and strong cultural and social conservatism within the context of the black church. In the United States it is often, but not exclusively, associated with the Republican Party.


== Overview ==
One of the main characteristics of black conservatism is its emphasis on personal responsibility and traditionalism. Black conservative may find common ground with Black Nationalists through their common belief in black empowerment and the theory that black people have been duped by the Welfare state. For many black conservatives, the singular objective is to bring social redemption and economic success to the black community.


== Black conservatism worldwide ==


=== Black conservatism in the United Kingdom ===
While there was an early link in the 18th century between Black Britons, mainly former slaves, and the abolitionist conservatives who successfully sought the end of the slave trade in 1807 many Black Britons have not traditionally supported conservative policies. This in some part emerged from the hostility of the Conservative Party to immigration from the Commonwealth during the 1950s and 1960s, culminating in the infamous speech by a leading Conservative Enoch Powell, in which he predicted mass immigration would lead to "a river of blood".
Despite this, there has long been a small number of conservative blacks. In recent years the Conservatives have attempted to undo the long-standing conservative prejudices, by attacking racism and trying to cultivate more of a following amongst the black community.
Increasingly more black and ethnic minority figures are being appointed and elected to positions within the Conservative Party. Notable black Conservatives in the United Kingdom include Lord Taylor of Warwick, Adam Afriyie MP, Wilfred Emmanuel-Jones, James Cleverly (a member of the London Assembly), and solicitor and businesswoman Helen Grant. Boxer Frank Bruno has also been a vocal supporter of the Conservative Party.


=== Black conservatism in Canada ===
Notable black conservatives in Canada include Senator Anne Cools and Senator Donald Oliver, both of whom serve in the Senate of Canada. Senator Oliver is a member of the Conservative Party of Canada, while Cools is a non-aligned Senator recognized as voting mainly with the Conservative caucus. Lincoln Alexander was Canada's first black MP, and served as a Federal Member of Parliament between 1968 to 1980 in the riding of Hamilton West. Alberta MPP Lindsay Blackett is a member of the Conservative Party.


=== Black conservatism in the Caribbean ===


=== Black conservatism in the United States ===

Black conservatism in the United States is a political and social movement rooted in communities of African descent that aligns largely with the American conservative movement. Since the Civil Rights Movement in the latter 20th Century, the African-American community has generally swung to the left of the right-wing conservative movement, and has predominantly favored itself on the side of liberalism. Black conservatism emphasizes traditionalism, strong patriotism, capitalism, free markets, and opposition to abortion and gay marriage in the context of the black church. Some elected black conservatives include Florida representative Allen West, U.S. Senator Tim Scott of South Carolina, former Oklahoma representative J.C. Watts, and former Connecticut representative Gary Franks. Other notable black conservatives include economist Thomas Sowell, former Secretary of State Condoleezza Rice, perennial political candidate Alan Keyes, and Supreme Court justice Clarence Thomas. In 2009, Michael Steele became the first black man to chair the Republican National Committee. In 2011, Herman Cain was considered the leading Republican presidential nominee for a period of time. In addition there are a number of up and coming voices in the arena of political talks shows, and guest analysts such as Dr. Carol Swain, professor of political science from Vanderbilt University with multiple appearances on CNN, Fox News, PBS, C-SPAN, and ABC Headline News..


== Conservatism in Africa ==
In the post-Cold War period, a number of avowedly-conservative parties have developed in most African countries. In countries where the population is divided by religion (i.e., Nigeria), conservative parties are often formed and constituted to target specific religions in their areas of greatest political dominance.


== See also ==

List of African American Republicans
Hip Hop Republican
Black leftism


== References ==
List of Black Conservatives running for federal office in 2010


== Organization Links ==
Republicans for Black Empowerment
The John Langston Forum
Dr. Carol Swain


== Blogs ==
Booker Rising
Voice of Chid
Conservative Black Chick
Black Conservative 360
HipHopRepublican.com
A Political Season
Dennis Sanders
Stacy on the Right


== Black Libertarian Blogs ==
Black Libertarians
Larry Walker
Kenneth Durden
Kaigler
Robert Wicks


== Documentary ==
Fear of a Black Republican


== Speakers ==
Shannon Reeves
Stephen Lackey
Lenny McAllister


== Journalist ==
Jennifer Oliver Oconnell
The Uncle Tom Dilemma from The Village Voice
The New Black Republicans from WBUR, Boston's NPR
Official website for the Conservative Party (UK).
Official website for the Conservative Party of Canada.