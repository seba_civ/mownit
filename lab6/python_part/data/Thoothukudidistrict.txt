Thoothukudi District, also known as Tuticorin District is a district in Tamil Nadu state in southern India. The district was formed by dividing Tirunelveli district in 1986 and Thoothukudi is the district headquarters. The district is known for pearl cultivation, with an abundance of pearls being found in the seas offshore. It was formerly ruled by one of the oldest kingdoms in India, the Pandyan Dynasty with the port of Korkai through which trade with Rome happened. The archaeological site at Adichanallur holds the remains of the ancient Tamil civilization. As of 2011, the district had a population of 1,750,176 with a sex-ratio of 1,023 females for every 1,000 males.


== Location ==
Thoothukudi district is situated in the south-eastern corner of Tamil Nadu. It is bounded on the north by the districts of Tirunelveli, Virudhunagar, and Ramanathapuram, on the east and southeast by the Gulf of Mannar and on the west and southwest by Tirunelveli district. The total area of the district is 4621 km². The district of Thoothukudi was carved out as a separate district on 20 October 1986 as a result of bifurcation of the Tirunelveli district.


=== Water bodies ===
There are no large reservoirs in this district so the Papanasam and Manimuthar dams located in the Tirunelveli district in the Thamirabarani River's flow are the main sources of irrigation. Other than the Thamirabarani River, the river Vaipar in Vilathikulam taluk, the river Karumeni which traverses through Sathankulam, and Tiruchendur taluks, Palayakayal are all sources.


== Administrative divisions ==
Thoothukudi District is divided into three revenue divisions and eight talukas. There are forty-one revenue firkas and 480 revenue villages.
The district is divided into twelve revenue blocks for rural and urban development. The twelve revenue blocks are Tuticorin, Thiruchendur, Udangudi, Sathankulam, Srivaikundam, Alwarthirunagari, Karunkulam, Ottapidaram, Kovilpatti, Kayathar, Vilathikulam, and Pudur. The district has one municipal corporation, Thoothukudi, two municipalities, Kayalpattinam and Kovilpatti, nineteen town panchayats, and 403 panchayat villages.


=== Constituencies ===
There is one parliamentary constituency for the district, Thoothukudi Constituency, and six assembly constituencies:
Kovilpatti Assembly Constituency
Ottapidaram Assembly Constituency
Srivaikundam Assembly Constituency
Tiruchendur Assembly Constituency
Thoothukudi Assembly Constituency
Vilathikulam Assembly Constituency


== Demographics ==
According to 2011 census, Thoothukudi district had a population of 1,750,176 with a sex-ratio of 1,023 females for every 1,000 males, much above the national average of 929. A total of 183,763 were under the age of six, constituting 93,605 males and 90,158 females. Scheduled Castes and Scheduled Tribes accounted for 19.88% and .28% of the population respectively. The average literacy of the district was 77.12%, compared to the national average of 72.99%. The district had a total of 462,010 households. There were a total of 748,095 workers, comprising 44,633 cultivators, 161,418 main agricultural labourers, 17,872 in house hold industries, 433,524 other workers, 90,648 marginal workers, 3,882 marginal cultivators, 39,226 marginal agricultural labourers, 4,991 marginal workers in household industries and 42,549 other marginal workers.


== Economy ==
Tuticorin port contributes majorly to the economy of the district besides providing employment. Tuticorin hosts industries such as SPIC, Sterlite, Tuticorin alkaline chemicals, DCW zirconium plant and numerous salt packing companies. Many coal based power plants are at various stages of commissioning. Kovilpatti consists of many small sized industry especially match stick industries.


=== Agriculture ===
Paddy is cultivated in the Palayakayal Srivaikundam, Sattankulam and Tiruchendur taluks. Cumbu, Cholam, Kuthiraivali and other pulses are raised in the dry tracts of Kovilpatti, Vilathikulam,Nagalapuram Ottapidaram, and Thoothukudi taluks. Cotton is cultivated in Kovilpatti, Ottapidaram and Thoothukudi Taluks. Groundnut cultivation is undertaken in Kovilpatti, Tiruchendur, and Sattankulam taluks. Groundnut cake is being used as manure and cattle feed. Nagalapuram makes its economy to be solely dependent agriculture. Main business of this area is dry chilly, cholam, cumbu wood charcoal, etc. With 35% share, the district is the top producer of Cumbu in Tamil Nadu. Palmyrah trees are grown mostly in Tiruchendur, Srivaikundam, Sattankulam and Vilathikulam taluks. Jaggery is produced from palmyrah juice; the production of jaggery is the main occupation of the people of Tiruchendur and Sattankulam taluks. Banana and other vegetables are raised in Srivaikundam and Tiruchendur taluks.


=== Salt production ===
The district constitutes 70 per cent of the total salt production of Tamil Nadu and 30 per cent of that of India. Tamil Nadu is the second largest producer of Salt in India next to Gujarat.


=== Transport ===
National Highway 45B, 7A and State Highways SH-32,33,40,44,75,76,77,93,176 connect to other parts of the State. Government buses connect the district with other parts of state. Vanchimaniyachi and Tuticorin station are major stations of Indian railway. The Port of Tuticorin provides container services. Tuticorin Airport is situated near Vaigaikulam and currently has flights from Chennai Tuticorin is the one of the cities which is having four ways of transportation (Road ways, Airways, Seaways and Railways).


=== Education ===
There are numerous educational institutions, colleges, schools providing education. The Agricultural College and Research Institute, Killikulam was established in 1984 – 85 as the third constituent College of Tamil Nadu Agricultural University.


== Recreation and Places of Interest ==
Hare Island,Tuticorin
Harbour Beach,Tuticorin
Roche Park,Tuticorin
Pearl Beach, Tuticorin
Lord Subramanya Temple, Thiruchendur
Lady of Snow Church, Tuticorin
Kulasekarapattinam Beach
Holy Cross Church, Manapad
Panchalankurchi
Kayalpattinam Beach
Samanar palli, Kalugumalai


== Notable people ==
Freedom fighters poet Subramanya Bharathi, V. O. Chidambaram Pillai, Oomaithurai, Veerapandiya Kattabomman, Vellaiyathevan, Veeran Sundaralingam were from the district.


== See also ==
Nalumavadi
Paravar


== References ==


== External links ==
Thoothukudi corporation
Thoothukudi District