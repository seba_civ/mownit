Andrei Yurievich Khrzhanovsky (Russian: Андрей Юрьевич Хржано́вский; born November 30, 1939 in Moscow ) is a Russian animator, documaker, writer and producer. He is the father of director Ilya Khrzhanovsky. People's Artist of Russia (2011).
He rose to prominence in the west with his 2009 picture "Room and a half" (ft Grigory Dityatkovsky, Sergei Yursky, Alisa Freindlich) about Joseph Brodsky. Although Khrzhanovsky's 1966 dark comedy "There Lived Kozyavin" was clearly a comment on the dangerous absurdity of a regimented communist bureaucracy it was approved by the state owned Soyuzmultfilm studio. However "The Glass Harmonica" in 1969 continuing a theme of heartless bureaucrats confronted by the liberating power of music and art was the first animated film to be officially banned in Russia.


== Filmography (selection) ==
Glass Harmonica (1968, short film, Russian: Стеклянная гармоника)
A Fantastic Tale (1978, Russian: Чудеса в решете)
A Pushkin Trilogy (1986)
The Lion With the White Beard (1995, Russian: Лев с седой бородой)
"Half Cat" (2002, Russian: Полтора кота)
A Room and a Half (2009, Russian: Полторы комнаты)


== References ==


== External links ==
Andrei Khrzhanovsky at the Internet Movie Database