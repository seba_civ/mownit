Yasha Malekzad (born 1 October 1984 in Norfolk, England) is an award winning music video director and producer, who has worked with international artists including Enrique Iglesias, Pitbull, Usher, Lil Wayne, Sharon Stone, The Wanted, Christina Milian, JLS, Tinchy Stryder, Romeo Santos, Lionel Richie, Paulina Rubio, Daley, Roselyn Sanchez, Parker Ighile, Camilla Belle and many more. He is also currently working on a long format documentary project on Olympic wrestling titled Pahlavan. Yasha is the creative director of Artist Preserve, his production company and product placement agency, located in Chelsea, London.


== Early life ==
Yasha Malekzad, of Persian and English descent, was born into a creative family of dancers and musicians. He pursued a career as a music producer and graduated from De Montfort University with an honours degree in music production and innovation. Whilst making a living DJing and selling records he fell into the world of film, first through producing music videos and then by directing them. Yasha always demonstrated great technical ability through his music production, which has been transferred into his film making and given him the reputation of being a very hands on director.


== Videography ==
Enrique Iglesias
Turn The Night Up
Loco ft Romeo Santos, including cameos by Danny Trejo & Roselyn Sánchez
Dirty Dancer ft Usher & Lil Wayne
Ayer
I Like How It Feels ft Pitbull
Tonight (I'm Fuckin' You) ft Ludacris
Finally Found You ft Sammy Adams & Daddy Yankee
Heart Attack
Bailando ft Descemer Bueno & Gente de Zona
Bailando -English Version ft Descemer Bueno, Gente de Zone and Sean Paul
Paulina Rubio
Boys Will Be Boys
Stafford Brothers ft Christina Milian & Lil Wayne
Hello
Parker Ighile
So Beautiful
Jodie Connor
Bring It
Daley
Broken
Alone Together
Pahlavan
A documentary Film telling wrestling's story.


== Awards ==
Best Music Video, 2011, Ev. Gerard Award, Dirty Dancer
Best Music Video, 2011, Los Premios 40 Principales Award, Tonight (I'm Fuckin' You)
Best Music Video, 2011, Univision’s Best of Music, Tonight (I'm Fuckin' You) Los Premios 40 Prinpales, 2014, Best Spanish Video - Bailando Latin Music Italian Awards, 2014, Best Latin Male Video of the Year - Bailando Premios Juventud, My Favourite Music Video - Bailando


== External links ==
Personal Website
Yasha Malekzad - IMDb
Artist Preserve London
Pahlavan Project


== References ==