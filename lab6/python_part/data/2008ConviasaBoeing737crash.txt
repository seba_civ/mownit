A Conviasa Boeing 737-291 Advanced (registered as YV-102T) ferry flight from Maiquetia, Venezuela to Latacunga, Ecuador crashed into Illiniza Volcano. The aircraft had been stored at Caracas and was being ferried to a new owner. There were 3 crew on board, none of whom survived.


== Aircraft ==
The crashed plane first flew on July 6, 1978 with Frontier Airlines. It later was operated by United Airlines and Atlantic Airlines de Honduras before changing hands to Conviasa.


== Investigation and final report ==
The accident was investigated by the Dirección General de Aviación Civil (DGAC) of Ecuador. The investigation spanned 622 days following the incident and the final report was released on May 14, 2010. The findings of the report were as follows (translated from Spanish):
"The Accident Investigation Board judges that the probable cause of this accident was non-observance by the flight crew of the technical procedures, configuration, speed and bank angle of the aircraft required to complete the initial turn of Instrument Approach Procedure Number 4 published in the Ecuador AIP [Aeronautical Information Publication] for Latacunga airport, a failure that placed the aircraft outside of the protected area (published pattern), bringing it into high elevation mountainous terrain."
The report also listed the crew's ignorance of the area surrounding the approach path and lack of airline documentation and procedures governing the conduct of flights to non-scheduled and special airports. 


== References ==