Polyplex are manufacturers of Biaxially Oriented Polyester (BOPET) Film for packaging, electrical and other industrial applications. Polyplex is the world's 4th largest producer of thin polyester film. It has manufacturing facilities in India, Thailand and Turkey.
With its headquarters in NOIDA, adjoining New Delhi the company has four PET Film manufacturing facilities–one in Khatima and one in Bajpur, in the state of Uttarakhand, India, one in Rayong province, Thailand, and one in Tekirdağ, Turkey (owned and operated by Polyplex (Thailand) Public Company Ltd. and Polyplex Europa Polyester Film Sanayi ve Ticaret A.S. respectively, its wholly owned subsidiaries).
Polyplex has established itself as one of the most profitable producers of PET Film by way of cost efficient operations resulting from high productivity and low overheads. Its products have gained wide acceptance in the global markets, such as the United States, Europe, Southeast Asia, South America and Australia, where the company has been consistently exporting a substantial part of its production.
Polyplex has also diversified into BoPP, CPP and downstream in metallising, silicone coating (Khatima and Thailand)and extrusion coating.
Polyplex is setting up a new line in Decatur, Alabama in the United States, and is further expanding into thick film in Thailand and bottle-grade resin in Turkey.
Product Description
Biaxially oriented PET film (BOPET) is used successfully in a wide range of applications, due to its excellent combination of optical, physical, mechanical, thermal, and chemical properties, as well as its unique versatility.
Optically brilliant, clear appearance
Unequaled mechanical strength and toughness
Excellent dielectric properties
Good flatness and coefficient of friction (COF)
Tear-resistant and puncture-resistant characteristics
Wide range of thickness-as thin as 1 micron up to 350 micron
Excellent dimensional stability over a wide range of temperatures
Very good resistance to most common solvents, moisture, oil, and grease
Excellent barrier against a wide range of gases

PET film can also be modified:
from extremely low shrinkage (<0.1%) to as high as about 75%-in any direction;
with pigments and fillers into a wide range of colors, haze, translucency, or opacity; and
to change surface textures from very smooth to any desired roughness.
A wide range of chemical treatments (in addition to corona) can be applied to PET film during its manufacture to help it adhere to various coatings, such as inks, adhesives, metalization, etc. Surface treatments can also be applied to incorporate properties like surface-slip and anti-static. Yet another approach is co-extrusion, where different polyester layers are combined to obtain properties like built-in heat sealability, rough surface with high clarity, etc.
Details on Surface Treatment
In addition to its versatility in properties and applications, PET film is also among the most environmentally friendly materials offered. Hundreds of film grades are currently available to meet your needs. Click here for details...
Environment Statement
PET is considered to be a "green" or environmentally friendly polymer. The reason being that PET and PET-based products are -
Safe
Non-toxic
Do not contains heavy metals
Do not use plasticizers
Lightweight
Easily recycled
As the demand for PET grows, more and more applications are being developed. This will allow industries throughout the world the ability to use PET Film to make stronger, lighter products-thus helping them replace other heavier materials that have undesirable environmental characteristics.


== References ==