Franz von Suppé or Francesco Suppé Demelli (18 April 1819 – 21 May 1895) was an Austrian composer of light operas from the Kingdom of Dalmatia, Austro-Hungarian Empire (now part of Croatia). A composer and conductor of the Romantic period, he is notable for his four dozen operettas.


== Life and education ==

Franz von Suppé's parents named him Francesco Ezechiele Ermenegildo Cavaliere di Suppé-Demelli when he was born on 18 April 1819 in Spalato, now Split, Dalmatia, Austrian Empire. His Belgian ancestors may have emigrated there in the 18th century. His father – a man of Italian and Belgian ancestry – was a civil servant in the service of the Austrian Empire, as was his father before him; Suppé's mother was Viennese by birth. He was a distant relative of Gaetano Donizetti. He simplified and Germanized his name when in Vienna, and changed "cavaliere di" to "von". Outside Germanic circles, his name may appear on programmes as Francesco Suppé-Demelli.
He spent his childhood in Zara, now Zadar, where he had his first music lessons and began to compose at an early age. As a boy he had no encouragement in music from his father, but was helped by a local bandmaster and by the Spalato cathedral choirmaster. His Missa dalmatica dates from this early period. As a teenager in Cremona, Suppé studied flute and harmony. His first extant composition is a Roman Catholic mass, which premiered at a Franciscan church in Zara in 1832. At the age of 16, he moved to Padua to study law – a field of study not chosen by him – but continued to study music. Suppé was also a singer, making his debut as a basso profundo in the role of Dulcamara in Donizetti's L'elisir d'amore at the Sopron theatre in 1842.
He was invited to Vienna by Franz Pokorny, the director of the Theater in der Josefstadt. In Vienna, after studying with Ignaz von Seyfried and Simon Sechter, he conducted in the theatre, without pay at first, but with the opportunity to present his own operas there. Eventually, Suppé wrote music for over a hundred productions at the Theater in der Josefstadt as well as the Carltheater in Leopoldstadt, at the Theater an der Wien. He also put on some landmark opera productions, such as the 1846 production of Meyerbeer's Les Huguenots with Jenny Lind.

Franz von Suppé died in Vienna on 21 May 1895 and is buried in the Zentralfriedhof.


== Works ==
Two of Suppé's comic operas – Boccaccio and Donna Juanita – have been performed at the Metropolitan Opera in New York, but failed to become repertoire works. He composed about 30 operettas and 180 farces, ballets, and other stage works. Although the bulk of Suppé's operas have sunk into relative obscurity, the overtures – particularly Dichter und Bauer (Poet and Peasant, 1846) and Leichte Kavallerie (Light Cavalry, 1866) – have survived and some of them have been used in all sorts of soundtracks for films, cartoons, advertisements, and so on, in addition to being frequently played at symphonic "pops" concerts. Some of Suppé's operas are still regularly performed in Europe; Peter Branscombe, writing in The New Grove Dictionary of Music and Musicians, characterizes Suppé's song "Das ist mein Österreich" as "Austria's second national song".
Suppé retained links with his native Dalmatia, occasionally visiting Split (Spalato), Zadar (Zara), and Šibenik. Some of his works are linked with Dalmatia, in particular his operetta The Mariner's Return, the action of which takes place in Hvar. After retiring from conducting, Suppé continued to write operas, but shifted his focus to sacred music. He wrote a Requiem for theatre director Franz Pokorny (now very rarely performed; it premiered on 22 November 1855, during the memorial service for Pokorny); an oratorio, Extremum Judicum; three masses, among them the Missa Dalmatica; songs; symphonies; and concert overtures.


=== Posthumous use ===
The descriptive nature of Suppé's overtures have earned them frequent use in numerous animated cartoons:
Morgen, Mittag, und Abend in Wien (Morning, Noon, and Night in Vienna) was the central subject of the Bugs Bunny cartoon Baton Bunny. Poet and Peasant appears in the Fleischer Studios 1935 Popeye cartoon The Spinach Overture and the Oscar nominated Walter Lantz film of the same title; the overture to Light Cavalry is used in Disney's 1942 Mickey Mouse cartoon Symphony Hour.
The start of the cello solo (about one minute in) of the Poet and Peasant overture is nearly an exact match to the start of the folk song "I've Been Working on the Railroad", which was published in 1894.
The Light Cavalry Overture was covered in electronic form by Gordon Langford on his 1974 album The Amazing Music of the Electronic Arp Synthesiser.


== List of works ==

Some of Suppé's more well-known works are listed here, listed with date of first performance. All are operettas unless indicated:
Overture Ein Morgen, ein Mittag und ein Abend in Wien (Morning, Noon, and Night in Vienna) – 1844
Dichter und Bauer (Poet and Peasant) – 24 August 1846, Theater an der Wien, Vienna
Die Irrfahrt um's Glück – 24 April 1853, Theater an der Wien, Vienna
Das Pensionat – 24 November 1860, Theater an der Wien, Vienna
Die Kartenschlägerin – 26 April 1862, Kai-Theater Vienna
Zehn Mädchen und kein Mann – 25 October 1862, Kai-Theater Vienna
Flotte Burschen – 18 April 1863, Kai-Theater Vienna
Pique Dame – Opera – 22 June 1864, Graz (revision of Die Kartenschlägerin; based on the same story by Pushkin as was Tchaikovsky's opera The Queen of Spades)
Die schöne Galathée (The Beautiful Galatea) – 9 September 1865, Carltheater Vienna
Leichte Kavallerie (Light Cavalry) – 21 March 1866, Carltheater Vienna
Banditenstreiche (Jolly Robbers) – 27 April 1867, Carltheater Vienna
Die Frau Meisterin – 20 January 1868, Carltheater Vienna
Fatinitza – 5 January 1876, Carltheater Vienna (plot)
Boccaccio – 1 February 1879, Carltheater Vienna
Donna Juanita – 21 February 1880, Carltheater Vienna
Der Gascogner – 22 March 1881, Carltheater Vienna
Bellmann – 26 February 1887, Theater an der Wien, Vienna
Die Jagd nach dem Glück – 27 October 1888, Carltheater Vienna
March Oh Du mein Österreich
Overture Tantalusqualen


== References ==
Notes

Sources
Gänzl, Kurt. The Encyclopedia of Musical Theatre (3 Volumes). New York: Schirmer Books, 2001.
Traubner, Richard. Operetta: A Theatrical History. Garden City, New York: Doubleday & Company, 1983
Blažeković, Zdravko. "Franz von Suppé und Dalmatien", Studien zur Musikwissenschaft: Beihefte der Denkmäler der Tonkunst in Österreich, 43 (1994), 262–272.


== External links ==
List of stage works by Franz von Suppé (with other composers)
List of 50 stage works by Franz von Suppé
Works by Franz von Suppé at Project Gutenberg
Works by or about Franz von Suppé at Internet Archive
Free scores by Franz von Suppé at the International Music Score Library Project
Free scores by Franz von Suppé in the Choral Public Domain Library (ChoralWiki)

Franz von Suppé at Find a Grave