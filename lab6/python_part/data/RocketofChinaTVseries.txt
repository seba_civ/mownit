Rocket of China (Chinese: 龙号机车; pinyin: Lóng Hào Jī Chē) is a Chinese historical comedy set in nineteenth century China based on the true story of the history of the construction of Rocket of China, the first steam locomotive made in China, with the help of an English engineer Claude W. Kinder (Jin Da) who in 1878 travelled to Qing Dynasty China in the hope of building the first railway through China. The 30 episode comedy was directed by Ying Da and written by Man Yu. Appearing in the comedy are Cao Yun Jin, Jiang Chao, Liu Jin Shan, Ma Ling, Yu Hui Zi, Li Jian Hua, Li Qi, Na Wei, Liu Ya Jin, Yan Guan, Qi Xiao Fei, Isabella Charlton (Zhou Xiaolin), Karl Robert Eislen, Jonathan Kos-Read and among others. It was produced by Xui Ji Wei in the Production Company of Long Teng Yi Du (Beijing) Film Investment Co., Ltd. The comedy was first aired on Shenzhen Media Group Public Channel on 1 February 2016.


== The Plot ==
Jin Da (Claude W. Kinder), an English railway engineer, goes to China to build a train in Beijing, and asks his niece, Miaoli (Mary), to join him. Miaoli arrives in China from America with her fiancé Bo Te (Potter) and Tian You Nian, a train mechanic. They try to raise investment to fund the building of the train, but the project falls into trouble due in part to the Qing Empress Dowager Cixi's divided court, where the very conservative courtiers believe the train is evil. Jin Da, Miaoli and Tian You Nian work tirelessly to make the project a success.


== Characters ==
Tian You Nian studies abroad in order to flee a forced marriage by an odd coincidence. During his time in the U.S., he widens his horizons about western industrial technologies and learns to drive locomotives. After he returns to China, he is appointed to construct the first Chinese locomotive by the Qing dynasty's chancellor. After overcoming a series of difficulties he finally accomplishes his mission and falls in love as well.
Jin Da, Claude W. Kinder, is the chief engineer of the Qing dynasty's Mining Bureau having moved to China in 1878. Together with Tian You Nian, he was the driving force in the construction of the locomotive with help from his niece Miaoli and Miaoli's fiancé Bo Te.
Miaoli, Mary, is the niece of Jin Da. After her uncle asks her to join him in China, she goes to America to fetch her fiancé and Tian You Nian. They journey to China together by boat. Miaoli is integral to the effort to get the train financed and running, from being involved in strategic planning to working on a stall collecting scrap metal.
Bai Li Bing is the girlfriend of Tian You Nian. During the construction of the locomotive, Tian You Nian and Bai Li Bing developed affection for each other. While the Hui Long Sect revolts, Tian You Nian marries into the family of Bai Li to enable the railways and coal mines to remain controlled by China. In cooperation with Bai Li Bing’s father, Tian You Nian eliminates the Hui Long Sect.
Bai Li Wu Yin is the father of Bai Li Bing. At the beginning, Bai Li Wu Yin tries to sabotage the construction of Rocket of China as the locomotive would jeopardize his interests. Despite numerous attempts, he fails. After Tian You Nian marries into the Bai Li family, he cooperates with Tian You Nian to eliminate the Hui Long Sect.
Mrs. Tian is the mother of Tian You Nian. She is a kindly woman and only wishes for a stable and secure life for her family. Unfortunately, she always has to worry about her husband Tian Ru Mi and son Tian You Nian. When the wealthy Tian family goes bankrupt unexpectedly, Mrs. Tian loses everything and overnight becomes impoverished.


== Cast ==
Cao Yun Jin as Tian You Nian
Karl Robert Eislen as Jin Da
Isabella Charlton Zhou Xiaolin as Miaoli (Jin Da’s niece)
Jonathan Kos-Read as Bo Te (Potter) (Miaoli’s fiancé)
Liu Jin Shan as Bai Li Wu Yin (father of Bai Li Bing)
Ma Ling as Mrs. Tian (mother of Tian You Nian)
Yu Hui Zi as Bai Li Bing (Tian You Nian’s girlfriend)


== Production ==
Filming started in October 2014 and ended in January 2015. The film was shot on locations in Tangshan, Hebei Province.


== Broadcast ==
Episodes 1-3 of Rocket of China were first broadcast on Shenzhen Media Group Public Channel on 1 February 2016.


== External links ==
Claude William Kinder on Baidu Baike
Isabella Charlton (Zhou Xiaolin周筱琳 )
Long Teng Yi Du (Beijing) Film Investment Co., Ltd
Rocket of China (龙号机车) on Baidu Baike
The development of Kaiping Tramway into Imperial Railways of North China by Peter Crush


== References ==