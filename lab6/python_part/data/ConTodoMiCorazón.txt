Con Todo Mi Corazón (English: With All My Heart) is the second album from Latin music singer and winner of the second season of Objetivo Fama, Anaís. The album was released in the U.S. on April 3, 2007 and had an April 18 release in Mexico. The lead single "Tu Amor No Es Garantía" premiered in January 2007. The majority of the songs included on the album are almost all ballads. Anaís has said in many interviews that she was given more creative control with this album. She did a cover of the Daniela Romo song "Quiero Amanecer Con Alguien" and of "Reencuentro" along with Milly Quezada and Sergio Vargas, originally recorded by Álvaro Torres. She also co-wrote "Sexy Mama", which is sung partly in English.
A special Deluxe Edition of the album was released on October 16, 2007. The new edition brings remixes, and two new songs (both which she worked on with Marco Antonio Solís). The first called "Como Tu Mujer" (which she performed in a televised tribute to Marco with great acclaim), becoming the third single from the album, and another song entitled "Sin Él". Aside from the songs, the new edition also brings a DVD with two music videos.


== Track listing ==
"Tu Amor No Es Garantía"
"Sólo Mío"
"Quiero Amanecer Con Alguien"
"Necesidad"
"Mi Amor Por Ti (Love Is)"
"Fue Un Placer"
"Reencuentro" (featuring Milly Quezada and Sergio Vargas)
"Vivir"
"Sexy Mama"
"Tu Amor No Es Garantía" [Duranguense Version] (featuring Montez de Durango)
"Reencuentro" (featuring Milly Quezada and Sergio Vargas) [Merengue Version]
Deluxe edition
CD
"Tu Amor No Es Garantía" (Dance version)
"Sólo Mio" (reggaeton version)
"Como Tu Mujer"
"Sin Él"
DVD
"Tu Amor No Es Garantía" [music video]
"Sólo Mio" [music video]


== Release history ==


== Singles ==
All regularly released singles from the album and their chart peak position in the United States Billboard Hot 100 (HOT), U.S. Hot Latin Songs (HLT), U.S. Latin Pop Airplay (LPA), U.S. Latin Tropical Airplay (LTA).


== Chart performance ==