USS Thomas Jefferson (SSBN-618), an Ethan Allen class nuclear-powered submarine, was the second ship of the United States Navy to be named for Thomas Jefferson (1743–1826), the third President of the United States (1801–1809). She later was reclassified as an attack submarine and redesignated SSN-618.


== Construction and commissioning ==
Thomas Jefferson's keel was laid down on 3 February 1961 at Newport News, Virginia, by the Newport News Shipbuilding sponsored by Mrs. Robert S. McNamara, wife of United States Secretary of Defense Robert McNamara; and commissioned on 4 January 1963, Commander Leon H. Rathbun commanding the Blue Crew and Commander Charles Priest, Jr., commanding the Gold Crew. Thomas Jefferson was initially a fleet ballistic missile submarine and was later converted to an attack submarine.


== Ship history ==


=== Fleet ballistic missile submarine service, 1963-1981 ===
After shakedown training by both crews and a repair period, Thomas Jefferson was assigned to Submarine Squadron 14 in early October 1963. On 28 October 1963, the Blue Crew took the submarine on her first deterrent patrol which ended at Holy Loch, Scotland, in December 1963. Thomas Jefferson continued patrols from Holy Loch for the next four years and also acted as flagship for Submarine Squadron 14. In 1966, she returned to New London, Connecticut, for two training and rehabilitation periods. She began her 15th deterrent patrol on 12 January 1967 and, upon its completion, returned to Newport News, Virginia, for her first overhaul and refuelling. On 17 June 1968, Thomas Jefferson was ready for sea, and refresher training was held for both crews. Her 16th deterrent patrol began on 29 October and terminated at Rota, Spain, on 5 December 1968.
Thomas Jefferson made four deterrent patrols in each of the following years: 1969, 1970, 1971, and 1972. She also conducted special operations in 1970 and 1971. On 20 October 1972, the Gold Crew was awarded a Meritorious Unit Commendation for its special operations of the previous year.
Thomas Jefferson completed two patrols in 1973 before returning to the United States to hold midshipman training from 18 June to 31 August. Her last patrol of the year terminated on 12 December 1973. Her 36th and final, patrol in the Atlantic lasted from 31 January to 22 March 1974. After calling at Norfolk, Virginia, and Charleston, South Carolina, the submarine returned to New London 22 May. Thomas Jefferson was then reassigned to the Pacific Fleet with her new homeport at Vallejo, California. She stood out of New London on 7 June en route to the United States West Coast and arrived at Mare Island on 27 June.
On 1 July 1974, Thomas Jefferson entered the Mare Island Naval Shipyard for overhaul, refueling, and conversion to the Polaris A-3 missile system. She remained in the shipyard until 17 November 1975 when she got underway for Bremerton, Washington. The submarine remained in Puget Sound for a month and then moved to San Diego, California.
During the period January to March 1976, Thomas Jefferson's Blue Crew conducted post-overhaul shakedown operations and then transited the Panama Canal to conduct a Polaris missile firing at Cape Canaveral, Florida.
While operating out of San Diego in 1976, with the Blue Crew aboard, the ship experienced a complete loss of electrical power and propulsion. The only lighting during this event were the battery powered lanterns located throughout the ship. There was fire and flooding which caused a reactor scram during ORSE(Operational Reactor Safety Exam). If not for the training the crew had, this event may have resulted in tragedy. After successful ascent, the boat proceeded under its own power back to San Diego where an investigation resulted in better training for the ORSE Board Examiners.
The Gold Crew took over the ship on 4 April and conducted additional post-overhaul shakedown operations which included a missile firing at Cape Canaveral, a transit of the Panama Canal, and a missile loadout at Bangor, Washington, before resuming deterrent patrol operations with the Pacific Fleet on 8 August. Thomas Jefferson continued these operations as a unit of Submarine Squadron 15 from 1977 to 1980, at the end of which she completed her 51st deterrent patrol.


=== Attack submarine service, 1981-1985 ===
In fiscal year 1981, in compliance with the SALT I treaty, the missile section of Thomas Jefferson was disabled. Concrete blocks were placed in the missile tubes, and the missile fire-control system was removed. The ship was reclassified as an attack submarine and redesignated SSN-618 on 11 March 1981 and retained primarily for training, antisubmarine warfare exercises, and other secondary duties.
History needed for 1981-1985.


== Decommissioning and disposal ==
Decommissioned on 24 January 1985 and stricken from the Naval Vessel Register on 30 April 1986, ex-Thomas Jefferson started the Navy's Nuclear Powered Ship and Submarine Recycling Program at Bremerton on 1 October 1996 and ceased to exist upon completion of the recycling process on 6 March 1998. The sail is preserved in Gosport park adjacent to the Norfolk Naval Shipyard in Portsmouth, Virginia.


== References ==

This article incorporates text from the public domain Dictionary of American Naval Fighting Ships. The entry can be found here.
NavSource Online: Submarine Photo Archive: Thomas Jefferson (SSBN / SSN 618)