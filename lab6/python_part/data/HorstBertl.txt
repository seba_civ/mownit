Horst Bertl (born 24 March 1947) is a retired German football midfielder.


== Player ==
In 1969, Bertl began his career with TuS Bremerhaven 93 before transferring to Bundesliga club Hannover 96 in 1970. He played two seasons with Hannover and moved to Borussia Dortmund where he also spent two seasons. In 1974, Bertl signed with Hamburger SV. He played five seasons with Hamburg before moving to the United States and signing with the Houston Hurricane of the North American Soccer League in 1979. The Hurricane folded after the 1980 season and Bertl spent two seasons with the Memphis Americans of the Major Indoor Soccer League.


== Manager ==
In 1981, the Memphis Americans of MISL signed Bertl as head coach. He compiled a 39-53 record over two seasons as a player-coach. In 1984, Bertl became a coach with the Comets Soccer Club in Dallas, Texas. In 2012, MLS's FC Dallas Youth acquired the Comets Soccer Club, adding Bertl to its program as well. In 1993, he coached the Dallas Rockets to the USISL playoffs.
Bertl served as player agent for Paul Caligiuri, Eric Eichmann, Braeden Cloutier and Brian McBride.


== Honours ==
UEFA Cup Winners' Cup: 1976–77
Bundesliga: 1978–79
DFB-Pokal: 1975–76


== References ==


== External links ==
Horst Bertl profile at Fussballdaten
NASL/MISL stats