This article is about President Ilham Aliyev's daughter. For the television presenter, see Leyla Aliyeva (presenter).
Leyla Aliyeva (Azerbaijani: Leyla Əliyeva, born 3 July 1984) is the first daughter of the President of Azerbaijan, Ilham Aliyev. She is editor-in-chief of style magazine Baku, launched in London in 2011.


== Personal life ==
According to her personal website, Aliyeva was born in Moscow on 3 July 1985. British company records, however, give her date of birth one year earlier, 3 July 1984, noting that the individual's details have been authenticated.
From 2006 to 2008, Leyla Aliyeva pursued a Master's degree at the Moscow State Institute of International Relations (MGIMO-MSIIR; also the alma mater of her father). She was the Chairwoman of the Azerbaijan Club at MGIMO until 2010.
In July 2011, the Taraggi (Progress) Medal was conferred upon Leyla Aliyeva for her contributions to the development of Azerbaijan–Russia relations.
Leyla Aliyeva is a mother of three children. Their names are Ali, Mickail and Amina.
In 2010 The Washington Post reported that Leyla along with her sister Arzu and brother Heydar own a real estate in Dubai that is worth about $75 million.
In May 2015 Aliyeva announced through Instagram that she had divorced Agalarov. Agalarov wrote: "We remain friends and continue to raise our children together".


== Professional career ==


=== Heydar Aliyev Foundation ===
Aliyeva is Vice-President of the Heydar Aliyev Foundation, a non-commercial organisation that carries out a mixture of charitable, cultural and political ambassadorial projects, presided over by Mehriban Aliyeva, the country's First Lady and her mother.
Since 10 May 2007, Leyla has headed the representative office of the Foundation in Russia. As part of these programmes, Leyla drives charitable initiatives, often with an emphasis on supporting underprivileged youths.
On 1 November 2011, Aliyeva was presented with a "Key to Life" award at the annual benefit ball of US charity the Children's Cancer & Blood Foundation.
In 2012, Leyla Aliyeva organized an exhibition called "Fly to Baku", showcasing works of 21 artists among which were Leyla Aliyeva's works, too. The exhibition was started in London and the next exhibition was held on 12 April 2012 in Paris, France.


=== Environmentalism ===
The initiative International Dialogue for Environmental Action (IDEA), announced by Vice President of the Heydar Aliyev Foundation Leyla Aliyeva on 12 July 2011 will serve as a platform for active involvement of the youth with the global movement on sustainable development. As the Founder and Director of the International Dialogue for Environmental Action (IDEA) global initiative, Leyla Aliyeva encourages young people to live in harmony with their environment, and writes articles on green issues, particularly those relating to the South Causasus.

When my team and I thought about the slogan for the campaign, we chose one that would unite and inspire us to action: "One Earth - One Future." We have one planet and one future, she said, calling on young people, concerned with the future of the planet, to join effort in defining objectives and actions required to ensure stable and secure future for generations to come.

According to her, environmental problems associated with climate change, increased carbon emissions, increasing concentrations of greenhouse gases in the atmosphere, processes in the oceans, drought and humanitarian crisis in different parts of the world cannot leave the younger generation indifferent.

I have seen in my own country the damage that can be done by unchecked industrial development. We have learnt from the past -- we inherited a toxic legacy from the Soviet era. Azerbaijan is rich in natural resources, including oil and gas, and now as a strategic partner, we are an important source of energy for the rest of Europe. This doesn't mean we stop thinking about the future or finding ways to secure sustainable development, which is why we are investing in renewable energy sources.

The vision of the campaign is: "Releasing the power of youth to create greater harmony with our global environment." The IDEA campaign entails a number of actions aimed at raising awareness of environmental issues and the need to protect the environment among young people and the international community.

Ultimately, only with changed attitudes - not just in one country, but across our region's cultural, political and military dividing lines - that we can create the conditions for a sustainable future.

The issues of ecology and environmental protection in Azerbaijan has traditionally given special attention. In this light, Leyla Aliyeva promotes large-scale projects on gardening in Absheron, forest conservation and rehabilitation, as well as for improvements in waste management and waste water, air quality and the protection of the Caspian Sea from pollution, improving the ecological situation in the oil-polluted territories in the Bay of Baku, the preservation of clean water and improving water quality, conservation and restoration of biological diversity, the development of infrastructure of national parks, etc. Aliyeva authored a successful blog at the Huffington Post's webpage, that generates significant reader interest on the topic of creating positive and sustainable environmental change.

Concerted cross-party action on environmental issues can be a precursor to joint agreement on issues which divide nations and threaten our economic prosperity and security.

As Rio+20 conference will be organized to celebrate the 20th anniversary of the historic United Nations Conference on Environment and Development held in 1992, a project "Mobilizing Azerbaijani Youth for Action in Preparation for the 'Rio+20' Summit in 2012" was initiated by the Heydar Aliyev Foundation and supported by UNDP, and was agreed upon and signed in Baku on 21 October 2011 by UN Resident Coordinator in Azerbaijan Fikrat Akcura and Vice-President of Heydar Aliyev Foundation Leyla Aliyeva.
In 2012 as a founder of IDEA (International Dialogue for Environmental Action), Leyla Aliyeva initiated The Children’s Eyes On Earth project with Reza, one of the best-known photojournalists in the world. Later, within the framework of that project The Children's Eyes On Earth International Youth Photography Contest was launched, becoming a great opportunity for youth to raise awareness about important environmental issues, with photographs on the themes "I Love Nature and I Fear Pollution." The main idea behind the contest was to create an opportunity to share experiences and to help promote an understanding of some of the challenges facing our planet and to establish one of the main platforms to inspire families, business people, decision-makers, politicians and others around the world to take action.


=== Political activity ===
Aliyeva is General Coordinator of Intercultural Dialogue for the Youth Forum for Dialogue and Cooperation of the Organization of the Islamic Conference (May 2008), and chair of the Azerbaijani Youth Organization of Russia (AYOR). She is the founder and editor-in-chief of Baku magazine, cutting-edge international art and fashion magazine that engenders and catalyses collaborations between some of the world's leading creatives and Azerbaijan. Russian-language version of Baku has been successfully published in Russia since 2007. In October 2011, the first edition of the English-language version of Baku magazine has been launched in London. On 20 April 2012 Baku magazine was launched in Rome, Italy. Besides the United Kingdom the magazine will also be available in other countries, including France and the United States.
Aliyeva promotes Azerbaĳani culture in Russia. She has long been active in working with young Azerbaĳanis who study and live in Russia and Europe. Leyla Aliyeva is guided in her public work by the slogan, "We must stick together and cooperate" Aliyeva initiated and leads the International Awareness Campaign "Justice for Khojaly, Freedom for Karabakh" and on 18 April 2009. On 24 February 2012 Leyla Aliyeva attended opening ceremony of "Park of Friendship" and unveiled a monument to victims of Khojaly Massacre in Sarajevo, the capital of Bosnia and Herzegovina. Leyla Aliyeva was elected Chairwoman of the Azerbaijani Youth Organization of Russia (AYOR). For example, one particular project by the Azerbaijani Youth Organization of Russia, called "Blood Has No Nationality", involves young Azerbaijanis donating blood for sick children.

Increased cooperation with international political organizations, as well as with student and youth organizations, representation in these organizations, providing training to young people, the observation of events in Europe, joint and flexible actions in the interests of Azerbaijan should be our main goal.

On 30 June 2012 Leyla Aliyeva attended a special meeting called "New leaders for tomorrow" of Crans Montana Forum and delivered speech about environmental issues and importance of the young generation in this sphere.


== References ==


== External links ==
Leyla Aliyeva's channel on YouTube