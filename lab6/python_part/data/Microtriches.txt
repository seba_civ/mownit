Microtriches (singular microtrix) are the highly specialized microvilli covering the entire surface of the tegument of cestodes. They are fine hair-like filaments distributed throughout the surface of the body, both unique to and ubiquitous among cestodes, giving the body surface a smooth and silky appearance. They are different from typical microvilli in that they contain conspicuous electron dense materials at the tip. Due to their morphological variation they make up unique defining structures in cestodes. Since cestodes are devoid of any digestive and excretory systems, the tegument with its microtriches is the principal site of absorption and secretion. In fact the tegument highly resembles the gut of animals turned inside out.


== Structure ==
The microtriches are fine cylindrical tubular filaments, with smooth rounded ends, and arranged in rows corresponding to the regular ridges of the tegument. Microtriches are documented to exhibit wide range of morphology in different species of tapeworms, and serve as an identifying character among the members of Eucestoda. They are also of special interest in pharmacology as they are the basic interface of the tapeworm with its surrounding, thus serve as the primary site of absorption of nutrients and the target site of anthelmintics.
Though microtriches have been described for several decades, the consensus terminology for the structure was made only at the turn of 21st century. Standardised terms were resolved based on discussions that occurred at the International Workshops on Cestode Systematics in Storrs, Connecticut, USA in 2002, in Ceské Budejovice, Czech Republic in 2005 and in Smolenice, Slovakia in 2008. The following terms were endorsed for the components of each microtrix: the distal, electron-dense portion is the "cap", the proximal more electron-lucent region is the "base", and these two parts are separated from one another by the "baseplate". The base is composed of, among other elements, microfilaments, while the cap is composed of cap tubules. The electron-lucent central portion of the base is referred to as the "core". The core may be surrounded by an electron-dense tunic. The entire microthrix is enveloped by a plasma membrane, the external layer of which is referred to as the glycocalyx. Two distinct sizes of microtriches are recognised: those < or = 200 nm in basal width, termed "filitriches", and those >200 nm in basal width, termed "spinitriches". Filitriches are considered to occur in three lengths: papilliform (< or = 2 times as long as wide), acicular (2-6 times as long as wide), and capilliform (>6 times as long as wide). Spinitriches are much more variable in form.


== Functions ==
All cestodes lack digestive and excretory systems, therefore, the tegument with its microtriches constitute the principal site of absorption of nutrients and elimination of waste materials. Moreover the microtriches are the primary structures for host-parasite interface, and are metabolically active performing all the vital activities such as sensory, absorptive and secretory functions. Thus their structural significance is clearly to amplify the total surface area of the tegument. The surface carbohydrate complex called glycocalyx is responsible for inhibition of the host digestive enzymes, absorption of cations and bile salts, and enhancement of the host amylase activity. The acidic glycosaminoglycans of the glycocalyx are specific for inhibiting a number of digestive enzymes of the host. The microtriches in cestodes, and pits and spines in trematodes increase the surface area of the teguments for enhanced absorption of nutrients. In addition, they act as sensory organs for detecting the surrounding environmental cues and primary target site of anthelmintic drugs. The capacity of the tegument to absorb exogenous materials is proportional to the number and extent of pits or microtriches and the number of mitochondria in the distal cytoplasm.


== See also ==
Platyhelminthes
Cestoda
Taenia (tapeworm)


== References ==


== External links ==
Schistosomiasis Research Group at University of Cambridge
Parasitology Informatik
The Parasitology Group at Aberystwyth University