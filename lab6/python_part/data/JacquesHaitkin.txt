Jacques Adam Haitkin, (born August 29, 1950, in Brooklyn, New York) is an American cinematographer. He is well-known as the cinematographer for A Nightmare on Elm Street Series 1 and 2.
He learned at film school of New York University, and American Film Institute, and graduated in 1975.
He is well-known mainly horror film cinematographer, especially Wes Craven and Jack Sholder films.
He also additional or second unit cinematographer for The Expendables, X-Men: First Class, and The Last Stand, Furious 7 and Teenage Mutant Ninja Turtles 2.


== Filmography ==
Hot Dogs for Gauguin (1972)
Hot Tomorrows (1977)
They Went That-A-Way & That-A-Way (1978)
The Prize Fighter (1979)
The Private Eyes (1980)
The Girl, the Gold Watch & Everything (1980)
St. Helens (1981)
Galaxy of Terror (1981)
The House Where Evil Dwells (1982)
Last Plane Out (1983)
The Lost Empire (1983)
Making the Grade (1984)
A Nightmare on Elm Street (1984)
A Nightmare on Elm Street 2: Freddy's Revenge (1985)
Quiet Cool (1986)
The Hidden (1987)
Cherry 2000 (1987)
To Die For (1989)
Cage (1989)
Shocker (1989)
The Ambulance (1990)
Buried Alive (1990)
Fast Getaway (1991)
We're Talking Serious Money (1991)
Mom and Dad Save the World (1992)
Maniac Cop III: Badge of Silence (1993)
Relentless 3 (1993)
The Silence of the Hams (1994)
Scanner Cop (1994)
The Force (1994)
Evolver (1995)
Fist of the North Star (1995)
Bloodsport II: The Next Kumite (1996)
Buried Alive II (1997)
Wishmaster (1997)
Team Knight Rider (1997–1998) TV series, 14 episodes
The Last Man on Planet Earth (1999)
The Base (1999)
The Apartment Complex (1999)
The Chaos Factor (2000)
Blowback (2000)
Son of the Beach (2000) TV series, 3 episodes
Faust: Love of the Damned (2000)
Rocket's Red Glare (2000)
A Month of Sundays (2001)
Storm Watch (2002)
Bad Karma (2002)
Shut Up and Kiss Me (2004)
Art Heist (2004)
12 Days of Terror (2004)
The Curse of El Charro (2005)
The Haunting Hour: Don't Think About It (2007)
Clean Break (2008) also story


== References ==


== External links ==
Official Site
Jacques Haitkin at the Internet Movie Database
Jacques Haitkin – cinematographers.nl