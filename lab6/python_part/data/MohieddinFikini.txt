Mohieddin Fikini (Arabic: محي الدين فكيني‎; 10 March 1925 – 1994), last name also spelled Fekini, was the Prime Minister of Libya from 19 March 1963 to 22 January 1964. He was also the Minister of foreign affairs from 19 March 1963 to 22 January 1964.


== Family background ==
Mohieddin Fikini is a descendant of Rajban tribe in Djebel Nefusa, Tripolitania. His father, Mohammed ben Khalifa Fikini, was one the leaders of resistance against the Italian invasion of Libya in 1911. In 1923, after the collapse of the resistance against the Italians (partially because of Mohammed Fikini's failure along with other leaders to unify their efforts against the invaders), Mohammed Fikini left Tripolitania for Fezzan along with his family.


== Early life ==
Mohieddin Fikini was born in Fezzan in 1925. In 1929, the Italians launched their offensive against Fezzan, so his father left Fezzan with his family for Algeria via Ghat, and at last reached Tunisia, and chose to live at Gabès. He remained there until his death in 1950. His family returned to Tripoli in 1953.


== Before Premiership ==
Mohieddin Fikini was successful in his studies. He studied law in the University of Paris and got his doctorate in 1953. The title of his thesis was "Le règlement de la question libyenne par l'Organisation des Nations Unies". He also learned to speak Italian, English, and French. Fikini participated in the agreements made by the Libyan government with the governments of Britain, the United States, and Italy between 1953–1956. Fikini served as Libya's ambassador in Egypt (1957–1958), and from late 1958 simultaneously in the United States, and the United Nations.


== Prime minister ==

In March 1963, King Idris I of Libya assigned Fikini to form a new cabinet, in which he also became foreign minister. His government accomplished some memorable achievements like:
Replacing the federal system in Libya with a central government in April 1963.
Launching the first five-year plan for development (1963–1968) in June 1963.
Amendments of some articles of the 1951 Libyan constitution, one of which gave the women the right to vote. This right will be lost by both of men and women in September 1969.
However, Fikini's government didn't last long. In January 1964, many young people in Benghazi made a demonstration because of the King's absence in an Arab summit in Cairo, Egypt (although Fikini himself was there), and some police officers behaved hard and shot some demonstrators, some of them were killed. After his return, Fikini promised to punish any one involved in this case. This led to a disagreement with the commander of the security force Mahmud Buguaitin who defended his officers (though he didn't order them to shoot the demonstrators). Fikini asked the King to remove Buguaitin. Buguaitin was a loyal officer to King Idris, and fought in the Senussi force with the British forces in World War II, so the King refused Fikini's demand. Fikini presented his resignation and King Idris accepted it on 22 January 1964.


== Later life ==
Thereafter, Fikini headed no political posts. The most recent information about him that he met the Libyan leader Muammar Gaddafi in 1982 along with many officials from the Kingdom era.


== References ==
Mohamed Yousef al Magariaf,"Libia bain al Madi wal Hadir", vol.4, Matabat Wahba, 14 Gohumriya st. Cairo, Egypt, 2006.
Kalifa Tillisi, "Mu'jam Ma'arek al Jihad fi Libia 1911–1931", dar ath Thaqafa, Beirut, Lebanon, 1973.
Taher Ahmed Ez Zawi, "A'lam Libia", Dar al Manar al Islami, Beirut, Lebanon.
Mustafa Ben Halim, "Libia : Inbe'ath Omma.. wa Soqout Dawla", Manshurat al Jamal, Cologne, Germany, 2003.
Notes


== External links ==
 Media related to Mohieddin Fikini at Wikimedia Commons