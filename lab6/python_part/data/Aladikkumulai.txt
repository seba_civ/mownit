Aladikkumulai is a village located in Pattukkottai Taluk, India. It is one of the 32 villages of Musugundan Community.
The village contains a pond called 'Thamarai Kulam', which is most beautiful with the growth of lotus. The village has a government primary and high school.Also has a private school named 'Brindhavan Higher Secondary School'. The other name for this village is 'Thamarai Kudi Kaadu'.
Most of the residents are educated. Agriculture used to be the main occupation, but this has declined in popularity, possibly due to lack of water and reduced profitability. There is a temple dedicated to Amman for whom a festival is conducted every year.


== Inhabitants ==
Although some people who were born or raised in Aladikkumulai have chosen different career paths, the predominant occupation is still farming.
The majority of the people living in this village belong to the Velallar subcaste Nadar, Thevar, Yadav and others.
There is a temple(Sri Muthu Mariyamman kovil) in Nadar Street, where a festival is conducted every year. It's very famous for Manjal Neerattu vizha dedicated to Amman.
The village's inhabitants represent a range of religions including Muslims, Christians and Hindus, who co-exist peacefully.


== References ==