"Porzellan" [Porcelain] is a song by Farin Urlaub. It's the second single and the fourth track from his album Am Ende der Sonne. It's about feeling, that everyone else's life is good, while your life is bad - the happiness, however, can be everywhere, if people could only let it.


== Video ==
Farin plays a man, who most probably survived a plane crash and lands on a deserted beach with a parachute. He settles down and learns to fish. After a long time he sees a group of people resting on the beach, who got there with a dinghy and runs to them to get help. However, he looks quite scary, because he now has a lot of facial hair and on the way to the people he trips and hits his mouth, so he's also got blood around his mouth and he also has a spear (for fishing) in his hands, so the people sail away in fear. As they are sailing away, we can see a girl wearing a shirt that says "R.I.P Farin Urlaub".
In an alternate version of the video, Farin just sings the song and plays guitar under palm trees on the beach.


== Track listing ==
"Porzellan" ("Porcelain") – 3:58
"Atrás" ("Behind" in Spanish) – 4:27
"Xenokratie" ("Xenocracy") – 3:08
"Porzellan" (Video) – 4:47