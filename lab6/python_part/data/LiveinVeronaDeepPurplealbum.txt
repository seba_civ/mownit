Live in Verona is a live release by English hard rock band Deep Purple's mk VIII lineup credited as Deep Purple with Orchestra, and performed alongside the Neue Philharmonie Frankfurt conducted by Stephen Bentley-Klein. This concert was recorded at the Arena di Verona, a Roman amphitheater originally built in 30 AD, on 18 July 2011. Besides a Blu-ray/DVD release, the concert has also been released in Japan on 2CD. Film of the concert was released on October 21, 2014 by German label Eagle Rock Entertainment. The audio album was released on October 8, 2014 by Ward Records.


== Track listing ==
All songs written by Ritchie Blackmore, Ian Gillan, Roger Glover, Jon Lord, and Ian Paice except where noted.


=== 2CD track listing ===


==== Disc 1 ====
"Deep Purple Overture" (Stephen Bentley-Klein, Jack Bruce, Pete Brown, Eric Clapton, Blackmore, Gillan, Glover, Lord, Paice)
"Highway Star"
"Hard Lovin' Man"
"Maybe I'm a Leo"
"Strange Kind of Woman"
"Rapture of the Deep" (Gillan, Steve Morse, Roger Glover, Don Airey, Paice)
"Woman from Tokyo"
"Contact Lost" (Morse)
Steve Morse solo
"When a Blind Man Cries"
"The Well Dressed Guitar" (Morse)


==== Disc 2 ====
"Knocking at Your Back Door" (Blackmore, Gillan, Glover)
"Lazy"
"No One Came"
Don Airey solo
"Perfect Strangers" (Gillan, Blackmore, Glover)
"Space Truckin'"
"Smoke on the Water"
"Hush" (Joe South)
Roger Glover Solo
"Black Night"


=== DVD / Blu Ray ===
"Deep Purple Overture"
"Highway Star"
"Hard Lovin' Man"
"Maybe I'm a Leo"
"Strange Kind of Woman"
"Rapture of the Deep"
"Woman from Tokyo"
"Contact Lost"
"When a Blind Man Cries"
"The Well-Dressed Guitar"
"Knocking at Your Back Door"
"Lazy"
"No One Came"
Don Airey solo
"Perfect Strangers"
"Space Truckin’"
"Smoke on the Water"
"Hush" - Bonus feature
"Black Night" - Bonus feature
"Hush" (South)
"Black Night"


== Personnel ==
Deep Purple
Ian Gillan - vocals, harmonica
Steve Morse - guitar
Roger Glover - bass
Ian Paice - drums
Don Airey - keyboards
with
Neue Philharmonie Frankfurt
Stephen Bentley-Klein - conductor


== Production notes ==
Audio engineer: David Richards
Mixed and mastered by Eike Freese and Alexander Dietz


== Chart performance (DVD) ==