The extensor indicis et medii communis is a rare anatomical variant in the extensor compartment of forearm. This additional muscle lies in the deep extensor layer next to the extensor indicis proprius and the extensor pollicis longus. The characteristics of this anomalous muscle resemble those of the extensor indicis proprius, with split tendons to the index and the middle finger. This muscle can also be considered as a variation of the aberrant extensor medii proprius. 


== Structure ==
The extensor indicis et medii communis originates from the distal third of ulna next to the extensor indicis proprius. After passing the wrist joint through the fourth extensor compartment, the tendon splits into two to insert to the extensor expansion of the index and the middle finger.


=== Prevalence ===
The extensor indicis et medii communis has an incidence between 0% and 6.5%. Meta-analysis showed that the muscle was present in average of 1.6% of the total 3,760 hands, and was more prevalent in North American populations.


== Function ==
The extensor indicis et medii communis extends the index and the middle finger.


== See also ==
Extensor medii proprius
Extensor indicis proprius
Extensor digitorum brevis manus
List of anatomical variations


== References ==


== External links ==
Extensor Digitorum Communis - Anatomy Atlases