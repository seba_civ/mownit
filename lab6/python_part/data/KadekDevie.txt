Ida Ayu Kade Devie (born November 7, 1985), better known as Kadek Devie, is an Indonesian actress. She was born in Bandung, Indonesia.


== Career ==
She started career at entertainment in 2003, when she participated at "Gadis Sampul" election and become one of Top Guest 2003 in Aneka Yess!! Magazine, a famous magazine for teen girls in Indonesia, especially in Jakarta.


== Filmography ==


=== TV Series ===
Cinta 2020
Indahnya Karunia-Mu
Celana Bulu Jeans
Aku Cemburu (2006)
Jangan Berhenti Mencintaiku (2005)
Senandung Masa Puber
Beningnya Cinta (2010)
Baruak gadang (2011)


=== TV Movie ===
Cowok Gue Pendek Bener
Cowok Gue Pendek Lagi
Jadikan Aku Pacarmu
Ku Kejar Cintaku ke Bandung
Kalo Cinta Ngomong Dong!
3 Pelancong Cinta
Pacar ke 17
Manajemen Cinta 17
Cinta Rasa Mocca
Kawin Gantung
Jadikan Aku Pacarmu
Kamulah Cinta
Hanna & Hilda High Heels
Buat Gue Jatuh Cinta
Cinta Tak Bersyarat
Cinta Tanpa rekayasa
Cinta Bikin Pusing
Pacar Setengah Tiang
Cinta Dikejar Jarum Jam
Cinta Anak Petinju
Bajaj Cinta Si Laura
Pacar Bayaran
Bu Lurah Idaman Hati


== Endorsements ==
Fuji Film
Esia
Easy Splash Cologne
Fanbo (icon)


== External links ==
Profile at Showbiz.liputan6.com (Indonesian)
News at Showbiz.liputan6.com (Indonesian)