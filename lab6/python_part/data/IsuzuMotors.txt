Isuzu Motors Ltd. (いすゞ自動車株式会社, Isuzu Jidōsha Kabushiki-Kaisha) (TYO: 7202), trading as Isuzu (Japanese pronunciation: [isuzu], /iˈsuːzuː/), is a Japanese commercial vehicles and diesel engine manufacturing company headquartered in Tokyo. Its principal activity is the production, marketing and sale of Isuzu commercial vehicles and diesel engines. It also has a number of subsidiaries, included Anadolu Isuzu (a Turkish joint venture with Anadolu Group), Sollers-Isuzu (a Russian joint venture with Sollers JSC), SML Isuzu (formerly Swaraj Mazda), Jianxi Isuzu Motors (a Chinese joint venture with Jiangling Motors Company Group), Isuzu Astra Motor Indonesia, Isuzu Malaysia (Isuzu HICOM), Isuzu UK, Isuzu South Africa, Isuzu Philippines, Taiwan Isuzu Motors, Isuzu Vietnam and Isuzu Motors India. The company was established in 1916. In 1934 the company fused with the carmaker and was renamed Isuzu, after the Isuzu River.
Isuzu has assembly and manufacturing plants in Fujisawa, as well as in the Tochigi and Hokkaidō prefectures. Isuzu-branded vehicles are sold in most commercial markets worldwide. Isuzu's primary market focus is on commercial diesel-powered truck, buses and construction, while their Japanese competitor Yanmar focuses on commercial-level powerplants and generators.
By 2009, Isuzu had produced over 21 million diesel engines, which can be found in vehicles all over the world. Isuzu diesel engines are used by dozens of manufacturers, to include Ford Motor Company and Renault-Nissan Alliance.


== History ==
Isuzu Motors' history began in 1916, when Tokyo Ishikawajima Shipbuilding and Engineering Co., Ltd. plan a cooperation with the Tokyo Gas and Electric Industrial Co. to build automobiles. The next step is taken in 1918, when a technical cooperation with Wolseley Motors Limited is initiated, meaning exclusive rights to the production and sales of Wolseley vehicles in East Asia. In 1922 the first Wolseley model, the A-9 car, is domestically produced. The CP truck follows two years later; 550 of these are built until 1927. In 1933, Ishikawajima Automotive Works merges with DAT Automobile Manufacturing Inc. (a predecessor of Datsun) and changes its name to Automobile Industries Co., Ltd. The products of this company, marketed as "Sumiya" and "Chiyoda", were renamed Isuzu (after the Isuzu River) in 1934, following a meeting with the Japanese Governments Ministry of Trade and Industry (MITI). The word Isuzu translated into English means "fifty bells" - hence the focus on "bell" in both the later Bellel and the Bellett.

In 1937 Automobile Industries is reorganized and formed into a new company, Tokyo Automobile Industries Co., Ltd. It was founded with a capital of one million yen. Only in 1949 is Isuzu also adopted as the company name. Meanwhile, in 1942, Hino Heavy Industries was split off from Isuzu, becoming a separate corporation. Truck production (TX40 and TU60) began anew in 1945, with the permission of the occupation authorities. Beginning in 1953 the Hillman Minx passenger car is produced under license of Rootes Group. The Minx remained in production until 1962, after the 1961 introduction of Isuzu's first own car, the Bellel. Being a small producer making cars which were somewhat too large and pricey for the Japanese market at the time, Isuzu spent some time looking for a commercial partner. Under pressure from MITI, who were attempting to limit the number of automobile manufacturers in Japan, a cooperation with Fuji Heavy Industries (Subaru) began in 1966. The Subaru 1000 was even shown in Isuzu's 1967 annual vehicle brochure, as a suitable complement to the larger Isuzu lineup. This tie-up was over by 1968, when an agreement with Mitsubishi was formed. This ended even quicker, by 1969, and the next year an equally short-lived collaboration was entered with Nissan. A few months later, in September 1971, what was to prove a more durable capital agreement was signed with General Motors.


=== Linking with General Motors ===
The first result of GM taking a 34% stake in Isuzu is seen in 1972, only months later, when the Chevrolet LUV becomes the first Isuzu-built vehicle to be sold in the United States. To symbolize the new beginning, Isuzu also developed a new logo for 1974, with two vertical pillars which are stylized representations of the first syllable in いすゞ ("Isuzu"). In 1974 Isuzu introduced the Gemini, which was co-produced with General Motors as the T-car. It was sold in the United States as Buick's Opel by Isuzu, and in Australia as the Holden Gemini. As a result of the collaboration, certain American GM products are sold to Japanese customers through Isuzu dealerships. Holden's Statesman was also briefly sold (246 examples) with Isuzu badging in Japan during the seventies. Isuzu exports also increased considerably as a result of being able to use GM networks, from 0.7% of production in 1973 to 35.2% by 1976; this while overall production increased more than fourfold in the same period. As a result of the GM joint venture, Isuzu engines were also used by existing GM divisions (some USA-market Chevrolet automobiles had Isuzu powertrains e.g. the Chevette and early S10/S15 trucks manufactured prior to 1985).

In 1981 Isuzu began selling consumer and commercial vehicles under their own brand in the United States. The Isuzu P'Up was the first model sold to consumers as an Isuzu, rather than as a Chevrolet or Buick. Isuzu's then president Toshio Okamoto then initiated a collaboration with small-car expert Suzuki to develop a global small car for GM, the S-car. A three-way agreement of co-ownership was signed in August 1981, with Isuzu and Suzuki exchanging shares and General Motors taking a 5% share of Suzuki. Following on from this, in 1985 Isuzu and GM established the IBC Vehicles venture in the United Kingdom, producing locally built versions of Isuzu and Suzuki light vans (the Isuzu Fargo and Suzuki Carry); to be sold in the European market under Vauxhall's Bedford brand. During this period Isuzu also developed a worldwide presence as an exporter of diesel engines, with their powerplants in use by Opel/Vauxhall, Land Rover, Hindustan, and many others. Two Isuzu model lines (Gemini, Impulse) were marketed as part of the Geo division (Spectrum, Storm) when it was initially launched as a Chevrolet subsidiary. In the domestic Japanese market, OEM deals with other manufacturers were entered to aid the poorly performing passenger car arm. It led to the badging of Suzukis, beginning in 1986, and Subaru small commercial vehicles as Isuzus (Geminett, Geminett II). This OEM tie-up occurred alongside the establishment of SIA (Subaru-Isuzu Automotive), an American joint venture with Fuji Heavy Industries (the parent company of Subaru). Shortly afterwards, the Lafayette, Indiana plant became operational.


=== Contraction begins ===

1992 - Isuzu ended US sales of the Impulse (Geo Storm). The following year it stopped exporting the Stylus (the basis for the Geo Spectrum), the last Isuzu-built car sold in the US.
1993 - Isuzu began a new vehicle exchange program with Honda, whereby Honda sold the Isuzu Rodeo and Isuzu Trooper as the Honda Passport and Acura SLX, respectively. In return Isuzu began selling the Honda Odyssey as the Isuzu Oasis. Thus, Honda's lineup gained two SUVs, and Isuzu's lineup gained a minivan. In the Japanese market, the Gemini (Stylus) was now a rebadged Honda Domani and the Aska (originally based on the GM J-car) was a Honda Accord.
1996 - the Isuzu Hombre pickup, a badge-engineered GM truck (using the sheetmetal of the Brazil-market Chevrolet S10), was introduced, and Isuzu's United States sales reached a peak.

1998 - General Motors and Isuzu form DMAX, a joint venture to produce diesel engines. Isuzu resurrects the beloved Amigo. The uniquely styled VehiCROSS concept is unveiled.
1999 - GM raises its stake in Isuzu to 49%, effectively gaining control of the company. GM quickly follows this up with the appointment of an American GM executive to head Isuzu's North American Operations. This is the first time a non-Japanese executive has ever held such a high position at Isuzu. The U.S. introduction of the production version of the heralded VehiCROSS is met with mixed reviews, as its high pricetag, unique styling and two-door configuration don't seem to meet with market demands.
2001 - Joe Isuzu, the immensely popular pitchman with implausible claims, is hired once again to promote the new Axiom. Isuzu sales begin to slide due to the aging of the Rodeo and Trooper, and poor management & lack of assistance from GM. Isuzu changes the name of the 2-door Amigo convertible to Rodeo Sport in an attempt to associate it with the better selling 4-door Rodeo. Movie Spy kids features Isuzu Axiom and Trooper.
Early 2002 - Fuji Heavy Industries (Subaru's parent company) buys Isuzu's share of Lafayette, Indiana plant. Subaru Isuzu Automotive (SIA) becomes Subaru of Indiana Automotive. After 8 years of heavy Honda Passport sales and light Isuzu Oasis sales, Honda and Isuzu cooperatively end their vehicle exchange agreement. The Oasis is dropped, and Honda replaces the Passport with the Pilot. Also, it was Isuzu's last year for passenger vehicles in Canada, as Isuzus in Canada were mostly sold at Saturn-Saab dealerships.
Late 2002 - Isuzu begins the re-purchase of its stock from shareholders, primarily General Motors. Isuzu reduces GM's 49% share to 12% as part of this comprehensive recapitalization plan. As part of this reorganization, GM gains full control of DMAX and Isuzu Motors Polska, as well as ownership of all diesel engine designs from Isuzu. Isuzu drops the venerable Trooper from the North American lineup.
2003 - The Rodeo Sport is discontinued.
July 2004 - Production of the Rodeo and Axiom cease. Sales in North America slow to just 27,188, with the discontinued Rodeo and Axiom making up 71% of that total. The number of Isuzu dealerships in the U.S. begins a rapid decline.
2005 - Isuzu dealers in the United States have only 2 models: the Ascender and the i-series pickup truck. The i-series is a rebadged Chevrolet Colorado, the Ascender is a re-badged GMC Envoy. At this point, Isuzu in the United States is primarily a distributor of medium duty trucks such as the N-series. These vehicles are sourced both from Japan and U.S. plants in Janesville, Wisconsin and Flint, Michigan. Isuzu has 290 light-vehicle dealers in the U.S. as of August 2006, and sells an average of just two Ascenders per dealer per month. Plans to introduce a new Thai-built SUV, expected to be added for 2007, are shelved; Isuzu Motors Limited believes that a new SUV would be too risky and proceeds with the launch of the i-series trucks. Rumors of Isuzu's withdrawal from the U.S. market are rampant. Despite extremely low sales figures of 12,177 passenger vehicles for 2005 (with leftover Axiom and Rodeos making up 30% of this), Isuzu Motors America announces its first profit in years, mainly due to restructuring cuts.
2006 - Production of the 7-passenger Ascender ends in February with the closure of GM's Oklahoma City Assembly plant, leaving Isuzu with the 5-passenger Ascender, built in Moraine, Ohio and the low-selling i-Series as its only retail products. The company sold just 1,504 vehicles in North America in the first two months of 2006. Isuzu finally purchases its remaining shares from GM, but claims the companies will continue their current relationship. There is no word as of April 12, 2006 on the effect this will have on DMAX operations.

June 2006 - Isuzu and GM agree to establish a joint venture called "LCV Platform Engineering Corporation (LPEC)" to develop a new pickup. Isuzu says it will use its engineering expertise to develop the pickup and GM will develop derivatives based on the integrated platform.
October 2006 - Mitsubishi Corp. becomes Isuzu's largest shareholder, after it converts all the preferred share in Isuzu it has held since 2005 into common stock, increasing its shareholding from 3.5% to 15.65%.
November 2006 - Toyota purchases 5.9% of Isuzu, becoming the third largest shareholder behind ITOCHU and Mitsubishi Corporation, and the two companies agree to study possible business collaboration focusing on the areas of R&D and production of diesel engines, related emissions-control, and other environmental technologies.
January 2007 - Isuzu and General Motors update the LCV range with a 3.0 litre common rail diesel engine that has far more torque and power than its predecessor.
August 2007 - Isuzu and Toyota agree to develop a 1.6-liter diesel engine for use in Toyota vehicles sold in European markets. Details of development, production and supply of the diesel engine, are still under discussion, but in principle, Isuzu will play the leading role. Production is scheduled to begin around 2012.
January 30, 2008 - Isuzu announces complete withdrawal from the US market, effective January 31, 2009. It will continue to provide support and parts. The decision was due to lack of sales. Some of the lack of sales have been blamed on consumer experiences with low quality engines and service. Isuzu had been experiencing a slow decline since the late 1990s. In less than 10 years, they had gone from selling a complete line of cars, trucks, and SUVs, into being a specialized SUV maker, and finally selling only a pair of rebadged, General Motors Trucks. They will continue to sell commercial vehicles in the U.S.
December 17, 2008 - Isuzu, Toyota shelve development of clean diesel engine.
January 29, 2009 - Isuzu and General Motors announce that they are in talks to transfer the operation of the medium-duty truck production line in Flint, Michigan to Isuzu for a five-year period. In June, however, GM announced that these talks failed to reach an agreement, and GM instead ceased production of the Chevrolet Kodiak and GMC Topkick vehicles on 31 July 2009.
Isuzu’s Andhra plant to begin operations in 2016 


== Market presence ==

In most of Asia and Africa, Isuzu is mostly known for trucks of all sizes, after Isuzu small automobile sales drastically plummeted and Isuzu had to drop all sales of sedans and compact cars in the late 1990s. In the days when Isuzu did sell passenger cars, they were known for focussing on the diesel-engined niche. In 1983, for instance, long before the explosion in diesel sales, diesels represented 63.4% of their passenger car production. In 2009, Isuzu abandoned the United States consumer market due to lack of sales. Isuzu as a corporation has always been primarily a manufacturer of small to medium compact automobiles and commercial trucks of sizes medium duty and larger, but markets around the world show different needs.
Isuzu Motors America discontinued the sale of passenger vehicles in the United States effective January 31, 2009. The company explained to its dealers that it had not been able to secure replacements for the Isuzu Ascender and Isuzu i-Series that would be commercially viable. Isuzu sold 7,098 cars in the year 2007. This action did not affect Isuzu's commercial vehicle or industrial diesel engine operations in the United States. Isuzu has a contract with Budget Truck Rental to manufacture their rental trucks, shared with Ford, GMC, and Navistar International.
In Australia, Isuzu was for many years a major supplier of light commercial and domestic vehicles to Holden (General Motors). However, by 2008, Holden was sourcing few Isuzus. At this time Isuzu began to sell the D-Max under the Isuzu name.


== Subsidiaries and joint ventures ==
List of Isuzu Japanese facilities


=== Japan ===
The Fujisawa Plant was built and opened for production November 1961. It is located at Tsuchidana, Fujisawa, Kanagawa, and is still producing commercial vehicles for domestic Japanese use and international exports. The Toghichi Plant, located at Hakuchu, Ohira-Machi, Tochigi, Tochigi, is where the engines are currently built.


=== Mimamori-kun Online service ===
Mimamori-kun, which means to watch, monitor, or observe in Japanese, (literally "Mr. Watcher") is a commercial vehicle telematics service developed by Isuzu Motors for monitoring and tracking commercial vehicle operations and movements in Japan. The service uses GPS satellite tracking services, and began February 2004. It is connected to the internet and provides government mandated driver activity logs, and records how long the driver was on-duty and how much time was spent driving.
The service also records when the driver took lunch breaks, where the truck stopped and for how long, and when the driver logged off for his duty shift. The service has been modified for personal use in Japan to keep track of family members, to include elderly members of health status and location of children for safety purposes.
Some of the main features include Internet Digital Tachograph, the first of its kind wirelessly in Japan, combined with hands-free communication, voice guidance, and text messages displayed from the dispatch office. The system also has a password enabled vehicle theft prevention feature that will not let the vehicle start without the driver having entered a password.


==== See also ====
Telematics
Field service management
Field force automation
Mobile enterprise application framework
Mobile asset management


=== International efforts ===
DMAX (engines) - former jv with General Motors in USA for production of diesel engines
Ghandhara Industries - jv in Pakistan - trucks, buses
Guangzhou Automobile Group Bus - jv in China - buses
HICOM Automotive Manufacturers (Malaysia) - jv in Malaysia - trucks, SUVs
Industries Mécaniques Maghrébines - jv at Kairouan, Tunisia - trucks, SUVs
Isuzu (Anadolu) - jv in Turkey - trucks, buses
Isuzu Astra Motor Indonesia - jv in Indonesia - trucks, SUVs
Isuzu HICOM Malaysia - jv in Malaysia - trucks, SUVs
Isuzu Commercial Truck of America, Inc.
Isuzu Malaysia - jv in Malaysia - trucks, SUVs
Isuzu Motors Polska - former jv in Poland - diesel engines, taken over by General Motors
Isuzu Philippines - jv in the Philippines - trucks, SUVs
General Motors De Portugal-FMAT S.A. at Tramagal near Abrantes (Assembling company of all Bedford & Isuzu medium to heavy diesel trucks and 4X4 Pickup models since the 1960s then vehicles are sent for sale in Portugal and Spain)
The assembling of Isuzu commercial trucks is carried out by the Az Universal Motors which is part of the AzGroup in the Azerbaijan.
Isuzu Truck (UK) - at the former IBC factory Dunstable, England - trucks, SUVs
Isuzu Truck South Africa - jv in South Africa - trucks
Isuzu Vietnam - jv in Vietnam - trucks, SUVs
Jiangling Motors - jv in China - trucks, SUVs
Qingling Motors - jv in China - trucks, SUVs
SML Isuzu - jv in India, formerly Swaraj Mazda
Sollers-Isuzu - jv in Russia - trucks
Subaru of Indiana Automotive, Inc. - former jv in USA, interest sold to Subaru - cars, SUVs
Taiwan Isuzu Motors - jv in Taiwan - trucks
Thai Rung Union Car - Thailand - assembles SUVs
Zexel - Japan - auto components, now part of Bosch


== Important car, bus and light truck models ==


=== Passenger vehicle and SUVs ===
1953–1962, Minx, Isuzu produced Hillman Minx under licence
1961–1966, Bellel sedan
1963–1973, Bellett sedan (PR10/20) and two-door GT (PR90 & PR91)
1967–1983, Isuzu Florian
1968–1981, 117 Coupe
1974–2000, Gemini/I-Mark/Stylus sedan
1983–1992, Piazza/Impulse/Storm hatchback
1983–2002, Aska sedan
1983–2002, Trooper midsize SUV
1983–1995, Isuzu P'up and TF models Japan built from 1987–1991, US built 1991–1995
1989–1994, Amigo compact SUV (First generation)
1991–2004, Rodeo mid-size SUV also sold as the rebadged Honda Passport and Mitsubishi Montero
1991–present, Panther, a van sold in Indonesia, sold as the Isuzu Hi-Lander / Crosswind in the Philippines, also sold throughout the ASEAN, and in India as the Chevrolet Tavera
1996–1999, Isuzu Oasis minivan, a rebadged Honda Odyssey
1996–2000, Isuzu Hombre pickup truck, a rebadged Chevrolet S10
1996–2001, Isuzu Vertex sedan
1998–2003, Amigo/Rodeo Sport compact SUV (Second generation)
1999–2001, VehiCROSS halo SUV
2001–2004, Axiom midsize SUV
2002–2008, Ascender Midsize SUV, a rebadged GMC Envoy
2002–present, D-Max Pickup Truck, a top selling diesel sold in the majority of Isuzu markets (excluding North America)
2004–present Isuzu MU-7 Midsize SUV develop from Isuzu D-Max sale only in Thailand, Philippines, India, China
2006–2008, i-280/i-350 pickup truck (a product of the co-developed D-Max platform Isuzu sells overseas) I-290/I-370
in later
2011–present Isuzu D-Max Second Generation
2013–present Isuzu Mu-X Successor from Isuzu Mu-7 develop from Isuzu D-Max


=== Commercial vehicles ===
Elf - Light Duty Truck (N-Series)
Forward - Medium Duty Truck (F-Series)
Giga - Heavy Duty Truck (C-Series, E-Series)
Gala - Heavy Duty Bus
Gala Mio - Medium Duty Bus
Erga - Low Deck Heavy Duty Bus
Erga-J - Heavy Duty Bus
Erga Mio - Low Deck Medium Duty Bus
Journey - Light Duty Bus
Journey-J - Medium Duty Bus
H-Series - Heavy Duty Truck in United States only (Rebadged from GMC Topkick and Chevrolet Kodiak)
Reach - commercial van offering over 35 percent better fuel efficiency, assembled by Utilimaster Corporation


=== Race cars ===
1969 R7, Group 7 racecar
1970 Bellett R6, Group 6 racecar


=== Concept cars ===
1970 Bellett MX1600
1983 COA
1985 COA-II
1987 COA III, AWD mid engine coupe
1989 Costa
1989 MultiCROSS
1989 4200R
1991 Como, a pickup-style crossover with a Lotus Formula One engine (the name was later used for the rebadged Nissan Caravan E25 produced from 2001)
1991 Nagisa
1991 Terraza
1993 XU-1
1993 VehiCROSS
1995 Deseo
1995 Aisance
1997 VX-2
1997 Isuzu Zaccar
1999 Isuzu VX-O2
1999 Revolutionary Vehicle-KAI
1999 ZXC
2000 Isuzu VX-4
2001 Zen
2001 GBX


=== Buses (Philippines) ===
LV314K
LV314L
CJM470
CJM500
LT132
LV423
LV123
PABFTR33PLB
FTR33P
FTR45
PABFVR33P


=== Buses (Thailand) ===
CQA650A/T
JCR600YZNN
LT112P
LV223S
LV423R
LV486R
LV771
MT111QB


== See also ==

List of automobile manufacturers
General Motors Company
Ghandhara Industries
List of Isuzu engines
Zexel


== References ==


== External links ==
Isuzu Worldwide
Isuzu corporate website (Japanese)
Official Isuzu South Africa
Isuzu Surabaya Indonesia
Explanation of Mimamori-kun (English)
Isuzu launches Mimamori in Japan