Pitsea railway station is on the London, Tilbury and Southend Railway line, serving the small town of Pitsea in the borough of Basildon, Essex. It is situated at a junction where a loop line via Tilbury Town re-joins the main line via Basildon. Down the main line it is 26 miles 42 chains (42.7 km) from London Fenchurch Street; via the alternative loop it is 32 miles 37 chains (52.2 km) down-line. Its three-letter station code is PSE.
It was originally opened in 1855 but was replaced by a new station on an adjacent site in 1888. The station was renamed Pitsea for Vange in 1932, but reverted to the original name Pitsea in 1952.
The station and all trains serving it are currently operated by c2c.


== Description ==
The station is immediately south of the A13 road, adjacent to a level crossing which gives the main road access to the marshes area south of Pitsea and Basildon.
A new station building was opened in October 2005. Derek Twigg (then rail minister) attended for the "ribbon cutting" in November 2005. The building houses customer toilets and a retail unit. The station also has four automatic ticket gates.
The ticket office has two serving positions and uses the Tribute issuing system. Outside the ticket office is a self-service ticket machine that takes payment by both cash and cards.


== Services ==
The typical Monday-Friday off-peak and Saturday service pattern is:
2 trains per hour to London Fenchurch Street via Basildon;
2 trains per hour to London Fenchurch Street via Ockendon;
2 trains per hour to Shoeburyness;
2 trains per hour to Southend Central.
Additional services to/from London start/terminate here during rush hour.


== References ==


== External links ==
Train times and station information for Pitsea railway station from National Rail