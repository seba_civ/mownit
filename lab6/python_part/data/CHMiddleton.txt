Cecil Henry Middleton (22 February 1886 – 18 September 1945), widely known simply as "Mr. Middleton", was a gardener, writer and one of the earliest radio and television broadcasters on gardening for the BBC. Middleton broadcast in Britain during the 1930s and 40s, especially in relation to the "Dig for Victory" campaign during the Second World War. Many of his wartime talks appeared also in print.


== Background ==
Middleton was born in Weston by Weedon, Northamptonshire on 22 February 1886. Gardening was the family trade. Middleton was the son of John Robert Middleton, who was employed as head gardener at Weston Hall by Sir George Sitwell, father of the talented trio of Edith, Osbert and Sacheverell, all of whom Cecil Middleton came to know well as a child, probably as a playmate. On his marriage certificate on 10 August 1912 in London to Rosa Annie Jenkins, Middelton was listed as a Horticultural Instructor. He left Northampton to work first in the seed trade, then spent time as a student at Kew Gardens.


== Broadcaster and wartime fame ==


=== In Your Garden and Dig for Victory ===
Middleton's broadcasting career began when Colonel Frank Rogers Durham (1872–1947), Secretary of the Royal Horticultural Society, recommended him to the BBC for radio talks. Others considered for the role had included the seasoned broadcaster Vita Sackville-West who, in the coming years, created a world famous garden at Sissinghurst. Middelton's first programme was on 9 May 1931 and, from 1934 onwards, he broadcast regularly on Sunday afternoons at 2.15 p.m. a series of gardening talks entitled In Your Garden. These talks continued until 1939 when the BBC and Ministry of Agriculture extended the series to include topical advice about what became the "Dig For Victory" campaign and launched the complementary Kitchen Front programme.
By 1940, 3.5 million listeners were tuning in to hear Middleton's 15 minute talks from the BBC's studios at Evesham (the corporation having dispersed or evacuated many of its departments in wartime). These broadcasts, which were extremely successful and listened to both by practical gardeners and those who "only dreamed of gardening", were published in book form during World War II and have since been reprinted. Hs innate humour, which he deployed when allowed to do so, once led him to remark (with reference to the consequences of German bombing) on the availability of mortar rubble for liming soil. In 1942 research into listening habits suggested that over 70% of people in Britain with wireless sets listened to advice about gardening; of these, almost 80% referred specifically to In Your Garden, which was far and away the best known programme dealing with the subject. When the Allotments Bill was debated in Parliament in 1950, the Minister of Agriculture Tom Williams recalled that "until his death [in 1945], Mr. Middleton stimulated and encouraged us all by his avuncular advice every Sunday after lunch".
In addition to giving practical advice, Middleton was unafraid to confront issues of public policy: for example, as early as 1940, he was concerned that the Dig for Victory campaign was focused too much on urban areas, thus tending to overlook the contribution of rural gardeners who often had more space available, with the potential for greater yields. He was also comparatively adventurous for his time, for example, advising listeners in 1940 to sow a row of garlic. However, although his influence on wartime food cultivation was considerable, Middleton's personal passion was for flowers, one colleague later remarking that "he could not love an onion where a dahlia might grow".


=== Early television ===
Television gardening broadcasts began on 21 November 1936 when Middleton presented In Your Garden from a purpose-built plot at Alexandra Palace in the first month of the BBC's official television service. Such was Middelton's fame, even at this stage, that a comic actor, Nelson Keys, dressed in a mangy coat, impersonated him on television, making such irreverent observations as "the thistles are doing nicely today". At the annual Radiolympia exhibition in August 1939, just before the outbreak of war, Middleton appeared in 'Television Avenue', tending a replica of the garden at Alexandra Palace, of which there was also a sixty-five-foot high model. After the suspension of television the following month, Middleton never used that medium again.


=== Relationship with the BBC ===
Despite his evident popularity, Middleton's relationship with the BBC was sometimes a little strained and he was treated with a degree of meanness and condescension. For example, during a spell of bronchitis, when his talks had to be relayed by an announcer, his fee was reduced and, after his home was bombed in 1940, obliging him to live with relatives in Northamptonshire, his claim for additional travelling costs was dismissed in an internal memorandum as "grabbing". He was also prevented from appearing on the rather highbrow discussion programme The Brains Trust on the basis that "he is an amateur expert". Even so, towards the end of the war, the corporation acknowledged the importance of his contribution:

Special series of talks for 'back-yarders' have ... for four years guided a new national movement towards self-sufficiency; it would be hard to write a social history of the war years without mentioning Mr. Middleton.


=== Other work ===
Middleton wrote for the Daily Express and was also an advisor and writer for publications by Boots stores, which promoted itself as 'The Gardener's Chemist'. He also appeared in British Pathe Newsreels. Other films included Ministry of Information film shorts such as Blitz on Bugs as a voice or in animated cartoon form.


== Death and legacy ==
Middleton died of a heart attack outside his house at Princes Villa, Surbiton on 18 September 1945. Warm tributes were paid to him and flowers were sent to his funeral from all over Britain, although an obituary in the Daily Express noted that, ironically, his own front garden was noticeably unkempt. Middleton is remembered in the allotment gates of the Weston and Lois Weedon Horticultural Society, Northants, where he was a founder member in 1940. This Society still remembers him at their annual horticultural shows, as he did much to promote the horticultural shows and societies continuing in wartime.
Middleton was the first in a long line of British "celebrity" gardeners, from Percy Thrower to Geoff Hamilton and Alan Titchmarsh, who became famous through radio or television. He received much fan mail and letters relating to gardening, and listeners and readers contributed by public subscription towards a set of memorial gates to his radio garden allotment in London near the Old Langham Hotel in London. These have since been moved to BBC Written Archives Centre at Caversham, near Reading. Describing Middleton as "the Gert and Daisy of the gardening world", historian Philip Ziegler cited the immense popularity of his wartime broadcasts which "did as much as anything to convince doubters that running an allotment was a pleasant and profitable pursuit". Indeed, such was his contemporaneous fame that, in 2008, author Byron Rogers observed that Middleton was "the first English national working class hero, apart from footballers and hangmen".


== Publications ==
Mr. Middleton Talks About Gardening, 1935
More Garden Talks, 1936
Winter-Flowering Plants for Outdoor Borders, 1937
Colour all the Year in My Garden, 1938
Your Garden in Wartime, 1941 (reprinted Aurum Press, 2010)
Mr. Middleton's All The Year Round Gardening Guide, 1945 (reprinted Aurum Press)
Mr. Middleton's Garden Book, Morrison and Gibb Ltd for The Daily Express, date unknown


== External links ==
Colour footage of Mr Middelton with Elizabeth Cowell at RadiOlympia, 1938 on YouTube
World War Zoo gardens project blog
'www.wartimegardening.wordpress.com (modern tribute)Blog following Mr Middleton's gardening advice today
British Pathe Newsreel link for Mr Middleton, Pictorial Personalities, In Your Garden, 1944
http://www.bbc.co.uk/historyofthebbc/resources/in-depth/gardening.shtml


== References ==