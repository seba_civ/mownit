Uchechukwu Peter Umezurike (or Uche Peter Umez) (born January 1, 1975) is a Nigerian author. Umez's first published work of poetry, Dark through the Delta, deals with the recurring despoliation of Nigeria using the Niger Delta as its motif.
A graduate of Government & Public Administration from Abia State University, Umez is also the author of Tears in her Eyes (short stories) and Aridity of Feelings (poems). He has a master’s degree in English Studies from the University of Port Harcourt and is currently working on another short story collection and a novel.


== Works ==


=== Children's Fiction ===
Sam and the Wallet (Funtime TV Enterprises, 2007)
“Tim the Monkey and Other Stories” (Africana First Publishers, 2013)
“The Boy Who Throws Stones at Animals and Other Stories” (Melrose Books, 2011)
“The Runaway Hero” (Jalaa Writers’ Collective, 2011)


=== Short fiction ===
Tears in Her Eyes (Edu-Edy Publications, 2005)


=== Poetry ===
Aridity of Feelings (Edu-Edy Publications, 2006)
Dark through the Delta (Edu-Edy Publications, 2004)


== Awards and grants ==
Fellow, Civitella Ranieri, Italy, 2012
Shortlisted, Nigeria LNG Prize for Literature, 2011
Fellow, Chateau de Lavigny, Switzerland, 2010
Laureate, UNESCO-Aschberg, Sanskriti Kendra, India, 2009
Alumnus, Caine Prize for African Writing Workshop, Accra, Ghana, 2009
Alumnus, International Writing Program, Iowa City, USA, 2008


== References ==