E. Jean Carroll (born December 12, 1943) is an American journalist and advice columnist. Her “Ask E. Jean” column has appeared in Elle magazine since 1993, and was ranked one of the five best magazine columns (along with Anthony Lane of The New Yorker and Lewis Lapham of Harper's Magazine ) by the Chicago Tribune in 2003.
Born Elizabeth Jean Carroll in Detroit, Michigan, and nicknamed "Betty Jean" from an early age, Carroll's opinions on sex, her impatient, boisterous counsel that women should “never never” wrap their lives around men, and her compassion for letter-writers experiencing life’s hard knocks, make her column unique in women’s magazines.
Amy Gross, former editor-in-chief of Elle and currently the editor-in-chief O, The Oprah Magazine, recalls the “Ask E. Jean” debut. “It was as though we had put her on a bucking bronco and her answers were the cheers and whoops and hollers of a fearless woman having a good ol time.”
NBC’s cable channel, America's Talking, produced the Ask E. Jean television show based on the column from 1994-1996 (when the channel became MSNBC). Entertainment Weekly called Carroll “[T]he most entertaining cable talk show host you will never see.”  Jeff Jarvis in his review in TV Guide said watching E. Jean and her “robotic hyperactivity drove [him] batty.” He went on: “However then I listened to her, and couldn’t help liking her. E. Jean gives good advice.”  Carroll was nominated for an Emmy for her writing for Saturday Night Live, (1985) and a Cable Ace Award for the Ask E. Jean show (1995).
The AskEJean.com website, based on the Elle column, is an on-going experiment in the gripping ways people give and get advice. Users can type in questions and receive instant video answers on topics such as career, beauty, sex, men, diet, "sticky situations," and friends. Or, users can join the Advice Vixens (a section where “YOU become the advice columnist”). "Top Campus Sex Columnists" features the best college advice columnists from across America.


== Journalism and Books ==
In 2002 Carroll's “The Cheerleaders” which appeared in Spin, was selected as one of the year's “Best True Crime Reporting” pieces. It appeared in Best American Crime Writing edited by Otto Penzler, Thomas H. Cook, Nicholas Pileggi (Pantheon Books, 2002).
Carroll has been a contributing editor to Esquire, Outside and Playboy magazines. Her beat is “the heart of the heart of the country”. For an April 1992 issue of Esquire, she chronicled the lives of basketball groupies in a story called “Love in the Time of Magic.” In June 1994, she went to Indiana and investigated why four white farm kids were thrown out of school for dressing like black artists in “The Return of the White Negro”.
In “The Loves of My Life”, (June 1995), she tracked down her old boyfriends and moved in with the fellows and their wives. Bill Tonelli, her Esquire and Rolling Stone editor has said: “All of E. Jean’s stories are pretty much the same thing. Which is: ‘What is this person like when he or she is in a room with E. Jean?’ She’s institutionally incapable of being uninteresting.”
For Playboy (February 1988) at the height of the “Sensitive Man” era, E. Jean told her editors that "modern women run around complaining that they want a primitive man, so I thought it would be fun to come to New Guinea and find a real one.” Carroll hiked into the Star Mountains, with an Atbalmin tracker who stood 4’2” and a Telefomin warrior. She became the first white woman to walk from Telefomin to Munbil in the former West Irian Jaya, and came close to perishing.
For Outside, Carroll wrote about (among other things) taking Fran Lebowitz camping and going down the Colorado with a group of “Women Who Run With No Clothes On.” Several of E. Jean’s pieces for Outside have been included in various Non-Fiction collections such The Best of Outside: The First 20 Years (Vintage Books, 1998), Out of the Noosphere: Adventure, Sports, Travel, and the Environment (Fireside, 1998) and Sand in My Bra: Funny Women Write from the Road (Traveler’s Tales, 2003).
E. Jean Carroll authored four books:
Female Difficulties: Sorority Sisters, Rodeo Queens, Frigid Women, Smut Stars, and Other Modern Girls (Bantam Books, 1985)
A Dog in Heat Is a Hot Dog and Other Rules to Live By (a collection of her Ask E. Jean columns, Pocket Books, 1996)
Hunter: The Strange and Savage Life of Hunter S. Thompson (Dutton, 1993)
Mr. Right, Right Now (HarperCollins, 2004)


== Websites ==
In 2002, Carroll got “sick sick sick of women writing to me asking how to find a man,” and co-founded (with her sister, Cande Carroll) Greatboyfriends.com. On the site, women recommend their ex-boyfriends to each other. (The Oprah Winfrey Show did a show about it in 2003). The Knot, Inc. bought GreatBoyfriends in 2005. In 2004 she launched Catch27.com as a spoof of Facebook. On the site, people put their profiles on trading cards and buy, sell, and trade each other. The Boston Globe headline was “You Can’t Buy Friends Like These...Well, Actually You Can.”  AskEJean.com was launched in 2007. The Top Campus Sex Columnists (a section of the AskEJean site) is the only select gathering of college columnists in the world.


== Biography ==
Her father, Tom Carroll, is an inventor, and her mother, Betty Carroll, is a retired Allen County Indiana politician. Betty Jean Carroll was raised in Fort Wayne, Indiana. She attended Indiana University where she was a Pi Beta Phi and a cheerleader and was crowned Miss Indiana University. In 1964, representing Indiana University, she won the Miss Cheerleader USA title. Carroll appeared on the mid-1980s edition of Card Sharks hosted by Bob Eubanks. She currently resides in upstate New York.


== References ==


== External links ==
E. Jean Carroll at the Internet Movie Database
Ask E. Jean
Ask E. Jean's AdviceVixens
Elle Magazine