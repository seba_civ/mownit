Brechen is a community in Limburg-Weilburg district in Hesse, Germany.


== Geography ==


=== Location ===
Brechen lies in the southeastern part of the Limburg Basin (Limburger Becken) between the Taunus and the Westerwald. The sparsely wooded land of loess hills is crossed here from southeast to northwest by the Emsbach, which is fed near Niederbrechen by the Wörsbach and drains the area down to the Lahn. Together with the Idsteiner Senke (basin), which joins it in the south, this patch of countryside is customarily known as the Goldener Grund (“Golden Ground”), a reference to the favourable climate and the fruitful earth.


=== Neighbouring communities ===
In the northwest the community of Brechen borders on the district seat of Limburg, in the north on the town of Runkel, in the northeast on the market town of Villmar, in the southeast on the community of Selters and in the southwest on the community of Hünfelden (all in Limburg-Weilburg).


=== Constituent communities ===
Brechen’s three Ortsteile are Niederbrechen (administrative seat as well as biggest of the three), Oberbrechen and Werschau.
Within the framework of administrative reform in Hesse on 31 December 1971, the community of Brechen came into being through the amalgamation of the formerly autonomous communities of Werschau and Niederbrechen. Since 1 July 1974, Oberbrechen has also belonged to the community.


== History ==


=== Roman camp ===
Very early in Brechen’s history, there was a Roman military camp in what is now the municipal area, at a site now known as Alteburg (“High Castle”), lying on the Emsbach.
The camp’s south frontage has a length of 140 m joining two sidewalls with respective lengths of 90 and 135 m.
The camp came to light after finds made with metal detectors led the regional archaeologist Dr. Schade-Lindig from the Hesse state office for memorial maintenance to carry out the first investigations in the Großer Wald (“Great Forest”) east of Oberbrechen between 1999 and 2001. Lying near a group of Hallstatt-era barrows, the site had long been known to contain something historical, but since it had never been properly investigated, it had always been taken to be a sconce from the Thirty Years' War (1618–1648). In particular, the site’s good state of preservation had led to this interpretation, but already by 1915, a denarius from Augustus’s time had been found there. The latest investigations have yielded the knowledge that this site was girded by a typical Roman V-shaped ditch, within which spread an area of roughly two hectares. Magnetic investigations show a gate in the eroded northern part. Thus far, no building within the site has been found, and during digs, no datable finds were unearthed.
Private finds from this area point to the likelihood of a marsh camp from Augustus’s time


=== Middle Ages ===
In a donation document from the Lorsch Abbey dated 12 August 772, Niederbrechen and Oberbrechen had their first documentary mention under the name Brachina (“at the mountain slope”). In the time that followed, ownership of the two places passed to St. Maximin's Abbey at Trier. Over many centuries, ownership shifted back and forth between the Electorate of Trier and the Counts of Molsberg.
The communities had importance as they lay on the long-distance trade road between Cologne and Frankfurt (Via Publica) as advance posts of Electorate of Trier territory for those going towards Frankfurt. Niederbrechen temporarily held town rights in the Middle Ages and had a town wall, parts of which are still preserved today.
The Berger Kirche (church) standing in the municipal area has existed since Carolingian times and was the region’s mother church. It was the local church for the now forsaken village of Bergen – hence the church’s name – which is believed to have vanished sometime between 1354 and 1490. The church’s first documentary mention was in 910; parts of today’s building can be dated to about the turn of the second millennium.
The constituent community of Werschau had its first documentary mention in 1235.


=== Modern times ===
In 1802, the municipal area became part of the Principality of Nassau-Weilburg, which itself passed to Prussia in 1866. As of that year, the three centres that now make up Brechen belonged to the district of Limburg; since 1974 they have belonged to the district of Limburg-Weilburg in Hesse.


== Politics ==


=== Community council ===
The municipal election held on 26 March 2006 yielded the following results:


=== Coat of arms ===
The community’s arms are quartered with each of the smaller fields in the escutcheon bearing a symbol of a constituent community’s patron saint, namely a bear (Saint Maximin), a dragon (Saint George) and a rose (Saint Felicity), while the fourth field bears the red Trier cross in reference to the community’s one-time territorial allegiance to the Electorate of Trier.


== Economy and infrastructure ==
The formerly dominant industry of agriculture is still important today with almost 70% of the community’s area being worked by fewer than ten full-time operations. Brechen has, however, developed into a residential community, 90% of whose working inhabitants earn their livelihood in surrounding towns, mainly in the Frankfurt Rhein-Main Region.


=== Transport ===
Brechen is well linked to the long-distance road network by way of the A 3 (Cologne–Frankfurt, Limburg-Nord interchange) lying 6 km away. Through the community runs Bundesstraße 8.
At the Limburg-Süd interchange also lies the Limburg-Süd railway station on the InterCityExpress’s Cologne-Frankfurt high-speed rail line with hourly trains offering a quick link to Frankfurt Airport (17 min.), Cologne Bonn Airport and the rest if the ICE network.
The railway stations Niederbrechen and Oberbrechen lie on RMV line 20 (Main-Lahn-Bahn) between Limburg an der Lahn and Frankfurt am Main.


=== Education ===
The community has at its disposal one primary school and one Hauptschule with a Realschule branch in Niederbrechen as well as a further primary school in Oberbrechen. Other secondary schools are to be found nearby in Limburg.


=== Public institutions ===
Schule im Emsbachtal in Niederbrechen
Grundschule Oberbrechen in Oberbrechen
Kindergarten Niederbrechen, In der Schlei 45
Kindergarten Niederbrechen, Westerwaldstraße 1-3
Kindergarten Oberbrechen
Kindergarten Werschau
Niederbrechen Volunteer Fire Brigade, founded 1897
Oberbrechen Volunteer Fire Brigade, founded 1895 (includes Youth Fire Brigade)
Werschau Volunteer Fire Brigade, founded 1927 (includes Youth Fire Brigade)
Niederbrechen Catholic public library
Oberbrechen Catholic public library
Werschau Catholic public library
Niederbrechen gymnastics club


== Famous people ==


=== Sons and daughters of the town ===
Joseph Neuhäuser (1890–1949), composer
Walter Neuhäusser (1926- ), architect
Albert Otto (b. 1885 in Oberbrechen, d. 1975), artist


=== People connected with the community ===
Peter Josef Blum (1808–1884), Bishop of Limburg


== References ==


== Further reading ==
F.-R. Herrmann und A. Jockenhövel: Die Vorgeschichte Hessens. 1990, S.329f.
F.-R. Herrmann: Römerüberraschung. Archäologie in Deutschland 4, 2001, S.41.


== External links ==
Oberbrechen’s homepage
Brechen at DMOZ
Chronik und Geschichte des Römerlagers Oberbrechen