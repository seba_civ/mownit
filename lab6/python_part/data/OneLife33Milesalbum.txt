One Life is the second studio album from contemporary Christian band, 33Miles, released on September 16, 2008 through Columbia Records. In October, the album peaked at No. 161 on the Billboard 200 and No. 4 on Billboard's Top Christian Albums chart.


== History ==
You only get just one time around. You only get one shot at this-one chance to find out the one thing that you don't want to miss. One day when it's all said and done, I hope you see that it was enough: this one ride, one try, one life to love.
The developing story of talented pop trio 33Miles goes hand in hand with the message of its completely heart-stirring second album, One Life. Group members Jason Barton (lead vocals), Chris Lockwood (guitar, vocals) and Collin Stoddard (keys, vocals) were taking a risk, giving their dreams a shot when they released their self-titled debut less than two years ago.
Validation was seemingly instant. Concerts at more than two hundred churches nationwide ensued, and radio spun several of the band's songs into hits. Journalists also took notice, calling these humble guys "a talented and devoted group of performers who possess some amazing gifts for music" (InFuze Magazine). Soon enough, 33Miles was a Gospel Music Association Dove Award nominee for New Artist of the Year.
Now, One Life reveals the heart of a group even more intent than before on loving the church with transparency. Committed family men who are as fun to meet personally as they are to hear in song, each member of 33Miles is married, and two have children. They've kept a constant presence at home and still managed to put 120,000 miles on the touring van over the past year, not to mention the thousands traveled by air.
The road isn't always easy (ask Chris about the car that crashed into their dressing room and started a gas leak), and the skyways have their turbulence (ask Collin about that landing during a tornado watch), but the rewards are eternal.
Jason puts One Life in perfect perspective: "The theme of the album is simple, it is about loving God and living every mile to the fullest."
Indeed, One Life resounds with the experience of casting off safety nets and following dreams. First single "One Life to Love" tells of men and women who finally learn to love their families and God more than work and money, played out across a powerful melody and musical track.
"'One Life to Love' still gives me chills," says Jason. "It also goes back to the meaning behind 33Miles-the idea that life can be short, that Christ lived only thirty-three years on this earth. So the question remains: what will you and I do with our miles, with the years we are given?"
Collin sees the song in relation to the band's unfolding adventure, how each member could have stayed put where they were, but instead chose to answer a no-guarantees call.
"We joke with each other about leaving school early or foregoing other job opportunities to be in 33Miles, but the song says, you only get one time around to find the one thing you don't want to miss, and this group is clearly a God-given opportunity to honor Him."
As implied, the formation of 33Miles didn't come without its leaps of faith. Jason was already an established studio and touring vocalist. Chris was about to enter Boston's eminent Berklee College of Music (John Mayer, Diana Krall) on a scholarship, and Collin was just one semester shy of a commercial music degree. But there's no doubt today about the appeal and strength that's found in the sum of their parts.
Like 33Miles' acclaimed debut, One Life was produced by Nathan Nockels (Passion, Point of Grace, Phillips, Craig and Dean) and Sam Mizell (Matthew West), adding top song-shaper Brown Bannister (Third Day, MercyMe) into the mix for the first time on "Something Different," "Gone" and "Apologize." The former cuts represent the muscular, rootsy pop/country tinge that flavors half the album and flows naturally from Jason's Louisiana upbringing, Collin's love for classic rock, and the passion Chris has for his well-worn six-string. "Apologize," a stand alone ballad with a classic ring to it, more quietly highlights the vocal and piano gifts of Barton and Stoddard, pleading to the wronged one: if I left a scar, let me say I'm sorry.
Deeper in, "When It All Comes Down" and "One of Those Days" also carry an upbeat style comparable to today's biggest selling country pop acts.
'"One of Those Days,'" admits Lockwood, "We've all said it . . . 'Ugh, it's just one of those days.' But this song puts a spin on that statement. Some days have their clutter and distraction, but God is worthy to be praised every day, so hallelujah anyway!"
Certainly, a signature element found throughout the music of 33Miles is the expressive worship so evident on the urgent "Jesus Calling" and unforgettable "My Offering."
"My favorite song, at this moment, is 'Jesus Calling.' It captures the sound 33Miles has looked for over the past year. When you come to our concerts, this is what you're gonna hear live," Chris says with enthusiasm. "And the message is clear: even though the world is so full of noise today, if you just listen, Jesus is always speaking in and through it all."
The performance merits of "My Offering" are no less definitive of 33Miles; shining examples of the band's exceptional, emotional vocal interplay, chiming guitars, and a poetic lyric for the church that could just as easily stir people outside the chapel walls:
I cursed your name; You called me child. I was to blame; You went to trial. And I lived to take . . . On my own I made a mess of things, but You take it as my offering.
At every turn, One Life grippingly offers the listener a reminder of what matters most: tuning in ever closer to God and those we love, aligning our earthly dreams with heavenly things. Be watching for the mile markers that you don't want to miss.


== Track listing ==
"Gone" - 3:39
"Jesus Calling" - 4:45
"One Life to Love" - 3:59
"Something Different" - 3:40
"Apologize" - 4:19
"Just One of Those Days" - 4:05
"When It All Comes Down" - 3:11
"I Loved You Then" - 3:53
"My Offering" - 4:25
"Little Bit of Love" - 3:47


== Radio Singles ==
"One Life to Love" Hot Christian Songs peak: #10
"Jesus Calling" Hot Christian Songs peak: #26


== References ==


== External links ==
33Miles' official website