Eight-man football is a form of gridiron football, generally played by high schools with smaller enrollments. Eight-man football differs from the traditional eleven-man game with the reduction of three players on each side of the ball and a field width of 40-yards; 13 1/3 yards narrower than the 53 1/3-yard eleven-man field. Most states continue to play on a 100-yard length field, whereas a few states opt for 80-yard lengths. Reduced-player football, which consists of eight-man, six-man, and nine-man football has gained popularity across the United States. As of 2015, 1,561 schools in 30 states sponsor reduced-player football, with 1,161 of those teams participating in eight-man leagues, whereas 284 teams play six-man football and 116 teams play nine-man football.


== Overview of Eight-man Football ==
Eight-man football shares the same rules, procedures, and structure as the traditional eleven-man game, with a few minor differences. Eight-man football is played with eight players on offense and defense, three less than the eleven-man game. It depends greatly on the type of formation used, but the eliminated players are commonly two offensive tackles and a skill position player on offense and two defensive backs and a defensive lineman on defense.
The size of the playing field is often smaller in eight-man football than in eleven-man. To accommodate six fewer players on the field, the width of the field is 40-yard-wide (37 m), 13 1/3-yards narrower than the 53 1/3-yard eleven-man field. Most eight-man leagues mandate 100-yard length fields, where few choose the 80-yard-long (73 m) field length option.
There are several professional eight-man football leagues in the United States, due to the eight-man format being adopted by most indoor football leagues. These leagues typically use a 50-yard (46 m) by roughly 25-yard (23 m) field, as professional eight-man football is usually played indoors. There are some eight-man leagues that play outdoors, however; in Texas the American Eightman Football League (AEFL) plays on a 100-yard field, and in Illinois and Missouri, the Eight Man Football League (8FL) plays on a 60-yard (55 m) field. In recent years, organizations that previously played six-man football have been converting to eight-man football, leading to the expansion of the eight-man game.


== Eight-man Teams by State ==
Note: States with limited eight-man teams may be affiliated with out-of-state leagues


== High School Eight-man Football ==
Of the 30 states that sponsor the 1,161 eight-man teams in the nation, teams are categorized by "class", "division", or "districts" with sub-conferences within each. States elect to use an either playoff system, a "bowl game" format (Jamboree), or for states with few eight-man teams, no official postseason is organized, instead electing for "Conference Champions".
Playoff Format States that elect a playoff format will seed teams based on regular season records and conference standings. Depending on the sizes of each class, division, or district, the playoff bracket is adjusted accordingly. Teams will advance through the bracket until a state champion is crowned.
Bowl Game Format  States that elect a bowl game format, also known as a Jamboree, will seed teams based on regular season records and pair them against like-seeded opponents (i.e. #1 vs #1, #2 vs #2, #3 vs #3, and #4 vs #4). In this format, teams play one postseason game as there is no advancement through levels as in a playoff format. Wisconsin currently uses this format for postseason eight-man games.


== Game play ==
Eight-man football consists of fast-paced games with higher scoring than the traditional game. Eight-man scores vary depending on the offensive and defensive strategies, but scores typically fall in the 40-60 point range, with "high scoring" games reaching the 70s and "low scoring" games falling below 30. Eight-man football is noted for producing multi-skilled players that are responsible for playing several positions, which require speed, agility, and strength. Eight-man players each account for 12.5% of the responsibility in eight-man football, compared to 9.09% responsibility in the eleven-man format, due to having fewer players.


=== Offense ===
A variety of offensive formations can be used in eight-man football, most of which are converted from traditional eleven-man formations. Eight-man football rules require five players to be on the line of scrimmage with players on each end remaining pass eligible. The interior of the line consists of two guards and a center. Most often, the line players on the edges of the formation are tight ends, or are occasionally split wide as wide receivers. Due to reduced sized teams requiring players to know different positions, players' jersey numbers do not affect pass eligibility, however, most teams follow the general guidelines set forth by the eleven-man game.

Attempting the extra point kick after a touchdown is less common in eight-man, due to the lack of specialized kickers and holders and the inability to block defenders from interfering with the kick. For this reason, teams often attempt a two-point conversion instead.


=== Defense ===

General defensive alignments in eight-man football consist of defensive linemen, linebackers, and defensive backs. Common formations include a 3-3-2, 3-2-3, 4-3-1, 3-4-1, 4-2-2, 5-3, and a 6-2 goal-line defense. The 3-2-3 defense has gained popularity due to the increase of passing-oriented offense in the eight-man game. It substitutes a third defensive back for a linebacker.


=== Special Teams ===
Eight-man football includes special teams units similar to the traditional format. One notable difference is significantly fewer teams using field goal or extra point units, instead electing to go for a fourth down conversion or a two-point conversion.


== Notable Reduced-Player Football Alumni ==
Every year, eight-man football players, as well as other reduced-player football players, receive scholarships and/or opportunities to play collegiately. Below is a list of notable reduced-player football alumni. 
Rashaan Salaam - (born October 8, 1974) is a former American college and professional football player who was a running back in the National Football League (NFL) for four seasons during the 1990s. Salaam played college football for the University of Colorado and won the 1994 Heisman Trophy. He was picked by the Chicago Bears in the first round of the 1995 NFL Draft, and played professionally for the Bears and Cleveland Browns of the NFL. Collegiately, in addition to winning the Heisman Trophy, Salaam was a unanimous All-American selection and awarded the Walter Camp Award (1994), Doak Walker Award (1994), and Jim Brown Award (1994). His NFL career lasted five seasons, along with two seasons spent in the Canadian Football League. He is the youngest player to rush for 1,000 yards in a season (21 years, 77 days old).
Josh Brown - (born April 29, 1979) is an American football placekicker for the New York Giants of the National Football League. He was drafted by the Seattle Seahawks in the seventh round of the 2003 NFL Draft. He played college football at Nebraska. Brown was a member of the 2005 Seattle Seahawks NFC Champion team. He was also awarded the PFW Golden Toe Award in 2006.
Nolan Cromwell - (born January 30, 1955) is an American football player and coach who currently serves as a senior offensive assistant for the Cleveland Browns. He was an All-Pro safety for the Los Angeles Rams of the NFL and played for the University of Kansas in college, where he earned All-American honors. Cromwell played for the Rams from 1977 through 1987 and was named to the Pro Bowl in four consecutive years, 1980 through 1983. He played on the Rams' 1979 - 1980 Super Bowl XIV team. He was the Rams' wide receivers coach from 2010 to 2011. He was named the Wichita Eagle’s high school football player of the decade for the 1970s.
Chad Greenway - (born January 12, 1983) is an American football linebacker for the Minnesota Vikings of the National Football League (NFL). He played college football at Iowa, and was drafted by the Vikings in the first round of the 2006 NFL Draft. He was a two-time Pro Bowl selection (2011, 2012) and Second-team All-Pro (2012). He was awarded the Ed Block Courage Award (2007) and was the NFC Combined Tackles Leader (2010) and also ranked #70 in the Top 100 NFL Players of 2013.
Jack Pardee (Six-man) - (April 19, 1936 – April 1, 2013) was an American football linebacker and the only head coach to helm a team in college football, the National Football League, the United States Football League, the World Football League, and the Canadian Football League. Pardee was inducted into the College Football Hall of Fame as a player in 1986. As a teenager, Pardee moved to Christoval, Texas, where he excelled as a member of the six-man football team. He was an All-American linebacker at Texas A&M University and a two-time All-Pro with the Los Angeles Rams (1963) and the Washington Redskins (1971). He was one of the few six-man players to ever make it to the NFL, and his knowledge of that wide-open game would serve him well as a coach.
Dean Steinkuhler - (born January 27, 1961) is a former American college and professional football player who was an offensive lineman in the National Football League (NFL) for eight seasons in the 1980s and 1990s. Steinkuhler played college football for the University of Nebraska, and was recognized as an All-American. While playing collegiately, he won the Outland Trophy (1983), Lombardi Award (1983), and the UPI Lineman of the Year (1983). He was selected in the first round of the 1984 NFL Draft, and played professionally for the Houston Oilers of the NFL. Steinkuhler is also remembered for being the player who picked up quarterback Turner Gill's intentional fumble in the 1984 Orange Bowl and ran it 19 yards for a touchdown in a play dubbed the "Fumblerooski".
Roland Woolsey - (born August 11, 1953 in Provo, Utah) is a former professional American football player who played in four NFL seasons for the Dallas Cowboys, Seattle Seahawks, Cleveland Browns and the St. Louis Cardinals. He played college football at Boise State University.


== Popularity in Countries Outside of the U.S. ==
The Israeli Football League, an eight-man league was established in Israel in 2005 with three teams, Haifa Underdogs, Tel Aviv Pioneers and Tel Aviv Sabres. The league was established by Israeli players and activists under the leadership of Ofri Becker, and though playing without equipment, this was the first ever tackle football league in this country, named Israeli Football League (IFL). In March 2008, at the end of the first season played in full gear, the Big Blue Jerusalem Lions defeated the Real Housing Haifa Underdogs 24 - 18 in overtime in Israel Bowl I. In Israel Bowl II in April 2009, the Dancing Camel Modi'in Pioneers defeated the defending champions Big Blue Jerusalem Lions 32 - 26 after two overtimes. The game was decided by a whole field interception return for a TD by Pioneers' Ohad Naveh. That season was played with five teams after the expansion franchise of Jerusalem Kings was added. The 2009-2010 season was played with seven teams, introducing two new franchises, the Beer Sheva Black Swarm and the Judean Rebels. In the 2010-2011 season, an eighth team was added (The Herzeliya Hammers), and the league was split into 2 divisions, IFL North and IFL South. The 2011-2012 season saw 10 teams, with five in each division, North and South. The North Division consisted of the three Tel Aviv-area teams: the Sabres, Pioneers and Hammers; as well as the Haifa Underdogs and Naharia North Stars. The South Division was made up of the three Jerusalem-area teams: the Rebels, Lions and Kings; as well as the Petah Tikva Troopers and Be'er Sheva Black Swarm. The IFL continued to expand for the 2012-2013 season, adding another Tel Aviv-area team, the Rehovot Silverbacks. Due to the odd number of teams, the IFL abandoned the North and South Divisions, and now each team plays every other team in the league one time during the 10 game season.
An Eight man league is also played in Ireland. This league, named DV8, is used as developmental league for rookies before they go on to compete in the 11man IAFL. In 2009, six teams competed in the DV8 league - Dublin Dragons, Edenderry Soldiers, Trinity College Dublin, Craigavon Cowboys, UCD Sentinels and Erris Rams


== See also ==
Six-man football
Nine-man football
Arena Football


== External links ==
AEFL
MaxPreps Reduced-Player Football Rankings
Wisconsin 8-Man Football Postseason
8FL Eight Man Football League
Wisconsin 8-Man Football Conferences
Kansas 8-Man Football Association
Missouri 8-Man Football Coaches Association
Nebraska 8-Man Football Coaches Association
Judean Rebels homepage, of the Kraft Family Israeli Football League


=== References ===