Maurizio Belpietro (born 10 May 1958) is an Italian journalist and television presenter.


== Biography ==
Belpietro was born in Castenedolo, near Brescia, but he grew up in the town of Palazzolo sull'Oglio. He started his career as a journalist writing for the local newspaper Bresciaoggi in 1975. In 1994 he moved on to il Giornale with the journalist Vittorio Feltri and returned few years later after a period as editor of the newspaper Il Tempo in 1996. From 2001 to 2007, he worked as editor of il Giornale. He was succeeded by the journalist Mario Giordano when he became editor of Italian newsmagazine Panorama.
In 2004, he became presenter of the television newsmagazine L'Antipatico, broadcast on Canale 5 and later on Rete 4. Since October 2007, he has also appeared on the morning news program Mattino Cinque.
In 2009, he became editor of the right-wing newspaper Libero, replacing Vittorio Feltri.
He also hosts a short radio news program every morning on the radio station R101.


== Sentences ==
In April 2010 was sentenced from the Italian Supreme Court of Cassation with the sentence # 13198 for contempt with this motivation:
italian: "(...) La pubblicazione di articoli fortemente polemici contro la magistratura può portare il direttore del quotidiano responsabile della pubblicazione alla condanna per vilipendio e diffamazione. (...)"
english: "(...) Publishing harshly news articles with a contentious content against Magistrates can lead to the editor in chief responsible of the publication to be sentenced for contempt and defamation. (...)"
However, the ECHR did not upheld the verdict. 


== References ==


== External links ==
Articles written on il Giornale (Italian)
Articles written on Panorama (Italian)