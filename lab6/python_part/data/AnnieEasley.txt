Annie J. Easley (April 23, 1933 – June 25, 2011) was an African-American computer scientist, mathematician, and rocket scientist. She worked for the Lewis Research Center of the National Aeronautics and Space Administration (NASA) and its predecessor, the National Advisory Committee for Aeronautics (NACA). She was a leading member of the team which developed software for the Centaur rocket stage and one of the first African-Americans in her field.


== Early life and education ==
Annie Jane Easley was born to Bud McCrory and Willie Sims in Birmingham, Alabama. In the days before the Civil Rights Movement, educational and career opportunities for African American children were very limited. African American children were educated separately from white children and their schools were most often inferior to white schools. Annie was fortunate in that her mother told her that she could be anything she wanted but she would have to work at it. She encouraged her to get a good education and from the fifth grade through high school, she attended a parochial school and was valedictorian of her graduating class.
After high school she went to New Orleans, Louisiana, to Xavier University, then an African-American Roman Catholic University, where she majored in pharmacy for about two years.
In 1954, she returned to Birmingham briefly. As part of the Jim Crow laws that established and maintained racial inequality, African Americans were required to pass an onerous literacy test and pay a poll tax in order to vote. She remembers the test giver looking at her application and saying only, "You went to Xavier University. Two dollars." Subsequently, she helped other African Americans prepare for the test. In 1963, racial segregation of Birmingham's downtown merchants ended as a result of the Birmingham campaign, and in 1964, the Twenty-fourth Amendment outlawed the poll tax in Federal elections. But it was not until 1965 that the Voting Rights Act eliminated the literacy test.
Shortly thereafter, she married and moved to Cleveland with the intention of continuing her studies. Unfortunately, the local university had ended its pharmacy program a short time before and no nearby alternative existed.


== Career at NACA and NASA ==
In 1955, she read a local newspaper article about a story on twin sisters who worked for the National Advisory Committee for Aeronautics (NACA) as "computers" and the next day she applied for a job. Within two weeks she was hired, one of four African Americans of about 2500 employees. She began her career in as a Mathematician and Computer Engineer at the NACA Lewis Flight Propulsion Laboratory (which became NASA Lewis Research Center, 1958–1999, and subsequently the John H. Glenn Research Center) in Cleveland, Ohio. She continued her education while working for the agency and in 1977, she obtained a Bachelor of Science in Mathematics from Cleveland State University. As part of a continuing education, Easley worked through specialization courses offered by NASA.
Her 34-year career included developing and implementing computer code that analyzed alternative power technologies, supported the Centaur high-energy upper rocket stage, determined solar, wind and energy projects, identified energy conversion systems and alternative systems to solve energy problems. Her energy assignments included studies to determine the life use of storage batteries, such as those used in electric utility vehicles. Her computer applications have been used to identify energy conversion systems that offer the improvement over commercially available technologies. She retired in 1989 (some sources say 1991).
Easley's work with the Centaur project helped as technological foundations for the space shuttle launches and launches of communication, military and weather satellites. Her work contributed to the 1997 flight to Saturn of the Cassini probe, the launcher of which had the Centaur as its upper stage.
Annie Easley was interviewed in Cleveland, on August 21, 2001 by Sandra Johnson. The interview is stored in the National Aeronautics and Space Administration Johnson Space Center Oral History Program. The 55 page interview transcript includes material on the history of the Civil Rights Movement, Glenn Research Center, Johnson Space Center, space flight, and the contribution of women to space flight.


== Selected works ==
Performance and Operational Economics Estimates for a Coal Gasification Combined-Cycle Cogeneration Powerplant. Nainiger, Joseph J.; Burns, Raymond K.; Easley, Annie J. NASA, Lewis Research Center, Cleveland, Ohio, USA NASA Tech Memo 82729 Mar 1982 31p
Bleed Cycle Propellant Pumping in a Gas-Core Nuclear Rocket Engine System. Kascak, A. F. ; Easley, A. J. National Aeronautics and Space Administration. Lewis Research Center, Cleveland, Ohio. Report No.: NASA-TM-X-2517; E-6639 March 1972
Effect of Turbulent Mixing on Average Fuel Temperatures in a Gas-Core Nuclear Rocket Engine. Easley, A. J. ; Kascak, A. F.; National Aeronautics and Space Administration. Lewis Research Center, Cleveland, Ohio. Report No.: NASA-TN-D-4882 Nov 1968


== Quotations ==
When people have their biases and prejudices, yes, I am aware. My head is not in the sand. But my thing is, if I can't work with you, I will work around you. I was not about to be so discouraged that I'd walk away. That may be a solution for some people, but it's not mine.
You're never too old, and if you want to, as my mother said, you can do anything you want to, but you have to work at it.
I'd like to just throw in here at this time that I tell people that it doesn't matter what your age is or what you decide to do when you're eighteen or sixteen, it doesn't matter if you change your mind later on and change fields, because we need to be flexible.


== See also ==
Katherine Johnson
List of African-American women in STEM fields


== Notes ==


== References ==
Black Contributors to Science and Energy Technology. U.S. Department of Energy (Washington, D.C.: Office of Public Affairs), 1979, p. 19. DOE/OPA-0035 (79).
The ACM-Mills Conference on Pioneering Women in Computing. Mills College, Oakland, California. May 7, 2000
In Black and White: A Guide to Magazine Articles, Newspaper Articles and Books Concerning More than 15,000 Black Individuals and Groups. 3rd edition Mary Mace Spradling, ed. (Detroit, MI: Gale Research Co.), 1980. p. 289.
"Easley, Annie J.: American Computer Scientist" in World of Computer Science. Brigham Narin, Ed. (Detroit, MI: Gales Group), 2002. p. 210.


== External links ==
BookRags Biography
The Faces of Science Biography
NASA Glen Research Center