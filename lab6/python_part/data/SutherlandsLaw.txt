Sutherland's Law is a television series made by BBC Scotland between 1973 and 1976.
The series had originated as a stand alone edition of the portmanteau programme Drama Playhouse in 1972 in which Derek Francis played Sutherland and was then commissioned as an ongoing series. The producer was Frank Cox.
Sutherland's Law dealt with the duties of the Procurator Fiscal in a small Scottish town. The major cast members included Iain Cuthbertson (as John Sutherland), Gareth Thomas, Moultrie Kelsall, Victor Carin, Martin Cochrane, Maev Alexander and Edith MacArthur.
The exteriors for the series were filmed in Oban, Argyll.
The signature tune was The Land of the Mountain and the Flood, by Hamish MacCunn. Series creator Lindsay Galloway released a novel based on the series in 1974.
The DVD of Sutherland's Law Series 1 was released on Region 2 by Acorn Media UK in the UK on 1 June 2009.


== References ==


== External links ==
Sutherland's Law at the Internet Movie Database
Action TV episode guide