Philippine Science High School Cordillera Administrative Region Campus (PSHS-CARC) is one of the two newest campuses of the Philippine Science High School System (The other one is Philippine Science High School Central Luzon Campus). It was established through the congressional efforts of Baguio City Congressman Mauricio G. Domogan.
It formally opened classes on June 22, 2009 with 66 students from Baguio City, Benguet, Mountain Province, Ifugao, Kalinga, Abra, Apayao, and Dagupan City.
The school is being administered by Dr. Conrado C. Rotor, Jr. as the Campus Director.
The campus temporarily holds classes within the Baguio City National High School (BCNHS) - San Vicente Annex Campus in Barangay San Vicente.
In the school's Annual Report, it was announced that the new campus will start its construction on the first quarter of 2012 in a portion of Baguio City National High School's lot in Irisan, Baguio City.
The school moved to their permanent site at Irisan, Baguio City last May 2013.


== References ==
http://www.cordilleraonline.com/co/index.php?option=com_content&view=article&id=216:philippine-science-high-school-car-opens&catid=1:latest-news&Itemid=78
http://northphiltimes.blogspot.com/2009/05/more-news-baguio-city.html
http://www.philstar.com/Article.aspx?articleId=461180&publicationSubCategoryId=473


== External links ==
PSHS Cordillera Administrative Region Campus official website