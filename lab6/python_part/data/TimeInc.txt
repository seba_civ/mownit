Time Inc. is an American New York-based publishing company. It publishes over 90 magazines, most notably its namesake, Time. Other magazines include Sports Illustrated, Travel + Leisure, Food & Wine, Fortune, People, InStyle, Life, Golf Magazine, Southern Living, Essence, Real Simple, and Entertainment Weekly. It also owns the UK magazine house Time Inc. UK, whose major titles include What's On TV, NME, Country Life, Marie Claire, Wallpaper* and InStyle. Time Inc. also operates the digital-only titles MyRecipes, TheSnug, and MIMI.
Time Inc. also owns the rights to LIFE, a well-known magazine that has been published in many different formats. Time Inc. currently owns and runs LIFE.com, a website dedicated to news and photography.
In 1990, Time Inc. merged with Warner Communications to form the media conglomerate Time Warner. This merger lasted until the company was spun off on June 9, 2014.


== History ==
Nightly discussions of the concept of a news magazine led its founders Henry Luce and Briton Hadden, both age 23, to quit their jobs in 1922. Later that same year, they formed Time Inc. Having raised $86,000 of a $100,000 goal, the first issue of Time was published on March 3, 1923, as the first weekly news magazine in the United States. Luce served as business manager while Hadden was editor-in-chief. Luce and Hadden annually alternated year-to-year the titles of president and secretary-treasurer. Upon Hadden's sudden death in 1929, Luce assumed Hadden's position.
Luce launched the business magazine Fortune in February 1930 and created/founded the pictorial Life magazine in 1934, and launched House & Home in 1952 and Sports Illustrated in 1954. He also produced The March of Time radio and newsreel series. By the mid 1960s, Time Inc. was the largest and most prestigious magazine publisher in the world. (Dwight Macdonald, a Fortune staffer during the 1930s, referred to him as "Il Luce", a play on the Italian dictator Benito Mussolini, who was called "Il Duce".)
Once ambitious to become Secretary of State in a Republican administration, Luce penned a famous article in Life magazine in 1941, called "The American Century", which defined the role of American foreign policy for the remainder of the 20th century, and perhaps beyond.
President Franklin D. Roosevelt, aware that most publishers were opposed to him, issued a decree in 1943 that blocked all publishers and media executives from visits to combat areas; he put General George Marshall in charge of enforcement. The main target was Luce, who had long opposed FDR. Historian Alan Brinkley argues the move was "badly mistaken", for had Luce been allowed to travel, he would have been an enthusiastic cheerleader for American forces around the globe. But stranded in New York City, Luce's frustration and anger expressed itself in hard-edged partisanship. Luce, supported by Editor-in-Chief T. S. Matthews, appointed Whittaker Chambers as acting Foreign News editor in 1944, despite the feuds Chambers had with reporters in the field.
In 1963, recommendations from Time Inc. based on how it delivered magazines led to the introduction of ZIP codes by the United States Post Office.
Luce, who remained editor-in-chief of all his publications until 1964, maintained a position as an influential member of the Republican Party. Holding anti-communist sentiments, he used Time to support right-wing dictatorships in the name of fighting communism. An instrumental figure behind the so-called "China Lobby", he played a large role in steering American foreign policy and popular sentiment in favor of Nationalist leader Chiang Kai-shek and his wife Soong Mei-ling in their war against the Japanese. (The Chiangs appeared in the cover of Time eleven times between 1927 and 1955.)
The merger of Time Inc. and Warner Communications was announced on March 4, 1989. During the summer of that same year, Paramount Communications (formerly Gulf+Western) launched a $12.2 billion hostile bid to acquire Time Inc. in an attempt to end a stock swap merger deal between Time and Warner Communications. This caused Time to raise its bid for Warner to $14.9 billion in cash and stock. Paramount responded by filing a lawsuit in a Delaware court to block the Time/Warner merger. The court ruled twice in favor of Time, forcing Paramount to drop both the Time acquisition and the lawsuit, and allowing the formation of the two companies' merger which was completed on January 10, 1990. However, instead of the companies becoming defunct, the impact of the merger and its resultant financial shock wave gave off a new corporate structure, resulting in the new company being called "Time Warner".
In 2008, Time Inc. launched Maghound, an internet-based magazine membership service that featured approximately 300 magazine titles from both Time Inc. brands and external publishing companies.
On January 19, 2010, Time Inc. acquired StyleFeeder, a personal shopping engine.
In August 2010, Time Inc. announced that Ann S. Moore, its chairman and chief executive, would step down as CEO and be replaced by Jack Griffin, an executive with Meredith Corporation, the nation's second-largest publisher of consumer magazines. In September 2010, Time Inc. entered into a licensing agreement with Kolkata-based ABP Group, one of India’s largest media conglomerates, to publish Fortune India magazine and the yearly Fortune India 500 list.
On March 6, 2013, Time Warner announced plans to spin-off Time Inc. into a publicly traded company. Time Warner's chairman/CEO Jeff Bewkes said that the split would allow Time Warner to focus entirely on its television and film businesses, and Time Inc. to focus on its core print media businesses. It was announced in May 2014 that Time Inc. would become a publicly traded company on June 6 of that year. The spinoff was completed on June 9, 2014.
Time Inc. purchased American Express Publishing Corporation's suite of titles, including Travel + Leisure, Food & Wine, Departures, Black Ink and Executive Travel on October 1, 2013.
On January 14, 2014, Time Inc. announced that Colin Bodell was joining the company in the newly created position of Executive Vice President and Chief Technology Officer.
On February 5, 2014, Time Inc. announced that it was cutting 500 jobs. However, most of the layoffs are at American Express Publishing.
Since April 2014, the Chairman of Time Inc. has been Joseph A. Ripp. Ripp has been Chief Executive since September 2013.
On February 11, 2016, Time Inc. announced that it has acquired Viant, a leading people based marketing platform and owner of MySpace.


== See also ==
Fortune
Fortune India 500


== References ==


== External links ==
Official website
Pathfinder (Time Inc. content portal)