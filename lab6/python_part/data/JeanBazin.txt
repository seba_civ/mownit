Jean Bazin (born January 31, 1940) is a Canadian lawyer and former senator.
Born in Quebec City, he received a B.Comm. and an LL.L. from Université Laval in 1964. He was elected president of the Canadian Union of Students for the 1964-65 school year. He was called to the Quebec Bar in 1965. He was appointed Queen's Counsel in 1984. From 1987 to 1988, he was the president of the Canadian Bar Association. He is a lawyer and partner with Fraser Milner Casgrain LLP.
He acts as commercial arbitrator and mediator in a number of fields.
In 1986, he was appointed to the Senate representing the senatorial division of De la Durantaye, Quebec. He sat as a Progressive Conservative and he resigned in 1989.
As reported in Le Devoir newspaper of January 29, 2004, Jean Bazin was hired as spokesmen for the Quebec government. His mandate was to help resolve leadership issues within the Kanesatake community.
He sits on a number of boards such the board of directors of the Canadian Unity Council, the Laurentian Bank, the Société générale de financement du Québec, and Miranda Technologies.


== External links ==

Jean Bazin – Parliament of Canada biography