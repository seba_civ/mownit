Blackout lights are head and tail lights that are equipped with special lenses that are designed to cast a diffused horizontal beam of light for the driver of the vehicle to navigate with, and drivers of other vehicles to spot the vehicle with. In the military they are intended for use when driving a convoy at night when it is necessary for the vehicles to maintain concealment by limiting their ability to be seen by the enemy.


== Types ==


=== Blackout driving lights ===
Blackout Driving Lights are mounted on the driver's side of the vehicle, typically on the fender or grille. They give off a diffused beam of white light that takes the place of regular headlights when driving under blackout conditions. It is intended to give off enough light for the driver to make his way. The Blackout Driving Light is also hooded making the light only to be able to be seen when looking up at it. With the use of night vision goggles the effectiveness of the blackout driving light can be increased.


=== Blackout marker lights ===

Front marker lights are typically mounted below or to the side of the vehicles normal headlights. They serve the purpose of making the vehicle visible to oncoming drivers or for a driver to see if a vehicle is following him or her.
Rear marker lights or blackout taillights are typically housed in the same unit as the vehicles taillights, and are the main tool in keeping the proper following distance in nighttime convoy driving.
Blackout Marker Lights serve the purpose of marking the position of the vehicle during blackout driving. They also are used as an aid in keeping the proper safe following distance when driving in a convoy at night. US Military marker lights are designed with a series of "cat eyes" that appear differently depending on the distance one is away from the vehicle that is equipped with the lights. When the following distance is too far the rear marker lights appear as a single red light and when too close as four separate red lights. When the correct following distance of 180 to 60 feet is maintained the taillights appear as two red lights.


=== Blackout stop lights ===
Blackout stop lights take the place the normal stoplight when operating in blackout conditions. In US military vehicles when the brakes are applied in blackout mode the brake light appears as a white light. The blackout stop lights are typically in the same taillight assembly as the rear blackout marker lights.


=== Blackout headlights ===
Blackout headlights can refer to either front blackout marker lights or blackout driving light depending on the vehicle that is equipped with them.


== Controls ==

Military vehicles that that are equipped with blackout lights have a special switch for controlling the lights that are on the vehicle. This switch serves the purpose of preventing the headlights or any other lights on the vehicle from being turned on by accident, by means of a lock that is on the switch that must be pushed before the lights are able to be turned on.


== References ==