Sibalom Natural Park is a 5,511.47-hectare (13,619.1-acre) protected area in the Philippines on the island of Panay in the municipality of Sibalom, Antique. It was proclaimed a natural park on 23 April 2000. It is considered as one of the last remaining lowland forests on Panay.
The park was first established on 28 June 1990 as the Tipulu-an Mau-it Rivers Watershed Forest Reserve covering 7,737 hectares (19,120 acres) of an important watershed.


== Geography ==
Sibalom Natural Park extends over sixteen barangays (village) in Sibalom. It is located 36 kilometres (22 mi) east from Antique's provincial capital, San Jose de Buenavista, and some 140 kilometres (87 mi) west from Iloilo City. The park is organized around the watershed area of the Tipulu-an and Mao-it rivers which are tributaries of the Sibalom River. It has seven other tributaries which provide drinking water for five municipalities and irrigate some 5,500 hectares (14,000 acres) of riceland in four municipalities of Antique. Mount Porras at 800 feet (240 m) is the highest peak in the area and is located at the park's center.
The park has a mild montane forest, a lowland forest, some bushland and open cogon or grassland. Its riverbeds contain semi-precious gemstones such as agate, jasper and onyx.


== Wildlife ==
The park serves as a corridor between two important bird areas on the island, the Northwest Panay Peninsula Natural Park and the Central Panay Mountain Range. It supports 76 bird species, 28 of which are endemic to the Philippines including the Negros bleeding-heart, Walden's hornbill, Visayan hornbill, brahminy kite, blue-naped parrot, white-winged cuckooshrike and eastern grass owl. Other endemic fauna known to inhabit the park include the Visayan spotted deer, Visayan warty pig, and some herpetofaunal species such as the giant Visayan frog, Negros truncated-toed chorus frog, collared monitor lizard and Philippine sailfin lizard.


== Flora ==
The Philippine dipterocarp trees such as white lauan and apitong, and fruit trees such as antipolo and malapaho are found in the forests of Sibalom. The globally endangered giant flower, the Rafflesia speciosa, also blooms in the park.


== References ==