The squacco heron (Ardeola ralloides) is a small heron, 44–47 cm (17–19 in) long, of which the body is 20–23 cm (7.9–9.1 in), with 80–92 cm (31–36 in) wingspan. It is of Old World origins, breeding in southern Europe and the Greater Middle East.


== Behaviour ==

The squacco heron is a migrant, wintering in Africa. It is rare north of its breeding range. This is a stocky species with a short neck, short thick bill and buff-brown back. In summer, adults have long neck feathers. Its appearance is transformed in flight, when it looks very white due to the colour of the wings.
The squacco heron's breeding habitat is marshy wetlands in warm countries. The birds nest in small colonies, often with other wading birds, usually on platforms of sticks in trees or shrubs. Three to four eggs are laid. They feed on fish, frogs and insects.


== Etymology ==
The English common name squacco comes via Francis Willughby (c. 1672) quoting a local Italian name sguacco. The current spelling comes from John Hill in 1752.
The scientific name comes from Latin ardeola, little heron, and ralloides, Latin rallus, a rail and Greek -oides, resembling.


== References ==


== External links ==
Ageing and sexing (PDF) by Javier Blasco-Zumeta & Gerd-Michael Heinze
Squacco Heron - The Atlas of Southern African Birds
Ardeola ralloides in the Flickr: Field Guide Birds of the World
Ardeola ralloides on Avibase
Squacco Heron at oiseaux.net
BirdLife species factsheet for Ardeola ralloides
Squacco heron videos, photos, and sounds at the Internet Bird Collection
Squacco heron photo gallery at VIREO (Drexel University)
Audio recordings of Squacco heron on Xeno-canto.