George Deardorff McCreary (September 28, 1846 – July 26, 1915) was a Republican member of the U.S. House of Representatives from Pennsylvania.
George D. McCreary was born at York Springs, Pennsylvania. He moved with his parents to Philadelphia in 1864. He entered the University of Pennsylvania at Philadelphia in 1864 and remained until 1867, when he left to take a position with a coal company of which his father was president. He began an independent business career in 1870. He was elected treasurer of the city and county of Philadelphia in November 1891, and served until 1895.
McCreary was elected as a Republican to the Fifty-eighth and to the four succeeding Congresses. He served as chairman of the United States House Committee on Ventilation and Acoustics during the Sixty-first Congress. He was not a candidate for renomination in 1912. McCreary had a heart for the poor and sponsored a tea service at the Sunday Breakfast Rescue Mission homeless shelter which is still running today. He was engaged in banking, and died in Philadelphia. Interment in Laurel Hill Cemetery.


== References ==


== Sources ==
 This article incorporates public domain material from websites or documents of the Biographical Directory of the United States Congress.

United States Congress. "George D. McCreary (id: M000381)". Biographical Directory of the United States Congress. 
George Deardorff McCreary entry at The Political Graveyard


== External links ==

George Deardorff McCreary at Find a Grave