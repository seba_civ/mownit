The Gran Premio d'Italia is a Listed flat horse race in Italy open to three-year-old thoroughbreds. It is run at Milan over a distance of 2,400 metres (about 1½ miles), and it is scheduled to take place each year in June.


== History ==
The event was established in 1921, and it was initially contested over 1,800 metres. It was run over 2,000 metres in 1926 and 1927, and extended to 2,400 metres in 1928.
The present system of race grading was introduced in the early 1970s, and for a period the Gran Premio d'Italia was classed at Group 1 level. During the late 1980s and early 1990s it took place in September.
The race was downgraded in 1996, and cut to 2,000 metres in 1997. From this point it held Listed status and was staged in June or July.
The Gran Premio d'Italia was run over 2,200 metres in 2009. It was increased to 2,400 metres in 2010.


== Records ==
Leading jockey since 1984 (3 wins):
Fernando Jovine – Jaunty Jack (1997), Clapham Common (1998), Endless Hall (1999)
Mirco Demuro – Sopran Glaumix (2001), Primary (2006), Rastignano (2008)
Leading trainer since 1984 (6 wins):
Stefano Botti – Apprimus (2009), Kidnapping (2010), Bacchelli (2011), Wild Wolf (2012), Bertinoro (2014), Time Chant (2015)
Leading owner since 1984 (2 wins):
Athos Christodoulou – St Hilarion (1985), Posidonas (1995)
Scuderia Effevi – Kidnapping (2010), Wild Wolf (2012)


== Winners since 1984 ==


== Earlier winners ==

* The 1936 and 1976 races were dead-heats and have joint winners.


== See also ==
List of Italian flat horse races


== References ==
Racing Post / www.labronica.it:
1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997
1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007
2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015

galopp-sieger.de – Gran Premio d'Italia.
pedigreequery.com – Gran Premio d'Italia – Milano San Siro.