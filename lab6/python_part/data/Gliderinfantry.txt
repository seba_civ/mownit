Glider infantry (also referred to as Airlanding infantry esp. in British usage) was a type of airborne infantry in which soldiers and their equipment were inserted into enemy-controlled territory via military glider rather than parachute. Initially developed in the late 1930s by Germany, glider infantry units were used extensively during World War II but are no longer used by any modern military.


== Early history ==
With the treaty of Versailles preventing any other form of pilot training in Germany, large numbers of gliding clubs and schools were formed there after World War I. Later, when planning the invasion of France, the German military was faced with the problem of the Belgian fort of Eben Emael which dominated the River Meuse. Someone (according to some reports, Adolf Hitler himself) pointed out that the top of the fort was a flat grassy expanse on which gliders could land.
Eight DFS 230 gliders, carrying 85 Pioneers under Leutnant Rudolf Witzig, landed on the roof of the fort in the early hours of May 10, 1940. There had been no declaration of war, and they achieved surprise. Using the new shaped charges, they disabled the fort's guns and trapped the garrison inside. The assault cost only 21 casualties.
In the aftermath of this episode, the Allies formed their own glider forces, as part of their airborne forces . Before they could see action, the Germans had made their largest airborne operation, the attack on Crete. Their glider troops and paratroops suffered heavy casualties, and the Germans decided that this mode of warfare was too costly. A subsequent plan for the invasion of Malta which called for extensive German and Italian airborne operations was cancelled. The Italian 80th La Spezia division was specially trained for airlanding operations, but never took part in any after the Maltese invasion was cancelled and it was instead deployed in the Tunisian Campaign.
In 1940, Winston Churchill, the British Prime Minister, decreed the formation of a British glider force of 5000 gliders. American plans were on a similar scale.


== Allied organization ==
The gliders which were most widely used by the Allies were the American-designed Waco CG-4A, which could carry 13 passengers, and the British-designed Airspeed Horsa, which could carry 25 passengers. Both of these aircraft used plywood extensively in their construction, with the CG-4A also using aluminium to provide greater strength in its framing. To deliver especially heavy loads, the British General Aircraft Hamilcar could carry up to eight tons (8,000 kg) of equipment.
Much like conventional gliders, these aircraft were towed behind a powered aircraft, usually a C-47 (or the Armstrong Whitworth Albemarle in British units), and were then released near the designated landing area called the Landing Zone' or 'LZ'.
The crews of these aircraft landed their aircraft in circumstances which would challenge the most seasoned pilot. Sometimes flying in at night they had but a few moments to pick a likely landing spot, avoid the other gliders making similar approaches and those already on the ground, avoid incoming enemy fire and then land the aircraft without crashing into any trees, ditches or enemy erected anti-troop stakes (called by pilots in Normandy "Rommel's asparagus"), and do so softly so as to ensure that the aircraft and/or cargo were not damaged in the process.
Prior to the Battle of Normandy, the Allied command feared that the losses suffered by glider groups would be as high as 50-70% before even encountering the enemy. This fear was based on expectations for high numbers of crash landings and encounters with anti-aircraft defences. The actual losses were less than the estimates and were comparable to the losses of associated parachute units; interestingly, the losses suffered by certain Glider Artillery battalions (e.g., the 319th and 320th of the 82nd Airborne Division) were higher than the losses of the associated Glider infantry (i.e., the 325th) primarily because the two artillery battalions landed in the evening hours of D-Day and the landing zone (LZ-W) near St. Mere Eglise was not secure with many casualties occurring from enemy anti-aircraft and machine gun fire in addition to crash landings. In contrast, the 325th landed on D-Day +1 and faced less intense enemy fire; while more than half of the 327th Glider Infantry Regiment landed by boat on the third day at Utah Beach because of the lack of gliders able to carry them into Normandy.
Initially the American Glider Infantry Regiments (GIR) had only two battalions, but later in Europe, the two battalions of the 401st GIR were divided in March 1944 to act as the 3rd battalions of the 325th and 327th GIRs. In March 1945 the 401st Glider Infantry Regiment was disbanded and the battalions formally became part of their new regiments.
In both the British and American armies, there was a sense that the glider infantry were poor cousins to the more glamorous paratroopers. In the British Army, whereas paratroops were all volunteers, airlanding units were standard line infantry units converted without any option (although they were entitled to wear the same maroon beret as the Parachute Regiment). In the United States Army, glider troops did not receive the extra pay awarded to paratroopers until after the Normandy invasion (where glider troops provided essential support to the parachute regiments and fought on the front-lines alongside their parachute brethren). This blatant inequality of treatment came to the attention of U.S. Airborne High Command and from that point forward the glider troops were issued the same jump boots and combat gear as paratroopers (including the M1A1 carbine with folding stock) and earned the same pay until the war ended in Europe in May 1945. There are numerous examples of glider troops volunteering as replacements for paratrooper units but very few, if any, examples of paratroopers volunteering for the glider units.
In one respect the American and British armies differed. The Glider Pilot Regiment in the British Army were not only trained aircrew, but also very well-trained infantry, most of whom would have been junior or senior NCO's in other units. By comparison, the American glider crews were treated as mere drivers.


== Usage and doctrine ==
Firstly, glider infantry are loaded into gliders which are attached to towing aircraft by a cable. The loaded gliders are then towed through the air by towing aircraft and flown to a release point usually just beyond the hearing range of enemy troops. The tow cables are then released and the gliders would be piloted, without engine power, to a designated landing zone. Once the gliders landed, the troops and equipment would disembark and enter combat. Glider pilots were often organized together after landing to fight or be extracted to safety.
Compared with paratroops, alongside whom they would operate, glider-borne troops had several advantages:
Gliders could carry and deliver much bulkier and heavier equipment (such as anti-tank guns, or vehicles such as jeeps or even light tanks) that could not be parachuted from the side-loading transport aircraft normally used in World War 2. Thus glider infantry units were usually better equipped than their parachute infantry counterparts.
Any one stick of glider infantry could disembark intact and combat ready, while paratroops needed time after landing to regroup and reorganize before beginning operations. Under ideal conditions, whole glider units could land intact.
Unlike drop planes which delivered paratroops, gliders were totally silent and detection by the enemy was difficult, greatly increasing the element of surprise. In fact, completely undetected insertions were possible, especially during night landings.
Glider infantry required much less training than parachute infantry. In fact many glider infantry units were simply converted from regular infantry units with only cursory training.
However using gliders as a method of insertion also had serious drawbacks:
Gliders required a relatively smooth landing area free from obstructions. A common countermeasure against gliders was to sow posts and other obstructions in likely landing areas.
Gliders were fragile and glider landings were rough and brutal affairs. All too often, gliders were destroyed during landing attempts, killing or injuring the crew and passengers.
In practice, it was difficult for entire units to land together and glider-borne units often ended up even more widely scattered than parachute units.
Gliders and towing planes were extremely vulnerable to interception by enemy aircraft while gliders were under tow. Gliders were also helpless against ground fire if they were detected before landing, but the same can be said for any aircraft or helicopter flying over enemy territory.
Glider pilots, who were expensive to train and replace, suffered heavy casualties.


== Later history ==


=== Sicily ===
The Allies first used gliders in the invasion of Sicily, Operation Husky, in 1943. This first experiment was disastrous. Poor planning and bad weather resulted in the gliders being scattered in the air. Several landed in the sea and 200 men drowned. Dozens of gliders and towplanes were damaged or shot down by friendly fire. Few gliders reached the intended landing zones, and only 73 men (from most of a brigade) reached the intended target, the Ponte Grande bridge south of Syracuse.


=== Normandy ===

With much better intelligence and planning, the glider landings in the Battle of Normandy were far more successful. In particular, one coup de main force in six Horsa gliders seized vital bridges over the River Orne by surprise, led by Major John Howard (see Operation Deadstick). The 6th British Airlanding Brigade, part of 7th British Airborne Division, were in action early on following concentrated landings, and prevented early German attempts to counter-attack the Allied landings. American landings were more scattered, but still more successful than many planners had hoped for.


=== Arnhem ===
In Operation Market Garden, the 1st British Airlanding Brigade, attached to 1st British Airborne Division, were landed on the first day of the operation. The landings took place in daylight and were unopposed, but the only landing and drop zones thought suitable for such a large force were a considerable distance from the vital bridge which was the objective. No attempt was made to mount a coup de main attack by glider (although this was largely due to the haste with which the operation was mounted). A jeep-mounted reconnaissance squadron brought in by glider failed in the mission.
In the subsequent fighting in Arnhem, the 1st Airlanding Brigade and the Glider Pilot Regiment suffered heavy casualties.


=== Rhine crossing ===

The last major operation involving gliders was the crossing of the River Rhine in March 1945. To avoid the long delay in relieving the airborne troops which had been a major cause of the failure of Operation Market Garden, the landings were made close to the German front line defences. The landings took place in daylight once again, and heavy German anti-aircraft fire took heavy toll of the vulnerable gliders. Most Allied casualties were incurred by the glider pilots.


=== Far East ===
The Chindits were the creation of Brigadier Orde Wingate, and were a large force operating behind Japanese lines during the Burma Campaign, were flown by the 1st Air Commando Group to landing zones which had been secured by advance guards landed by glider on March 5, 1944. This operation, although successful, also incurred heavy casualties. This was partly because the intended landing ground was changed at the last minute. Also, the distance flown and the loads towed by the tug aircraft were greater than anything met in Europe. Many gliders had to be released over enemy territory or mountains. Others crashed on landing on the unfamiliar landing zone. However, enough construction equipment was landed to make the landing ground fit for transport aircraft.


=== Later German operations ===
After the heavy losses at Crete the Germans made no more large-scale glider assaults. They did make several coup de main attacks against targets which were not protected by anti-aircraft guns. One of these was Unternehmen Eiche ("Operation Oak") a landing on the Gran Sasso in Italy on September 12, 1943, in which the deposed Italian dictator Benito Mussolini was rescued from house arrest.
Another was an attack (codenamed Operation Rösselsprung) on the headquarters of Marshal Josip Broz Tito in Yugoslavia in March 1944. Glider troops landed above Tito's headquarters. This was in the middle of a large concentration of Yugoslav partisans, and the glider troops once again suffered heavy casualties, while Tito escaped.
The last German glider attack was on the liberated Free French redoubt of the Vercors in July 1944. This attack from an unexpected direction drove the resistance fighters from the plateau, but the conduct of the operation was marred by the brutal behaviour of the glider troops.


=== Post war ===
Glider infantry did not survive long after the close of World War II. The German glider infantry units were disbanded. The U.S. Army Glider Infantry School was closed in 1948 and remaining glider units were eventually converted into parachute infantry. About the same time the British Glider Pilot Regiment was subsumed into the Army Air Corps and the Airlanding brigades were disbanded. However the Soviet Union continued to train and use glider troops well into the 1960s.
A convergence of factors led to the rather quick displacement of glider-borne infantry by regular paratroops. Larger capacity post war cargo plane designs enabled paratroops to carry heavier equipment. High speed four engined transports made the former wooden glider unsafe. Improvements in parachute infantry training and tactics reduced the scattering when paratroops disembarked. Newer anti-aircraft technology like radar and radar directed guns made gliders readily detectable.
The concept of using aircraft to forcibly insert infantry, however, never completely died and was eventually revived in the late 1950s with the advent of helicopters and air assault infantry.


== References ==


== External links ==
325th Glider Infantry Association
National WWII Glider Pilots Association, Inc.
Tribute To The American Combat Glider Pilots Of World War II