The Iowa Supreme Court is the highest court in the U.S. state of Iowa. As constitutional head of the Iowa Judicial Branch, the Court is composed of a Chief Justice and six Associate Justices.
The Court holds its regular sessions in Des Moines in the Iowa Judicial Branch Building located at 1111 East Court Avenue on the state Capitol grounds just south of the Iowa State Capitol.


== History ==
In 1846, Iowa became the 29th state to join the United States. Following the constitution of the federal government, the powers of the government in Iowa were divided into the legislative branch, the executive branch, and the judicial branch. In the judicial branch, the Iowa General Assembly divided the state into four judicial districts, and Supreme Court justices were to serve six year terms, while district judges were elected for five year terms. The Iowa Constitution of 1857 increased the judicial districts from four to 11, and allowed the General Assembly to reorganize districts after 1860 and every four years thereafter.


== Functions ==
The Iowa Supreme Court is an appellate court. An appellate court reviews decisions of trial courts in which appeals have been allowed. An appellate court does not preside over trials. Appellate court hearings do not involve witnesses, juries, new evidence, or court reporters. Instead, an appellate court reviews the written record of the trial court to determine whether any significant legal errors occurred. The Rules of Appellate Procedure list the requirements for filing an appeal.
The seven-member Iowa Supreme Court has many important responsibilities.
The Court is the "court of last resort" or the highest court in the Iowa state court system. Its opinions are binding on all other Iowa state courts.
The Iowa Supreme Court has the sole power to admit persons to practice as attorneys in the courts of Iowa, to prescribe rules to supervise attorney conduct, and to discipline attorneys.
The Court is responsible for promulgating rules of procedure and practice used throughout the state courts.
The Iowa Supreme Court has supervisory and administrative control over the judicial branch and over all judicial officers and court employees.


== Justices ==

Justices are appointed by the governor from a list of nominees submitted by the State Judicial Nominating Commission. A justice serves an initial term of office that is one year after appointment and until January 1 following the next judicial retention election after expiration of such year. The regular term of office of justices retained at election is eight years. A justice must retire upon reaching the age of 72. The justices elect the chief justice.
Mark Cady is the current Chief Justice on the Court.
The Court had three vacancies following the defeat of three justices in the November 2, 2010, retention election. Those vacancies were filled in February 2011 by the appointments of Edward Mansfield, Thomas D. Waterman, and Bruce Zager. In March 2011, the Court voted for Justice Cady to continue as Chief Justice.


== Notable decisions ==

The first decision by the Iowa Supreme Court freed a black slave named Ralph in 1858. The US Supreme Court could have followed the Iowa reasoning in the Dred Scott decision, but did not. http://ir.uiowa.edu/cgi/viewcontent.cgi?article=7490&context=annals-of-iowa.


=== Clark v. The Board of Directors ===
In 1868, the Iowa Supreme Court decided Clark v Board of School Directors, ruling that racially segregated “separate but equal” schools had no place in Iowa, 86 years before the U.S. Supreme Court reached the same decision.


=== Arabella A. Mansfield ===
In 1869, Iowa became the first state in the union to admit women to the practice of law, with the Court ruling that women may not be denied the right to practice law in Iowa and admitting Arabella A. Mansfield to the practice of law.


=== Coger v. The North Western Union Packet Co. ===
The Court heard Coger v. The North Western Union Packet Co. in 1873, ruling against racial discrimination in public accommodations 91 years before the U.S. Supreme Court reached the same decision.


=== Varnum v. Brien ===
On April 3, 2009, in Varnum v. Brien, the Iowa Supreme Court unanimously struck down a statutory same-sex marriage ban as unconstitutional, joining the highest judicial bodies of Massachusetts, Connecticut, California, and Hawaii as the fifth court to rule for the right of same-sex marriage under the state constitution.


== See also ==
Courts of Iowa


== References ==


== External links ==
More information on the justices
Website of the judicial branch of Iowa