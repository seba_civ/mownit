Daniel Worcester Faunce (January 3, 1829 – January 3, 1911) was an American clergyman and the father of William Faunce, born at Plymouth, Massachusetts. Graduating from Amherst College in 1850, he then studied at the Newton Theological Institution, was ordained to the Baptist ministry in 1853, and thereafter held charges from 1853 to 1866 in Somerville, Worcester, and Malden — all in Massachusetts — in Concord, New Hampshire (1866–1875), Lynn, Massachusetts (1875–1881), Washington, D.C. (1881–1889), West Newton, Massachusetts (1889–1893), and Pawtucket, Rhode Island (1894–1899). He was a member of the board of managers of the American Baptist Missionary Union. His works include:
Words and Works of Jesus (1873)
Words and Acts of the Apostles (1874)
The Christian in the World (1875)
A Young Man's Difficulties with his Bible (1877)
The Christian Experience (1880)
Hours with a Sceptic (1889)
Prayer as a Theory and a Fact (1890)
Advent and Ascension (1893)
Shall We Believe in Divine Providence? (1900)
The Mature Man's Difficulties with his Bible (1908)
Faunce died on January 3, 1911 in Providence, Rhode Island, at the age of 82.


== References ==

 This article incorporates text from a publication now in the public domain: Gilman, D. C.; Thurston, H. T.; Colby, F. M., eds. (1905). "article name needed". New International Encyclopedia (1st ed.). New York: Dodd, Mead. 


== External links ==

Daniel Faunce at Find a Grave
Faunce, D. W.