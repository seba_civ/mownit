The Servants were an indie band formed in 1985 in Hayes, Middlesex, England by singer-songwriter David Westlake. The band was the original home of Luke Haines.
The band's Small Time album was well received on its 2012 release, more than twenty years after its 1991-recording. The belated release followed the inclusion of 1990's Disinterest in Mojo magazine's 2011 list of the greatest British indie records of all time.


== History ==
Singer-songwriter David Westlake started the band in Hayes, Middlesex with school-friend Ed Moran. The Servants played their first gig at The Water Rats Theatre in London's King's Cross on 1 July 1985.
The line-up for most of the early gigs was: David Westlake, John Mohan, Phil King and Eamon Lynam (a.k.a. Neasden Riots). Declining offers from Statik, Stiff, and Él, they signed with Head Records, set up by Jeff Barrett, later head of Heavenly Records.
Westlake's urbane English song-writing was well received by the press, and the band was invited to record a John Peel session soon after the release of first single "She's Always Hiding" (March '86).
Keen to distance themselves from the "shambling" scene, the band earned a reputation for haughtiness. They grudgingly accepted an invitation by the then-popular NME to appear on their C86 compilation, insisting on the track being B-side of their first single – the wrong-footing "Transparent". The NME compilation turned out to sell well and the Servants became known for a lesser track.
The band's next release, the four-song e.p. "The Sun, A Small Star" (August '86), showed Westlake's song-writing becoming still more deft, the title-track being later described as "a 24 carat 'Brown Eyed Girl' classic".
Luke Haines was in the Servants from early 1987 to late 1991. Drummer Hugh Whitaker left The Housemartins and joined the band as they returned to the studio to demo new material for Creation Records.
In early '88 the Servants moved to Glass Records, who promised a reasonable budget to record an album. Plans were made to go into the studio with John Brand, producer of Hayes punk-band the Ruts. At the eleventh hour the band were told that Glass distributors Red Rhino had "gone bust". The budget was slashed, and they went into the studio to record an epic single, "It's My Turn". They played some gigs to support the single but Glass delayed releasing the record for a year.
The Servants released its debut album, Disinterest, in 1990 on Paperhouse Records. "It is Art Rock," Haines later said, "Ten years too late and fifteen years too early."
Westlake and Haines recorded the Servants' second album, Small Time, in 1991. Not until twenty-one years later – following the inclusion of Disinterest in Mojo magazine's 2011 list of the greatest British indie records of all time, – was it released, in 2012 on Cherry Red Records (then in 2013 on Captured Tracks). Small Time is Westlake's own favourite Servants record. The songs are, says Haines, "looser, more mysterious, strange and beautiful, . . . sounding . . . like nothing else really."
The long unavailability of 1990's Disinterest is explained in the Small Time notes: it is "stuck in an irretrievable record company quagmire, where it looks set to remain." Small Time was issued with a second disc, Hey Hey We're the Manqués, containing demos and rehearsal versions of first-album material.
The Servants' last gig was at the Rock Garden, 21 August 1991 – "With no room to manoeuvre and no opportunities left", the band finally called it a day.
Belle & Sebastian frontman Stuart Murdoch told U.S. music magazine The Big Takeover (issue 53, 2004) he was a huge Westlake fan and that he had tried to locate Westlake in the early '90s in hope of forming a band with him, before starting Belle & Sebastian.
Cherry Red Records released a 2006 retrospective of the Servants, called Reserved. The compilation features all of the releases prior to the Disinterest album plus Peel session tracks and demos. US label Captured Tracks released a 2011 vinyl compilation of the Servants, called Youth Club Disco.
Westlake and Haines played together for the first time in twenty-three years at the Lexington, London N1 on 4 May 2014. Westlake and band played at an NME C86 show on 14 June 2014 in London to coincide with Cherry Red's expanded reissue of C86.


== David Westlake solo ==
David Westlake recorded a solo album for Creation Records in 1987. Backed by Luke Haines (making his recording debut) and the Triffids' rhythm section, Westlake cut against the grain of the paisley psychedeliasts then on the Creation roster, Haines later describing it as a minor classic. Westlake and Haines undertook a tour of Britain to promote the record, but Creation failed to release it until six months later. Westlake received good reviews, but otherwise disappeared.
Westlake's Play Dusty For Me album appeared in 2002. This was released, sort of, in a highly limited issue that quickly sold out but was still never re-pressed. Captured Tracks re-issued Play Dusty For Me in 2015.


== Discography ==
Albums
David Westlake, Westlake (Nov 1987, Creation Records, CRELP019 [LP]; reissued on CD by Sony in 2004 -)
The Servants, Disinterest (Sep 1989, Paperhouse Records, PAPLP005 [LP]/PAPCD005 [CD])
David Westlake, Play Dusty For Me (Jun 2002, Mahlerphone, CDA 001 [CD]; reissued Jul 2010 Angular Recording Corporation ARC 018 [digital])
The Servants, Reserved (2006, Cherry Red Records, CDMRED297 [CD]) (compilation)
The Servants, Youth Club Disco (2011, Captured Tracks, CT-111 [LP]) (compilation)
The Servants, Small Time/Hey Hey We're The Manqués (Oct 2012, Cherry Red Records, CDB RED 535 [2CD]; reissued Dec 2013, Captured Tracks, CT-185 [2LP])
David Westlake, Play Dusty For Me (2015, Captured Tracks, CT-220 [LP & CD])
Singles
The Servants, "She's Always Hiding"/"Transparent" (Mar 1986, Head Records, HEAD1 [7"])
The Servants, "The Sun, a Small Star"/"Meredith"/"It Takes No Gentleman"/"Funny Business" (Oct 1986, Head Records, HEAD3 [12"])
The Servants, "It's My Turn"/"Afterglow" (Sep 1989, Glass Records, GLASS056 [7"])
The Servants, "It's My Turn"/"Afterglow"/"Faithful to 3 Lovers"/"Do or Be Done" (Sep 1989, Glass Records, GLASS12 056 [12"])
The Servants, "Look Like A Girl"/"Bad Habits Die Hard" (- 1990, Paperhouse Records, – [7"])
Videos
David Westlake & Luke Haines, "It's My Turn" (2014)
David Westlake, "Everybody Has A Dream" (2014)
David Westlake, "The Sun, A Small Star" (2014)
The Servants, "Look Like A Girl" (1990)
The Servants, "Who's Calling You Baby Now?" (1988)
The Servants, "The Sun, A Small Star" (1985)


== External sources ==
Derek Sozou, David Westlake site
Luke Haines, Bad Vibes (London: William Heinemann, 2009), 5–10.
Luke Haines, sleevenotes to the Servants' compilation Reserved (Cherry Red Records CDMRED 297, 2006)
Luke Haines, sleevenotes to the Servants' album Small Time (Cherry Red Records CDB RED 535, 2012)
John Peel session information
Servants Retrospective


== References ==
Citations