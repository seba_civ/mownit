James Patrick Keleher (born July 31, 1931) is an American prelate of the Roman Catholic Church. He served as Bishop of Belleville (1984-1993) and Archbishop of Kansas City (1993-2005).


== Early life and education ==
James Keleher was born on the South Side of Chicago, Illinois, to James and Rita (née Cullinane) Keleher. His mother was born in County Kerry, Ireland, and came to Chicago in the 1920s, later abandoning her occupation as a registered nurse to raise her children. His father was a salesman for Will & Baumer Candle Company, a liturgical candle manufacturer. He belonged to St. Felicitas Church, and briefly considered a career as a firefighter as child. After attending Mount Carmel High School for one year, he entered Archbishop Quigley Preparatory Seminary, from where he graduated in 1951. He then studied philosophy and theology at St. Mary of the Lake Seminary in Mundelein.


== Priesthood ==
At age 26, Keleher was ordained to the priesthood by Cardinal Samuel Stritch on April 12, 1958. He then continued his studies at Mundelein until 1962, when he obtained a Doctorate in Sacred Theology with a dissertation on the writings of St. Augustine. In addition to his studies, he served as chaplain and confessor to the Benedictine Sisters of Perpetual Adoration. In 1962 he was named an associate pastor at St. Henry's Church in the Rogers Park section of Chicago.
In addition to his pastoral duties, Keleher was academic dean and teacher of religion and social studies at Quigley North from 1966 to 1969. He then served as dean of formation at Niles College (1969-1972) and at Mundelein Seminary (1972-1975), and as rector of Quigley South (1975-1978). During this period, he also furthered his studies in spiritual theology at Rome. In 1978 he was named president and rector of Mundelein Seminary, where he also served as an associate professor of systematic theology.


== Episcopal ministry ==
On October 23, 1984, Keleher was appointed the sixth Bishop of Belleville by Pope John Paul II. He received his episcopal consecration on the following December 11 from Cardinal Joseph Bernardin, with Bishops William Michael Cosgrove and Thomas Joseph Murphy serving as co-consecrators.
Following the retirement of Archbishop Ignatius Jerome Strecker, Keleher was named the third Archbishop of Kansas City, Kansas, on June 28, 1993. He was installed at the Cathedral of St. Peter the Apostle on the following September 8. After eleven years as archbishop, he resigned on January 15, 2005.


== References ==