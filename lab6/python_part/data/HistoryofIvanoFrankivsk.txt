Ivano-Frankivsk (Ukrainian: Івано-Франківськ, Ivano-Frankivs'k; Polish: Iwano-Frankowsk; German: Iwano-Frankiwsk; Russian: Івано-Франкoвск, Ivano-Frankovsk, see also other names) is one of administrative centers in western Ukraine with almost 350 years of history as a city settlement. For the most part of its history the city was known for its Polish name of Stanisławów (Stanislaviv) until 1962 (300 years). In the Soviet times it was decided to change the name of the city during its 300th Anniversary. The current name of the city is longer than some sentences, therefore the local population sometimes refers to it as Frankivsk or even Franyk.


== History ==


=== Establishment (Andrzej na Potoky) ===

The city, named Stanisławów (Stanyslaviv), was erected as a fortress to protect the Polish-Lithuanian Commonwealth from Tatar invasions and to reinforce the region in case of some other Khmelnytsky Uprising would occur. It was built out of a fort that was erected next to the villages of Zabolotiv which had been known since 1435 and Knyahynyn (1449). The village of Zabolotiv and the land around it were purchased by Stanisław Rewera Potocki from another Polish nobleman Rzeczkowski. The area was utilised for recreation, in and particular hunting. The city's name was later coined by the Stanisław's son, Polish nobleman Andrzej Potocki commemorating it either to his father or his first-born son Stanisław Potocki.

Andrzej issued his declaration of establishing the city of Stanislawow by the Magdeburg rights on May 7, 1662 for economical purposes of creating a city market, while allowing its local population to organize a city government headed by Wójt (Vogt), city council, and city court. The Magdeburg rights also allowed for creation of various craftsman shops, independent craftsmen guilds, and, the most importantly, the freedom of religion. However, it was not until August 14, 1663 that the city and its rights were recognized by the Polish Crown when Jan Casimir has finally approved them along with the city's heraldry. The first architect of the Stanisławow fortress was from Avignon, Francisco Corasini at the time when Andrzej initiated the redesigning of the Zabolotiv and Knyahynyn villages into a fortress in 1650. The fortress had two main gates which were known as the Halytska gate and Tysmenytska gate. The alternative names were Lvivska and Kamianetska respectively. The names were given for the direction in which they were facing. There was one more smaller gate known as Armenian or Zabolocki.
On September 17, 1662 Andrzej awarded the Jewish community of the city the right of self-government, which included permission to build their own schools, community buildings, and others. On May 23, 1663 the Armenian community of the city was allowed to build its own church as well, which was finished in 1665. Also in 1663 monks of Trinitarian Order from Warsaw arrived in Stanisławów. On April 24, 1664 the newly created city's Butchers Guild was awarded the "20-year freedom" exception from taxation. In 1666 was finished the city's ratusz.
By 1672 the fortress was restructured out of wood into brick. Also a new large fortified Potocki palace was erected in the place of older smaller wooden one. Today this building serves as the military hospital. In the same year Jews were granted the right to become permanent residents, and who could work, conduct commerce, and come and go from the city as they pleased. In 1666 the city's first town hall (or ‘ratusha’) was erected and built out of wood. Soon after the Turks conquered the fortress of Kamianets-Podilskyi in 1672, Stanisławow, together with Halych, became the strongholds against Turkish forces. It was attacked and besieged in early September 1676, but the Turks did not manage to capture and pillage the city. However, some of Stanisławow fortifications were so badly damaged that in 1677 the Sejm in Warsaw relieved the city of its tax duties. On September 12, 1683 the oldest son of the city founder - Stanisław perished in battle against the Turks near Vienna. His body was transferred to the native city and buried in the Potocki family parish kosciol also known as Fara (today the Art museum on Sheptytsky Square).


=== 18th century ===

Originally the city was divided into two districts: Tysmenytsia and Halych. Sometime in 1817-1819 the neighboring village of Zabolottya, that had a special status, was incorporated into the city as a new district, while the Tysmenytsia district was divided into Tysmenytsia and Lysets districts. Each district had its main street corresponded with its name: Halych Street (Halych district), Tysmenytsia Street which today is Independence Street (Tysmenytsia district), Zabolotiv Street - Mykhailo Hrushevsky Street and Street of Vasylyanok (Zabolottya district), and Lysets Street - Hetman Mazepa Street (Lysets district). Later the city was split into six small districts: midtown where lived rich catholic population and patricians, pidzamche (subcastle), and four suburbs - Zabolotiv, Tysmenytia, Halych, and Lysets where lived plebeians. Jews were assigned a specific "Street of the Jews" near the river, where they were allowed to live. The first Jewish cemetery was established in 1662. By 1672 a wooden synagogue had been built. The Chevra Kadisha burial society was also founded. 
According to the 1709 census in the fortified midtown lived 62 Ruthenian families, 50 Armenian, 25 Jewish, and 9 Polish. In the city operated the Polish-Armenian court that had a strict stance against the local peasant uprising known as Opryshky. The last public execution that took place in the city was on April 25, 1754 at the Market Square where Vasyl Bayurak was killed. Due to numerous military conflicts, diseases, and other socially dangerous events, the population of the city by the end of 18th century did not supersede 5,000. Among such events was the invasion of the city by the Russian forces in course of the Great Northern War in 1706 that robbed the city in the revenge for Józef Potocki's switching the sides in the support of Stanisław Leszczyński. In 1712 the city was robbed again during some inter-magnate conflicts when it was invaded by the forces of Polish Hetman Sieniawski. In 1710 a quarter of the city population (1332) died of typhus.
The streets in the city were starting to be paved in cobbles around 1695. In 1728 the Akademia Stanislawowa was converted into Jesuit Collegium for which a separate building was erected in 1733-1743. In 1729 the Jesuit Church was built in the city, around 1744 the city's Jewish community started to the construction of a new synagogue which was finished in 1777, and in 1762 the Armenian Church was restored. In 1767 the city brewery was built as the oldest industrial venture, building of which was preserved to our days. In 1759 the Jewish community of Stanisławów took part in a dispute with the Frankists from Lwow eventually joining the later group (see Jacob Frank). Due to that the Stanislawow rabbis were subordinated to the Rabbi of Tysmenytsia. One of the most prominent Jewish figures of that time was Rabbi Dov Berish, a son of Yaakov Avraham, as well as Rabbi Yehuda Zelka known for his commentary "Ravid Zahav" on "Yoreh De'ah", and many others.
On February 26, 1761 the city was passed to Vincent Potocki who was a minor. The city was managed by his guardian Kateryna Kossakiwska who was also of the Potocki family. Eventually the Potocki family went bankrupt and the city was passed to the state treasury. Extensively rebuilt during the Renaissance, it was sometimes called Little Leopolis. The city was also an important centre of Armenian culture in Poland after the fall of the Armenian Kingdom of Cilicia, with an Armenian church in which a painting of Mary was kept. The painting was in 1945 moved to Gdansk.


=== Austria-Hungary ===
In 1772, after the Partitions of Poland it became a part of the Austro-Hungarian Empire, and successively of the autonomous Kingdom of Galicia and Lodomeria. The Austrian riflement entered the city on October 25 of that year. The new administration ceased the functioning of the city fort. According to the "Vienna patent" since 1789 a city magistrate was introduced as form of government headed by its burg-minister, while the city itself was returned into ownership of countess Kossakiwska until 1797. In 1801 due the next bankruptcy the city was passed now into the Austrian state possession. Since then and until 1820 all the fortifications in the city were disassembled and their materials were used to build new buildings and pave streets. The moats around the fortifications were evened out and changed into streets. With the rock material from the fortifications were cobblestoned four city squares and 24 streets. One of the first streets that appeared outside of the city fortifications were Dvirska (today Chodkewicz), Mlynarska, Tartakova (Dudayev), Polyova (Petlyura). On the territory of the former moats today run the following streets: Sich Riflemen, Dnistrovska, and Vasyliyanok. Some other streets such as Valova, Starozamkova, and Fortechna, kept their historical toponyms. By the start of the 20th century the adjacent villages of Knyahynyn and Sofiivka were fully incorporated into the city. The street that connected the railway station with the old town (midtown) the city magistrate named Grunwaldska to commemorate the 500 Anniversary of the Battle of Grunwald.

The center of education and culture became the First state German-Polish gymnasium that was founded in 1774. One of the famous students of that school was the Ukrainian writer, historian, and ethnographer Ivan Vahylevych who studied there in 1824 - 1830. On May 8–10, 1848 during the Spring of Nations in the city was established the Rus Council (Ukrainian: Руська рада) and was formed the National Guard. On September 2, 1848 the first city newspaper was issued in the Polish language "Kurier Stanislawowski". In 1862 the first recorded city celebration took place to commemorate the 200th Anniversary of the foundation of the city. On September 1, 1866 the city was connected to a railway network Lviv-Chernivtsi, while the locomotive-repair shop was opened along with the train terminal. At around that time series of plants and factories were built.

On September 28, 1868 Stanisławów experienced a major disaster. The city was engulfed in a major fire which originated at Lypova street and destroyed the third of the city (some 260 buildings) and the market place of the town. The city required a major renovation and was almost completely rebuilt. A new six-stories rathaus was built in 1871. During that time the center of the city slowly moved from the market square southward towards the Tysmenytsia Road (today Nezalezhnist street - stometrovka). Here for the first time in all Galicia gas street lights were installed in 1876. Dr. Arthur Nemhein was the mayor of the city from 1897 to 1919, but was later fired by Polish authorities in 1919 for cooperating with Ukrainian separatists. In the elections to the Austrian parliament of 1907, Dr. Marcus Braude, a Zionist candidate, gained the majority of votes. During World War I, the front-line was for some time in the area of the city, Russians and Austro-Hungarian forces fought several battles in Stanisławów and its vicinity. In 1917 Russian forces burned the central districts during the Kerensky Offensive.


=== Start of the 20th century ===

In October 1918, the Austro-Hungarian Empire collapsed and the Western Ukrainian People's Republic (ZUNR) was proclaimed.
In the early months of 1919 (from January to May) the city became a temporary capital of the West Ukrainian National Republic, while still recovering from World War I. All the state affairs were taken place in the building of Dnister Hotel where the Act Zluky was composed. The same year it was subjected to the Polish–Ukrainian and the Romanian-Ukrainian skirmishes eventually being annexed by Poland as part of the Second Polish Republic as the center of the Stanisławów Voivodship. It was occupied by the Romanian army for the summer months from May 25 through August 21, 1919.
During the Polish-Soviet War in 1920, the Red Army took over the city for a brief period. After the Soviet retreat, Ukrainian troops loyal to Symon Petlura occupied the city for a few days. At this period of history the city was in complete disorder.
According to the 1931 Polish census there were 198,400 residents in the Stanisławów county (159 per square kilometre, the area of the county was 1,249 km2 (482 sq mi)). Among them there were 120,214 Poles, 49,032 Ukrainians, and 26,996 Jews. The population of the city itself was as follows: 27,000 in 1900, 28,200 in 1921 and 60,000 in 1931 (70,000 together with the suburb of Knyahynyn). Knyahynyn was incorporated into the city of Stanisławów on January 1, 1925 by the decision of Rada Ministrów from November 17, 1924. During the interbellum period, Stanisławów was a large military base for the Polish Army, with two major units stationed there – 11th Infantry Division and Podolska Cavalry Brigade.
In the 1939 invasion of Poland by German and Soviet forces, the territory was captured by the Soviets in September 1939 and included into the Ukrainian SSR. Between September 1939 and June 1941, the Soviet regime ordered thousands of inhabitants of the city to leave their houses and move to Siberia, where most of them perished. Numerous people were taken out of the city prison and simply shot outside of the city when Soviet forces were leaving it in 1941.


=== Nazi occupation ===
There were more than 40,000 Jews in Stanisławów when it was occupied by the Nazi Germany on July 26, 1941. During the occupation (1941–44), more than 600 educated Poles and most of the city's Jewish population were murdered.
On August 1, 1941, Galicia became part of the General Government. On October 12, 1941, later called "Blutsonntag" ("Bloody Sunday"), the Nazi Security Police (Sicherheitspolizei; SiPo) rounded up thousands of Jewish residents. They were forced under escort to the Jewish cemetery, where mass graves had already been prepared. Here they were ordered to strip naked and were shot by the SiPo, assisted by the German Order Police (Ordnungspolizei) and the railroad police. An estimated 8,000 and 12,000 Jews were killed.
On August 8–9, 1941, the Security Police commanded by Hans Krüger with the help of the Ukrainian Auxiliary Police arrested more than 250 members of the Polish Intelligentsia (mainly teachers, professors). At night around 14th on August 15, they were transported to a place near the city, named the Black Forest and executed. 
The remaining Jews were sealed into the Stanisławów Ghetto, which was walled up on December 20, 1941. Around 1,000 Jews were shot in an "Aktion" purportedly as a reprisal for the death of a Ukrainian. Some of the female victims were raped before they were killed. The final liquidation of the ghetto was ordered on February 22 or 23, 1943, at which time there were around 11,000 Jews remaining. The town was declared Judenfrei (free of Jews) four days later. At this time 27 members of the UPA were shot by the Nazi government in the centre of the city.
When Jehovah's Witnesses learned that the Nazis planned to execute all Jews in the city, they organized an escape from the Jewish ghetto for a woman of Jewish origin and her two daughters who later became Witnesses. Risking their own lives, the Witnesses hid these Jewish sisters throughout the entire period of the war.
The Red Army liberated Stanisławów on July 27, 1944, at which time there were around 100 Jews in the city who had hidden and survived. Around 1,500 Jews from Stanisławów survived the war.
SS-Hauptsturmführer Hans Krueger was indicted in October 1965 for crimes committed at Stanisławów. He was sentenced to life imprisonment on May 6, 1968, and was released in 1986. Trials against Schupo and Gestapo members who took part in massacres at Stanisławów took place in Vienna and Salzburg in 1966.


=== Recent history ===

Beginning in 1944 it was a part of the Soviet Union. The Soviets forced most of the Polish population to leave the city, most of them settled in the Recovered Territories. During the post-war period the city was part of the Carpathian Military District housing the 38th Army (70th Motor Rifle Division) that participated in the Operation Dunai. On March 1, 1945 with the help of the documents of regional state archives the Stanislav regional extraordinary commission in the investigation of Nazi crimes composed lists of executed pedagogues of Stanislav in 1941. The same year, the city's Medical Institute (today's university) was opened. On April 11, 1945 Stanishlav's bishop Hryhory Khomyshyn (1867–1947) was arrested. On August 16 the "Prykarpattian Pravda" (local newspaper) announced about the start of preparation works in the reconstruction of the city on which the Soviet government of Ukraine promised 100,000 rubles. According to a plan the reconstruction was supposed to take place for the next 20 years, however some of them had to be finished by 1945. T.Klochko was appointed author of the project.
On October 31, 1945 a local guerrilla group "Chornyi Lis" (Black Forest, name of the forests outside of the city) headed by Vasyl Andrusyak conducted a raid on the city occupying a store of the Regional Customer Association (Oblspozhyvspilka), medical warehouses, and taking hostage several officials of the local Communist party and NKVD. Until February of the next year the Soviet authorities were conducting "cleansing" of the local area burning down woods around the city of Stanislav and conducting ambushes on centers of Ukrainian Insurgent Army in the area. On February 25 the body of the killed Vasyl Andrusyak (also known as Hrehota-Rizun) was brought to the city where he was viewed for four days by several Soviet officials.
On September 1, 1946 in the city was opened a pharmaceutical school that existed until July 1949. The same year there also was established a food company "Peremoha" (Victory) in production of bread, confectionery, sausage (kielbasa) food. Later that year the city park of Shevchenko there was opened stretching over some 22 ha (54 acres).
In 1958 the adjacent village of Pasichna was annexed to the city. Today it is being referred to as a city's locality. In few years (1962) the village of Opryshivtsi was added to the city. In 1962 the name was changed to honour Ukrainian writer Ivan Franko. Five years later the Ivano-Frankivsk National Technical University of Oil and Gas was established.
In the early 1990s the city was a strong centre of the Ukrainian independence movement.
In 2002, the Anti-Defamation League (ADL) called a move by the city council inexcusable and "profoundly insulting to honour Nazi war veterans of the SS Galicia division as "fighters for independence" whom the head of the SS, Himmler, congratulated in May 1944 for having cleansed Ukraine of all its Jews. ADL authorities chose to completely ignore the previous legal cases involving the Division in war crimes (see Halychyna Division).


== City Mayors ==


=== Castles/cities owners ===
Great and Field Hetman Stanisław "Rewera" Potocki 1654 - 1667
Field Hetman Andrzej Potocki 1662 - 1692
Great Hetman Józef Potocki 1692 - 1751
Great Straznik Stanisław Potocki 1751 - 1760
Stanislaw's widow Helena Zamojska 1760 - 1761
Franciszek Ksawery Potocki and Wincenty Potocki (as minors)
Katarzyna Kosakiwska Potocka 1761 - 1789
In 1786 Katerzyna's rights to the city were cut, however she continued to receive a certain portion of city's taxes. In 1797 she sold her city estate to her brother Prot Potocki whose property was transferred to a religion fund in 1801 due to great debts. Next year the Austrian authorities managed to buy the city out of debts.


=== City's starostas ===
Francisco Kratter 1827


=== City's Representatives to the Austrian Reichsrat ===
O.Prokopczyc June 3, 1848


=== Burgomasters (Galicia) ===
Since January 23, 1867 Stanislau (Stanislawow) became an administrative center of county (powiat).
Antoni Suchanek 1867 - 1868
Ignaci Kaminski 1870 - 1888
Valeri Shidlowski 1889 - 1896
Arthur Niemgin 1896 - 1919


=== Burgomasters and City-Presidents (Poland) ===
Mihal Ferensiewycz 1919 - 1921
Stanislaw Teodorowycz 1921 - 1923
Teofil Zaidler 1923
Wolodziemir Dambrowski 1924
Waclaw Chovanec 1924 - 1935
Zdislaw Stronski (president) 1935 - 1937
Franciszek Kotliarczuk (president) 1937 - 1939


=== Soviet/German invasions ===
V.Chuchukalo (temp. government) Sep.1939 - Dec.1939
Petro Ivanov December 20, 1939 - June 22, 1941
Ivan Holembiovsky 1941 - 1944


=== Head of the city council executive committee ===
Afanasi Shatokhin 1944 - 1945
Mykhailo Lazarenko 1945 - 1953
Mykola Koval 1953 - 1957
Yefrem Ferchuk 1957 - 1963
Yevhen Babenko 1963 - 1972
Omelian Tomei 1972 - 1980
Oleksandr Bekhtyev 1980 - 1987
Bohdan Yakovychyn 1987 - 1990
Yaroslav Taylikh (Rukh) March 4, 1990 – June 25, 1994


=== City Mayors ===
Bohdan Borovych (OUN) July 1994 – June 1998
Zinoviy Shkutiak (Our Ukraine) March 1998 – March 26, 2006
Viktor Anushkevychus (UPP) March 26, 2006 – November 2015
Ruslan Marcinkiv Svoboda (political party) November 2015 – present


== References ==


== External links ==
Website dedicated to the city of Ivano-Frankivsk