VfB Fichte Bielefeld is a German association football club based in Bielefeld, North Rhine-Westphalia. The club was formed out of the merger of VfB Bielefeld 03 and SpVgg Fichte Bielefeld 06/07 on 1 July 1999.


== History ==

Of the these predecessor sides VfB Bielefeld 03 was the more successful, as SpVgg remained an anonymous local side. In 1931 VfB finished runners-up in the Western German football championship. Two years later German football was re-organized under the Third Reich into sixteen top flight divisions and VfB eventually earned promotion to the Gauliga Westfalen in 1941. However, they were immediately relegated after finishing at the bottom of the table well back of other clubs. In 1943 VfB joined Arminia Bielefeld in the wartime side (Kriegspielgemeinschaft) KSG VfB/DSC Arminia Bielefeld to play to the Gauliga. The combined side also finished last, but remained in the top flight through the incomplete 1944–45 season as the league struggled to continue play as Allied armies advanced on Germany.
After the war the two clubs resumed play as separate sides. VfB's best result was ascent into the 2. Oberliga West in 1950 and participation in the semi-finals of the German amateur championship. Through the 60s and into the early 70s the club played as a third division side in the Verbandsliga Westfalen-NO, before slipping and bouncing between fourth and fifth tier competition until the end of the 90s.
SpVgg played in the Landesliga Westfalen (IV) during most of this period, with the occasional descent to the Verbandsliga (V) or Bezirksliga Westfalen (VI).
At the time of their 1999 merger VfB and SpVgg were both competing in the Verbandsliga Westfalen (V). Two years later the new club was promoted to the Oberliga Westfalen (IV) where they remained until being sent down in 2006. Since 2013 the club is part of the tier six Westalenliga again.


== Honours ==
The club's honours: VfB Bielefeld
Western German football championship
Runners-up: 1931

Verbandsliga Westfalen
Champions: 1973

VfB Fichte Bielefeld
Verbandsliga Westfalen
Champions: 2001


== Stadium ==
Fichte plays its home matches at the Stadion Rußheide which has a capacity of ~12,000 spectators.


== External links ==
Official team site
Abseits Guide to German Soccer