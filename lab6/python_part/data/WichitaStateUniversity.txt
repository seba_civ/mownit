Wichita State University (WSU) is a public research university in Wichita, Kansas, United States. It is the third-largest university governed by the Kansas Board of Regents.
Wichita State University offers more than 60 undergraduate degree programs in more than 200 areas of study in six colleges. The Graduate School offers 44 master's degrees in more than 100 areas and a specialist in education degree. It offers doctoral degrees in applied mathematics; audiology; chemistry; communicative disorders and sciences; nursing practice; physical therapy; psychology (programs in human factors, community and APA-accredited clinical psychology); educational administration; aerospace, industrial and mechanical engineering; and electrical engineering and computer science.
Wichita State University also hosts classes at four satellite locations. West Campus is located in Maize. This 9-acre (3.6 ha) campus hosts 100–150 university classes each academic semester. The university's South Campus began offering Wichita State University coursework at a new facility in Derby in January 2008. The WSU Downtown Center houses the university's Center for Community Support & Research and the Department of Physical Therapy. A quarter-mile northeast of campus, the Advanced Education in General Dentistry building, built in 2011, houses classrooms and a dental clinic. It is adjacent to the university's 75,000-square-foot (7,000 m2) Eugene M. Hughes Metropolitan Complex, where many of WSU noncredit courses are taught.


== History ==
Wichita State University began in 1886 as Fairmount College, a private Congregational preparatory school, founded by Rev. Joseph Homer Parker. Initially it was referred to as "Young Ladies College", "Wichita Ladies College", and "Congregational Female College". It was to admit women twelve years and older who were "able to read, write, spell and recite the parts of speech." Support came mainly from the Plymouth Congregational Church to build it, but the school never opened its doors. In 1892, a corporation bought the property and named the preparatory school Fairmount Institute. It opened in September to men and women, with an emphasis on training in preaching or teaching. It closed because of financial difficulties.
In 1895, on the same site, Fairmount College opened collegiate classes for men and women with funding by the Congregational Education Society. Amid growing financial troubles, the college's supporters tried to get the city of Wichita to buy it in 1925, but failed. A second referendum passed in 1926, and that fall it became the Municipal University of Wichita (popularly known as "Wichita University" or "WU"). It was the first municipal university west of the Mississippi, and catered to students of limited means. On July 1, 1964, the school officially entered the state system of higher education as Wichita State University (WSU).
WSU is one of three research institutions in the state of Kansas, along with Kansas State University (KSU) and the University of Kansas (KU).


== Campuses ==

The Main Campus is located at 1845 North Fairmount in northeast Wichita, is mostly bounded between the streets of 17th St N, 21st St N, Hillside St, Oliver Ave. Research facilities include the National Institute for Aviation Research, biology research labs (Hubbard Hall), the WSU Field Station, chemistry research labs (McKinley Hall), and physics research labs (Jabara Hall). The campus includes the Edwin A. Ulrich Museum of Art.
WSU has four satellite campuses: The South Campus is located at 200 West Greenway in Derby, Kansas, began offering Wichita State University coursework in January 2008. The West Campus is located at 3801 North Walker in northwest Wichita near Maize, Kansas. This 9 acre (3.6 ha) campus hosts 100–150 university classes each academic semester.


== Academics ==
The University comprises the following six academic colleges:
W. Frank Barton School of Business
College of Education
College of Engineering
College of Fine Arts
College of Health Professions
Fairmount College of Liberal Arts and Sciences
Wichita State University was ranked the 35th top college in the United States by the Social Mobility Index college rankings. Wichita State is placed among National Universities in the United States in rankings done by U.S. News & World Report. For all engineering research and development expenditures, WSU ranked No. 63 in the USA for year 2013, with $47 million The National Science Foundation ranked Wichita State University No. 4 among all U.S. universities in money spent on aerospace research and development in fiscal year 2013, with $39 million in expenditures and No. 1 in industry-funded aerospace R&D. Wichita State's W. Frank Barton School of Business was listed in The Princeton Review 2011 "301 Best Business Schools," ranked as the 11th best program in the country for students seeking an undergraduate degree in entrepreneurship for 2007.
The Aerospace Engineering department was founded in 1928 and has longstanding collaborative relationships with Airbus North America, Boeing, Bombardier-Learjet, Cessna, Hawker Beechcraft, Spirit AeroSystems, and other Wichita aviation concerns. The department teaches in the areas of composites, structures, Engineering mechanics, computational Fluid dynamics, applied Aerodynamics, and Flight simulation. Students can readily do internships at the nearby airports and many airplane company like Cessna, Learjet, etc.
The Wichita State University Libraries have holdings of more than 2 million volumes, over 200 electronic databases and more than 70,000 journal subscriptions. The University Libraries consist of the main Ablah Library, the McKinley Chemistry Library, the Thurlow Lieurance Music Library and University Libraries Special Collections. The libraries are open to community users and serve as a regional United States Federal Government Documents Depository, a State of Kansas Government Documents Depository, and is the State of Kansas' only Patents and Trademarks Library. WSU Special Collections and University Archives contains numerous rare books, incunabula, historical manuscripts collections, maps and photographic archives documenting Kansas history, as well as hosting the Wichita Photo Archives.
In 2014, WSU President Dr. John Bardo announced plans to launch a major academic and student life initiative, dubbed the "Innovation University."  The plan kicked off with the completion of renovations to the university's student union, the Rhatigan Student Center, and the opening of Shocker Hall, a new 318,000-square-foot, 784-bed housing facility on the main campus. It includes public/private partnerships with domestic and international companies that would build offices on the WSU main campus and collaborate with the students and faculty on research projects and product development through a technology transfer system. The university has secured partnerships with three companies: Wichita-based ABI Group of Companies; Sunnyvale, Calif.-based NetApp; and the Indian IT firm Tech Mahindra. The plan calls for the addition of more than 20 new buildings, including a prospective new building for the Frank W. Barton School of Business, a new residence hall, commercial offices, "creative collision" facilities, two mixed-use developments and a hotel. Ground will break on the first building, the Technology Transfer/Experiential Learning Building, in Q1 2015. The development will occur on the site of the WSU-owned Braeburn Golf Course adjacent to campus, which closed in November.


=== Admissions and tuition ===

Freshmen are required to live on campus and have a meal plan. The average ACT score among Wichita State University students is 23, and the average high school GPA for the entering Fall 2012 class was 3.35. For 2012, WSU had 3,515 total freshman applications and admitted 3,347 of those for an admission rate of 95.2 percent, with an entering class of 1,359. In 2011, the university had 3,304 total applicants, admitted 3,102 of them (admission rate of 93.8%) with an entering class GPA of 3.39 amongst the 1,366 students.
Of the 6,122 members of the 2006 freshman class, 290 had been named valedictorian of their high school's graduating class.
Tuition for full-time, Kansas residents attending Wichita State for the 2010–2011 academic year is $5,890. While fees for out-of-state residents for the 2010–2011 academic year are $13,924, tuition at Wichita State for Kansas residents placed it as the exemplary public university, slightly beneath the weighted average tuition among Kansas's six public four-year universities.


== Athletics ==

WSU is an NCAA Division I institution and fields teams in tennis, cross-country, basketball, track, golf, men's baseball and women's volleyball and softball. Also, it offers club sports such as crew, bowling, shooting sports, and other intramural sports.
The men's baseball team is college baseball's highest winning team for the past 31 years, with numerous conference championships and NCAA tournament appearances. The baseball team won the national championship in 1989 and was runner-up in 1982, 1991 and 1993. They play at Eck Stadium.
The men's basketball team has played in the NCAA tournament ten times since 1954, advancing to the Final Four in 1965 and 2013, the Elite Eight in 1981, and the Sweet Sixteen in 2006 and 2015. The team also won the 2011 National Invitation Tournament Championship, beating the Alabama Crimson Tide. Among the Wichita State players who have played in the NBA are All-Star Xavier McDaniel, power forwards Antoine Carr and Cliff Levingston, two-time All-American Dave Stallworth, center Gene Wiley, guards Gal Mekel and Toure' Murry, and Greg Dreiling. Four-time All-American Cleo Littleton joined the Shocks in 1951, breaking the unofficial color barrier in the Missouri Valley Conference.
The men’s and women’s bowling teams have won numerous USBC Intercollegiate Team Championships, including the men’s 2003, 2008, 2009 and 2010 title and the women's 2005, 2007 and 2009 title.
Shocker Track and Field History: Seven Olympians. Two National Champions. 60 NCAA All-Americans. Under Steve Rainbolt (2001-2012): 14 Missouri Valley Conference Championship Teams. 29 NCAA All-Americans.
Men's Cross Country: Established in 1947. Eight Missouri Valley Conference titles, five consecutive (1971–75). Five NCAA All-Americans. Nine Missouri Valley Conference Champions. 46 All-MVC award winners.
Women's Cross Country: Established in 1983. 10 Missouri Valley Conference titles, six consecutive (2005–10). Four NCAA All-Americans. Six Missouri Valley Conference Champions. 56 All-MVC award winners.
The school discontinued its football program following the 1986 season due to poor attendance, financial red ink, NCAA recruiting violations, and the state of disrepair of Cessna Stadium. It had been never fully recovered from losing 16 starters, its athletic director, football coach and many others critical to the WSU program in a plane crash in 1970 (see below). Legendary NFL coach Bill Parcells was a linebacker at WSU in 1962 and 1963 before serving as a graduate assistant in 1964. Wichita State University was also the first Division 1-A school to hire a black head coach in College Football, Willie Jeffries in 1979.


=== Shockers ===
The name for WSU's athletic teams is the Shockers and, collectively, students are also referred to as being "Shockers." The name reflects the University's heritage: Early students earned money by shocking, or harvesting, wheat in nearby fields. Early football games were played on a stubbled wheat field. Pep club members were known as Wheaties. Tradition has it that in 1904, football manager and student R.J. Kirk came up with the nickname Wheatshockers. Although the Wheatshockers name was never officially adopted by the university, it caught on and survived until it was later shortened to Shockers. Until 1948, the university used a nameless shock of wheat as its symbol. WuShock came to life when junior Wilbur Elsea won the Kappa Pi honorary society's competition to design a mascot typifying the spirit of the school. Elsea, who had been a Marine during World War II, decided that "the school needed a mascot who gave a tough impression, with a serious, no-nonsense scowl."
Once Elsea's mascot was adopted by the university, which by that time was known as the Municipal University of Wichita, all that was needed was a name. The Oct 7, 1948, issue of The Sunflower, the student newspaper, ran an advertisement urging students to submit names for the school's new mascot. It was freshman Jack Kersting who suggested the winning name, "WuShock."
In 1998, WuShock, also referred to as "Wu," marked his 50th birthday by undergoing a redesign and getting a pumped-up physique and revved-up attitude. The mascot's costume has changed over the years, as well. With the redesign, a new costume was introduced in fall 1998. In fall 1999, the head of the new costume underwent another redesign after a number of supporters suggested the mascot needed a more intimidating look. In 2006 it was decided to once again update the Wu costume. The general consensus was that many wanted the costume to more accurately reflect the depiction of WU in the school's logo. The new WuShock now has the ability to run, jump, and walk up stairs without help. Many officials feel that a more professional and intimidating mascot on the field will certainly bolster WSU's image.


=== Football team plane crash ===

On October 2, 1970, the first, or "gold" plane (the twin plane to the second, or black, plane) carrying players and staff of the WSU football team took off from a Colorado airport after refueling, bound for Logan, Utah, for a game against Utah State University. It flew into a mountain valley too narrow to enable it to turn back and smashed into a mountainside, killing 31 of the 40 players, administrators, and fans near a ski resort 40 miles (64 km) away from Denver. President Richard Nixon sent the president of the university a note which read, "Our thoughts and prayers go out to you in this time of sorrow."


== Notable alumni and faculty ==


== References ==


== Further reading ==
History of Wichita and Sedgwick County Kansas : Past and present, including an account of the cities, towns, and villages of the county; 2 Volumes; O.H. Bentley; C.F. Cooper & Co; 454 / 479 pages; 1910. (Volume1 - Download 20MB PDF eBook),(Volume2 - Download 31MB PDF eBook)
Kansas : A Cyclopedia of State History, Embracing Events, Institutions, Industries, Counties, Cities, Towns, Prominent Persons, Etc; 3 Volumes; Frank W. Blackmar; Standard Publishing Co; 944 / 955 / 824 pages; 1912. (Volume1 - 54MB PDF), (Volume2 - 53MB PDF), (Volume3 - 33MB PDF)


== External links ==
Official website
Wichita State Athletics website
Main Campus Map (PDF)