The Serbs of Sarajevo numbered 157,526 according to the 1991 census, making up more than 30% of the city's population. Today, following the Bosnian War, few Serbs remain in Sarajevo. Forcefully, most have either moved abroad, to Serbia or other countries, or moved to a new settlement on the outskirts of Sarajevo, located in the Republika Srpska, known as East Sarajevo (previously Srpsko Sarajevo - Serbian Sarajevo).


== Churches ==
There are three main Serb Orthodox Christian places of worship in Sarajevo: the Old Orthodox Church (Serbian: Стара православна црква or Stara pravoslavna crkva), dating back to the 16th century, the Cathedral Church (Саборна црква or Saborna crkva), which was erected in the 1860s, and the Church of Sveto Preobraženje in Novo Sarajevo.


== Notable people ==

Notable Serbs who were born in or lived in Sarajevo include:
Manojlo Jeftanović, merchant
Sima Milutinović Sarajlija
Bishop Georgije (Đorđe Nikolajević), theology professor, Orthodox priest, and monk, Dabar-Bosnia Metropolitan bishop (1885-1896)
Jovan Marinović, politician and diplomat, Principality of Serbia President of the Ministry (1873-1874)
Dimitrije Jeftanović, merchant and industrialist
Petrakija "Petro" Petrović, merchant
Hadži Makso Despić, fur merchant
Mićo Despić, fur merchant
Stevo Petranović, schoolteacher, dramaturgist, and translator
Sava Kosanović, theologist, schoolteacher, Orthodox priest, and monk, Dabar-Bosnia Metropolitan bishop (1881-1885)
Bishop Nikolaj (Petar Mandić), theologist, Orthodox priest, and monk, Dabar-Bosnia Metropolitan bishop (1896-1907)
Gligorije Jeftanović, merchant, industrialist, and politician, Hotel Evropa owner (1882-1927)
Vladan Đorđević, physician and politician, Mayor of Belgrade (1884-1885), Kingdom of Serbia President of the Ministry (1897-1900)
Bishop Evgenije (Manojlo Letica), lawyer and Orthodox monk, Dabar-Bosnia Metropolitan bishop (1907-1920)
Nikola T. Kašiković, writer and publisher, Bosanska vila owner and editor-in-chief
Hieromartyr Petar (Jovan Zimonjić), theology professor and Orthodox monk, Dabar-Bosnia Metropolitan bishop (1920-1941), canonized as hieromartyr in 1998
Vladislav Skarić, tobacco merchant, historian, geographer, and SANU academician
Bishop Nektarije (Nikola Krulj), Juris Utriusque Doctor, theology professor, and Orthodox monk, Dabar-Bosnia Metropolitan bishop (1951-1966)
Jevto Dedijer, geographer
Vojislav "Đedo" Kecmanović, Balkan Wars volunteer soldier, World War II Partisan, and post-war communist politician
Lazar Drljača, painter
Dušan Jeftanović, merchant, industrialist, and juris doctor, Hotel Evropa owner (1927-1941)
Bogdan Žerajić, revolutionary
Veljko Čubrilović, Young Bosnia revolutionary
Đoko Mazalić, painter
Petar Tiješić, painter
Ljubica Stefanović, theater actress
Stanko Karaman, biologist
Vladimir Gaćinović, writer and revolutionary
Danilo Ilić, Young Bosnia revolutionary
Feodor Lukač, surgeon and football pioneer
Jelena Kešeljević, theater actress
Gavrilo Princip, Young Bosnia revolutionary
Nedeljko Čabrinović, Young Bosnia revolutionary
Dobroslav Jevđević, politician and World War II Chetnik leader
Trifko Grabež, Young Bosnia revolutionary
Cvjetko Popović, Young Bosnia revolutionary
Vojislav Bogićević, Young Bosnia revolutionary and historian
Roman Petrović, painter
Vaso Čubrilović, Young Bosnia revolutionary
Jezdimir Dangić, Young Bosnia revolutionary, lawyer, Yugoslav Gendarmerie sub-lieutenant, and World War II Chetnik
Đuro Pucar, World War II Partisan leader and post-war communist politician
Rodoljub Čolaković, World War II Partisan leader and post-war communist politician
Aleksa "Brko" Bojović, World War II Partisan
Vlado Šegrt, World War II Partisan leader and post-war communist politician
Boriša "Šćepan" Kovačević, World War II Partisan
Dušan Pajić-Dašić, World War II Partisan leader
Erih Koš, lawyer and writer
Bishop Vladislav (Vojislav Mitrović), schoolteacher, theology professor, and Orthodox monk, Dabar-Bosnia Metropolitan bishop (1967-1992)
Vukašin Milošević, Mechanical and Energetics Engineer
Gliša Janković, World War II Partisan
Slobodan "Seljo" Princip, World War II Partisan leader
Miladin Radojević, World War II Partisan
Rato Dugonjić, World War II Partisan leader and post-war communist politician
Vaso "Crni" Miskin, World War II Partisan
Milan Rajlić, football player
Radojka Lakić, World War II Partisan
Branko "Obren" Milutinović, World War II Partisan
Vladimir "Valter" Perić, World War II Partisan leader
Rava Janković, World War II Partisan
Ljubo Kojo, World War II Partisan, mayor of Sarajevo 1955-1962
Lazo Materić, World War II Partisan, mayor of Sarajevo 1962-1963
Bane Šurbat, World War II Partisan leader
Dara Dragišić, World War II Partisan
Branko Stanković, footballer and football coach
Vaso Radić, World War II Partisan, mayor of Sarajevo 1963-1965
Dragutin "Braco" Kosovac, World War II Partisan, post-war communist politician, and state-appointed business manager (CEO of Energoinvest)
Aleksandar "Aca" Nikolić, basketball coach
Dane Maljković, electrical engineer, mayor of Sarajevo 1973-1975
Milorad Ekmečić, historian
Milanko Renovica, communist politician
Bishop Nikolaj (Gojko Mrđa), theologist and Orthodox monk, Dabar-Bosnia Metropolitan bishop (1992-present)
Nikola Milošević, political philosopher
Predrag Palavestra, writer and literary historian
Biljana Plavšić, biologist and politician, convicted for crimes against humanity
Toma Kuruzović, actor
Duško Trifunović, poet and writer
Obrad Piljak, banker and communist politician
Boro Drašković, playwright and film director
Predrag Golubović, film director
Dimitrije Bjelica, chess player
Nikola Koljević, scholar and politician
Momo Kapor, writer
Uglješa Uzelac, economist and sports administrator, mayor of Sarajevo 1983-1985
Svetozar Vujović, football player
Boriša Starović, surgeon, UofS medical faculty dean (1988-1992), UIS medical faculty dean (1993-2005), UIS rector (2000-2005)
Kornelije Kovač, musician and composer
Boško Antić, football player and football coach
Momčilo Krajišnik, politician, convicted for crimes against humanity
Rajko Nogo, poet
Blagoje Bratić, football player and football coach
Nenad Kecmanović, academician
Vladimir "Čobi" Savčić, singer
Braco Dimitrijević, artist
Svetislav Pešić, basketball player and basketball coach
Dragan Kalinić, politician
Mladen Savović, structural engineer
Milić Vukašinović, musician
Goran Bregović, musician
Neda Ukraden, singer
Božo Janković, football player
Zdravko Čolić, singer
Slobodan "Čobo" Janjuš, football goalkeeper
Mila Mulroney, First Lady of Canada 1984-1993
Želimir "Keli" Vidović, football player
Vojislav Šešelj, politician
Mladen Materić, dramaturgist and theater director
Emir Kusturica, film director
Ipe Ivandić, musician
Nikola Nikić, football player
Aleksandar Obradović, journalist
Ljiljana Smajlović, journalist, Politika editor-in-chief 2005-2008, 2013-present
Ratko Radovanović, basketball player and basketball administrator
Milomir Ninković, plastic, reconstructive, and hand surgeon, Klinikum Bogenhausen
Boris Tadić, President of Serbia 2004-2012
Dragan Đokanović, politician
Predrag Pašić, football player
Željko Lukajić, basketball coach
Vladimir Pištalo, writer, 2008 NIN Prize winner
Vlado Čapljić, football player
Dragan Škrba, football goalkeeper
Nele Karajlić, singer, actor, and TV personality
Nebojša Novaković, football player and football coach
Vesna Mišanović, chess player
Gorčin Stojanović, film and television director
Srđan Koljević, screenplay writer and film director
Isidora Bjelica, writer
Dragan "Maca" Marinković, actor and TV presenter
Predrag Danilović, basketball player, KK Partizan president 2007-present
Rade Bogdanović, football player
Vladimir Kecmanović, novelist
Ognjen Tadić, politician
Haris Brkić, basketball player
Goran Trobok, football player
Nenad Mišković, football player
Dušan Vukčević, basketball player
Predrag Materić, basketball player
Veselin Petrović, basketball player
Neven Pajkić, boxer
Ognjen Koroman, football player
Ognjen Aškrabić, basketball player
Đorđe Babalj, football player
Aleksandar Ćapin, basketball player
Aleksej Nešović, basketball player
Danina Jeftić, actress
Zoran Čegar


== See also ==
Sarajevo Old Orthodox Church
Serb Orthodox Cathedral (Sarajevo)
Serbs of Bosnia and Herzegovina


== References ==


== External links ==
Prosvjeta - Serb Cultural Society
Serb Orthodox Mitropolitanate of Dabro-Bosnia
Official site of the Old Orthodox Church