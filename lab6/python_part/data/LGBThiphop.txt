LGBT hip hop, also known as by names such as homo hop or queer hip hop, is a subgenre label applied by critics and music media to hip hop music performed by LGBT artists and performers. It has been described as "a global movement of gay hip-hop MCs and fans determined to stake their claim in a genre too often associated with homophobia and anti-gay lyrics." LGBT hip hop as a genre is not marked by a specific production style—artists within it may simultaneously be associated with virtually any other subgenre of hip hop, or may also make music that falls outside the genre entirely. Rather, it is defined by a direct engagement with LGBT culture in elements such as the lyrical themes or the artist's visual identity and presentation. Artists who have been labelled as part of the genre have, however, varied in their acceptance of the terminology. Some have supported the identification of a distinct phenomenon of "LGBT hip hop" as an important tool for promoting LGBT visibility in popular music, while others have criticized it for essentially ghettoizing their music as a limited "niche" interest. Furthermore, LGBT Hip Hop is represented in not just the musical and rapping parts of hip hop, but LGBT visibility is seen through graffiti and breakdancing. Similarly to graffiti, LGBT graffiti writers use this art form as self-expression and empowerment. However, these graffiti artists break the hegemonic hold of sexist gender roles and heterosexual promotion. Through their work, queer graffiti interrupts heterosexual public landscape.


== History ==
The genre first emerged in the 1990s as an underground movement, particularly in the American state of California, in part as a reaction to the widespread acceptance of homophobia in the lyrics of mainstream hip hop performers such as Eminem. Initially coined by Tim'm T. West of Deep Dickollective, the term "homo hop" was not meant to signify a distinct genre of music, but simply to serve as a community building tool and promotional hook for LGBT artists. According to West:
West's bandmate Juba Kalamka offered a similar assessment:
Deep Dickollective, along with many other LGBT hip hop artists of color, went beyond rapping about sexuality, but also about race, gender and class struggles. Because hip hop history is about self expression and shedding light onto those who are invisible, LGBT Hip Hop does the same to the different individuals within the LGBT community. It goes against purity mythologies =, which is the idea that hip hop is purely a heterosexual industry. Concepts of masculinity and femininity are redefined when an artist from the LGBT community expresses it through Hip Hop forms. In a 2001 interview with SFGate.com, West elaborated on the movement's goals:
The genre received a mainstream publicity boost in 2002 and 2003 when Caushun was widely reported as the first openly LGBT rapper to be signed to a major label, although Caushun was later revealed to have been a publicity stunt engineered by heterosexual musician Ivan Matias.
Notable events in the 2000s included the PeaceOUT World Homo Hop Festival, which was founded in 2001 and mounted annually until 2008, and the 2006 documentary film Pick Up the Mic. However, some music critics in this era dismissed the genre as too often sacrificing musical quality in favour of a "didactic" political agenda.
The most commercially successful LGBT rapper in the 2000s was Cazwell, who emerged as a popular artist in gay dance clubs, and has to date scored six top 40 hits on Billboard's Hot Dance Club Songs chart, with a hybrid pop-rap style which he has described as "if Biggie Smalls ate Donna Summer for breakfast". Cazwell described his philosophy of music as "create your own space, your own music and have people come to you," and has noted in interviews that he achieved much greater success by "breaking" the rules of the hip hop industry than he ever did in his earlier attempts to pursue mainstream success with the 1990s hip hop duo Morplay.


== Evolution ==

By the early 2010s, a new wave of openly LGBT hip hop musicians began to emerge, spurred in part by the increased visibility and social acceptance of LGBT people, the coming out of mainstream hip hop stars such as Azealia Banks and Frank Ocean, and the release of LGBT-positive songs by heterosexual artists such as Murs, Macklemore, and Ryan Lewis.
Although inspired and empowered by the homo hop movement, this newer generation of artists garnered more mainstream media coverage and were able to make greater use of social media tools to build their audience, and thus did not need to rely on the old homo hop model of community building. Many of these artists were also strongly influenced by the LGBT African American ball culture, an influence not widely seen in the first wave of homo hop, and many began as performance art projects and incorporated the use of drag. Accordingly, many of the newer artists were identified in media coverage with the newer "queer hip hop" label instead of "homo hop".
In March 2012, Carrie Battan of Pitchfork profiled Mykki Blanco, Le1f, Zebra Katz and House of LaDosha in an article titled "We Invented Swag: NYC Queer Rap" about "a group of NYC artists [who] are breaking down ideas of hip-hop identity".
In October 2012, Details profiled several LGBT hip hop artists "indelibly changing the face—and sound—of rap".
In March 2014 the online magazine Norient.com has published a first overview of queer hip hop videos worldwide. The article talks about topics, aesthetics and challenges of LGBT hip hop in Angola, Argentina, Cuba, Germany, Israel, Serbia, South Africa and USA."


== Criticism ==
Some artists, however, have criticized the genre as an arbitrary label which can potentially limit the artist's audience and may not actually correspond to their artistic goals or career aspirations. In 2013, Brooke Candy told The Guardian:
One unspecified artist declined to be interviewed for the Guardian feature at all, stating that he preferred to be known as a rapper rather than as a "gay rapper".
Other artists have been more circumspect about the dichotomy. In the same article, British rapper RoxXxan told the Guardian that "I want to be perceived as 'RoxXxan,' but if people label me as 'gay rapper RoxXxan' I'm not offended." Nicky Da B told Austinist that "Basically, I perform for a LGBT crowd but also for everyone. A lot of the bounce rappers that are rapping and touring at the moment are all gay. The LGBT community just capitalizes on that I guess, from us being gay, and they support us on it, so that's how it goes I guess."


== Notable artists ==


== See also ==
African-American culture and sexual orientation


== References ==