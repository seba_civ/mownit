The Real Thing is the third studio album by the American rock band Faith No More. It was first released through Slash Records on June 20, 1989. It was the first release by the band not to feature vocalist Chuck Mosley. Instead, the album featured Mike Patton from the experimental band Mr. Bungle. On this album, Faith No More advanced their sound range combining heavy metal, funk and rock.
On March 2, 2015 a deluxe edition containing previous unreleased tracks was released via Slash Records.


== Background ==
Faith No More underwent several line-up changes before releasing their first album, We Care a Lot, released in 1985 and distributed through San Francisco-based label Mordam Records. On the original vinyl release, the band is credited as "Faith. No More" on the album's liner notes, back cover, and on the record itself. Within a year the band signed up with Slash Records. The debut album's title track "We Care a Lot" was later rerecorded, for their follow-up album Introduce Yourself in 1987, and released as their first single. Membership remained stable until vocalist Chuck Mosley was replaced by Mike Patton in 1988.


== Production ==
The writing for the majority of the music for The Real Thing took place after the tour for Introduce Yourself. A demo version of "The Morning After", under the moniker "New Improved Song", with alternate lyrics written and sung by Chuck Mosley was released on the Sounds·Waves 2 extended play with the Sounds magazine. "Surprise! You're Dead!" was composed by Jim Martin in the 1970s, while he was guitarist for Agents of Misfortune who also featured Cliff Burton in the line up. The recording of the song took place in December 1988 following Chuck Mosley's removal from the band and was completed prior to the hiring of Mike Patton, who then wrote all the lyrics for the songs and recorded them the following month over the music.
The recording sessions also yielded several songs that did not appear on the album. Two of them, "The Grade" and "The Cowboy Song", later appeared on the singles and on the UK edition of Live at the Brixton Academy. A third song, "Sweet Emotion", was later rerecorded with different lyrics as "The Perfect Crime" for the soundtrack to the film that also starred a cameo appearance from guitarist Jim Martin, Bill & Ted's Bogus Journey. The original version was released on Flexible Fiend 3 with Kerrang! magazine issue 258 and, more recently The Very Best Definitive Ultimate Greatest Hits Collection, the greatest hits compilation released to coincide with the band's reunion tour.


== Touring and support ==


=== Tours ===

The tour in support of The Real Thing was the first Faith No More did with Mike Patton. The second show of the tour was filmed for the music video to "From out of Nowhere" in the I-Beam nightclub. During the show, Patton had a beer bottle smashed over his right hand causing lacerations to some tendons. He was able to use his hand again after it healed, but he has no feeling in it.


=== Singles ===

The first single to be released from the album was "From Out of Nowhere" on August 30, 1989 which failed to make the UK Singles Chart. It was re-released in April 2, 1990 and made number twenty-three on the UK Singles Chart. In between these releases was "Epic" on January 30, 1990 the music video for which received extensive airplay on MTV throughout the year, despite provoking anger from animal rights activists for a slow motion shot of a fish flopping out of water. "Falling to Pieces" then saw release on July 2, 1990 and made it to number 92 on the Billboard Hot 100 before the reissue of "Epic" which then provided the band's first number one hit single, on the ARIA Charts, and their only top ten single on the Billboard Hot 100, making it to number nine.
"Surprise! You're Dead!" had a music video produced for it, that was directed by bassist Billy Gould featuring footage shot in Chile during a South American tour in 1991, but never saw release as an official single and the video wasn't released until its appearance on Video Croissant. "Edge of the World" saw limited release as a two track promo single in Brazil on CD and 12" vinyl, with the album version as track one and the live at Brixton Academy version as the second track, in a yellow slipcase with basic black text.


== Critical reception ==
The Real Thing is one of Faith No More's most successful albums to date. It is now considered a classic metal album by fans and critics alike. Although released in mid-1989, The Real Thing didn't enter the Billboard 200 until February 1990, after the release of the second single from the album, "Epic". The album eventually peaked at number eleven on the chart in October 1990, following the reissue of "Epic" almost a year and half after the initial release of the album. It was eventually certified platinum in U.S. and Canada as well as being certified Silver in the United Kingdom.


== Legacy ==
"Epic" was ranked number thirty on VH1's 40 Greatest Metal Songs, and number sixty-seven on their 100 Greatest One-hit Wonders list.


=== Compilation appearances ===
Many of the songs from The Real Thing have appeared on Faith No More compilation releases. The opening three tracks have appeared on every video and compilation album released by the band, except for Epic and Other Hits, which lacks "From Out of Nowhere".


=== Soundtrack appearances ===
"From Out of Nowhere" has been used in the video games Madden NFL 2005 and NHL 2005. It was released as downloadable content for Rock Band 3.
"Epic" was featured on video games Rock Band, Burnout Paradise, SingStar and also appeared in a commercial for the console version of Street Fighter IV. It is also featured on the Saints Row: The Third soundtrack. The song also appeared as a downloadable track in Guitar Hero 5.
"Falling to Pieces" was featured in the film Black Hawk Down.
"Surprise! You're Dead!" was included on the soundtrack for the film Gremlins 2: The New Batch and was used in the trailer for Violent Shit 3. It is recycled for The King of Fighters '94 for the soundtrack titled "Jungle Bouncer" as a part of the Ikari Warriors/Brazil Team, which is later reused in The King of Fighters 2002.


=== Covers ===
"From Out of Nowhere" has been covered by Finnish band Apocalyptica on their second album, Inquisition Symphony, German power metal band Helloween covered the song on their cover album Metal Jukebox and the Danish metal band, Raunchy covered the song on their album Velvet Noise Extended. It has also been covered by the band Five Finger Death Punch, and is featured on the UK version of their album The Way of the Fist as well as the soundtrack for The Avengers.
"Epic" has been covered both in concerts and on the Kerrang! Higher Voltage CD, a compilation of artists covering other songs. Such artists include the Welsh rock band The Automatic; the CD was released 20 June 2007. The metalcore band Atreyu also covered the song on their album Lead Sails Paper Anchor, and the Swedish indie band Love Is All plans to cover it in their 2008 tour.
"Surprise! You're Dead!" has been covered by Jim Martin following his departure from the band, on his solo release Milk and Blood, and by Humans Being for the Tribute of the Year album. It was also covered by the death metal band Aborted on Slaughter and Apparatus and by American thrash metal band Revocation as a bonus track on their 2011 album Chaos of Forms.
"Zombie Eaters" was covered by Ill Niño with Chino Moreno for their extended play The Under Cover Sessions.
"Edge of the World" is covered twice on the Tribute of the Year album by both Hate Dept. and Combine Heathen.


== Awards ==
The Real Thing was nominated for a Grammy Award for Best Metal Performance category in 1989 and "Epic" was nominated for a Grammy Award for Best Hard Rock Performance in 1991.


== Accolades ==


== Track listing ==
All lyrics written by Mike Patton, except "The Real Thing" by Patton/Gould, "Surprise! You're Dead!" by Patton/Martin and "War Pigs" by Geezer Butler. 


== Personnel ==
Mike "Puffy" Bordin – drums
Roddy Bottum – keyboards
Bill Gould – bass
James Martin – guitars
Mike Patton – vocals


=== Production ===
Matt Wallace – producer, engineer
Jim "Watts" Vereecke – assistant engineer
Craig Doubet – assistant engineer
John Golden – mastering
Lendon Flanagon – photography
Jeff Price – artwork
Terry Robertson – CD design


== Chart performance ==


=== Album ===


==== Year end ====


=== Singles ===


==== Year end ====


== References ==

Bibliography
Chirazi, Steffan; Faith No More (1994). The Real Story. Castle Communications PLC. ISBN 1-898141-15-0.  


== External links ==
The Real Thing (Adobe Flash) at Radio3Net (streamed copy where licensed)