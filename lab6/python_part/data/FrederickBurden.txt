Sir Frederick Frank Arthur Burden (27 December 1905 – 6 July 1987) was a British Conservative politician.


== Early life ==
Burden was educated at the Sloane School, Chelsea and was British schools boxing champion 1921-22. He served with the Royal Air Force in World War II, first with a Polish unit then with Eastern Air Command, and later on the staff of Lord Louis Mountbatten at South East Asia Command, attaining the rank of Squadron Leader.
He became a company director, including of British Eagle International Airlines.


== Politics ==
Burden contested South Shields as a National Labour candidate in 1935, and as a Conservative stood in Finsbury in 1945 and Rotherhithe in a 1946 by-election.
He was Member of Parliament (MP) for Gillingham from 1950 to 1983. He was chairman of the Parliamentary Animal Welfare Group. By the time of his retirement at the age of 77, he was among one of the oldest sitting MPs. James Couchman was his successor.


== References ==
Times Guide to the House of Commons, 1935, 1950, 1966 & 1979
Leigh Rayment's Historical List of MPs 


== External links ==
Hansard 1803–2005: contributions in Parliament by Frederick Burden