Christopher "Chris" King (born 14 November 1980 in Birkenhead, Merseyside) is an English footballer playing for Conwy Borough. He is the son of former Tranmere and Bangor City player Alan King. He played as a defender for TNS after joining from Southport.
Whilst with TNS, he played in both legs of the Champions League ties with Liverpool and in Europe on six other occasions, scoring nine goals from a total of 128 (+14) Welsh Premier appearances with the Saints. He later signed for Accrington Stanley in July 2008. He made his Accrington debut on 9 August 2008 in the Football League Two clash with Aldershot Town which ended in a 1–0 loss. He left Stanley by mutual consent on 26 November 2009.
He then moved to Welsh side Colwyn Bay in March 2010.
King later went on to join Isthmian League side Harrow Borough for the end of the 2010–11 season having signed in November 2010.
In July 2012 he rejoined The New Saints although he quickly moved on, joining Conwy Borough in September.


== References ==


== External links ==
Chris King career statistics at Soccerbase
Welsh Premier profile