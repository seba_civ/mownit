MyEx.com is a controversial, free revenge porn website focusing on nude photographs of people posted by former lovers along with their real names.


== Concept ==
MyEx.com provides an opportunity for former lovers to upload pornographic photos of their ex-lovers acquired during the course of the relationship. Photos are generally posted with the full names of the person pictured nude and without that person's consent. Sexual comments about the subjects of the photos are solicited from visitors to the site. The site makes its money through what has been described by opponents of the site as a form of blackmail—requiring subjects wishing to remove their photos pay a fee for doing so.


== Controversy ==
Revenge porn in general is highly controversial and has been the subject of repeated attempts in different jurisdictions to criminalize it. A Missouri woman obtained a restraining order against her abuser, an ex-husband. The man then posted her nude photos on MyEx.com. She lobbied for a change in Missouri laws after she discovered there were no laws against posting sexually explicit nude photos, even with a restraining order in effect. A Seattle man was convicted and sentenced to one year in jail after posting topless and nude photos of his female clients on MyEx.com. He obtained the photos illegally through his work as a computer consultant. His specific crime appeared to be the stealing of the photos—as well as stalking the women—rather than the revenge porn per se. In addition, civil lawsuits have been filed in connection with specific alleged postings on MyEx.com. An ex-wife of former New York Giants player Jeff Roehl alleged that the former NFL star posted her nude photos on MyEx.com. Job loss has also resulted from postings on MyEx.com. MyEx.com, along with Google and Yahoo, has also been sued for copyright infringement based upon posted photos.


== Ownership ==
MyEx.com is owned by Websolutions Netherlands BV.


== References ==