Royal musicians are members of royal families who have shown talent in playing musical instruments, singing, or composing music, most often at a gifted amateur level, and on rare occasions having popular hits in their own countries, or giving public performances during most often charities at home, or royal visits abroad. At the same time, the royals who have been performers, have also been intimately interested in preserving and extending traditional court music and its musicians as patrons of the traditional music and arts of their own countries.
Perhaps the most famous musician in royal families has been Henry VIII of England cited by legend as composing the popular English folk song Greensleeves.
The following is a preliminary list of the more famous members of world royal families with their musical instruments or skills. Citations in the bibliography may include streaming audio of royal compositions.


== List ==

Denmark: Frederick IX of Denmark, conducting
Denmark: Henrik, Prince Consort of Denmark, piano
England: Elizabeth I, Harpsichordist
England: Mary I, Harpsichordist
Germany: Frederick II of Prussia, composer
Hawaii: Liliuokalani,composer
Hawaii: William Charles Lunalilo, lyric writer
Japan, Crown Prince Naruhito, viola
Korea, Prince Yi Seok, popular song
Monaco, Princess Stéphanie of Monaco, popular song
Thailand, King Bhumibol Adulyadej, clarinet/saxophone
UK: Charles, Prince of Wales, cello
UK: Queen Victoria, Organist
UK: Prince Albert, Organist


== References ==