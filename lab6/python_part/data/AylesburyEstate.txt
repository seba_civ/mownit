The Aylesbury Estate is a large housing estate located in Walworth, South East London.
The Aylesbury Estate contains 2,704 dwellings and was built between 1963 and 1977. There are approximately 7500 residents, spread over a number of different blocks and buildings. The estate is currently undergoing a major regeneration programme.
Major problems with the physical buildings on the estate and the poor perception of estates in Britain as a whole have led to the Aylesbury estate gaining the title of "one of the most notorious estates in the United Kingdom."
Tony Blair chose to make his first speech as Prime Minister here, in an effort to demonstrate that the government would care for the poorest within society. The estate is often used as a typical example of urban decay.
The Aylesbury Estate is an ethnically diverse area: according to the most recent census, around 25% of respondents were White British, with Black ethnic groups accounting for over half of all respondents. Around a third (34%) of residents are of school age, and fewer than 1 in 10 are aged 65 or above. Nearly two thirds of respondents identified themselves as being Christian, with 17% being Muslim.


== Construction and decline ==
The estate was designed by architect Hans Peter "Felix" Trenton and construction started in 1963. Built on 285,000 square metres the estate was an attempt by planners to house some of London's poorest families. The 2,700 dwellings were designed to house a population of roughly 10,000 residents, making it one of the largest public housing estates in Europe. The estate is named after Aylesbury in Buckinghamshire and the various sections of the estate are named after other local towns and villages in Buckinghamshire including Foxcote, Wendover, Winslow, Padbury, Taplow, Ravenstone, Latimer and Chiltern.
In the 1970s residents in the ground floor flats successfully campaigned for gardens to be fenced off adjoining their flats. The final blocks of flats were completed in 1977 and the estate included a nursery, a day centre and a health centre.

However, as old tenants moved out and new tenants came in, the estate went through a period of decline in the 1980s. The area is now considered to be in the bottom category on the ACORN classification for inner city adversity, signifying an area of extremely high social disadvantage. Crime and the fear of crime is a major concern for residents. However, in the major headline grabbing incidents that have taken place on the estate, the perpetrators were not residents of the Aylesbury estate, but had found the architecture of the area conducive to carrying out their crimes. Designing out crime in the new buildings, is certainly a high priority for the Council and Creation Trust and the new development partner.
In 1999 the estate was awarded New Deal for Communities status and given £56.2m of central government funding (over 10 years). It was expected that this money would bring in £400m of housing association funding into the estate as part of a stock transfer deal. A tenant ballot was held on transfer to a housing association which was rejected by 73% of the votes on a 73% tenant turnout.


== Recent history ==
On 27 September 2005, the London Borough of Southwark decided that rather than spend £350 million updating the estate to basic living standards they would order its demolition and replace the dwellings with modern houses controlled by a housing association. The plan involves increasing the density of housing from the current 2,700 units to 4,900. 2,288 units would remain social housing and the remainder would be for sale. The sale of these units is planned to fund the whole scheme.
In 2009 the nearby Heygate Estate served as the backdrop to a feature-length film called Harry Brown which starred Michael Caine. The film centres on the violence of a youth gang in a fictional British housing estate and the violent response exacted by a lonely pensioner. Many of the young men living at Aylesbury Estate had small parts in the film.
The regeneration of the Aylesbury Estate has been divided into several phases which will see the estate being re-built in 20 years. The indicative phasing plan states when tenants plan to be re-housed and when leasehold properties would be bought by Southwark Council however, this timetable is subject to a certain amount of flux, until the development partner is appointed and the more detailed scheduling of the work can begin, which will offer greater certainty to residents about when they will need to move. The first Phase 1a was completed in August 2013, it lies to the south west corner of the Aylesbury Estate and is divided into four development sites: A, C, B/E and D. This phase was developed with the L&Q housing association. It comprises 261 units, and a new resource centre for adults with disabilities. It is a mixture of affordable and private housing, with existing Aylesbury residents given priority to move into the new buildings.


== Occupation ==
January 2015 following the March for Homes a block on the estate was occupied by a group of squatters and housing activists in a protest against the demolition of the estate and the gentrification of London. Despite facing violence by the police and security guards the occupation continued, shifting from block to block as the protesters faced eviction notices. The occupation gained press attention for its insurrectionary tactics and for the level of aggression shown by Southwark Council who installed security guards, security dogs, and a spiked fence to deter to protesters but it continues.


== Channel 4 ident ==
The estate has featured in many television shows including The Bill, Spooks, and since 2004 has been featured in a Channel 4 ident, in which the camera "tracks down rubbish-strewn balconies [while] other balconies and floating concrete structures shift into place forming the shape of the Channel 4 logo." The washing lines, shopping trolley, rubbish bags and satellite dishes were, however, "all artificial embellishments added in by film-makers" to assist in what Ben Campkin, director of the UCL Urban Laboratory, called the estate's depiction as "a desolate concrete dystopia [which] provides visual confirmation of tabloid journalists' descriptions of a 'ghost town' estate."
In January 2014, Creation, a development trust for the estate, released a remake of the ident, produced and directed by Nick Street, and launched a campaign to persuade Channel 4 to use it instead of the original. Charlotte Benstead, a director at Creation, said:

For years it was the first port of call for directors, producers and location scouts looking for grim backdrops to murder scenes, gun and drug storylines and gang-related crimes in soaps and gritty dramas. Due to pressure from local residents, Southwark council banned filming on the estate, but the ident continues to be aired regularly. All these representations have perpetuated the reputation of the estate... We felt we needed to record an alternative and more truthful version of the clip. We worked with film-maker Nick Street to make our own version that looks beyond the concrete exterior.

In February 2014, Channel 4 told BBC London that it had "viewed the new film, liked it and has been in contact with the filmmaker about running it once. However the broadcaster said it would not be dropping the original ident."


== Transport ==


=== Buses ===
The estate is served by London Buses routes 42, 136 and 343. Routes 12, 35, 40, 45, 68, 148, 171, 176, 468 and P5 run nearby.


=== London Underground ===
The nearest station is Elephant & Castle on the Bakerloo and Northern lines.


== Notable residents ==
British rapper Tinie Tempah grew up on the Aylesbury Estate until the age of 12.


== See also ==
Ferrier Estate
Heygate Estate


== References ==


== External links ==
Aylesbury Area Action Plan - review of the main planning submission
Creation Trust - successor to the Aylesbury NDC, working to ensure that residents receive the economic and social benefits of the regeneration of the area.
Knit the Aylesbury Estate - community organisation
Heygate & Aylesbury Leaseholders Action Group - Action Group