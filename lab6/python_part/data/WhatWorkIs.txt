What Work Is is a collection of American poetry by Philip Levine. The collection has many themes that are representative of Levine's writing including physical labor, class identity, family relationships and personal loss. Its primary focus on work and the working class led to it being studied with emphasis on Marxist literary criticism. The focus on work is expressed in thematically different ways throughout the collection. Furthermore much of the collection was shaped by concerns for blue collar workers as well as nationwide political events. Critical acclaim for Levine has also led his poetry, including this collection, to undergo further analysis by critics and students.


== Background ==
"What Work Is" was first published in 1991 by Alfred A. Knopf, Inc. in New York. This poetry collection won the National Book Award for Poetry in 1991.
After Levine was named Poet Laureate, What Work Is graced the top of Amazon.com's "Movers and Shakers" list.
Because of Philip Levine growing up in an industrial Detroit much of his work focuses on that aspect of his life. This is perhaps what has led Edward Hirsch of the New York Times Book Review to label Levine as "a large, ironic Whitman of the industrial heartland". Levine was inspired by many events as he was growing up during the Great Depression. His heroes were not only those soldiers fighting against oppression in the Spanish Civil War but also the people around him who worked simply to stave off poverty. This shows in Levine's poetry as his subject matter is often the harshness of the life of the working class. Many poems in "What Work Is" seem to speak for the working class and show the reality of the labor that they endure. Levine has stated that he chose to represent the working class and be a voice for them.
When the collection What Work Is was first released, unemployment was the highest it had been in nearly seven years. The Persian Gulf War had started and ended quickly, yet the Iraqi invasion of Kuwait had caused the 1990 oil price shock which created a mild recession. The mild recession combined with Reaganomics caused many steel mills and factories were forced to close forming Rust Belt.


== Themes ==
Major themes that run throughout the collection mostly pertain to the American working class. Many of the characters in the poems are vivid images of American workers. Many of the settings of the poems are blue collar jobs. Plumbing and plating factories, brass factories, automobile manufacturing plants, as well as neighborhoods in Detroit all give the setting to the themes that play out. Yet the poetry finds its power when Levine reveals the inner lives of his characters. Many of the characters are not only workers, but artists, readers, and academics. Levine's poetry is accessible and moving and is utterly American.
The poems in "What Work Is" are more subtle in their thematic construction. Levine does not use caricatures in these poems but instead intimates real blue-collar workers. Many of the poems give a representation of a station of life not common in the subject matter of contemporary American poetry. Although not all of "What Work Is" is primarily about the working class. Instead the poems have a wide range and variety. However many of these poems, despite their subject matter, show elements that identify with the working class or work in general. This helps place this collection within the tradition of working class poetry. For instance, besides work, love also is a major theme for Levine in this collection. "What Work Is" is a poem primarily written for his brother, and the difficulty of expressing that love. Often enough the theme of love is expressed in a subdued way and is shadowing the bolder themes most largely associated with work. The poems feature different types of work, some of which reflect the actual work experience of the author. For instance, the poems "Fear and Fame" and "Growth" are direct accounts of two of Levine's jobs as a young man. "Fear and Fame" is based on a job Levine had cleaning out and refilling acid vats in his mid twenties. "Growth" is about the job Levine held at a soap factory at the age of fourteen. Many of Levine's poems are based on his experience growing up in Detroit. The titular poem, "What Work Is", is based on Levine's experience of waiting in a Detroit employment line. "The Right Cross" is related to a boxing teacher who taught Levine to defend himself while growing up in Detroit.


== Style ==
The cadence of many of the poems in the collection are simple free verse. The rhythm and dictation of much of the collection is normal and easily accessible. Though "What Work Is" takes on many sophisticated subjects such as death, love, loss, and struggle, the collection maintains an easy-to-access feeling. Levine's work in this collection is not to obscure or make grandiose, but instead to reveal and show plainly matters that are important. Levine's structures and word choices reflect this mood of his poetry as he rarely uses complicated punctuation or rhythms. Instead of using highly symbolic words or imagery "What Work Is" tends to contain direct images and a more direct and perceptible intention. This use of strong and stable syntax allows one to perceive the people and places in the collection without getting caught up in the construction of the poems. Levine's intention is more of a narrative one, for the reader to get caught up in the images of the poem rather than the words.


== Analysis ==
Much of the work done on Levine's Poetry is Marxist literary criticism. Because the subject matter and Levine's focus on work, Marxist Criticism seems to be the most fitting. Rumiano proposes a Marxist reading of much of Levine's work in a dissertation. Rumiano analyzes many of Levine's poems and work in a Marxist manner as it relates to the working class characters that appear in Levine's poetry. Most notable are the concerns with the power struggles of the working class, working conditions, and how a life as a worker relates to a politics of living.


=== "What Work Is" ===
According to Ruo, in this poem Levine defines and expresses the struggles between the working class and the upper middle class. Two different aspects of the poem support and define this struggle. Firstly the narrator's brother is at home recovering after a hard night of labor. This means that the brother can not find time to practice singing opera which is what he most wants to do. If he were a member of the upper class he would not have to expend himself at the cost of his education just to survive. It can be seen as one way the working-class upper-class struggle is defined. Secondly the narrator, who waits in line to see if he can get a job that day, is at the mercy of the upper-class manager who decides who can work or not. This represents the struggle between the two classes as well as the balance of power between the two.
Contemporary critics often label Levine a working class poet, describing his writing as working class verse. Levine’s poetry illustrates one of the most basic tenets of Marxist theory: that class antagonisms comprise all of human history.


=== "Growth" ===
This poem is about a job Levine had when he was a boy. The poem's narrator takes a retrospective look at his job at the soap factory. Rumiano states that the main issues and concerns in this poem are: the lack of communication, lack of a superior, and the hellish nature of the task that the narrator is performing and its effect upon him.


== Critical reception ==
Much of the collection contains a series of grim and brooding passages. Paul Gray, a contributor of "Time" called Levine's narrators “guerrillas, trapped in an endless battle long after the war is lost.” This may refer to instances where Levine recalls his childhood hometown of Detroit. Yet others, such as Marie Borroff, suggest that Levines work is joyful despite is painful material.
Initial reviews for What Work Is praised Levine for his working-class subject matter, which represented a marked change from contemporary poets who wrote more about the domestic sphere. Levine's 1991 collection made an important digression from a trend of "meditation about a seemingly inconsequential comer of one's personal life" to the earnestness of work: "unglamorous, bluecollar, industrial, assembly-line work". Richard Hugo stated in the American Poetry Reviewthat “Levine’s poems are important because in them we hear and we care.” Hugo shares a similar opinion as other critics of Levine's work; it is edgy, gritty, and brutal but also beautiful and hopeful.
Levine once referred to himself as “a dirty Detroit Jew with bad manners", and he has sometimes been criticized for leaning too hard on his blue-collar bona fides. The critic Adam Kirsch, writing in The Times Book Review in 1999, noted, accurately enough, that “in his autobiographical essays he goes out of his way to tell us that he is essentially a peasant.”
David Baker, writing about What Work Is (1991) in the Kenyon Review, said Levine has “one of our most resonant voices of social conviction and witness, and he speaks with a powerful clarity…What Work Is may be one of the most important books of poetry of our time. Poem after poem confronts the terribly damaged conditions of American labor, whose circumstance has perhaps never been more wrecked.”


== Contents ==


=== I ===
"Fear and Fame"
"Coming Close"
"Fire"
"Every Blessed Day"
"Growth"
"Innocence"
"Coming Home from the Post Office"
"Among Children"
"What Work Is"


=== II ===
"Snails"
"My Grave"
"Agnus Dei"
"Facts"
"Gin"
"Perennials"
"Above the World"
"M.Degas Teaches Art & Science at Durfee Intermediate School"


=== III ===
"Burned"


=== IV ===
"Soloing"
"Scouting"
"Coming of Age in Michigan"
"The Right Cross"
"The Sweetness of Bobby Hefka"
"On the River"
"The Seventh Summer"


== References ==


== External links ==
New Poet Laureate Philip Levine's 'Absolute Truth' on NPR, [August 14, 2011] (audio)
Nobody's Detroit in Harper's Magazine, [March 2010]
Poetry Foundation Recorded Poetry Readings (audio)