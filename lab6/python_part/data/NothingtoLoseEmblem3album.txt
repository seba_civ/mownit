Nothing to Lose is the debut studio album by American pop rock band Emblem3. It was released on July 30, 2013 through Syco Music, Columbia Records, and Mr. Kanani. It debuted at number seven on the US Billboard 200. It is the only album with the original line-up after Drew Chadwick announced his departure in June 2014.


== Background ==
Emblem3 found fame during the second season of The X Factor, and after placing fourth overall, the group was signed to Columbia Records and Simon Cowell's record label Syco Music. fun. band member Jack Antonoff helped write the song "Spaghetti."


== Promotion ==
The band promoted the album by opening for Selena Gomez during her Stars Dance Tour in 2013.


== Singles ==
The band's first single, "Chloe (You're the One I Want)", was released on April 15, 2013. It has since charted at number 25 on the US Pop Songs chart and number 93 at the Billboard Hot 100. The single has over 267,000 downloads as of August 1, 2013 
The second single from the album, "3000 miles", impacted Top 40 radio on October 15, 2013.


== Reception ==


=== Critical reception ===
Nothing to Lose has received mixed reviews from music critics. Chuck Campbell for Knoxville gave the album 2.5 out of 5 stars, calling Emblem 3 "an alternative to boy bands as well as a poor man’s version of a boy band as well as a straight-up boy band", while complimenting the tracks "Just for One Day" and "Girl Next Door". However, Campbell noted that at the album's worst, it "exposes the disservice that network talent shows are doing to the music industry". Campbell also noted that Emblem3 was "undercooked" and likely not ready to be famous. Campbell said the album largely contained filler tracks, and stated that the album was at times "mundane"; particularly the tracks "Teenage Kings", "I Love LA", and "Sunset Blvd", which he described as "aggressively bad".
Amy Sciarreto of PopCrush gave the album 3.5 out of 5 stars, comparing the band to Hot Chelle Rae and One Direction and praising the album's "golden melodies and saccharine harmonies". However, Sciarreto said the album was the equivalent "to a large cup of Pinkberry frozen yogurt", and noted that their "cheesy raps" often fell flat (comparing their rap verses to Karmin), and stated that Emblem3 was "about as edgy as a circle". Brent Faulkner of Starpulse described the album as "mediocre", praising the album's production however criticizing its songwriting and calling the album "inconsistent", however noted that Nothing to Lose showed potential. Faulkner described the album's tracks as "gimmicky" and "annoying", and noted the quality of the album plummeted during its second half, however complimented Emblem3 for making mostly "catchy" songs that would appeal to the youthful demographic that they were targeting, and stated that the album had decent songs but generally lacked consistency.


=== Commercial performance ===
The album debuted at #7 on the US Billboard 200, selling around 46,000 copies with its first week and the first X Factor USA act to have an album on the top 10. The album also debuted at #21 on New Zealand Albums Chart. As of August 22, 2013 the album has sold over 70,000 copies.


== Track listing ==
Notes
^[a] signifies an additional producer
^[b] signifies a co-producer
^[c] signifies a vocal producer


== Chart performance ==


== References ==