Four Oaks is a town in Johnston County, North Carolina, United States. The population was 1,921 at the 2010 census. Four Oaks is the home of the Acorn Festival.


== History ==
Four Oaks was named by Colonel R. R. Bridgers in 1886 for an unusual sight that graced the land upon which he found himself. In 1850, 37 years before there was a town of Four Oaks, Aaron Wallace went hunting. He "treed a possum" and as was the protocol of the day, he "felled the tree" to secure the possum.
Years later Kinchen Barbour purchased that land and built a home nestled in a beautiful grove of oaks. None of the oaks was more appreciated or admired than the one closest to the house. This oak was actually four huge trees which had grown from the stump left by Wallace the possum hunter. It was the same oak so strongly admired by Col. Bridgers that he named his town in its honor.
Col. Bridgers had come to the area as the President of Wilmington and Weldon Railroad. The site, the size, and even the name of the town were determined by the railroad. In completing the track from Benson, NC to Smithfield, NC the company set up a worksite on high ground between the two towns. The worksite brought an increase in activity to the area. Col. Bridgers purchased the right of way for the immediate site. He then added additional acres with a purchase from Isaac Evans Blackwell, a Tuscarora Indian man whose family had been free since the 1700s. His final plot was 40 acres. Thirteen blocks were to be the town. Seven blocks would be developed on the south side of the tracks, and six would be developed on the north. The railroad, and its accompanying depot, split the town down the center. Today, the business center of town remains housed in these 13 blocks.
The Four Oaks Commercial Historic District was listed on the National Register of Historic Places in 2006.


== Geography ==
Four Oaks is located at 35°26′50″N 78°25′34″W (35.447278, -78.426157).
According to the United States Census Bureau, the town has a total area of 1.1 square miles (2.8 km2), of which, 1.1 square miles (2.8 km2) of it is land and 0.93% is water.


== Demographics ==
As of the census of 2000, there were 1,424 people, 614 households, and 386 families residing in the town. The population density was 1,341.5 people per square mile (518.7/km²). There were 667 housing units at an average density of 628.4 per square mile (243.0/km²). The racial makeup of the town was 78.23% White, 16.22% African American, 0.63% Native American, 0.14% Asian, 3.72% from other races, and 1.05% from two or more races. Hispanic or Latino of any race were 7.65% of the population. The oldest living person in Four Oaks is 96 years old.
There were 614 households out of which 30.3% had children under the age of 18 living with them, 44.3% were married couples living together, 15.3% had a female householder with no husband present, and 37.1% were non-families. 32.6% of all households were made up of individuals and 14.8% had someone living alone who was 65 years of age or older. The average household size was 2.32 and the average family size was 2.91.
In the town the population was spread out with 24.6% under the age of 18, 8.4% from 18 to 24, 28.1% from 25 to 44, 23.7% from 45 to 64, and 15.2% who were 65 years of age or older. The median age was 37 years. For every 100 females there were 83.7 males. For every 100 females age 18 and over, there were 78.4 males.
The median income for a household in the town was $29,427, and the median income for a family was $41,875. Males had a median income of $31,875 versus $25,444 for females. The per capita income for the town was $17,473. About 12.8% of families and 17.5% of the population were below the poverty line, including 23.0% of those under age 18 and 21.1% of those age 65 or over.


== Education ==
Four Oaks Elementary School
Four Oaks Middle School
South Johnston High School


== References ==


== External links ==
Official website
Four Oaks Visitor Info