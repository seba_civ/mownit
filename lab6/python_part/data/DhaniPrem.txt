Dhani Ram Prem (1904–1979) was a naturalized British political activist, social worker and physician of Indian origin. He was the first councillor of Asian origin of Birmingham, representing Labour Party at Great Barr, in south Staffordshire. He was awarded the fourth highest Indian civilian award of Padma Shri by the Government of India in 1977, making him the first non resident Indian to receive the award.


== Biography ==
Dhani Prem was born in 1904 in Aligarh in the Indian state of Uttar Pradesh and lost his parents before he turned two. He was brought up by his uncle and did not get proper early education. He worked as an errand boy, lived in a library after running away from his uncle's house and taught himself from the library books. During this period, he became involved in Indian freedom struggle after getting influenced by Mahatma Gandhi and was imprisoned twice by the age of 14.
Prem graduated in medicine from the Topiwala National Medical College and Nair Hospital, Mumbai and secured his LRCP and MRCS degrees from the Royal College of Physicians of Edinburgh and Kings College Hospital in London respectively. Returning to India during the Great Depression of the 1930s, he failed to find a job as a medical practitioner and had to work as an editor of a literary magazine by name, Chand. During this period, he wrote stories and was reported to have been a successful writer. By this time, he got married and moved to London in 1938 with his wife and daughter for a one-year stay but had to stay back due to World War II. He found work as a general practitioner in Birmingham in 1939 and stayed at Gosta Green, Aston, England for 40 years till his return to India in 1978, about an year before his death.
Dhani Ram Prem died on 11 November 1979, at the age of 75, succumbing to the injuries sustained in a road accident.


== Political and social career ==
Prem spent three years in Edinburgh and the rest in Birmingham and worked as a general practitioner among the British working class. During this period, he published a booklet based on the Indian freedom movement, Indian National Congress which was released in 1945. He joined the Labour Party and became involved with the British Medical Association. In 1946, he contested the Birmingham council elections and was elected from Great Barr constituency representing Labour Party, the first person of Asian origin to become a councilor in Birmingham, a post he held through two elections till 1950. Apart from being a councilor, he was a member of the Health Committee and the Committee for Mental Hospitals. He was also involved with the issue of immigrants when the influx of immigrants began in the early 1950s. He founded the Indian Immigrant Council in 1953 to assist the immigrants to integrate themselves into British mainstream. When the Ugandan immigrants started to arrive in the 1960s, he established the Uganda Relief Trust and became its trustee. The immigrant issue was one of the main themes of his 1964 election contest.
In the 1955, Prem founded the Commonwealth Welfare Council composed of bus conductors in the wake of racial discrimination of two bus conductors by the trade unionists in West Bromwich and Birmingham. He also established an Indian Advisory Council for the UK for assisting the Indian immigrants to the UK. He dissociated himself from the Labour Party in 1968 in protest against the expulsion of Kenyans Asians and joined the Liberal Party, becoming a member of their National Council. He contested two elections in Coventry South as a Liberal Party candidate in 1970 and 1974 but both the attempts were unsuccessful. He also published a book in 1965, The Parliamentary Leper: a history of colour prejudice in Britain detailing the racial issues in Britain.
Prem was the president of the Asian Conference, a society of the Indian, Pakistani and Bangladeshi immigrants in the UK. He was associated with the Race Relations Council of Birmingham as a member and was the chairman of the Midlands India League and the Finance subcommittee of the Coleshill Group of Hospitals. He also set up a trust for the education of young women and a primary health centre in his home town of Aligarh. His efforts were reported behind the establishment of a local radio station and served as an advisor to the British Broadcasting Corporation for their Asian programmes.
Prem was also involved in a controversy when he advocated for the sterilization of coloured wives to protect the country from the proliferation of coloured people. He urged the local health authorities to implement the plan. Sterilization was a birth control method practised in India at that time but the suggestion invited widespread criticism in England.


== Awards and honours ==
It was reported that Prem was selected for the British honour of the Order of the British Empire which he declined because he considered the honour to be imperial in nature. The Government of India awarded him the civilian honour of Padma Shri in 1977, making him the first overseas Indian to receive the award. He received a doctoral degree (DSc honoris causa) from the Aston University in 1978.


== See also ==


== References ==


== Further reading ==
Dhani Ram Prem (1945). Indian National Congress. Indian Publications. ASIN B0007K4ETQ. 
D. R. Prem (1965). The Parliamentary Leper: a history of colour prejudice in Britain. Metric Publications. p. 177. 


== External links ==
"Interview with Dhani Prem". Midland News. 10 July 1968. Retrieved June 23, 2015.