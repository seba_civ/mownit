Michael Moutoussis (Greek: Μιχαήλ Μουτούσης, 1885 – 16 March 1956) was one of the first military aviators in the Greek Armed Forces. During the Balkan Wars (1912–1913) he performed together with Aristeidis Moraitinis the first naval air co-operation mission in history.


== Early career ==
Moutoussis, originally an Engineers officer, became one of the first six Greek officers in 1912 that were selected to receive aviation training in France, in order to man the newly established aviation branch of the Greek Army. In the following Balkan Wars (1912–1913) he initially carried out bombing missions on Turkish positions in the Macedonian front. At the beginning of December 1912, he was positioned in the Epirus front, where he performed various scouting and bombing operations in the region around Ioannina. At the end of the month he was ordered to move to the Aegean front of the war, where he took part in the Battle of Lemnos.


== Reconnaissance over the Dardanelles ==

On 5 February [O.S. 24 January] 1913 a few days after the Greek naval victory at Lemnos, First Lieutenant Moutoussis and Ensign Aristeidis Moraitinis were ordered to spot the position of the retreated Ottoman fleet in the Dardaneles with their seaplane, a converted Maurice Farman MF.7. When they reached the Nara naval base they noted down the Turkish ships and installations. Additionally, before they left the area they dropped four bombs, but without inflicting any serious damage or casualties. During the return flight an engine failure forced them down in the Aegean Sea and they were finally collected by the crew of the nearby Greek destroyer Velos. This operation is regarded as the first naval-air operation in military history and was widely commented upon in the press, both Greek and international.
Moutoussis also participated in the Macedonian front of World War I as well as in the Greco-Turkish War (1919-1922), where he was positioned in Proussa Air Field (in modern Bursa).


== References ==