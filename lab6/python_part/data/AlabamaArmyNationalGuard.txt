The Alabama Army National Guard is a component of the United States Army and the United States National Guard. National coordination of various state National Guard units are maintained through the National Guard Bureau.
Alabama Army National Guard units are trained and equipped as part of the United States Army. The same ranks and insignia are used and National Guardsmen are eligible to receive all United States military awards. The Alabama Guard also bestows a number of state awards for local services rendered in or to the state of Alabama.


== Units ==
167th Theater Sustainment Command
440th Theater Opening Element
279th Army Field Support Brigade
111th Ordnance Group (EOD)
441st Ordnance Battalion (EOD)
641st Ordnance Co (EOD)
666th Ordnance Co (EOD)

1307th Engineer Detachment (EHCC)

135th Sustainment Command (Expeditionary)

122d Troop Command
1103d Combat Support Sustainment Battalion
161st Medical Battalion (Multifunctional)

200th Regiment Training Institute (Schools, Studies and Academics): 1st BN - Officer Candidate School; 2d BN - Military Police; 3d BN - Engineers, BNOC
62d Troop Command
1st Battalion, 167th Infantry Regiment (attached)
226th Maneuver Enhancement Brigade
31st Signal Company
711th Brigade Support Battalion
877th Engineer Battalion
1st Battalion, 117th Field Artillery Regiment (EAB FA 155 Towed)
1st Battalion, 131st Aviation Regiment

142nd Battlefield Surveillance Brigade (activated in September 2009)HHC, Decatur, AL
321st Military Intelligence Battalion, Austin, TX (USAR)
1st Squadron (R&S), 131st Cavalry Regiment, Enterprise, AL
Troop A
Troop B
Troop C (LRS)

31st Brigade Support Company, Ozark, AL
67th Network Support Company, Billings, MT (MT ARNG)

31st Chemical Brigade
145th Chemical Battalion
690th Chemical Company (Recon/Decon)
440th Chemical Company (Smoke/Decon)

151st Chemical Battalion
1343rd Chemical Company (Smoke/Decon)
208th Chemical Company (Recon/Decon)

231st Military Police Battalion
214th Military Police Company (Combat Support)
217th Military Police Company (Combat Support)
1165th Military Police Company (Combat Support)

203d Military Police Battalion
128th Military Police Company (Combat Support)
152d Military Police Company (Combat Support)
1166th Military Police Company (Combat Support)

20th Special Forces Group


== Duties ==
National Guard units can be mobilized at any time by presidential order to supplement regular armed forces, and upon declaration of a state of emergency by the governor of the state in which they serve. Unlike Army Reserve members, National Guard members cannot be mobilized individually (except through voluntary transfers and Temporary DutY Assignments TDY), but only as part of their respective units.


=== Active Duty Callups ===
For much of the final decades of the twentieth century, National Guard personnel typically served "One weekend a month, two weeks a year", with a small portion of each unit working for the Guard in a full-time capacity. New forces formation plans of the US Army were announced in early 2007 modifying the recent (2001–2006) United States National Guard active duty callup pace. The new plan will nominally anticipate that each National Guard unit (or National Guardsman) will serve one year of active duty for every five years of service. Secretary of Defense Robert M. Gates imposed "a one-year limit to the length of [Federal deployments] for National Guard Soldiers." Callups by Alabama authorities for state emergencies are not included in this policy.


== History ==
The Alabama Army National Guard was originally formed in 1807. The Militia Act of 1903 organized the various state militias into the present National Guard system.
On the morning of 21 July 1861, the Union Army under the command of Brig. Gen. Irvin McDowell, in an effort to cripple the newly assembled Confederate Army at Manassas, Virginia, fired the opening shots of the first major battle of the Civil War. Both armies were largely made up of volunteer militia with regiments of both sides wearing blue and gray uniforms. The brunt of the Union attack fell on the Confederate left flank. Confederate Brig. Gen. Bernard Bee (Barnard Elliott Bee, Jr.?), having recently resigned from the U. S. Army and still wearing his blue uniform, realized that the army's left flank was seriously exposed. Gen. Bee ordered the Fourth Alabama Regiment (of The Alabama Brigade) to advance rapidly in order to plug the gap in the Confederate line. For over an hour, the Fourth Alabama held position and repulsed several Union regiments. The gallant stand of the Fourth Alabama stalled the Union advance and gave the Confederate forces more time to regroup. The regiment played a prominent part in the fighting all day and contributed to the Confederate victory. The Battle of First Manassas proved to both sides that the Civil War would be a bitterly contested struggle. The Fourth Alabama went on to fight in every major battle in the Eastern Theater of the Civil War and never surrendered its colors. The heritage and traditions of the Fourth Alabama are carried on by the 1st Battalion, 167th Infantry, Alabama Army National Guard.
The 31st Infantry Division ('Dixie') had elements in Alabama for many years, though divisional HQ was in Jackson, MS. It was deactivated in 1968.
Following the inactivation, the Alabama Army National Guard was allotted the 31st Brigade, 30th 'Volunteer' Armored Division, located in Tuscaloosa. In 1973 or 1974 the 30th Armored Division was inactivated and Alabama was assigned a new major headquarters, the 31st Armored Brigade (Separate), with its headquarters at Northport. In 2002, the 31st Armored Brigade was inactivated, merging into the 149th Armored Brigade headquartered in Kentucky. Alabama was again assigned a new major headquarters, the 122nd Chemical Brigade, later redesignated as the 31st Chemical Brigade.
Approx. 300 Alabama ARNG soldiers deployed to Iraq with the Combat Aviation Brigade, 36th Infantry Division ('Task Force Mustang'), in September 2006.
The previously active 142d Signal Brigade was inactivated in August 2008.
On 13 February 2009, the comedian Sacha Baron Cohen tricked guard officers into allowing him to participate in training at the Alabama Military Academy at Fort McClellan. The officers were led to believe that Cohen was a reporter making a German TV documentary. The ruse ended when a young Alabama cadet recognized the actor. Said Guard spokesperson Staff Sergeant Katrina Timmons on 16 March 2009 about the incident, "It's an embarrassment to the Alabama National Guard. Since then we have put in protocols to make sure this doesn't happen again."


== Historic units ==
 167th Infantry Regiment (United States)
 131st Cavalry Regiment (United States)
 200th Infantry Regiment (United States)
 117th Field Artillery Regiment (United States)
203rd Field Artillery Battalion (United States)


== See also ==

Alabama State Defense Force
Transformation of the Army National Guard


== References ==


== External links ==
Alabama National Guard, accessed 20 Nov 2006
GlobalSecurity.org Alabama Army National Guard, accessed 20 Nov 2006
Alabama National Guard article, Encyclopedia of Alabama
Bibliography of Alabama Army National Guard History compiled by the United States Army Center of Military History