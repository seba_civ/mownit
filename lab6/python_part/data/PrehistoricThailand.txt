Prehistoric Thailand may be traced back as far as 1,000,000 years ago from the fossils and stone tools found in northern and western Thailand. At an archaeological site in Lampang, northern Thailand Homo erectus fossils, Lampang Man, dating back 1,000,000 – 500,000 years, have been discovered. Stone tools have been widely found in Kanchanaburi, Ubon Ratchathani, Nakhon Si Thammarat, and Lopburi. Prehistoric cave paintings have also been found in these regions, dating back 10,000 years.


== 2,500,000 - 10,000 years ago: Palaeolithic ==


=== Early Stone Age ===
The Lower Palaeolithic is the earliest subdivision of the Palaeolithic or Old Stone Age. It spans the time from around 2.5 million years ago, when the first craft and use of stone tools by hominids appears in the archaeological record, until around 120,000 years ago when important evolutionary and technological changes ushered in the Middle Palaeolithic.


=== Early species ===
The earliest hominids, known as Homo erectus and recognisable as human, appear in the archaeological record between 1,000,000-500,000 years ago. Locally typified by the fossil, Lampang Man.
About 1,000,000 years ago, Homo erectus moved to Asia from Africa, where it had originated. Its use and control of fire was an important tool in its hunter-gatherer means of subsistence. Homo erectus's skull was smaller and thicker than that of modern human beings. It lived in the mouth of caves near streams or other water supplies. Its main natural enemies included the giant hyena Hyaena senesis, the sabre-toothed tiger, the orang-utan, and the giant panda.
In 1999, skull fragments of Homo erectus were found by Somsak Pramankit in Ko Kha, Lampang. It was comparable to the skull fossils of Sangiran II Man found in Java, (Java man), which is 400,000 - 800,000 years old, as well as Peking Man. Stone artefacts dating to 40,000 years ago have been found at Tham Lod Rockshelter in Mae Hong Son.


=== Relation to modern Thai people ===
Modern Thais are not descendants of Lampang Man. Genetic research supports this assertion. Geneticists have proved that there was no inter-breeding between modern human immigrants to Southeast Asia and Homo erectus, affirming that the Thai descended from Africans in accordance with the Recent single-origin hypothesis.


== 10,000 - 4,000 years ago: Neolithic ==


=== New Stone Age ===
The Neolithic or "New" Stone Age was a period in the development of human technology that is traditionally the last part of the Stone Age. The Neolithic era follows the terminal Holocene Epipalaeolithic periods, beginning with the rise of farming, which produced the "Neolithic Revolution" and ending when metal tools became widespread in the Copper Age (chalcolithic) or Bronze Age or developing directly into the Iron Age, depending on geographical region.
Recent archaeological excavations suggests that domesticated rice was introduced to central Thailand by immigrating rice farming societies about 4000 B.P.


=== Domestication ===
Neolithic culture appeared in many parts of Thailand, Mae Hong Son, Kanchanaburi, Nakhon Ratchasima, Ubon Ratchathani about 9000 BCE. People pioneered wild cereal use, which then evolved into true farming. For peninsular Thailand evidence of rice agriculture exists from 2500 - 2200 B.P. However, the possibility of an early presence of rice agriculture in southern-peninsular Thailand has recently been discussed by scholars 
Early Neolithic farming was limited to a narrow range of crops, both wild and domesticated, which included betel, bean, pea, nut, pepper, cucumber and domesticated cattle and pigs. The establishment of permanently or seasonally inhabited settlements, and the use of pottery.
In Southeast Asia, the independent domestication events led to their own regionally-distinctive Neolithic cultures which arose completely independent of those in other parts of the world.


=== Neolithic settlements in Thailand ===

Spirit Cave
Spirit Cave (Thai: ถ้ำผีแมน) is an archaeological site in Pang Mapha District, Mae Hong Son Province, northwestern Thailand. It was occupied from 9000 to 5500 BCE by Hoabinhian hunter-gatherers from North Vietnam. The site is at an elevation of 650 m. above sea level on a hillside overlooking the Salween River.
Lang Kamnan Cave
Lang Kamnan Cave is an archaeological site in Muang District, Kanchanaburi Province, and is on a limestone upland, facing northeast and 110 m above sea level. The cave is about 4 km from the Khwae Noi River. By analysing the faunal remains in the cave, the cave is believed to be one of the many temporary camps of the seasonally mobile hunter-gatherers. It was occupied from Late Pleistocene to Early Holocene.
Wang Bhodi
Wang Bhodi (Thai: วังโพธิ) is an archaeological site in Sai Yok District, Kanchanaburi Province, western Thailand. Dating from 4500 to 3000 BCE. Since World War II, many stone tools have been found in the caves and along the rivers in this region.
Ban Chiang
Ban Chiang (Thai: บ้านเชียง) is an archaeological site in Nong Han District, Udon Thani Province. Dating of the artefacts using the thermoluminescence technique resulted in 4420-3400 BCE dates. The oldest graves found contain no bronze and are therefore from a Neolithic culture, the most recent ones are from the Iron Age.
Khok Phanom Di
Khok Phanom Di is in southeast Thailand near the flood plain of the Bang Pakong River in Chonburi Province. This site was populated from 2000-1500 BCE. Seven mortuary phases were identified in the excavation, including 154 graves, yielding abundant archaeological remains, such as fish, crab, hearths, post holes, and the burials of adults and infants. By analysing the change in mortuary practices and the isotopes of strontium, carbon, and oxygen found within dental remains, archaeologists have examined the possibility of integration between an inland agriculture with the coastal hunter-gathers of Khok Phanom Di. The isotopic studies showed that in earlier phases, the female inhabitants in the site were immigrants from both inland and coastal areas, while males were raised locally. The localisation of the immigrants occurred during phase 4 and later, with rice growing increased in the same period.
Khao Rakian
Khao Rakian is in Rhattaphum District, Songkhla Province, peninsular Thailand. The cave was excavated in 1986 by the Fine Arts Department and revealed Neolithic ceramics, stone tools, and human skeletal remains.


== 2,500 years ago: Bronze Age ==


=== Copper and Bronze Age ===
The Bronze Age was a period in civilisation's development when the most advanced metalworking consisted of techniques for smelting copper and tin from naturally occurring outcroppings of ore, and then alloying those metals to cast bronze. There are claims of an earlier appearance of tin bronze in Thailand in the 5th millennium BCE.


=== Bronze Age settlements in Thailand ===
Ban Chiang
In Ban Chiang, bronze artefacts have been discovered dating to 2100 BCE. The earliest grave was about 2100 BCE, the most recent about 200 CE. The evidence of crucibles and bronze fragments have been found in this area. The bronze objects include ornaments, spearheads, axes and adzes, hooks, blades, and little bells.


== 1,700 years ago: Iron Age ==
The Iron Age was the stage in the development of people in which tools and weapons whose main ingredient was iron were prominent. People made tools from bronze before they figured out how to make them from iron because iron's melting point is higher than that of bronze or its components. The adoption of this material coincided with other changes in some past societies, often including differing agricultural practices, religious beliefs and artistic styles, although this was not always the case.Archaeological sites in Thailand, such as None Nok Tha, Lopburi Artillery centre, Ong Ba Cave and Ban Don Ta Phet show iron implements in the period between 3,400-1,700 years ago


=== The Iron Age settlements in Thailand ===
None Nok Tha
None Nok Tha (Thai: โนนนกทา) is an archaeological site in Phu Wiang District, Khon Kaen Province, northeastern Thailand, dating from 1420 to 50 BCE.
Lopburi Artillery centre
Lopburi Artillery centre (Thai: ศูนย์กลางทหารปืนใหญ่) is an archaeological site in Mueang District, Lopburi Province, northeastern Thailand, dating from 1225 to 700 BCE.
Ong Ba Cave
Ong Ba Cave (Thai: องบะ) is an archaeological site in Sri Sawat District, Kanchanaburi Province, western Thailand, dating from 310 to 150 BCE.
Ban Don Ta Phet
Ban Don Ta Phet (Thai: บ้านดอนตาเพชร) is an archaeological site in Phanom Thuan District, Kanchanaburi Province, western Thailand, dating from 24 BCE to 276 CE. Many artefacts found in a 4th-century cemetery provide evidence of trade relations with India, Vietnam, and the Philippines. Such artefacts include flat hexagonal shaped carnelians, small stone figurines of lions and tigers, and various metallic vessels.


== See also ==
Early history of Cambodia
Initial states of Thailand
Migration period of ancient Burma
Peopling of Thailand
Prehistoric Asia


== References ==

9.Halcrow, S., Tayles, N., Inglis, R., Higham, C., Carver, M. (2012). Newborn twins from prehistoric mainland Southeast Asia: birth, death and personhood. Antiquity, 86 (333).


== External links ==
This Ancient Land of Dinosaurs, Siamoid, Siamese, and Thais; English and Thai