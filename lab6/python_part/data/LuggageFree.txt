Luggage Free is a shipping company specializing in worldwide baggage shipping.


== History ==
Founded in 2001 and launched in 2003, Luggage Free was created in response to new federal regulations requiring all bags to be thoroughly inspected at airports and mounting customer dissatisfaction with commercial airline baggage handling. The number of bags the company ships doubled each year from 2004 to 2007, with shipments reaching around 40,000 in 2007.
As the global economy sputtered in 2008, with fewer people traveling and relying more heavily on carry-on luggage, Luggage Free’s growth slowed. With the economy, traveler numbers and baggage mishandling rates on the rise in 2011, luggage shipment growth is again approaching pre-2008 levels.


== Shipping ==
Luggage Free uses a network of agents to retrieve luggage from a client’s location at a specific time and then wraps the luggage in plastic protective packaging. Luggage Free prices by the weight of the shipment.


== See also ==
Lost luggage


== References ==


== External links ==
luggagefree.com