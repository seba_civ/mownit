Wolfgang Hugo Rheinhold (26 March 1853 – 2 October 1900) was a German sculptor arguably most famous for his Affe mit Schädel (Ape with Skull). His name is often misspelled Reinhold.


== Life ==
Hugo Rheinhold was born in Oberlahnstein, Prussia, on March 26, 1853. He schooled in Koblenz before entering the merchant trade. When 21, Rheinhold sought success in the USA and became an import and export merchant, living in San Francisco (1874–78) where he had headquarters for his business as well as in Hamburg, where he settled in 1879. The next year he married his childhood sweetheart Emma Levy from Cologne, but tragically, she was to die in 1882, after only one-and-a-half years of marriage. Emma's death had a huge impact Rheinhold. He sold his successful business and moved to Berlin to study science and philosophy at the Friedrich-Wilhelms University. In 1886, he studied under the sculptors Ernst Herter and Max Kruse, before officially enrolling as a student at the Berlin Academy of Arts (1888–92). He died, aged 47, in Berlin.


== Works ==
Rheinhold produced a series of notable works within a very short career span. These include, a group of reading monks ("Lesende Monche"), a tribute to Alfred Nobel (“Dynamite in the Service of Mankind”), a bust of socialist leader August Bebel and his most famous piece “Am Wege” (1896) a marble of “an unfortunate young woman with a child at her breast”. Rheinhold was protective of his Jewish heritage being a strong influence in the Deutsch-Israelitischer Gemeindebund (an association of Jewish corporations) and sculpting "Die Kämpfer" (“The Warriors”) in protest against burgeoning anti-Semitism. Rheinhold’s last work of serpentine deities in a fountain (“Brunnengrotte mit zwei Wassergottheiten”) was exhibited close to his death in 1900.”. Today, he is probably most revered for his iconic Affe mit Schädel.


== Notes ==


== References ==
Morgan, R., & Moore, A. Hugo Rheinhold's monkey. Boston Medical Library. (Website). 1. Dec. 1998. Retrieved on 1. Dec. 2008.
Richter, J., & Schmetzke, A. (2007). Hugo Rheinhold's philosophizing monkey—a modern Owl of the Minerva. NTM—International Journal of History and Ethics of Natural Sciences, Technology and Medicine, 15(2), 81-97.
Schmetzke, A. Other Sculptures by Hugo Rheinhold. Hugo Rheinhold ... and his Philosophizing Monkey. (Website). Library, University of Wisconsin–Stevens Point. 8. Dec 1999, last revised 6. Nov. 2008. Retrieved on 1. Dec. 2008.
Singer, I., & Mannheimer, S.(1901–1906). Rheinhold, Hugo. In The Jewish Encyclopedia (pp. 399–400). New York: Funk & Wagnalls.


== External links ==
Hugo Rheinhold ... and his Philosophizing Monkey
Varieties of Rheinhold's Philosophizing Darwin Monkey
Der Philosophische Affe und die Eule der Minerva
Hugo's Philosophical Ape