The Menkemaborg (Dutch pronunciation: [ˈmɛnkəmaːˌbɔrɣ]; Menkema Borough) is a borg (castle) in the village of Uithuizen in Groningen in the Netherlands. Since 1927, the borg is a historic house museum.


== Mansion ==
Menkemaborg is an originally 14th-century, brick-built house, which was dramatically altered around 1700 but has since been barely changed. The Alberda family, the 18th-century occupants, commissioned artists to decorate the interior with impressive chimney-pieces carved with baroque ornaments, and paintings of mythological scenes.
A four-poster bed, draped with yellow silk damask from China, has also been preserved.
The rooms, which comprise reception rooms, the gentlemen's room, a study, dining room, bedroom, kitchen and cellars, are fully furnished with furniture, silverware, china, brassware and portrait paintings from the 17th and 18th centuries.


== Gardens ==
The gardens were reconstructed after the surviving garden design dating from around 1705, and are marked by a clear layout with symmetrical patterns of clipped box enclosing beds of 18th-century flowering and ornamental plants. A particularly pretty part is the walled pleasure garden in front of the pavilion, with its trellissed arches and arbours. The natural "sundial garden" is special too. To the east of the mansion is the utility area with a kitchen garden where vegetables and potherbs are grown and an orchard with ancient strains of apple trees and a pear-tree pergola. At the heart of the maze you will eventually find an old plane tree. In summer, the flowering rose tunnel is not to be missed!
The complex of the mansion and gardens, surrounded by their tree-shaded moat, offers a vivid impression of how the Groningen aristocracy lived in the 17th and 18th centuries.


== Groninger Museum ==
The Menkemaborg was owned privately until 1902. The last heirs donated it in 1921 to the Groninger Museum, which restored it in 1927 and opened it to the public. In 1969 the "Stichting Museum Menkemaborg" was founded that manages the museum and grounds. The largest part of the collection in the Menkemaborg belongs to the Groninger Museum. Most of these came from deconstructed "borgs", because the province of Groningen once had 200 borgs, and only 16 survive.


== References ==

Helsdingen, H.W. van, Gids voor de Nederlandse kastelen en buitenplaatsen, Amsterdam 1966
Kalkwiek, K.A., A.I.J.M. Schellart, H.P.H. Jansen & P.W. Geudeke, Atlas van de Nederlandse kastelen, Alphen aan den Rijn 1980 (ISBN 90 218 2477 9)
Kransber, D. & H. Mils, Kastelengids van Nederland, middeleeuwen, Bussem 1979 (ISBN 90 228 3856 0)
Tromp, H.M.J., Kijk op kastelen Amsterdam 1979 (ISBN 90 10 02446 6)


== External links ==
Menkemaborg, official website