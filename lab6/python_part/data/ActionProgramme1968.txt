The Action Programme is a political plan, devised by Alexander Dubček and his associates in the Communist Party of Czechoslovakia (KSČ), that was published on April 5, 1968. The program suggested that the Czechoslovak Socialist Republic (ČSSR) find its own path towards mature socialism rather than follow the Soviet Union. AP called for the acknowledgment of individual liberties, the introduction of political and economic reforms, and a change in the structure of the nation. In many ways, the document was the basis for Prague Spring and prompted the ensuing Warsaw Pact invasion of the ČSSR in August 1968.


== Individual liberties ==
With regards to individual liberties, the programme promised complete freedom of speech, movement (which included the right to travel to western countries), debate and association as well as an end to arbitrary arrests. Security organizations would be held accountable to parliament and the courts were to have a larger, more prominent role, although they would not be completely independent.


== Economic changes ==
Economically, AP suggested that the government be confined to general economic policy and to protect the consumers' rather than the producers' interests. Therefore, there was a much greater freedom for the industrial enterprises in finding markets. Also, there was an effort to have an equality in economic relations between the Soviet Union and the ČSSR and the withdrawal of the remaining Soviet economic advisors.


== Federal structure ==
With respect to Slovakia, the programme admitted that the existing asymmetrical system was unfair, and proposed federalisation between Slovakia and the Czech lands. The Slovak national council and the Slovak council of ministries would serve as the executive authority in Bratislava. To enjoy the full benefit from the federalization; however, Slovakia would have to drastically catch up economically to the industrialized Czech lands.


== The End of Democratic Centralism ==
In this time, the party organization was under a considerable amount of pressure for immediate change. The programme admitted to these ideas but insisted that decisions were binding, in that democratic centralism was to be restructured and lower party organizations should have more say and influence on decision making, but party discipline was to remain.


== Foreign relations ==
In foreign relations, the programme called for recognition of Israel and the enforcement of a cut in arms deliveries to Egypt and Nigeria.


== External links ==
(Czech) Akční program KSČ (full text)


== References ==