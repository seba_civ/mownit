Sheila T. Stroup (born 1943, Covington, Louisiana) is a "Living" section columnist for the New Orleans Times-Picayune. Her column, rather than dealing with the celebrated news events of the day, invariably has to do with perceptions of the otherwise-unnoticed aspects of life, particularly in her native Northshore Region. Part of her style is to refer to her husband Merwin by his surname alone, Stroup.
A frequent topic for Sheila Stroup is dogs. In 1999 her dog Harry was elected Grand Duke in the Canine Court of the Mystic Krewe of Mardi Paws.
Stroup was a featured speaker on the 1996 Business Week program at Southeastern Louisiana University, of which she is alumna. For her fundraising efforts to benefit charitable and not-for-profit organizations, in 2004 Stroup received the Will Rogers Humanitarian Award from the National Society of Newspaper Columnists.
In 2009 Stroup was chosen by fellow columnist Chris Rose to serve as the addressee for a series of fictitious e-mails about yet another columnist, Angus Lind, as Rose parodied New Orleans City Councilwoman Stacy Head's e-mails about fellow Councilwoman Jacquelyn Brechtel Clarkson and other subjects.
Stroup and her husband, who live near the village of Folsom (which is on LA 25 due north of Covington), have two grown daughters.


== See also ==

Stacy Head
Angus Lind
Chris Rose


== Notes ==