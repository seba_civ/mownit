The Sonderkommando Blaich (English: Special detail Blaich) was a German special unit consisting of one Heinkel He 111 medium bomber that raided Free French–controlled Fort Lamy in the Chad region of French Equatorial Africa.
The raid against a target located 1,250 mi (2,010 km) from the German bases in North Africa was a success but, on its return flight, the German plane ran out of fuel and had to make an emergency landing; both crew and plane were rescued a week later.


== Background ==
The Chad and Fort Lamy came under control of the Free French Forces in 1940 and was a major staging post for the operations against the Kufra oasis group as well as a supply point for the RAF on the route from Takoradi in Ghana to Egypt.
Theo Blaich—a German adventurer and plantation owner who had joined the Wehrmacht in 1939, arriving in his own Messerschmitt Bf 108 Taifun—recognised the importance of Fort Lamy as a way point in the overland transport and communication route from the west coast of Africa to the Nile, as well as an assembly point for Allied operations. Blaich proposed the capture of Fort Lamy to safeguard the southern border of Libya. When his suggestions weren't taken seriously in Berlin, he proposed that he should at least carry out a bombing mission.
Blaich found a more interested audience in Erwin Rommel, who approved the idea and forwarded it to the Fliegerführer Afrika. The date for the operation was set as 21 January 1942, to coincide with Rommel′s offensive against the British defences at El Agheila.
Blaich′s commando left the oasis of Hun on 20 January, and consisted of German and Italian soldiers and three aircraft, an He 111, a Savoia and Blaich′s Taifun. The following six of the group were to go on the air raid, while the Italian crew, minus their pilot, stayed behind:
Captain Theo Blaich, pilot of the Taifun
Lieutenant Franz Bohnsack, pilot of the He 111
Sergeant Major Heinrich Geissler, engineer
Sergeant Wolfgang Wichmann, wireless operator
Lieutenant Fritz Dettmann, war correspondent
Major Roberto Count Vimercati-San Severino, desert expert of the Italian Army and pilot of the Savoia
The small group flew to a remote natural airstrip in southern Libya, called Campo Uno, discovered by Roberto Count Vimercati-San Severino in 1935 when he landed there during a safari. He later had surveyed and marked out the site after discovering it, but it lacked any facilities. For the purpose of supplies, the small group used the Savoia as their supply base.


== Operation ==
The Heinkel He 111 took off from Campo Uno at 08:00 on 21 January but experienced, contrary to the weather forecast, bad weather. The plane had been loaded with 1,000 gallons of fuel, but the supply had been carefully calculated and the weather caused the plane to use more fuel than expected.
The plane reached Lake Chad by midday, after which navigation became easier despite the storm intensifying. At 14:30, the plane reached Fort Lamy. No air defence was organised at Fort Lamy and they were able to drop their 16 bombs, 800 kg (1,800 lb) of explosives, unhindered. The French forces were too surprised to organise any air defence and 80,000 gallons of fuel and the complete oil supply were destroyed and, possibly, as many as ten aircraft.
Undamaged, the plane returned northward but the crew found it hard to navigate. As darkness approached, the plane was almost out of fuel and the crew was aware that it would not be able to find its way back to Campo Uno. As the plane possessed a 100 m (328 ft 1 in) trailing antenna, which it could unfold if needed, it released the aerial and sent an SOS; however, no reply was received. Eventually, the plane had to make an emergency landing. Bohnsack was able to land the plane without damage and the crew attempted to contact the Luftwaffe headquarters at Agedabia at the arranged time, without success. The crew had provisions for six days.
After two days at their landing location, presumed to be 120 mi (190 km) from Campo Uno, the crew was able to make contact with the German headquarters.
On 27 January, almost out of water, the stranded crew was discovered by an Italian reconnaissance plane, which re-supplied them with food and water. The following day, a Junkers 52 transport aircraft brought fuel from Agedabia, having left for an unauthorised search of Blaich′s missing commando, and plane and crew were able to return to Campo Uno.


== Aftermath ==
The attack on Fort Lamy caused only minor damage to installations and light casualties but did destroy vital fuel supplies despite valiant efforts to save them. It reduced the available supplies for the Free French Forces and the RAF in the region by half. The raid caused French general Philippe Leclerc to strengthen the anti-aircraft defences at Fort Lamy and to start hit-and-run operations against the Italian forces in Fezzan region.
Blaich and his Sonderkommando continued operations against the Long Range Desert Group throughout the first half of 1942. In June 1942 the units Heinkel bomber crashed near Kufra after an engine failure with the crew being rescued four days later, putting an end to the Sonderkommando's operations.


== References ==


== Bibliography ==
Oliver, David (2005). Airborne Espionage: International Special Duty Operations in the World Wars. United Kingdom: The History Press. ISBN 978-0-7524-9552-1. 
Moore, William Mortimer (2011). Free France's Lion: The Life of Philippe Leclerc, de Gaulle's Greatest General. United Kingdom: Casemate Publishers. ISBN 978-1-61200-080-0. 


== Further reading ==
Wolfgang Wichmann, (German) Husarenstreich gegen Fort Lamy, Flugzeug Magazin, No 5, October/November 1986