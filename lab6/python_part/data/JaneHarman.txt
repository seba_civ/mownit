Jane Margaret Lakes Harman (born June 28, 1945) is the former U.S. Representative for California's 36th congressional district, serving from 1993 to 1999, and from 2001 to 2011; she is a member of the Democratic Party. Resigning from Congress in February 2011, Harman became President and CEO of the Woodrow Wilson International Center for Scholars. She succeeded former Congressman Lee Hamilton and is the first woman to lead the 45-year-old “living memorial” to America’s 28th President.


== Early life and education ==
Harman was born Jane Margaret Lakes in New York City, the daughter of Lucille (née Geier) and Adolf N. Lakes. Her father was born in Poland and escaped from Nazi Germany in 1935; he worked as a medical doctor. Her mother was born in the United States and was the first one in her family to receive college education. Her maternal grandparents immigrated from Russia. Harman attended Los Angeles public schools, graduating from University High School in 1962. She received a bachelor's degree in government with honors from Smith College in 1966 and was Phi Beta Kappa. Harman continued her studies at Harvard Law School, earning her law degree in 1969. Harman married Richard Frank in 1969 and divorced him in 1980; later that year, she married Sidney Harman, 26 years her senior.


== Career ==


=== Early career ===
After graduating from law school, Harman began her political career in Washington, D.C., by serving as chief counsel and staff director for the Senate Judiciary Subcommittee on Constitutional Rights. She served in that position until moving over to the Executive Branch of government where she served as special counsel to the Department of Defense, and as Deputy Secretary of the Cabinet, both positions in the Carter administration.


=== U.S. Representative, 1993 to 1999 ===
Harman was first elected to Congress in 1992 and became the first Smith College graduate to be elected to Congress. From 1993 to 1999, Harman represented the 36th, serving in the 103rd, 104th, and 105th Congresses. In 1994, she barely survived reelection in a heavily Republican year, winning by 811 votes.


=== 1998 California gubernatorial campaign ===
Harman did not run for the 106th United States Congress in 1998, instead entering the 1998 California gubernatorial race. It was during that race that she was called "the best Republican in the Democratic Party".
After losing the Democratic nomination to Lieutenant Governor Gray Davis, she briefly taught public policy and international relations at UCLA as a Regents' Professor before running for and winning her old congressional seat in the 2000 election.


=== U.S. Representative, 2000 to 2011 ===
Harman won her old seat in 2000, and was easily re-elected in 2002, 2004, 2006, 2008, and 2010.
Representing the aerospace center of California during her nine terms in Congress, she served on all the major security committees: six years on Armed Services, eight years on Intelligence, and eight on Homeland Security. She made numerous congressional fact-finding missions to hotspots around the world, including North Korea, Syria, Libya, Afghanistan, Pakistan, Yemen, and Guantanamo Bay. During her long public career, Harman has been recognized as a national expert at the nexus of security and public policy issues. She received the Defense Department Medal for Distinguished Service in 1998, the CIA Agency Seal Medal in 2007, and the CIA Director’s Award and the Director of National Intelligence Distinguished Public Service Medal in 2011.


=== 2014 Justin Bieber incident ===
She received international attention for being interrupted during an interview on MSNBC, when host Andrea Mitchell, reported Justin Bieber's arrest.


=== 2009 wiretap/AIPAC allegations ===
In 2009, it was revealed NSA wiretaps reportedly intercepted a 2005 phone call between Harman and an agent of the Israeli government, in which Harman allegedly agreed to lobby the Justice Department to reduce or drop criminal charges against two employees of AIPAC in exchange for increased support for Harman's campaign to chair the House Intelligence Committee. The NSA transcripts reportedly recorded Harman ending the phone call after saying, "this conversation doesn't exist." It was reported that Alberto Gonzales, Attorney General at the time of the phone call, blocked Justice Department lawyers from continuing the investigation into Harman (in spite of the alleged crime) because the Bush administration "needed Jane" to support their warrantless wiretapping program, which was soon to be revealed to the public by the New York Times.
Harman denied the allegations, and called for the government to release the full transcript of the wire-tapped conversation. In June 2009, Harman received a letter from the Justice Department declaring her "neither a subject nor a target of an ongoing investigation by the Criminal Division." Though the espionage charges were later dropped on the two employees from AIPAC, against the wishes of the FBI, Harman did not get the chair for the foreign intelligence committee.


=== Political positions ===
Harman is on most issues a liberal, earning a 95% rating from the liberal group Americans for Democratic Action. On intelligence and defense issues, she tends to be a moderate. For example, she was one of many Democrats who supported the Iraq War. Harman has combined a moderate stance on economic, trade, and foreign policy issues with a more liberal stance on social issues. For instance, while voting with Republicans to restrict rules on personal bankruptcy, for lawsuit reform, and to abolish the estate tax—as well as on protecting those defense contractors with business interests in her congressional district—Harman voted against the ban on partial-birth abortions, lawsuits against gun manufacturers, the Defense of Marriage Act, and banning indecent broadcasting.


==== H.R. 1955 ====
Harman was criticized by the American Civil Liberties Union (ACLU) for submitting HR 1955, the Violent Radicalization and Homegrown Terrorism Prevention Act of 2007, which passed in the House 404–6. The ACLU claimed the bill included unconstitutional limitations on free speech and beliefs. A related piece of legislation in the U.S. Senate, S. 1959, was submitted by Maine Republican Senator Susan Collins in 2007 but died in committee.


==== Armenian Genocide ====
Harman was a co-sponsor of the Armenian Genocide recognition resolution bill in 2007. However, while still cosponsoring the bill, she wrote a letter to House Foreign Affairs Committee Chair Tom Lantos urging him to delay a floor vote on the legislation. Her argument was that while the genocide deserved recognition, it was not a good time to embarrass Turkey, given that country's role in moderating extremism in the Middle East.


=== Other activities ===
Harman is currently a member of the Defense Policy Board, the State Department Foreign Affairs Policy Board, the Director of National Intelligence’s Senior Advisory Group, and the Homeland Security Advisory Council. She was a member of the CIA External Advisory Board from 2011 to 2013.
Harman is a Trustee of the Aspen Institute and the University of Southern California.


== Personal life ==
Harman's first marriage was to Richard Frank, in 1969, with whom she had two children. Her second marriage was to audio pioneer and multi-millionaire Sidney Harman, who served from 1977 to 1979 as the Undersecretary of the Department of Commerce in the Carter administration before repurchasing the company he founded, Harman International Industries, and later taking it public. She also had two children with him. She has four grandchildren.
Asked in 2010 about a possible conflict of interest, Sidney Harman said: "We’ve been married for over 30 years. I’ve never told her how to run the government and she’s never told me how to run the business [Harman Industries]. That’s absolutely fundamental to us." He retired in 2008 from Harman Industries, purchased Newsweek Magazine in 2010, and founded the Academy for Polymathic Study at USC before he died in April 2011.
Jane Harman maintains a residence in Venice Beach, California.


== References ==


== External links ==
U.S. Congresswoman Jane Harman official U.S. House site
Jane Harman for U.S. Congress official campaign site

Biography at the Biographical Directory of the United States Congress
Financial information (federal office) at the Federal Election Commission
Appearances on C-SPAN
Jane Harman Papers: Finding Aid (Sophia Smith Collection)
Jane Harman Papers: Finding Aid (Sophia Smith Collection)