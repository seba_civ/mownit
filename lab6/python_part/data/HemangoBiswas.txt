Hemango Biswas (Bengali: হেমাঙ্গ বিশ্বাস Hemango Bish-shash) was a Bengali singer, composer, author and political activist, known for his advocacy of peoples music, drawing from the rich genres of folk music (Lokgeet), including Bhatiali originally popular among the fishermen of Bangladesh. He was born in Assam 14, January 1912 and he was admitted with respiratory problem in PG hospital and died on 22nd November 1987.


== Early life ==
Biswas was born in Habiganj, Sylhet (now in Bangladesh) on 14 December 1912, to Harakumar and Sarojini Biswas. did not complete his formal education. He went to the Middle English school in Habiganj and got enrolled in the George Institution of Dibrugarh (now in Assam), and passed his Entrance in 1930 from Habiganj Govt High School.


== Songs ==

Hemango Biswas wrote and sang some popular songs in Bengali. Once a fierce debate ensued between Hemango Biswas and Salil Choudhury on the way of translating the ideal of people's art:

In the meeting of commission on music in Bombay Conference of IPTA, difference of opinion arose between two exponents of people's culture, Hemango Biswas and Salil Choudhury. Hemango Biswas, who was setting most of his lyrics to folk tunes, was in favour of relying only on folk tunes for mass songs with an eye to take it to the peasant masses. Salil Choudhury, on the contrary was of the view of blending folk tunes with harmony of western music. He went to the extent of making oblique remark to Com. Biswas to give up traveling by train, as that is a gift of western civilization and accept bullock cart that is used by the peasant masses. Com. Biswas, in reply accused Com. Choudhury of propounding formalism in people's art. During later part of his life, however Com. Biswas could realize shortcoming of his position and went on to compose mass songs with diverse experimentation. It may be mentioned here that during the debate in Bombay Conference, both Com. Biswas and Com. Choudhury touched upon certain important aspect of people's culture, which was not dealt with till then, but ultimately entire exercise ended in personal accusations.


=== The Internationale ===
Hemango Biswas translated The Internationale to Bengali.


=== Amra Karbo Joy ===
In West Bengal, India and in Bangladesh there are two versions of "We shall overcome", both popular among school-children and political activists. one of those versions, Amra Karbo Joy was translated by the Bengali folk singer Hemango Biswas and re-recorded by Bhupen Hazarika.


=== Ajadee hoyni tor ===
Ajadee Hoyni Tor [English: Freedom you did not get] written & sung by Hemango Biswas, is one of many songs in the legacy of the movement of the progressive left of India.


=== Negro bhai amar ===
This song is not written and composed by Hemango Biswas. It is composed and written by Kamal Sarcar based on Subhas Mukhopadhyaya's translation of Najim Hikmet's poem. The song was sung extensively by Hemango Biswas' troop which often went by the name "Mass Singers" or "Gono Gayen".
Some of his popular songs were shonkhyachil, hobi gonger jwalani koitor, mount batton Mongol kabyo, roosh desher comrade lenin, amra to bhuli nai shahid, name tar chhilo Jon Henry,banchbo re banchbo amra, ful guli kothai gelo, kollolito nobo kolkata(natok kallol based on freedom struggle by the navy), shona bandhu re, deho tori dilam chhariyo,etc.
He sang duet bhupen hazarika, debobrato biswas, pete siger. He motivated the mass to fight for their right, to be united and to be vocal against any form of corruption. He believed in equal right for all and repeatedly tried to request and urge the then Congress Government headed by Siddhartha Shankar Roy as CM of West Bengal to extend their helping hand to the labour class people."Banchbo re banchbo amra" was composed to motivate the poor hapless folks and labours across and section of the society and to improve their stansard of living.
He was exponent of bhawaiya and bhatiwali and created a parallel style (gharana).


== Movies ==
Hemango Biswas was the playback singer in Meghe Dhaka Tara (The Cloud Capped Star) (1960), Lalon Fakir(deho tori dilam chhariyo)and Komal Gandhar (1961).
He was also playback singer of the famous dramma "kallol" composed and played by Utpal Datta.
He was married to Ranu Dutta, the daughter of famous Abhinash Dutta from Bangladesh.His son, Moinak Biswas, teaches Film Studies at Jadavpur University. His daughter Rongili Biswas, an economist by profession, is a writer and folk music performer.


== Partial discography ==
Hemango Biswas-er Gaan
Shankhochil


== See also ==
Left Front
The Internationale


== References ==


== External links ==
Hemango Biswas at the Internet Movie Database
Listen Songs by Hemango Biswas
Rhyming Revolution: Marxism and Culture in Colonial Bengal
Translated chapter from Biswas's travelogue: "Abar Chin Dekhe Elam" (Trip To China Again), 1975
Hemango Biswas’s popular songs presented at University of Dhaka, in Bangladesh, celebrating his 99th birth anniversary