Major General Lord Albert Edward Wilfred Gleichen KCVO CB CMG DSO (15 January 1863–14 December 1937) was a British courtier and soldier.


== Early life and family history ==
Born Count Albert Edward Wilfred Gleichen, he was the only son of Prince Victor of Hohenlohe-Langenburg (a half-nephew of Queen Victoria) and his wife, Laura Williamina (a sister of the 5th Marquess of Hertford). Lady Feodora Gleichen, the noted sculptor, was his sister.
Gleichen's comital title, shared by his sisters, derived from his mother, who had received it from Ernst II, Duke of Saxe-Coburg and Gotha, shortly before her morganatic marriage to his father. Gleichen had been an hereditary estate of the Princes of Hohenlohe in Germany since 1631, and their father voluntarily used it as a comital title to place himself on the same social footing as his wife. But Edward was not entitled to any land or revenues derived from this dynastic property.
On 15 December 1885, the Court Circular announced Queen Victoria's permission for Edward's mother to share his father's rank at the Court of St. James's, and henceforth they were known as TSH Prince and Princess Victor of Hohenlohe-Langenburg. But the Queen did not extend that privilege to their children, although she confirmed use of their German style as count and countesses. In 1913, Edward was granted precedence before marquesses in the Peerage of England (whilst his sisters were granted precedence before the daughters of dukes in the English peerage).


== Career ==
Gleichen served as a Page of Honour to The Queen from 1874 to 1879. He joined the Grenadier Guards in 1881 and gradually rose through the ranks over the years, eventually becoming a Major General. He served in the short-lived Guards Camel Corps in the Sudan campaign in 1884-5 and with the Egyptian army in the Dongala campaign in 1896. In 1899-1900 he served in the Second Boer War in South Africa, and was mentioned in despatches for his actions during the Battle of Modder River 28 November 1899. In January 1900 he was appointed Deputy Assistant-Adjutant-General to the forces in South Africa. He was Sudan agent in Cairo from 1901 to 1903 with the local rank of Lieutenant-colonel, then Military Attaché to Berlin from 1903 to 1906. He and Kaiser Wilhelm II fell out, and Gleichen was sent to be Military Attaché in Washington D.C. from 1906 1907. He met the Wright brothers while in Washington and wrote a report on their aircraft, but also failed to form a relationship with U.S. President Teddy Roosevelt. He was Assistant Director of Military Operations from 1907 to 1911. He served in the First World War, commanding the 15th Brigade from 1911 to 1915, and then the 37th Division from 1915 to 1916. He was Director of the Intelligence Bureau at the Department of Information from 1917 to 1918. He served as Chairman of the Permanent Committee on Geographical Names from 1919.
At Court, the Count was appointed an Extra Equerry to King Edward VII in July 1901.
He wrote a number of books, including:
With the Camel Corps up the Nile (1888)
With the mission to Menelik (1898)
The doings of the Fifteenth Infantry Brigade, August 1914 to March 1915 (1917)
London's open air statuary (1928)
A Guardsman's Memories (1932).


== Change of title ==
When King George V commanded his German relatives domiciled in Britain to Anglicize their names and titles in 1917, the Gleichens' 1913 precedence was reduced to that of younger son/daughters of a marquess in the Peerage of the United Kingdom. This was because only marquisal rank was conferred upon the King's nearer, heretofore princely relatives, the Tecks and Battenbergs. Although inexplicably allowed to retain their German surname, the Gleichens relinquished use of the comital title and acquired the prefix of Lord or Lady, although this was not hereditary for Edward's descendants as his countship had been.
On 2 July 1910, Gleichen married Hon. Sylvia Gay Edwardes (a daughter of the 4th Baron Kensington), who was a Maid of Honour to Queens Victoria and Alexandra. They had no children.


== Honours and awards ==
British decorations
CMG: Companion of the Order of St Michael and St George - 1898 - for his contributions on a Mission to Ethiopia.
DSO: Companion of the Distinguished Service Order - 29 November 1900 - for his contributions in the Second Boer War.
CVO: Commander of the Royal Victorian Order - 2 February 1901 - on the day of the funeral of Queen Victoria
CB: Companion of the Order of the Bath - 1906
KCVO: Knight Commander of the Royal Victorian Order - 1909
Foreign decorations
1897: Order of the Star of Ethiopia (Third Class) - for his contributions on a Mission to Ethiopia.
1905: Order of the Medjidie (Second Class) - for his services in the Egyptian Army
Unknown dates: Commander of the Legion of Honour, Order of the Dannebrog (First Class)


== Ancestry ==


== References ==

The London Gazette
Burke's Peerage & Gentry, 107th edition
Centre for First World War Studies
Liddell Hart Centre for Military Archives


== External links ==
 Works written by or about Albert Edward Wilfred Gleichen at Wikisource
Works by Lord Edward Gleichen at Project Gutenberg
Works by or about Lord Edward Gleichen at Internet Archive