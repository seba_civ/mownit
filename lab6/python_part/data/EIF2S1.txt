Eukaryotic translation initiation factor 2 subunit 1 (eIF2α) is a protein that in humans is encoded by the EIF2S1 gene.


== Function ==
The protein encoded by this gene is the alpha (α) subunit of the translation initiation factor eIF2 complex which catalyzes an early regulated step of protein synthesis initiation, promoting the binding of the initiator tRNA (Met-tRNAiMet) to 40S ribosomal subunits. Binding occurs as a ternary complex of methionyl-tRNA, eIF2, and GTP. eIF2 is composed of 3 nonidentical subunits, alpha (α, 36 kD, this article), beta (β, 38 kD), and gamma (γ, 52 kD). The rate of formation of the ternary complex is modulated by the phosphorylation state of eIF2α.


== Clinical significance ==
After reperfusion following brain ischemia, there is inhibition of neuron protein synthesis due to phosphorylation of eIF2α. There is colocalization between phosphorylated eIF2α and cytosolic cytochrome c, which is released from mitochondria in apoptosis. Phosphorylated Eif2-alpha appeared before cytochrome c release, suggesting that phosphorylation of eIF2α triggers cytochrome c release during apoptotic cell death.
Mice heterozygous for the S51A mutation become obese and diabetic on a high-fat diet. Glucose intolerance resulted from reduced insulin secretion, defective transport of proinsulin, and a reduced number of insulin granules in beta cells. Hence proper functioning of eIF2α appears essential for preventing diet-induced type II diabetes. 


== Dephosphorylation inhibitors ==
Salubrinal is a selective inhibitor of enzymes that dephosphorylate eIF2α. Salubrinal also blocks eIF2α dephosphorylation by a herpes simplex virus protein and inhibits viral replication. eIF2α phosphorylation is cytoprotective during endoplasmic reticulum stress.


== See also ==
eIF2


== References ==


== Further reading ==