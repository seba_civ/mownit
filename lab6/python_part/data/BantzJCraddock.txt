Bantz John Craddock (born August 24, 1949) is a former United States Army general. His last military assignment was as Commander, U.S. European Command (USEUCOM) and the NATO's Supreme Allied Commander Europe (SACEUR) as well as the commanding officer of Allied Command Operations (ACO) from December 2006 to June 30, 2009. He also served as Commander, U.S. Southern Command from November 9, 2004 to December 2006. After his retirement in 2009, he was hired by MPRI, Inc., (sometimes called Military Professional Resources, Inc.) to serve as its corporate president.


== Early life and education ==
Craddock was raised in Doddridge County, West Virginia. He graduated from Doddridge County High School in 1967.


== Career ==

Craddock was commissioned as an Armor officer upon graduation from West Virginia University. His initial tour of duty was with the 3rd Armored Division in Germany, followed by an assignment at Fort Knox, Kentucky, as an armor test officer for the U.S. Army Armor and Engineer Board. After completion of the Armor Officer Advanced Course, he was again assigned to the 3rd Armored Division, commanding a tank company in the 1st Battalion, 32nd Armor Regiment.
In September 1981, Craddock was reassigned to the Office of the Program Manager, Abrams Tank Systems in Warren, Michigan, as a Systems Analyst and later as Program Executive Officer. After graduation from the Command and General Staff College, Craddock joined the 8th Infantry Division (Mechanized) in Germany, serving as the Executive Officer of the 4th Battalion, 69th Armor Regiment for two years. He was subsequently reassigned to the Division Headquarters as the Deputy G3, Operations.
In May 1989, Craddock assumed command of the 4th Battalion, 64th Armor Regiment 24th Infantry Division (Mechanized) at Fort Stewart, Georgia. He commanded the Tuskers for 26 months, deploying to Operation Desert Shield and Desert Storm. Following command, Craddock was the Assistant Chief of Staff, G3, Operations, for the 24th Division. Subsequently, he attended the U.S. Army War College, graduating in 1993. Craddock then assumed command of the 194th Armored Brigade (Separate) at Fort Knox. In June 1995, Craddock inactivated the Brigade as part of the U.S. Army's post-cold war drawdown, and was assigned as the Assistant Chief of Staff, G3, for III Corps at Fort Hood, Texas.
In 1996, Craddock was reassigned to the Joint Staff in the Pentagon as an Assistant Deputy Director in J5. In August 1998, he joined the 1st Infantry Division (Mechanized) in Germany as the Assistant Division Commander for Maneuver. While serving in that capacity, Craddock was designated as Commander of U.S. Forces for the initial entry operation into Kosovo. In August 1999, Craddock was reassigned as the Commanding General of the 7th Army Training Command, U.S. Army Europe. In September 2000, Craddock assumed command of the 1st Infantry Division (Mechanized) – the "Big Red One".
From August 2002 to 2004, Craddock served as the Senior Military Assistant to Secretary of Defense Donald Rumsfeld.
Craddock served as Combatant Commander of United States Southern Command from 2004 until 2006. On July 14, 2006, NATO announced that, when his term as COCOM of the United States Southern Command expired, Craddock would succeed James L. Jones as Supreme Allied Commander Europe (SACEUR) — NATO's top commander of operations in Europe. The change-of-command ceremony at Mons, Belgium, occurred on December 7, 2006.


=== Controversy ===
Craddock defended the controversial Guantanamo Bay detention camp against criticism. While overseeing Guantanamo, he blocked attempts to get a commander of the camp reprimanded over abuse claims. Craddock insisted that the officer had done nothing wrong.
On January 28, 2009, Der Spiegel reported obtaining a classified NATO document in which Craddock ordered troops to kill drug traffickers and bomb narcotics laboratories in Afghanistan, even if there is no evidence that they are involved in terrorist activities.
Former U.S. Defense Secretary Robert Gates' book Duty is very critical of Craddock in his role as Supreme Allied Commander Europe. Gates cites examples where Craddock did not want fellow U.S. Army generals, such as Stanley A. McChrystal, to attend coalition meetings with partner nations. At one point Craddock provided his unsolicited advice to Gates regarding who he thought should attend a senior coalition meeting. Gates then had to order Craddock to carry out the mission as instructed by him. In the book, Gates goes on to opine that was the only time in his career in governmental service in which he had to "order" a general officer to carry out a specific task.


== Awards and decorations ==
Additionally, Craddock has been honored of the following associations:
United States Armor Association — Order of Saint George
National Infantry Association — Order of Saint Maurice Legionnaire
Ordnance Association — Order of Samuel Sharpe, Honorary Kentucky Colonel
Honorary Texan, Artillery Association — Order of Saint Barbara


== Personal life ==
A bridge in Doddridge County, West Virginia on U.S. Route 50 was dedicated to Craddock in 2006.


== See also ==


== References ==
 This article incorporates public domain material from the United States Government document "http://www.southcom.mil/PA/Media/Media%20Relations/bios/southcom/bioCDR.htm".

"SACEUR General John Craddock". SHAPE Biographies. NATO. Archived from the original on 7 December 2006. Retrieved December 8, 2006. 


== External links ==
Supreme Headquarters Allied Powers Europe
United States European Command