The Northern Force were the northern-most team in Netball New Zealand's former premier competition - the National Bank Cup. For the 2008 ANZ Championship the Force merged with the Auckland Diamonds to create the Northern Mystics.
Based at the North Shore Events Centre in Auckland, the Force region included North Harbour and Northland, with the majority of its players being based in Auckland. The playing colours of the team were Maroon and Black, and were sponsored by Fujifilm.


== History ==
Perhaps the perennial under-achievers of the competition, the team had always been blessed with many talented sportswomen, yet were unable to win the competition.
Coached by former Silver Ferns coach Yvonne Willering, the Force were renowned for their formidable defensive line-ups which have included players such as Linda Vagana, Leana de Bruin and Sheryl Scanlan. The Force's midcourt was led by dynamic and inspirational captain and Silver Fern Temepara "the pocket-rocket" George. In the shooting circle, the Force boasted one of the tallest shooters in New Zealand netball at the time, Catherine Latu (at 192 cm).


== Players 1998–2007 ==
The Northern Force were filled with New Zealand talent. The defensive line was always strong and included the likes of Leana De-Bruin, Linda Vagana, Lorna Suofoa, Villimina Davu, Sheryl Scanlin, Sharlene Smith-Wolfgramm, Kate Dowling all former or current NZ players. The mid-court included world class Silver fern Temepara George, Angelina Yates and Sonya Hardcastle, all highly rated New Zealand players. Often, the shooting circle would crumble, the shooters were known for drifting in and out of the match. It included Daneka Wipiiti (Silver Fern) and Megan Dehn (former Australian Netball team) but lost them both to the Southern Sting and their new team Southern Steel. Other shooters included Catherine Latu (Samoa), Teresa Tairi (Silver Ferns), Tania Dalton (Silver Ferns) who were all former Internationals.


== Shooters ==
In the 2006 National Bank Cup the Force were semi-finalists, being beaten by the Southern Sting 64-59 in overtime. The shooters finished the season with the following percentages:

Catherine Latu: 83% (135 from 162)
Megan Dehn: 82% (121 from 148)
Brigette Tapene: 76% (70 from 92)


== 2007 Force Team ==
Leana de Bruin - Now With Southern Steel
Vilimaina Davu
Megan Dehn - Now With Northern Mystics
Temepara George - With Northern Mystics
Julie Kelman-Poto
Catherine Latu - Now With Northern Mystics
Ritua Petero
Finau Pulu - Now With Canterbury Tactix
Sheryl Scanlan - Now With Southern Steel
Lorna Suafoa
Bridgette Tapene
Angelina Yates - Since Retired.


== 2006 Force Team ==
Leana de Bruin
Megan Dehn
Temepara George
Kimberley Horton- retired in 2007
Catherine Latu
Leigh Price
Finau Pulu
Lorna Suafoa - since retired
Brigette Tapene
Daneka Wipiiti
Angelina Yates


== 2005 Force Team ==
Leana de Bruin
Kate Dowling - signed with the Canterbury Flames in 2006 and 2007
Temepara George
Kimberley Horton
Catherine Latu
Ritua Petero
Nicolette Ropati
Sheryl Scanlan
Teresa Tairi
Linda Vagana
Daneka Wipiiti
Angelina Yates


== 2004 Force Team ==
Fleur Bell
Kate Dowling
Temepara George
Kimberley Horton
Julie Kelman-Poto
Sheryl Scanlan
Matelita Shaw
Teresa Tairi
Linda Vagana
Daneka Wipiiti
Angelina Yates


== 2003 Force Team ==
Kate Dowling
Temepara George
Malu Fa'asavalu
Kimberley Horton
Nicolette Ropati
Sheryl Scanlan
Laila Sturgeon
Lorna Suafoa
Teresa Tairi
Linda Vagana
Daneka Wipiiti
Angelina Yates


== 2002 Force Team ==
Kate Dowling
Temepara George
Malu Fa'asavalu
Rochelle Hardcastle
Leonie Matoe
Sheryl Scanlan
Lorna Suafoa
Laila Sturgeon
Jenna Swann
Teresa Tairi
Linda Vagana
Daneka Wipiiti


== 2001 Force Team ==
Tania Dalton
Kate Dowling
Temepara George
Rochelle Hardcastle
Sonya Hardcastle
Kathleen Lye
Nicolette Ropati
Sheryl Scanlan
Sharlene Smith signed for comtez
Anna Tai
Teresa Tairi
Corlie Wolmarans
Linda Vagana
Owena Zanders


== 2000 Force Team ==
Vanessa Banbrook
Elizabeth Dean
Tania Dalton
Kate Dowling
Temepara George
Kathleen Lye
Elise Middleton
Sheryl Scanlan
Sharlene Smith
Anna Tai
Teresa Tairi
Linda Vagana


== 1999 Force Team ==
Jenny May Coffin
Elizabeth Dean
Sonya Hardcastle
Opheira Harder
Dionne Moors
Sharon Pomfrett
Sheryl Scanlan
Sharlene Smith
Laila Sturgeon
Lorna Suafoa
Teresa Tairi
Linda Vagana


== 1998 Force Team ==
Teresa Dobbs
Sonya Hardcastle
Lise Inu
Kate Newson
Tania Dalton
Pauline Marsh
Janine Topia
Sheryl Scanlan
Sharlene Smith
Lorna Suafoa
Teresa Tairi
Linda Vagana


== Coaches ==
Renowned coach Yvonne Willering served the longest coaching term from 2003 - 2007. Australian Maria Lynch served from 2000-2002. Alison Wieringa in 1999 and ex-National league player Mary Jane Araroa coached the team in its first round of the competition in 1998.


== Record for the Force in the National Bank Cup ==
2007- 2nd
2006- 3rd
2005- 3rd
2004- 4th
2003- 2nd
2002- 3rd
2001- 5th
2000- 4th
1999- 3rd
1998- 5th
Overall winning percentage of 61%, which is the second highest winning percentage for any team in the history of the competition.


== 2007 ==
For the 2007 competition, the Force named a formidable squad, with the inclusion of former Silver Ferns defender Vilimaina Davu transferring from the Canterbury Flames and Sheryl Scanlan returning from pregnancy. This left the Force with an intimidating four Silver Ferns (three former, one current) in their defence end. Suafoa, Scanlan and Davu were former Silver Ferns while de Bruin was a current member of the New Zealand team.


== References ==
http://www.netballnz.co.nz/default.aspx?s=nbank_team_force
'Northern Force 1998 - 2007'