Cassie Davis (born 1987) is an Australian singer, songwriter and producer from Perth, Western Australia. In addition to vocals, Cassie can play the guitar, piano, keyboard and sampler, all of which she plays at some point on her debut album, Differently.


== Biography ==
Cassie Maree Davis was born in Gnangara, Western Australia) is the second child of a family of four, her father, Steven, is a pastor in Perth. Davis' parents introduced her to music.

My dad would always put instruments in my hands and encourage me to play music. He pretty much taught me how to sing, and when I was eight my parents bought me my first guitar. I had played piano before that, but it was when I got my guitar that I started to be become creative. I taught myself to play and started writing songs.

Davis recorded her first songs on her home computer at age twelve. In 1999, she did work experience at a local recording studio for three years and learnt how to use mics, song arranging, basic producing and how to make a good recording. Davis then enrolled at the Western Australian Academy of Performing Arts (WAAPA), where she studied sound engineering and production. In 2003 she travelled to the United States, accompanied by her elder sister, Emma, who handles the business side of their label, 12 Stones.

Emma and I started going back and forth between Australia and New York from the end of 2003. We were songwriting and knocking on doors. Looking back we were so naive to go there and think that we could make it. But all opportunities have come from us stepping out on a limb

. Her younger brother, Joseph, plays guitar in her band.
Davis wrote and recorded the majority of her album independently. She also worked with producer Printz Board, producer Rodney Jerkins and songwriter/producer Wayne Wilkins who has already brought her in to work on a number of writing and production projects.
In a 2009 article in Rolling Stone Australia, she describes herself as

I'm like a bitzer. I never wanted to be 'an artist', I always wanted to be known as 'a producer' or 'a writer' as well.


== Music career ==


=== 2009–present: Differently ===

Davis landed a worldwide four album deal with Sony BMG through her label 12 Stones. Her debut release was the single "Like It Loud" which reached #11 on the ARIA singles chart. Davis performed "Like It Loud" on Week 2: Top 18 Results episode of So You Think You Can Dance Australia backed by her band and 50 dancers.
In support of her single "Like It Loud", Davis joined forces with Australian clothing label Supre to create a limited edition T-shirt. Cathy Van Der Meulen said "Cassie is such an exciting new talent and represents everything our brand stands for in an Australian girl...fun, fashionable and forward thinking. Cassie is going to be an Australian icon not unlike Supre."
Her second single "Differently" was released in Australia on 24 April 2009. The music video features Travis McCoy, Printz Board and Fish from Fishbone. The third single, "Do it Again", was co-written by former Australian artist Leah Haywood, and Daniel James and produced under Haywood and James moniker Dreamlab. 'Do it Again' was released digitally and physically on 7 August. Davis' debut album was released 14 August 2009. Cassie Davis' second single "Differently" was certified Gold on 18 September 2009. Confirmed via her Twitter page  Cassie Davis also wrote and appears on Warren G's 2009 album The G Files, with Snoop Dogg on the track "Swagger Rich" which appeared on the final season of Ugly Betty.
Cassie is currently the ambassador for Camp Quality and their new program for high school students "The Teenage Alchemist" which is the children's family cancer charity that believes in bring optimism and happiness to the lives of children and families affected by cancer through fun therapy. "I'm thrilled to be helping Camp Quality spread optimism through high schools and to be involved in educating teenagers about cancer, making positive life choices and peer pressure. I want to encourage people to be strong and stay true to who they are and I think The Teenage Alchemist is a great way to get the message out there‘’, " says Cassie.
In Early 2010, Cassie wrote a single titled, "Choose You" for Stan Walker's, second studio album, From the Inside Out and was released as the second single the song went platinum
In 2011, Cassie started a production group with fellow writer/producer Snob Scrilla. Together, they wrote and produced Havana Brown's debut single "We Run The Night". The song was released in late April, and was the first Australian track to debut in the top 10 in 2011 and is certified double platinum. They also produced and wrote Havana browns second single "Get It"
Cassie Co-wrote the song "Take Me Away" for Marvin Priest Feat. Wynter Gordon.
More recently they have produced and written the newly released song "All We Have" for X-factor judge Natalie Bassingthwaighte.


== Discography ==


=== Studio albums ===


=== Singles ===


=== Music videos ===


== References ==