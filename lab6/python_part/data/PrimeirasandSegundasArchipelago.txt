The Primeiras and Segundas Archipelago is a chain of 10 sparsely inhabited barrier islands and two coral reef complexes situated in the Indian Ocean off the coast of Mozambique and near the coastal city of Angoche. The islands lie in two groups along the western side of the Mozambique Channel.


== Description ==
The islands lie in a string along Africa's continental shelf. The five Segundas islands are in the north, separated by a stretch of open water and reefs from the five islands of the Primeiras chain to the south. The eastern sides of the islands are fringed with coral reefs, composed mainly of soft corals, with hard corals at their southern edges. Beds of seagrass are situated between the islands and the mainland, which are important habitat for sea turtles and dugongs. The southern islands support Mozambique's larges nesting grounds for green sea turtles, and hawksbill sea turtle also use the beaches. The archipelago also hosts the most important dugong population in the western Indian Ocean.
Vegetation on these low islands includes mangrove, grass and scrub. Offshore, they are more noted for the biodiversity of their spectacular coral reefs, which support an important fishery. Due to the lack of reliable sources of fresh water, habitation on the islands is sparse — mainly in support of fishing operations.


== History ==

The European discovery of the archipelago came on 25 February 1498 during Vasco da Gama's first expedition to India. The islands became an important stopping-off point for Portuguese trading fleets sailing for India and the Orient, which were often in need of emergency repairs after rounding the Cape of Good Hope. The islands remained a colony of Portugal until Mozambican independence in 1975, at which time they became part of Mozambique.


== Conservation efforts ==
The area has been threatened by illegal fishing operations and impacts from unauthorized tourism. Cutting down of coastal mangrove has also increased erosion, with negative effects on marine life. The archipelago is currently the focus of joint conservation and development projects by CARE and the World Wildlife Fund in cooperation with the government of Mozambique and local NGOs. These projects are aimed at preserving the environment and surrounding coral reef system, restoring fisheries, protecting breeding grounds for sooty terns, dugongs and green sea turtles, and creating a better quality of life for the people of the region.
The initial goal of the projects was the creation of a 17,000 km2 area to be protected as a marine reserve. This was realized in 2012 when Mozambique established Africa's largest protected marine zone surrounding the islands. A continuing goal is to increase awareness among local people of how their activities affect the larger environment and their own long-term security and prosperity. Moving towards more sustainable farming and fishing methods is also a focus of educational outreach to area communities.


== References ==