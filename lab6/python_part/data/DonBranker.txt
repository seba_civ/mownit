Don E. Branker is an American music event promoter and emcee, known for organizing a number of historic rock festivals such as California Jam and California Jam II.


== Early life ==
Branker grew up in Fresno. His father and uncle owned and operated Branker Brothers Cattle Co., one of the larger ranching operations in the San Joaquin Valley, until the 1960s when land buyouts by the oil companies cut down on the amount of grazing acreage.
The family moved to Los Angeles, where Branker became a self-described "street punk."


== The Festivals ==

Don E. Branker, promoter and emcee of the first and largest of its time Southern California outdoor one day concerts at Ontario Motor Speedway, California Jam on April 6, 1974. With a record breaking paid attendance of over 250,000. "The Greatest Concert of the 20th Century" and Considered the "Woodstock of the West".

California Jam II, also known as Cal Jam II, was a music festival held in Ontario, California, at Ontario Motor Speedway on March 18, 1978. Promoted and Produced by Don E. Branker. More than 300,000 people attended. The festival was a sequel to the original California Jam held in 1974.A television special featuring highlights of the festival was broadcast a few months later on the American Broadcasting Company network.
Branker has promoted/produced such famous acts as The Doors, Jimi Hendrix, The Rolling Stones, Pink Floyd, Aerosmith, Rush, Donovan and Minnie Riperton.


== Television ==
Hosted "In Concert (TV series)" show in early 70's
Promoted Evel Knievel's Snake River Canyon Jump. Sunday, Sept 8 1974


== Skateboarding ==
Don moved from Rock Concerts to promoting Skateboarding Expositions and competitions. Don made a reference to an example of the reasons he could think of for moving away from "Rock Stars", there was an incident a Cal Jam II, trying to fulfill all the request and requirements of the artist is part of the job. One particular request of the headliners of the festival Aerosmith was to have two pounds of "jellybeans" available in their dressing room. At some point during the show Don was told that the lead singer Steven Tyler of Aerosmith had refused to go on stage because Branker's assistants forgot to "as requested" remove all the "black" jellybeans from the two pounds provided for them. Don promptly went backstage and told Tyler "Fine. I'm going out on stage and announce to 300,000 kids you refuse to perform because of 'black jellybeans.'" When Don turned to go on stage, Aerosmith's manager tackled Don to his knees and pleaded with him. Aerosmith did perform. Don said "this [incident with Tyler] sickened me so much, I stopped producing rock concerts."


== Autobiography ==
When asked "What's you favorite story" Don replied, "My favorite one will be the one where I tell the whole unbelievable story". A great author from Fresno, Greg Gonzales, is right now starting to write the bio story. Its tentatively title:The Promoter Sex, Drugs, Rock N Roll.


== Awards ==
Don E Branker, California Jam, California Jam II promoter, was the featured star of the 2008 Los Angeles Music Awards and Recipient for "The Greatest Concert of the 20th Century!"


== See also ==
Cal Jam II Interview on YouTube


== References ==


== External links ==