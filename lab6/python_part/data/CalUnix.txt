cal is a standard program on Unix and Unix-like operating systems that prints an ASCII calendar of the given month or year. If the user does not specify any command-line options, cal will print a calendar of the current month.


== Examples ==


== Features ==

The Gregorian reformation was adopted by the Kingdom of Great Britain, including its possessions in North America (later to become eastern USA), in September 1752. As a result the September 1752 cal shows the adjusted days missing. This month was the official (British) adoption of the Gregorian calendar from the previously used Julian calendar. This has been documented in the man pages for Sun Solaris as follows. "An unusual calendar is printed for September 1752. That is the month when 11 days were skipped to make up for lack of leap year adjustments." The Plan 9 from Bell Labs manual states: "Try cal sep 1752."
The cal command was present in 1st Edition Unix.


== See also ==
Cron process for scheduling jobs to run on a particular date.
List of Unix programs


== References ==


== External links ==
cal(1) – NetBSD General Commands Manual
Source of explanation of cal 9 1752 phenomena (humor)