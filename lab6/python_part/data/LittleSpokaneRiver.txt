The Little Spokane River is a major tributary of the Spokane River, approximately 35 mi (56 km) long, in eastern Washington in the United States. It drains a rural area of forested foothills and a farming valley north of the city of Spokane along the Idaho-Washington border. It has two branches one starting west of Newport and the other comes from Eloika lake which is further west. The two branches come together about a quarter mile east of Milan. The supply from Eloika is quite warm in the summer and has different fish habitat than the colder branch from Newport. The best trout fishing is where the two branches come together, but is on private owned land. It's also a privately owned water body which is rare.
It rises in southern Pend Oreille County, south of Newport near the Idaho state line. It flows SSW past Milan and Colbert. It joins the Spokane River from the east approximately 10 mi (16 km) northwest of Spokane. There is a natural area near the mouth with an Indian painted rock, several trails to walk on and berths for canoes and kayaks.
Flyfishing is popular on The Little Spokane River but standard angling does work. The river is abundant with native and introduced fish.
The Little Spokane River contains native Rainbow Trout, introduced Brown Trout, Suckerfish migrated from Spokane River, native pikeminnow, introduced Brook Trout, and native Mountain whitefish. Strict rules are placed for the winter whitefish season. The speed and depth varies in the river but is generally slow moving and 2–5 feet deep. The river has an average width of 40–60 feet. The native protected species of fish in The Little Spokane River are Redband Trout and Westslope cutthroat trout.
In 1893, the ichthyologists Charles Gilbert and Barton Warren Evermann reported extensive damage to the Little Spokane as a result of human activities:
"The character of this stream is being materially changed by the advent of civilization, a fact which is, or has been, true of most streams in this country. The cutting away of the timber and brush on the immediate bank and the cultivation of the land within the drainage area of the stream have greatly increased the surface erosion and, in consequence, the impurities of the stream."


== See also ==
Missoula Floods
List of Washington rivers
Spokane River


== References ==


== External links ==
USGS: Spokane River Basin
Hydrogeology of the Little Spokane River Basin, Spokane, Stevens, and Pend Oreille Counties, Washington United States Geological Survey