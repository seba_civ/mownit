Gary Charles Gait (born April 5, 1967) is a Canadian retired professional lacrosse player and currently the head coach of the women's lacrosse team at Syracuse University, where he played the sport collegiately.
He played collegiately for the Syracuse Orange men's lacrosse team and professionally in the indoor National Lacrosse League and the outdoor MLL, while representing Canada at the international level. Gait has been inducted into the United States Lacrosse National Hall of Fame and the National Lacrosse League Hall of Fame.
He was a four-time All-American for the Syracuse Orange men's lacrosse team from 1987-90 (including first-team honors from 1988 to 1990), and was on three NCAA championship-winning teams. He twice won the Lt. Raymond Enners Award, given to the most outstanding college lacrosse player, in 1988 and 1990. Gait holds the Syracuse career goals record at 192 and the single-season goals record at 70, an NCAA record until 2008. In 1997, the NCAA Lacrosse Committee named Gait, along with his twin brother and Syracuse teammate, Paul, to the 25th Anniversary Lacrosse team.
He played in the NLL for 17 years, winning Rookie of the Year in 1991, earning league MVP honors for five straight years, from 1995 to '99 and winning All-Pro honors each season. Gait led the league in points and goals seven times, won three league championships and finished his indoor career with 1,091 points, a league record at the time.
Gait also played five seasons in MLL from 2001 to 2005, winning the league title three times and co-MVP honors in 2005.
He helped Canada win the 2006 World Lacrosse Championship, the country's first world championship since 1978, by scoring four goals in the final against the United States.


== College career ==
Gary Gait and his twin brother Paul played lacrosse for the Syracuse Orange of Syracuse University from 1987 to 1990, where they set numerous records during their time there. Gait was named an All-American by the USILA four times — three times as a First Team selection and once as an Honorable Mention. Gary graduated as Syracuse University's all-time goal leader with 192 career goals. He led the Orange to three NCAA DI Championships and was named the NCAA Player of the Year in 1988 and 1990 and the Most Outstanding Player of the NCAA Tournament in 1990. The Gait twins are also widely known for popularizing innovative plays such as behind-the-back passes and shots and the "Air Gait," an acrobatic scoring move where they would jump from behind the goal crease and score a goal in mid-air by dunking the ball over the top goal crossbar and land on the opposite side of the crease. This move was later banned in NCAA play.


== NLL career ==


=== Detroit (1991–92) ===
Gait started playing in the Major Indoor Lacrosse League (later the National Lacrosse League) in 1991. He was drafted by the Detroit Turbos who got both Gait brothers in a slightly controversial double-pick. In this season he won the MILL championship and was given the Rookie of the Year Award. He played with Detroit for one more season before him and his brother were both traded to the Philadelphia Wings in 1993. This trade was highly controversial as the Wings traded all of their draft picks to Detroit for the two players.


=== Philadelphia (1993–97) ===
Gait played five seasons on the Philadelphia Wings. Gait visited the championship his first four years on the Wings and won the title twice (1994 & 1995). He was voted league MVP in 1996 and 1997 and was crowned Championship MVP in 1995. Gait spent more time with the Wings than any other single NLL/MILL team and is where he is most remembered for playing. In 1995 Gary's brother Paul was traded to Rochester, separating the brothers until they played together on the Washington Power in 2001. Gary was traded to Baltimore for the 1998 season so he could play closer to his home.


=== Baltimore Thunder (1998–99) ===
Gait only played two seasons with the Baltimore Thunder. In Gait's first year on the team they went to the Championship which was a new best-of-3 style series. They were defeated two games to zero by his former Philadelphia Wings. Gait was still voted league MVP for the 1998 season for how much he helped Baltimore improve. In 1999, Baltimore went 8-4 in the regular season but lost in the first round of the playoffs to the Rochester Knighthwaks. The Baltimore Thunder re-located to become the Pittsburgh CrosseFire in 2000 and so Gary moved with the franchise.


=== Pittsburgh CrosseFire (2000) ===
In Pittsburgh's only season, they went 6-6 and missed the playoffs. This was the first time in Gait's NLL/MILL career that he missed the playoffs, he would only miss 3 times in his entire 18-season career. After a not-so-good first season the CrosseFire re-located to Washington, D.C. where they became the Washington Power.


=== Washington Power (2001-02) ===
In the two seasons the Washington Power existed, they made it to the playoffs. Gary was there for both of these season and so was his brother Paul, re-uniting them for the first time since 1994 when the both played in Philadelphia. In 2001, Washington lost in a semi-final game to Toronto with a final score of 10 to 9. In 2002, Washington beat Philadelphia in the quarter-finals 10–11 before having a rematch with Toronto in the semi-final where the Power was defeated 11–12. The Washington Power faced low attendance and as such re-located to Colorado where they became the Colorado Mammoth. As with the Thunder and CrosseFire, Gait moved along with the franchise as it re-located once again.


=== Colorado Mammoth (2003–05) ===
Gait played with the Colorado Mammoth for three seasons. In his first game of the 2006 season, Gary was honored by the Mammoth. On December 30, 2005, at the Pepsi Center in Denver, Colorado, the Colorado Mammoth raised his jersey number (22) to the rafters, making him the second player in NLL history to have his number retired, after Buffalo's Darris Kilgour. They went to the playoffs all three seasons but never advanced to the championship. Gait announced his retirement following the 2005 season. He was then voted into the NLL Hall of Fame along with his brother, Paul. After retiring he became Head coach of the Mammoth for 2006 & 2007, but he stepped down as head coach in August 2007 so he could pursue other interests.


=== Rochester Knighthawks (2009–11) ===
Gait returned to the NLL in 2009 when he joined the Rochester Knighthawks. During the 2009 season, Gary was named a reserve to the All-Star game. 2009 would also mark the last time Gary would visit the NLL playoffs as the Knighthawks missed the playoffs in the 2010 and 2011 seasons. Following the 2011 season, Gary announced he was retiring from NLL play for good to pursue a career of lacrosse coaching.
Gary Gait had one of the most spectacular NLL careers of any player in league history. He has the second most career goals of any player with 635 and the highest goals per game average in league history with 3.207 goals per game. Gary is also tied with his brother for the most goals in one single game, 10. He had his number retired by Colorado and he is a member of the National Lacrosse League Hall of Fame.


== MLL career ==
Gait played in Major League Lacrosse since its inception in 2001. He was a member of the Long Island Lizards. After the first season, Gait was traded to the Baltimore Bayhawks where he served as a player-coach for the next four years. In 2005, Gait won the Steinfeld Cup as a player-coach. He scored six goals in the Championship Game and was named MVP of that game as well as the season.
Gait initially retired from MLL play in 2005 but recently returned and signed with the Hamilton Nationals for their inaugural season in 2009.


== WLA career ==
Gait, along with his brother, had outstanding seasons with the Victoria Shamrocks of the Western Lacrosse Association. Gait won the Mike Kelly Memorial Trophy as most valuable player of the Mann Cup as a Shamrock in 1997, and shared the most valuable player award with his brother Paul in 1999.


== Coaching career ==
In June 2005, Gait was named head coach of his former NLL team, the Colorado Mammoth. After a 10-6 season in which the Mammoth finished second in the Western Division, Gait led them to an overtime 18-17 win over Calgary and a 13-12 win over Arizona before shutting down the East Division champion Buffalo Bandits 16-9 in the Championship game. Gait became the first rookie head coach to win a championship since Tony Resch did it with the Philadelphia Wings in 1994, a team on which Gait played.
In August 2007, Gait stepped down after two seasons as head coach of the Mammoth and returned to his alma mater Syracuse University, becoming the second head coach in the history of the women's lacrosse program. Prior to this position, Gait had served as an assistant coach for the University of Maryland women's team for nine seasons.
On February 3, 2011, Gait was announced as a new assistant coach for the Hamilton Nationals of Major League Lacrosse.


== International lacrosse career ==
Gait was a member of the Canadian National Team in 1990, 1994, 1998, 2004, and 2006. In that final year, he led Team Canada to a historic 15-10 victory over the United States in the 2006 World Lacrosse Championship, his last ever international game. Gait scored four goals in the final quarter, marking a fairytale finish to his international playing career as the World title gave him every possible major lacrosse title (three NCAA championships at Syracuse in 1988, 1989, 1990; three NLL titles in 1991, 1994, 1995; three MLL titles in 2001, 2002, 2005; three Mann Cups in 1991, 1997, 1999; the Heritage Cup in 2004; and the International Lacrosse Federation World Championship in 2006).


=== Results of international play ===
1990 – Finalist in the World Lacrosse Championship
1994 – Third place at the World Lacrosse Championship
1998 – Finalist in the World Lacrosse Championship
2002 – Finalist in the Heritage Cup
2004 – Winner of the Heritage Cup
2006 – Winner of the World Lacrosse Championship


== Club lacrosse career ==
Gait also played amateur lacrosse for the storied Mount Washington Lacrosse Club in the 1990s.


== Statistics ==


=== NLL ===
Reference:
GP–Games played;G–Goals;A–Assists;Pts–Points;LB–Loose balls;PIM–Penalty minutes;Pts/GP–Points per games played;LB/GP–Loose balls per games played;PIM/GP–Penalty minutes per games played;


=== Syracuse University ===
(a) 4th in NCAA single-season goals
(b) 4th in NCAA career goals


== Records and awards ==

Gait set many NLL scoring records during his career.
Gait was named NLL MVP six times, including five consecutive seasons. Other than Gary Gait, only John Tavares (3 times) has ever won the award more than once.
He was also given the NLL Sportsmanship Award twice, in 2004 (tie with Peter Lough) and 2005.
NLL Weekly and Monthly Awards:
Player of the Week (1994–2001) – 7 times
Overall Player of the Week (2002–present) – 6 times
Offensive Player of the Week (2002–present) – 3 times
Player of the Month – 6 times
Gait was named the MLL MVP his final season in 2005, sharing it with Mark Millon. That season he also led the league in goals and points with 42 goals and 21 assists for 63 points.
MLL Weekly Awards:
Offensive Player of the Week – 2 times
Gary along with his twin brother Paul Gait will be inducted into their home province's highest honour in 2011. The BC Sports Hall of Fame will place the legendary brothers into its Hall on September 13, 2011 in a ceremony in Vancouver.


== Personal life ==
Gary Gait lives in Fayetteville, New York with his wife, Nicole, and their children Taylor and Braedon. Braedon plays lacrosse at Christian Brothers Academy in Syracuse, New York, and Taylor plays for her father at Syracuse.


== See also ==
Career achievements of Gary Gait
Syracuse Orange men's lacrosse


== References ==