Make the Grade is a children's game show that aired from October 2, 1989 through December 29, 1991 on Nickelodeon.


== Broadcast history ==
Make the Grade premiered on Nickelodeon on October 2, 1989, with three seasons worth of first-run episodes airing until 1990, and was reran until December 29, 1991. Reruns later aired on Nickelodeon Games and Sports from January 2, 2000 to April 2, 2004.
The first two seasons were hosted by Lew Schneider, and were taped in a small New York studio with no live audience and false crowd noise. For the third season, the show moved to the newly opened Nickelodeon Studios in Orlando, Florida, with Robb Edward Morris taking Schneider's place as host. New York-based disc jockey Maria Milito was the announcer for the entire run.
The third season with Robb Edward Morris shot over 55 episodes in June 1990, shortly after the opening of Nickelodeon Studios and Universal Studios Florida.


== Main game ==
Three contestants – each situated at either a red, green, or blue desk – competed to answer trivia questions and acquire squares on a 7x7 split-flap game board. The category icons and grade levels lit up on the front of each desk when a question was answered correctly. Grade levels, which ranged from elementary school and grades 7 through 12, ran along the top of the board; six subjects plus a "special elective" ran down the left. The contestants' goal was to answer enough questions to light every category and grade level on the desk.
In the first season, each episode had a different set of all seven categories. In the second and third seasons, the last category was a "Special Elective", which was represented by a checkmark.
Most squares contained questions. If a contestant answered the question correctly, he earned that square for his desk and control of the board. If incorrect, the other two had a chance to answer once the host had re-read the question. If no one answered correctly, the square turned black and could not be selected again, which Schneider referred to as a "dead square". Additionally, several squares contained "wild card" panels that could ultimately alter the outcome of the game. The wild cards were:
Take: Allows a contestant to steal any square from an opponent.
Lose: Forces a contestant to give up a square of his/her choice, which would be placed back on the board as another question or wild card.
Free: Gives the square to the contestant who picked it without having to answer a question.
Fire: Leads to a "Fire Drill," a physical challenge for all three contestants.


=== Fire drills ===
Like other Nickelodeon game shows before it, Make the Grade allowed contestants to participate in (sometimes messy) challenge stunts called "Fire Drills." Fire Drills took place when a contestant selected a square with the Fire wild card. All three contestants participate.
The goal of each Fire Drill was to complete the challenge first, thereby earning first choice at the three desks. When contestants answered questions correctly, the squares they earned belonged to the desks at which they were seated, rather than the contestants themselves. Once the Fire Drill was completed, the first place contestant picked whichever desk he or she desired, usually the one with the most grade levels and subjects completed. The second place contestant got his/her choice of the remaining two desks, and third place took the last desk left.
Because of this structure, a contestant could do poorly in answering questions but successfully complete Fire Drills to win the game. In extreme situations, a contestant completed their cards and won the game with one correct answer. While it was theoretically possible to win a game without answering any questions (by winning a fire drill, being only one square away from victory, and picking a "free" or "take" square), this never happened.
After two trivia rounds, the first contestant to light up 14 squares on their desk, or the contestant with the most squares in as many grade levels and subjects as possible, won $500 and goes on to the Honors Round. In the event of a tie for most lights lit up at the end of the game, the player who has the most squares in their current color on the category board wins. The other two contestants received $50 and a consolation prize, and all contestants were given British Knights sneakers to take home.


== Honors round ==
In this round, the winner is offered their choice of three question categories. Each category contains questions from each of the seven subjects in the main game. The contestant has 45 seconds to answer one question correctly from all seven subjects.


=== First season ===
Each subject contained only one question and the contestant was only able to give one answer to each question. Each correct answer won $100, and getting all seven questions right augmented the bonus round total to $1,000.


=== Second and third seasons ===
The bonus round was played as before, but missing or passing a question moved to the next subject, and a contestant was able to return to the subject missed with new questions if time permitted. Each of the first six correct answers won $100, and the seventh correct answer won a trip to Universal Studios Florida.


==== University round ====
In second season episodes where the game finished early (and there was additional time to fill), a special University Round was played. A series of five questions were asked, for $50, $100, $200, $500, and $1,000, respectively. The contestant could stop and take the money at any time. Any cash and prizes won in the earlier rounds was safe and never risked, so any cash won in the University Round was added to the winnings from the earlier rounds.
In the first season, extra time was filled with clips of host Schneider going to malls and asking questions, and during the third season, studio audience members were asked questions to win T-shirts and other small prizes. On a few episodes, a contestant won the game so early that they started another game with a second set of contestants, playing the second game in abbreviated time.


== References ==


== External links ==
Nickelodeon Games & Sports
Make the Grade at IMDb
Classic Nick Online
Make The Grade Rule Page
TV.com Page