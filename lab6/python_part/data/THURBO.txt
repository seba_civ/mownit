THURBO is a railway company in eastern Switzerland, jointly owned by Swiss Federal Railways (90%) and the canton of Thurgau.


== Description ==
THURBO was founded in September 2001 by Swiss Federal Railways (SBB-CFF-FFS) and Mittelthurgau-Bahn (MThB). Its main business is regional passenger traffic. Infrastructure Kreuzlingen - Weinfelden - Wil SG, taken over from Mittelthurgau-Bahn, is operated by THURBO but maintained by SBB.
Several ZVV lines of S-Bahn Zürich, for example S26 in the cantons of Zürich and St. Gallen between Winterthur and Rüti ZH respectively Rapperswil, are also operated by THURBO.


== Operation ==
Wil SG – Weinfelden – Kreuzlingen (– Konstanz) (formerly Mittelthurgaubahn)
S-Bahn St. Gallen
S 1 Wil SG – St. Gallen – Heerbrugg – Altstätten
S 2 Herisau – St. Gallen – Heerbrugg
S 3 St. Gallen Haggen – St. Gallen – Romanshorn – Kreuzlingen – Schaffhausen (Seelinie)
S 5 St. Gallen – Bischofszell – Weinfelden
S 6 St. Gallen – St. Gallen Haggen – Herisau
S 7 Rorschach – Romanshorn – Weinfelden
S 8 Schaffhausen – Kreuzlingen – Romanshorn – Rorschach
S 9 Wil SG – Wattwil
Buchs SG – Sargans
S-Bahn Zürich
S 22 Bülach – Schaffhausen – Singen (Hohentwiel)
S 26 Winterthur - Bauma - Rüti ZH
S 29 Winterthur HB – Stein am Rhein
S 30 Winterthur HB – Frauenfeld – Weinfelden (– Romanshorn – Rorschach)
S 33 Winterthur HB – Andelfingen – Schaffhausen
S 35 Winterthur HB – Wil SG
S 41 Winterthur HB – Bülach (– Bad Zurzach – Waldshut )


== Rolling stock ==

THURBO's fleet consists of
41 GTW 2/6 EMUs (RABe 526 701–751)
39 GTW 2/8 EMUs (RABe 526 752–790; 781–790 ex 709–718)
12 GTW 2/8 EMUs on order (RABe 526 791–802)
10 GTW 2/6 EMUs (RABe 526 680–689)


== External links ==
Website of Thurbo AG


== References ==