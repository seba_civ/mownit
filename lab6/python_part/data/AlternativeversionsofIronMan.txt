In addition to his mainstream incarnation, Iron Man has been depicted in other fictional universes.


== Modern continuityEdit ==
Anthony Edward "Tony" Stark - The first Iron Man, Tony Stark a wealthy industrialist and genius inventor creates the powered suit of armor to save his life after receiving a mortal wound by a devastating weapon.
James Rupert "Rhodey" Rhodes - After Stark loses his fortune to Obadiah Stane and regresses into alcoholism, long-time friend, confidant, and pilot "Rhodey" Rhodes assumes the mantle of Iron Man. Rhodes relinquishes the role to Stark after being injured by Stane, but resumes the role of Iron Man after Stark's purported death.


== 1602Edit ==
1602: New World features a 17th-century Spanish Iron Man named Lord Iron. Taken captive in the Holy Land during the English / Spanish war, he was forced to make weapons for them by way of weeks of torture by David Banner, the later Hulk of that world. He needs his massive armor to survive. The armor is powered by "lightning bottles" and provides him with super-strength and invulnerability as well as several electricity-powered weapons.
With his Moorish associate, Rhodes, Lord Iron is assigned by King James to put an end to the traitors and witchbreed in the New World. Instead, he realizes he has let bitterness consume him, and makes his peace with Banner. He is last seen using his armor to power the colony's printing press.


== 2020Edit ==
Machine Man 2020 features Arno Stark in the red and gold armor, acting as a mercenary in the employ of Sunset Bain. Arno travels to the past in an effort to save his family from a madman's bomb. He needs the retinal patterns of the bomb maker (who was killed in Stark's time) so he travels to the past to get them. He runs into the original Blizzard who mistakes him for Tony Stark only to be killed (as Arno didn't have time for a prolonged fight). While attempting to get the retinal patterns he fights with Spider-Man only to be pulled back to his time and find everything he cared about destroyed.


== 2093Edit ==
Tony Stark and Doctor Doom are brought to the year 2093 by Merlin to stop a plot by a primarily robotic Doom and the Iron Man of 2093, Andros Stark. Andros is a psychotic madman and utilizes his grandfather Arno's armor. Tony ultimately defeats Andros while wielding the legendary sword Excalibur.


== Secret Wars (2015) 2099Edit ==
In the Marvel 2099 line, the new Iron Man of that era is a man named Sonny Frisco. Despite piloting a normal-sized suit of Iron Man armor, Frisco actually suffers from dwarfism. He is a member of the Alchemax corporation's team of Avengers, and secretly utilizes the help of Vision, a woman with precognitive abilities. 


== 3030Edit ==
The Iron Man of 3030 is Tony Stark's biracial granddaughter, Rhodey Stark (named after Stark's close friend James Rhodes). She travels to the present in order to help the Avengers save Earth from a rogue planet that had been fired from the future, and departs after warning her grandfather that his life is in danger.


== Adam WarlockEdit ==
In Adam Warlock #2 (1972), Peter Parker's counterpart on Counter-Earth is mentioned: "the heart of Tony Stark beats unscathed".


== Age of ApocalypseEdit ==
In the Age of Apocalypse, Tony Stark is an agent of the Human High Council. The injury that compromised his heart is caused by the attack of a mutant.


== Age of XEdit ==

Officially code named Iron Man, he prefers the name Steel Corpse. Iron Man was infected by a disease, thought to be of mutant origin, that bonded him permanently to his armor. Not only can he never remove the armor, the disease is causing the armor to slowly consume his flesh, meaning that one day Tony Stark will cease to exist and only the armor will be left. He works with this reality's version of the Avengers to exterminate all mutants, but eventually rebelled against his purpose when a 'Trojan horse' in the armor nearly drove him to kill innocent mutant children, forcing his teammates to kill him.


== AvataarsEdit ==
In the sword and sorcery world of the Avataars: Covenant of the Shield miniseries, Iron Man's counterpart is Ironheart, one of the Champions of the Realm. A powerful warrior, he wears a huge suit of grey armor.


== Bullet PointsEdit ==
In Bullet Points, Iron Man is Steve Rogers, who, following the assassination of Dr. Abraham Erskine and the cancellation of the 'Captain America' project, agrees to be bonded to the prototype 'Iron Man' armor despite the intense physical pain and discomfort this will cause. Rogers is later killed fighting an alternative version of the Hulk; following this, Tony Stark, who has been recruited into S.H.I.E.L.D. expresses a desire to continue in Rogers' footsteps as Iron Man, but is rejected owing to a heart condition. He later disobeys this command and adopts the mantle upon the arrival of Galactus.


== Earth XEdit ==
In the alternative reality of Earth X, Tony Stark builds a headquarters that protects himself from a plague that grants all humans superpowers. Afterwards, he builds the Iron Avengers. Later, his headquarters is revealed to be a giant armor in and of itself, based on the old Godzilla fighting mecha, the Red Ronin, which he uses to delay the Celestial attack until the coming of Galactus, sacrificing his life in the process. In Paradise X, he becomes part of the angelic Avenging Host of Mar-well's "Paradise", with an Iron Man motif.


== Earth-691Edit ==
In the continuity of Earth-691, Tony Stark is devastated by the horrors of the Martian invasion and jettisons his technology into space. It is found by a primitive alien race who use it to become an interstellar menace calling themselves the Stark, who subsequently clash with the Guardians of the Galaxy in the 31st century.


== Earth-2122Edit ==
In the continuity of Earth-2122, the home of Crusader X, where the British won the American Revolution and still control North America, Anthony Stark is a member of a group called the Sons of Liberty. In this reality, Stark is willing to kill innocent people.


== Earth-3490Edit ==
In the continuity of Earth-3490, Tony Stark was born a woman (Natasha Stark) rather than a man; Stark's superhero alter-ego in this universe is Iron Woman. The Civil War between superheroes in Earth-3490 was averted due to the fact that Stark and Steve Rogers (Captain America) are romantically involved, and have since married.


== ExilesEdit ==
In Exiles, an villainous alternative Iron Man of Earth-2020 is a member of Weapon X, the more ruthless team of reality fixers. After ending up at the 'Crystal Palace' (the Exiles' headquarters) and fighting them there, he is eventually exposed and sent back to his own timeline where he is arrested by the Army for starting a world war.
In one alternative reality Tony Stark has become the absolute ruler of the entire planet Earth, and kills many of that Earth's heroes and mutants. Weapon X arrives on this reality to help him conquer Attilan, though their true purpose is to cause his downfall. Tony is eventually killed by Susan Storm.
In one alternative reality he is partners with both Mr. Fantastic and Doctor Octopus before he created the Iron Man suit.
On an alternative world devastated by the Hulk's Annihilation Wave. Iron Man was one of those killed in the attack. When the Exiles arranged for the dead heroes to be replaced by alternatives, Iron Man's replacement was a version of Spitfire, on the grounds that they have never got along with any alternative Tony Starks.
The Sons of Iron are a group of armor-wearing warriors from an Earth shared by humans and reptilian humanoids. Because they are completely concealed by the Iron Man armor, no-one can tell which they are.


== Fantastic Four: The EndEdit ==
In the miniseries Fantastic Four: The End, which is set in a future where Reed Richards' technology has launched humanity into a golden age without poverty, hunger, disease or war and where mankind has colonized the entire Solar System, awaiting membership in the intergalactic federation while in quarantine from the rest of the universe, Tony Stark has died long ago - but his consciousness survives, 'hopping' from artificial body to artificial body. Most of the bodies shown in the mini resemble Iron Man armors, often being identical to existing armors. One notable exception was the bulky, stocky space-armor which played an important role in the battle of humanity's heroes versus several alien armadas.


== House of MEdit ==
Born to Howard and Maria Stark, the heads of the powerful business conglomerate Stark Industries, Tony Stark grew to be an imaginative and brilliant inventor. He worked side by side with his father from an early age, and surpassed his father's technical brilliance by the age of 16. After some poor business decisions, Tony's father retired from the business and left Tony in charge to do as he saw fit.
With Tony's brilliant tech ideas becoming the principal product of Stark Industries, the business boomed. Stark became the key supplier of hi-tech weaponry used to fight mutants across the planet. While the tech was not always successful, Stark was on the cutting edge of science. Stark was on the verge of a technological breakthrough when the Mutant-Human war came to an end.
Stark had designed an armor suit that could possibly counter the strongest mutants on the planet. In the years that followed, those suits were powered down to become part of a game called Robo Death Match, a television sport with giant robots fighting each other. Tony and his father were avid competitors and were two of the best in the sport.
Stark Industries scored its biggest victory when it secured the Sentinel production contracts, pushing major competitor, Jason Wyngarde, out of business. This contract was the largest in the world and would ensure the existence of Stark Industries for years to come. Erik Magnus and Sebastian Shaw awarded Tony the contract under the condition that he would hire Beast and Forge as observers. Tony agreed, and McCoy became a key contributor along with Doctor Pym on The Vision project.
Tony secretly worked on a special project beneath Stark Industries: a brand new suit of hi-tech armor he planned to use as his new Robo Death Match suit.


== Iron Man: The EndEdit ==
In the one-shot Iron Man: The End, an aging Tony Stark works on his greatest creation, a space elevator called "Big Jump." Stark faces retirement due to age and the physical toll of an illness, no longer allowing him to run his business "Stark Universal" and continue to be Iron Man. This leads to the need to groom a replacement.


== Iron ManiacEdit ==
Iron Maniac is an evil alternative universe version of Iron Man from Earth-5012. He first appeared in Marvel Team-Up (vol. 3) #2, wearing armor that bears heavy resemblance to that of Doctor Doom (and is, because of that, mistaken for that villain).
He comes from an alternative reality where most of the Avengers were killed when they encountered the vicious alien Titannus in space. While the team is rescued by the reserve Avengers five years later, it takes another five years to fight back the Trellions - the alien race that has brainwashed Titannus - and, during that time, an apparently power-hungry Reed Richards turns his back on the surviving heroes (the exact circumstances behind this are unknown). Scarred for life due to an attack from the Human Torch shortly before the aforementioned hero is killed, Iron Man sets his own operation base in Latveria for the express purpose of "take over the world to save it from Richards," until Richards somehow manages to banish him into Earth-616 (For some reason, emphasis is placed by the alternative Iron Man on the fact that Richards got rid of him without killing him, although why this would be necessary is unknown). Other differences between his world and the one we know include that there is no Spider-Man - or, at least, he and Iron Man have never met - and that Hank Pym is another version of the Hulk.
After being transported to Earth-616, the alternative Iron Man fights the Fantastic Four and Doctor Strange, all of whom initially assumed him to be their Doctor Doom having escaped from Hell. However, after he unmasks himself, they learn his true identity, shortly before he manages to temporarily negate the FF's powers and escape, concluding that he has no reason to trust that they will not turn on him like the FF of his world did. Capturing a recently discovered mutant, the alternative Iron Man attempts to return to his home dimension by using the mutant as a power source, but is attacked by Spider-Man and X-23 as they investigate the situation. After the appearance of Captain America and Black Widow, he realizes that he is in an alternative world, but continues to fight the heroes, calling them all 'Richards' lackeys'. He is defeated thanks to Spider-Man and X-23's use of their own version of the fastball special to destroy his equipment, shortly after 'warning' the other heroes of the Titannus War (by saying that he would not kill them now because it would be a kindness).
While the alternative Tony Stark is kept locked up and drugged in the S.H.I.E.L.D. helicarrier, he is briefly visited by his counterpart in this universe, although he is unaware of the visit. Shortly after the alternative Stark is transferred to a conventional cell, Titannus soon arrives and fights the heroes, this time confronting a new group of Defenders assembled by Doctor Strange. When Titannus' comatose lover is revived, she tells him that she never loved him and that he was insane, causing Titannus to kill himself. The alternative Iron Man later discovers from Spider-Man and Wolverine that the Avengers were never massacred in space in this reality because the group had been disassembled, thus never encountering Titannus and averting the so-called 'Titannus War'.
He subsequently broke free from captivity, having immunized himself to the gas that was used to keep him sedated on board the S.H.I.E.L.D. Helicarrier. In the process, he gained the unwilling alliance of the LMD Diamondback. Having convinced her that he is the "real" Tony, the AU Tony Stark erased her memories, reshaping the former LMD into an advanced suit of armor. This armor, even more advanced than the pre-Extremis suit Iron Man wore at that time, was able to replicate any weapon from the wearer's memory. He subsequently battled Spider-Man, Wolverine, Captain America and Luke Cage, but was only defeated after the sacrifice of rookie hero Freedom Ring, who kept Iron Maniac occupied long enough for Captain America to knock him out with a shield thrown at the back of his neck.
The name Iron Maniac is what he decided to call himself, due to being the "sole survivor of a sane world living in a backwards, insane world".
Iron Maniac is known to be at least partially cyberized, with armor plating implanted in his chest (revealed during his escape from the Helicarrier, when he is shot). It is unknown whether the rest of his body is similarly armored or if he possesses other cybernetic enhancements.
He has been briefly mentioned as being held in a S.H.I.E.L.D. Helicarrier recently in The Irredeemable Ant-Man.


== Iron Man NoirEdit ==
In Iron Man Noir, Tony Stark is an industrialist in the 1930s. He is also an adventurer, whose exploits are recorded in Marvels: A Magazine Of Men's Adventure. He is initially accompanied by his associate James Rhodes, his personal assistant Giulietta Nefaria and his biographer Vergil Munsey. When Nefaria is revealed as working with the Nazis (specifically Baron Zemo and Baron von Strucker) and Vergil is killed, their role in the story is taken by Stark's new biographer Pepper Potts. His heart having been damaged on an earlier adventure, Stark keeps it going with repulsor technology installed and recharged by Stark Industries engineer Edwin Jarvis.
While investigating a mysterious power source in the ruins of Atlantis, Pepper Potts gets kidnapped by the Nazis and taken to their stronghold in Norway. To rescue her, Stark and Rhodes don suits of bulky power armour built by Jarvis, but are shocked to discover that 'Baron Zemo' is actually Tony's missing father Howard Stark, brainwashed by a unique chemical compound to serve the Nazis. Despite his repleted power supply, Tony manages to destroy the various suits of armor that Zemo had built for the Nazis, concluding that his father had died long ago, before returning to the USA.


== Inter-company crossoversEdit ==
In Marvel and DC's Amalgam Comics, Stark is merged with the Green Lantern Hal Jordan into the Iron Lantern. "Hal Stark" wears a suit resembling a green Iron Man armor.
In the miniseries JLA/Avengers, Iron Man aids the Avengers in the battle against Starro the Conqueror. Afterwards, he creates a dimensional alarm in order to tell when invaders from another dimension come into their universe. After a brief scuffle with the JLA in the Savage Land, the Avengers are confronted by Metron, who gives Tony a Mother Box. Using this, Tony is able to get the Avengers to Metropolis, where the Avengers confront the JLA again. The Avengers escape, but Tony and Hawkeye manage to take Green Lantern's Power Battery before they leave, with Tony able to stop the Flash in his tracks. The two later take down Captain Atom and Green Arrow in order to collect the Casket of Ancient Winters. Tony then leaves and arrives to save Photon and Quasar from Wonder Woman and Green Lantern, allowing them to take the Spear of Destiny. After the battle in the Savage Land, Tony is one of the Avengers and is clueless as to the dimensional shifts that are happening around him. After Cap and Superman attack each other, Tony ends up in Metropolis. When the two worlds are briefly corrected by the Grandmaster, Tony is shown his true future with his alcoholism and his defeat by Obadiah Stane. Accepting this, he aids the JLA and the Avengers in the final battle and helped build the ship that took them to Krona's base. During the battle, he teams up with Kyle Rayner to create a weapon to use against their enemies and the two are shown to be impressed by one another, Kyle expressing his awe at Tony's engineering prowess and Tony asking Kyle where he could get a Green Lantern ring.


== MangaverseEdit ==
In the Marvel Mangaverse reality, Tony Stark creates the original armor together with Dr. Ho Yinsen and acts as Iron Man for a time, but eventually vanishes after a battle with Namor, the Submariner. He is succeeded by Antoinette (Toni) Stark, his twin sister, a former agent of SHIELD, who turns Iron Man into a massive operation - a veritable army of Iron Men in many forms, with herself as Iron Woman. After she dies in battle against the Hulk, Tony Stark reveals himself again; he has gone underground after spinal cancer reduced him to a disembodied head hooked up to a life support system. However, he has designed a new armor, and a body that he can integrate with.
He also had designed four massive vehicles for the Avengers of his world to use, which could combine (in a manner resembling old fashioned combining super robots like Combattler V and Voltes V) into a skyscraper-sized Iron Man-mecha (Dubbed Ultimate Iron Man in its first appearance, then the Iron Avenger in its battle with the Hulk and finally simply called "the Avenger's mecha" in Volume 2 of the series). Unfortunately it was quickly destroyed by that world's Hulk. Apparently, however, it was rebuilt again by the time of the second volume, this time as a single robotic unit without transformation (or, if it was capable of transformation, it was never demonstrated). This unit helped fight off the giant Galactus spores, but was later destroyed, along with most of the Avengers, single handedly by the Mangaverse version of Dr. Doom.


== Marvel AdventuresEdit ==
The Marvel Adventures Iron Man is very similar to the Earth-616 Iron Man, but with some changes. Instead of suffering damage to his heart due to a booby trap in Vietnam, Tony Stark's heart was damaged when an experimental plane he was flying was brought down by AIM. AIM wanted Stark to build weapons and devices for them. Dr. Gia-Bao Yinsen aided Tony in escaping AIM, but Yinsen died saving his country from AIM. Iron Man does not seem to have problems with alcoholism, since the Marvel Adventures is aimed at a younger demographic. Iron Man's armor resembles his Extremis armor although Iron Man has other armors that fit over his regular armor, as in the case of his underwater armor.


== Marvel ApesEdit ==
The version of Iron Man appearing in the Marvel Apes mini-series is a mandrill, appropriately being named the Iron Mandrill. He is a member of the Apevengers. At one point, he is attacked by the zombified Wasp of the Marvel Zombies universe and infected, though he is later apparently cured when these events are undone via time travel.


== Marvel ZombiesEdit ==


=== The first seriesEdit ===
In the Marvel Zombies universe, Tony Stark has been infected by the zombie virus. Alongside a horde of starving undead superhuman zombies, Iron Man attacks the Silver Surfer. The attack is successful, but one of the Surfer's energy bolts hits Iron Man's lower torso, cutting him in half. The zombie "survives" this wound and later gains cosmic powers (including flight) by eating part of the Surfer's corpse. When Galactus arrives, Iron Man and the five other surviving zombies devour him. They are able to absorb Galactus' power, and call themselves "The Galacti".


=== Marvel Zombies 2Edit ===
He also appears in Marvel Zombies 2, one of the small group of super-powered zombies that have eaten their way across all known space. Here Stark has had his entire lost lower body replaced with cybernetics. He also appears to have forgotten he had some design in the machine which opened a link to the Ultimate Universe. He was shocked to see Forge, one of the surviving X-Men, wearing his Mark I armored suit. The zombified Hulk kills Iron Man when he stomps through the armor, forcing Tony Stark's flesh and blood through any openings left in the armor. However Iron Man had recently revived next issue, but only as a cameo, on Marvel Zombies 3.


=== Marvel Zombies ReturnEdit ===
In the final issue of Marvel Zombies 2, the remaining zombies are transported to another universe. At the point where the zombies reach this new reality, the period is nearly identical to the one where Tony Stark was an alcoholic. Zombie Giant Man infects Happy Hogan, Pepper Potts and a number of other people at Stark Industries. A drunken Tony Stark lacks the will power to become Iron Man despite Pepper Potts' requests, so James Rhodes dons the suit to save him. Crucial to fate of the multiverse are the nanites that Stark has accidentally created, which destroy damaged flesh and tissue as a cure for cancer, and prove to be a potent weapon against the zombies. This was grafted onto Flint Marko's body. He sacrifices himself to kill several zombies in Stark Tower, with Rhodes permanently succeeding him in the role of Iron Man. His nanites are then used by his successor, now a member of the New Avengers, years later to kill the remaining super-powered zombies and end the inter-dimensional zombie threat.


== MC2Edit ==
In the alternative future of MC2, Tony Stark retires after the loss of many heroes in battle, but eventually creates the armored computer program Mainframe, which joins the next generation of Avengers.


== Mini MarvelsEdit ==
Iron Man is a recurring character in "Mini Marvels". He appears in story arcs like "The Armored Avengers" & "World War Hulk". He is portrayed as conceited and thinks himself the best of the team. He has a friendly rivalry with Hawkeye.


== NewuniversalEdit ==
In the alternative world of newuniversal, Tony Stark is one of three humans altered by the Fireworks on April 26, 1953, gaining abilities associated with the Cipher glyph. Prior to the Fireworks, Stark is unexceptional, but he then becomes a technological genius. His discoveries revitalise his father's company, Stark Industries, and are "five years ahead of everything everyone else is working on". There are suggestions that he is capable of more, but is not making all of his discoveries public.
Stark's transformation is noticed by the National Security Agency's Project Spitfire, which is discreetly monitoring the superhumans created by the Fireworks. In March 1959, Stark's plane crashes in North Vietnam and he is imprisoned. He escapes by constructing an Iron Man suit from "spare parts" and flying out of the country.
On April 4, 1959, when he returns to the USA, the NSA takes Stark to a San Diego naval base, ostensibly to debrief him. Stark is then shot dead by Philip L. Voight, a Project Spitfire agent, to prevent him from making contact with the other superhumans.
The Iron Man suit is seized by Project Spitfire and reverse engineered by Doctor Joe Swann, eventually becoming the basis of the project's H.E.X suit, an exoskeleton designed for combat with superhumans.


== RuinsEdit ==
In the two issue Warren Ellis series Ruins, Tony Stark is a rich industrialist who supplied weapons for the US military in an attempt to win the Vietnam War. This version of Iron Man was injured while mediating between US forces and pro-secessionist Californians by a piece of shrapnel thrown by the National Guard. This embittered Stark who formed a revolutionary cell named the Avengers. This version of Iron Man was betrayed by Scarlet Witch who provided the United States military information to crush the Avengers. Tony Stark is presumably killed.


== Spider-VerseEdit ==
In the Amazing Spider-man comic's event Spider-Verse, Spider-Woman (Jessica Drew), Scarlet Spider (Kaine) and Spider-man (Ben Reily) meet and fight a clone of Tony Stark (Earth-802) as Iron Man serving one of the Inheritors, Jennix.


== Spider-IslandEdit ==
In a Secret Wars Warzone version of Spider-Island, he is mutated into one of the Spider Queen's spider minions and battles Agent Venom and the now monstrous Avengers. Venom sprays Tony with Norman Osborn's Green Goblin formula, freeing Tony from the Queen, but slowly making him insane. He modifies Norman's armor and becomes the Iron Goblin to aid the resistance. When they are surrounded by a number of spider creatures (including Giant Man) with few means of escape, Tony sacrifices himself using the Black Knight's Ebony Blade so he can aid them and die before the Goblin formula completely takes him over.


== Spider-GwenEdit ==
In Spider-Gwen Tony Stark is an arms dealer, the owner of global private military company WAR MACHINE, and owner of the coffee chain StarkBucks.


== Ultimate MarvelEdit ==
See also: Ultimate Iron Man and Ultimate Iron Man (character).


== What if: Newer Fantastic FourEdit ==
In the timeline of What if: Newer Fantastic Four, in which the Fantastic Four were killed by De'Lila (a rogue Skrull) and the Hulk, Spider-Man, the Ghost Rider and Wolverine had joined together to avenge them and soldier on in their stead as the New Fantastic Four, Thanos of Titan, as in the mainline universe, came into possession of the six Infinity Gems and became ruler of all reality, before erasing half of all living beings from existence. Among those who vanished was Ghost Rider, and, being present at the battle during which he was erased, Iron Man in turn stepped up to fill his place.
Taking on Thanos, the Newer Fantastic Four soon realized they were outmatched to the Nth degree - but Tony Stark, with help from the Hulk, managed to salvage the empty armor of Ziran, a Celestial, and soon realized it could be controlled by thoughts. With its original wearer gone, it now responded to him, and instantly, Tony took control of the immense armor. Using the late Mr. Fantastic's technology, he connected the armor to the Negative Zone, allowing him to call on all the power of that reality. Despite now being in control of quite possibly the most powerful armor any Iron Man has ever worn in any reality, he was swiftly defeated by the omnipotent Thanos, although Wolverine was able to trick Thanos into a position where he could remove the Gauntlet, Spider-Man subsequently using the Gauntlet to undo the damage Thanos had caused.


== What If: Iron Man: Demon in an ArmorEdit ==
In What If: Iron Man: Demon in an Armor, Tony Stark is Doom's college roommate rather than Reed Richards, inspiring Doom to develop a machine that allows him to transfer his mind into Stark's body while leaving Stark trapped in Doom's body with no memory of his past. While Doom uses Stark's connections and company to establish himself, the amnesic Stark- believing himself to be Doom- works to rebuild his life, creating his own company and forming his own reputation from the ground up. This culminates in a confrontation between the two wearing early versions of their respective armours- Doom having developed a green-and-silver Iron Man armour while Stark has created Doom's costume with gold and a red cloak-, during which Doom reveals the truth about their switch, only for Stark to reject the offer to switch back because Doom has destroyed the name of Tony Stark while Doctor von Doom has developed an honorable reputation.


== Realm of KingsEdit ==
In this one-shot, Quasar, the newly resurrected Protector of the Universe travels into the Fault, the immense tear which has appeared in the fabric of spacetime itself after the catastrophic battle between Vulcan and Black Bolt. Reaching what he perceives to be the other end of the tunnel that is the Fault, he arrives in another universe... a dark, twisted universe, the `corpse of a universe´, possessed by Lovecraftian horrors which are worshipped by all the denizens of that universe, including Earth's mightiest heroes. Iron Man is never seen outside his armor, but he, like the others, serves the "Many-angled ones" with total devotion.


== X-Men ForeverEdit ==
In this alternative universe of X-Men Forever, Tony Stark, while still publicly the super-hero Iron Man, is also the head of the shadowy organization known as The Consortium, which has been seen to kidnap mutants to use in experimentation to find the cause of the so-called "Burnout" syndrome that causes mutants to die early. However, as part of his efforts to undermine the Consortium's anti-mutant agenda, he assists Storm- who has been split into an amnesic child version of herself and an energy form with full memory and no body- by providing her energy self with a suit based on the now-deceased Black Panther so that her energy can maintain corporeal form, although he is subsequently killed by a twisted clone of Storm before he can reveal her existence to anyone else.


== ReferencesEdit ==