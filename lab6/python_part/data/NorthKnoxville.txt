North Knoxville is the section of Knoxville, Tennessee, USA, that lies north of the city's downtown area. It is concentrated around Broadway (US-441), Clinton Highway (US-25W), Tazewell Pike (TN-331), Washington Pike, and adjacent roads, and includes the neighborhoods of Fountain City, Inskip-Norwood, Oakwood-Lincoln Park, Old North Knoxville, Fourth and Gill, North Hills, and Whittle Springs. North Knoxville is bisected by Sharp's Ridge, a 7-mile (11 km) elongate ridge that rises prominently above the surrounding terrain.
"North Knoxville" originally referred to the area between Downtown Knoxville and Sharp's Ridge. A portion of this area, namely the Old North Knoxville and Fourth and Gill neighborhoods, incorporated as the City of North Knoxville in 1888, though this city was annexed by Knoxville in 1897. Lincoln Park and Oakwood, which developed alongside the Southern Railroad's Coster Yards, were annexed in 1917, pushing the city's boundaries to the base of Sharp's Ridge.
The residential development of North Knoxville began with the advent of streetcars in the 1880s, and the establishment of the so-called "Dummy Line," a train connecting Knoxville with Fountain City, in 1890. This train and the trolley that replaced it in 1905 ran along Broadway. Fountain City and other areas north of Sharp's Ridge were annexed in 1962, and the area south of Sharp's Ridge, namely the Old North Knoxville, Fourth and Gill, and Oakwood-Lincoln Park neighborhoods, are now typically referred to as "old" North Knoxville, or "Downtown North."
Recent economic initiatives have focused on rezoning commercial or industrial areas as "mixed-use" areas or low-density residential areas, improving sidewalks and greenways, and improving (i.e., widening or adding turn lanes) important roads.


== See also ==
Corryton
Halls Crossroads
Karns
Powell


== References ==


== External links ==
North City Sector Plan — Knoxville-Knox County Metropolitan Planning Commission report
Central City Sector Plan — Knoxville-Knox County Metropolitan Planning Commission report
North Knoxville Business and Professional Association