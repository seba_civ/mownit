Lela Margaret Cole Kitson (May 25, 1891 in Hill City, South Dakota – November 25, 1970 in El Paso, Texas) was a freelance writer of primarily western romances from 1920 to 1955.


== Newspapers ==
Later she worked in the newspaper and radio industries in El Paso, Texas. Besides her own name, Kitson wrote under the pen names Lupe Loya, Elsie Kay, Cole Kitson, Shirley Manners and Susan Cole. Her newspaper career included: The Tucson Star 1912–14; The Los Angeles Times 1922–? as feature writer; The El Paso Herald Post 1943–50 as Woman's Page Editor and the Hudsbeth County News 1964–1970 as "The Observer", a political commentary column.


== History ==
She was born to David Cole of New York and Maude Carr of Wisconsin, and was the eldest and only daughter of six children. Her father's upward move in the mining plant design and reduction machinery design led the family in 1896 to Chicago, Illinois, in 1900 to Aspen, Colorado, and in 1902 to Cananea, Mexico. Higher education was not available in Cananea so she was sent to the Marlborough School for Girls located in Los Angeles, where other Cole family members resided, including her great-granduncle U.S. Senator Cornelius Cole.
Following her graduation from Marlborough and a 1911 trip to Japan with the Mining Association, she joined her family in Tucson, Arizona in 1912, working for the Tucson Star Newspaper as Society Editor. In 1914 she married Howard Waldo Kitson of New York City, a 1909 Columbia College graduate specializing in mining and geology, in Morenci, Arizona where he was employed.
Following the death of their son in late 1917, the Kitsons moved to New York City, where they boarded with his mother, silent film actress May Kitson. In early 1918 Mrs. Kitson joined the Author's League of America. For the birth of their daughter she removed to El Paso where her parents had moved in 1917. Between 1919 and the 1931 suicide of her husband Waldo in Long Beach, the Kitsons lived in Arizona and California. Whenever Waldo was out in the field, Lela and their daughter temporarily moved back to El Paso, Texas where they resided with her parents.
In 1921 Street & Smith was the first publisher of her short romantic stories written under Lupe Loya. Their publication was Western Story Magazine. Between 1927 and 1931 she also wrote under the name Elsie Kay. Lela wrote advice stories in Picture Play, also a Street & Smith publication. In 1926 The Clayton Magazines Inc. added Lela to their stable of writers. As Lupe Loya, her romantic tales were published in Ranch Romances, Western Love Stories and Ranchland Love Stories. In 1933 Warner Publishing Inc took over Street & Smith and continued publishing Lupe Loya stories in RANCH ROMANCES. More than twice her stories were featured on the magazine cover. In the 40s and 50s her pseudonym was Shirley Manners and her publisher was Standard Magazines Inc, "Thrilling Love".
Lela also wrote freelance for the Los Angeles Times and other publications. In 1943 Lela joined the El Paso Herald-Post a Scripps-Howard News publication as the Woman's Page Editor, a position she held until 1950. She then began a radio show "Woman to Woman" on KTSM El Paso Texas, a CBS affiliate. In 1964, under the pen name "The Observer", Lela wrote a political column for the Hudsbeth County News, a bi-monthly newspaper published in Dell City, Texas, which she continued until her death.


== Works (A partial list) ==
Loya, Lupe; pseudonym of Lela Cole Kitson, (1891–1970) (chron.)
Archibald Tries Life in the Open, (ss) Western Story Magazine May 6, 1922
Cactus Turns the Other Cheek, (ss) The Live Wire June 1925
Clementine, (ss) Western Story Magazine April 16, 1921
Cosey Corner Changes Cooks, (ss) Western Story Magazine February 17, 1923
Desert Vistas, (ar) Western Story Magazine July 16, 1921
The Dog That Isn’t a Dog, (ss) Western Story Magazine October 1, 1921
Has the Coon Butte Mystery Been Solved?, (ar) Western Story Magazine September 16, 1922
Joe’s Pets, (ss) Western Story Magazine November 29, 1924
Peace on Earth, (nv) Ranch Romances December 3, 1932
Puncher vs. Poet, (ss) Western Story Magazine June 24, 1922
Rio Grande Romance, (ss) Ranch Romances April 2, 1941
The Rodeo at El Paso, (ar) Western Story Magazine July 9, 1921
The Sea in the Desert, (ar) Western Story Magazine October 29, 1921
Temperamental Tillie, (ss) Western Story Magazine October 21, 1922
Uncle Caleb Dodges Matrimony, (ss) Western Story Magazine September 3, 1921
The White Chink, (ss) Western Story Magazine December 23, 1922
Will Big Game Come Back?, (ar) Western Story Magazine September 24, 1921


== References ==
Arizona Record Newspaper, Globe, Arizona May 7, 1922 "Arizona Girl Pens Unique Version of Tenderfoot Guest"
The Scripps-Howard News Company Magazine Vol 2 Number 4 January 1948, page 7 "El Paso Herald-Post, Lela Cole Kitson"
Torture Trek, Johnson, Ryerson, Barricade Books Inc., New York 1995 pages 6, 12
Street & Smith 79–89 Seventh Ave, New York City, New York
Clayton Magazines Inc 155 East 44th Street, New York City, New York
Warner Publishing Inc. 578 Madison Ave, New York City, New York
Time


== External links ==
Sample of her non-fiction writing style are available here