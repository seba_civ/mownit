Mazin Masoud Darwish Al-Kasbi commonly known as Mazin Al-Kasbi (Arabic: مازن بن مسعود الكاسبي‎; born 27 April 1993) is an Omani footballer who plays as a goalkeeper for Fanja SC.


== Club career ==
On 28 July 2013, he signed a contract with 2012–13 Oman Elite League runners-up Fanja SC. On 6 August 2014, he signed a one-year contract extension with Fanja SC.


=== Club career statistics ===


== International career ==
National Team
Mazin is part of the first team squad of the Oman national football team. He was selected for the national team for the first time in 2012. He made his first appearance for Oman on 8 November 2012 in a friendly match against Estonia. He has made appearances in the 2012 WAFF Championship, the 2014 FIFA World Cup qualification, the 2013 Gulf Cup of Nations, the 2014 WAFF Championship, the 2015 AFC Asian Cup qualification and the 2014 Gulf Cup of Nations.


== Honours ==


=== Club ===
With Fanja

Oman Professional League (0): Runner-up 2013-14
Sultan Qaboos Cup (1): 2013-14
Oman Professional League Cup (1): 2014-15
Oman Super Cup (0): Runner-up 2013, 2014


== References ==


== External links ==
Mazin AL KASBI – FIFA competition record
Mazin Al-Kasbi profile at Soccerway
Mazin Al-Kasbi - GOAL.com
Mazin Al-Kasbi - FootballDatabase.eu
Mazin Al-Kasbi - GOALZZ.com
Mazin Al-Kasbi - KOOORA
Mazin Al-Kasbi - 17th Asian Games INCHEON 2014
Mazin Al-Kasbi - ASIAN CUP Australia 2015