An infectious intracranial aneurysm (IIA, also called mycotic aneurysm) is a cerebral aneurysm that is caused by infection of the cerebral arterial wall.


== Etiology ==
Most IIAs are caused by bacterial infection, most commonly Staphylococcus aureus and Streptococcus species. In most cases the infection originates from left-sided bacterial endocarditis. Other common sources include cavernous sinus thrombosis, bacterial meningitis, poor dental hygiene and intravenous drug use.


== Signs and symptoms ==
Many patients with unruptured IIA may have no symptoms. In patients who do have symptoms these are often related to rupture of the aneurysm and to its cause. Rupture of an IIA results in subarachnoid hemorrhage, symptoms of which include headache, dizziness, seizures, altered mental status and focal neurological deficits.
In contrast to other cerebral aneurysms, large aneurysm size does not increase the chance of rupture. Small IIAs tend to have high rupture rates, while larger IIAs more commonly cause symptoms due to pressure on the surrounding brain tissue.


== Diagnosis ==
Diagnosis of IIA is based on finding an intracranial aneurysm on vascular imaging in the presence of predisposing infectious conditions. Positive bacterial cultures from blood or the infected aneurysm wall itself may confirm the diagnosis, however blood cultures are often negative. Other supporting findings include leukocytosis, an elevated erythrocyte sedimentation rate and elevated C-reactive protein in blood.


== Treatment ==
Treatment depends on whether the aneurysm is ruptured and may involve a combination of antimicrobial drugs, surgery and/or endovascular treatment.


== Prognosis ==
Mortality of IIA is high, unruptured IIA are associated with a mortality reaching 30%, while ruptured IIA has a mortality of up to 80%. IIAs caused by fungal infections have a worse prognosis than those caused by bacterial infection.


== Epidemiology ==
IIAs are uncommon, accounting for 2.6% to 6% of all intracranial aneurysms in autopsy studies.


== References ==