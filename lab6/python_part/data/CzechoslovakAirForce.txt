The Czechoslovak Air Force (Československé vojenské letectvo) was the air force branch of the military of Czechoslovakia. It was known as the Czechoslovak Army Air Force (Československé letectvo) from 1918 to 1939. In 1993, it was divided into the Czech Air Force and the Slovak Air Force.


== History ==


=== Interwar period (1918–1939) ===

For a modern nation surrounded by potentially hostile neighbors, without access to the ocean, the Czechoslovak leadership needed to build a capable air force. So was born the motto "Air is our sea". The Czechoslovak government between the wars balanced a home-grown aviation industry with licensing engine and aircraft designs from allied nations. Several major aircraft companies, and a few engine companies, thrived in Czechoslovakia during the 1930s. One well-known engine manufacturer was A. S. Walter located in Prague.

The Aero Company (Aero továrna letadel), was located in the Vysočany quarter of Prague. Its mixed construction (wood, metal, and fabric covering) and all-metal aircraft were competitive in the early 1930s; however, by 1938, only its MB.200 (a licensed Bloch design) was not totally obsolete.
The Avia Company (Avia akciová společnost pro průmysl letecký Škoda), a branch of the enormous Škoda Works (Škodovy závody) heavy machinery and military industrial enterprise, was different. Founded in 1919 in an old sugar factory in the eastern Prague suburbs of Letňany and Čakovice, Avia made entire airplanes, including motors, which were usually licensed Hispano-Suiza designs. The standard Czechoslovak pursuit plane of the late 1930s, the B-534 reached a total production of 514 units. It was one of the last biplane fighters in operational use, and also one of the best ever produced.
The state-controlled Letov (Vojenská továrna na letadla Letov) was also situated in Letňany. It employed about 1,200 workers in the late 1930s, and it manufactured the Š-328 biplane, of which over 450 were produced. The entire airframe was welded together, not bolted or riveted. The Letov factory was the only Czechoslovak plant that manufactured metal propellers.


=== Second World War (1939–1945) ===

During this time, Czechoslovakia was divided into the "Protektorat Böhmen und Mähren" (Protectorate of Bohemia and Moravia) – a rump state directly controlled by Nazi Germany – and the Slovak Republic – a German puppet state.
Many Czechoslovak pilots successfully escaped to Poland and France, where they helped to fight against the Nazi "blitzkrieg" during the Battle of France. Later, in Great Britain, the Inspectorate of the Czechoslovak Air Force was established on July 12, 1940, as a co-ordinating and liaison body. Brigadier General Dr Karel Janoušek was appointed to head the Inspectorate by the British Ministry of Defence. Janousek joined the ranks of the Royal Air Force Volunteer Reserve (RAFVR) and was given the rank of Acting Air Commodore (and the RAF eventually promoted him to Air Marshal). Czechoslovakian airmen were enlisted into the RAFVR and the formation of Czechoslovak fighter and bomber squadrons within the Royal Air Force was put in hand. Czech fighter ace Josef František became arguably the best top scoring allied fighter pilot of the Battle of Britain. Other Czech and Slovak pilots continued to fight against the Germans in the Soviet Union.
Under German rule all Czechoslovak aircraft where absorbed into the Luftwaffe – and the huge Czechoslovak manufacturing base was converted to produce German aircraft and engines. After the end of the war in late 1945 four Czechoslovak RAF squadrons, Numbers 310, 311, 312, and 313 were all dispatched to Czechoslovakia and became part of the Czechoslovak armed forces.
After the division of Czechoslovakia by Nazi Germany in 1939, Slovakia was left with a small air force (the Slovak Air Force (1939-1945)) composed primarily of Czechoslovakian combat aircraft. In 1939 this force defended Slovakia against Hungary in the Slovak–Hungarian War , and took part in the invasion of Poland in support of Germany. During the Second World War, the Slovak Air force was charged with the defence of Slovak airspace, and, after the invasion of Russia, provided air cover for Slovak forces fighting against the Soviet Union on the Eastern Front While engaged on the Eastern Front, Slovakia’s obsolete biplanes were replaced with German combat aircraft, including the Messerschmitt Bf 109. The air force was sent back to Slovakia after combat fatigue and desertion had reduced the pilots' effectiveness. Slovak air units took part in the Slovak National Uprising against Germany from late August 1944.


=== 1946–1988 ===

Towards the end of the war, General Alois Vicherek (cs:Alois Vicherek) left Britain for the Soviet Union, where he was supposed to take over command of the Czechoslovak Air Force in the USSR. However he only arrived on May 1, 1945, when the war was almost over. Vicherek was happy to serve an Eastern Bloc Czechoslovakia, and on May 29, 1945, he was appointed the Commander of the Czechoslovak Air Force. From 1955 Czechoslovakia was a member of the Warsaw Pact. Because of this, the Czechoslovak Air Force used Soviet aircraft, doctrines, and tactics. Mostly Mikoyan-Gurevich aircraft (MiGs) were bought. MiG-15, MiG-19, and MiG-21F fighters were produced under licence; in the 1970s, MiG-23MF were acquired, followed by MiG−23MLs and MiG-29s in the 1980s.
In 1951 the 1st, 2nd, and 3rd Air Defence Districts of State Territory were created, at about the same time as the creation of the 15th Fighter Air Corps. The 15th Fighter Air Corps controlled the 1st, 3rd, 5th, and 166th Fighter Air Divisions at various times; the 166th Fighter Air Division later became the 2nd Fighter Air Division. From 1964 to 1969 the 10th Air Army included the 46th Transport Air Division, of two regiments of helicopters and a transport regiment.
Reportedly from January 1976, the 7th Air Army was disbanded and replaced by the State Air Defence Command with the 2nd and 3rd Air Defence Divisions, which existed until 1990. The State Air Defence Command moved from Prague to Stará Boleslav in 1981.
In May 1987, two Czechoslovak Air Force jets were scrambled to try to bring down a Czechoslovak engineer attempting to escape his home country via a home-built ultralight aircraft. After flying about 10 miles (16 km) to the West German border, the refugee's aircraft ran out of fuel, and he landed safely in a Bavarian forest, just before the Czechoslovak fighters could intercept him.
During the 1980s and early 1990s, the Czechoslovak Air Force consisted of the state air defence command, with air defence fighters, surface to air missiles, and air defence radars, and the 10th Air Army, responsible for ground forces support. The state air defence command had 2nd Air Defence Division (Brno) with 8th Fighter Air Regiment, radars, and surface to air missiles, and the 3rd Air Defence Division (Žatec) with the 1st (České Budějovice), 5th (Dobřany), and 11th Fighter Air Regiments (Zatec), and the 71st Anti-Aircraft Missile Brigade and 185th Anti-Aircraft Missile Regiment. 8th Fighter Air Regiment was based at (Ostrava) (Mošnov) from 1959 until 1 April 1985, whereupon it relocated to Brno (Tuřany). It was equipped with the MiG-21 from 1965 to 1991. 1st Fighter Air Regiment at České Budějovice was equipped with MiG-21s from 1964, and was disbanded in 1992.
The 10th Air Army had two air divisions and a total of six regiments of fighters and attack aircraft. There were also two reconnaissance regiments, two transport regiments, three training regiments, and two helicopter regiments. In 1990 the 10th Air Army, with headquarters at Hradec Králové, comprised the 1st Fighter Air Division (HQ Bechyně, included the 9th Fighter Air Regiment at the same base until 30 June 1990), the 34th Fighter Bomber Air Division (HQ Čáslav), the 47th Reconnaissance Air Regiment (Ostrava-Mošnov), the 10th Signal Regiment, the 11th Helicopter Regiment, the 1st Composite Transport Air Regiment, and the 30th Attack Air Regiment (Pardubice Airport, with Su-25Ks). It was disbanded on 1 October 1990 and succeeded by the 1st Mixed Air Corps.
Between 1945 and 1968 the Czechoslovak Air Force operated several regiments from Hradčany airfield:
46 Bomber Division (46. letecká bombardovací divize) between 1951 and 1955.
24 Bomber Regiment (24. letecký bombardovací pluk) between 1952 and 1954.
25 Bomber Regiment (25. letecký bombardovací pluk) between 1952 and 1954.
17 Fighter Regiment (17. stíhací letecký pluk) between 1955 and 1964.
26 Fighter Regiment (26. stíhací letecký pluk) between 1956 and 1958.
30 Fighter-Bomber Regiment (30. stíhací bombardovací letecký pluk) between 1958 and 1959.
2 Fighter-Bomber Regiment (2. stíhací bombardovací letecký pluk) between 1964 and 1968.


=== 1988–1993 ===
In November 1989 the communist leaders and guidelines fell across Czechoslovakia. The two parliaments of the two new states the Czech republic and Slovakia, dissolved their union on 1 January 1993. The assets of the former air force were divided 2:1 in the Czech favor, and thus the Czech Air Force and the Slovak Air Force were formed. The 18 MiG-29s then in service were divided 1:1 between the new countries.
A 1992-93 reorganisation resulted in a completely new structure of the Czech Air Force which came into effect in the course of 1994. One of the first units which closed down as a direct result of the transfer of a large number of aircraft to Slovakia was the 9th Fighter Bomber Air Regiment (9. SBoLP) at Bechyně.


== References ==
Notes

Bibliography


== External links ==
Global Security entry
Official Czech Army site