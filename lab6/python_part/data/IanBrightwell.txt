Ian Robert Brightwell (born 9 April 1968) is an English former professional footballer and manager. As a player he was a defender from 1986 to 2006 and who played 468 league games in a 20-year career the Football League and Premier League.
He started his professional career at Manchester City in 1986, having won the FA Youth Cup with the club, and remained at Maine Road for the next 12 years, helping City to win promotion out of the Second Division in 1988–89. He joined Coventry City in 1998, before moving on to Walsall two years later. He helped the "Saddlers" to win the Second Division play-offs in 2001, before he joined Stoke City in March 2002. After playing for the "Potters" in their Second Division play-off success in 2002, he moved on to Port Vale. He was appointed as a coach at Vale Park in June 2003, before joining Macclesfield Town as a player-coach a year later. He served the club as caretaker-manager in October 2006, before being given the job permanently in June 2007. He left Moss Rose in February 2008 after a poor start to the 2007–08 season.


== Playing career ==


=== Manchester City ===
Brightwell began his career at Manchester City, where he signed schoolboy forms at the age of 14. He was part of the 1986 FA Youth Cup winning team that also included the likes of Paul Lake and David White. Brightwell made his City debut under Billy McNeill on 23 August 1986 against Wimbledon, and therefore came into the first team picture just as City were being relegated out of the First Division in 1986–87 under McNeill and assistant turned replacement manager Jimmy Frizzell. Brightwell became known as a versatile player who played at every single outfield position during his career at the club; however he was most commonly used either as a right-back or as on the right side of midfield. City then finished ninth in the Second Division in 1987–88, before Mel Machin led them to promotion in 1988–89 with a second-place finish; they ended 17 points behind champions Chelsea and one point ahead of third-place Crystal Palace. Brightwell was also capped four times by England under-21s, scoring twice, in 1988 and 1989. He did not score many goals, but did memorably shoot into the top corner from 25 yards on 3 February 1990, to earn City a draw with rivals Manchester United at Old Trafford. They ended the 1989–90 season in 14th place under short-term boss Howard Kendall, behind United only on goal difference.
City shot up the table in 1990–91 under Peter Reid, finishing in fifth place, though still 21 points behind champions Arsenal. He helped City to record a fifth-place finish in 1991–92, some 12 points behind champions Leeds United. They then finished ninth in 1992–93, the first ever season of Premier League football. New manager Brian Horton led the club to disappointing 16th and 17th-place finishes in 1993–94 and 1994–95; Brightwell did not feature in the first team however, as he snapped his patella tendon, and was sidelined for more than a year. He did eventually recover, and returned to the City line-up for the 1995–96 campaign under Alan Ball, but could not prevent the "Sky Blues" from being relegated in 18th spot, finishing behind Southampton due to their inferior goal difference.
The 1996–97 season was turbulent, with Ball being replaced by Steve Coppell, who was in turn replaced by Frank Clark; Brightwell remained a constant first team presence however, making 39 appearances. He played just 25 games in 1997–98 though, and was powerless to prevent City from being relegated into the third tier for the first time in the club's history. He was given a testimonial match and a free transfer, having made 382 league and cup appearances, scoring 19 goals, in an 18-year association with the Maine Road club.


=== Coventry City ===
Brightwell joined Premier League side Coventry City for the 1998–99 season, but was given just one League Cup game by manager Gordon Strachan. He left Highfield Road at the end of the 1999–2000 season without having featured for the "Sky Blues" in the league. He was loaned out to First Division side Walsall at the end of the 1999–2000 campaign, playing ten games, but could not prevent the "Saddlers" from suffering relegation.


=== Walsall ===
Despite Walsall's relegation, he had impressed manager Ray Graydon during his time at the Bescot Stadium, and joined the club permanently in summer 2000. He played 54 games in the 2000–01 campaign, helping the club to qualify for the Second Division play-offs with a fourth-place finish. He played the full 120 minutes of the play-off final at the Millennium Stadium, as Walsall beat Reading 3–2 after extra-time, having to come from behind twice in the game.


=== Stoke City ===
He returned to the third tier when he joined Guðjón Þórðarson's Stoke City in March 2002. He played just four league games for the "Potters" in 2001–02, though came on for Tony Dinning 85 minutes into Stoke's 2–0 win over Brentford in the play-off final.


=== Port Vale ===
In August 2002 he moved on to local rivals Port Vale, who were back in the Second Division under the management of his former boss at Manchester City, Brian Horton. He played 38 games for the "Valiants" in 2002–03, before he was appointed as a coach at Vale Park in June 2003. He featured three times for the Vale first team in 2003–04, before he left the club in May 2004. He also served the club as caretaker-manager for less than 24 hours between Brian Horton's resignation and Martin Foyle's appointment in February 2004.


=== Macclesfield Town ===
He then joined Brian Horton at Macclesfield Town as a reserve team coach, and also remained registered as a player. He played six League Two and two FA Cup games for the "Silkmen" in 2004–05, all in the first half the campaign. He then played 11 league games in the 2005–06 campaign, and played five league and cup games at the start of the 2006–07 season.


== Management career ==
He was appointed caretaker-manager at Macclesfield Town on 2 October 2006 after the sacking of manager of Brian Horton, before the board appointed Paul Ince as permanent manager three weeks later; Ince took them to a 22nd-place finish in 2006–07, one place and two points above the relegation zone. Ince resigned on 24 June 2007 to take over at Milton Keynes Dons, and Macclesfield appointed Brightwell as permanent manager, with Asa Hartford as his assistant. The pair left the club in February 2008 after a poor run of results and were replaced by Keith Alexander; Brightwell was given the opportunity to stay at Moss Rose as Alexander's assistant manager, but declined the offer. Alexander kept the cub in the Football League with a 19th-place finish in 2007–08.
In October 2008, Brightwell was brought to back to Port Vale by Dean Glover in a temporary coaching capacity. He spent five months with the "Valiants", leaving the club at the end of February 2009. He appeared in the Master's Tournament at the 2009 HKFC International Soccer Sevens, and began working at BBC Radio Manchester as a co-commentator.


== Personal life ==
Brightwell was born in Lutterworth but grew up in Congleton, with his parents; Olympic 800m gold medalist Ann Packer and 400m runner Robbie Brightwell. His younger brother David also played for Manchester City.
He is a married man and has two children with wife Sally.


== Career statistics ==


=== As a player ===
Sourced from The English National Football Archive


=== As a manager ===


== Honours ==
with Manchester City
FA Youth Cup winner: 1986
Football League Second Division runner-up: 1988–89
with Walsall
Football League Second Division play-off winner: 2001
with Stoke City
Football League Second Division play-off winner: 2002


== References ==


== External links ==
Ian Brightwell career statistics at Soccerbase
Ian Brightwell management career statistics at Soccerbase