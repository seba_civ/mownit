The 471st Tactical Electronic Warfare Squadron is an inactive unit of the United States Air Force. It was formed in 1985 by the consolidation of the World War II 71st Liaison Squadron and the Cold War era 471st Fighter-Bomber Squadron of the Air Force Reserve.


== History ==


=== World War II ===
The first predecessor of the squadron was the 71st Liaison Squadron, which was activated overseas at Ondal, India in June 1943. The squadron served in India, Burma and China until the end of the war. It was inactivated in India in December 1945.


=== Cold War ===
The 471st Fighter-Bomber Squadron was activated at Selfridge Air Force Base, Michigan in April 1954. It trained with Republic F-84 Thunderjets until 1957, ehen its parent 439th Fighter-Bomber Group converted to a troop carrier unit.
In September 1985 the 71st Liaison Squadron and the 471st Fighter-Bomber Squadron were consolidated as the 471st Tactical Electronic Warfare Training Squadron.


== Lineage ==
71st Liaison Squadron
Constituted as the 71st Liaison Squadron on 17 June 1943
Activated on 15 July 1943
Inactivated on 8 December 1945
Consolidated on 19 September 1985 with the 471st Fighter-Bomber Squadron as the 471st Tactical Electronic Warfare Training Squadron
471st Tactical Electronic Warfare Squadron
Constituted as the 471st Fighter-Bomber Squadron
Activated on 1 April 1954
Inactivated on 1 July 1957
Consolidated on 19 September 1985 with the 71st Liaison Squadron as the 471st Tactical Electronic Warfare Training Squadron


=== Assignments ===
Tenth Air Force: 15 July 1943
United States Army Forces, China-Burma-India: 19 August 1943 (attached to 5903d Combat Troops (later 5903d Area Command, Northern Combat Area Command) from c. Oct 1943
Army Air Forces, India-Burma Sector: 2 March 1944 (remained attached to Northern Combat Area Command)

Detachment attached to Y Force: November 1943 - c. 1 July 1944

Tenth Air Force: 21 August 1944 (attached to 1st Liaison Group (Provisional) 19 August 1944 - 30 April 1945)
Fourteenth Air Force: 6 July 1945 (attached to 14th AF Tactical Air Command 24 July 1945 - 1 August 1945)
Tenth Air Force: 1 August 1945 - 8 December 1945
439th Fighter-Bomber Group: 1 April 1954 – 1 July 1957


=== Stations ===


=== Aircraft ===


=== Campaigns ===


== See also ==
List of MAJCOM wings of the United States Air Force


== References ==


=== Notes ===


=== Bibliography ===
 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.
Maurer, Maurer, ed. (1983) [1961]. Air Force Combat Units of World War II (PDF) (reprint ed.). Washington, DC: Office of Air Force History. ISBN 0-912799-02-1. LCCN 61060979. 
Maurer, Maurer, ed. (1982) [1969]. Combat Squadrons of the Air Force, World War II (PDF) (reprint ed.). Washington, DC: Office of Air Force History. ISBN 0-405-12194-6. LCCN 70605402. OCLC 72556. 
Further reading
Cantwell, Gerald T. (1997). Citizen Airmen: a History of the Air Force Reserve, 1946-1994. Washington, D.C.: Air Force History and Museums Program. ISBN 0-16049-269-6.