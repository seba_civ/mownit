Norman Doray whose real name is Jérémy Lecarour, is a French musician, producer and DJ of electronic music.


== Biography ==
Norman was born in Brittany , where first discovered disco music thanks to his family and later be linked with electronic music by artists like Daft Punk , Cassius and Junior Jack . Norman started playing in his 20s, becoming a resident of several bars and clubs in France, among which stand out Le Queen in Paris. In 2005, he made his first steps as a producer and started making music on their own. Their first release was next to Pierre de la Touche and Arno Cost in the musical project called "The Freshmakers".
Norman discovers his true passion for production and soon launched his first productions through Serial Records in France.
In late 2007, Norman will join again with his friend Arno Cost to create one of his most famous productions, "Apocalypse". This song gave both Norman and Arno some recognition on the world scene of electronic music, and in turn, Norman gets his first Essential New Tune on the prestigious radio program of Pete Tong.
Thanks to this success, in 2008, Norman will be required for remixing songs like "Humanoidz" for Tom de Neef & Laidback Luke , " Delirious " by David Guetta and "Kalle" Zoo Brazil. Along the project "The Freshmakers" produce their second single "Miracle", which was known in the dance floors by a remix by the German Mischa Daniëls.
They are also known collaborations with artists such as Avicii , Tristan Garner , David Tort , Albin Myers and Laidback Luke . Several of his productions are included in the hallmarks of most important electronic music, including Size Records (label of the Swedish producer Steve Angello ) Axtone (the record of Axwell ), Cr2, Strictly Rhythm, Warner Music and the legendary American label, Atlantic Records .
After releasing "Kalifornia" by the label Strictly Rhythm in 2011, Norman was honored to mix with Dirty South and Avicii in a series of compilations for the label Strictly presenting Strictly Ibiza to Amsterdam.
Norman also has the recognition of radios worldwide, including BBC in England through the weekly Pete Tong, who regularly includes productions of exclusva way "Apocalypse", "Last Forever", "Tobita" and "Chase the Sun" referred to as "Essential New Tune".
Norman also has a strong presence in Ibiza every year, hitting the scene from the center of influence. Besides playing for festivals organized by Erick Morillo , David and Cathy Guetta is often invited to perform in your party concept F *** Me I'm Famous in Ibiza . In April 2011, David Guetta summon Norman Doray to play at the opening of his tour of the UK, which allowed him to act in some of the most renowned places in England, such as Apollo Manchester, Leeds Academy, and the legendary Academy Brixton in London. Norman continues to impress David because this gives an opportunity to produce a remix of Little Bad Girl .
Parallel to his solo performances, Norman is invited to play along with the Swedish House Mafia at Pacha Ibiza during the Masquerade Motel event and, since 2009, usually occupies scenarios of major festivals such as Creamfields England.
EIn 2012, its co-production stands "Celsius" with German Eddie Thoneick and his work with Australian twins NERVO in "Something to Believe in".


== Discography ==


=== Singles ===
2006: "Let U Go" (como The Freshmakers) (Serial Records)
2006: "In The Name of Love"
2006: "Dance All Night EP" (con Pierre de la Touche) (Disco Galaxy)
2007: "Jetlag" (Serial Records)
2007: "Apocalypse" (con Arno Cost) (CR2 Records)
2008: "Miracle" (como The Frenchmakers) (Serial Records)
2008: "Krystal" (CR2 Records
2009: "Last Forever" (con Tristan Garner & Errol Reid) (CR2 Records)
2009: "Drink N Dial" (con Albin Myers) (Joia Records/Serial Records)
2009: "Tobita" (CR2 Records)
2010: "Chase the Sun" (con David Tort) (Nero Records)
2010: "Tweet it" (con Tim Berg & Sebastien Drums) (Size Records)
2010: "Paradisco" (Arno Cost, Norman Doray & Ben Macklin)
2011: "Champagne" (con Richard Grey)
2011: "Kalifornia" (Strictly Rhythm)
2011: "Breakaway" Ft Tawiah (Warner Music)
2012: "Don't Give Up" bootleg (con Albin Myers)"
2012: "Trilogy" (con Laidback Luke & Arno Cost) (Mixmash Records)
2012: "Leo" (Spinnin Records)
2012: "Cracks" Ft Andreas Moe (Spinnin Records)
2012: "Music" (con Mitomi Tokoto) (Azuli Records)
2012: "Celsius" (con Eddie Thoneick) (Size Records)
2012: "Something To Believe In" (con Nervo & Cookie) (Spinnin Records)
2013: "Filtre" (LE7ELS)
2013: "TroubleMaker" (Size Records)
2014: Arno Cost & Norman Doray feat. Dev – "Darkest Days (Apocalypse 2014)" (Spinnin' Records)
2014: "Strong" (con Arno Cost) (Size Records)
2015: "Rising Love" (Arno Cost & Norman Doray con Mike Taylor) (Polydor)


=== Remixes ===
2006:
Da Sushiman – "In & Out" (The Freshmakers Remix) – Ledge Music
Rilod – Thriller (The Freshmakers Remix – Nice Music
Mathieu Bouthier & Muttonheads aka Serial Crew – Make Your Own Kind of Music (The Frenchmakers Remix) – Serial Records
2007:
Henrik B – Soul Heaven (Norman Doray Remix) – Nero Records
Sunfreakz – Counting Down the Days (Norman Doray & Arno Cost aka Le Monde Remix) – Pool E Music
Antoine Clamaran feat. Lulu Hughes – Give Some Love (Arno Cost & Norman Doray aka Le Monde Remix) – Pool E Music
2008:
David Guetta Feat. Tara McDonald – Delirious (Norman Doray & Arno Cost Remix) – EMI
Tom De Neef & Laidback Luke – Humanoidz (Norman Doray & Arno Cost Remix) – MixMash Records
Seamus Haji vs Lords of Flatbush – 24 Hours (Nice Tight Derriere) (Norman Doray Remix) – Big Love
The Cube Guys – Baba O'Riley (Norman Doray & Arno Cost Remix) – Ministry of Sound Uk
Zoo Brazil – Kalle (Norman Doray & Arno Cost Remix) – Nero Records
2009:
Those Usual Suspects – Shadows (Norman Doray Remix) – Ministry of Sound Australia
David Guetta feat. Kelly Rowland – When Love Takes Over (Norman Doray & Arno Cost Remix) – EMI
The Face vs Mark Brown & Adam Shaw – Needin U (Norman Doray Eivissa Remix) – CR2 Records
Eric Prydz & Steve Angello – Woz not Noz (Norman Doray & Arno Cost Tribute Remix) – CR2 Records
2010:
Toni Braxton – Make My Heart (Norman Doray Remix) – Atlantic Records
Henrik B Feat. Christian Alvestam – Now and Forever (Norman Doray Cazzo Remix) – Nero Records
Dirty South feat. Rudy – Phazing (Norman Doray Remix) – Phazing Records
2011:
Sandy Rivera & Rae – Hide U (Norman Doray Remix) – Defected Records
David Tort feat. Gosha – One Look (Norman Doray Remix) – Axtone Records
David Guetta feat. Taio Cruz – Little Bad Girl (Norman Doray Remix) – EMI
2012:
Kaskade & Skrillex – Lick It (Norman Doray Remix) – Ultra
Sneaky Sound System – Friends (Norman Doray Remix) – Modular
2013:
Robin Thicke feat. Kendrick Lamar – Give It 2 U (Norman Doray & Rob Adans Remix) – Star Trak
2014:
Andrew Bayer – England (Norman Doray Edit) (Norman Doray Edit) – Anjunadeep
Disclosure feat. Sam Smith – Latch (Norman Doray Remix) – PMR
La Roux – Uptight Downtown (Norman Doray Remix) – Polydor
2015:
Cerrone – Love in C Minor (Norman Doray LA Remix) – Malligator


== References ==


== External links ==
Official website
Norman Doray on Facebook
Norman Doray on Twitter
Norman Doray on SoundCloud