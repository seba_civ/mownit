import json
import os
import sys

from numpy import dot
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import svds


def main():
    svd_k = int(sys.argv[1])

    with open(os.path.join(os.getcwd(), "bag.json"), encoding='utf-8') as json_file:
        bag = json.load(json_file)

    with open(os.path.join(os.getcwd(), "files.json"), encoding='utf-8') as json_file:
        files = json.load(json_file)

    val = [(k, i, v) for i, d in enumerate(files) for k, v in d.items()]
    rows = [e[0] for e in val]
    cols = [e[1] for e in val]
    data = [e[2] for e in val]

    matrix = csc_matrix((data, (rows, cols)), shape=(len(bag), len(files)))
    u, s, vt = svds(matrix, svd_k)
    n = len(s)
    # reverse the n first columns of u
    u[:,:n] = u[:, n-1::-1]
    # reverse s
    s = s[::-1]
    # reverse the n first rows of vt
    vt[:n, :] = vt[n-1::-1, :]

    diagonal_matrix = csc_matrix((s, ([i for i in range(n)], [i for i in range(n)])), shape=(len(s), len(s))).todense()
    new_v = dot(diagonal_matrix, vt)

    print(u.shape, new_v.shape)

    result_u = [dict([(i, e) for i, e in enumerate(u[:, col])]) for col in range(svd_k)]
    with open('svd_result_u.json', 'w', encoding='utf-8') as json_file:
        json.dump(result_u, json_file)

    result_v = [dict([(i, new_v[row, i]) for i in range(len(files))]) for row in range(svd_k)]
    with open('svd_result_v.json', 'w', encoding='utf-8') as json_file:
        json.dump(result_v, json_file)


if __name__ == "__main__":
    main()
